package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.UniversityData;

public class AddOtherUniversityPersister extends SingletonRule {

	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
	private String dbConnName;

	public AddOtherUniversityPersister(String ruleName, String dbconnName) {
		super(ruleName);
		this.dbConnName = dbconnName;
	}

	@Override
	public RuleDecisionKey executeRule(FlowContext flowContext) {
		UniversityData univData = (UniversityData) flowContext
				.getFromContext(EzedKeyConstants.UNIVERSITY_DATA);

		Connection conn = DbConnectionManager.getConnectionByName(dbConnName);

		Response response = (Response) flowContext
				.getFromContext(EzedKeyConstants.EZED_RESPONSE);
		PreparedStatement ps = null;

		try {
			ps = DbConnectionManager.getPreparedStatement(dbConnName, conn,
					"InsertOtherUniversity");
			ps.setString(1, univData.getUnivName());
			ps.setString(2, univData.getPhoneNo());
			ps.setString(3, univData.getCityName());
			ps.setString(4, univData.getWebsite());
			ps.setInt(5, univData.getShowFlag());
			System.out.println("getShowFlag():"+univData.getShowFlag());
			ps.setString(6, univData.getCreatedBy());
			ps.setInt(7, univData.getUserFlag());
			System.out.println("getUserFlag():"+univData.getUserFlag());
			ps.setString(8, univData.getCreatedOn());
			
			System.out.println(ps);
			
			int rows = ps.executeUpdate();
			if (logger.isDebugEnabled()) {
				logger.debug(rows + " rows inserted in database when "
						+ univData.getUnivName() + "was added.");
			}
			response.setParameters(EzedTransactionCodes.SUCCESS, "Success");
			return RuleDecisionKey.ON_SUCCESS;
		} catch (SQLException e) {
			logger.error(
					"DB error while inserting other university name by user in db",
					e);
			response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED,
					"Error while processing add other university request by "
							+ univData.getCreatedBy());
			return RuleDecisionKey.ON_FAILURE;
		} catch (Exception e) {
			logger.error(
					"error while inserting other university name by user in db",
					e);
			response.setParameters(EzedTransactionCodes.INTERNAL_ERROR,
					"Error while processing add other university request");
			return RuleDecisionKey.ON_FAILURE;
		} finally {
			DbConnectionManager.releaseConnection(dbConnName, conn);
		}

	}
}
