package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.a.helper.utils.StringSplitter;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.utils.EzedHelper;

public class PointsActionUpdater extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public PointsActionUpdater(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      response.setStringOnlySend(true);

      String newPoints = messageData.getParameter("value");
      if (newPoints == null || newPoints.trim().compareTo("") == 0 || !EzedHelper.isNumeric(newPoints.trim()))
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Points to be updated is invalid " + newPoints);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "");
         return RuleDecisionKey.ON_FAILURE;
      }
      int newPointsInt = 0;
      try
      {
         newPointsInt = Integer.parseInt(newPoints.trim());
      }
      catch (Exception e)
      {
         logger.error("Points to be updated is invalid " + newPoints);
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "");
         return RuleDecisionKey.ON_FAILURE;
      }

      if (newPointsInt <= 0)
      {
         logger.error("Points to be updated is invalid " + newPoints);
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "");
         return RuleDecisionKey.ON_FAILURE;
      }
      
      String origRowData = messageData.getParameter("rowdata");
      if (origRowData == null || origRowData.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Orig row data is invalid " + origRowData);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "");
         return RuleDecisionKey.ON_FAILURE;
      }

      String[] origRowStrings = StringSplitter.splitString(origRowData, "~~");
      if (origRowStrings.length < 5)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Orig row data is invalid since its length is not proper " + origRowData);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "");
         return RuleDecisionKey.ON_FAILURE;
      }

      String actionId = origRowStrings[1];
      if (actionId == null || actionId.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Action id in request is not proper " + actionId);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "");
         return RuleDecisionKey.ON_FAILURE;
      }

      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdatePointAction");
         ps.setInt(1, newPointsInt);
         ps.setString(2, actionId);
         int rows = ps.executeUpdate();
         if (rows > 0)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug(rows + " rows updated in db while updating points for action id " + actionId);
            }
            response.setParameters(EzedTransactionCodes.SUCCESS, newPoints);
            return RuleDecisionKey.ON_SUCCESS;
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Points cannot be updated in request for action id " + actionId);
            }
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "");
            return RuleDecisionKey.ON_FAILURE;
         }
      }
      catch (SQLException e)
      {
         logger.error("DB error while updating points for action id " + actionId, e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while updating points for action id " + actionId, e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
}
