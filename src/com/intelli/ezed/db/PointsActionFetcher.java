package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.ActionData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.FrontendFetchData;
import com.intelli.ezed.data.Response;

public class PointsActionFetcher extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public PointsActionFetcher(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      String[] pointsActionArr = null;
      FrontendFetchData pointsActionData = new FrontendFetchData();
      response.setResponseObject(pointsActionData);
      response.setStatusCodeSend(false);
      pointsActionData.setAttribName("aaData");
      ArrayList<String[]> pointActions = new ArrayList<String[]>();
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchActionDetails");
         rs = ps.executeQuery();
         int i = 1;
         int actionType = 0;
         int points = 0;
         while (rs.next())
         {
            points = rs.getInt(3);
            if (points == 0)
            {
               continue;
            }
            pointsActionArr = new String[5];
            pointsActionArr[0] = Integer.toString(i);
            pointsActionArr[1] = rs.getString(1);
            pointsActionArr[2] = rs.getString(2);
            pointsActionArr[3] = Integer.toString(points);
            actionType = rs.getInt(4);
            if (actionType == ActionData.ONE_TIME_ACTION)
            {
               pointsActionArr[4] = "ONE TIME ACTION";
            }
            else
            {
               pointsActionArr[4] = "REPEATABLE ACTION";
            }
            i++;
            pointActions.add(pointsActionArr);
         }
         pointsActionData.setAttribList(pointActions);
         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
      }
      catch (SQLException e)
      {
         logger.error("DB error while fetching list of action points", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "DB error while fetching action points");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching list of action points", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching action points");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing result set while fetching action points list", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
      return RuleDecisionKey.ON_SUCCESS;
   }
}
