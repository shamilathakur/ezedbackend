package com.intelli.ezed.data;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

public class CourseCalendarData extends ResponseObject
{
   private String courseId;
   private ArrayList<GoalData> goalDatas;
   private Date startDate;
   private Date endDate;

   public Date getStartDate()
   {
      return startDate;
   }

   public void setStartDate(Date startDate)
   {
      this.startDate = startDate;
   }

   public Date getEndDate()
   {
      return endDate;
   }

   public void setEndDate(Date endDate)
   {
      this.endDate = endDate;
   }

   public String getCourseId()
   {
      return courseId;
   }

   public void setCourseId(String courseId)
   {
      this.courseId = courseId;
   }

   public ArrayList<GoalData> getGoalDatas()
   {
      return goalDatas;
   }

   public void setGoalDatas(ArrayList<GoalData> goalDatas)
   {
      this.goalDatas = goalDatas;
   }

   public void checkGoalValidityForUpdate(CourseCalendarData validData, int dayBufferBeforeEnd)
   {
      Iterator<GoalData> goalIte = getGoalDatas().iterator();
      while (goalIte.hasNext())
      {
         validData.isNewGoalValid(goalIte.next(), dayBufferBeforeEnd);
      }
   }

   public void updateNewGoal(GoalData goalData)
   {
      GoalData singleGoalData = null;
      for (int i = 0; i < getGoalDatas().size(); i++)
      {
         singleGoalData = getGoalDatas().get(i);
         if (singleGoalData.getCourseId().compareTo(goalData.getCourseId()) == 0 && singleGoalData.getChapterId().compareTo(goalData.getChapterId()) == 0 && singleGoalData.getContentId().compareTo(goalData.getContentId()) == 0)
         {
            getGoalDatas().remove(i);
            getGoalDatas().add(goalData);
            return;
         }
      }
   }

   public boolean isNewGoalValid(GoalData goalData, int dayBufferBeforeEnd)
   {
      Calendar courseStartCalendar = Calendar.getInstance();
      Calendar courseEndCalendar = Calendar.getInstance();
      Calendar todayCalendar = Calendar.getInstance();
      Calendar newGoalCalendar = Calendar.getInstance();

      courseStartCalendar.setTime(getStartDate());
      courseEndCalendar.setTime(getEndDate());
      todayCalendar.setTime(new Date(System.currentTimeMillis()));
      newGoalCalendar.setTime(goalData.getEndDate());

      courseEndCalendar.add(Calendar.DATE, -dayBufferBeforeEnd);
      todayCalendar.add(Calendar.DATE, -1); //to allow goals ending today

      if (newGoalCalendar.before(courseStartCalendar))
      {
         goalData.setValidity(GoalData.GOAL_BEFORE_STARTDATE);
         return false;
      }

      if (newGoalCalendar.after(courseEndCalendar))
      {
         goalData.setValidity(GoalData.GOAL_AFTER_ENDDATE);
         return false;
      }

      if (newGoalCalendar.before(todayCalendar))
      {
         goalData.setValidity(GoalData.GOAL_BEFORE_TODAY);
         return false;
      }

      GoalData singleGoalData = null;
      for (int i = 0; i < getGoalDatas().size(); i++)
      {
         singleGoalData = getGoalDatas().get(i);
         if (singleGoalData.getCourseId() == goalData.getCourseId() && singleGoalData.getChapterId() == goalData.getChapterId() && singleGoalData.getContentId() == goalData.getContentId())
         {
            if (singleGoalData.isGoalComplete())
            {
               goalData.setValidity(GoalData.GOAL_ALREADY_COMPLETE);
               return false;
            }
         }
      }
      goalData.setValidity(GoalData.GOAL_VALID_FOR_UPDATE);
      return true;
   }

   //   @Override
   //   public void prepareResponse() throws Exception
   //   {
   //      GoalData[] goalDataArray = new GoalData[getGoalDatas().size()];
   //
   //      for (int i = 0; i < getGoalDatas().size(); i++)
   //      {
   //         getGoalDatas().get(i).prepareResponse();
   //         goalDataArray[i] = getGoalDatas().get(i);
   //      }
   //      //      this.put("courseid", getCourseId());
   //      this.put("goals", goalDataArray);
   //   }

   @Override
   public void prepareResponse() throws Exception
   {
      StringBuffer sb = new StringBuffer();
      for (int i = 0; i < getGoalDatas().size(); i++)
      {
         sb.append("{").append(getGoalDatas().get(i).getString(i + 1)).append("},");
      }
      if (sb.length() >= 1)
      {
         sb.deleteCharAt(sb.length() - 1);
      }
      this.put("goals", sb.toString());
   }
}
