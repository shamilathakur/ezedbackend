package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class UpdateChallengeTest extends SingletonRule
{

   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;

   public UpdateChallengeTest(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {

      String testSessionId = (String) flowContext.getFromContext(EzedKeyConstants.TEST_SESSION_ID);
      String challengeId = (String) flowContext.getFromContext(EzedKeyConstants.CHALLENGE_ID);
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      Connection connection = DbConnectionManager.getConnectionByName(dbConnName);

      //update challengetest set challengeetestsessionid = ? where challengeid = ?
      PreparedStatement preparedStatement = DbConnectionManager.getPreparedStatement(dbConnName, connection, "UpdateChallengeTest");
      try
      {
         preparedStatement.setString(1, testSessionId);
         preparedStatement.setString(2, challengeId);
         int i = preparedStatement.executeUpdate();
         if (i != 1)
         {
            logger.debug("Number if rows updated in challengetest are : " + i);
            response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while updating. No rows updated in challengetest. " + i);
            return RuleDecisionKey.ON_FAILURE;
         }

      }
      catch (SQLException e)
      {
         logger.error("Database Exception occoured.", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Internal Error. Please try again later.");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Exception occoured.", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal Error. Please try again later.");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, connection);
      }
      return RuleDecisionKey.ON_SUCCESS;
   }
}
