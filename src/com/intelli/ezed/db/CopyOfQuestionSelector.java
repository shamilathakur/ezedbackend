package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.QuestionContent;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.TestDetails;
import com.intelli.ezed.data.TestRequestResponse;
import com.intelli.ezed.data.UserProfile;

public class CopyOfQuestionSelector extends SingletonRule
{

   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;
   private boolean whetherFirstQuestion;

   public CopyOfQuestionSelector(String ruleName, String dbConnName, String whetherFirstQuestion)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
      this.whetherFirstQuestion = Boolean.parseBoolean(whetherFirstQuestion);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      UserProfile userProfile = (UserProfile) flowContext.getFromContext(EzedKeyConstants.USER_DETAILS);
      TestDetails testDetails = (TestDetails) flowContext.getFromContext(TestDetails.TEST_DETAILS);
      EzedRequestData requestData = (EzedRequestData) flowContext.getFromContext(EzedRequestData.EZED_REQUEST_DATA);

      String testId = testDetails.getTestid();
      testDetails.setTestid(testId);
      testDetails.setDifficultyLevel(userProfile.getUserLevel());

      if (!whetherFirstQuestion)
      {
         testDetails.setUserAns(requestData.getUserTestAns());
         //Handling Difficulty Level
         if (testDetails.getLastQuestionCorrectAns().compareTo(testDetails.getUserAns()) == 0)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("User ans is correct. Increasing difficulty level.");
            }
            testDetails.increaseDifficultyLevel();
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("User ans is wrong. Decreasing difficulty level.");
            }
            testDetails.decreaseDifficultyLevel();
         }
      }
      int difficultyLevel = testDetails.getDifficultyLevel();


      TestRequestResponse testResponse = new TestRequestResponse();
      String testSessionId = (String) flowContext.getFromContext(EzedKeyConstants.TEST_SESSION_ID);

      Connection connection = DbConnectionManager.getConnectionByName(dbConnName);
      ResultSet resultSet = null;
      try
      {
         /*
          *  select * from randomizedquestionbank 
          *  where chapterid = ? 
          *  and difficultylevel > ? 
          *  and questionid not in (select questionid from ongoingtest where userid = ? and testid = ?) 
          *  ORDER BY random() limit 1
          */
         PreparedStatement getNextQuestion = DbConnectionManager.getPreparedStatement(dbConnName, connection, "GetNextQuestion");
         getNextQuestion.setString(1, testDetails.getChapterId());
         getNextQuestion.setInt(2, difficultyLevel);
         getNextQuestion.setString(3, userProfile.getUserid());
         getNextQuestion.setString(4, testId);
         getNextQuestion.setString(5, testSessionId);

         resultSet = getNextQuestion.executeQuery();

         if (resultSet != null && resultSet.next())
         {
            testResponse.setQuestiontype(resultSet.getInt("questionType"));
            testResponse.setQuestionId(resultSet.getString("questionid"));
            testResponse.setQuestionNumberOfRandomQuestion(resultSet.getInt("questionnumber"));

            //select question 
            QuestionContent question = new QuestionContent();
            int i = resultSet.getInt("questioncontenttype");
            question.setType(i); //type 0 = no data , 1 = string, 2 = content
            if (i == QuestionContent.CONTENT_ABSENT)
            {
               //set No content
            }
            else if (i == QuestionContent.STRING_TYPE)
            {
               question.setContentString(resultSet.getString("questionstring"));
            }
            else if (i == QuestionContent.CONTENT_TYPE)
            {
               question.setContentString(resultSet.getString("questioncontent"));
            }
            //Setting the question into the response object
            testResponse.setQuestion(question);

            //Creating a array list to put all the answers inside it
            ArrayList<QuestionContent> answers = new ArrayList<QuestionContent>();

            QuestionContent ans1 = new QuestionContent();
            i = resultSet.getInt("ans1type");
            ans1.setType(i);
            if (i == QuestionContent.CONTENT_ABSENT)
            {
               //set No content
            }
            else
            {
               if (i == QuestionContent.STRING_TYPE)
               {
                  ans1.setContentString(resultSet.getString("ans1string"));
               }
               else if (i == QuestionContent.CONTENT_TYPE)
               {
                  ans1.setContentString(resultSet.getString("ans1content"));
               }
               //adding answers to array list
               answers.add(ans1);
            }


            QuestionContent ans2 = new QuestionContent();
            i = resultSet.getInt("ans2type");
            ans2.setType(i);
            if (i == QuestionContent.CONTENT_ABSENT)
            {
               //set No content
            }
            else
            {
               if (i == QuestionContent.STRING_TYPE)
               {
                  ans2.setContentString(resultSet.getString("ans2string"));
               }
               else if (i == QuestionContent.CONTENT_TYPE)
               {
                  ans2.setContentString(resultSet.getString("ans2content"));
               }
               //adding answers to array list
               answers.add(ans2);
            }

            QuestionContent ans3 = new QuestionContent();
            i = resultSet.getInt("ans3type");
            ans3.setType(i);
            if (i == QuestionContent.CONTENT_ABSENT)
            {
               //set No content
            }
            else
            {
               if (i == QuestionContent.STRING_TYPE)
               {
                  ans3.setContentString(resultSet.getString("ans3string"));
               }
               else if (i == QuestionContent.CONTENT_TYPE)
               {
                  ans3.setContentString(resultSet.getString("ans3content"));
               }
               //adding answers to array list
               answers.add(ans3);
            }


            QuestionContent ans4 = new QuestionContent();
            i = resultSet.getInt("ans4type");
            ans4.setType(i);
            if (i == QuestionContent.CONTENT_ABSENT)
            {
               //set No content
            }
            else
            {
               if (i == QuestionContent.STRING_TYPE)
               {
                  ans4.setContentString(resultSet.getString("ans4string"));
               }
               else if (i == QuestionContent.CONTENT_TYPE)
               {
                  ans4.setContentString(resultSet.getString("ans4content"));
               }
               //adding answers to array list
               answers.add(ans4);
            }


            QuestionContent ans5 = new QuestionContent();
            i = resultSet.getInt("ans5type");
            ans5.setType(i);
            if (i == QuestionContent.CONTENT_ABSENT)
            {
               //set No content
            }
            else
            {
               if (i == QuestionContent.STRING_TYPE)
               {
                  ans5.setContentString(resultSet.getString("ans5string"));
               }
               else if (i == QuestionContent.CONTENT_TYPE)
               {
                  ans5.setContentString(resultSet.getString("ans5content"));
               }
               //adding answers to array list
               answers.add(ans5);
            }


            QuestionContent ans6 = new QuestionContent();
            i = resultSet.getInt("ans6type");
            ans6.setType(i);
            if (i == QuestionContent.CONTENT_ABSENT)
            {
               //set No content
            }
            else
            {
               if (i == QuestionContent.STRING_TYPE)
               {
                  ans6.setContentString(resultSet.getString("ans6string"));
               }
               else if (i == QuestionContent.CONTENT_TYPE)
               {
                  ans6.setContentString(resultSet.getString("ans6content"));
               }
               //adding answers to array list
               answers.add(ans6);
            }

            //Adding Arraylist to Response
            testResponse.setAnswers(answers);
            testResponse.setCorrectAnswer(resultSet.getString("correctans"));
            testDetails.setLastQuestionCorrectAns(testResponse.getCorrectAnswer());
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while getting test request", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while processing test request");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while getting test request", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while processing test request");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         try
         {
            resultSet.close();
         }
         catch (SQLException e)
         {
            logger.error("Error while closing resultset", e);
         }
         DbConnectionManager.releaseConnection(dbConnName, connection);
      }
      response.setResponseObject(testResponse);
      testDetails.setSentQuestionDetails(testResponse);
      response.setParameters(EzedTransactionCodes.SUCCESS, "Sending first question in response.");
      return RuleDecisionKey.ON_SUCCESS;
   }
}
