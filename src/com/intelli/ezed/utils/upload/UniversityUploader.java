package com.intelli.ezed.utils.upload;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import au.com.bytecode.opencsv.CSVReader;

public class UniversityUploader
{
   public static void main(String[] args) throws FileNotFoundException
   {
      File file = new File("samplebulkdata/collegelistmumbai.csv");
      //      BufferedReader reader = new BufferedReader(new FileReader(file));
      Connection conn = null;
      PreparedStatement ps = null;
      CSVReader reader = new CSVReader(new FileReader(file));
      try
      {
         String[] read = reader.readNext();
         read = reader.readNext();
         while (read != null)
         {
            conn = getDbConnectionPostgres();
            ps = getSelectPostStatement(conn);
            try
            {
               ps.setString(1, read[1]);
               ps.setString(2, "mumbai university");
               ps.setString(3, read[2]);
               ps.setString(4, read[3]);
               ps.setString(5, "admin@ezed.in");
               int rows = ps.executeUpdate();
               if (rows == 1)
               {
                  System.out.println("SUCCESS " + read[0] + " " + read[1] + " " + read[2] + " " + read[3]);
               }
            }
            catch (SQLException e)
            {
               // TODO Auto-generated catch block
               e.printStackTrace();
            }
            finally
            {
               try
               {
                  ps.close();
                  conn.close();
               }
               catch (SQLException e)
               {
                  // TODO Auto-generated catch block
                  e.printStackTrace();
               }
               read = reader.readNext();
            }
         }
      }
      catch (IOException e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
      finally
      {
         try
         {
            reader.close();
         }
         catch (IOException e)
         {
            // TODO Auto-generated catch block
            e.printStackTrace();
         }
      }
   }

   private static Connection getDbConnectionPostgres()
   {
      try
      {
         String url = "jdbc:postgresql://localhost:15432/ezed";
         Class.forName("org.postgresql.Driver");
         return DriverManager.getConnection(url, "postgres", "postgres");
      }
      catch (Exception e)
      {
         e.printStackTrace();
         System.out.println("Error initialising Postgres connection");
         System.exit(1);
         return null;
      }
   }

   private static PreparedStatement getSelectPostStatement(Connection conn)
   {
      try
      {
         return (conn.prepareStatement("insert into collegelist (collegename,univname,website,phonenumber,showflag,addedby) values (?,?,?,?,1,?)"));
      }
      catch (Exception e)
      {
         System.out.println("Error initializing postgres statment");
         System.exit(1);
         return null;
      }
   }
}
