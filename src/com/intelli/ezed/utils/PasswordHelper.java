package com.intelli.ezed.utils;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.axis.encoding.Base64;
import org.sabre.utils.HexUtil;

import com.a.helper.data.DataFiller;


public class PasswordHelper
{

   private static Cipher encryptCypher = null;
   private static Cipher decryptCypher = null;
   private static Cipher internalEncCipher = null;
   private static Cipher internalDecCipher = null;

   private static String key = "YTc5NDZmYzIyNGI4NTNjZQ==";
   public static void initializeCipher(String desEdeKey) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, NumberFormatException, IllegalBlockSizeException, BadPaddingException
   {
      IvParameterSpec ivInt = new IvParameterSpec(new byte[8]);
      byte[] zpkBytesInt = HexUtil.hex2data(new String(Base64.decode(key)));
      SecretKey zpkKeySpecInt = new SecretKeySpec(zpkBytesInt, "DES");

      internalEncCipher = Cipher.getInstance("DES/CBC/NoPadding");
      internalDecCipher = Cipher.getInstance("DES/CBC/NoPadding");

      internalEncCipher.init(Cipher.ENCRYPT_MODE, zpkKeySpecInt, ivInt);
      internalDecCipher.init(Cipher.DECRYPT_MODE, zpkKeySpecInt, ivInt);

      String decryptedKey = getDecryptedKey(desEdeKey);

      IvParameterSpec iv = new IvParameterSpec(new byte[8]);
      String encoded64key = new String(HexUtil.hex2data(decryptedKey));
      encoded64key = new String(Base64.decode(encoded64key));
      byte[] zpkBytes = HexUtil.hex2data(encoded64key);
      SecretKey zpkKeySpec = new SecretKeySpec(zpkBytes, "DESede");

      encryptCypher = Cipher.getInstance("DESede/CBC/NoPadding");
      decryptCypher = Cipher.getInstance("DESede/CBC/NoPadding");

      encryptCypher.init(Cipher.ENCRYPT_MODE, zpkKeySpec, iv);
      decryptCypher.init(Cipher.DECRYPT_MODE, zpkKeySpec, iv);
   }

   public static String getEncryptedData(String data) throws IllegalBlockSizeException, BadPaddingException
   {
      //            return data;
      String data1 = data;
      int length = data1.length();
      if (data1.length() % 8 != 0)
      {
         length = ((length / 8) + 1) * 8;
         data1 = DataFiller.getLeftSpacePadding(data1, length);
      }
      byte[] cipherText = encryptCypher.doFinal(data1.getBytes());
      return Base64.encode(HexUtil.data2hex(cipherText).getBytes());
   }

   public static String getDecryptedData(String data) throws NumberFormatException, IllegalBlockSizeException, BadPaddingException
   {
      byte[] cipherText = decryptCypher.doFinal(data.getBytes());
      return HexUtil.data2hex(cipherText);
      //      return data;
   }

   private static String getDecryptedKey(String key) throws NumberFormatException, IllegalBlockSizeException, BadPaddingException
   {
      byte[] cipherText = internalDecCipher.doFinal(HexUtil.hex2data(key));
      return HexUtil.data2hex(cipherText);
      //      return key;
   }

   public static String getFString(int length)
   {
      StringBuffer sb = new StringBuffer();
      for (int i = 0; i < length; i++)
      {
         sb.append("F");
      }
      return sb.toString();
   }

   public static String xorData(String cardNumber, String otp) throws Exception
   {
      String binary1 = Long.toBinaryString(Long.parseLong(cardNumber, 16));
      String binary2 = Long.toBinaryString(Long.parseLong(otp, 16));
      String xor = xor(binary1, binary2);
      return xor;
   }
   public static String xor(String binary1, String binary2)
   {
      if (binary1.length() < 8)
      {
         for (int n = binary1.length(); n < 8; n++)
         {
            binary1 = "0" + binary1;
         }
      }
      if (binary2.length() < 8)
      {
         System.out.println(binary2.length());
         for (int n = binary2.length(); n < 8; n++)
         {
            binary2 = "0" + binary2;
         }
      }

      String binary161 = DataFiller.getLeftZeroPadding(binary1, 64);
      String binary162 = DataFiller.getLeftZeroPadding(binary2, 64);

      char[] binary1Array = binary161.toCharArray();
      char[] binary2Array = binary162.toCharArray();

      String result = "";
      int xorResult;
      for (int i = 0; i < 64; i++)
      {
         xorResult = (binary1Array[i] ^ binary2Array[i]);
         result = result.concat(Integer.toString(xorResult));
      }

      return Long.toHexString(Long.parseLong(result, 2));
   }

   public static void main(String[] args) throws InvalidKeyException, NumberFormatException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException
   {
      PasswordHelper.initializeCipher("02b7085095a9187e954c05274b5707acfb31d77f2744aa32bcb361e337ac926cbbc39bc6ffd2e479aae031abbdf3cc69998d7eef75fc9de3f8b7e32027098f49");
      System.out.println(PasswordHelper.getEncryptedData("37FFF53"));
      System.out.println(PasswordHelper.getDecryptedData(HexUtil.data2hex(Base64.decode("YzFkN2RiYWFkNjA3ZTZlNQ=="))));
      System.out.println(PasswordHelper.getEncryptedData("password"));
   }
}
