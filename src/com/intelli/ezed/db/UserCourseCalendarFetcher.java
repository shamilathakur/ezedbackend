package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.CourseCalendarData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.GoalData;
import com.intelli.ezed.data.Response;

public class UserCourseCalendarFetcher extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public UserCourseCalendarFetcher(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      CourseCalendarData courseCalendarData = (CourseCalendarData) response.getResponseObject();
      String userId = (String) flowContext.getFromContext(EzedKeyConstants.USER_ID);
      PreparedStatement ps = null;
      ResultSet rs = null;
      ArrayList<GoalData> goalDatas = new ArrayList<GoalData>();
      GoalData singleGoalData = null;
      SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
      String dateFromDB = null;
      int goalCompleted = 0;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchUserCourseCalendar");
         ps.setString(1, userId);
         ps.setString(2, courseCalendarData.getCourseId());
         rs = ps.executeQuery();
         while (rs.next())
         {
            singleGoalData = new GoalData();
            singleGoalData.setCourseId(rs.getString(1));
            singleGoalData.setChapterId(rs.getString(2));
            singleGoalData.setContentId(rs.getString(3));
            dateFromDB = rs.getString(4);
            singleGoalData.setEndDate(sdf.parse(dateFromDB));
            goalCompleted = rs.getInt(5);
            singleGoalData.setGoalComplete(goalCompleted);
            singleGoalData.setContentname(rs.getString(6));
            goalDatas.add(singleGoalData);
         }
         courseCalendarData.setGoalDatas(goalDatas);
      }
      catch (SQLException e)
      {
         logger.error("DB error while fetching course calendar", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching user goals");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (ParseException e)
      {
         logger.error("Error while fetching course calendar", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching user goals");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching course calendar", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching user goals");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing result set while fetching calendar for course", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchUserCourseCalendarCoursetest");
         ps.setString(1, userId);
         ps.setString(2, courseCalendarData.getCourseId());
         rs = ps.executeQuery();
         while (rs.next())
         {
            singleGoalData = new GoalData();
            singleGoalData.setCourseId(rs.getString(1));
            singleGoalData.setChapterId(rs.getString(2));
            singleGoalData.setContentId(rs.getString(3));
            dateFromDB = rs.getString(4);
            singleGoalData.setEndDate(sdf.parse(dateFromDB));
            goalCompleted = rs.getInt(5);
            singleGoalData.setGoalComplete(goalCompleted);
            singleGoalData.setContentname(rs.getString(6));
            courseCalendarData.getGoalDatas().add((singleGoalData));
         }
      }
      catch (SQLException e)
      {
         logger.error("DB error while fetching course calendar", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching user goals");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (ParseException e)
      {
         logger.error("Error while fetching course calendar", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching user goals");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching course calendar", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching user goals");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing result set while fetching calendar for course", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchUserCourseFailedGoals");
         ps.setString(1, userId);
         ps.setString(2, courseCalendarData.getCourseId());
         rs = ps.executeQuery();
         while (rs.next())
         {
            singleGoalData = new GoalData();
            singleGoalData.setCourseId(rs.getString("courseid"));
            singleGoalData.setChapterId(rs.getString("chapterid"));
            singleGoalData.setContentId(rs.getString("contentid"));
            dateFromDB = rs.getString("endtime");
            singleGoalData.setEndDate(sdf.parse(dateFromDB));
            singleGoalData.setContentname(rs.getString("contentname"));
            singleGoalData.setGoalFailedShadow(true);
            courseCalendarData.getGoalDatas().add(singleGoalData);
         }
      }
      catch (SQLException e)
      {
         logger.error("DB error while fetching course calendar", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching user goals");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (ParseException e)
      {
         logger.error("Error while fetching course calendar", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching user goals");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching course calendar", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching user goals");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing result set while fetching calendar for course", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchUserCourseFailedGoalsCourseTest");
         ps.setString(1, userId);
         ps.setString(2, courseCalendarData.getCourseId());
         rs = ps.executeQuery();
         while (rs.next())
         {
            singleGoalData = new GoalData();
            singleGoalData.setCourseId(rs.getString("courseid"));
            singleGoalData.setChapterId(rs.getString("chapterid"));
            singleGoalData.setContentId(rs.getString("contentid"));
            dateFromDB = rs.getString("endtime");
            singleGoalData.setEndDate(sdf.parse(dateFromDB));
            singleGoalData.setContentname(rs.getString("testname"));
            singleGoalData.setGoalFailedShadow(true);
            courseCalendarData.getGoalDatas().add(singleGoalData);
         }
         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
         return RuleDecisionKey.ON_SUCCESS;
      }
      catch (SQLException e)
      {
         logger.error("DB error while fetching course calendar", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching user goals");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (ParseException e)
      {
         logger.error("Error while fetching course calendar", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching user goals");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching course calendar", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching user goals");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing result set while fetching calendar for course", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
}
