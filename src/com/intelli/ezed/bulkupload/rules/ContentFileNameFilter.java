package com.intelli.ezed.bulkupload.rules;

import java.io.File;
import java.io.FileFilter;

public class ContentFileNameFilter implements FileFilter
   {
      @Override
      public boolean accept(File pathname)
      {
         String fileName = pathname.getName();
         if (!pathname.isFile())
         {
            return false;
         }
         if (fileName.endsWith(".html") || fileName.endsWith(".HTML") || fileName.endsWith(".htm") || fileName.endsWith(".HTM") || fileName.endsWith(".jpg") || fileName.endsWith(".JPG"))
         {
            return true;
         }
         return false;
      }
   }