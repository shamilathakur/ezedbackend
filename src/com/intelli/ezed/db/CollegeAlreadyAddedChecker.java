package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.CollegeData;

public class CollegeAlreadyAddedChecker extends SingletonRule {
	 private String dbConnName;
	   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

	   public CollegeAlreadyAddedChecker(String ruleName, String dbConnName)
	   {
	      super(ruleName);
	      this.dbConnName = dbConnName;
	   }

	   @Override
	   public RuleDecisionKey executeRule(FlowContext flowContext)
	   {
	      CollegeData collegeData = (CollegeData) flowContext.getFromContext(EzedKeyConstants.COLLEGE_DATA);
	      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
	     
	      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
	      PreparedStatement ps = null;
	      ResultSet rs = null;

	      try
	      {
	         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "GetAddedCollege");
	         ps.setString(1, collegeData.getUnivName());
	         ps.setString(2, collegeData.getCollege());
	         rs = ps.executeQuery();
	         if (rs.next())
	         {
	            if (logger.isDebugEnabled())
	            {
	               logger.debug( collegeData.getUnivName() +" "+ collegeData.getCollege()+ " has already been added ");
	            }
	            response.setParameters(EzedTransactionCodes.COLLEGE_ALREADY_ADDED, "College already added");
	            return RuleDecisionKey.ON_FAILURE;
	         }
	         return RuleDecisionKey.ON_SUCCESS;
	      }
	      catch (SQLException e)
	      {
	         logger.error("Error while checking if college is already added", e);
	         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while checking if college is already added");
	         return RuleDecisionKey.ON_FAILURE;
	      }
	      catch (Exception e)
	      {
	    	  logger.error("Error while checking if college is already added", e);
		         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while checking if college is already added");
		         return RuleDecisionKey.ON_FAILURE;
	      }
	      finally
	      {
	         if (rs != null)
	         {
	            try
	            {
	               rs.close();
	            }
	            catch (SQLException e)
	            {
	               logger.error("Error while closing result set while checking if college is already added", e);
	            }
	         }
	         DbConnectionManager.releaseConnection(dbConnName, conn);
	      }
	   }

	   
}
