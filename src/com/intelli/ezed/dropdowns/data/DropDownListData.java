package com.intelli.ezed.dropdowns.data;

import java.util.ArrayList;

import org.json.JSONArray;

import com.intelli.ezed.data.ResponseObject;

public class DropDownListData extends ResponseObject
{
   private String name;
   private ArrayList<SingleDropDownData> dropDownDatas = new ArrayList<SingleDropDownData>();

   public String getName()
   {
      return name;
   }

   public void setName(String name)
   {
      this.name = name;
   }

   public ArrayList<SingleDropDownData> getDropDownDatas()
   {
      return dropDownDatas;
   }

   public void setDropDownDatas(ArrayList<SingleDropDownData> dropDownDatas)
   {
      this.dropDownDatas = dropDownDatas;
   }

   @Override
   public void prepareResponse() throws Exception
   {
      JSONArray jsonArray = new JSONArray();
      for (SingleDropDownData singleDropDownData : getDropDownDatas())
      {
         singleDropDownData.prepareResponse();
         jsonArray.put(singleDropDownData);
      }
      this.put(getName(), jsonArray);
   }

}
