package com.intelli.ezed.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.slf4j.Logger;

import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.GoalData;

public class EzedHelper {

	public static long getTimeInMillisec(String time) throws Exception {
		// time will come in format hh:mm
		// String[] timeParts = StringSplitter.splitString(time, ":");

		// int hrs = Integer.parseInt(timeParts[0]);
		// int min = Integer.parseInt(timeParts[1]);

		int hrs = Integer.parseInt(time.substring(0, 2));
		int min = Integer.parseInt(time.substring(2, 4));

		if (hrs == 0 && min == 0) {
			min = 1;
		}
		long millisec = hrs * 60 * 60000;
		millisec = millisec + (min * 60000);

		return millisec;
	}

	public long getFactorial(long no) {
		if (no == 1) {
			return 1;
		} else {
			return no * getFactorial(no - 1);
		}
	}

	public static boolean isNumeric(String string) {
		try {
			Long.parseLong(string);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static String getTenDigMobNo(String string) {
		String mobNumber = string.trim();
		if (mobNumber.length() < 10) {
			return null;
		}

		if (mobNumber.startsWith("+91")) {
			if (mobNumber.length() != 13) {
				return null;
			}
			mobNumber = mobNumber.substring(3);
		} else if (mobNumber.startsWith("0")) {
			if (mobNumber.length() != 11) {
				return null;
			}
			mobNumber = mobNumber.substring(1);
		} else if (mobNumber.startsWith("91")) {
			if (mobNumber.length() == 10) {

			} else {
				if (mobNumber.length() != 12) {
					return null;
				} else if (mobNumber.length() == 12) {
					mobNumber = mobNumber.substring(2);
				}
			}
		}
		if (!isNumeric(mobNumber)) {
			return null;
		}
		return mobNumber;
	}

	public static String fetchCourseNameForCourseId(String dbConnName,
			String courseId, Logger logger) {
		Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = DbConnectionManager.getPreparedStatement(dbConnName, conn,
					"GetCourseNameForId");
			ps.setString(1, courseId);
			rs = ps.executeQuery();
			if (rs.next()) {
				String courseName = rs.getString(1);
				if (courseName != null && courseName.trim().compareTo("") == 0) {
					courseName = null;
				}
				return courseName;
			}
			if (logger.isDebugEnabled()) {
				logger.debug("No coursename found against courseid " + courseId);
			}
			return null;
		} catch (Exception e) {
			if (logger.isDebugEnabled()) {
				logger.debug("Error while fetching coursename from courseid "
						+ courseId, e);
			}
			return null;
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					logger.error("Error closing resultset whule fetching coursename against courseid");
				}
			}
			DbConnectionManager.releaseConnection(dbConnName, conn);
		}
	}

	public static String fetchChapterNameForChapterId(String dbConnName,
			String chapterId, Logger logger) {
		Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = DbConnectionManager.getPreparedStatement(dbConnName, conn,
					"GetChapterNameForId");
			ps.setString(1, chapterId);
			rs = ps.executeQuery();
			if (rs.next()) {
				String chapterName = rs.getString(1);
				if (chapterName != null
						&& chapterName.trim().compareTo("") == 0) {
					chapterName = null;
				}
				return chapterName;
			}
			if (logger.isDebugEnabled()) {
				logger.debug("No chaptername found against chapterid "
						+ chapterId);
			}
			return null;
		} catch (Exception e) {
			if (logger.isDebugEnabled()) {
				logger.debug("Error while fetching chaptername from chapterid "
						+ chapterId, e);
			}
			return null;
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					logger.error("Error closing resultset whule fetching chaptername against chapterid");
				}
			}
			DbConnectionManager.releaseConnection(dbConnName, conn);
		}
	}

	public static String fetchContentNameForContentId(String dbConnName,
			String contentId, Logger logger) {
		Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = DbConnectionManager.getPreparedStatement(dbConnName, conn,
					"GetContentNameForId");
			ps.setString(1, contentId);
			rs = ps.executeQuery();
			if (rs.next()) {
				String contentName = rs.getString(1);
				if (contentName != null
						&& contentName.trim().compareTo("") == 0) {
					contentName = null;
				}
				return contentName;
			}
			if (logger.isDebugEnabled()) {
				logger.debug("No contentname found against contentid "
						+ contentId);
			}
			return null;
		} catch (Exception e) {
			if (logger.isDebugEnabled()) {
				logger.debug("Error while fetching contentname from contentid "
						+ contentId, e);
			}
			return null;
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					logger.error("Error closing resultset whule fetching contentname against contentid");
				}
			}
			DbConnectionManager.releaseConnection(dbConnName, conn);
		}
	}

	public static String fetchTestNameForTestId(String dbConnName,
			String testId, Logger logger) {
		Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = DbConnectionManager.getPreparedStatement(dbConnName, conn,
					"GetTestNameForId");
			ps.setString(1, testId);
			rs = ps.executeQuery();
			if (rs.next()) {
				String testName = rs.getString("testname");
				if (testName != null && testName.trim().compareTo("") == 0) {
					testName = null;
				}
				return testName;
			}
			if (logger.isDebugEnabled()) {
				logger.debug("No testname found against testid " + testId);
			}
			return null;
		} catch (Exception e) {
			if (logger.isDebugEnabled()) {
				logger.debug("Error while fetching testname from testid "
						+ testId, e);
			}
			return null;
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					logger.error(
							"Error closing resultset while fetching testname against testid",
							e);
				}
			}
			DbConnectionManager.releaseConnection(dbConnName, conn);
		}
	}

	public static float calculateEndDateViaDefHours(float defMinutes,
			ArrayList<GoalData> goalDatas, Date startDate, Date endDate) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(startDate);

		Calendar endCalendar = Calendar.getInstance();
		endCalendar.setTime(endDate);

		if (calendar.after(endCalendar)) {
			// start date is after end date i.e. end date is so close that with
			// buffer, it goes before start date
			return 0;
		}

		int totalMinutes = getTotalMinutes(goalDatas);
		float totalDays = totalMinutes / defMinutes;
		int totalDaysInt = (int) totalDays;
		if (totalDaysInt < totalDays) {
			totalDaysInt++;
		}
		calendar.add(Calendar.DATE, totalDaysInt);

		if (calendar.after(endCalendar)) {
			final long DAY_IN_MILLIS = 1000 * 60 * 60 * 24;

			int diffInDays = (int) ((endDate.getTime() - startDate.getTime()) / DAY_IN_MILLIS);
			float newDefMinutes = (float) totalMinutes / (diffInDays + 1);
			return newDefMinutes;
		}
		return defMinutes;
	}

	public static int getTotalMinutes(ArrayList<GoalData> goalDatas) {
		Iterator<GoalData> goalsIte = goalDatas.iterator();
		int totalHours = 0;
		GoalData singleGoalData = null;
		while (goalsIte.hasNext()) {
			singleGoalData = goalsIte.next();
			totalHours += singleGoalData.getHours();
		}
		return totalHours;
	}

	// public static String handleBulkUploadStringUpperCase(String string, int
	// length)
	// {
	// if (string == null)
	// {
	// return null;
	// }
	// if (string.length() > length)
	// {
	// return string.substring(0, length).toUpperCase();
	// }
	// return string.toUpperCase();
	// }

	public static String handleBulkUploadStringUpperCase(String string,
			int length) {
		if (string == null) {
			return null;
		}
		if (string.length() > length) {
			return string.substring(0, length);
		}
		return string;
	}

	public static String handleBulkUploadString(String string, int length) {
		if (string == null) {
			return null;
		}
		if (string.length() > length) {
			return string.substring(0, length);
		}
		return string;
	}

	public static float[] calculatePercentage(float[] input, float total) {
		if (total == 0) {
			for (float temp : input) {
				total = total + temp;
			}
		}
		for (int i = 0; i < input.length; i++) {
			float temp = input[i];
			input[i] = (temp / total) * 100;
		}

		return input;
	}

	public static void main(String[] args) {
		float[] f = { (float) 2.3, (float) 1.3, 14, (float) 1.44 };

		EzedHelper.calculatePercentage(f, 500);

		for (int i = 0; i < f.length; i++) {
			System.out.println(f[i]);
		}

		String a = "204000";
		System.out.println(a.substring(0, 2));
		System.out.println(a.substring(2, 4));
	}

	public static String getDayOfMonthSuffix(int n) throws Exception {
		if (n < 1 || n > 31) {
			throw new Exception("Invalid day of month");
		}
		if (n >= 11 && n <= 13) {
			return "th";
		}
		switch (n % 10) {
		case 1:
			return "st";
		case 2:
			return "nd";
		case 3:
			return "rd";
		default:
			return "th";
		}
	}

	public static String fetchCourseName(String dbConnName, String courseId,
			Logger logger) {
		Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = DbConnectionManager.getPreparedStatement(dbConnName, conn,
					"GetCourseNameForCourseId");
			ps.setString(1, courseId);
			rs = ps.executeQuery();
			if (rs.next()) {
				String contentName = rs.getString(1);
				if (contentName != null
						&& contentName.trim().compareTo("") == 0) {
					contentName = null;
				}
				return contentName;
			}
			if (logger.isDebugEnabled()) {
				logger.debug("No course name found against courseid "
						+ courseId);
			}
			return null;
		} catch (Exception e) {
			if (logger.isDebugEnabled()) {
				logger.debug("Error while fetching course name from courseid "
						+ courseId, e);
			}
			return null;
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					logger.error(
							"Error closing resultset whule fetching course name against course id :"
									+ courseId, e);
				}
			}
			DbConnectionManager.releaseConnection(dbConnName, conn);
		}
	}

	public static String getDateMonthYear() {
		int year = Calendar.getInstance().get(Calendar.YEAR);
		int monthInNumeric = Calendar.getInstance().get(Calendar.MONTH);
		int dateNumeric = Calendar.getInstance().get(Calendar.DATE);
		monthInNumeric = monthInNumeric + 1;
		String yearMonth = String.valueOf(year);

		String monthInNumeric1 = "";
		if (monthInNumeric <= 9) {
			monthInNumeric1 = "0" + String.valueOf(monthInNumeric);
		} else {
			monthInNumeric1 = String.valueOf(monthInNumeric);
		}
		yearMonth = yearMonth + String.valueOf(monthInNumeric1);

		String dateNumeric1 = "";
		if (dateNumeric <= 9) {
			dateNumeric1 = "0" + String.valueOf(dateNumeric);
		} else {
			dateNumeric1 = String.valueOf(dateNumeric);
		}
		String yearMonthDate = yearMonth + dateNumeric1;

		return yearMonthDate;
	}

	public static String getMonthYear() {
		int year = Calendar.getInstance().get(Calendar.YEAR);
		int monthInNumeric = Calendar.getInstance().get(Calendar.MONTH);
		monthInNumeric = monthInNumeric + 1;
		String yearMonth = String.valueOf(year);

		String monthInNumeric1 = "";
		if (monthInNumeric <= 9) {
			monthInNumeric1 = "0" + String.valueOf(monthInNumeric);
		} else {
			monthInNumeric1 = String.valueOf(monthInNumeric);
		}
		yearMonth = yearMonth + String.valueOf(monthInNumeric1);

		return yearMonth;
	}

	public static String fetchChapterName(String dbConnName, String chapterId,
			Logger logger) {
		Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = DbConnectionManager.getPreparedStatement(dbConnName, conn,
					"GetChapterNameForChapterid");
			ps.setString(1, chapterId);
			rs = ps.executeQuery();
			if (rs.next()) {
				String contentName = rs.getString(1);
				if (contentName != null
						&& contentName.trim().compareTo("") == 0) {
					contentName = null;
				}
				return contentName;
			}
			if (logger.isDebugEnabled()) {
				logger.debug("No course name found against courseid "
						+ chapterId);
			}
			return null;
		} catch (Exception e) {
			if (logger.isDebugEnabled()) {
				logger.debug("Error while fetching course name from courseid "
						+ chapterId, e);
			}
			return null;
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					logger.error(
							"Error closing resultset whule fetching course name against course id :"
									+ chapterId, e);
				}
			}
			DbConnectionManager.releaseConnection(dbConnName, conn);
		}
	}

	/** Added by kunal on 28/02/2015 */
	public static String getPaymentDetailId(String dbConnName, Logger logger) {
		String paymentdetailid = null;
		Connection connection = DbConnectionManager
				.getConnectionByName(dbConnName);

		PreparedStatement ps1 = null;
		ResultSet rs1;
		ps1 = DbConnectionManager.getPreparedStatement(dbConnName, connection,
				"MaxPaymentDetailId");
		try {
			rs1 = ps1.executeQuery();

			// System.out.println("Max Paymentdetailid returned : " +
			// paymentdetailid);
			if (!rs1.next()) {
				// create new paymentdetailid
			} else {
				paymentdetailid = rs1.getString("id");
				System.out.println("Max Paymentdetailid returned : "
						+ paymentdetailid);
				int temp = Integer.parseInt(paymentdetailid.substring(4));
				System.out.println("Temp : " + temp);
				temp = temp + 1;
				paymentdetailid = "PAY1" + temp + "";
			}
			System.out
					.println("Final Max paymentdetailid : " + paymentdetailid);
		} catch (SQLException e1) {
			e1.printStackTrace();
		} catch (Exception e) {
			System.out.println("Something went wrong ... !!!");
		} finally {
			DbConnectionManager.releaseConnection(dbConnName, connection);
		}

		return paymentdetailid;
	}

	public static boolean checkIfCourseAlreadyPurchased(String dbConnName,
			Logger logger, String userid, String courseId) {
		boolean ownedStatus = false;
		Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			ps = DbConnectionManager.getPreparedStatement(dbConnName, conn,
					"CheckIfCourseOwned");
			ps.setString(1, courseId);
			ps.setString(2, userid);
			rs = ps.executeQuery();
			if (rs.next()) {
				logger.info("Course is already owned. Process stopped. ");
				ownedStatus = true;
			}

		} catch (SQLException e) {

			if (logger.isDebugEnabled()) {
				System.out
						.println("SQLException while checking owned courses !"
								+ e);
				logger.debug(
						"SQL Exception occured while checking the owned course.",
						e);
			}
		} catch (Exception e) {
			if (logger.isDebugEnabled()) {
				logger.debug(
						"Exception occured while checking the owned course.", e);
			}
			System.out
					.println("Some other Exception occured while checking owned courses !"
							+ e);
		} finally {
			DbConnectionManager.releaseConnection(dbConnName, conn);
		}
		return ownedStatus;
	}
}
