package com.intelli.ezed.data;

public class PostData extends ResponseObject{
	
	private String threadId;
	private String userId;
	private String postId;
	private String postData;
	private String createTime;
	private String imagePath;
	private int abuseCount;
	private String parentPost;
	private int likeCount;
	private String profleImage;
	private int adminFlag;
	private String status;
	private String firstname;
	private String lastname;
	
	
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getParentPost() {
		return parentPost;
	}
	public void setParentPost(String parentPost) {
		this.parentPost = parentPost;
	}
	public String getThreadId() {
		return threadId;
	}
	public void setThreadId(String threadId) {
		this.threadId = threadId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPostId() {
		return postId;
	}
	public void setPostId(String postId) {
		this.postId = postId;
	}
	public String getPostData() {
		return postData;
	}
	public void setPostData(String postData) {
		this.postData = postData;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getImagePath() {
		return imagePath;
	}
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	public int getAbuseCount() {
		return abuseCount;
	}
	public void setAbuseCount(int abuseCount) {
		this.abuseCount = abuseCount;
	}
	public int getLikeCount() {
		return likeCount;
	}
	public void setLikeCount(int likeCount) {
		this.likeCount = likeCount;
	}
	public String getProfleImage() {
		return profleImage;
	}
	public void setProfleImage(String profleImage) {
		this.profleImage = profleImage;
	}
	public int getAdminFlag() {
		return adminFlag;
	}
	public void setAdminFlag(int adminFlag) {
		this.adminFlag = adminFlag;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Override
	public void prepareResponse() throws Exception {
		this.put("threadid", getThreadId());
		this.put("userId", getUserId());
		this.put("status",getStatus());
		this.put("postId",getPostId());
		this.put("postData",getPostData());
		this.put("createTime",getCreateTime());
		this.put("imagePath",getImagePath());
		this.put("abuseCount",getAbuseCount());
		this.put("parentPost",getParentPost());
		this.put("likeCount",getLikeCount());
		this.put("profleImage",getProfleImage());
		this.put("adminFlag",getAdminFlag());
		this.put("firstname",getFirstname());
		this.put("lastname",getLastname());
	}
	
	
}
