package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;

public class NewUserPointsInserter extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private Logger pointsLogger = LoggerFactory.getLogger(EzedLoggerName.POINTS_ERROR_LOG);

   public NewUserPointsInserter(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      String userid = (String) flowContext.getFromContext(EzedKeyConstants.USER_ID);
      String inTime = (String) flowContext.getFromContext(EzedKeyConstants.REQUEST_IN_TIME);
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;

      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertVerifiedUsers");
         ps.setString(1, userid);
         ps.setString(2, inTime);
         int rows = ps.executeUpdate();
         if (rows == 0)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Userid could not be registered for point entry making " + userid);
            }
            pointsLogger.info(userid + " " + inTime);
            return RuleDecisionKey.ON_FAILURE;
         }
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in db so that point entries can be made for user after verif");
         }
         return RuleDecisionKey.ON_SUCCESS;
      }
      catch (Exception e)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Userid could not be registered for point entry making " + userid, e);
         }
         pointsLogger.info(userid + " " + inTime);
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
}
