package com.intelli.ezed.analytics.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.slf4j.Logger;

import com.intelli.ezed.data.ResponseObject;

public class AreaWiseRegReportData extends ResponseObject
{

   private String userId;
   private String courseId;
   private String courseName;

   private int totalUsersRegistered = 0;
   private int yesterdayUsersRegistered = 0;
   private int dayBeforeYesterdayUsersRegistered = 0;
   private int lastWeekUsersRegistered = 0;
   private int lastFortnightUsersRegistered = 0;
   private int lastMonthUsersRegistered = 0;
   private int lastTwoMonthUsersRegistered = 0;
   private int lastSixMonthUsersRegistered = 0;
   private int lastYearUsersRegistered = 0;

   private int totalUsersEnrolled = 0;
   private int yesterdayUsersEnrolled = 0;
   private int dayBeforeYesterdayUsersEnrolled = 0;
   private int lastWeekUsersEnrolled = 0;
   private int lastFortnightUsersEnrolled = 0;
   private int lastMonthUsersEnrolled = 0;
   private int lastTwoMonthUsersEnrolled = 0;
   private int lastSixMonthUsersEnrolled = 0;
   private int lastYearUsersEnrolled = 0;


   public String getUserId()
   {
      return userId;
   }

   public int getTotalUsersRegistered()
   {
      return totalUsersRegistered;
   }
   public void setTotalUsersRegistered(int totalUsersRegistered)
   {
      this.totalUsersRegistered = totalUsersRegistered;
   }


   public int getYesterdayUsersRegistered()
   {
      return yesterdayUsersRegistered;
   }


   public void setYesterdayUsersRegistered(int yesterdayUsersRegistered)
   {
      this.yesterdayUsersRegistered = yesterdayUsersRegistered;
   }


   public int getLastWeekUsersRegistered()
   {
      return lastWeekUsersRegistered;
   }

   public void setLastWeekUsersRegistered(int lastWeekUsersRegistered)
   {
      this.lastWeekUsersRegistered = lastWeekUsersRegistered;
   }

   public int getLastMonthUsersRegistered()
   {
      return lastMonthUsersRegistered;
   }

   public void setLastMonthUsersRegistered(int lastMonthUsersRegistered)
   {
      this.lastMonthUsersRegistered = lastMonthUsersRegistered;
   }

   public int getLastYearUsersRegistered()
   {
      return lastYearUsersRegistered;
   }

   public void setLastYearUsersRegistered(int lastYearUsersRegistered)
   {
      this.lastYearUsersRegistered = lastYearUsersRegistered;
   }

   public int getTotalUsersEnrolled()
   {
      return totalUsersEnrolled;
   }


   public void setTotalUsersEnrolled(int totalUsersEnrolled)
   {
      this.totalUsersEnrolled = totalUsersEnrolled;
   }

   public int getYesterdayUsersEnrolled()
   {
      return yesterdayUsersEnrolled;
   }

   public void setYesterdayUsersEnrolled(int yesterdayUsersEnrolled)
   {
      this.yesterdayUsersEnrolled = yesterdayUsersEnrolled;
   }

   public int getLastWeekUsersEnrolled()
   {
      return lastWeekUsersEnrolled;
   }

   public void setLastWeekUsersEnrolled(int lastWeekUsersEnrolled)
   {
      this.lastWeekUsersEnrolled = lastWeekUsersEnrolled;
   }

   public int getLastMonthUsersEnrolled()
   {
      return lastMonthUsersEnrolled;
   }

   public void setLastMonthUsersEnrolled(int lastMonthUsersEnrolled)
   {
      this.lastMonthUsersEnrolled = lastMonthUsersEnrolled;
   }

   public int getLastYearUsersEnrolled()
   {
      return lastYearUsersEnrolled;
   }

   public void setLastYearUsersEnrolled(int lastYearUsersEnrolled)
   {
      this.lastYearUsersEnrolled = lastYearUsersEnrolled;
   }

   public void setUserId(String userId)
   {
      this.userId = userId;
   }

   public String getCourseId()
   {
      return courseId;
   }

   public void setCourseId(String courseId)
   {
      this.courseId = courseId;
   }

   public String getCourseName()
   {
      return courseName;
   }

   public void setCourseName(String courseName)
   {
      this.courseName = courseName;
   }


   public void calculateUserRegCounts(String dbConnName, Logger logger) throws SQLException, Exception
   {
      this.fetchAllHistoryPointsForUserCourse(dbConnName, logger);
   }

   public void calculateUserRegCounts(String dbConnName, Logger logger, String userid) throws SQLException, Exception
   {
      this.fetchAllHistoryPointsForUserCourse(dbConnName, logger, userid);
   }

   private void fetchAllHistoryPointsForUserCourse(String dbConnName, Logger logger, String userid) throws SQLException, Exception
   {
      setTotalUsersRegistered(fetchChkflagFromUserTable(dbConnName, "GetAreaUserCountFromUser", logger, userid));
      setYesterdayUsersRegistered(fetchChkflagFromUserTable(dbConnName, "GetAreaUserCountFromYesterdayUser", logger, userid));
      setDayBeforeYesterdayUsersRegistered(fetchChkflagFromUserTable(dbConnName, "GetAreaUserCountFromDayBeforeYesterdayUser", logger, userid));
      setLastWeekUsersRegistered(fetchChkflagFromUserTable(dbConnName, "GetAreaUserCountFromLastWeekUser", logger, userid));
      setLastFortnightUsersRegistered(fetchChkflagFromUserTable(dbConnName, "GetAreaUserCountFromLastFortnightUser", logger, userid));
      setLastMonthUsersRegistered(fetchChkflagFromUserTable(dbConnName, "GetAreaUserCountFromLastMonthUser", logger, userid));
      setLastTwoMonthUsersRegistered(fetchChkflagFromUserTable(dbConnName, "GetAreaUserCountFromLastTwoMonthUser", logger, userid));
      setLastSixMonthUsersRegistered(fetchChkflagFromUserTable(dbConnName, "GetAreaUserCountFromLastSixMonthUser", logger, userid));
      setLastYearUsersRegistered(fetchChkflagFromUserTable(dbConnName, "GetAreaUserCountFromLastYearUser", logger, userid));
   }

   private void fetchAllHistoryPointsForUserCourse(String dbConnName, Logger logger) throws SQLException, Exception
   {
      setTotalUsersRegistered(fetchChkflagFromUserTable(dbConnName, "GetUserCountFromUsers", logger));
      setYesterdayUsersRegistered(fetchChkflagFromUserTable(dbConnName, "GetUserCountFromYesterdayUsers", logger));
      setDayBeforeYesterdayUsersRegistered(fetchChkflagFromUserTable(dbConnName, "GetUserCountFromDayBeforeYesterdayUsers", logger));
      setLastWeekUsersRegistered(fetchChkflagFromUserTable(dbConnName, "GetUserCountFromLastWeekUsers", logger));
      setLastTwoMonthUsersRegistered(fetchChkflagFromUserTable(dbConnName, "GetUserCountFromLastFortnightUsers", logger));
      setLastMonthUsersRegistered(fetchChkflagFromUserTable(dbConnName, "GetUserCountFromLastMonthUsers", logger));
      setLastTwoMonthUsersRegistered(fetchChkflagFromUserTable(dbConnName, "GetUserCountFromLastTwoMonthUsers", logger));
      setLastSixMonthUsersRegistered(fetchChkflagFromUserTable(dbConnName, "GetUserCountFromLastSixMonthUsers", logger));
      setLastYearUsersRegistered(fetchChkflagFromUserTable(dbConnName, "GetUserCountFromLastYearUsers", logger));
   }

   private int fetchChkflagFromUserTable(String dbConnName, String prepStmtName, Logger logger) throws SQLException, Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, prepStmtName);
         rs = ps.executeQuery();
         if (rs.next())
         {
            int count = rs.getInt("count");
            return count;
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("No entry found for user in history table for prep stmt name " + prepStmtName);
            }
            return 0;
         }

      }
      catch (SQLException e)
      {
         logger.error("Error while fetching points from one of users history tables for prep stmt name " + prepStmtName, e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching points from one of users history tables for prep stmt name " + prepStmtName, e);
         throw e;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
               logger.error("Error while closing result set while fetching points from one of users table");
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   private int fetchChkflagFromUserTable(String dbConnName, String prepStmtName, Logger logger, String userid) throws SQLException, Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, prepStmtName);
         ps.setString(1, userid);
         rs = ps.executeQuery();
         if (rs.next())
         {
            int count = rs.getInt("count");
            return count;
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("No entry found for user in history table for prep stmt name " + prepStmtName);
            }
            return 0;
         }

      }
      catch (SQLException e)
      {
         logger.error("Error while fetching points from one of users history tables for prep stmt name " + prepStmtName, e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching points from one of users history tables for prep stmt name " + prepStmtName, e);
         throw e;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
               logger.error("Error while closing result set while fetching points from one of users table");
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

   }

   @Override
   public void prepareResponse() throws Exception
   {
   }

   public int getDayBeforeYesterdayUsersRegistered()
   {
      return dayBeforeYesterdayUsersRegistered;
   }

   public void setDayBeforeYesterdayUsersRegistered(int dayBeforeYesterdayUsersRegistered)
   {
      this.dayBeforeYesterdayUsersRegistered = dayBeforeYesterdayUsersRegistered;
   }

   public int getLastFortnightUsersRegistered()
   {
      return lastFortnightUsersRegistered;
   }

   public void setLastFortnightUsersRegistered(int lastFortnightUsersRegistered)
   {
      this.lastFortnightUsersRegistered = lastFortnightUsersRegistered;
   }

   public int getLastTwoMonthUsersRegistered()
   {
      return lastTwoMonthUsersRegistered;
   }

   public void setLastTwoMonthUsersRegistered(int lastTwoMonthUsersRegistered)
   {
      this.lastTwoMonthUsersRegistered = lastTwoMonthUsersRegistered;
   }

   public int getLastSixMonthUsersRegistered()
   {
      return lastSixMonthUsersRegistered;
   }

   public void setLastSixMonthUsersRegistered(int lastSixMonthUsersRegistered)
   {
      this.lastSixMonthUsersRegistered = lastSixMonthUsersRegistered;
   }

   public int getDayBeforeYesterdayUsersEnrolled()
   {
      return dayBeforeYesterdayUsersEnrolled;
   }

   public void setDayBeforeYesterdayUsersEnrolled(int dayBeforeYesterdayUsersEnrolled)
   {
      this.dayBeforeYesterdayUsersEnrolled = dayBeforeYesterdayUsersEnrolled;
   }

   public int getLastFortnightUsersEnrolled()
   {
      return lastFortnightUsersEnrolled;
   }

   public void setLastFortnightUsersEnrolled(int lastFortnightUsersEnrolled)
   {
      this.lastFortnightUsersEnrolled = lastFortnightUsersEnrolled;
   }

   public int getLastTwoMonthUsersEnrolled()
   {
      return lastTwoMonthUsersEnrolled;
   }

   public void setLastTwoMonthUsersEnrolled(int lastTwoMonthUsersEnrolled)
   {
      this.lastTwoMonthUsersEnrolled = lastTwoMonthUsersEnrolled;
   }

   public int getLastSixMonthUsersEnrolled()
   {
      return lastSixMonthUsersEnrolled;
   }

   public void setLastSixMonthUsersEnrolled(int lastSixMonthUsersEnrolled)
   {
      this.lastSixMonthUsersEnrolled = lastSixMonthUsersEnrolled;
   }

   public String fetchCityName(String dbConnName, Logger logger, String cityId) throws SQLException, Exception
   {
      String cityName = null;
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "SelectCityNameFromID");
         ps.setString(1, cityId);
         rs = ps.executeQuery();
         if (rs.next())
         {
            cityName = rs.getString("cityname");
            if (cityName == null)
            {
               cityName = "City";
            }
         }
         else
         {
            cityName = "city";
         }
         return cityName;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {

            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
}
