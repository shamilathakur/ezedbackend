package com.intelli.ezed.bulkupload.rules;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.digester3.Digester;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.LinkedProcessorManager;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import com.intelli.ezed.bulkupload.data.CourseDetails;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;

public class CourseDetailsHandler extends SingletonRule
{
   private static Object lockObject = new Object();
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.BULK_UPLOAD_LOG);
   private String coursePropertyFileName;
   private Digester courseDigester = new Digester();

   public CourseDetailsHandler(String ruleName, String coursePropertyFileName)
   {
      super(ruleName);
      initDigester();
      this.coursePropertyFileName = coursePropertyFileName;
   }

   private void initDigester()
   {
      courseDigester.setValidating(false);
      courseDigester.addCallMethod("coursedetails/name", "setCourseName", 0);
      courseDigester.addCallMethod("coursedetails/shortdesc", "setShortDesc", 0);
      courseDigester.addCallMethod("coursedetails/longdesctype", "setLongDescType", 0, new Class<?>[]{Integer.class});
      courseDigester.addCallMethod("coursedetails/longdescstring", "setLongDescString", 0);
      courseDigester.addCallMethod("coursedetails/longdesccontent", "setLongDescContent", 0);
      courseDigester.addCallMethod("coursedetails/introvid", "setIntroVid", 0);
      courseDigester.addCallMethod("coursedetails/defchapter", "setDefChapter", 0);
      courseDigester.addCallMethod("coursedetails/lmr", "setLmr", 0);
      courseDigester.addCallMethod("coursedetails/univquestpaper", "setUnivQuestPaper", 0);
      courseDigester.addCallMethod("coursedetails/univsolution", "setUnivSolution", 0);
      courseDigester.addCallMethod("coursedetails/enddate", "setEndDate", 0);
      courseDigester.addCallMethod("coursedetails/totalcost", "setTotalCost", 0);
      courseDigester.addCallMethod("coursedetails/contentcost", "setContentCost", 0);
      courseDigester.addCallMethod("coursedetails/testcost", "setTestCost", 0);
      courseDigester.addCallMethod("coursedetails/coursepath", "setChapterPath", 0);
      courseDigester.addObjectCreate("coursedetails/test", "com.intelli.ezed.bulkupload.data.TestDetails");
      courseDigester.addSetNext("coursedetails/test", "addTestDetails", "com.intelli.ezed.bulkupload.data.TestDetails");
      courseDigester.addCallMethod("coursedetails/test/time", "setTestTime", 0);
      courseDigester.addCallMethod("coursedetails/test/noofquestions", "setNoOfQuestions", 0, new Class<?>[]{Integer.class});
      courseDigester.addCallMethod("coursedetails/test/description", "setDescription", 0);
      courseDigester.addCallMethod("coursedetails/test/name", "setTestName", 0);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      CourseDetails courseDetails = (CourseDetails) flowContext.getFromContext(EzedKeyConstants.BULK_UPLOAD_COURSEOBJECT);

      File courseDirFile = courseDetails.getCourseFile();
      String coursePropFile = courseDirFile.getAbsolutePath() + "/" + coursePropertyFileName;
      if (logger.isDebugEnabled())
      {
         logger.debug("Reading course file " + coursePropFile);
      }
      Map<Object, Object> processorMap = null;
      try
      {
         synchronized (lockObject)
         {
            courseDigester.push(courseDetails);
            courseDetails = courseDigester.parse(new File(coursePropFile));
            courseDetails.setNewCourseId();
         }
         File[] chapterFiles = courseDirFile.listFiles();
         for (File singleChapterFile : chapterFiles)
         {
            if (singleChapterFile.isDirectory())
            {
               processorMap = new HashMap<Object, Object>();
               processorMap.put(EzedKeyConstants.BULK_UPLOAD_COURSEOBJECT, courseDetails);
               processorMap.put(EzedKeyConstants.BULK_UPLOAD_CHAPTERFILE, singleChapterFile);
               processorMap.put(EzedKeyConstants.BULK_UPLOAD_CONTENTMAP, flowContext.getFromContext(EzedKeyConstants.BULK_UPLOAD_CONTENTMAP));
               LinkedProcessorManager.addTask("BulkUploadChapterHandler", processorMap);
            }
         }
         flowContext.putIntoContext(EzedKeyConstants.BULK_UPLOAD_COURSEOBJECT, courseDetails);
         flowContext.putIntoContext(EzedKeyConstants.BULK_UPLOAD_QBANK_DIR_FILE, courseDirFile);
      }
      catch (IOException e)
      {
         logger.error("Error while reading course details properties file", e);
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (SAXException e)
      {
         logger.error("Error while reading course details properties file", e);
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while processing course details after reading properly from properties file", e);
         return RuleDecisionKey.ON_FAILURE;
      }
      return RuleDecisionKey.ON_SUCCESS;
   }

}
