package com.intelli.ezed.utils.upload;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicLong;

import com.a.helper.data.DataFiller;

public class PrepareQuestionIdDuplicate
{
   //keep calling getnextid() to get new note ID's. Note ID's will be
   //in the format NOT1, NOT2, NOT3, and so on...
   private AtomicLong atomicLong = null;
   private String startString;

   public PrepareQuestionIdDuplicate(String startString)
   {
      this.startString = startString;
      long initialValue = getStartUpTransactionId();
      if (initialValue == 0)
      {
         initialValue = (System.currentTimeMillis() / 10000);
      }
      atomicLong = new AtomicLong(initialValue);
   }

   private long getStartUpTransactionId()
   {
      //change how you get your connection here.
      Connection connection = getDbConnection();
      if (connection == null)
      {
         return 0;
      }

      //change how you get your prepared statement here.
      PreparedStatement preparedStatement = getStatement(connection);
      if (preparedStatement == null)
      {
         return 0;
      }
      try
      {
         preparedStatement.setString(1, startString + "%");
         ResultSet resultSet = preparedStatement.executeQuery();
         if (resultSet.next())
         {
            String valueString = resultSet.getString(1);
            valueString = valueString.substring(startString.length());
            long value = Long.parseLong(valueString);
            resultSet.close();
            resultSet = null;

            if (value == 0)
            {
               value = 1;
               return value;
            }

            return value + 1;
         }
         return 1;
      }
      catch (Exception e)
      {
         return 0;
      }
      finally
      {
         //if there is any code to release the prepared statements
         //or connections back to pool, write them here.
         try
         {
            preparedStatement.close();
         }
         catch (SQLException e)
         {
         }
         try
         {
            connection.close();
         }
         catch (SQLException e)
         {
         }
      }
   }


   public String getNextId()
   {
      return startString + DataFiller.getLeftZeroPadding(Long.toString(this.atomicLong.incrementAndGet()), 16);
   }

   private Connection getDbConnection()
   {
      try
      {
         String url = "jdbc:postgresql://localhost:15432/ezed";
         Class.forName("org.postgresql.Driver");
         return DriverManager.getConnection(url, "postgres", "postgres");
      }
      catch (Exception e)
      {
         e.printStackTrace();
         System.out.println("Error initialising Postgres connection");
         System.exit(1);
         return null;
      }
   }

   private PreparedStatement getStatement(Connection conn)
   {
      try
      {
         return (conn.prepareStatement("select max(questionid) from questionbank where questionid like ?"));
      }
      catch (Exception e)
      {
         System.out.println("Error initializing postgres statment");
         System.exit(1);
         return null;
      }
   }
}
