package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.frontend.utils.PrepareCartId;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class CartDataAdderM extends SingletonRule
{

	public CartDataAdderM(String ruleName,  String dbConnName) {
		super(ruleName);
		 this.dbConnName = dbConnName;
	}
	private String dbConnName;
	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
	
	PrepareCartId prepareCartId = null;
	
	@Override
	public RuleDecisionKey executeRule(FlowContext  flowContext) {
		 MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
	     Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
	     
	     String courseid = messageData.getParameter("courseid");
	     if (courseid == null || courseid.trim().compareTo("") == 0)
	      {
	         if (logger.isDebugEnabled())
	         {
	            logger.debug("Course id is invalid " + courseid);
	         }
	         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid  course id");
	         return RuleDecisionKey.ON_FAILURE;
	      }
	     
	     String userid = messageData.getParameter("email");
	     if (userid == null || userid.trim().compareTo("") == 0)
	      {
	         if (logger.isDebugEnabled())
	         {
	            logger.debug("user id is invalid " + userid);
	         }
	         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid  user id");
	         return RuleDecisionKey.ON_FAILURE;
	      }
	     
	     String coursetype = messageData.getParameter("coursetype");
	     if (coursetype == null || coursetype.trim().compareTo("") == 0)
	      {
	         if (logger.isDebugEnabled())
	         {
	            logger.debug("course type is invalid " + coursetype);
	         }
	         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid  course type");
	         return RuleDecisionKey.ON_FAILURE;
	      }
	     
	     prepareCartId = PrepareCartId.getPrepareCartId();
	     String cartId =null;
	     
	     if (prepareCartId != null) {
				
	    	    cartId = prepareCartId.getCartId();
				if (cartId == null || cartId.trim().compareTo("") == 0) {
					logger.error("Generated CartId is Null !");
					 response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Error while generating Cart Id ");
			         return RuleDecisionKey.ON_FAILURE;
			      }
			} else {
				logger.error("Generated Cart Id is Null !");
				 response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Error while generating Cart Id ");
		         return RuleDecisionKey.ON_FAILURE;
			}
	     String startdate = messageData.getParameter("startdate");
		 String enddate = messageData.getParameter("enddate");
	     Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
	     PreparedStatement ps = null;
	     try {
		     ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertToCart");
	         ps.setString(1, userid);
	         ps.setString(2, courseid);
	         ps.setString(3, coursetype);
	         ps.setString(4, startdate);
		     ps.setString(5, cartId);
	         ps.setString(6, enddate);
	         int rows = ps.executeUpdate();
	         if (rows > 0)
	         {
	            if (logger.isDebugEnabled())
	            {
	               logger.debug(rows + " rows inserted in db while adding cart data to user " + userid + ". Added  for course " + courseid);
	            }
	            response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
	            return RuleDecisionKey.ON_SUCCESS;
	         }
	         else
	         {
	            if (logger.isDebugEnabled())
	            {
	               logger.debug("No rows were updated");
	            }
	            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "New notes could not be added");
	            return RuleDecisionKey.ON_FAILURE;
	         }
         
		} catch (SQLException e)
	      {
	         logger.error("Error while insering notes for user " + userid, e);
	         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while insering notes for user" + e.getMessage());
	         return RuleDecisionKey.ON_FAILURE;
	      }
	      catch (Exception e)
	      {
	    	  logger.error("Error while insering notes for user " + userid, e);
	         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while insering notes for user" + e.getMessage());
	         return RuleDecisionKey.ON_FAILURE;
	      }
	      finally
	      {
	         DbConnectionManager.releaseConnection(dbConnName, conn);
	      }


	}
}
