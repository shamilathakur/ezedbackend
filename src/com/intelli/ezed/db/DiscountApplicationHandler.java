package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.analytics.data.GenericChartData;
import com.intelli.ezed.data.DiscountCouponData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class DiscountApplicationHandler extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;

   public DiscountApplicationHandler(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());

      String totalAmountString = messageData.getParameter("totalamount");
      if (totalAmountString == null || totalAmountString.compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Total amount is invalid " + totalAmountString);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid total amount");
         return RuleDecisionKey.ON_FAILURE;
      }

      String discountCouponCode = messageData.getParameter("couponval");
      if (discountCouponCode == null || discountCouponCode.compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Discount coupon is invalid " + discountCouponCode);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid discount coupon code");
         return RuleDecisionKey.ON_FAILURE;
      }

      int totalAmount = 0;
      try
      {
         totalAmount = Integer.parseInt(totalAmountString);
      }
      catch (Exception e)
      {
         logger.error("Total amount is an invalid numnber ", e);
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid total amount");
         return RuleDecisionKey.ON_FAILURE;
      }

      DiscountCouponData discData = null;
      Connection conn = null;
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         //select * from couponlist where couponcode=?
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchDiscountDetails");
         ps.setString(1, discountCouponCode);
         rs = ps.executeQuery();
         if (rs.next())
         {
            discData = new DiscountCouponData(0, 0);
            discData.setCouponCode(discountCouponCode);
            discData.setStartTime(DiscountCouponData.sdfDb.parse(rs.getString("starttime")));
            discData.setEndTime(DiscountCouponData.sdfDb.parse(rs.getString("endtime")));
            discData.setMaxValueDiscount(rs.getInt("maxvaluediscount"));
            discData.setValueDiscount(rs.getInt("valuediscount"));
            discData.setPercentDiscount(Float.parseFloat(rs.getString("percentagediscount")));
            discData.setValidFlag(rs.getInt("validflag"));
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Discount details not found for discount code " + discountCouponCode);
            }
            response.setParameters(EzedTransactionCodes.DISCOUNT_INVALID, "No such discount exists");
            return RuleDecisionKey.ON_FAILURE;
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching discount details for coupon code " + discountCouponCode, e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching discount details");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching discount details for coupon code " + discountCouponCode, e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching discount details");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      if (!discData.isCouponValid(response))
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Discount coupon is invalid for reason " + response.getResponseMessage());
         }
         return RuleDecisionKey.ON_FAILURE;
      }

      try
      {
         GenericChartData data = discData.applyDiscountInitial(totalAmount);
         response.setResponseObject(data);
         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
      }
      catch (Exception e)
      {
         logger.error("Error while applying discount", e);
         response.setParameters(EzedTransactionCodes.DISCOUNT_INVALID, "Discount not applicable on the total amount");
         return RuleDecisionKey.ON_FAILURE;
      }
      return RuleDecisionKey.ON_SUCCESS;
   }
}
