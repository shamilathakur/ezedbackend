package com.intelli.ezed.frontend.utils;

public class EzedFrontEndTransactionCodes
{
   public static final int SUCCESS = 0;

   public static final int INVALID_MESSAGE_KEYWORD = 201;
   public static final int INVALID_MESSAGE_PARAMETER = 202;
   public static final int INVALID_USERID = 203;
   public static final int INVALID_PASSWORD = 204;
   public static final int INVALID_CHKFLAG = 205;
   public static final int USERID_NOT_REGISTERED = 206;
   public static final int PAYMENT_NOT_FOUND = 207;
   public static final int INVALID_CHAPTER_ID = 208;
   public static final int INVALID_OPRATION = 209;
   public static final int WRONG_COUNTRY = 210;
   public static final int WRONG_STATE = 211;
   public static final int WRONG_CITY = 212;
   
   public static final int TEST_END_REACHED = 401;
   public static final int INVALID_TEST_ID = 402;
   
   public static final int DB_EXCEPTION_OCCURRED = 301;
   public static final int INTERNAL_ERROR = 302;
   public static final int SESSION_ERROR_OCCURRED = 303;

   public static final int PROCESSING_REQUEST = 399;
   
   public static final int NO_CONNECTION = 101;
   public static final int NO_STATEMENT = 102;
   public static final int NO_ROWS_INSERTED = 104;
   
}
