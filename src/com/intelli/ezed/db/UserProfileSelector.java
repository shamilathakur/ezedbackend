package com.intelli.ezed.db;

import java.sql.SQLException;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.UserProfile;

public class UserProfileSelector extends SingletonRule
{

   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;

   public UserProfileSelector(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      String userid = (String) flowContext.getFromContext(EzedKeyConstants.USER_ID);
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      if (userid == null)
      {
         userid = messageData.getParameter("email");
      }

      if (logger.isDebugEnabled())
      {
         logger.debug("Getting user profile for userid : " + userid);
      }

      UserProfile userProfile = new UserProfile();
      userProfile.setUserid(userid);
      flowContext.putIntoContext(EzedKeyConstants.USER_DETAILS, userProfile);

      try
      {
         int status = userProfile.populateUserProfile(dbConnName, logger);
         if (status != EzedTransactionCodes.SUCCESS)
         {
            response.setParameters(status, "Error while fetching user profile");
            if (logger.isDebugEnabled())
            {
               logger.debug("Error while fetching user profile " + status);
            }
            return RuleDecisionKey.ON_FAILURE;
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while populating user profile from DB", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Failed to populate user profile");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while populating user profile from DB", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Failed to populate user profile");
         return RuleDecisionKey.ON_FAILURE;
      }
      flowContext.putIntoContext(
				EzedKeyConstants.PAYMENT_ID_FOR_TRANSACTION_TYPE,
				EzedKeyConstants.PAYMENT_ID_FOR_TRANSACTION_TYPE_ONLINE_PAYMENT);
      return RuleDecisionKey.ON_SUCCESS;
   }
}
