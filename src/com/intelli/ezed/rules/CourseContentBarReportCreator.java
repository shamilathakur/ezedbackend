package com.intelli.ezed.rules;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.a.helper.utils.StringSplitter;
import com.intelli.ezed.analytics.data.TestResponseOne;
import com.intelli.ezed.analytics.data.TestResposeTwo;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class CourseContentBarReportCreator extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;

   public CourseContentBarReportCreator(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = new Response();
      flowContext.putIntoContext(EzedKeyConstants.EZED_RESPONSE, response);

      String courseId = messageData.getParameter("courseid");

      boolean fetchAll = false;

      if (courseId == null || courseId.equals("") || courseId.equals(" "))
      {
         fetchAll = true;
      }

      String contentId = messageData.getParameter("contentType");

      if (contentId == null || contentId.equals("") || contentId.equals(" "))
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("No conetent id specified.");
         }
      }

      int contentType = Integer.parseInt(contentId);

      String[] courseids = StringSplitter.splitString(courseId, ",");

      Vector<TestResponseOne> testResponseOnes = new Vector<TestResponseOne>();

      int count = 0;

      for (String course : courseids)
      {
         Connection connection = DbConnectionManager.getConnectionByName(dbConnName);
         ResultSet rs = null;
         try
         {
            //select count(c.contenttype),c.contenttype,o.courseid
            //from contentlist c, learningpath l, coursepath o 
            //where o.courseid = ? and c.contenttype = ?
            //and c.contentid = l.contentid and o.chapterid = l.chapterid GROUP BY c.contenttype, o.courseid
            PreparedStatement preparedStatement = DbConnectionManager.getPreparedStatement(dbConnName, connection, "GetCourseContentCount");
            preparedStatement.setString(1, course);
            preparedStatement.setInt(2, contentType);
            rs = preparedStatement.executeQuery();
            if (rs.next())
            {
               count = rs.getInt("count");
               String cType = rs.getString("contenttype");
            }

         }
         catch (SQLException e)
         {
            logger.error("Database Exception occoured while fetching user flags", e);
            response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Please check the userid and try again.");
            return RuleDecisionKey.ON_FAILURE;
         }
         catch (Exception e)
         {
            logger.error("Exception occoured while fetching user flags", e);
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal Error please try again later.");
            return RuleDecisionKey.ON_FAILURE;
         }
         finally
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Exception while closing result set.", e);
            }
            DbConnectionManager.releaseConnection(dbConnName, connection);
         }

         TestResponseOne one = new TestResponseOne();
         float[] f1 = new float[0];
         f1[0] = count;
         one.setValues(f1);
         one.setName(course);
         testResponseOnes.add(one);

      }

      TestResponseOne[] testResponseOnesArray = new TestResponseOne[testResponseOnes.size()];
      for (int i = 0; i < testResponseOnes.size(); i++)
      {
         testResponseOnesArray[i] = testResponseOnes.get(i);
      }

      TestResposeTwo testResposeTwo = new TestResposeTwo();
      testResposeTwo.setTestResponseOnes(testResponseOnesArray);

      String chartType = "bar";
      String yAxisDisplayText = "Text";
      String xAxisDisplayText = "Text";
      String yAxisDisplayLable = "Lable";
      String xAxisDisplayLable = "Lable";
      String[] cats = {"Total", "Yesterday", "Last Week", "Last Month", "Last Year"};

      testResposeTwo.setCats(cats);
      testResposeTwo.setxAxisDisplayLable(xAxisDisplayLable);
      testResposeTwo.setxAxisDisplayText(xAxisDisplayText);
      testResposeTwo.setyAxisDisplayLable(yAxisDisplayLable);
      testResposeTwo.setyAxisDisplayText(yAxisDisplayText);
      testResposeTwo.setChartType(chartType);

      response.setResponseObject(testResposeTwo);
      response.setStatusCodeSend(false);

      return RuleDecisionKey.ON_SUCCESS;
   }

}
