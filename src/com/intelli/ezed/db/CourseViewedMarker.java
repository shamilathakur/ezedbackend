package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EventData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;

public class CourseViewedMarker extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public CourseViewedMarker(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      EventData eventData = (EventData) flowContext.getFromContext(EzedKeyConstants.EVENT_OBJECT);
      String userid = (String) flowContext.getFromContext(EzedKeyConstants.USER_ID);
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "MarkGoalChanged");
         ps.setString(1, userid);
         ps.setString(2, eventData.getCourseId());
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in db while marking course view as changed for userid " + userid);
         }
      }
      catch (SQLException e)
      {
         if (logger.isTraceEnabled())
         {
            logger.trace("Cannot insert course in db while marking for change", e);
         }
         if (logger.isDebugEnabled())
         {
            logger.debug("Cannot insert course in db while marking course as changed for userid " + userid);
         }
      }
      catch (Exception e)
      {
         if (logger.isTraceEnabled())
         {
            logger.trace("Cannot insert course in db while marking for change", e);
         }
         if (logger.isDebugEnabled())
         {
            logger.debug("Cannot insert course in db while marking course as changed for userid " + userid);
         }
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
      return RuleDecisionKey.ON_SUCCESS;
   }

}
