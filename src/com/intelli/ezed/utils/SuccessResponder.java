package com.intelli.ezed.utils;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class SuccessResponder extends SingletonRule
{

   public SuccessResponder(String ruleName)
   {
      super(ruleName);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
      return RuleDecisionKey.ON_SUCCESS;
   }

}
