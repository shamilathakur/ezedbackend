package com.intelli.ezed.rules;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class TransactionPreparer extends SingletonRule
{

   public TransactionPreparer(String ruleName)
   {
      super(ruleName);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      
      String userid = messageData.getParameter("email");
      String sessionID = messageData.getParameter("sessionid");
      
      //System.out.println(sessionID);
      
      if (userid != null)
      {
         flowContext.putIntoContext(EzedKeyConstants.USER_ID, userid);
      }

      if (sessionID != null)
      {
         flowContext.putIntoContext(EzedKeyConstants.SESSION_ID, sessionID);
      }

      Response response = new Response();
      response.setParameters(EzedTransactionCodes.PROCESSING_REQUEST, "Request accepted for processing");
      flowContext.putIntoContext(EzedKeyConstants.EZED_RESPONSE, response);

      SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
      String currentTimeStamp = df.format(new Date(System.currentTimeMillis()));
      flowContext.putIntoContext(EzedKeyConstants.REQUEST_IN_TIME, currentTimeStamp);


      return RuleDecisionKey.ON_SUCCESS;
   }
}
