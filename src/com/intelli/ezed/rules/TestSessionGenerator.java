package com.intelli.ezed.rules;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.TestCorrectAnsTracker;
import com.intelli.ezed.utils.PrepareTestSessionIdBackend;

public class TestSessionGenerator extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private static PrepareTestSessionIdBackend sessionGen = new PrepareTestSessionIdBackend("TSE2", "ezeddb");

   public TestSessionGenerator(String ruleName)
   {
      super(ruleName);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      //      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      //      SessionManager sessionManager = (SessionManager) flowContext.getFromContext(EzedKeyConstants.TEST_SESSION_MANAGER);
      //      try
      //      {
      String sessionId = sessionGen.getNextId();
      if (logger.isDebugEnabled())
      {
         logger.debug("Test Session id generated " + sessionId);
      }
      flowContext.putIntoContext(EzedKeyConstants.TEST_SESSION_ID, sessionId);
      flowContext.putIntoContext(EzedKeyConstants.CORRECT_ANSWER_TRACKER, new TestCorrectAnsTracker());
      //      }
      //      catch (SessionException e)
      //      {
      //         logger.error("Session id could not be generated", e);
      //         response.setParameters(EzedTransactionCodes.SESSION_ERROR_OCCURRED, "Error while generating session");
      //         return RuleDecisionKey.ON_FAILURE;
      //      }
      return RuleDecisionKey.ON_SUCCESS;
   }
}
