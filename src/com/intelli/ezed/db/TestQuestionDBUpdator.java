package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.TestDetails;
import com.intelli.ezed.data.UserProfile;

public class TestQuestionDBUpdator extends SingletonRule
{

   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;
   public TestQuestionDBUpdator(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      UserProfile userProfile = (UserProfile) flowContext.getFromContext(EzedKeyConstants.USER_DETAILS);
      TestDetails testDetails = (TestDetails) flowContext.getFromContext(TestDetails.TEST_DETAILS);
      EzedRequestData requestData = (EzedRequestData) flowContext.getFromContext(EzedRequestData.EZED_REQUEST_DATA);
      String testSessionId = (String) flowContext.getFromContext(EzedKeyConstants.TEST_SESSION_ID);
      int isCorrect = 0;

      testDetails.setUserAns(requestData.getUserTestAns());
      if (testDetails.getLastQuestionCorrectAns().compareToIgnoreCase(testDetails.getUserAns()) == 0)
      {
         isCorrect = 1;
      }

      Connection connection = DbConnectionManager.getConnectionByName(dbConnName);
      String userAnswer = requestData.getUserTestAns();
      String userid = userProfile.getUserid();
      String questionId = testDetails.getSentQuestionDetails().getQuestionId();

      try
      {
         //update ongoingtest set receivedcorrectansstring = ? where userid  = ? and testsessionid = ? and questionId = ? 
         PreparedStatement preparedStatement = DbConnectionManager.getPreparedStatement(dbConnName, connection, "UpdateTestQuestion");
         preparedStatement.setString(1, userAnswer);
         preparedStatement.setInt(2, isCorrect);
         preparedStatement.setString(3, userid);
         preparedStatement.setString(4, testSessionId);
         preparedStatement.setString(5, questionId);
         int i = preparedStatement.executeUpdate();
         if (i != 1)
         {
            logger.error("No rows updated for userid : " + userid + " testSessionId : " + testSessionId + " questionId : " + questionId);
            response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while updating question in test.");
            return RuleDecisionKey.ON_FAILURE;
         }

      }
      catch (SQLException e)
      {
         logger.error("Error while updating details in ongoing test : " + testSessionId, e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Internal Error Please try again later.");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while updating details in ongoing test : " + testSessionId, e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Internal Error Please try again later.");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, connection);
      }
      return RuleDecisionKey.ON_SUCCESS;
   }
}
