package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.ScratchCardData;

public class ScratchCardsGenerator extends SingletonRule {
	private String dbConnName;
	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

	public ScratchCardsGenerator(String ruleName, String dbConnName) {
		super(ruleName);
		this.dbConnName = dbConnName;
	}

	@Override
	public RuleDecisionKey executeRule(FlowContext flowContext) {

		MessageData messageData = (MessageData) flowContext
				.getFromContext(MessageData.class.getName());

		Response response = (Response) flowContext
				.getFromContext(EzedKeyConstants.EZED_RESPONSE);

		ScratchCardData data = (ScratchCardData) flowContext
				.getFromContext(EzedKeyConstants.SCRATCH_CARD_OBJECT);
		Connection connection = DbConnectionManager
				.getConnectionByName(dbConnName);

		PreparedStatement ps = null;
		ResultSet rs1;
		int serialNumber = 1;
		ps = DbConnectionManager.getPreparedStatement(dbConnName, connection,
				"MaxSrNoFromScratchCard");
		try {
			rs1 = ps.executeQuery();

			System.out.println("Max Count returned : " + serialNumber);
			if (!rs1.next()) {
				serialNumber = 1;
			} else {
				serialNumber = rs1.getInt("count");
				serialNumber = serialNumber + 1;
			}
			System.out.println("Final Max Count : " + serialNumber);
		} catch (SQLException e1) {
			e1.printStackTrace();
		} catch (Exception e) {
			System.out.println("Something went wrong ... !!!");
		}
		finally
		{
			DbConnectionManager.releaseConnection(dbConnName, connection);
		}

		int count = data.getNoOfCards();
		for (int i = 0; i < count; i++) {
			try {
				data.handleCreateCard(dbConnName, logger, serialNumber);
				serialNumber = serialNumber + 1;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		response.setParameters(EzedTransactionCodes.SUCCESS,
				"Scratch card generated and added to the database successful.");
		return RuleDecisionKey.ON_SUCCESS;
	}

}
