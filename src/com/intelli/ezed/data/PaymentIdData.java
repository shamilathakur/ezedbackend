package com.intelli.ezed.data;

import com.intelli.ezed.dropdowns.data.DropDownListData;


public class PaymentIdData extends ResponseObject
{
   private String paymentId;
   private String appId;
   private Float newAmount = 0f;
   private String discountString;
   private DropDownListData list;

   public String getAppId()
   {
      return appId;
   }

   public void setAppId(String appId)
   {
      this.appId = appId;
   }

   public String getPaymentId()
   {
      return paymentId;
   }

   public void setPaymentId(String paymentId)
   {
      this.paymentId = paymentId;
   }

   @Override
   public void prepareResponse() throws Exception
   {
      this.put("paymentid", getAppId() + getPaymentId());
      if (newAmount != 0)
      {
         this.put("newamount", Float.toString((float) Math.round(getNewAmount() * 100) / 100));
      }
      this.put("coupondesc", getDiscountString());
      if (getList() != null)
      {
         getList().prepareResponse();
         this.put("desclist", list);
      }
   }

   public Float getNewAmount()
   {
      return newAmount;
   }

   public void setNewAmount(Float newAmount)
   {
      this.newAmount = newAmount;
   }

   public String getDiscountString()
   {
      return discountString;
   }

   public void setDiscountString(String discountString)
   {
      this.discountString = discountString;
   }

   public DropDownListData getList()
   {
      return list;
   }

   public void setList(DropDownListData list)
   {
      this.list = list;
   }

}
