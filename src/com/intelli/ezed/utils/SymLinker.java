package com.intelli.ezed.utils;

import java.io.File;

import org.slf4j.Logger;

public class SymLinker
{
   private native void createSymLink(String oldLink, String newLink);
   private native void removeSymLink(String newLink);
   public void createSymLink(String oldLink, String newLink, Logger logger)
   {
      if (logger.isDebugEnabled())
      {
         logger.debug("Creating sym link for Old link " + oldLink + " new link " + newLink);
      }
      createSymLink(oldLink, newLink);
   }

   public void removeSymLink(String newLink, Logger logger)
   {
      if (logger.isDebugEnabled())
      {
         logger.debug("Removing sym link for new link " + newLink);
      }
      removeSymLink(newLink);
   }

   private static SymLinker symLinker;

   static
   {
      System.load(new File("lib/symlink.so").getAbsolutePath());
   }

   public static SymLinker getInstance()
   {
      if (symLinker == null)
      {
         symLinker = new SymLinker();
      }
      return symLinker;
   }
}
