package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.FrontendFetchData;
import com.intelli.ezed.data.Response;

public class UtilizedScratchCardReportGenerator extends SingletonRule {

	private String dbConnName;
	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
	SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyyMMddHHmmss");

	public UtilizedScratchCardReportGenerator(String ruleName, String dbConnName) {
		super(ruleName);
		this.dbConnName = dbConnName;
	}

	@Override
	public RuleDecisionKey executeRule(FlowContext flowContext) {
		Response response = (Response) flowContext
				.getFromContext(EzedKeyConstants.EZED_RESPONSE);
		Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
		MessageData messageData = (MessageData) flowContext
				.getFromContext(MessageData.class.getName());
		PreparedStatement ps = null;
		ResultSet rs = null;

		String dynaQuery = "SELECT pay.userid,pay.scratchcardid,pay.totalamount,"
				+ "sc.srno,sc.coursetype,sc.generatedby,"
				+ "course.coursename,"
				+ "p.partnerid,p.partnername,"
				+ "city.cityname "
				+ "from "
				+ "paydata pay "
				+ "join "
				+ "scratchcardlist sc on pay.scratchcardid=sc.scratchcardid "
				+ "join "
				+ "partnerscratchcardmapping pscm on sc.scratchcardid=pscm.scratchcardid "
				+ "join "
				+ "partnerlist p on pscm.partnerid=p.partnerid "
				+ "join "
				+ "courselist course on sc.courseid=course.courseid "
				+ "join "
				+ "users us on pay.userid=us.userid "
				+ "join "
				+ "citylist city on us.cityid=city.cityid "
				+ "and pay.scratchcardid is not null";
		String finalQuery = dynaQuery + "";

		String startdate = messageData.getParameter("startdate");
		String enddate = messageData.getParameter("enddate");
		if ((startdate != null && startdate.compareTo("null") != 0 && startdate.compareTo("") != 0)
				&& enddate.compareTo("null") == 0) {
			enddate = sourceFormat.format(new Date());
		}
		String coursetype = messageData.getParameter("coursetype");
		String courseid = messageData.getParameter("courseid");
		String createdBy = messageData.getParameter("createdby");
		String partnerid = messageData.getParameter("partnerid");

		String qryDate = " and pay.intime between '" + startdate + "' and '"
				+ enddate + "'";
		String qryCourseId = " and sc.courseid='" + courseid + "'";
		String qryAdmin = " and sc.generatedby='" + createdBy + "'";
		String qryCourseType = " and sc.coursetype='" + coursetype + "'";
		String qryByPartner = " and p.partnerid='" + partnerid + "'";

		if (startdate != null && startdate.compareTo("null") != 0 && startdate.compareTo("") != 0
				) {
			finalQuery = finalQuery + qryDate;
		}
		if (createdBy.compareTo("null") != 0 && createdBy.compareTo("") != 0
				&& createdBy != null) {
			finalQuery = finalQuery + qryAdmin;
		}
		if (coursetype.compareTo("null") != 0 && coursetype.compareTo("") != 0
				&& coursetype != null) {
			finalQuery = finalQuery + qryCourseType;
		}
		if (courseid.compareTo("null") != 0 && courseid.compareTo("") != 0
				&& courseid != null) {
			finalQuery = finalQuery + qryCourseId;
		}
		if (partnerid.compareTo("null") != 0 && partnerid.compareTo("") != 0
				&& partnerid != null) {
			finalQuery = finalQuery + qryByPartner;
		}

		System.out.println("Final Qurey : " + finalQuery);

		String[] reportData = null;
		FrontendFetchData UtilizedScratchCardReport = new FrontendFetchData();
		response.setResponseObject(UtilizedScratchCardReport);
		response.setStatusCodeSend(false);
		UtilizedScratchCardReport.setAttribName("aaData");
		ArrayList<String[]> reportList = new ArrayList<String[]>();

		try {
			ps = conn.prepareStatement(finalQuery);
			rs = ps.executeQuery();
			while (rs.next()) {
				reportData = new String[9];
				reportData[0] = rs.getInt("srno") +"";
				reportData[1] = rs.getString("scratchcardid");
				reportData[2] = rs.getString("userid");
				reportData[3] = rs.getString("cityname");
				reportData[4] = rs.getString("coursename");
				int type = rs.getInt("coursetype");
				if(type == 1)
				{
					reportData[5] = "Content Only";
				}
				else
					if(type == 2)
					{
						reportData[5] = "Test Only";
					}
					else
					if(type == 3)
					{
						reportData[5] = "Full Course";
					}
				reportData[6] = rs.getInt("totalamount")+"";
				reportData[7] = rs.getString("partnerid");
				reportData[8] = rs.getString("partnername");
				
				
				reportList.add(reportData);
			}
			UtilizedScratchCardReport.setAttribList(reportList);
			response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
		} catch (SQLException e) {
			logger.error("Error while fetching Utilized SC details from db : ", e);
			response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED,
					"Error while fetching Utilized SC details");
			return RuleDecisionKey.ON_FAILURE;
		} catch (Exception e) {
			logger.error("Internal Error while fetching Utilized SC details : ", e);
			response.setParameters(EzedTransactionCodes.INTERNAL_ERROR,
					"Error while fetching Utilized SC details");
			return RuleDecisionKey.ON_FAILURE;
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					logger.error(
							"Error while closing result set while fetching Utilized SC list",
							e);
				}
			}
			DbConnectionManager.releaseConnection(dbConnName, conn);
		}

		return RuleDecisionKey.ON_SUCCESS;
	}

}
