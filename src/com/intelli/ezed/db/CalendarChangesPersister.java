package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.ActionData;
import com.intelli.ezed.data.CourseCalendarData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.GoalData;
import com.intelli.ezed.data.MasterActionRecords;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.UserProfile;
import com.intelli.ezed.utils.EzedHelper;

public class CalendarChangesPersister extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String actionId;

   public CalendarChangesPersister(String ruleName, String dbConnName, String actionId)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
      this.actionId = actionId;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      CourseCalendarData courseCalendarData = (CourseCalendarData) flowContext.getFromContext(EzedKeyConstants.COURSE_CALENDAR);
      String userId = (String) flowContext.getFromContext(EzedKeyConstants.USER_ID);
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      CourseCalendarData existingCalendarData = (CourseCalendarData) flowContext.getFromContext(EzedKeyConstants.EXISTING_COURSE_CALENDAR);
      UserProfile userProfile = (UserProfile) flowContext.getFromContext(EzedKeyConstants.USER_DETAILS);

      Connection conn = null;
      PreparedStatement ps = null;
      GoalData singleGoalData = null;
      SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

      int statusCode = 0;
      String statusDesc = null;
      StringBuffer sb = new StringBuffer();

      ActionData actionData = MasterActionRecords.fetchRecord(actionId);

      for (int i = 0; i < courseCalendarData.getGoalDatas().size(); i++)
      {
         singleGoalData = courseCalendarData.getGoalDatas().get(i);
         if (singleGoalData.getValidity() == GoalData.GOAL_VALID_FOR_UPDATE)
         {
            conn = DbConnectionManager.getConnectionByName(dbConnName);
            try
            {
               if (singleGoalData.getChapterId() != null)
               {
                  ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdateUserGoals");
                  ps.setString(1, sdf.format(singleGoalData.getEndDate()));
                  ps.setString(2, userId);
                  ps.setString(3, singleGoalData.getCourseId());
                  ps.setString(4, singleGoalData.getChapterId());
                  ps.setString(5, singleGoalData.getContentId());
               }
               else
               {
                  ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdateUserGoalsCourseTest");
                  ps.setString(1, sdf.format(singleGoalData.getEndDate()));
                  ps.setString(2, userId);
                  ps.setString(3, singleGoalData.getCourseId());
                  ps.setString(4, singleGoalData.getContentId());
               }
               int rows = ps.executeUpdate();
               if (rows == 0)
               {
                  statusCode = EzedTransactionCodes.INTERNAL_ERROR;
                  statusDesc = "Goal Could not be Updated. Please try again later";
               }
               else
               {
                  statusCode = EzedTransactionCodes.SUCCESS;
                  statusDesc = "Goal Updated Successfully";
                  //                  existingCalendarData.updateNewGoal(singleGoalData);
               }
            }
            catch (SQLException e)
            {
               logger.error("Error while updating goal in db", e);
               statusCode = EzedTransactionCodes.DB_EXCEPTION_OCCURRED;
               statusDesc = "Goal Could not be Updated. Please Try Again Later";
            }
            finally
            {
               DbConnectionManager.releaseConnection(dbConnName, conn);
            }

            int customFlag = 0;
            for (int j = 0; j < existingCalendarData.getGoalDatas().size(); j++)
            {
               GoalData goalData = existingCalendarData.getGoalDatas().get(j);
               if (singleGoalData.getCourseId() == goalData.getCourseId() && singleGoalData.getChapterId() == goalData.getChapterId() && singleGoalData.getContentId() == goalData.getContentId())
               {
                  customFlag = goalData.getCustomFlag();
                  break;
               }
            }

            if (customFlag == 0)
            {
               StringBuffer sb1 = new StringBuffer();
               sb1.append(userProfile.getName());
               sb1.append("modified a goal");
               sb1.append(" in course ");
               sb1.append(EzedHelper.fetchCourseNameForCourseId(dbConnName, singleGoalData.getCourseId(), logger));
               String goalChangedStream = sb1.toString();

               //Goal was modified for the first time
               try
               {
                  conn = DbConnectionManager.getConnectionByName(dbConnName);
                  ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdateCoursePoints");
                  ps.setInt(1, actionData.getPoints());
                  ps.setString(2, userProfile.getUserid());
                  ps.setString(3, singleGoalData.getCourseId());
                  int rows = ps.executeUpdate();
                  if (logger.isDebugEnabled())
                  {
                     logger.debug(rows + " rows updated in db while incrementing points for userid " + userProfile.getUserid() + " and course " + singleGoalData.getCourseId());
                  }
               }
               catch (SQLException e)
               {
                  logger.error("Error while incrementing points for userid " + userProfile.getUserid() + " and course " + singleGoalData.getCourseId(), e);
               }
               catch (Exception e)
               {
                  logger.error("Error while incrementing points for userid " + userProfile.getUserid() + " and course " + singleGoalData.getCourseId(), e);
               }
               finally
               {
                  DbConnectionManager.releaseConnection(dbConnName, conn);
               }


               try
               {
                  conn = DbConnectionManager.getConnectionByName(dbConnName);
                  ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdateUserPoints");
                  ps.setInt(1, actionData.getPoints());
                  ps.setString(2, userProfile.getUserid());
                  int rows = ps.executeUpdate();
                  if (logger.isDebugEnabled())
                  {
                     logger.debug(rows + " rows updated in db while incrementing points for userid " + userProfile.getUserid());
                  }
               }
               catch (SQLException e)
               {
                  logger.error("Error while incrementing points for userid " + userProfile.getUserid(), e);
               }
               catch (Exception e)
               {
                  logger.error("Error while incrementing points for userid " + userProfile.getUserid(), e);
               }
               finally
               {
                  DbConnectionManager.releaseConnection(dbConnName, conn);
               }

               try
               {
                  conn = DbConnectionManager.getConnectionByName(dbConnName);
                  ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertUserStreamAll");
                  ps.setString(1, goalChangedStream);
                  ps.setString(2, userProfile.getUserid());
                  ps.setString(3, sdf.format(new Date()));
                  ps.setString(4, singleGoalData.getCourseId());
                  ps.setString(5, singleGoalData.getChapterId());
                  ps.setString(6, singleGoalData.getContentId());
                  ps.setString(7, actionId);
                  int rows = ps.executeUpdate();
                  if (logger.isDebugEnabled())
                  {
                     logger.debug(rows + " rows inserted in db while inserting stream for user " + userProfile.getUserid());
                  }
               }
               catch (Exception e)
               {
                  logger.error("Error inserting stream entry for " + userProfile.getUserid(), e);
                  logger.error("INSERT FAILED " + goalChangedStream);
               }
               finally
               {
                  DbConnectionManager.releaseConnection(dbConnName, conn);
               }
            }
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Single goal data is not valid for update " + singleGoalData.getString(1));
            }
            if (singleGoalData.getValidity() == GoalData.GOAL_ALREADY_COMPLETE)
            {
               statusCode = EzedTransactionCodes.INTERNAL_ERROR;
               statusDesc = "Goal is already Achieved";
            }
            else if (singleGoalData.getValidity() == GoalData.GOAL_AFTER_ENDDATE)
            {
               statusCode = EzedTransactionCodes.INTERNAL_ERROR;
               statusDesc = "Goal cannot be Updated to a Date after the Course End Date";
            }
            else if (singleGoalData.getValidity() == GoalData.GOAL_BEFORE_STARTDATE)
            {
               statusCode = EzedTransactionCodes.INTERNAL_ERROR;
               statusDesc = "Goal cannot be Updated to a Date before the Course Start Date";
            }
            else if (singleGoalData.getValidity() == GoalData.GOAL_BEFORE_TODAY)
            {
               statusCode = EzedTransactionCodes.INTERNAL_ERROR;
               statusDesc = "Goal cannot be Updated to a Date before today";
            }
            else
            {
               statusCode = EzedTransactionCodes.INTERNAL_ERROR;
               statusDesc = "Goal could not be Updated. Please Try Again Later";
            }
         }
         sb.append(userId).append(",");
         sb.append(singleGoalData.getCourseId()).append(",");
         sb.append(singleGoalData.getChapterId()).append(",");
         sb.append(singleGoalData.getContentId()).append(",");
         sb.append(sdf.format(singleGoalData.getEndDate())).append(",");
         sb.append(singleGoalData.getValidity()).append(",");
         sb.append(statusCode).append(",");
         sb.append(statusDesc).append("\n");
      }

      if (logger.isDebugEnabled())
      {
         logger.debug("Goal update status log : " + sb.toString());
      }
      //      response.setResponseObject(existingCalendarData);
      response.setResponseObject(null);
      response.setParameters(statusCode, statusDesc);
      //      response.setParameters(EzedTransactionCodes.SUCCESS, "Goals updated successfully");
      return RuleDecisionKey.ON_SUCCESS;
   }
}
