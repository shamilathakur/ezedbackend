package com.intelli.ezed.install;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.utils.map.TimedMap;
import org.sabre.workflow.context.FlowContext;

import com.intelli.ezed.utils.TestTimeOutHandler;

public class TimedMapTestser extends SingletonRule
{

   public TimedMapTestser(String ruleName)
   {
      super(ruleName);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      TimedMap<String, Object> timedMap = new TimedMap<String, Object>(25, 10000l, new TestTimeOutHandler());
      for (int i = 0; i < 10; i++)
      {
         timedMap.put(Integer.toString(i), i);
      }
      return RuleDecisionKey.ON_SUCCESS;
   }

}
