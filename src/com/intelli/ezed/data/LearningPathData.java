package com.intelli.ezed.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.sabre.db.DbConnectionManager;
import org.sabre.exceptions.InputProcessorException;
import org.sabre.workflow.LinkedProcessorManager;
import org.slf4j.Logger;

import com.intelli.ezed.bulkupload.data.ChapterDetails;

public class LearningPathData
{
   private String chapterId;
   private ArrayList<String> contentIdList = new ArrayList<String>();
   private ArrayList<String> contentShowFlagList = new ArrayList<String>();
   private ArrayList<String> origContentList = new ArrayList<String>();
   private ArrayList<String> origContentShowFlagList = new ArrayList<String>();
   public String getChapterId()
   {
      return chapterId;
   }
   public void setChapterId(String chapterId)
   {
      this.chapterId = chapterId;
   }
   public ArrayList<String> getContentIdList()
   {
      return contentIdList;
   }
   public void setContentIdList(ArrayList<String> contentIdList)
   {
      this.contentIdList = contentIdList;
   }

   public void setContentIdList(String[] contentIdList)
   {
      for (int i = 0; i < contentIdList.length; i++)
      {
         this.getContentIdList().add(contentIdList[i]);
      }
   }
   public ArrayList<String> getContentShowFlagList()
   {
      return contentShowFlagList;
   }
   public void setContentShowFlagList(ArrayList<String> contentShowFlagList)
   {
      this.contentShowFlagList = contentShowFlagList;
   }
   public void setContentShowFlagList(String[] contentShowFlagList)
   {
      for (int i = 0; i < contentShowFlagList.length; i++)
      {
         if (contentShowFlagList[i].trim().compareTo(ChapterDetails.HIDE_CONTENT) == 0)
         {
            this.getContentShowFlagList().add(ChapterDetails.HIDE_CONTENT);
         }
         else
         {
            this.getContentShowFlagList().add(ChapterDetails.SHOW_CONTENT);
         }
      }
   }
   public ArrayList<String> getOrigContentList()
   {
      return origContentList;
   }
   public void setOrigContentList(ArrayList<String> origContentList)
   {
      this.origContentList = origContentList;
   }
   public ArrayList<String> getOrigContentShowFlagList()
   {
      return origContentShowFlagList;
   }
   public void setOrigContentShowFlagList(ArrayList<String> origContentShowFlagList)
   {
      this.origContentShowFlagList = origContentShowFlagList;
   }

   public void fetchExistingLearningPathDetails(String dbConnName, Logger logger) throws SQLException, Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchLearningPathForUpdate");
         ps.setString(1, getChapterId());
         rs = ps.executeQuery();
         while (rs.next())
         {
            this.getOrigContentList().add(rs.getString("contentid"));
            this.getOrigContentShowFlagList().add(rs.getString("contentshowflag"));
         }
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {

            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   private GoalModificationData calculateExtraContents()
   {
      GoalModificationData data = new GoalModificationData();
      data.setChapterId(getChapterId());
      String currentContent = null;
      String currentContentShowFlag = null;
      boolean contentMatches = false;
      for (int i = 0; i < getContentIdList().size(); i++)
      {
         currentContent = getContentIdList().get(i);
         contentMatches = false;
         currentContentShowFlag = getContentShowFlagList().get(i);
         for (int j = 0; j < getOrigContentList().size(); j++)
         {
            if (currentContent.compareTo(getOrigContentList().get(j)) == 0)
            {
               //above if shows that the content matches from previous entry
               contentMatches = true;
               if (getOrigContentShowFlagList().get(j).compareTo(ChapterDetails.HIDE_CONTENT) == 0 && currentContentShowFlag.compareTo(ChapterDetails.SHOW_CONTENT) == 0)
               {
                  //previously content was hidden, now it is visible, add it to extra content
                  data.getContentIdList().add(currentContent);
                  data.getContentshowFlag().add(currentContentShowFlag);
               }
               break;
            }
         }
         if (!contentMatches)
         {
            //if this content was not present originally
            data.getContentIdList().add(currentContent);
            data.getContentshowFlag().add(currentContentShowFlag);
         }
      }
      return data;
   }

   private GoalModificationData calculateContentsToBeDeleted()
   {
      GoalModificationData data = new GoalModificationData();
      data.setChapterId(getChapterId());
      String currentContent = null;
      String currentContentShowFlag = null;
      boolean contentMatches = false;
      for (int i = 0; i < getOrigContentList().size(); i++)
      {
         currentContent = getOrigContentList().get(i);
         contentMatches = false;
         currentContentShowFlag = getOrigContentShowFlagList().get(i);
         for (int j = 0; j < getContentIdList().size(); j++)
         {
            if (currentContent.compareTo(getContentIdList().get(j)) == 0)
            {
               contentMatches = true;
               break;
            }
         }
         if (!contentMatches)
         {
            //if this content was not present originally
            if (currentContentShowFlag.compareTo(ChapterDetails.SHOW_CONTENT) == 0)
            {
               data.getContentIdList().add(currentContent);
               data.getContentshowFlag().add(currentContentShowFlag);
            }
         }
      }
      return data;
   }

   public void startGoalModificationHandler(String processorName) throws InputProcessorException
   {
      Map<Object, Object> data = new HashMap<Object, Object>();
      data.put(EzedKeyConstants.GOAL_MODIFICATION_DATA, calculateExtraContents());
      LinkedProcessorManager.addTask(processorName, data);
   }
   
   public void startGoalDeletionHandler(String processorName) throws InputProcessorException
   {
      Map<Object, Object> data = new HashMap<Object, Object>();
      data.put(EzedKeyConstants.GOAL_MODIFICATION_DATA, calculateContentsToBeDeleted());
      LinkedProcessorManager.addTask(processorName, data);
   }
}
