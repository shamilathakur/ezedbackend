package com.intelli.ezed.utils;

import org.sabre.utils.map.TimedMap.MapTimeoutHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedLoggerName;

public class TestTimeOutHandler implements MapTimeoutHandler<String, Object>
{

   Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   @Override
   public void handleTimeout(String arg0, Object arg1)
   {
      logger.debug("Timed out map key " + arg0 + " value " + arg1);
   }

}
