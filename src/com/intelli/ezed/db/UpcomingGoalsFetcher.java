package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.analytics.data.GenericChartData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.UpcomingGoalData;

public class UpcomingGoalsFetcher extends SingletonRule
{
   private String dbConnName;
   private int limit;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public UpcomingGoalsFetcher(String ruleName, String dbConnName, String limit)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
      this.limit = Integer.parseInt(limit);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      String userId = (String) flowContext.getFromContext(EzedKeyConstants.USER_ID);
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      Connection conn = null;
      PreparedStatement ps = null;
      ResultSet rs = null;
      UpcomingGoalData singleData = null;
      ArrayList<UpcomingGoalData> goalDatas = new ArrayList<UpcomingGoalData>(limit);
      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchUpcomingGoals");
         ps.setString(1, userId);
         ps.setInt(2, limit);
         rs = ps.executeQuery();
         while (rs.next())
         {
            singleData = new UpcomingGoalData();
            singleData.setCourseId(rs.getString("courseid"));
            singleData.setCourseName(rs.getString("coursename"));
            singleData.setChapterId(rs.getString("chapterid"));
            singleData.setChapterName(rs.getString("chaptername"));
            singleData.setContentId(rs.getString("contentid"));
            singleData.setContentName(rs.getString("contentname"));
            singleData.setEndDate(rs.getString("endtime"));
            goalDatas.add(singleData);
         }

         JSONArray array = new JSONArray();
         for (int i = 0; i < goalDatas.size(); i++)
         {
            goalDatas.get(i).prepareResponse();
            array.put(goalDatas.get(i));
         }
         GenericChartData chartData = new GenericChartData();
         chartData.getText().add("upcominggoals");
         chartData.getValues().add(array);
         response.setResponseObject(chartData);
         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
         return RuleDecisionKey.ON_SUCCESS;
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching upcoming goals", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching upcoming goals");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching upcoming goals", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching upcoming goals");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing resultset while fetching upcoming goals", e);
            }
            DbConnectionManager.releaseConnection(dbConnName, conn);
         }
      }
   }
}
