package com.intelli.ezed.bulkupload.rules;

import java.io.File;
import java.io.IOException;

import org.apache.commons.digester3.Digester;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import com.intelli.ezed.bulkupload.data.ChapterDetails;
import com.intelli.ezed.bulkupload.data.CourseDetails;
import com.intelli.ezed.bulkupload.data.QuestionBankDetails;
import com.intelli.ezed.bulkupload.data.TestQuestionConditionRule;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;

public class QuestionBankHandler extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.BULK_UPLOAD_LOG);
   private String questionPropertiesFileName;
   private Digester questionDigester = new Digester();
   private static Object lockObject = new Object();
   private String dbConnName;

   public QuestionBankHandler(String ruleName, String dbConnName, String questionPropertiesFileName,
                              String questionDetailsClassFQCN)
   {
      super(ruleName);
      initDigester(questionDetailsClassFQCN);
      this.dbConnName = dbConnName;
      this.questionPropertiesFileName = questionPropertiesFileName;
   }

   private void initDigester(String questionDetailsClassFQCN)
   {
      questionDigester.addObjectCreate("questionbank", questionDetailsClassFQCN);
      questionDigester.addRule("questionbank/question", new TestQuestionConditionRule());

      questionDigester.addObjectCreate("questionbank/question/questionstatement", "com.intelli.ezed.data.QuestionContent");
      questionDigester.addCallMethod("questionbank/question/questionstatement/type", "setType", 0, new Class<?>[]{Integer.class});
      questionDigester.addCallMethod("questionbank/question/questionstatement/string", "setContentString", 0);
      questionDigester.addCallMethod("questionbank/question/questionstatement/content", "setContentLoc", 0);
      questionDigester.addSetNext("questionbank/question/questionstatement/content", "setQuestion", "com.intelli.ezed.data.QuestionContent");

      questionDigester.addObjectCreate("questionbank/question/answer1statement", "com.intelli.ezed.data.QuestionContent");
      questionDigester.addCallMethod("questionbank/question/answer1statement/type", "setType", 0, new Class<?>[]{Integer.class});
      questionDigester.addCallMethod("questionbank/question/answer1statement/string", "setContentString", 0);
      questionDigester.addCallMethod("questionbank/question/answer1statement/content", "setContentLoc", 0);
      questionDigester.addSetNext("questionbank/question/answer1statement/content", "addAnswers", "com.intelli.ezed.data.QuestionContent");

      questionDigester.addObjectCreate("questionbank/question/question1statement", "com.intelli.ezed.data.QuestionContent");
      questionDigester.addCallMethod("questionbank/question/question1statement/type", "setType", 0, new Class<?>[]{Integer.class});
      questionDigester.addCallMethod("questionbank/question/question1statement/string", "setContentString", 0);
      questionDigester.addCallMethod("questionbank/question/question1statement/content", "setContentLoc", 0);
      questionDigester.addSetNext("questionbank/question/question1statement/content", "addQuestions", "com.intelli.ezed.data.QuestionContent");

      questionDigester.addObjectCreate("questionbank/question/answer2statement", "com.intelli.ezed.data.QuestionContent");
      questionDigester.addCallMethod("questionbank/question/answer2statement/type", "setType", 0, new Class<?>[]{Integer.class});
      questionDigester.addCallMethod("questionbank/question/answer2statement/string", "setContentString", 0);
      questionDigester.addCallMethod("questionbank/question/answer2statement/content", "setContentLoc", 0);
      questionDigester.addSetNext("questionbank/question/answer2statement/content", "addAnswers", "com.intelli.ezed.data.QuestionContent");

      questionDigester.addObjectCreate("questionbank/question/question2statement", "com.intelli.ezed.data.QuestionContent");
      questionDigester.addCallMethod("questionbank/question/question2statement/type", "setType", 0, new Class<?>[]{Integer.class});
      questionDigester.addCallMethod("questionbank/question/question2statement/string", "setContentString", 0);
      questionDigester.addCallMethod("questionbank/question/question2statement/content", "setContentLoc", 0);
      questionDigester.addSetNext("questionbank/question/question2statement/content", "addQuestions", "com.intelli.ezed.data.QuestionContent");

      questionDigester.addObjectCreate("questionbank/question/answer3statement", "com.intelli.ezed.data.QuestionContent");
      questionDigester.addCallMethod("questionbank/question/answer3statement/type", "setType", 0, new Class<?>[]{Integer.class});
      questionDigester.addCallMethod("questionbank/question/answer3statement/string", "setContentString", 0);
      questionDigester.addCallMethod("questionbank/question/answer3statement/content", "setContentLoc", 0);
      questionDigester.addSetNext("questionbank/question/answer3statement/content", "addAnswers", "com.intelli.ezed.data.QuestionContent");

      questionDigester.addObjectCreate("questionbank/question/question3statement", "com.intelli.ezed.data.QuestionContent");
      questionDigester.addCallMethod("questionbank/question/question3statement/type", "setType", 0, new Class<?>[]{Integer.class});
      questionDigester.addCallMethod("questionbank/question/question3statement/string", "setContentString", 0);
      questionDigester.addCallMethod("questionbank/question/question3statement/content", "setContentLoc", 0);
      questionDigester.addSetNext("questionbank/question/question3statement/content", "addQuestions", "com.intelli.ezed.data.QuestionContent");

      questionDigester.addObjectCreate("questionbank/question/answer4statement", "com.intelli.ezed.data.QuestionContent");
      questionDigester.addCallMethod("questionbank/question/answer4statement/type", "setType", 0, new Class<?>[]{Integer.class});
      questionDigester.addCallMethod("questionbank/question/answer4statement/string", "setContentString", 0);
      questionDigester.addCallMethod("questionbank/question/answer4statement/content", "setContentLoc", 0);
      questionDigester.addSetNext("questionbank/question/answer4statement/content", "addAnswers", "com.intelli.ezed.data.QuestionContent");

      questionDigester.addObjectCreate("questionbank/question/question4statement", "com.intelli.ezed.data.QuestionContent");
      questionDigester.addCallMethod("questionbank/question/question4statement/type", "setType", 0, new Class<?>[]{Integer.class});
      questionDigester.addCallMethod("questionbank/question/question4statement/string", "setContentString", 0);
      questionDigester.addCallMethod("questionbank/question/question4statement/content", "setContentLoc", 0);
      questionDigester.addSetNext("questionbank/question/question4statement/content", "addQuestions", "com.intelli.ezed.data.QuestionContent");

      questionDigester.addObjectCreate("questionbank/question/answer5statement", "com.intelli.ezed.data.QuestionContent");
      questionDigester.addCallMethod("questionbank/question/answer5statement/type", "setType", 0, new Class<?>[]{Integer.class});
      questionDigester.addCallMethod("questionbank/question/answer5statement/string", "setContentString", 0);
      questionDigester.addCallMethod("questionbank/question/answer5statement/content", "setContentLoc", 0);
      questionDigester.addSetNext("questionbank/question/answer5statement/content", "addAnswers", "com.intelli.ezed.data.QuestionContent");

      questionDigester.addObjectCreate("questionbank/question/question5statement", "com.intelli.ezed.data.QuestionContent");
      questionDigester.addCallMethod("questionbank/question/question5statement/type", "setType", 0, new Class<?>[]{Integer.class});
      questionDigester.addCallMethod("questionbank/question/question5statement/string", "setContentString", 0);
      questionDigester.addCallMethod("questionbank/question/question5statement/content", "setContentLoc", 0);
      questionDigester.addSetNext("questionbank/question/question5statement/content", "addQuestions", "com.intelli.ezed.data.QuestionContent");

      questionDigester.addObjectCreate("questionbank/question/answer6statement", "com.intelli.ezed.data.QuestionContent");
      questionDigester.addCallMethod("questionbank/question/answer6statement/type", "setType", 0, new Class<?>[]{Integer.class});
      questionDigester.addCallMethod("questionbank/question/answer6statement/string", "setContentString", 0);
      questionDigester.addCallMethod("questionbank/question/answer6statement/content", "setContentLoc", 0);
      questionDigester.addSetNext("questionbank/question/answer6statement/content", "addAnswers", "com.intelli.ezed.data.QuestionContent");

      questionDigester.addObjectCreate("questionbank/question/question6statement", "com.intelli.ezed.data.QuestionContent");
      questionDigester.addCallMethod("questionbank/question/question6statement/type", "setType", 0, new Class<?>[]{Integer.class});
      questionDigester.addCallMethod("questionbank/question/question6statement/string", "setContentString", 0);
      questionDigester.addCallMethod("questionbank/question/question6statement/content", "setContentLoc", 0);
      questionDigester.addSetNext("questionbank/question/question6statement/content", "addQuestions", "com.intelli.ezed.data.QuestionContent");

      questionDigester.addCallMethod("questionbank/question/difficulty", "setDifficultyLevel", 0, new Class<?>[]{Integer.class});

      questionDigester.addCallMethod("questionbank/question/correctanswer", "setCorrectAnswer", 0);
      questionDigester.addCallMethod("quizquestions/question/explanation", "setExplanation", 0);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      CourseDetails courseDetails = (CourseDetails) flowContext.getFromContext(EzedKeyConstants.BULK_UPLOAD_COURSEOBJECT);
      ChapterDetails chapterDetails = (ChapterDetails) flowContext.getFromContext(EzedKeyConstants.BULK_UPLOAD_CHAPTEROBJECT);
      File dirFile = (File) flowContext.getFromContext(EzedKeyConstants.BULK_UPLOAD_QBANK_DIR_FILE);
      String qbankPropertiesFile = dirFile.getAbsolutePath() + "/" + questionPropertiesFileName;
      QuestionBankDetails qbankDetails = null;

      try
      {
         synchronized (lockObject)
         {
            qbankDetails = (QuestionBankDetails) questionDigester.parse(new File(qbankPropertiesFile));
         }
         qbankDetails.setCourseId(courseDetails.getCourseId());
         if (chapterDetails != null)
         {
            qbankDetails.setChapterId(chapterDetails.getChapterId());
         }
         qbankDetails.persistQuestionBank(dbConnName, logger);
      }
      catch (IOException e)
      {
         logger.error("Error while reading question bank details properties file", e);
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (SAXException e)
      {
         logger.error("Error while reading question bank details properties file", e);
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while processing question bank details after reading properly from properties file", e);
         return RuleDecisionKey.ON_FAILURE;
      }
      return RuleDecisionKey.ON_SUCCESS;
   }
}
