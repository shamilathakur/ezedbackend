package com.intelli.ezed.data;

import org.json.JSONArray;
import org.json.JSONObject;

public class Response extends JSONObject
{
   private int responseCode;
   private String responseMessage;
   private ResponseObject responseObject;
   private boolean isStatusCodeSend = true;
   private boolean isStringOnlySend = false;
   private boolean isOnlyArraySend = false;


   public boolean isOnlyArraySend()
   {
      return isOnlyArraySend;
   }

   public void setOnlyArraySend(boolean isOnlyArraySend)
   {
      this.isOnlyArraySend = isOnlyArraySend;
   }

   public boolean isStringOnlySend()
   {
      return isStringOnlySend;
   }

   public void setStringOnlySend(boolean isStringOnlySend)
   {
      this.isStringOnlySend = isStringOnlySend;
   }

   public boolean isStatusCodeSend()
   {
      return isStatusCodeSend;
   }

   public void setStatusCodeSend(boolean isStatusCodeSend)
   {
      this.isStatusCodeSend = isStatusCodeSend;
   }

   public int getResponseCode()
   {
      return responseCode;
   }

   public void setResponseCode(int responseCode)
   {
      this.responseCode = responseCode;
   }

   public String getResponseMessage()
   {
      return responseMessage;
   }

   public void setResponseMessage(String responseMessage)
   {
      this.responseMessage = responseMessage;
   }

   public ResponseObject getResponseObject()
   {
      return responseObject;
   }

   public void setResponseObject(ResponseObject responseObject)
   {
      this.responseObject = responseObject;
   }

   public void setParameters(int responseCode, String responseMessage)
   {
      setResponseCode(responseCode);
      setResponseMessage(responseMessage);
   }

   public void setParameters(String responseCode, String responseMessage)
   {
      setParameters(Integer.parseInt(responseCode), responseMessage);
   }

   public String getResponse() throws Exception
   {
      if (!isStatusCodeSend())
      {
         if (isOnlyArraySend())
         {
            if (responseObject != null)
            {
               getResponseObject().prepareResponse();
               JSONArray array = (JSONArray) getResponseObject().get("values");
               return array.toString(2);
            }

         }

         if (responseObject != null)
         {
            getResponseObject().prepareResponse();
            return getResponseObject().toString(2);
         }
      }
      if (isStringOnlySend())
      {
         return getResponseMessage();
      }
      this.put("statuscode", getResponseCode());
      this.put("statusmessage", getResponseMessage());
      if (getResponseObject() != null && (getResponseCode() == EzedTransactionCodes.SUCCESS || getResponseCode() == EzedTransactionCodes.TEST_END_REACHED))
      {
         getResponseObject().prepareResponse();
         this.put("response", getResponseObject());
      }
      return this.toString(2);
      //      StringBuffer sb = new StringBuffer();
      //      sb.append("statuscode").append(getResponseCode());
      //      sb.append("&statusmessage=").append(getResponseMessage());
      //      if (getResponseData() != null && getResponseCode() == EzedTransactionCodes.SUCCESS)
      //      {
      //         sb.append("&response=").append(getResponseData().getResponse());
      //      }
      //      return sb.toString();

   }
}
