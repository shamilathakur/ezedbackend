//package com.intelli.ezed.install;
//
//import java.io.File;
//import java.io.IOException;
//
//import org.apache.commons.digester3.Digester;
//import org.apache.log4j.PropertyConfigurator;
//import org.xml.sax.SAXException;
//
//import com.intelli.ezed.bulkupload.data.QuestionBankDetails;
//import com.intelli.ezed.bulkupload.data.QuestionConditionRule;
//import com.intelli.ezed.bulkupload.data.QuizDetails;
//
//public class QuizDigester
//{
//
//   public static void main(String[] args)
//   {
//      PropertyConfigurator.configure("conf/log4j.properties");
//      Digester digester = new Digester();
//      digester.addRule("quizquestions/question", new QuestionConditionRule());
//
//      digester.addObjectCreate("quizquestions/question/questionstatement", "com.intelli.ezed.data.QuestionContent");
//      digester.addCallMethod("quizquestions/question/questionstatement/type", "setType", 0, new Class<?>[]{Integer.class});
//      digester.addCallMethod("quizquestions/question/questionstatement/string", "setContentString", 0);
//      digester.addCallMethod("quizquestions/question/questionstatement/content", "setContentLoc", 0);
//      digester.addSetNext("quizquestions/question/questionstatement/content", "setQuestion", "com.intelli.ezed.data.QuestionContent");
//
//      digester.addObjectCreate("quizquestions/question/answer1statement", "com.intelli.ezed.data.QuestionContent");
//      digester.addCallMethod("quizquestions/question/answer1statement/type", "setType", 0, new Class<?>[]{Integer.class});
//      digester.addCallMethod("quizquestions/question/answer1statement/string", "setContentString", 0);
//      digester.addCallMethod("quizquestions/question/answer1statement/content", "setContentLoc", 0);
//      digester.addSetNext("quizquestions/question/answer1statement/content", "addAnswers", "com.intelli.ezed.data.QuestionContent");
//
//      digester.addObjectCreate("quizquestions/question/question1statement", "com.intelli.ezed.data.QuestionContent");
//      digester.addCallMethod("quizquestions/question/question1statement/type", "setType", 0, new Class<?>[]{Integer.class});
//      digester.addCallMethod("quizquestions/question/question1statement/string", "setContentString", 0);
//      digester.addCallMethod("quizquestions/question/question1statement/content", "setContentLoc", 0);
//      digester.addSetNext("quizquestions/question/question1statement/content", "addQuestions", "com.intelli.ezed.data.QuestionContent");
//
//      digester.addObjectCreate("quizquestions/question/answer2statement", "com.intelli.ezed.data.QuestionContent");
//      digester.addCallMethod("quizquestions/question/answer2statement/type", "setType", 0, new Class<?>[]{Integer.class});
//      digester.addCallMethod("quizquestions/question/answer2statement/string", "setContentString", 0);
//      digester.addCallMethod("quizquestions/question/answer2statement/content", "setContentLoc", 0);
//      digester.addSetNext("quizquestions/question/answer2statement/content", "addAnswers", "com.intelli.ezed.data.QuestionContent");
//
//      digester.addObjectCreate("quizquestions/question/question2statement", "com.intelli.ezed.data.QuestionContent");
//      digester.addCallMethod("quizquestions/question/question2statement/type", "setType", 0, new Class<?>[]{Integer.class});
//      digester.addCallMethod("quizquestions/question/question2statement/string", "setContentString", 0);
//      digester.addCallMethod("quizquestions/question/question2statement/content", "setContentLoc", 0);
//      digester.addSetNext("quizquestions/question/question2statement/content", "addQuestions", "com.intelli.ezed.data.QuestionContent");
//
//      digester.addObjectCreate("quizquestions/question/answer3statement", "com.intelli.ezed.data.QuestionContent");
//      digester.addCallMethod("quizquestions/question/answer3statement/type", "setType", 0, new Class<?>[]{Integer.class});
//      digester.addCallMethod("quizquestions/question/answer3statement/string", "setContentString", 0);
//      digester.addCallMethod("quizquestions/question/answer3statement/content", "setContentLoc", 0);
//      digester.addSetNext("quizquestions/question/answer3statement/content", "addAnswers", "com.intelli.ezed.data.QuestionContent");
//
//      digester.addObjectCreate("quizquestions/question/question3statement", "com.intelli.ezed.data.QuestionContent");
//      digester.addCallMethod("quizquestions/question/question3statement/type", "setType", 0, new Class<?>[]{Integer.class});
//      digester.addCallMethod("quizquestions/question/question3statement/string", "setContentString", 0);
//      digester.addCallMethod("quizquestions/question/question3statement/content", "setContentLoc", 0);
//      digester.addSetNext("quizquestions/question/question3statement/content", "addQuestions", "com.intelli.ezed.data.QuestionContent");
//
//      digester.addObjectCreate("quizquestions/question/answer4statement", "com.intelli.ezed.data.QuestionContent");
//      digester.addCallMethod("quizquestions/question/answer4statement/type", "setType", 0, new Class<?>[]{Integer.class});
//      digester.addCallMethod("quizquestions/question/answer4statement/string", "setContentString", 0);
//      digester.addCallMethod("quizquestions/question/answer4statement/content", "setContentLoc", 0);
//      digester.addSetNext("quizquestions/question/answer4statement/content", "addAnswers", "com.intelli.ezed.data.QuestionContent");
//
//      digester.addObjectCreate("quizquestions/question/question4statement", "com.intelli.ezed.data.QuestionContent");
//      digester.addCallMethod("quizquestions/question/question4statement/type", "setType", 0, new Class<?>[]{Integer.class});
//      digester.addCallMethod("quizquestions/question/question4statement/string", "setContentString", 0);
//      digester.addCallMethod("quizquestions/question/question4statement/content", "setContentLoc", 0);
//      digester.addSetNext("quizquestions/question/question4statement/content", "addQuestions", "com.intelli.ezed.data.QuestionContent");
//
//      digester.addObjectCreate("quizquestions/question/answer5statement", "com.intelli.ezed.data.QuestionContent");
//      digester.addCallMethod("quizquestions/question/answer5statement/type", "setType", 0, new Class<?>[]{Integer.class});
//      digester.addCallMethod("quizquestions/question/answer5statement/string", "setContentString", 0);
//      digester.addCallMethod("quizquestions/question/answer5statement/content", "setContentLoc", 0);
//      digester.addSetNext("quizquestions/question/answer5statement/content", "addAnswers", "com.intelli.ezed.data.QuestionContent");
//
//      digester.addObjectCreate("quizquestions/question/question5statement", "com.intelli.ezed.data.QuestionContent");
//      digester.addCallMethod("quizquestions/question/question5statement/type", "setType", 0, new Class<?>[]{Integer.class});
//      digester.addCallMethod("quizquestions/question/question5statement/string", "setContentString", 0);
//      digester.addCallMethod("quizquestions/question/question5statement/content", "setContentLoc", 0);
//      digester.addSetNext("quizquestions/question/question5statement/content", "addQuestions", "com.intelli.ezed.data.QuestionContent");
//
//      digester.addObjectCreate("quizquestions/question/answer6statement", "com.intelli.ezed.data.QuestionContent");
//      digester.addCallMethod("quizquestions/question/answer6statement/type", "setType", 0, new Class<?>[]{Integer.class});
//      digester.addCallMethod("quizquestions/question/answer6statement/string", "setContentString", 0);
//      digester.addCallMethod("quizquestions/question/answer6statement/content", "setContentLoc", 0);
//      digester.addSetNext("quizquestions/question/answer6statement/content", "addAnswers", "com.intelli.ezed.data.QuestionContent");
//
//      digester.addObjectCreate("quizquestions/question/question6statement", "com.intelli.ezed.data.QuestionContent");
//      digester.addCallMethod("quizquestions/question/question6statement/type", "setType", 0, new Class<?>[]{Integer.class});
//      digester.addCallMethod("quizquestions/question/question6statement/string", "setContentString", 0);
//      digester.addCallMethod("quizquestions/question/question6statement/content", "setContentLoc", 0);
//      digester.addSetNext("quizquestions/question/question6statement/content", "addQuestions", "com.intelli.ezed.data.QuestionContent");
//
//      digester.addCallMethod("quizquestions/question/correctanswer", "setCorrectAnswer", 0);
//
//      try
//      {
//         QuizDetails quizDetails = new QuizDetails();
//         digester.push(quizDetails);
//         quizDetails = (QuizDetails) digester.parse(new File("C:\\Users\\indraneeld\\Desktop\\samplebulkdata\\contents\\cpquiz\\quiz.xml"));
//         System.out.println(1);
//      }
//      catch (IOException e)
//      {
//         // TODO Auto-generated catch block
//         e.printStackTrace();
//      }
//      catch (SAXException e)
//      {
//         // TODO Auto-generated catch block
//         e.printStackTrace();
//      }
//   }
//
//}
