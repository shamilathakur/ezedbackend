package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.utils.StringSplitter;
import com.intelli.ezed.data.AdminStatusData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.utils.AdminRoles;

public class AdditionalInfoForAdminBasedOnTypeGetter extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public AdditionalInfoForAdminBasedOnTypeGetter(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      AdminStatusData data = (AdminStatusData) flowContext.getFromContext(EzedKeyConstants.ADMIN_STATUS_DATA);
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      String adminRoles = data.getRoll();
      if (adminRoles == null || adminRoles.compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Admin role is empty. No additional data will be fetched");
         }
         response.setResponseObject(data);
         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
         return RuleDecisionKey.ON_SUCCESS;
      }

      String[] roles = StringSplitter.splitString(adminRoles, ",");
      int currentRole = 0;
      Connection conn = null;
      PreparedStatement ps = null;
      ResultSet rs = null;
      for (int i = 0; i < roles.length; i++)
      {
         currentRole = Integer.parseInt(roles[i]);
         if (currentRole == AdminRoles.MIC_ADMIN)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Admin is MIC admin. Trying to fetch MIC specific details");
            }
            try
            {
               conn = DbConnectionManager.getConnectionByName(dbConnName);
               ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "GetMICDetails");
               ps.setString(1, data.getUserid());
               rs = ps.executeQuery();
               if (rs.next())
               {
                  data.setCollege(rs.getString("collegename"));
                  data.setUniversity(rs.getString("univname"));
               }
            }
            catch (SQLException e)
            {
               logger.error("Error while fetching role specific admin details", e);
               response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching role specific admin details");
               return RuleDecisionKey.ON_FAILURE;
            }
            catch (Exception e)
            {
               logger.error("Error while fetching role specific admin details", e);
               response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching role specific admin details");
               return RuleDecisionKey.ON_FAILURE;
            }
            finally
            {
               if (rs != null)
               {
                  try
                  {
                     rs.close();
                  }
                  catch (SQLException e)
                  {
                  }
               }
               DbConnectionManager.releaseConnection(dbConnName, conn);
            }
            continue;
         }
         if (currentRole == AdminRoles.GROUP_ADMIN)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Admin is Group admin. Trying to fetch Group admin specific details");
            }
            try
            {
               conn = DbConnectionManager.getConnectionByName(dbConnName);
               ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchGroupAdminMICDetails");
               ps.setString(1, data.getUserid());
               rs = ps.executeQuery();
               if (rs.next())
               {
                  data.setMicUserId(rs.getString("userid"));
                  data.setMicUserName(rs.getString("name"));
               }
            }
            catch (SQLException e)
            {
               logger.error("Error while fetching role specific admin details", e);
               response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching role specific admin details");
               return RuleDecisionKey.ON_FAILURE;
            }
            catch (Exception e)
            {
               logger.error("Error while fetching role specific admin details", e);
               response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching role specific admin details");
               return RuleDecisionKey.ON_FAILURE;
            }
            finally
            {
               if (rs != null)
               {
                  try
                  {
                     rs.close();
                  }
                  catch (SQLException e)
                  {
                  }
               }
               DbConnectionManager.releaseConnection(dbConnName, conn);
            }
            continue;
         }
      }
      response.setResponseObject(data);
      response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
      return RuleDecisionKey.ON_SUCCESS;
   }
}
