package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.FrontendFetchData;
import com.intelli.ezed.data.Response;

public class CityListFetcher extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String defCityId;

   public CityListFetcher(String ruleName, String dbConnName, String defCityId)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
      this.defCityId = defCityId;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      String[] cityArr = null;
      FrontendFetchData cityData = new FrontendFetchData();
      response.setResponseObject(cityData);
      response.setStatusCodeSend(false);
      ArrayList<String[]> cities = new ArrayList<String[]>();
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchCityList");
         ps.setString(1, defCityId);
         rs = ps.executeQuery();
         int i = 1;
         while (rs.next())
         {
            cityArr = new String[5];
            cityArr[0] = Integer.toString(i);
            cityArr[1] = rs.getString(4);
            cityArr[2] = rs.getString(1);
            cityArr[3] = rs.getString(2);
            cityArr[4] = rs.getString(3);
            i++;
            cities.add(cityArr);
         }
         cityData.setAttribList(cities);
         cityData.setAttribName("aaData");
         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
      }
      catch (SQLException e)
      {
         logger.error("DB error while fetching list of cities", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "DB error while fetching cities");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching list of cities", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching cities");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing result set while fetching city list", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
      return RuleDecisionKey.ON_SUCCESS;
   }
}
