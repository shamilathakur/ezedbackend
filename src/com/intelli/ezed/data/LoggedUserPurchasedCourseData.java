package com.intelli.ezed.data;

public class LoggedUserPurchasedCourseData extends ResponseObject{
	
	private String courseId;
	private String courseName;
	private String videoCount;
	private String chapterCount;
	private String questionCount;
	private String courseImagePath;
	
	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getVideoCount() {
		return videoCount;
	}

	public void setVideoCount(String videoCount) {
		this.videoCount = videoCount;
	}

	public String getChapterCount() {
		return chapterCount;
	}

	public void setChapterCount(String chapterCount) {
		this.chapterCount = chapterCount;
	}

	public String getQuestionCount() {
		return questionCount;
	}

	public void setQuestionCount(String questionCount) {
		this.questionCount = questionCount;
	}

	public String getCourseImagePath() {
		return courseImagePath;
	}

	public void setCourseImagePath(String courseImagePath) {
		this.courseImagePath = courseImagePath;
	}


	
	 @Override
	   public void prepareResponse() throws Exception
	   {
		  this.put("courseId", getCourseId());
		  this.put("courseName", getCourseName());
		  this.put("courseImagePath", getCourseImagePath());
		  this.put("courseId", getVideoCount());
		  this.put("courseId", getChapterCount());
		  this.put("courseId", getQuestionCount());
	   }
}
