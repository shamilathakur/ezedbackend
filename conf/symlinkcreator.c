#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

int main()
{
	char st[1024];
	char oldpath[1024];
	int count = 0;

	while (fgets(st,sizeof(st),stdin) != NULL)
	{
		for (char *cp = st; *cp; cp++)
			if (*cp == '\n' || *cp == '\r') 
				*cp = '\0';

		if (count++ % 2)
			symlink(oldpath,st);
		else
			strcpy(oldpath,st);
	}
}