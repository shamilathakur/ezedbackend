package com.intelli.ezed.bulkupload.data;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.slf4j.Logger;

import com.intelli.ezed.utils.EzedHelper;
import com.intelli.ezed.utils.PrepareCityIdBackend;

public class UniversityDetails
{
   private String country;
   private String state;
   private String city;
   private String cityId;
   private String university;
   private String website;
   private String phoneno;
   private static PrepareCityIdBackend prepareCityId = new PrepareCityIdBackend("CIT2", "ezeddb");
   private static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

   public String getCountry()
   {
      return country;
   }
   public void setCountry(String country)
   {
      this.country = EzedHelper.handleBulkUploadStringUpperCase(country, 50);
   }
   public String getState()
   {
      return state;
   }
   public void setState(String state)
   {
      this.state = EzedHelper.handleBulkUploadStringUpperCase(state, 50);
   }
   public String getCity()
   {
      return city;
   }
   public void setCity(String city)
   {
      this.city = EzedHelper.handleBulkUploadStringUpperCase(city, 50);
   }
   public String getCityId()
   {
      return cityId;
   }
   public void setCityId(String cityId)
   {
      this.cityId = cityId;
   }
   public void setCityIdCalculated(String dbConnName, Logger logger) throws Exception
   {
      this.fetchCityId(dbConnName, logger);
   }
   public String getUniversity()
   {
      return university;
   }
   public void setUniversity(String university)
   {
      this.university = EzedHelper.handleBulkUploadStringUpperCase(university, 200);
   }
   public String getWebsite()
   {
      return website;
   }
   public void setWebsite(String website)
   {
      this.website = EzedHelper.handleBulkUploadStringUpperCase(website, 200);
   }
   public String getPhoneno()
   {
      return phoneno;
   }
   public void setPhoneno(String phoneno)
   {
      this.phoneno = phoneno;
   }

   public void persistUniversity(String dbConnName, Logger logger) throws Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertUniversity");
         ps.setString(1, getCityId());
         ps.setString(2, getUniversity());
         ps.setString(3, getWebsite());
         ps.setString(4, getPhoneno());
         ps.setInt(5, 1);
         ps.setString(6, "bulkupload");
         ps.setInt(7, 0);
         ps.setString(8, sdf.format(new Date()));

         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in db while inserting university");
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while inserting university in db", e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while inserting university in db", e);
         throw e;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   private void fetchCityId(String dbConnName, Logger logger) throws Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      String cityId = null;

      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchCityId");
         ps.setString(1, getCountry());
         ps.setString(2, getState());
         ps.setString(3, getCity());
         rs = ps.executeQuery();
         if (rs.next())
         {
            cityId = rs.getString(1);
            if (cityId == null || cityId.trim().compareTo("") == 0)
            {
               cityId = null;
            }
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching cityId from db", e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching cityId from db", e);
         throw e;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing resultset while fetching city id from db", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      if (cityId == null)
      {
         if (!isCountryPresent(dbConnName, logger))
         {
            persistCountry(dbConnName, logger);
            persistState(dbConnName, logger);
         }
         else
         {
            if (!isStatePresent(dbConnName, logger))
            {
               persistState(dbConnName, logger);
            }
         }
         cityId = fetchNewCityId();
         setCityId(cityId);
         persistCity(dbConnName, logger);
      }
   }

   private String fetchNewCityId()
   {
      return prepareCityId.getNextId();
   }

   private boolean isCountryPresent(String dbConnName, Logger logger) throws Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchCountry");
         ps.setString(1, getCountry());
         rs = ps.executeQuery();
         if (rs.next())
         {
            return true;
         }
         else
         {
            return false;
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching country from db", e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching country from db", e);
         throw e;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   private void persistCountry(String dbConnName, Logger logger) throws Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertCountry");
         ps.setString(1, getCountry());
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in db while inserting country");
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while inserting country in db", e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while inserting country in db", e);
         throw e;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   private boolean isStatePresent(String dbConnName, Logger logger) throws Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchState");
         ps.setString(1, getCountry());
         ps.setString(2, getState());
         rs = ps.executeQuery();
         if (rs.next())
         {
            return true;
         }
         else
         {
            return false;
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching state from db", e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching state from db", e);
         throw e;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   private void persistState(String dbConnName, Logger logger) throws Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertState");
         ps.setString(1, getCountry());
         ps.setString(2, getState());
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in db while inserting state");
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while inserting state in db", e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while inserting state in db", e);
         throw e;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   private void persistCity(String dbConnName, Logger logger) throws Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertCity");
         ps.setString(1, getCountry());
         ps.setString(2, getState());
         ps.setString(3, getCity());
         ps.setString(4, getCityId());
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in db while inserting city");
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while inserting city in db", e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while inserting city in db", e);
         throw e;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   public boolean isUniversityPresent(String dbConnName, Logger logger) throws Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchUniversity");
         ps.setString(1, getUniversity());
         rs = ps.executeQuery();
         if (rs.next())
         {
            return true;
         }
         else
         {
            return false;
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching university from db", e);
         return false;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching university from db", e);
         return false;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   public void printDetails(Logger logger)
   {
      if (logger.isTraceEnabled())
      {
         logger.trace("Country " + getCountry());
         logger.trace("State " + getState());
         logger.trace("City " + getCity());
         logger.trace("university " + getUniversity());
         logger.trace("Website " + getWebsite());
         logger.trace("Ph no " + getPhoneno());
      }
   }

   public void writeToFile(String rootDir, String univFileName) throws IOException
   {
      File file = new File(rootDir + File.separator + getUniversity());
      if (!file.exists())
      {
         file.mkdir();
      }
      File univFile = new File(file.getAbsolutePath() + File.separator + univFileName);
      if (!univFile.exists())
      {
         univFile.createNewFile();
      }
      FileOutputStream fos = null;
      try
      {
         fos = new FileOutputStream(univFile);
         StringBuffer sb = new StringBuffer();
         sb.append("<universitydetails>\n");
         sb.append("<country>");
         sb.append(getCountry());
         sb.append("</country>\n");
         sb.append("<state>");
         sb.append(getState());
         sb.append("</state>\n");
         sb.append("<city>");
         sb.append(getCity());
         sb.append("</city>\n");
         sb.append("<university>");
         sb.append(getUniversity());
         sb.append("</university>\n");
         sb.append("<website>");
         sb.append(getWebsite());
         sb.append("</website>\n");
         sb.append("<phoneno>");
         sb.append(getPhoneno());
         sb.append("</phoneno>\n</universitydetails>");
         fos.write(sb.toString().getBytes());
      }
      finally
      {
         if (fos != null)
         {
            try
            {
               fos.close();
            }
            catch (IOException e)
            {

            }
         }
      }
   }
}
