package com.intelli.ezed.data;

import java.text.SimpleDateFormat;
import java.util.Date;

public class SingleTestDataForGroup extends ResponseObject
{
   private static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
   private String testId;
   private String testName;
   private Date deadLine;
   
   public String getTestId()
   {
      return testId;
   }

   public void setTestId(String testId)
   {
      this.testId = testId;
   }
   
   public String getTestName()
   {
      return testName;
   }

   public void setTestName(String testName)
   {
      this.testName = testName;
   }
   
   public Date getDeadLine()
   {
      return deadLine;
   }

   public void setDeadLine(Date deadLine)
   {
      this.deadLine = deadLine;
   }
   
   @Override
   public void prepareResponse() throws Exception
   {
      this.put("testid", getTestId());
      this.put("testname", getTestName());
      this.put("deadline", sdf.format(getDeadLine()));
   }

}
