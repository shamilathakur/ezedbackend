package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.json.JSONArray;
import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.analytics.data.GenericChartData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.GenericResponseObject;
import com.intelli.ezed.data.Response;

public class RoleSpecificAdminUserFetcher extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;
   private int roleid;

   public RoleSpecificAdminUserFetcher(String ruleName, String dbConnName, String roleid)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
      this.roleid = Integer.parseInt(roleid);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "SelectRoleSpecificAdmins");
         ps.setInt(1, roleid);
         rs = ps.executeQuery();
         GenericChartData data = null;
         JSONArray adminUserList = new JSONArray();
         while (rs.next())
         {
            data = new GenericChartData();
            data.getText().add("name");
            data.getValues().add(rs.getString("name"));
            data.getText().add("id");
            data.getValues().add(rs.getString("userid"));
            adminUserList.put(data);
            data.prepareResponse();
         }
         GenericResponseObject obj = new GenericResponseObject();
         obj.setValue(adminUserList);
         response.setResponseObject(obj);
         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
         return RuleDecisionKey.ON_SUCCESS;
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching admin users", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching list of admin users");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching admin users", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching list of admin users");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {

            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
}
