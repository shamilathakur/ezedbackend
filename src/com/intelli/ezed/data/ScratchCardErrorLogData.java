package com.intelli.ezed.data;

public class ScratchCardErrorLogData {
	private String scratchcardid = null;
	private String courseid = null;
	private String partnerid = null;
	private String userid = null;
	private int errorcode;
	private String errordesc;
	private String errordate;
	private String classname;

	public String getScratchcardid() {
		return scratchcardid;
	}

	public void setScratchcardid(String scratchcardid) {
		this.scratchcardid = scratchcardid;
	}

	public String getCourseid() {
		return courseid;
	}

	public void setCourseid(String courseid) {
		this.courseid = courseid;
	}

	public String getPartnerid() {
		return partnerid;
	}

	public void setPartnerid(String partnerid) {
		this.partnerid = partnerid;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public int getErrorcode() {
		return errorcode;
	}

	public void setErrorcode(int errorcode) {
		this.errorcode = errorcode;
	}

	public String getErrordesc() {
		return errordesc;
	}

	public void setErrordesc(String errordesc) {
		this.errordesc = errordesc;
	}

	public String getErrordate() {
		return errordate;
	}

	public void setErrordate(String errordate) {
		this.errordate = errordate;
	}

	public String getClassname() {
		return classname;
	}

	public void setClassname(String classname) {
		this.classname = classname;
	}

}
