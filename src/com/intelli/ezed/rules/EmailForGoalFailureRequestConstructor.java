package com.intelli.ezed.rules;

import java.util.HashMap;

import org.sabre.io.transport.http.data.HttpTransactionData;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.GoalData;
import com.intelli.ezed.data.UserProfile;

public class EmailForGoalFailureRequestConstructor extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public EmailForGoalFailureRequestConstructor(String ruleName)
   {
      super(ruleName);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      UserProfile profile = (UserProfile) flowContext.getFromContext(EzedKeyConstants.USER_DETAILS);
      if (profile.getGoalsAlertStatus() != null && profile.getGoalsAlertStatus().compareToIgnoreCase("0") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("User has requested not to receive goal alerts");
         }
         return RuleDecisionKey.ON_FAILURE;
      }
      String email = (String) flowContext.getFromContext(EzedKeyConstants.USER_ID);
      GoalData goalData = (GoalData) flowContext.getFromContext(EzedKeyConstants.EXPIRED_GOAL);
      HttpTransactionData transData = new HttpTransactionData();
      transData.setRequestType(HttpTransactionData.POST);
      transData.setParaMap(new HashMap<String, String>());
      String failMessage = goalData.getFailedEmailMessage();
      transData.addParam("emailid", email);
      transData.addParam("message", failMessage);
      transData.addParam("emailtype", "EXPIREDGOALS");
      if (logger.isDebugEnabled())
      {
         logger.debug("Failure mail for goal to be sent to userid " + email + " is " + failMessage);
      }

      flowContext.putIntoContext(HttpTransactionData.WRITE_REQ_DATA, transData);
      return RuleDecisionKey.ON_SUCCESS;
   }

}
