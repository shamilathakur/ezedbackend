package com.intelli.ezed.utils;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedLoggerName;

public class PaymentLockManager
{
   private static ArrayList<String> paymentIdList = new ArrayList<String>();
   private static Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public static void addToList(String paymentId)
   {
      synchronized (paymentIdList)
      {
         while (paymentIdList.contains(paymentId.intern()))
         {
            try
            {
               paymentIdList.wait();
            }
            catch (InterruptedException e)
            {
               if (logger.isDebugEnabled())
               {
                  logger.debug("caught while adding rrn data to rrn list", e);
               }
            }
         }
         paymentIdList.add(paymentId.intern());
         //         paymentId.setLockedState(true);
         if (logger.isDebugEnabled())
         {
            logger.debug("String " + paymentId + " locked");
         }
         //         timedMap.put(paymentId.getLockString(), paymentId);
      }
   }

   public static void removeFromList(String paymentId)
   {
      //      if (paymentId != null && paymentId.isLockedState())
      //      {
      synchronized (paymentIdList)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("String " + paymentId + " unlocked");
         }
         paymentIdList.remove(paymentId.intern());
         //            paymentId.setLockedState(false);
         //            timedMap.remove(paymentId.getLockString());
         paymentIdList.notify();
      }
   }
   //   }
}
