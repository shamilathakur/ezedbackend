package com.intelli.ezed.db;

public class EzedRequestData
{
   public static final String EZED_REQUEST_DATA = "EzedRequestData";

   private String userid;
   private String password;
   private String newPassword;
   private String chapterId;
   
   private String testId;
   private String loginWith;
   private String regWith;
   
   private String userTestAns;
   
   /* 
    * This integer type is used determine the login or registration type of a user.
    * it can take values as follows
    *  0 - registration vi Ezed i.e by entering the userid and password at ezed site.  
    *  1 - registration vi facebook or gmail or any other site.
    */
   private int regLoginType;

   public String getUserTestAns()
   {
      return userTestAns;
   }
   public void setUserTestAns(String userTestAns)
   {
      this.userTestAns = userTestAns;
   }
   public int getRegLoginType()
   {
      return regLoginType;
   }
   public void setRegLoginType(int regLoginType)
   {
      this.regLoginType = regLoginType;
   }
   
   public String getNewPassword()
   {
      return newPassword;
   }
   public void setNewPassword(String newPassword)
   {
      this.newPassword = newPassword;
   }
   public String getRegWith()
   {
      return regWith;
   }
   public void setRegWith(String regWith)
   {
      this.regWith = regWith;
   }
   public String getLoginWith()
   {
      return loginWith;
   }
   public void setLoginWith(String loginWith)
   {
      this.loginWith = loginWith;
   }
   public String getTestId()
   {
      return testId;
   }
   public void setTestId(String testId)
   {
      this.testId = testId;
   }
   public String getChapterId()
   {
      return chapterId;
   }
   public void setChapterId(String chapterId)
   {
      this.chapterId = chapterId;
   }
   public String getUserid()
   {
      return userid;
   }
   public void setUserid(String userid)
   {
      this.userid = userid.toLowerCase();
   }
   public String getPassword()
   {
      return password;
   }
   public void setPassword(String password)
   {
      this.password = password;
   }
}
