package com.intelli.ezed.dropdowns;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.a.helper.utils.StringSplitter;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.dropdowns.data.DropDownListData;
import com.intelli.ezed.dropdowns.data.SingleDropDownData;

public class TestListGetter extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;

   public TestListGetter(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      String courseIdList = messageData.getParameter("courseid");
      if (courseIdList == null || courseIdList.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Course id is invalid " + courseIdList);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid course ids");
         return RuleDecisionKey.ON_FAILURE;
      }

      String[] courseIds = StringSplitter.splitString(courseIdList.trim(), ",");
      Connection conn = null;
      PreparedStatement ps = null;
      ResultSet rs = null;
      DropDownListData dropDownListData = new DropDownListData();
      dropDownListData.setName("testIdMapList");
      Set<String> testIdList = new HashSet<String>();
      String testId = null;

      for (int i = 0; i < courseIds.length; i++)
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         try
         {
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "SelectTestsForCourse");
            ps.setString(1, courseIds[i]);
            ps.setString(2, courseIds[i]);
            rs = ps.executeQuery();
            SingleDropDownData data = null;
            while (rs.next())
            {
               testId = rs.getString("testid");
               if (!testIdList.contains(testId))
               {
                  data = new SingleDropDownData();
                  data.setParam(testId);
                  data.setValue(rs.getString("testname"));
                  dropDownListData.getDropDownDatas().add(data);
               }
            }
         }
         catch (SQLException e)
         {
            logger.error("Error while fetching courses for course id drop downs", e);
            response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while populating course list drop down");
            return RuleDecisionKey.ON_FAILURE;
         }
         catch (Exception e)
         {
            logger.error("Error while fetching courses for course id drop downs", e);
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while populating course list drop down");
            return RuleDecisionKey.ON_FAILURE;
         }
         finally
         {
            if (rs != null)
            {
               try
               {
                  rs.close();
               }
               catch (SQLException e)
               {
                  logger.error("Error while closing result set while populating drop down for courses");
               }
            }
            DbConnectionManager.releaseConnection(dbConnName, conn);
         }
      }

      response.setStatusCodeSend(false);
      response.setResponseObject(dropDownListData);
      response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
      return RuleDecisionKey.ON_SUCCESS;
   }

}
