package com.intelli.ezed.frontend.utils;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicLong;

import org.sabre.db.DbConnectionManager;

import com.a.helper.data.DataFiller;

public class PrepareCartId {

	//keep calling getnextid() to get new note ID's. Note ID's will be
	   //in the format NOT1, NOT2, NOT3, and so on...
	   private AtomicLong atomicLong = null;
	   private Connection connection=null;
	   private ResultSet resultSet =null;
	   static PrepareCartId genId=null;
	   
	   
	   private PrepareCartId()
	   {
	      long initialValue = getStartUpTransactionId();
	      if (initialValue == 0)
	      {
	         initialValue = (System.currentTimeMillis() / 10000);
	      }
	      atomicLong = new AtomicLong(initialValue);
	   }
	   
	   public static PrepareCartId getPrepareCartId()
	   {
		   if(genId==null)
		   {
			   genId=new PrepareCartId();
			   return genId;
			}else{
			   return genId;
		   }
			   
		   
	   }

	   public PrepareCartId(Connection connection)
	   {
		  this.connection=connection;
		  
	      long initialValue = getStartUpTransactionId();
	      if (initialValue == 0)
	      {
	         initialValue = (System.currentTimeMillis() / 10000);
	      }
	      atomicLong = new AtomicLong(initialValue);
	   }

	   private long getStartUpTransactionId()
	   {
	      //change how you get your connection here.
	      if (connection == null)
	      {
	         return 0;
	      }

	      //change how you get your prepared statement here.
	      PreparedStatement preparedStatement = getStatement(connection);
	      if (preparedStatement == null)
	      {
	         return 0;
	      }
	      try
	      {
	         ResultSet resultSet = preparedStatement.executeQuery();
	         if (resultSet.next())
	         {
	            long value = resultSet.getLong(1);
	            resultSet.close();
	            resultSet = null;

	            if (value == 0)
	            {
	               value = 1;
	               return value;
	            }

	            return value + 1;
	         }
	         return 1;
	      }
	      catch (Exception e)
	      {
	         return 0;
	      }
	      finally
	      {
	         //if there is any code to release the prepared statements
	         //or connections back to pool, write them here.
	         try
	         {
	            preparedStatement.close();
	           // resultSet.close();
	         }
	         catch (SQLException e)
	         {
	         }
	         try
	         {
	          //  connection.close();
	         }
	         catch (Exception e)
	         {
	         }
	      }
	   }


	   public String getCartId()
	   {
	      return "CAR" + Long.toString(this.atomicLong.incrementAndGet());
	   }
	   
	  	  /* private Connection getDbConnection()
	   {
	      try
	      {
	         String url = "jdbc:postgresql://localhost:5432/ezed";
	         Class.forName("org.postgresql.Driver");
	         return DriverManager.getConnection(url, "postgres", "postgres");
	      }
	      catch (Exception e)
	      {
	         return null;
	      }
	   }*/

	   private static PreparedStatement getStatement(Connection conn)
	   {
	      try
	      {
	         return (conn.prepareStatement("select max(noteid) from mynotes"));
	      }
	      catch (Exception e)
	      {
	         return null;
	      }
	   }
}
