package com.intelli.ezed.frontend.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedLoggerName;

public class CollegeDatabaseInserter
{
   //use your own logger name at this position
   private String loggerName = EzedLoggerName.SERVICE_LOG;
   private Logger logger = LoggerFactory.getLogger(loggerName);

   private String website;
   private String univname;
   private String phonenumber;
   private String collegename;

   public String getWebsite()
   {
      return website;
   }
   public void setWebsite(String website)
   {
      this.website = website;
   }
   public String getUnivname()
   {
      return univname;
   }
   public void setUnivname(String univname)
   {
      this.univname = univname;
   }
   public String getPhonenumber()
   {
      return phonenumber;
   }
   public void setPhonenumber(String phonenumber)
   {
      this.phonenumber = phonenumber;
   }
   public String getCollegeName()
   {
      return collegename;
   }
   public void setCollegeName(String collegeName)
   {
      this.collegename = collegeName;
   }
   
   
   public int deleteFromDatabase()
   {
      //change how you get your connection here. you can use your own database connection pool or whatever.
      Connection connection = getDbConnection();
      if (connection == null)
      {
         return EzedFrontEndTransactionCodes.NO_CONNECTION;
      }

      //change how you get your prepared statement here. you can put the connection in some configuration files for any other setup.
      PreparedStatement preparedStatement = getDeleteStatement(connection);
      if (preparedStatement == null)
      {
         return EzedFrontEndTransactionCodes.NO_STATEMENT;
      }

      try
      {
         preparedStatement.setString(1, getCollegeName());
         int i = preparedStatement.executeUpdate();
         
         if (i == 1)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("collage delete from database.");
            }
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("no data delete from database status : " + i);
            }
            return EzedFrontEndTransactionCodes.NO_ROWS_INSERTED;
         }

      }
      catch (SQLException e)
      {
         logger.error("Database exception occoured while deleting college details from database. Collage might be referaced somewhere.", e);
         return EzedFrontEndTransactionCodes.DB_EXCEPTION_OCCURRED;
      }
      catch (Exception e)
      {
         logger.error("Exception occoured while getting college details from database. ", e);
         return EzedFrontEndTransactionCodes.INTERNAL_ERROR;
      }
      return EzedFrontEndTransactionCodes.SUCCESS;
   }

   public int fetchFromDatabase()
   {
      //change how you get your connection here. you can use your own database connection pool or whatever.
      Connection connection = getDbConnection();
      if (connection == null)
      {
         return EzedFrontEndTransactionCodes.NO_CONNECTION;
      }

      //change how you get your prepared statement here. you can put the connection in some configuration files for any other setup.
      PreparedStatement preparedStatement = getFetchStatement(connection);
      if (preparedStatement == null)
      {
         return EzedFrontEndTransactionCodes.NO_STATEMENT;
      }

      ResultSet resultSet = null;

      try
      {
         preparedStatement.setString(1, getCollegeName());
         resultSet = preparedStatement.executeQuery();

         if (resultSet != null && resultSet.next())
         {
            setUnivname(resultSet.getString("univname"));
            setPhonenumber(resultSet.getString("phonenumber"));
            setWebsite(resultSet.getString("website"));
         }
         else
         {
            if (logger.isInfoEnabled())
            {
               logger.info("No data available for college Name : " + getCollegeName());
            }
            return EzedFrontEndTransactionCodes.INVALID_OPRATION;
         }

      }
      catch (SQLException e)
      {
         logger.error("Database exception occoured while fetching college details from database. ", e);
         return EzedFrontEndTransactionCodes.DB_EXCEPTION_OCCURRED;
      }
      catch (Exception e)
      {
         logger.error("Exception occoured while getting college details from database. ", e);
         return EzedFrontEndTransactionCodes.INTERNAL_ERROR;
      }
      finally
      {
         try
         {
            resultSet.close();
         }
         catch (SQLException e)
         {
            logger.error("Exception occoured while closing the resultset", e);
         }
      }
      return EzedFrontEndTransactionCodes.SUCCESS;
   }

   public int insertIntoDatabase()
   {
      //change how you get your connection here. you can use your own database connection pool or whatever.
      Connection connection = getDbConnection();
      if (connection == null)
      {
         return EzedFrontEndTransactionCodes.NO_CONNECTION;
      }

      //change how you get your prepared statement here. you can put the connection in some configuration files for any other setup.
      PreparedStatement preparedStatement = getInsertStatement(connection);
      if (preparedStatement == null)
      {
         return EzedFrontEndTransactionCodes.NO_STATEMENT;
      }

      //website,univname,phonenumber,cityid

      try
      {
         preparedStatement.setString(1, getWebsite());
         preparedStatement.setString(2, getUnivname().toUpperCase());
         preparedStatement.setString(3, getPhonenumber());
         preparedStatement.setString(4, getCollegeName());

         int i = preparedStatement.executeUpdate();

         if (i == 1)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("collage added in database.");
            }
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("no data inserted in database status : " + i);
            }
            return EzedFrontEndTransactionCodes.NO_ROWS_INSERTED;
         }

      }
      catch (SQLException e)
      {
         System.out.println(e);
         if (e.getMessage().contains("foreign key constraint"))
         {
            if (logger.isInfoEnabled())
            {
               logger.info("University name provided is not present in the system.");
            }
            return EzedFrontEndTransactionCodes.INVALID_MESSAGE_PARAMETER;
         }

         logger.error("Database exception occoured while adding college name into the database.", e);
         return EzedFrontEndTransactionCodes.DB_EXCEPTION_OCCURRED;

      }
      catch (Exception e)
      {
         logger.error("Exception occourd while inserting college details in database.", e);
         return EzedFrontEndTransactionCodes.INTERNAL_ERROR;
      }


      return EzedFrontEndTransactionCodes.SUCCESS;
   }

   public int persisteIntoDatabase()
   {
      //change how you get your connection here. you can use your own database connection pool or whatever.
      Connection connection = getDbConnection();
      if (connection == null)
      {
         return EzedFrontEndTransactionCodes.NO_CONNECTION;
      }

      //change how you get your prepared statement here. you can put the connection in some configuration files for any other setup.
      PreparedStatement preparedStatement = getUpdateStatement(connection);
      if (preparedStatement == null)
      {
         return EzedFrontEndTransactionCodes.NO_STATEMENT;
      }

      //website,univname,phonenumber,cityid

      try
      {
         preparedStatement.setString(1, getWebsite());
         preparedStatement.setString(2, getPhonenumber());
         preparedStatement.setString(3, getUnivname());
         preparedStatement.setString(4, getCollegeName());

         boolean i = preparedStatement.execute();

         if (i == true)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Course added in database.");
            }
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("no data inserted in database status : " + i);
            }
            return EzedFrontEndTransactionCodes.NO_ROWS_INSERTED;
         }

      }
      catch (SQLException e)
      {
         System.out.println(e);
         if (e.getMessage().contains("foreign key constraint"))
         {
            if (logger.isInfoEnabled())
            {
               logger.info("City id provided is not present in the system.");
            }
            return EzedFrontEndTransactionCodes.INVALID_MESSAGE_PARAMETER;
         }

         logger.error("Database exception occoured while adding city name into the database.", e);
         return EzedFrontEndTransactionCodes.DB_EXCEPTION_OCCURRED;

      }
      catch (Exception e)
      {
         System.out.println(e);
         logger.error("Exception occourd while inserting univeristy details in database.", e);
         return EzedFrontEndTransactionCodes.INTERNAL_ERROR;
      }
      return EzedFrontEndTransactionCodes.SUCCESS;
   }

   private Connection getDbConnection()
   {
      try
      {
         String url = "jdbc:postgresql://localhost:5432/ezed2";
         Class.forName("org.postgresql.Driver");
         return DriverManager.getConnection(url, "postgres", "postgres");
      }
      catch (Exception e)
      {
         return null;
      }
   }

   private PreparedStatement getInsertStatement(Connection conn)
   {
      try
      {
         return (conn.prepareStatement("insert into collegelist (website,univname,phonenumber,collegename) values (?,?,?,?)"));
      }
      catch (Exception e)
      {
         return null;
      }
   }

   private PreparedStatement getUpdateStatement(Connection conn)
   {
      try
      {
         return (conn.prepareStatement("update collegelist set website = ?,phonenumber = ?,univname = ? where collegename=?"));
      }
      catch (Exception e)
      {
         return null;
      }
   }

   private PreparedStatement getFetchStatement(Connection conn)
   {
      try
      {
         return (conn.prepareStatement("select * from collegelist where collegename = ?"));
      }
      catch (Exception e)
      {
         return null;
      }
   }
   
   private PreparedStatement getDeleteStatement(Connection conn)
   {
      try
      {
         return (conn.prepareStatement("delete from collegelist where collegename = ?"));
      }
      catch (Exception e)
      {
         return null;
      }
   }
   
   
   

   public static void main(String[] args)
   {
      CollegeDatabaseInserter collegeDatabaseInserter =  new CollegeDatabaseInserter();
      
      collegeDatabaseInserter.setCollegeName("TESTCOLL");
//      collegeDatabaseInserter.setPhonenumber("23421342");
//      collegeDatabaseInserter.setUnivname("MUMBAI UNIVERSITY");
//      collegeDatabaseInserter.setWebsite("www.uiyfgsdf.com");
//      
//      collegeDatabaseInserter.insertIntoDatabase();
      
      
      //collegeDatabaseInserter.fetchFromDatabase();
      
      //collegeDatabaseInserter.setWebsite("NEWTESTCOLL");
      
      //collegeDatabaseInserter.persisteIntoDatabase();
      
      collegeDatabaseInserter.deleteFromDatabase();
      
   }
   
   
}
