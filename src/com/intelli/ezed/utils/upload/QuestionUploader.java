package com.intelli.ezed.utils.upload;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

public class QuestionUploader
{

   /**
    * @param args
    */
   public static void main(String[] args)
   {
      // TODO Auto-generated method stub

   }

   private static Connection getDbConnectionPostgres()
   {
      try
      {
         String url = "jdbc:postgresql://localhost:5432/ezed";
         Class.forName("org.postgresql.Driver");
         return DriverManager.getConnection(url, "postgres", "postgres");
      }
      catch (Exception e)
      {
         e.printStackTrace();
         System.out.println("Error initialising Postgres connection");
         System.exit(1);
         return null;
      }
   }

   private static PreparedStatement getSelectPostStatement(Connection conn)
   {
      try
      {
         return (conn.prepareStatement("insert into quizquestionlist values ('CON20000000137036704',null,null,1,1,'1','',null,1,1,1,1,0,0,'The frictional force and self-weight','The frictional force and normal reaction','Applied force and frictional force','Normal reaction and resultant reaction',null,null,null,null,null,null,null,null,'4',null);"));
      }
      catch (Exception e)
      {
         System.out.println("Error initializing postgres statment");
         System.exit(1);
         return null;
      }
   }
}
