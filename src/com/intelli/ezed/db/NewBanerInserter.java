package com.intelli.ezed.db;

import java.sql.SQLException;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.BannerData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class NewBanerInserter extends SingletonRule
{

   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public NewBanerInserter(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      BannerData bannerData = new BannerData();

      String temp = messageData.getParameter("bannertitle").trim();
      if (temp == null || temp.equals("") || temp.equals(" "))
      {
         logger.debug("bannertitle invalid.");
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Please check bannertitle");
         return RuleDecisionKey.ON_FAILURE;
      }
      bannerData.setBannertitle(temp);

      temp = messageData.getParameter("bannerdesc");
      if (temp == null || temp.trim().equals(""))
      {
         logger.debug("bannerdesc invalid.");
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Please check bannerdesc");
         return RuleDecisionKey.ON_FAILURE;
      }
      bannerData.setBannerdesc(temp.trim());

      String bannerImage = messageData.getParameter("bannerdata");
      if (bannerImage == null || bannerImage.equals("") || bannerImage.equals(" "))
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Banner data is not provided");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Please mention either banner video or banner image or banner youtube");
         return RuleDecisionKey.ON_FAILURE;
      }

      int type = 0;
      try
      {
         type = Integer.parseInt(messageData.getParameter("bannertype"));
         if (type != BannerData.BANNER_IMAGE_TYPE && type != BannerData.BANNER_VIDEO_TYPE && type != BannerData.BANNER_YOUTUBE_TYPE)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Banner type is invalid");
            }
            response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid banner type");
            return RuleDecisionKey.ON_FAILURE;
         }
      }
      catch (Exception e)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Banner type is invalid");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid banner type");
         return RuleDecisionKey.ON_FAILURE;
      }

      bannerData.setBannerimage(bannerImage);
      bannerData.setBannerType(type);
      temp = messageData.getParameter("bannerstatus").trim();
      if (temp == null || temp.equals("") || temp.equals(" "))
      {
         logger.debug("bannerstatus invalid.");
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Please check bannerstatus");
         return RuleDecisionKey.ON_FAILURE;
      }
      bannerData.setBannerstatus(temp);

      try
      {
         int i = bannerData.insertIntoDatabase(dbConnName);
         if (i != 0)
         {
            logger.error("No insertion in database.");
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal Error. Please try again later.");
            return RuleDecisionKey.ON_FAILURE;
         }
      }
      catch (SQLException e)
      {
         logger.error("Database Exception Occoured.", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Database Exception please try again later.");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Exception Occoured.", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal error. Please try again later.");
         return RuleDecisionKey.ON_FAILURE;
      }
      response.setParameters(EzedTransactionCodes.SUCCESS, "Banner data inserted into database.");
      return RuleDecisionKey.ON_SUCCESS;
   }
}
