package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.a.helper.utils.RandomNumberGenerator;
import com.a.helper.utils.StringSplitter;
import com.intelli.ezed.data.AdminProfile;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.RegistrationResponse;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.utils.AdminRoles;

public class RegisterAdminUser extends SingletonRule
{

   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;
   private static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

   public RegisterAdminUser(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      AdminProfile adminProfile = (AdminProfile) flowContext.getFromContext(EzedKeyConstants.ADMIN_PROFILE);

      String newUserEmailId = messageData.getParameter("newemailid");
      String[] role = StringSplitter.splitString(messageData.getParameter("role"), ",");
      String password = messageData.getParameter("password");
      String name = messageData.getParameter("name");

      for (int i = 0; i < role.length; i++)
      {
         if (Integer.parseInt(role[i]) == 10)
         {
            String univName = messageData.getParameter("univname");
            if (univName == null || univName.trim().compareTo("") == 0)
            {
               if (logger.isDebugEnabled())
               {
                  logger.debug("University name is invalid " + univName);
               }
               response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid University");
               return RuleDecisionKey.ON_FAILURE;
            }
            String collegeName = messageData.getParameter("collegename");
            if (collegeName == null || collegeName.trim().compareTo("") == 0)
            {
               if (logger.isDebugEnabled())
               {
                  logger.debug("College name is invalid " + collegeName);
               }
               response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid College");
               return RuleDecisionKey.ON_FAILURE;
            }
            break;
         }
      }

      if (password == null)
      {
         String defaultPass = RandomNumberGenerator.generateRandomHex(7);
         password = defaultPass;
      }

      Connection connection = DbConnectionManager.getConnectionByName(dbConnName);

      try
      {
         //insert into adminusers (userid,chkflag,password) values (?,?,?); 
         PreparedStatement preparedStatement = DbConnectionManager.getPreparedStatement(dbConnName, connection, "InsertIntoAdminUsers");
         preparedStatement.setString(1, newUserEmailId);
         preparedStatement.setInt(2, 0);
         preparedStatement.setString(3, password);
         preparedStatement.setString(4, name);
         preparedStatement.setString(5, sdf.format(new Date()));
         preparedStatement.setString(6, adminProfile.getUserid());

         int i = preparedStatement.executeUpdate();

         if (i != 1)
         {
            logger.error("No Rowes inserted into database.");
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal error please try again.");
            return RuleDecisionKey.ON_FAILURE;
         }

         if (logger.isDebugEnabled())
         {
            logger.debug("Added rows in database : " + i);
         }

      }
      catch (SQLException e)
      {
         logger.error("Exception occoured while registring the admin user " + newUserEmailId, e);
         if (e.getMessage().contains("adminuser_pk1"))
         {
            if (logger.isInfoEnabled())
            {
               logger.info("userid " + newUserEmailId + " already registred.");
            }
            response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Userid already registered.");
            return RuleDecisionKey.ON_FAILURE;
         }

         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Internal Error. Please try again.");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Exception occoured while registring the admin user " + newUserEmailId, e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal Error. Please try again.");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, connection);
      }

      for (int i = 0; i < role.length; i++)
      {
         try
         {
            connection = DbConnectionManager.getConnectionByName(dbConnName);
            PreparedStatement ps = DbConnectionManager.getPreparedStatement(dbConnName, connection, "InsertAdminRoles");
            ps.setString(1, newUserEmailId);
            ps.setInt(2, Integer.parseInt(role[i]));
            int rows = ps.executeUpdate();
            if (logger.isDebugEnabled())
            {
               logger.debug(rows + " rows inserted in db while inserting roles for user id " + newUserEmailId);
            }
         }
         catch (SQLException e)
         {
            logger.error("Error while inserting role for admin " + newUserEmailId, e);
            response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while processing user role");
            return RuleDecisionKey.ON_FAILURE;
         }
         catch (Exception e)
         {
            logger.error("Error while inserting role for admin " + newUserEmailId, e);
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while processing user role");
            return RuleDecisionKey.ON_FAILURE;
         }
         finally
         {
            DbConnectionManager.releaseConnection(dbConnName, connection);
         }
      }


      //Generating a Hex Key for verification
      String registrationVerificationKey = RandomNumberGenerator.generateRandomHex(15);
      String requestInTime = (String) flowContext.getFromContext(EzedKeyConstants.REQUEST_IN_TIME);
      connection = DbConnectionManager.getConnectionByName(dbConnName);

      try
      {
         //insert into userverif (userid,verifid,intime) values (?,?,?)
         PreparedStatement insertIntoUserverif = DbConnectionManager.getPreparedStatement(dbConnName, connection, "InsertInUserverif");
         insertIntoUserverif.setString(1, newUserEmailId);
         insertIntoUserverif.setString(2, registrationVerificationKey);
         insertIntoUserverif.setString(3, requestInTime);
         int i = insertIntoUserverif.executeUpdate();

         if (i == 0)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("No rows inserted into table. RegisterUSer.java");
            }
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal Error. Please try again.");
            return RuleDecisionKey.ON_FAILURE;
         }

      }
      catch (SQLException e)
      {
         logger.error("Database Exception.", e);
         if (e.getMessage().contains("userverif_uniq"))
         {
            logger.debug("Need to replace the verification key");
            try
            {
               //update userverif set verifid = ?, intime = ? where userid = ?
               PreparedStatement updateIntoUserverif = DbConnectionManager.getPreparedStatement(dbConnName, connection, "UpdateInUserverif");
               updateIntoUserverif.setString(1, registrationVerificationKey);
               updateIntoUserverif.setString(2, requestInTime);
               updateIntoUserverif.setString(3, newUserEmailId);
               int j = updateIntoUserverif.executeUpdate();
               if (j == 0)
               {
                  if (logger.isDebugEnabled())
                  {
                     logger.debug("No rows inserted into table while updating userveirf table. RegisterUSer.java");
                  }
                  response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal Error. Please try again.");
                  return RuleDecisionKey.ON_FAILURE;
               }
            }
            catch (Exception e1)
            {
               logger.error("error", e1);
               response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Database exception occourred. Please try again.");
               return RuleDecisionKey.ON_FAILURE;
            }
         }
         else
         {
            response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Database exception occourred. Please try again.");
            return RuleDecisionKey.ON_FAILURE;
         }
      }
      catch (Exception e)
      {
         logger.error("Exception.", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal Error. Please try again.");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, connection);
      }

      RegistrationResponse registrationResponse = new RegistrationResponse();
      registrationResponse.setRegistrationVerificaitonKey(registrationVerificationKey);
      registrationResponse.setDefaultPassword(password);
      response.setResponseObject(registrationResponse);
      response.setParameters(EzedTransactionCodes.SUCCESS, "User registered successfully.");

      flowContext.putIntoContext(EzedKeyConstants.ADMIN_USER_ID, newUserEmailId);
      flowContext.putIntoContext(EzedKeyConstants.ADMIN_ROLE, role);
      int roleInt = 0;
      for (int i = 0; i < role.length; i++)
      {
         roleInt = Integer.parseInt(role[i]);
         if (roleInt == AdminRoles.MIC_ADMIN)
         {
            return RuleDecisionKey.IF_MIC;
         }
         if (roleInt == AdminRoles.GROUP_ADMIN)
         {
            return RuleDecisionKey.IF_GROUP;
         }
      }
      return RuleDecisionKey.ON_SUCCESS;
   }
}
