package com.intelli.ezed.rules;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.PaymentData;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.utils.PaymentLockManager;

public class AddCartRequestParser extends SingletonRule {
	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

	public AddCartRequestParser(String ruleName) {
		super(ruleName);
	}

	@Override
	public RuleDecisionKey executeRule(FlowContext flowContext) {
		MessageData messageData = (MessageData) flowContext
				.getFromContext(MessageData.class.getName());
		Response response = (Response) flowContext
				.getFromContext(EzedKeyConstants.EZED_RESPONSE);

		String paymentId = messageData.getParameter("paymentid");
		
		/** Added by kunal for reusing the flow in scratch card module */
		if (paymentId == null || paymentId.trim().compareTo("") == 0) {
			paymentId = (String) flowContext.getFromContext("paymentid");
		}

		if (paymentId == null || paymentId.trim().compareTo("") == 0) {
			if (logger.isDebugEnabled()) {
				logger.debug("Payment id received in update request is invalid");
			}
			response.setParameters(
					EzedTransactionCodes.INVALID_MESSAGE_PARAMETER,
					"Invalid payment id");
			return RuleDecisionKey.ON_FAILURE;
		}

		String appId = null;

		if (paymentId.length() >= 12) {
			appId = paymentId.substring(0, 2);
			paymentId = paymentId.substring(2);
		} else {
			if (logger.isDebugEnabled()) {
				logger.debug("Payment id received in update request is not of sufficient length");
			}
			response.setParameters(
					EzedTransactionCodes.INVALID_MESSAGE_PARAMETER,
					"Invalid payment id");
			return RuleDecisionKey.ON_FAILURE;
		}

		PaymentData payData = new PaymentData();
		payData.setAppId(appId);
		payData.setPaymentId(paymentId);

		flowContext.putIntoContext(EzedKeyConstants.PAYMENT_DATA, payData);
		// taking lock on payment id
		PaymentLockManager.addToList(payData.getAppId()
				+ payData.getPaymentId());
		return RuleDecisionKey.ON_SUCCESS;
	}
}
