package com.intelli.ezed.utils;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import com.a.helper.data.DataFiller;
import com.a.helper.session.SessionIdGenerator;

public class SymlinkNameGenerator
{
   private String seqNoGenName;
   private int linkSize;
   private SimpleDateFormat sdf;

   public SymlinkNameGenerator(String seqNoGenName, int linkSize)
   {
      this.seqNoGenName = seqNoGenName;
      this.linkSize = linkSize;
      this.sdf = new SimpleDateFormat("yyyyMMddHHmmss");
   }

   public String getSymLinkName()
   {
      StringBuffer sb = new StringBuffer();
      String seqNoPart = SessionIdGenerator.getSessionId(seqNoGenName);
      if (seqNoPart.length() <= linkSize - 14)
      {
         seqNoPart = DataFiller.getLeftZeroPadding(seqNoPart, linkSize - 14);
      }
      else
      {
         seqNoPart = seqNoPart.substring(seqNoPart.length() - (linkSize - 14));
      }
      //      String seqNoPart = DataFiller.getLeftZeroPadding(SessionIdGenerator.getSessionId(seqNoGenName), linkSize - 14);
      String datePart = sdf.format(new Date(System.currentTimeMillis()));
      sb.append(datePart).append(seqNoPart);
      String fullSymlink = sb.toString();
      if (fullSymlink.length() > linkSize)
      {
         return fullSymlink.substring(0, linkSize - 1);
      }
      else
      {
         return DataFiller.getLeftZeroPadding(fullSymlink, linkSize);
      }
   }
   public String getEncryptedSymLinkName() throws NumberFormatException, IllegalBlockSizeException, BadPaddingException
   {
      return SymlinkEncryptionHandler.getEncryptedKey(getSymLinkName());
   }

   public static void main(String[] args) throws InvalidKeyException, NumberFormatException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException
   {
      SymlinkEncryptionHandler.initializeCipher();
      SymlinkNameGenerator generator = new SymlinkNameGenerator("abc", 32);
      for (int i = 0; i < 200; i++)
      {
         System.out.println(SymlinkEncryptionHandler.getEncryptedKey(generator.getSymLinkName()));
      }
   }
}
