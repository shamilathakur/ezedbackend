package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class StateAddDeleter extends SingletonRule
{

   private static final int ADD_STATE = 1;
   private static final int MOD_STATE = 2;
   private static final int DEL_STATE = 3;

   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;
   private int actionType;

   public StateAddDeleter(String ruleName, String dbConnName, String actionType)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
      this.actionType = Integer.parseInt(actionType);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      String countryName = messageData.getParameter("country");
      String stateName = messageData.getParameter("state");
      
      if (countryName == null || countryName.equals("") || countryName.equals(" "))
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("country Name not provided.");
         }
         response.setParameters(EzedTransactionCodes.WRONG_COUNTRY, "Please enter correct country Name.");
         return RuleDecisionKey.ON_FAILURE;
      }
      
      if (stateName == null || stateName.equals("") || stateName.equals(" "))
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("state Name not provided.");
         }
         response.setParameters(EzedTransactionCodes.WRONG_STATE, "Please enter correct state Name.");
         return RuleDecisionKey.ON_FAILURE;
      }

      Boolean status = false;

      if (actionType == ADD_STATE)
      {
         status = addState(countryName, stateName, response);
      }
      else if (actionType == DEL_STATE)
      {
         status = deleteState(countryName, stateName, response);
      }
      //CB0023
      else if (actionType == MOD_STATE)
      {
         String oldCountryName = messageData.getParameter("oldcountry");
         String oldState = messageData.getParameter("oldstate");
         
         if (oldCountryName == null || oldCountryName.equals("") || oldCountryName.equals(" "))
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("old country Name not provided.");
            }
            response.setParameters(EzedTransactionCodes.WRONG_COUNTRY, "Please enter correct old country Name.");
            return RuleDecisionKey.ON_FAILURE;
         }
         
         if (oldState == null || oldState.equals("") || oldState.equals(" "))
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("old state Name not provided.");
            }
            response.setParameters(EzedTransactionCodes.WRONG_STATE, "Please enter correct old state Name.");
            return RuleDecisionKey.ON_FAILURE;
         }
         
         status = deleteState(oldCountryName, oldState, response);
         if (status == true)
         {
            status = addState(countryName, stateName, response);
         }
      }


      if (status == true)
      {
         return RuleDecisionKey.ON_SUCCESS;
      }
      else
      {
         return RuleDecisionKey.ON_FAILURE;
      }
   }

   private Boolean deleteState(String countryName, String stateName, Response response)
   {
      Connection connection = DbConnectionManager.getConnectionByName(dbConnName);

      try
      {
         //Delete from statelist where statename = ? and countryname = ?
         PreparedStatement deleteState = DbConnectionManager.getPreparedStatement(dbConnName, connection, "DeleteStateFromDb");
         deleteState.setString(1, stateName);
         deleteState.setString(2, countryName);
         int i = deleteState.executeUpdate();

         if (logger.isDebugEnabled())
         {
            logger.debug("Number of rows deleted : " + i + " while deleting state : " + stateName + "," + countryName);
         }
         if (i == 0)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("No state found in db with state name : " + stateName + "," + countryName);
            }
            response.setParameters(EzedTransactionCodes.SUCCESS, stateName + " not found in system");
            return true;
         }

      }
      catch (SQLException e)
      {
         //e.printStackTrace();
         if (e.getMessage().contains("foreign key constraint"))
         {
            if (logger.isInfoEnabled())
            {
               logger.info("Can not delete state : " + stateName + " from database it is refereced from some other table.", e);
            }
            response.setParameters(EzedTransactionCodes.INVALID_OPRATION, "State is being used in application can not delete.");
         }
         else
         {
            logger.error("Database exception occoured.", e);
            response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Database exception occoured while deleting the state.");
         }
         return false;
      }
      catch (Exception e)
      {
         logger.error("exception occoured.", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal Error while adding the state.");
         return false;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, connection);
      }

      response.setParameters(EzedTransactionCodes.SUCCESS, "State : " + stateName + " deleted from database.");
      return true;
   }

   private Boolean addState(String countryName, String stateName, Response response)
   {
      Connection connection = DbConnectionManager.getConnectionByName(dbConnName);

      try
      {
         //insert into statelist (countryname,statename) values (?,?)
         PreparedStatement addState = DbConnectionManager.getPreparedStatement(dbConnName, connection, "AddStateInDb");
         addState.setString(1, countryName);
         addState.setString(2, stateName);
         int i = addState.executeUpdate();

         if (logger.isDebugEnabled())
         {
            logger.debug(i + " rows added in database.");
         }

      }
      catch (SQLException e)
      {
         if (e.getMessage().contains("foreign key constraint"))
         {
            if (logger.isInfoEnabled())
            {
               logger.info("Invalid contry name provided. ", e);
            }
            response.setParameters(EzedTransactionCodes.WRONG_COUNTRY, "Country name not present in system.");
            return false;
         }
         else if (e.getMessage().contains("unique constraint"))
         {
            if (logger.isInfoEnabled())
            {
               logger.info("State name already present in system.", e);
            }
            response.setParameters(EzedTransactionCodes.DUPLICATE_STATE, "State name already added in system.");
            return false;
         }

         logger.error("Database exception occoured while trying to add state");
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Database exception occoured while trying to add state.");
         return false;
      }
      catch (Exception e)
      {
         logger.error("exception occoured.", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal Error while adding the state.");
         return false;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, connection);
      }

      response.setParameters(EzedTransactionCodes.SUCCESS, "State : " + stateName + " added to database.");
      return true;
   }
}
