package com.intelli.ezed.rules;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.bulkupload.data.ActiveSessionList;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;

public class ActiveSessionRemover extends SingletonRule
{

   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   public ActiveSessionRemover(String ruleName)
   {
      super(ruleName);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      String userId = (String) flowContext.getFromContext(EzedKeyConstants.USER_ID);
      
      ActiveSessionList.removeFromActiveSession(userId);
      if (logger.isDebugEnabled())
      {
         logger.debug("Session id " + userId + " removed from list of active sessions");
      }
      return RuleDecisionKey.ON_SUCCESS;
   }
}
