package com.ezed.utils.dbutil;

public class DataFiller
{
   //indra
   static String[] spacesString = new String[51];
   static String[] zerosString = new String[51];
   static String[] hashString = new String[16];
   static String[] starString = new String[16];

   static
   {
      spacesString[0] = "";
      zerosString[0] = "";
      hashString[0] = "";
      starString[0] = "";

      StringBuffer buffer = new StringBuffer("");
      buffer = new StringBuffer(" ");
      for (int i = 1; i <= 50; i++)
      {
         spacesString[i] = buffer.toString();
         buffer.append(" ");
      }

      buffer = new StringBuffer("0");
      for (int i = 1; i <= 50; i++)
      {
         zerosString[i] = buffer.toString();
         buffer.append("0");
      }

      buffer = new StringBuffer("#");
      for (int i = 1; i <= 15; i++)
      {
         hashString[i] = buffer.toString();
         buffer.append("#");
      }
      buffer = new StringBuffer("#");
      for (int i = 1; i <= 15; i++)
      {
         starString[i] = buffer.toString();
         buffer.append("#");
      }
   }

   public static String getZeros(int i)
   {
      return zerosString[i];
   }

   public static String getSpaces(int i)
   {
      return spacesString[i];
   }

   public static String getRightZeroPadding(String s, int length)
   {
      if (s != null)
      {
         if (s.length() > length)
         {
            return s.substring(0, length);
         }
         else
         {
            return (s += getZeros(length - s.length()));
         }

      }
      else
      {
         return getZeros(length);
      }
   }

   public static String getRightSpacePadding(String s, int length)
   {
      if (s != null)
      {
         if (s.length() > length)
         {
            return s.substring(0, length);
         }
         else
         {
            return (s += getSpaces(length - s.length()));
         }
      }
      else
      {
         return getSpaces(length);
      }
   }

   public static String getLeftSpacePadding(String s, int length)
   {
      if (s != null)
      {
         if (s.length() > length)
         {
            return s.substring(0, length);
         }
         else
         {
            return (getSpaces(length - s.length()) + s);
         }
      }
      else
      {
         return getSpaces(length);
      }
   }

   public static String getLeftZeroPadding(String s, int length)
   {
      if (s != null)
      {
         if (s.length() > length)
         {
            return s.substring(0, length);
         }
         else
         {
            return (getZeros(length - s.length()) + s);
         }
      }
      else
      {
         return getZeros(length);
      }
   }

   public static String getRightHashPadding(String s, int length)
   {
      if (s != null)
      {
         if (s.length() > length)
         {
            return s.substring(0, length);
         }
         else
         {
            return (s += getHashes(length - s.length()));
         }
      }
      else
      {
         return getHashes(length);
      }
   }

   public static String getLeftHashPadding(String s, int length)
   {
      if (s != null)
      {
         if (s.length() > length)
         {
            return s.substring(0, length);
         }
         else
         {
            return (getHashes(length - s.length()) + s);
         }
      }
      else
      {
         return getHashes(length);
      }
   }

   public static String getHashes(int i)
   {
      return hashString[i];
   }

   public static String getRightStarPadding(String s, int length)
   {
      if (s != null)
      {
         if (s.length() > length)
         {
            return s.substring(0, length);
         }
         else
         {
            return (s += getStarChars(length - s.length()));
         }
      }
      else
      {
         return getStarChars(length);
      }
   }

   public static String getLeftStarPadding(String s, int length)
   {
      if (s != null)
      {
         if (s.length() > length)
         {
            return s.substring(0, length);
         }
         else
         {
            return (getStarChars(length - s.length()) + s);
         }
      }
      else
      {
         return getStarChars(length);
      }
   }

   public static String getStarChars(int i)
   {
      return hashString[i];
   }

}
