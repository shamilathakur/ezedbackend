package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.CartData;
import com.intelli.ezed.data.DiscountCouponData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.PaymentIdData;
import com.intelli.ezed.data.Response;

public class DiscountHandler extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;

   public DiscountHandler(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      CartData[] cart = (CartData[]) flowContext.getFromContext(EzedKeyConstants.CART_DETAILS);
      PaymentIdData payData = (PaymentIdData) response.getResponseObject();

      int totalAmount = 0;
      for (CartData singleCart : cart)
      {
         totalAmount = totalAmount + singleCart.getAmount();
      }
      flowContext.putIntoContext(EzedKeyConstants.TOTAL_CART_AMOUNT, totalAmount);
      String discountCoupon = (String) flowContext.getFromContext(EzedKeyConstants.DISCOUNT_COUPON);
      if (discountCoupon == null)
      {
         return RuleDecisionKey.ON_SUCCESS;
      }
      DiscountCouponData discData = null;
      //      else
      //      {
      //         payData.setNewAmount(totalAmount - 100);
      //         payData.setDiscountString("Coupon successfully applied");
      //      }

      Connection conn = null;
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         //select * from couponlist where couponcode=?
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchDiscountDetails");
         ps.setString(1, discountCoupon);
         rs = ps.executeQuery();
         if (rs.next())
         {
            discData = new DiscountCouponData(0, 0);
            discData.setCouponCode(discountCoupon);
            discData.setStartTime(DiscountCouponData.sdfDb.parse(rs.getString("starttime")));
            discData.setEndTime(DiscountCouponData.sdfDb.parse(rs.getString("endtime")));
            discData.setMaxValueDiscount(rs.getInt("maxvaluediscount"));
            discData.setValueDiscount(rs.getInt("valuediscount"));
            discData.setPercentDiscount(Float.parseFloat(rs.getString("percentagediscount")));
            discData.setValidFlag(rs.getInt("validflag"));
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Discount details not found for discount code " + discountCoupon);
            }
            response.setParameters(EzedTransactionCodes.DISCOUNT_INVALID, "No such discount exists");
            return RuleDecisionKey.ON_FAILURE;
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching discount details for coupon code " + discountCoupon, e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching discount details");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching discount details for coupon code " + discountCoupon, e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching discount details");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      if (!discData.isCouponValid(response))
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Discount coupon is invalid for reason " + response.getResponseMessage());
         }
         return RuleDecisionKey.ON_FAILURE;
      }
      
      discData.applyDiscount(totalAmount, payData, flowContext);
      return RuleDecisionKey.ON_SUCCESS;
   }
}
