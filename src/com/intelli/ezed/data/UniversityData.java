package com.intelli.ezed.data;

public class UniversityData extends ResponseObject
{
   private String univName;
   private String website;
   private String phoneNo;
   private String cityName;
   private String stateName;
   private String countryName;
   private String createdOn;
   private String createdBy;
   private int showFlag;
   private int userFlag;
   
public String getCreatedOn() {
	return createdOn;
}

public void setCreatedOn(String createdOn) {
	this.createdOn = createdOn;
}

public String getCreatedBy() {
	return createdBy;
}

public void setCreatedBy(String createdBy) {
	this.createdBy = createdBy;
}

public int getShowFlag() {
	return showFlag;
}

public void setShowFlag(int showFlag) {
	this.showFlag = showFlag;
}

public int getUserFlag() {
	return userFlag;
}

public void setUserFlag(int userFlag) {
	this.userFlag = userFlag;
}



   public String getUnivName()
   {
      return univName;
   }

   public void setUnivName(String univName)
   {
      this.univName = univName;
   }

   public String getWebsite()
   {
      return website;
   }

   public void setWebsite(String website)
   {
      this.website = website;
   }

   public String getPhoneNo()
   {
      return phoneNo;
   }

   public void setPhoneNo(String phoneNo)
   {
      this.phoneNo = phoneNo;
   }

   public String getCityName()
   {
      return cityName;
   }

   public void setCityName(String cityName)
   {
      this.cityName = cityName;
   }

   public String getStateName()
   {
      return stateName;
   }

   public void setStateName(String stateName)
   {
      this.stateName = stateName;
   }

   public String getCountryName()
   {
      return countryName;
   }

   public void setCountryName(String countryName)
   {
      this.countryName = countryName;
   }

   @Override
   public void prepareResponse() throws Exception
   {
      this.put("univname", getUnivName());
      this.put("website", getWebsite());
      this.put("phno", getPhoneNo());
      this.put("city", getCityName());
      this.put("state", getStateName());
      this.put("country", getCountryName());
   }

}
