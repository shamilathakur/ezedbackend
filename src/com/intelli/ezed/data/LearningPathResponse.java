package com.intelli.ezed.data;

import java.util.Vector;

public class LearningPathResponse extends ResponseObject
{
   private Vector<String> learningPath;

   public Vector<String> getLearningPath()
   {
      return learningPath;
   }

   public void setLearningPath(Vector<String> learningPath)
   {
      this.learningPath = learningPath;
   }

   @Override
   public void prepareResponse() throws Exception
   {
      StringBuffer sb = new StringBuffer();
      //      sb.append("LearningPath").append(EzedKeyConstants.RESPONSE_SEPARATOR);

      for (String str : learningPath)
      {
         sb.append("'").append(str).append("'").append(",");
      }

      sb.deleteCharAt(sb.length() - 1);
      this.put("learningpath", sb.toString());
   }
}
