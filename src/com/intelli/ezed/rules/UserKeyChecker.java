package com.intelli.ezed.rules;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.LoginResponse;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.UserProfile;
import com.intelli.ezed.db.EzedRequestData;
import com.intelli.ezed.utils.AccountStatusHelper;
import com.intelli.ezed.utils.PasswordHelper;

public class UserKeyChecker extends SingletonRule
{

   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private boolean checkAuthorisedOrNot;

   public UserKeyChecker(String rulaName, String checkAuthorisedOrNot)
   {
      super(rulaName);
      this.checkAuthorisedOrNot = Boolean.parseBoolean(checkAuthorisedOrNot);
   }


   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      EzedRequestData requestData = (EzedRequestData) flowContext.getFromContext(EzedRequestData.EZED_REQUEST_DATA);
      UserProfile userProfile = (UserProfile) flowContext.getFromContext(EzedKeyConstants.USER_DETAILS);
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      int chkflag = userProfile.getChkflag();

      if (requestData.getUserid().compareTo(userProfile.getUserid()) != 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Userid does not match.");
         }
         response.setParameters(EzedTransactionCodes.INVALID_USERID, "Please enter valid userid.");
         return RuleDecisionKey.ON_FAILURE;
      }

      String password = requestData.getPassword();
      try
      {
         password = PasswordHelper.getEncryptedData(password);
      }
      catch (IllegalBlockSizeException e)
      {
         logger.error("Error while authenticating user", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while authenticating user");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (BadPaddingException e)
      {
         logger.error("Error while authenticating user", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while authenticating user");
         return RuleDecisionKey.ON_FAILURE;
      }

      if (password.compareTo(userProfile.getPassword()) != 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Password does not match. Entered pass : " + password + " Saved password : " + userProfile.getPassword());
         }
         response.setParameters(EzedTransactionCodes.INVALID_PASSWORD, "Please enter valid password.");
         return RuleDecisionKey.ON_FAILURE;
      }

      if (checkAuthorisedOrNot)
      {
         if (!AccountStatusHelper.isRegistered(chkflag))
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("User not registered.");
            }
            response.setParameters(EzedTransactionCodes.USERID_NOT_REGISTERED, "Userid not registred.");
            return RuleDecisionKey.ON_FAILURE;
         }
         if (AccountStatusHelper.isLock(chkflag))
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("User Locked.");
            }
            response.setParameters(EzedTransactionCodes.USERID_NOT_REGISTERED, "Userid locked.");
            return RuleDecisionKey.ON_FAILURE;
         }
      }

      if (logger.isDebugEnabled())
      {
         logger.debug("Userid validated. Sending to generate session.");
      }

      response.setParameters(EzedTransactionCodes.SUCCESS, "Userid validated.");
      LoginResponse loginResponse = new LoginResponse();
      loginResponse.setUserProfile(userProfile);
      response.setResponseObject(loginResponse);
      return RuleDecisionKey.ON_SUCCESS;
   }

}
