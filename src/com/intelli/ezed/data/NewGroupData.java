package com.intelli.ezed.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.regex.Matcher;

import org.sabre.db.DbConnectionManager;
import org.slf4j.Logger;

import com.a.helper.utils.StringSplitter;
import com.intelli.ezed.utils.Globals;
import com.intelli.ezed.utils.PrepareGroupIdBackend;

public class NewGroupData extends ResponseObject
{
   private String groupName;
   private String groupId;
   private String adminId;
   private static PrepareGroupIdBackend prepareGroupId = new PrepareGroupIdBackend("GRO2", "ezeddb");
   private String[] users;
   private String userString;
   private String courseId;
   private String micAdminId;

   public String getCourseId()
   {
      return courseId;
   }

   public void setCourseId(String courseId)
   {
      this.courseId = courseId;
   }

   public String[] getUsers()
   {
      return users;
   }

   public void setUsers(String[] users)
   {
      this.users = users;
   }

   public String getUserString()
   {
      return userString;
   }

   public void setUserString(String userString) throws Exception
   {
      this.userString = userString;
      String[] userArr = StringSplitter.splitString(userString, ",");
      Matcher matcher = null;
      for (int i = 0; i < userArr.length; i++)
      {
         matcher = Globals.emailPattern.matcher(userArr[i]);
         if (!matcher.matches())
         {
            throw new Exception("User " + userArr[i] + " has invalid user id");
         }
      }
      this.setUsers(userArr);
   }

   public String getGroupName()
   {
      return groupName;
   }

   public void setGroupName(String groupName)
   {
      this.groupName = groupName;
   }

   public String getGroupId()
   {
      return groupId;
   }

   public void setGroupId(String groupId)
   {
      this.groupId = groupId;
   }

   public void setNewGroupId()
   {
      this.setGroupId(fetchNewGroupId());
   }

   private String fetchNewGroupId()
   {
      return prepareGroupId.getNextId();
   }

   public String getAdminId()
   {
      return adminId;
   }

   public void setAdminId(String adminId)
   {
      this.adminId = adminId;
   }

   @Override
   public void prepareResponse() throws Exception
   {
      this.put("groupid", getGroupId());
   }

   public void persistGroup(String dbConnName, Logger logger) throws SQLException, Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertNewGroup");
         ps.setString(1, getAdminId());
         ps.setString(2, getGroupId());
         ps.setString(3, getGroupName());
         ps.setString(4, getCourseId());
         ps.setString(5, getMicAdminId());
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in db while inserting group by " + getAdminId());
         }
      }
      catch (SQLException e)
      {
         logger.error("DB error while inserting new group by " + getAdminId(), e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while inserting new group by " + getAdminId(), e);
         throw e;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   public void persistGroupUsers(String dbConnName, Logger logger) throws SQLException, Exception
   {
      Connection conn = null;
      PreparedStatement ps = null;
      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         for (String singleUser : getUsers())
         {
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertGroupUsers");
            ps.setString(1, getGroupId());
            ps.setString(2, singleUser);
            int rows = ps.executeUpdate();
            if (logger.isDebugEnabled())
            {
               logger.debug(rows + " rows inserted in db while inserting " + groupId + singleUser);
            }
         }
      }
      catch (SQLException e)
      {
         if (e.getMessage().toUpperCase().contains("UNIQUE"))
         {
            logger.error("Unique error while inserting group id " + getGroupId(), e);
         }
         else
         {
            logger.error("DB error while inserting for group id " + getGroupId(), e);
            throw e;
         }
      }
      catch (Exception e)
      {
         logger.error("Error while inserting for group id " + getGroupId(), e);
         throw e;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   public static void main(String[] args)
   {
      Matcher matcher = Globals.emailPattern.matcher("wasim@mailinator.com");
      System.out.println(matcher.matches());
   }

   public String getMicAdminId()
   {
      return micAdminId;
   }

   public void setMicAdminId(String micAdminId)
   {
      this.micAdminId = micAdminId;
   }
}
