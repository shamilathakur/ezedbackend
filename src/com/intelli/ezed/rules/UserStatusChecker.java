package com.intelli.ezed.rules;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.UserProfile;
import com.intelli.ezed.utils.AccountStatusHelper;

public class UserStatusChecker extends SingletonRule
{
   Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private int statusToCheck;

   public UserStatusChecker(String ruleName, String statusToCheck)
   {
      super(ruleName);
      this.statusToCheck = Integer.parseInt(statusToCheck);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      UserProfile userProfile = (UserProfile) flowContext.getFromContext(EzedKeyConstants.USER_DETAILS);
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      if (userProfile == null)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("UserProfile is null.");
         }
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "User profile not found to be updated");
         return RuleDecisionKey.ON_FAILURE;
      }
      int chkflag = userProfile.getChkflag();
      boolean status = false;

      switch (statusToCheck)
      {
         //Set Registered
         case 1 :
            status = AccountStatusHelper.isRegistered(chkflag);
            if (status == false)
            {
               if (logger.isDebugEnabled())
               {
                  logger.debug("User is not registered. Userid " + userProfile.getUserid());
               }
               response.setParameters(EzedTransactionCodes.USERID_NOT_REGISTERED, "User not registered");
               return RuleDecisionKey.ON_FAILURE;
            }
            break;

         //Set validated
         case 2 :
            status = AccountStatusHelper.isValidate(chkflag);
            if (status == false)
            {
               if (logger.isDebugEnabled())
               {
                  logger.debug("User is not registered. Userid " + userProfile.getUserid());
               }
               response.setParameters(EzedTransactionCodes.USER_NOT_VERIFIED, "User's profile not completed");
               return RuleDecisionKey.ON_FAILURE;
            }
            break;

         //Check forgot password
         case 3 :
            status = AccountStatusHelper.isPasswordForgotten(chkflag);
            if (status == false)
            {
               if (logger.isDebugEnabled())
               {
                  logger.debug("User has not forgotten password. Userid " + userProfile.getUserid());
               }
               response.setParameters(EzedTransactionCodes.USER_NOT_FORGOTTEN_PASSWORD, "User has not forgotten password earlier");
               return RuleDecisionKey.ON_FAILURE;
            }
            break;
      }
      userProfile.setChkflag(chkflag);
      return RuleDecisionKey.ON_SUCCESS;
   }
}
