package com.intelli.ezed.analytics.data;

import com.intelli.ezed.data.ResponseObject;

public class CompletedCourseCountData extends ResponseObject
{
   private int notesCompleted = 0;
   private int audiosCompleted = 0;
   private int videosCompleted = 0;
   private int totalTestsCompleted = 0;
   private int uniqueTestsCompleted = 0;
   private int quizzesCompleted = 0;
   
   public CompletedCourseCountData(int notesCompleted, int audioscompleted, int videosCompleted, int totalTestsCompleted, int totalUniqueTestsCompleted, int quizzesCompleted)
   {
      this.notesCompleted = notesCompleted;
      this.audiosCompleted = audioscompleted;
      this.videosCompleted = videosCompleted;
      this.totalTestsCompleted = totalTestsCompleted;
      this.uniqueTestsCompleted = totalUniqueTestsCompleted;
      this.quizzesCompleted = quizzesCompleted;
   }

   @Override
   public void prepareResponse() throws Exception
   {
      float[] floats = new float[7];
      floats[0] = (float) this.notesCompleted;
      floats[1] = (float) this.videosCompleted;
      floats[2] = (float) this.audiosCompleted;
      floats[3] = (float) this.uniqueTestsCompleted;
      floats[4] = (float) this.totalTestsCompleted;
      floats[5] = (float) this.uniqueTestsCompleted;
      floats[6] = (float) this.quizzesCompleted;

      this.put("name", "completed");
      this.put("data", floats);
   }

}
