package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.a.helper.utils.StringSplitter;
import com.intelli.ezed.data.AdminStatusData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.utils.AdminRoles;

public class RoleSpecificDetailsUpdater extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

   public RoleSpecificDetailsUpdater(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      AdminStatusData data = (AdminStatusData) flowContext.getFromContext(EzedKeyConstants.ADMIN_STATUS_DATA);
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      String adminRoles = data.getRoll();
      if (adminRoles == null || adminRoles.compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Admin role is empty. No additional data will be fetched");
         }
         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
         return RuleDecisionKey.ON_SUCCESS;
      }

      String[] roles = StringSplitter.splitString(adminRoles, ",");
      int currentRole = 0;
      Connection conn = null;
      PreparedStatement ps = null;
      for (int i = 0; i < roles.length; i++)
      {
         currentRole = Integer.parseInt(roles[i]);
         if (currentRole == AdminRoles.MIC_ADMIN)
         {
            String college = messageData.getParameter("collegename");
            if (college == null || college.trim().compareTo("") == 0 || college.trim().compareTo("default") == 0 || college.trim().compareTo("null") == 0)
            {
               college = null;
            }
            String university = messageData.getParameter("univname");
            if (university == null || university.trim().compareTo("") == 0 || university.trim().compareTo("default") == 0 || university.trim().compareTo("null") == 0)
            {
               university = null;
            }
            try
            {
               conn = DbConnectionManager.getConnectionByName(dbConnName);
               ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdateMICDetails");
               ps.setString(1, college);
               ps.setString(2, university);
               ps.setString(3, data.getUserid());
               int rows = ps.executeUpdate();
               if (logger.isDebugEnabled())
               {
                  logger.debug(rows + " rows updated in DB while updating college and university for MIC admin. Trying to insert");
               }
               if (rows == 0)
               {
                  DbConnectionManager.releaseConnection(dbConnName, conn);
                  conn = DbConnectionManager.getConnectionByName(dbConnName);
                  ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertMICCollege");
                  ps.setString(1, data.getUserid());
                  ps.setString(2, university);
                  ps.setString(3, college);
                  rows = ps.executeUpdate();
                  if (logger.isDebugEnabled())
                  {
                     logger.debug(rows + " rows inserted in db while inserting college and mic details for MIC admin " + data.getUserid());
                  }
               }
            }
            catch (Exception e)
            {
               logger.error("Error while updating college and university for MIC admin", e);
               response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while updating MIC specific details");
               return RuleDecisionKey.ON_FAILURE;
            }
            finally
            {
               DbConnectionManager.releaseConnection(dbConnName, conn);
            }
         }
         else if (currentRole == AdminRoles.GROUP_ADMIN)
         {
            String micAdminId = messageData.getParameter("micadminid");
            if (micAdminId == null || micAdminId.trim().compareTo("") == 0 || micAdminId.trim().compareTo("default") == 0 || micAdminId.trim().compareTo("null") == 0)
            {
               micAdminId = null;
            }
            try
            {
               conn = DbConnectionManager.getConnectionByName(dbConnName);
               ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdateGroupMIC");
               ps.setString(1, micAdminId);
               ps.setString(2, data.getUserid());
               int rows = ps.executeUpdate();
               if (logger.isDebugEnabled())
               {
                  logger.debug(rows + " rows updated in DB while updating MIC details for group admin. Trying to insert");
               }
               if (rows == 0)
               {
                  DbConnectionManager.releaseConnection(dbConnName, conn);
                  conn = DbConnectionManager.getConnectionByName(dbConnName);
                  ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertGroupDetails");
                  ps.setString(1, data.getUserid());
                  ps.setString(2, micAdminId);
                  ps.setString(3, sdf.format(new Date()));
                  rows = ps.executeUpdate();
                  if (logger.isDebugEnabled())
                  {
                     logger.debug(rows + " rows inserted in db while inserting college and mic details for MIC admin " + data.getUserid());
                  }
               }
            }
            catch (Exception e)
            {
               logger.error("Error while updating MIC details for group admin", e);
               response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while updating group admin specific details");
               return RuleDecisionKey.ON_FAILURE;
            }
            finally
            {
               DbConnectionManager.releaseConnection(dbConnName, conn);
            }
         }
      }
      response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
      return RuleDecisionKey.ON_SUCCESS;
   }
}
