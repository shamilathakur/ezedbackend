package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.QuizRequestResponse;
import com.intelli.ezed.data.Response;

public class UserQuizPersister extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public UserQuizPersister(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      QuizRequestResponse quizResponse = (QuizRequestResponse) response.getResponseObject();
      String userid = (String) flowContext.getFromContext(EzedKeyConstants.USER_ID);
      String inTime = (String) flowContext.getFromContext(EzedKeyConstants.REQUEST_IN_TIME);
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertUserQuizTrail");
         ps.setString(1, userid);
         ps.setString(2, quizResponse.getQuizId());
         ps.setString(3, inTime);
         ps.setInt(4, quizResponse.getQuizQuestions().size());
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in user quiz trail for user id " + userid + " and quiz id " + quizResponse.getQuizId());
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching questions against the quiz id " + quizResponse.getQuizId(), e);
      }
      catch (Exception e)
      {
         logger.error("Error while fetching questions against the quiz id " + quizResponse.getQuizId(), e);
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
      return RuleDecisionKey.ON_SUCCESS;
   }
}
