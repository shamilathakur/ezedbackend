package com.intelli.ezed.bulkupload.rules;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import au.com.bytecode.opencsv.CSVReader;

import com.intelli.ezed.bulkupload.data.ChapterDetails;
import com.intelli.ezed.bulkupload.data.ContentDetails;
import com.intelli.ezed.bulkupload.data.CourseDetails;
import com.intelli.ezed.bulkupload.data.QuestionBankDetails;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.FIBData;
import com.intelli.ezed.data.MCQData;
import com.intelli.ezed.data.MTFData;
import com.intelli.ezed.data.QuestionContent;

public class QuestionBankCSVDetailsHandler extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.BULK_UPLOAD_LOG);
   private String questionPropertiesFileName;
   private static Object lockObject = new Object();
   private String dbConnName;
   private String qBankDirName;

   public QuestionBankCSVDetailsHandler(String ruleName, String dbConnName, String questionPropertiesFileName,
                                        String qBankDirName)
   {
      super(ruleName);
      //      initDigester(questionDetailsClassFQCN);
      this.dbConnName = dbConnName;
      this.questionPropertiesFileName = questionPropertiesFileName;
      this.qBankDirName = qBankDirName;
   }

   //   private void initDigester(String questionDetailsClassFQCN)
   //   {
   //      questionDigester.addObjectCreate("questionbank", questionDetailsClassFQCN);
   //      questionDigester.addRule("questionbank/question", new TestQuestionConditionRule());
   //
   //      questionDigester.addObjectCreate("questionbank/question/questionstatement", "com.intelli.ezed.data.QuestionContent");
   //      questionDigester.addCallMethod("questionbank/question/questionstatement/type", "setType", 0, new Class<?>[]{Integer.class});
   //      questionDigester.addCallMethod("questionbank/question/questionstatement/string", "setContentString", 0);
   //      questionDigester.addCallMethod("questionbank/question/questionstatement/content", "setContentLoc", 0);
   //      questionDigester.addSetNext("questionbank/question/questionstatement/content", "setQuestion", "com.intelli.ezed.data.QuestionContent");
   //
   //      questionDigester.addObjectCreate("questionbank/question/answer1statement", "com.intelli.ezed.data.QuestionContent");
   //      questionDigester.addCallMethod("questionbank/question/answer1statement/type", "setType", 0, new Class<?>[]{Integer.class});
   //      questionDigester.addCallMethod("questionbank/question/answer1statement/string", "setContentString", 0);
   //      questionDigester.addCallMethod("questionbank/question/answer1statement/content", "setContentLoc", 0);
   //      questionDigester.addSetNext("questionbank/question/answer1statement/content", "addAnswers", "com.intelli.ezed.data.QuestionContent");
   //
   //      questionDigester.addObjectCreate("questionbank/question/question1statement", "com.intelli.ezed.data.QuestionContent");
   //      questionDigester.addCallMethod("questionbank/question/question1statement/type", "setType", 0, new Class<?>[]{Integer.class});
   //      questionDigester.addCallMethod("questionbank/question/question1statement/string", "setContentString", 0);
   //      questionDigester.addCallMethod("questionbank/question/question1statement/content", "setContentLoc", 0);
   //      questionDigester.addSetNext("questionbank/question/question1statement/content", "addQuestions", "com.intelli.ezed.data.QuestionContent");
   //
   //      questionDigester.addObjectCreate("questionbank/question/answer2statement", "com.intelli.ezed.data.QuestionContent");
   //      questionDigester.addCallMethod("questionbank/question/answer2statement/type", "setType", 0, new Class<?>[]{Integer.class});
   //      questionDigester.addCallMethod("questionbank/question/answer2statement/string", "setContentString", 0);
   //      questionDigester.addCallMethod("questionbank/question/answer2statement/content", "setContentLoc", 0);
   //      questionDigester.addSetNext("questionbank/question/answer2statement/content", "addAnswers", "com.intelli.ezed.data.QuestionContent");
   //
   //      questionDigester.addObjectCreate("questionbank/question/question2statement", "com.intelli.ezed.data.QuestionContent");
   //      questionDigester.addCallMethod("questionbank/question/question2statement/type", "setType", 0, new Class<?>[]{Integer.class});
   //      questionDigester.addCallMethod("questionbank/question/question2statement/string", "setContentString", 0);
   //      questionDigester.addCallMethod("questionbank/question/question2statement/content", "setContentLoc", 0);
   //      questionDigester.addSetNext("questionbank/question/question2statement/content", "addQuestions", "com.intelli.ezed.data.QuestionContent");
   //
   //      questionDigester.addObjectCreate("questionbank/question/answer3statement", "com.intelli.ezed.data.QuestionContent");
   //      questionDigester.addCallMethod("questionbank/question/answer3statement/type", "setType", 0, new Class<?>[]{Integer.class});
   //      questionDigester.addCallMethod("questionbank/question/answer3statement/string", "setContentString", 0);
   //      questionDigester.addCallMethod("questionbank/question/answer3statement/content", "setContentLoc", 0);
   //      questionDigester.addSetNext("questionbank/question/answer3statement/content", "addAnswers", "com.intelli.ezed.data.QuestionContent");
   //
   //      questionDigester.addObjectCreate("questionbank/question/question3statement", "com.intelli.ezed.data.QuestionContent");
   //      questionDigester.addCallMethod("questionbank/question/question3statement/type", "setType", 0, new Class<?>[]{Integer.class});
   //      questionDigester.addCallMethod("questionbank/question/question3statement/string", "setContentString", 0);
   //      questionDigester.addCallMethod("questionbank/question/question3statement/content", "setContentLoc", 0);
   //      questionDigester.addSetNext("questionbank/question/question3statement/content", "addQuestions", "com.intelli.ezed.data.QuestionContent");
   //
   //      questionDigester.addObjectCreate("questionbank/question/answer4statement", "com.intelli.ezed.data.QuestionContent");
   //      questionDigester.addCallMethod("questionbank/question/answer4statement/type", "setType", 0, new Class<?>[]{Integer.class});
   //      questionDigester.addCallMethod("questionbank/question/answer4statement/string", "setContentString", 0);
   //      questionDigester.addCallMethod("questionbank/question/answer4statement/content", "setContentLoc", 0);
   //      questionDigester.addSetNext("questionbank/question/answer4statement/content", "addAnswers", "com.intelli.ezed.data.QuestionContent");
   //
   //      questionDigester.addObjectCreate("questionbank/question/question4statement", "com.intelli.ezed.data.QuestionContent");
   //      questionDigester.addCallMethod("questionbank/question/question4statement/type", "setType", 0, new Class<?>[]{Integer.class});
   //      questionDigester.addCallMethod("questionbank/question/question4statement/string", "setContentString", 0);
   //      questionDigester.addCallMethod("questionbank/question/question4statement/content", "setContentLoc", 0);
   //      questionDigester.addSetNext("questionbank/question/question4statement/content", "addQuestions", "com.intelli.ezed.data.QuestionContent");
   //
   //      questionDigester.addObjectCreate("questionbank/question/answer5statement", "com.intelli.ezed.data.QuestionContent");
   //      questionDigester.addCallMethod("questionbank/question/answer5statement/type", "setType", 0, new Class<?>[]{Integer.class});
   //      questionDigester.addCallMethod("questionbank/question/answer5statement/string", "setContentString", 0);
   //      questionDigester.addCallMethod("questionbank/question/answer5statement/content", "setContentLoc", 0);
   //      questionDigester.addSetNext("questionbank/question/answer5statement/content", "addAnswers", "com.intelli.ezed.data.QuestionContent");
   //
   //      questionDigester.addObjectCreate("questionbank/question/question5statement", "com.intelli.ezed.data.QuestionContent");
   //      questionDigester.addCallMethod("questionbank/question/question5statement/type", "setType", 0, new Class<?>[]{Integer.class});
   //      questionDigester.addCallMethod("questionbank/question/question5statement/string", "setContentString", 0);
   //      questionDigester.addCallMethod("questionbank/question/question5statement/content", "setContentLoc", 0);
   //      questionDigester.addSetNext("questionbank/question/question5statement/content", "addQuestions", "com.intelli.ezed.data.QuestionContent");
   //
   //      questionDigester.addObjectCreate("questionbank/question/answer6statement", "com.intelli.ezed.data.QuestionContent");
   //      questionDigester.addCallMethod("questionbank/question/answer6statement/type", "setType", 0, new Class<?>[]{Integer.class});
   //      questionDigester.addCallMethod("questionbank/question/answer6statement/string", "setContentString", 0);
   //      questionDigester.addCallMethod("questionbank/question/answer6statement/content", "setContentLoc", 0);
   //      questionDigester.addSetNext("questionbank/question/answer6statement/content", "addAnswers", "com.intelli.ezed.data.QuestionContent");
   //
   //      questionDigester.addObjectCreate("questionbank/question/question6statement", "com.intelli.ezed.data.QuestionContent");
   //      questionDigester.addCallMethod("questionbank/question/question6statement/type", "setType", 0, new Class<?>[]{Integer.class});
   //      questionDigester.addCallMethod("questionbank/question/question6statement/string", "setContentString", 0);
   //      questionDigester.addCallMethod("questionbank/question/question6statement/content", "setContentLoc", 0);
   //      questionDigester.addSetNext("questionbank/question/question6statement/content", "addQuestions", "com.intelli.ezed.data.QuestionContent");
   //
   //      questionDigester.addCallMethod("questionbank/question/difficulty", "setDifficultyLevel", 0, new Class<?>[]{Integer.class});
   //
   //      questionDigester.addCallMethod("questionbank/question/correctanswer", "setCorrectAnswer", 0);
   //      questionDigester.addCallMethod("quizquestions/question/explanation", "setExplanation", 0);
   //   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      CourseDetails courseDetails = (CourseDetails) flowContext.getFromContext(EzedKeyConstants.BULK_UPLOAD_COURSEOBJECT);
      ChapterDetails chapterDetails = (ChapterDetails) flowContext.getFromContext(EzedKeyConstants.BULK_UPLOAD_CHAPTEROBJECT);
      File dirFile = (File) flowContext.getFromContext(EzedKeyConstants.BULK_UPLOAD_QBANK_DIR_FILE);
      File qBankDirFile = new File(dirFile.getAbsolutePath() + File.separator + qBankDirName);
      if (!qBankDirFile.exists())
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("No Question bank directory found in folder " + dirFile);
         }
         return RuleDecisionKey.ON_FAILURE;
      }

      Map<String, String> qBankDirIdMap = new HashMap<String, String>();

      ContentDetails contentDetails = null;
      File[] files = qBankDirFile.listFiles(new ContentFileNameFilter());
      try
      {
         for (int i = 0; i < files.length; i++)
         {
            contentDetails = new ContentDetails();
            contentDetails.setContentName(files[i].getName().substring(0, files[i].getName().lastIndexOf('.')));
            contentDetails.setDesc("");
            contentDetails.setHours(1);
            contentDetails.setContentTypeFromName(files[i].getName());
            contentDetails.setContentLink(files[i].getAbsolutePath());
            contentDetails.setqFlag(1);
            synchronized (lockObject)
            {
               contentDetails.setNewContentId();
            }
            contentDetails.persistContent(dbConnName, logger);
            qBankDirIdMap.put(contentDetails.getContentName(), contentDetails.getContentId());
         }
      }
      catch (Exception e)
      {
         logger.error("Error while processing individual contents of quiz", e);
         return RuleDecisionKey.ON_FAILURE;
      }

      String qbankPropertiesFile = qBankDirFile.getAbsolutePath() + "/" + questionPropertiesFileName;
      QuestionBankDetails qbankDetails = new QuestionBankDetails();

      try
      {
         CSVReader csvReader = null;
         try
         {
            csvReader = new CSVReader(new FileReader(qbankPropertiesFile));
         }
         catch (FileNotFoundException e1)
         {
            logger.error(questionPropertiesFileName + " was not found. Trying Questions.csv");
            qbankPropertiesFile = qBankDirFile.getAbsolutePath() + "/" + "Questions.csv";
            csvReader = new CSVReader(new FileReader(qbankPropertiesFile));
         }
         csvReader.readNext();
         String[] quizDetailRow = null;
         int i = 1;
         String questionContent = null;
         while (true)
         {
            quizDetailRow = csvReader.readNext();
            if (quizDetailRow == null || quizDetailRow[0] == null || quizDetailRow[0].compareTo("") == 0)
            {
               break;
            }

            QuestionContent question = new QuestionContent();
            if (quizDetailRow[1] == null || quizDetailRow[1].compareTo("") == 0)
            {
               question.setType(QuestionContent.CONTENT_TYPE);
               questionContent = qBankDirIdMap.get("Q" + Integer.toString(i));
               if (questionContent == null)
               {
                  throw new Exception("Question content not found for question no " + i + " for file " + qbankPropertiesFile);
               }
               question.setContentLoc(questionContent);
            }
            else
            {
               question.setType(QuestionContent.STRING_TYPE);
               question.setContentString(quizDetailRow[1]);
            }

            String explanationContentId = qBankDirIdMap.get("E" + Integer.toString(i));
            QuestionContent explanation = null;
            if (explanationContentId != null)
            {
               explanation = new QuestionContent();
               explanation.setType(QuestionContent.CONTENT_TYPE);
               explanation.setContentLoc(explanationContentId);
            }
            else
            {
               explanation = new QuestionContent();
               explanation.setType(QuestionContent.CONTENT_ABSENT);
               explanation.setContentLoc(null);
               explanation.setContentString(null);
            }

            if (quizDetailRow[0].compareToIgnoreCase("MCQ") == 0)
            {
               MCQData mcqData = new MCQData();
               mcqData.setQuestion(question);

               String answerType = quizDetailRow[2];
               if (answerType.compareToIgnoreCase("S") == 0)
               {
                  if (quizDetailRow[3] != null && quizDetailRow[3].compareTo("") != 0)
                  {
                     QuestionContent answer = new QuestionContent();
                     answer.setType(QuestionContent.STRING_TYPE);
                     answer.setContentString(quizDetailRow[3]);
                     mcqData.addAnswers(answer);
                  }
                  else
                  {
                     logger.error("No string options found for quiz for file " + qbankPropertiesFile + " and option no " + i);
                     i++;
                     continue;
                  }
                  if (quizDetailRow[4] != null && quizDetailRow[4].compareTo("") != 0)
                  {
                     QuestionContent answer = new QuestionContent();
                     answer.setType(QuestionContent.STRING_TYPE);
                     answer.setContentString(quizDetailRow[4]);
                     mcqData.addAnswers(answer);
                  }
                  if (quizDetailRow[5] != null && quizDetailRow[5].compareTo("") != 0)
                  {
                     QuestionContent answer = new QuestionContent();
                     answer.setType(QuestionContent.STRING_TYPE);
                     answer.setContentString(quizDetailRow[5]);
                     mcqData.addAnswers(answer);
                  }
                  if (quizDetailRow[6] != null && quizDetailRow[6].compareTo("") != 0)
                  {
                     QuestionContent answer = new QuestionContent();
                     answer.setType(QuestionContent.STRING_TYPE);
                     answer.setContentString(quizDetailRow[6]);
                     mcqData.addAnswers(answer);
                  }
                  if (quizDetailRow[7] != null && quizDetailRow[7].compareTo("") != 0)
                  {
                     QuestionContent answer = new QuestionContent();
                     answer.setType(QuestionContent.STRING_TYPE);
                     answer.setContentString(quizDetailRow[7]);
                     mcqData.addAnswers(answer);
                  }
                  if (quizDetailRow[8] != null && quizDetailRow[8].compareTo("") != 0)
                  {
                     QuestionContent answer = new QuestionContent();
                     answer.setType(QuestionContent.STRING_TYPE);
                     answer.setContentString(quizDetailRow[8]);
                     mcqData.addAnswers(answer);
                  }
               }
               else
               {
                  QuestionContent answer = new QuestionContent();
                  String optionContentId = qBankDirIdMap.get("O" + Integer.toString(i) + "_1");
                  if (optionContentId == null)
                  {
                     answer.setType(QuestionContent.CONTENT_ABSENT);
                     logger.error("No content found for option 1 in HTML file for " + qbankPropertiesFile + " in question no " + i);
                     i++;
                     continue;
                  }
                  else
                  {
                     answer.setType(QuestionContent.CONTENT_TYPE);
                     answer.setContentLoc(optionContentId);
                  }
                  mcqData.addAnswers(answer);
                  for (int j = 2; j < 7; j++)
                  {
                     answer = new QuestionContent();
                     optionContentId = qBankDirIdMap.get("O" + Integer.toString(i) + "_" + Integer.toString(j));
                     if (optionContentId == null)
                     {
                        answer.setType(QuestionContent.CONTENT_ABSENT);
                     }
                     else
                     {
                        answer.setType(QuestionContent.CONTENT_TYPE);
                        answer.setContentLoc(optionContentId);
                        mcqData.addAnswers(answer);
                     }
                  }
               }
               int status = mcqData.setCorrectAnswer(quizDetailRow[9]);
               if (status == 1)
               {
                  logger.error("Error while reading correct ans for file " + qbankPropertiesFile + " and question no " + i);
               }
               mcqData.setDifficultyLevel(quizDetailRow[10]);
               mcqData.setExplanation(explanation);
               qbankDetails.addQuestion(mcqData);
            }
            else if (quizDetailRow[0].compareToIgnoreCase("MTF") == 0)
            {
               MTFData mtfData = new MTFData();
               mtfData.setQuestion(question);

               String answerType = quizDetailRow[2];
               if (answerType.compareToIgnoreCase("S") == 0)
               {
                  if (quizDetailRow[3] != null && quizDetailRow[3].compareTo("") != 0)
                  {
                     QuestionContent answer = new QuestionContent();
                     answer.setType(QuestionContent.STRING_TYPE);
                     answer.setContentString(quizDetailRow[3]);
                     mtfData.addAnswers(answer);
                  }
                  else
                  {
                     logger.error("No string options found for quiz for file " + qbankPropertiesFile + " and option no " + i);
                     i++;
                     continue;
                  }
                  if (quizDetailRow[4] != null && quizDetailRow[4].compareTo("") != 0)
                  {
                     QuestionContent answer = new QuestionContent();
                     answer.setType(QuestionContent.STRING_TYPE);
                     answer.setContentString(quizDetailRow[4]);
                     mtfData.addAnswers(answer);
                  }
                  if (quizDetailRow[5] != null && quizDetailRow[5].compareTo("") != 0)
                  {
                     QuestionContent answer = new QuestionContent();
                     answer.setType(QuestionContent.STRING_TYPE);
                     answer.setContentString(quizDetailRow[5]);
                     mtfData.addAnswers(answer);
                  }
                  if (quizDetailRow[6] != null && quizDetailRow[6].compareTo("") != 0)
                  {
                     QuestionContent answer = new QuestionContent();
                     answer.setType(QuestionContent.STRING_TYPE);
                     answer.setContentString(quizDetailRow[6]);
                     mtfData.addAnswers(answer);
                  }
                  if (quizDetailRow[7] != null && quizDetailRow[7].compareTo("") != 0)
                  {
                     QuestionContent answer = new QuestionContent();
                     answer.setType(QuestionContent.STRING_TYPE);
                     answer.setContentString(quizDetailRow[7]);
                     mtfData.addAnswers(answer);
                  }
                  if (quizDetailRow[8] != null && quizDetailRow[8].compareTo("") != 0)
                  {
                     QuestionContent answer = new QuestionContent();
                     answer.setType(QuestionContent.STRING_TYPE);
                     answer.setContentString(quizDetailRow[8]);
                     mtfData.addAnswers(answer);
                  }
               }
               else
               {
                  QuestionContent answer = new QuestionContent();
                  String optionContentId = qBankDirIdMap.get("O" + Integer.toString(i) + "_1");
                  if (optionContentId == null)
                  {
                     answer.setType(QuestionContent.CONTENT_ABSENT);
                     logger.error("No content found for option 1 in HTML file for " + qbankPropertiesFile + " in question no " + i);
                     i++;
                     continue;
                  }
                  else
                  {
                     answer.setType(QuestionContent.CONTENT_TYPE);
                     answer.setContentLoc(optionContentId);
                  }
                  mtfData.addAnswers(answer);
                  for (int j = 2; j < 7; j++)
                  {
                     answer = new QuestionContent();
                     optionContentId = qBankDirIdMap.get("O" + Integer.toString(i) + "_" + Integer.toString(j));
                     if (optionContentId == null)
                     {
                        //                        answer.setType(QuestionContent.CONTENT_ABSENT);
                     }
                     else
                     {
                        answer.setType(QuestionContent.CONTENT_TYPE);
                        answer.setContentLoc(optionContentId);
                        mtfData.addAnswers(answer);
                     }
                  }
               }

               String questionType = quizDetailRow[11];
               if (questionType.compareToIgnoreCase("S") == 0)
               {
                  if (quizDetailRow[12] != null && quizDetailRow[12].compareTo("") != 0)
                  {
                     QuestionContent answer = new QuestionContent();
                     answer.setType(QuestionContent.STRING_TYPE);
                     answer.setContentString(quizDetailRow[12]);
                     mtfData.addQuestions(answer);
                  }
                  else
                  {
                     logger.error("No string LHS options found for quiz for file " + qbankPropertiesFile + " and option no " + i);
                     i++;
                     continue;
                  }
                  if (quizDetailRow[13] != null && quizDetailRow[13].compareTo("") != 0)
                  {
                     QuestionContent answer = new QuestionContent();
                     answer.setType(QuestionContent.STRING_TYPE);
                     answer.setContentString(quizDetailRow[13]);
                     mtfData.addQuestions(answer);
                  }
                  if (quizDetailRow[14] != null && quizDetailRow[14].compareTo("") != 0)
                  {
                     QuestionContent answer = new QuestionContent();
                     answer.setType(QuestionContent.STRING_TYPE);
                     answer.setContentString(quizDetailRow[14]);
                     mtfData.addQuestions(answer);
                  }
                  if (quizDetailRow[15] != null && quizDetailRow[15].compareTo("") != 0)
                  {
                     QuestionContent answer = new QuestionContent();
                     answer.setType(QuestionContent.STRING_TYPE);
                     answer.setContentString(quizDetailRow[15]);
                     mtfData.addQuestions(answer);
                  }
                  if (quizDetailRow[16] != null && quizDetailRow[16].compareTo("") != 0)
                  {
                     QuestionContent answer = new QuestionContent();
                     answer.setType(QuestionContent.STRING_TYPE);
                     answer.setContentString(quizDetailRow[16]);
                     mtfData.addQuestions(answer);
                  }
                  if (quizDetailRow[17] != null && quizDetailRow[17].compareTo("") != 0)
                  {
                     QuestionContent answer = new QuestionContent();
                     answer.setType(QuestionContent.STRING_TYPE);
                     answer.setContentString(quizDetailRow[17]);
                     mtfData.addQuestions(answer);
                  }
               }
               else
               {
                  QuestionContent answer = new QuestionContent();
                  String optionContentId = qBankDirIdMap.get("Q" + Integer.toString(i) + "_1");
                  if (optionContentId == null)
                  {
                     answer.setType(QuestionContent.CONTENT_ABSENT);
                     logger.error("No content found for LHS option 1 in HTML file for " + qbankPropertiesFile + " in question no " + i);
                     i++;
                     continue;
                  }
                  else
                  {
                     answer.setType(QuestionContent.CONTENT_TYPE);
                     answer.setContentLoc(optionContentId);
                  }
                  mtfData.addQuestions(answer);
                  for (int j = 2; j < 7; j++)
                  {
                     answer = new QuestionContent();
                     optionContentId = qBankDirIdMap.get("Q" + Integer.toString(i) + "_" + Integer.toString(j));
                     if (optionContentId == null)
                     {
                        //                        answer.setType(QuestionContent.CONTENT_ABSENT);
                     }
                     else
                     {
                        answer.setType(QuestionContent.CONTENT_TYPE);
                        answer.setContentLoc(optionContentId);
                        mtfData.addQuestions(answer);
                     }
                  }
               }
               int status = mtfData.setCorrectAnswer(quizDetailRow[9]);
               if (status == 1)
               {
                  logger.error("Error while reading correct ans for file " + qbankPropertiesFile + " and question no " + i);
               }
               mtfData.setDifficultyLevel(quizDetailRow[10]);
               mtfData.setExplanation(explanation);
               qbankDetails.addQuestion(mtfData);
            }
            else if (quizDetailRow[0].compareToIgnoreCase("FIB") == 0)
            {
               FIBData fibData = new FIBData();
               fibData.setQuestion(question);

               String answerType = quizDetailRow[2];
               if (answerType.compareToIgnoreCase("S") == 0)
               {
                  if (quizDetailRow[3] != null && quizDetailRow[3].compareTo("") != 0)
                  {
                     QuestionContent answer = new QuestionContent();
                     answer.setType(QuestionContent.STRING_TYPE);
                     answer.setContentString(quizDetailRow[3]);
                     fibData.addAnswers(answer);
                  }
                  else
                  {
                     logger.error("No answers found for FIB for quiz for file " + qbankPropertiesFile + " and option no " + i);
                     i++;
                     continue;
                  }
                  if (quizDetailRow[4] != null && quizDetailRow[4].compareTo("") != 0)
                  {
                     QuestionContent answer = new QuestionContent();
                     answer.setType(QuestionContent.STRING_TYPE);
                     answer.setContentString(quizDetailRow[4]);
                     fibData.addAnswers(answer);
                  }
                  if (quizDetailRow[5] != null && quizDetailRow[5].compareTo("") != 0)
                  {
                     QuestionContent answer = new QuestionContent();
                     answer.setType(QuestionContent.STRING_TYPE);
                     answer.setContentString(quizDetailRow[5]);
                     fibData.addAnswers(answer);
                  }
                  if (quizDetailRow[6] != null && quizDetailRow[6].compareTo("") != 0)
                  {
                     QuestionContent answer = new QuestionContent();
                     answer.setType(QuestionContent.STRING_TYPE);
                     answer.setContentString(quizDetailRow[6]);
                     fibData.addAnswers(answer);
                  }
                  if (quizDetailRow[7] != null && quizDetailRow[7].compareTo("") != 0)
                  {
                     QuestionContent answer = new QuestionContent();
                     answer.setType(QuestionContent.STRING_TYPE);
                     answer.setContentString(quizDetailRow[7]);
                     fibData.addAnswers(answer);
                  }
                  if (quizDetailRow[8] != null && quizDetailRow[8].compareTo("") != 0)
                  {
                     QuestionContent answer = new QuestionContent();
                     answer.setType(QuestionContent.STRING_TYPE);
                     answer.setContentString(quizDetailRow[8]);
                     fibData.addAnswers(answer);
                  }
               }
               else
               {
                  throw new Exception("Correct answers cannot be contents in case of Fill in the Blanks");
               }
               fibData.setCorrectAnswer(quizDetailRow[9]);
               fibData.setDifficultyLevel(quizDetailRow[10]);
               fibData.setExplanation(explanation);
               qbankDetails.addQuestion(fibData);
            }
            else
            {
               if (logger.isDebugEnabled())
               {
                  logger.debug("Questions.csv for question bank has the wrong value in column no. 1 for file " + qbankPropertiesFile);
               }
            }
            i++;
         }
      }
      catch (FileNotFoundException e)
      {
         logger.error("Error while reading quiz file " + qbankPropertiesFile + " " + e.getMessage(), e);
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (IOException e)
      {
         logger.error("Error while reading quiz file " + qbankPropertiesFile + " " + e.getMessage(), e);
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while reading quiz file " + qbankPropertiesFile + " " + e.getMessage(), e);
         return RuleDecisionKey.ON_FAILURE;
      }

      try
      {
         qbankDetails.setCourseId(courseDetails.getCourseId());
         if (chapterDetails != null)
         {
            qbankDetails.setChapterId(chapterDetails.getChapterId());
         }
         qbankDetails.persistQuestionBank(dbConnName, logger);
      }
      catch (IOException e)
      {
         logger.error("Error while reading question bank details properties file " + qbankPropertiesFile + " " + e.getMessage(), e);
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (SAXException e)
      {
         logger.error("Error while reading question bank details properties file " + qbankPropertiesFile + " " + e.getMessage(), e);
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while processing question bank details after reading properly from properties file " + qbankPropertiesFile + " " + e.getMessage(), e);
         return RuleDecisionKey.ON_FAILURE;
      }
      return RuleDecisionKey.ON_SUCCESS;
   }
}
