package com.intelli.ezed.data;

public class CartDetails
{
   private CartData[] cartDatas;
   private String appId;
   private String paymentId;
   public CartData[] getCartDatas()
   {
      return cartDatas;
   }
   public void setCartDatas(CartData[] cartDatas)
   {
      this.cartDatas = cartDatas;
   }
   public String getAppId()
   {
      return appId;
   }
   public void setAppId(String appId)
   {
      this.appId = appId;
   }
   public String getPaymentId()
   {
      return paymentId;
   }
   public void setPaymentId(String paymentId)
   {
      this.paymentId = paymentId;
   }
   
}
