package com.intelli.ezed.db;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class MyNotesUpdaterM extends SingletonRule
{

	private String dbConnName;
	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
	
	public MyNotesUpdaterM(String ruleName,  String dbConnName) {
		super(ruleName);
		 this.dbConnName = dbConnName;
	}

	@Override
	public RuleDecisionKey executeRule(FlowContext flowContext) {
		 MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
	     Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
	     
	     String noteid = messageData.getParameter("noteid");
	     if (noteid == null || noteid.trim().compareTo("") == 0)
	      {
	         if (logger.isDebugEnabled())
	         {
	            logger.debug("Note id is invalid " + noteid);
	         }
	         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid  note id");
	         return RuleDecisionKey.ON_FAILURE;
	      }
	     String userid = messageData.getParameter("email");
	     if (userid == null || userid.trim().compareTo("") == 0)
	      {
	         if (logger.isDebugEnabled())
	         {
	            logger.debug("user id is invalid " + userid);
	         }
	         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid  user id");
	         return RuleDecisionKey.ON_FAILURE;
	      }
	     String note = messageData.getParameter("note");
	     if (note == null || note.trim().compareTo("") == 0)
	      {
	         if (logger.isDebugEnabled())
	         {
	            logger.debug("note id is invalid " + note);
	         }
	         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid  note ");
	         return RuleDecisionKey.ON_FAILURE;
	      }
	     SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
	         
	  String imagepath = messageData.getParameter("imagepath");
	    Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
			PreparedStatement ps = null;
			try {
				ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdateMyNotes");
		         ps.setString(1, note);
		         ps.setString(2, dateFormat.format(new Date()));
		         ps.setString(3, imagepath);
		         ps.setString(4, noteid);
		         
				System.out.println("Final Query : " + ps);
				int rows = ps.executeUpdate();
		         if (rows > 0)
		         {
		            if (logger.isDebugEnabled())
		            {
		               logger.debug(rows + " rows updated in db while adding notes noteid " + noteid );
		            }
		            response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
		            return RuleDecisionKey.ON_SUCCESS;
		         }
		         else
		         {
		            if (logger.isDebugEnabled())
		            {
		               logger.debug("No rows were updated");
		            }
		            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, " notes could not be updated");
		            return RuleDecisionKey.ON_FAILURE;
		         }
	         
			} catch (SQLException e)
		      {
		         logger.error("Error while insering notes for user " + userid, e);
		         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while updates notes " + e.getMessage());
		         return RuleDecisionKey.ON_FAILURE;
		      }
		      catch (Exception e)
		      {
		    	  logger.error("Error while insering notes for user " + userid, e);
		         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while updating notes " + e.getMessage());
		         return RuleDecisionKey.ON_FAILURE;
		      }
		      finally
		      {
		         DbConnectionManager.releaseConnection(dbConnName, conn);
		      }
	}

}
