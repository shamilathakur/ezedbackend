package com.intelli.ezed.data;
import java.util.ArrayList;

public class CartDataList  extends ResponseObject{

	private ArrayList<CartDataObject> cartobjectlist;
	@Override
	public void prepareResponse() throws Exception {
		CartDataObject[] cartitemsData = new CartDataObject[getCartobjectlist().size()];
		if(cartitemsData != null ){
			for(int i=0;i<cartitemsData.length; i++)
			{
				cartitemsData[i] = getCartobjectlist().get(i);
			}
		}
		this.put("itemsInCart", cartitemsData);

	}
	/**
	 * @return the cartobjectlist
	 */
	public ArrayList<CartDataObject> getCartobjectlist() {
		return cartobjectlist;
	}
	/**
	 * @param cartobjectlist the cartobjectlist to set
	 */
	public void setCartobjectlist(ArrayList<CartDataObject> cartobjectlist) {
		this.cartobjectlist = cartobjectlist;
	}
	
}
