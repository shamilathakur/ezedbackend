package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.a.helper.utils.StringSplitter;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class CourseChapterListForFilterFetcherIntermediate extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public CourseChapterListForFilterFetcherIntermediate(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      String chapterId = messageData.getParameter("chapterid");
      if (chapterId != null && chapterId.compareTo("") != 0 && chapterId.compareToIgnoreCase("null") != 0)
      {
         String[] chapterIdList = StringSplitter.splitString(chapterId, ",");
         String courseId = messageData.getParameter("courseid");
         if (courseId == null || courseId.compareTo("") == 0 || courseId.compareToIgnoreCase("null") == 0)
         {
            courseId = null;
         }
         flowContext.putIntoContext(EzedKeyConstants.COURSE_ID_LIST, courseId);
         flowContext.putIntoContext(EzedKeyConstants.CHAPTER_ID_LIST, chapterIdList);
         return RuleDecisionKey.ON_PARTIAL;
      }

      String courseId = messageData.getParameter("courseid");
      if (courseId != null && courseId.compareTo("") != 0 && courseId.compareToIgnoreCase("null") != 0)
      {
         String[] courseIdList = StringSplitter.splitString(courseId, ",");
         flowContext.putIntoContext(EzedKeyConstants.COURSE_ID_LIST, courseIdList);
         return RuleDecisionKey.ON_SUCCESS;
      }

      String univName = messageData.getParameter("univname");
      if (univName == null || univName.compareTo("") == 0 || univName.compareToIgnoreCase("null") == 0)
      {
         univName = null;
      }

      String degree = messageData.getParameter("degree");
      if (degree == null || degree.compareTo("") == 0 || degree.compareToIgnoreCase("null") == 0)
      {
         degree = null;
      }

      String stream = messageData.getParameter("stream");
      if (stream == null || stream.compareTo("") == 0 || stream.compareToIgnoreCase("null") == 0)
      {
         stream = null;
      }

      String year = messageData.getParameter("year");
      if (year == null || year.compareTo("") == 0 || year.compareToIgnoreCase("null") == 0)
      {
         year = null;
      }

      String semester = messageData.getParameter("semester");
      if (semester == null || semester.compareTo("") == 0 || semester.compareToIgnoreCase("null") == 0)
      {
         semester = null;
      }
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      ArrayList<String> courseIdList = new ArrayList<String>();
      try
      {
         if (univName == null)
         {
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchCourseListAdmin");
         }
         else if (degree == null)
         {
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchCourseListAdminFilterUniversity");
            ps.setString(1, univName);
         }
         else if (stream == null)
         {
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchCourseListAdminFilterDegree");
            ps.setString(1, univName);
            ps.setString(2, degree);
         }
         else if (year == null)
         {
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchCourseListAdminFilterStream");
            ps.setString(1, univName);
            ps.setString(2, degree);
            ps.setString(3, stream);
         }
         else if (semester == null)
         {
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchCourseListAdminFilterYear");
            ps.setString(1, univName);
            ps.setString(2, degree);
            ps.setString(3, stream);
            ps.setString(4, year);
         }
         else
         {
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchCourseListAdminFilter");
            ps.setString(1, univName);
            ps.setString(2, degree);
            ps.setString(3, stream);
            ps.setString(4, year);
            ps.setString(5, semester);
         }
         rs = ps.executeQuery();
         while (rs.next())
         {
            courseIdList.add(rs.getString("courseid"));
         }
         String[] courseArr = new String[courseIdList.size()];
         for (int i = 0; i < courseIdList.size(); i++)
         {
            courseArr[i] = courseIdList.get(i);
         }
         flowContext.putIntoContext(EzedKeyConstants.COURSE_ID_LIST, courseArr);
      }
      catch (SQLException e)
      {
         logger.error("DB error while fetching list of courses", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "DB error while fetching courses");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching list of courses", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching courses");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing result set while fetching course list", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
      return RuleDecisionKey.ON_SUCCESS;
   }
}
