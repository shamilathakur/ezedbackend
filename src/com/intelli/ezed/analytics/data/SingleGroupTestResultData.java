package com.intelli.ezed.analytics.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.sabre.db.DbConnectionManager;
import org.slf4j.Logger;

import com.intelli.ezed.data.ResponseObject;

public class SingleGroupTestResultData extends ResponseObject
{
   private String testId;
   private String testName;
   private ArrayList<Integer> userResult = new ArrayList<Integer>();
   private Integer notAttemptedCount = new Integer(0);
   private int maxQuestions;

   public void fetchTestName(String dbConnName, Logger logger) throws SQLException, Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "SelectTestDetails");
         ps.setString(1, getTestId());
         rs = ps.executeQuery();
         if (rs.next())
         {
            this.setTestName(rs.getString("testname"));
         }
      }
      catch (SQLException e)
      {
         throw e;
      }
      catch (Exception e)
      {
         throw e;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

   }

   public int fetchUserTestResult(String dbConnName, Logger logger, ArrayList<String> groupUsers) throws SQLException, Exception
   {
      Connection conn = null;
      PreparedStatement ps = null;
      ResultSet rs = null;
      Boolean doneOnce = false;
      int correctAnsCount = 0;
      for (int i = 0; i < groupUsers.size(); i++)
      {
         try
         {
            conn = DbConnectionManager.getConnectionByName(dbConnName);
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchGroupTestSummary");
            ps.setString(1, getTestId());
            ps.setString(2, groupUsers.get(i));
            rs = ps.executeQuery();
            if (rs.next())
            {
               if (!doneOnce)
               {
                  int noOfQuestions = rs.getInt("numberofquestions");
                  for (int j = 0; j < noOfQuestions + 1; j++)
                  {
                     userResult.add(new Integer(0));
                  }
                  doneOnce = true;
               }
               correctAnsCount = rs.getInt("correct");
               userResult.set(correctAnsCount, userResult.get(correctAnsCount) + 1);
            }
            else
            {
               notAttemptedCount++;
            }
         }
         catch (SQLException e)
         {
            throw e;
         }
         catch (Exception e)
         {
            throw e;
         }
         finally
         {
            if (rs != null)
            {
               try
               {
                  rs.close();
               }
               catch (Exception e)
               {
               }
            }
            DbConnectionManager.releaseConnection(dbConnName, conn);
         }
      }
      return userResult.size();
   }

   public String getTestId()
   {
      return testId;
   }

   public void setTestId(String testId)
   {
      this.testId = testId;
   }

   public String getTestName()
   {
      return testName;
   }

   public void setTestName(String testName)
   {
      this.testName = testName;
   }

   public ArrayList<Integer> getUserResult()
   {
      return userResult;
   }

   public void setUserResult(ArrayList<Integer> userResult)
   {
      this.userResult = userResult;
   }

   public Integer getNotAttemptedCount()
   {
      return notAttemptedCount;
   }

   public void setNotAttemptedCount(Integer notAttemptedCount)
   {
      this.notAttemptedCount = notAttemptedCount;
   }

   @Override
   public void prepareResponse() throws Exception
   {
      Integer[] testResult = new Integer[maxQuestions + 1];
      for (int i = 0; i < userResult.size(); i++)
      {
         testResult[i] = userResult.get(i);
      }
      for (int i = userResult.size(); i < maxQuestions; i++)
      {
         testResult[i] = 0;
      }
      testResult[testResult.length - 1] = notAttemptedCount;

      this.put("name", getTestName());
      this.put("data", testResult);
   }

   public int getMaxQuestions()
   {
      return maxQuestions;
   }

   public void setMaxQuestions(int maxQuestions)
   {
      this.maxQuestions = maxQuestions;
   }
}
