package com.intelli.ezed.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import com.a.helper.utils.StringSplitter;

public class ZipFileExtractor
{
   ArrayList<File> listOfFiles = new ArrayList<File>();

   public String extractZipFile(String fName) throws IOException, Exception
   {
      ZipInputStream zinstream = null;
      File dir = null;
      try
      {
         byte[] buf = new byte[1024];
         zinstream = new ZipInputStream(new FileInputStream(fName));
         String dirName = fName.substring(0, fName.lastIndexOf("."));
         dir = new File(dirName);
         if (!dir.exists())
         {
            dir.mkdir();
         }

         ZipEntry zentry = zinstream.getNextEntry();

         File file = null;

         while (zentry != null)
         {
            System.out.println(dir.getAbsolutePath());
            System.out.println("zentry : " + zentry.getName());

            if (zentry.isDirectory())
            {
               System.out.println("I am a directory");
               String split[] = zentry.getName().split("/");
               String path = split[0];
               file = new File(dir.getAbsolutePath() + "/" + split[0]);
               file.mkdir();
               for (int i = 1; i < split.length; i++)
               {
                  path = path + "/" + split[i];
                  file = new File(dir.getAbsolutePath() + "/" + path);
                  file.mkdir();
               }

               zinstream.closeEntry();
               zentry = zinstream.getNextEntry();
               continue;
            }

            String[] path = null;

            if (zentry.getName().contains(File.separator))
            {
               path = StringSplitter.splitString(zentry.getName(), File.separator);
            }
            else
            {
               path = StringSplitter.splitString(zentry.getName(), "/");
            }
            StringBuffer pathName = new StringBuffer(dir.getAbsolutePath());
            File intermediateFile = null;
            for (int i = 0; i < path.length - 1; i++)
            {
               pathName.append(File.separator).append(path[i]);
               intermediateFile = new File(pathName.toString());
               if (!intermediateFile.exists())
               {
                  intermediateFile.mkdirs();
               }
            }

            FileOutputStream outstream = new FileOutputStream(pathName.append(File.separator).append(path[path.length - 1]).toString());
            int n = 0;

            while ((n = zinstream.read(buf, 0, 1024)) > -1)
            {
               outstream.write(buf, 0, n);

            }
            outstream.close();

            zinstream.closeEntry();
            zentry = zinstream.getNextEntry();
         }

         String result = getFiles(dir);
         if (result != null && !result.equals(""))
            return result;

         for (File singleFile : listOfFiles)
         {
            if (singleFile.getName().endsWith("html") || singleFile.getName().endsWith("htm") || singleFile.getName().endsWith("HTML") || singleFile.getName().endsWith("HTM"))
            {
               return singleFile.getAbsolutePath();
            }
         }
         throw new Exception("No HTML file found in zip");

      }
      catch (IOException e)
      {
         throw e;
      }
      catch (Exception e)
      {
         throw e;
      }
      finally
      {
         if (zinstream != null)
         {
            try
            {
               zinstream.close();
            }
            catch (IOException e)
            {
            }
         }
      }
   }

   public static void main(String[] args) throws Exception
   {
      //      String fName = "conf/N_CayleyHamiltonTheorem.zip";
      //      String fName = "conf/VQ_1_files.zip";
      String fName = "conf/Applied Maths1_About the course_MU.zip";
      ZipFileExtractor extractor = new ZipFileExtractor();
      System.out.println("HTML file : " + extractor.extractZipFile(fName));
   }

   public String getFiles(File dir)
   {
      String result = "";
      File[] files = dir.listFiles();
      for (File singleFile : files)
      {
         if (singleFile.isDirectory())
         {
            result = getFiles(singleFile);
            if (!result.equals(""))
               break;

         }
         else if (singleFile.getName().equals("book.html"))
            return singleFile.getAbsolutePath();
         else
         {
            listOfFiles.add(singleFile);

         }

      }
      return result;

   }
}