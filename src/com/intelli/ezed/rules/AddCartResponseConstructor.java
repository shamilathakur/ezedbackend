package com.intelli.ezed.rules;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.UserProfile;

public class AddCartResponseConstructor extends SingletonRule
{

   public AddCartResponseConstructor(String ruleName)
   {
      super(ruleName);
      // TODO Auto-generated constructor stub
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      UserProfile profile = (UserProfile) flowContext.getFromContext(EzedKeyConstants.USER_DETAILS);

      response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
      response.setResponseObject(profile);
      return RuleDecisionKey.ON_SUCCESS;
   }
}
