package com.intelli.ezed.rules;

import java.util.ArrayList;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.TestQuestion;
import com.intelli.ezed.data.TestQuestionData;
import com.intelli.ezed.utils.Randomizer;

public class RandomQuestionSetMakerPersister extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.RANDOM_QUESTION_LOG);
   private int noOfRandomQuestions;
   private String dbConnName;

   public RandomQuestionSetMakerPersister(String ruleName, String dbConnName, String noOfRandomQuestions)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
      this.noOfRandomQuestions = Integer.parseInt(noOfRandomQuestions);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      TestQuestionData questData = (TestQuestionData) flowContext.getFromContext(EzedKeyConstants.QUESTION_OBJECT);
      TestQuestion question = questData.getTestQuestion();
      ArrayList<TestQuestion> questionList = question.getPermutedQuestions(noOfRandomQuestions, new Randomizer(question.getNoOfAnswers()));
      questData.persistData(questionList, dbConnName, logger);
      return RuleDecisionKey.ON_SUCCESS;
   }

}
