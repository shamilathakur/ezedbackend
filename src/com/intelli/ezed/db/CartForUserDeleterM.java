package com.intelli.ezed.db;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class CartForUserDeleterM extends SingletonRule{

	public CartForUserDeleterM(String ruleName,  String dbConnName) {
		super(ruleName);
		 this.dbConnName = dbConnName;
	}
	private String dbConnName;
	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
	@Override
	public RuleDecisionKey executeRule(FlowContext flowContext) {
		 MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
	     Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
	     String userid = messageData.getParameter("email");
	     if (userid == null || userid.trim().compareTo("") == 0)
	      {
	         if (logger.isDebugEnabled())
	         {
	            logger.debug("user id is invalid " + userid);
	         }
	         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid  user id");
	         return RuleDecisionKey.ON_FAILURE;
	      }
	     Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
	     PreparedStatement ps = null;
	     try {
		     ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "DeleteCartForUser");
	         ps.setString(1, userid);
	         int rows = ps.executeUpdate();
	         if (rows > 0)
	         {
	            if (logger.isDebugEnabled())
	            {
	               logger.debug(rows + " rows inserted in db while deleting cart data to user " + userid );
	            }
	            response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
	            return RuleDecisionKey.ON_SUCCESS;
	         }
	         else
	         {
	            if (logger.isDebugEnabled())
	            {
	               logger.debug("No rows were updated");
	            }
	            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while deleting cart object");
	            return RuleDecisionKey.ON_FAILURE;
	         }
         
		} catch (SQLException e)
	      {
	         logger.error("Error while insering notes for user " + userid, e);
	         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while deleting cart object" + e.getMessage());
	         return RuleDecisionKey.ON_FAILURE;
	      }
	      catch (Exception e)
	      {
	    	  logger.error("Error while insering notes for user " + userid, e);
	         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while deleting cart object" + e.getMessage());
	         return RuleDecisionKey.ON_FAILURE;
	      }
	      finally
	      {
	         DbConnectionManager.releaseConnection(dbConnName, conn);
	      }

	}
	

}
