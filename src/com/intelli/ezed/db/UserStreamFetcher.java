package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.dropdowns.data.DropDownListData;
import com.intelli.ezed.dropdowns.data.SingleDropDownData;

public class UserStreamFetcher extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private int defCount;
   private SimpleDateFormat sdfDb = new SimpleDateFormat("yyyyMMddHHmmss");
   private SimpleDateFormat sdfOut = new SimpleDateFormat("HH:mm:ss dd-MM-yyyy");

   public UserStreamFetcher(String ruleName, String dbConnName, String defCount)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
      this.defCount = Integer.parseInt(defCount);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      String userId = (String) flowContext.getFromContext(EzedKeyConstants.USER_ID);
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());

      String count = messageData.getParameter("count");
      if (count == null || count.trim().compareTo("") == 0)
      {
         count = "0";
      }

      int countInt = 0;
      try
      {
         countInt = Integer.parseInt(count);
      }
      catch (Exception e)
      {
         countInt = 0;
      }

      if (countInt == 0)
      {
         countInt = defCount;
      }

      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      DropDownListData dropDownListData = new DropDownListData();
      dropDownListData.setName("userstream");
      SingleDropDownData data = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchUserStream");
         ps.setString(1, userId);
         ps.setInt(2, countInt);
         rs = ps.executeQuery();
         while (rs.next())
         {
            data = new SingleDropDownData();
            data.setParam(sdfOut.format(sdfDb.parse(rs.getString("createtime"))));
            data.setValue(rs.getString("stream"));
            dropDownListData.getDropDownDatas().add(data);
         }
         response.setStatusCodeSend(false);
         response.setResponseObject(dropDownListData);
         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
         return RuleDecisionKey.ON_SUCCESS;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching user stream", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Stream could not be fetched");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing resultset while fetching user stream", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
}
