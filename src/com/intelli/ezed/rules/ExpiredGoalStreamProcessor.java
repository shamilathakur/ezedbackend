package com.intelli.ezed.rules;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EventData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.GoalData;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.UserProfile;

public class ExpiredGoalStreamProcessor extends SingletonRule
{
   private String expiredActionId;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public ExpiredGoalStreamProcessor(String ruleName, String expiredActionId)
   {
      super(ruleName);
      this.expiredActionId = expiredActionId;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      GoalData goalData = (GoalData) flowContext.getFromContext(EzedKeyConstants.EXPIRED_GOAL);
      UserProfile userProfile = (UserProfile) flowContext.getFromContext(EzedKeyConstants.USER_DETAILS);
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      EventData eventData = new EventData();
      eventData.setActionId(expiredActionId);
      eventData.setCourseId(goalData.getCourseId());
      eventData.setChapterId(goalData.getChapterId());
      eventData.setContentId(goalData.getContentId());

      try
      {
         UserEventProcessor.processUserStream(eventData, userProfile, response);
      }
      catch (Exception e)
      {
         logger.error("Failed to process failed goal stream for userid " + userProfile.getUserid(), e);
         return RuleDecisionKey.ON_FAILURE;
      }
      return RuleDecisionKey.ON_SUCCESS;
   }
}
