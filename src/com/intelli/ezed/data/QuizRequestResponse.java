package com.intelli.ezed.data;

import java.util.ArrayList;

public class QuizRequestResponse extends ResponseObject
{
   private ArrayList<TestRequestResponse> quizQuestions;
   private ChallengeTestData challengeTestData = null;
   private String quizId;

   public ChallengeTestData getChallengeTestData()
   {
      return challengeTestData;
   }

   public void setChallengeTestData(ChallengeTestData challengeTestData)
   {
      this.challengeTestData = challengeTestData;
   }

   public ArrayList<TestRequestResponse> getQuizQuestions()
   {
      return quizQuestions;
   }

   public void setQuizQuestions(ArrayList<TestRequestResponse> quizQuestions)
   {
      this.quizQuestions = quizQuestions;
   }

   public String getQuizId()
   {
      return quizId;
   }

   public void setQuizId(String quizId)
   {
      this.quizId = quizId;
   }

   @Override
   public void prepareResponse() throws Exception
   {
      TestRequestResponse[] quizQuestions = new TestRequestResponse[getQuizQuestions().size()];
      //      if (getQuizQuestions().size() == 1)
      if (getQuizId() == null)
      {
         getQuizQuestions().get(0).prepareResponse();
         this.put("question", getQuizQuestions().get(0));
      }
      else
      {
         this.put("quizid", getQuizId());
         for (int i = 0; i < getQuizQuestions().size(); i++)
         {
            getQuizQuestions().get(i).setCurrentQuestionNumber(i + 1);
            quizQuestions[i] = getQuizQuestions().get(i);
            getQuizQuestions().get(i).prepareResponse();
         }
         this.put("question", quizQuestions);
      }

      if (getChallengeTestData() != null)
      {
         getChallengeTestData().prepareResponse();
         this.put("challengedetails", getChallengeTestData());
      }

   }
}
