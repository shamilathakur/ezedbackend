package com.intelli.ezed.data;

public class ThreadData extends ResponseObject{
	String threadId,threadTitle,threadDesc,createTime,level,status,correction,postCount,editedThreadDesc,modifiedTime;

	public String getThreadId() {
		return threadId;
	}

	public void setThreadId(String threadId) {
		this.threadId = threadId;
	}

	public String getThreadTitle() {
		return threadTitle;
	}

	public void setThreadTitle(String threadTitle) {
		this.threadTitle = threadTitle;
	}

	public String getThreadDesc() {
		return threadDesc;
	}

	public void setThreadDesc(String threadDesc) {
		this.threadDesc = threadDesc;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCorrection() {
		return correction;
	}

	public void setCorrection(String correction) {
		this.correction = correction;
	}

	public String getPostCount() {
		return postCount;
	}

	public void setPostCount(String postCount) {
		this.postCount = postCount;
	}

	public String getEditedThreadDesc() {
		return editedThreadDesc;
	}

	public void setEditedThreadDesc(String editedThreadDesc) {
		this.editedThreadDesc = editedThreadDesc;
	}

	public String getModifiedTime() {
		return modifiedTime;
	}

	public void setModifiedTime(String modifiedTime) {
		this.modifiedTime = modifiedTime;
	}

	@Override
	public void prepareResponse() throws Exception {
		this.put("threadid", getThreadId());
		this.put("threaddesc", getThreadDesc());
		this.put("threadtitle", getThreadTitle());
		this.put("postcount",getPostCount());
		this.put("editeddesc",getEditedThreadDesc());
		this.put("createtime",getCreateTime());
		this.put("modifiedtime",getModifiedTime());
		this.put("status",getStatus());
		this.put("level", getLevel());
		this.put("correction",getCorrection());
	}
	
}
