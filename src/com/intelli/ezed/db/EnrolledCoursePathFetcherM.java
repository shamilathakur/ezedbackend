package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EnrolledCourseChapter;
import com.intelli.ezed.data.EnrolledCoursePath;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.FrontendFetchData;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.utils.EzedHelper;

public class EnrolledCoursePathFetcherM extends SingletonRule{

	private String dbConnName;
	   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

	public EnrolledCoursePathFetcherM(	String ruleName, String dbConnName) {
		super(ruleName);
		this.dbConnName = dbConnName;
	}

	@Override
	public RuleDecisionKey executeRule(FlowContext flowContext) {
		
		 Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
	     MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
		
	     String courseId = messageData.getParameter("courseid");
	     if(courseId == null || courseId.trim().compareTo("")==0){
	    	 if(logger.isDebugEnabled()){
	    		 logger.debug("courseid in request is invalid");
	    	 }
	    	 response.setParameters(EzedTransactionCodes.INVALID_COURSE_ID, "Invalid Course Id");
	    	 return RuleDecisionKey.ON_FAILURE;
	     }
	     
	     String userid = messageData.getParameter("email");
	     if(userid == null || userid.trim().compareTo("")==0){
	    	 if(logger.isDebugEnabled()){
	    		 logger.debug("userid in request is invalid");
	    	 }
	    	 response.setParameters(EzedTransactionCodes.INVALID_USERID, "Invalid User Id");
	    	 return RuleDecisionKey.ON_FAILURE;
	     }
	     Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
	     
	    boolean isCoursePurchased = EzedHelper.checkIfCourseAlreadyPurchased(dbConnName, logger, userid, courseId);
	   
	    if(!isCoursePurchased){
	    	 response.setParameters(EzedTransactionCodes.INVALID_COURSE_ID, "Course Not Purchased By User");
	    	 return RuleDecisionKey.ON_FAILURE;
	    }
	    PreparedStatement ps = null;
	     ResultSet rs = null;
	     ResultSet rs1 = null;
	     String[] chapterListArr = null;

	     
	     ArrayList<EnrolledCourseChapter> chapterList = new ArrayList<EnrolledCourseChapter>();
	     EnrolledCoursePath enrolledPath = new EnrolledCoursePath();
	     int sequenceno =0;
	     try{
	     conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchEnrolledCoursePath");
         ps.setString(1, courseId);
         rs = ps.executeQuery();
         	while(rs.next())
         	{
         		EnrolledCourseChapter  enrolledCourseChapter = new EnrolledCourseChapter();
         		enrolledCourseChapter.setChapterId(rs.getString("chapterid"));
         		enrolledCourseChapter.setChapterName(rs.getString("chaptername"));
         		enrolledCourseChapter.setTestId(rs.getString("testid"));
         		enrolledCourseChapter.setSequenceNo(rs.getInt("sequenceno"));
         		sequenceno = enrolledCourseChapter.getSequenceNo();
         		enrolledCourseChapter.setTestName(rs.getString("testname"));
         		chapterList.add(enrolledCourseChapter);
         		
         	}
           	ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchDefLMRQuestionSolutionFromCourseList");
         	 ps.setString(1, courseId);
             rs1 = ps.executeQuery();
             if (rs1.next())
             {
            		EnrolledCourseChapter enrolledCourseChapterForQuestion = new EnrolledCourseChapter();
             		enrolledCourseChapterForQuestion.setQuestionPaperId(rs1.getString("questionpaperid"));
             		if(enrolledCourseChapterForQuestion.getQuestionPaperId()!= null)
             		{
    	         		enrolledCourseChapterForQuestion.setQuestionPaperName(rs1.getString("questionpapername"));
    	         		enrolledCourseChapterForQuestion.setSequenceNo(++sequenceno);
    	          		chapterList.add(enrolledCourseChapterForQuestion);
             		}
              		
             		EnrolledCourseChapter enrolledCourseChapterForSolvedPaper = new EnrolledCourseChapter();
             		enrolledCourseChapterForSolvedPaper.setSolvedPaperId(rs1.getString("solvedpaperid"));
             		if(enrolledCourseChapterForSolvedPaper.getSolvedPaperId()!=null)
             		{
    	            	enrolledCourseChapterForSolvedPaper.setSolvedPaperName(rs1.getString("solvedpapername"));
    	            	enrolledCourseChapterForSolvedPaper.setSequenceNo(++sequenceno);
    	          		chapterList.add(enrolledCourseChapterForSolvedPaper);
             		}
          		EnrolledCourseChapter  enrolledCourseChapter = new EnrolledCourseChapter();
          		
          		enrolledCourseChapter.setLmr(rs1.getString("lmr"));
          		if(enrolledCourseChapter.getLmr()!=null){
	          		enrolledCourseChapter.setLmrName(rs1.getString("lmrname"));
	                enrolledCourseChapter.setSequenceNo(++sequenceno);
	         		chapterList.add(enrolledCourseChapter);
          		}
         	
             }
             
         	
         	enrolledPath.setCoursepath(chapterList);
         	enrolledPath.setCourseId(courseId);
	     }
	     catch (SQLException e)
         {
            logger.error("Error while fetching course path details", e);
            response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching course path details " + e.getMessage());
            return RuleDecisionKey.ON_FAILURE;
         }
         catch (Exception e)
         {
            logger.error("Error while fetching test details", e);
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching course path details" + e.getMessage());
            return RuleDecisionKey.ON_FAILURE;
         }
         finally
         {
            if (rs != null)
            {
               try
               {
                  rs.close();
               }
               catch (SQLException e)
               {
            	   logger.error("Error while fetching course path details", e);
                   response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching course path " + e.getMessage());
                   return RuleDecisionKey.ON_FAILURE;
               }
            }
            if (rs1 != null)
            {
               try
               {
                  rs1.close();
               }
               catch (SQLException e)
               {
            	   logger.error("Error while fetching test details", e);
                   response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching course path " + e.getMessage());
                   return RuleDecisionKey.ON_FAILURE;
               }
            }
            DbConnectionManager.releaseConnection(dbConnName, conn);
         }
		 response.setResponseObject(enrolledPath);
		 response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
		 return RuleDecisionKey.ON_SUCCESS;
	}
	

}
