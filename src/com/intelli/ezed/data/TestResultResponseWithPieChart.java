package com.intelli.ezed.data;

import com.intelli.ezed.analytics.data.TestResultPieChartData;

public class TestResultResponseWithPieChart extends ResponseObject
{

   private TestResultResponse testResultResponse = null;
   private TestResultPieChartData testResultPieChartData = null;


   @Override
   public void prepareResponse() throws Exception
   {
      this.getTestResultResponse().prepareResponse();
      this.getTestResultPieChartData().prepareResponse();
      this.put("testresultdetails", getTestResultResponse());
      this.put("pieChart", getTestResultPieChartData());
   }


   public TestResultResponse getTestResultResponse()
   {
      return testResultResponse;
   }


   public void setTestResultResponse(TestResultResponse testResultResponse)
   {
      this.testResultResponse = testResultResponse;
   }


   public TestResultPieChartData getTestResultPieChartData()
   {
      return testResultPieChartData;
   }


   public void setTestResultPieChartData(TestResultPieChartData testResultPieChartData)
   {
      this.testResultPieChartData = testResultPieChartData;
   }

}
