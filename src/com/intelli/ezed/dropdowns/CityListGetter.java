package com.intelli.ezed.dropdowns;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.a.helper.utils.StringSplitter;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.dropdowns.data.DropDownListData;
import com.intelli.ezed.dropdowns.data.SingleDropDownData;

public class CityListGetter extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;

   public CityListGetter(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      String countryIdList = messageData.getParameter("countryname");
      if (countryIdList == null || countryIdList.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Country id is invalid " + countryIdList);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid country ids");
         return RuleDecisionKey.ON_FAILURE;
      }

      String stateIdList = messageData.getParameter("statename");
      if (stateIdList == null || stateIdList.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("State id is invalid " + stateIdList);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid state ids");
         return RuleDecisionKey.ON_FAILURE;
      }

      String[] countryIds = StringSplitter.splitString(countryIdList.trim(), ",");
      String[] stateIds = StringSplitter.splitString(stateIdList.trim(), ",");
      Connection conn = null;
      PreparedStatement ps = null;
      ResultSet rs = null;
      DropDownListData dropDownListData = new DropDownListData();
      dropDownListData.setName("cityIdMapList");

      for (int i = 0; i < countryIds.length; i++)
      {
         for (int j = 0; j < stateIds.length; j++)
         {
            conn = DbConnectionManager.getConnectionByName(dbConnName);
            try
            {
               ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchCityDropDownList");
               ps.setString(1, countryIds[i]);
               ps.setString(2, stateIds[j]);
               rs = ps.executeQuery();
               SingleDropDownData data = null;
               while (rs.next())
               {
                  data = new SingleDropDownData();
                  data.setParam(rs.getString("cityid"));
                  data.setValue(rs.getString("cityname"));
                  dropDownListData.getDropDownDatas().add(data);
               }
            }
            catch (SQLException e)
            {
               logger.error("Error while fetching states for state id drop downs", e);
               response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while populating course list drop down");
               return RuleDecisionKey.ON_FAILURE;
            }
            catch (Exception e)
            {
               logger.error("Error while fetching states for state id drop downs", e);
               response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while populating course list drop down");
               return RuleDecisionKey.ON_FAILURE;
            }
            finally
            {
               if (rs != null)
               {
                  try
                  {
                     rs.close();
                  }
                  catch (SQLException e)
                  {
                     logger.error("Error while closing result set while populating drop down for states");
                  }
               }
               DbConnectionManager.releaseConnection(dbConnName, conn);
            }
         }
      }

      response.setStatusCodeSend(false);
      response.setResponseObject(dropDownListData);
      response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
      return RuleDecisionKey.ON_SUCCESS;
   }

}
