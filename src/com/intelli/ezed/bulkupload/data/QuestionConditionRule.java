package com.intelli.ezed.bulkupload.data;

import org.apache.commons.digester3.ObjectCreateRule;
import org.xml.sax.Attributes;

import com.intelli.ezed.data.FIBData;
import com.intelli.ezed.data.MCQData;
import com.intelli.ezed.data.MTFData;

public class QuestionConditionRule extends ObjectCreateRule
{
   public QuestionConditionRule()
   {
      super(Object.class);
   }

   @Override
   public void begin(String namespace, String name, Attributes attributes) throws Exception
   {
      String type = attributes.getValue("type");
      QuizDetails quizDetails = (QuizDetails) getDigester().pop();
      if (type.toUpperCase().compareTo("MCQ") == 0)
      {
         //         QuizDetails quizDetails = (QuizDetails) getDigester().pop();
         System.out.println("Quiz details " + quizDetails);
         MCQData mcqData = new MCQData();
         quizDetails.addQuestion(mcqData);
         getDigester().push(quizDetails);
         getDigester().push(mcqData);
      }
      else if (type.toUpperCase().compareTo("MTF") == 0)
      {
         //         QuizDetails quizDetails = (QuizDetails) getDigester().pop();
         System.out.println("Quiz details " + quizDetails);
         MTFData mtfData = new MTFData();
         quizDetails.addQuestion(mtfData);
         getDigester().push(quizDetails);
         getDigester().push(mtfData);
      }
      else if (type.toUpperCase().compareTo("FIB") == 0)
      {
         //         QuizDetails quizDetails = (QuizDetails) getDigester().pop();
         System.out.println("Quiz details " + quizDetails);
         FIBData fibData = new FIBData();
         quizDetails.addQuestion(fibData);
         getDigester().push(quizDetails);
         getDigester().push(fibData);
      }
      else
      {
         throw new Exception("Proper question type not mentioned");
      }
   }

   //   @Override
   //   public void end() throws Exception
   //   {
   //      digester.pop();
   //   }

   @Override
   public void end(String namespace, String name) throws Exception
   {
      super.end(namespace, name);
   }
}
