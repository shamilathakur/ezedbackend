package com.intelli.ezed.db;

import java.util.HashMap;
import java.util.Map;

import org.sabre.exceptions.InputProcessorException;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.LinkedProcessorManager;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.GoalModificationData;
import com.intelli.ezed.data.Response;

public class NewContentInLearningPathHandler extends SingletonRule
{

   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String goalAdditionProcessor;

   public NewContentInLearningPathHandler(String ruleName, String goalAdditionProcessor)
   {
      super(ruleName);
      this.goalAdditionProcessor = goalAdditionProcessor;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      String chapterId = messageData.getParameter("chapterid");
      if (chapterId == null || chapterId.trim().compareTo("") == 0 || chapterId.trim().compareTo("null") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Chapter id is invalid " + chapterId);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid chapter id");
         return RuleDecisionKey.ON_FAILURE;
      }

      String contentId = messageData.getParameter("contentid");
      if (contentId == null || contentId.trim().compareTo("") == 0 || contentId.trim().compareTo("null") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Content id is invalid " + chapterId);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid content id");
         return RuleDecisionKey.ON_FAILURE;
      }

      String contentShowFlagString = messageData.getParameter("showflag");
      if (contentShowFlagString == null || contentShowFlagString.trim().compareTo("") == 0 || contentShowFlagString.trim().compareTo("null") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Content show flag is missing " + contentShowFlagString);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid content show flag");
         return RuleDecisionKey.ON_FAILURE;
      }

      int contentShowFlag = 0;
      try
      {
         contentShowFlag = Integer.parseInt(contentShowFlagString);
      }
      catch (Exception e)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Content show flag is invalid " + contentShowFlagString);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid content show flag");
         return RuleDecisionKey.ON_FAILURE;
      }

      GoalModificationData data = new GoalModificationData();
      data.setChapterId(chapterId);
      data.getContentIdList().add(contentId);
      data.getContentshowFlag().add(contentShowFlagString);

      Map<Object, Object> mapData = new HashMap<Object, Object>();
      mapData.put(EzedKeyConstants.GOAL_MODIFICATION_DATA, data);
      try
      {
         LinkedProcessorManager.addTask(goalAdditionProcessor, mapData);
      }
      catch (InputProcessorException e)
      {
         logger.error("Error while starting goal addition processor", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error occurred. Pls try again later");
         return RuleDecisionKey.ON_FAILURE;
      }
      response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
      return RuleDecisionKey.ON_SUCCESS;
   }
}
