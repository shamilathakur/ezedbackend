package com.intelli.ezed.utils;

import org.sabre.io.transport.http.data.HttpTransactionData;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;

public class DummyResponseCreatorRule extends SingletonRule
{

   public DummyResponseCreatorRule(String ruleName)
   {
      super(ruleName);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      HttpTransactionData transData = new HttpTransactionData();
      transData.setRequestType(HttpTransactionData.POST);
      //      transData.setHeaderMap(new HashMap<String, List<String>>());
      //      transData.addHeader("content-type", "application/json");
      transData.setContent("newdata".getBytes());
      flowContext.putIntoContext(HttpTransactionData.WRITE_RESP_DATA, transData);
      return RuleDecisionKey.ON_SUCCESS;
   }

}
