package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.ContentData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class ContentDetailsFetcherM extends SingletonRule {

	private String dbConnName;
	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

	public ContentDetailsFetcherM(String ruleName, String dbConnName) {
		super(ruleName);
		this.dbConnName = dbConnName;
	}
	
	@Override
	   public RuleDecisionKey executeRule(FlowContext flowContext)
	   {
		
		Response response = (Response) flowContext
				.getFromContext(EzedKeyConstants.EZED_RESPONSE);
		MessageData messageData = (MessageData) flowContext
				.getFromContext(MessageData.class.getName());
		
		
		if (messageData.getParameter("contentId") == null && messageData.getParameter("contentId").trim().compareTo("") == 0) {
			 if (logger.isDebugEnabled())
	         {
	            logger.debug("ContentId in request is null or blank");
	         }
	         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Blank contentId in query");
	         return RuleDecisionKey.ON_FAILURE;
		}
		
	      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
	      PreparedStatement ps = null;
	      ResultSet rs = null;
	      try
	      {
	         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchContentDetailsFromContentId");
	         ps.setString(1, messageData.getParameter("contentId"));
	         rs = ps.executeQuery();
	        
	         while (rs.next())
	         {
	             ContentData contentObj = new ContentData();
		         contentObj.setContentId(rs.getString("contentid"));
		         contentObj.setContentName(rs.getString("contentname"));
		         contentObj.setContentLink(rs.getString("link"));
		         contentObj.setContentDescription(rs.getString("description"));
		         contentObj.setContentType(rs.getInt("contenttype"));		        
		         response.setResponseObject(contentObj);
		         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
		         return RuleDecisionKey.ON_SUCCESS;
	         }
	      }
	      catch (SQLException e)
	      {
	         logger.error("DB error while fetching content data", e);
	         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "DB error while fetching content data");
	         return RuleDecisionKey.ON_FAILURE;
	      }
	      catch (Exception e)
	      {
	         logger.error("Error while fetching content data", e);
	         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching content data");
	         return RuleDecisionKey.ON_FAILURE;
	      }
	      finally
	      {
	         if (rs != null)
	         {
	            try
	            {
	               rs.close();
	            }
	            catch (SQLException e)
	            {
	               logger.error("Error while closing result set while fetching content data", e);
	            }
	         }
	         DbConnectionManager.releaseConnection(dbConnName, conn);
	      }
	      
	      return RuleDecisionKey.ON_SUCCESS;
	   }
	
}
