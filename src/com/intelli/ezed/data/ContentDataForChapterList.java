package com.intelli.ezed.data;

import java.util.ArrayList;

public class ContentDataForChapterList extends ResponseObject{

	private String chapterId;
	private ArrayList<ContentDataForChapter> contentDataForChapter ;
	
	@Override
	public void prepareResponse() throws Exception {

		ContentDataForChapter[] contentForChapterList = new ContentDataForChapter[getContentDataForChapter().size()];
		if(contentForChapterList != null ){
			for(int i=0;i<contentForChapterList.length; i++)
			{
				contentForChapterList[i] = getContentDataForChapter().get(i);
			}
		}
		this.put("contentDataForChapter", contentForChapterList);
		this.put("chapterId", getChapterId());
	}

	/**
	 * @return the chapterId
	 */
	public String getChapterId() {
		return chapterId;
	}

	/**
	 * @param chapterId the chapterId to set
	 */
	public void setChapterId(String chapterId) {
		this.chapterId = chapterId;
	}

	/**
	 * @return the contentDataForChapter
	 */
	public ArrayList<ContentDataForChapter> getContentDataForChapter() {
		return contentDataForChapter;
	}

	/**
	 * @param contentDataForChapter the contentDataForChapter to set
	 */
	public void setContentDataForChapter(ArrayList<ContentDataForChapter> contentDataForChapter) {
		this.contentDataForChapter = contentDataForChapter;
	}

}
