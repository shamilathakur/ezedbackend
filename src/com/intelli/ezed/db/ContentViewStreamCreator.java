package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.bulkupload.data.ContentDetails;
import com.intelli.ezed.data.EventData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.UserProfile;

public class ContentViewStreamCreator extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public ContentViewStreamCreator(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      EventData eventData = (EventData) flowContext.getFromContext(EzedKeyConstants.EVENT_OBJECT);
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      UserProfile profile = (UserProfile) flowContext.getFromContext(EzedKeyConstants.USER_DETAILS);

      Connection conn = null;
      PreparedStatement ps = null;
      ResultSet rs = null;


      StringBuffer sb = new StringBuffer();
      sb.append(profile.getName());
      boolean isTest = false;

      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchContentDetailsForId");
         ps.setString(1, eventData.getContentId());
         rs = ps.executeQuery();
         if (rs.next())
         {
            int contentType = rs.getInt("contenttype");
            if (contentType == ContentDetails.CONTENT_QUIZ || contentType == ContentDetails.CONTENT_TEST || contentType == ContentDetails.CONTENT_TEST_1)
            {
               sb.append(" took ").append(rs.getString("contentname"));
            }
            else
            {
               sb.append(" viewed ").append(rs.getString("contentname"));
            }
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Content name could not be found for id " + eventData.getContentId());
            }
            isTest = true;
            //            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Content details could not be found");
            //            return RuleDecisionKey.ON_FAILURE;
         }
      }
      catch (Exception e)
      {
         logger.error("Error while fetching content details for id " + eventData.getContentId(), e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Content details could not be fetched");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
               logger.error("Error while closing result set while fetching content details", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      if (isTest)
      {
         try
         {
            conn = DbConnectionManager.getConnectionByName(dbConnName);
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchTestDetails");
            ps.setString(1, eventData.getContentId());
            rs = ps.executeQuery();
            if (rs.next())
            {
               sb.append(" took ").append(rs.getString("testname"));
            }
            else
            {
               if (logger.isDebugEnabled())
               {
                  logger.debug("Test name could not be found for id " + eventData.getContentId());
               }
               response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Test details could not be found");
               return RuleDecisionKey.ON_FAILURE;
            }
         }
         catch (Exception e)
         {
            logger.error("Error while fetching test details for id " + eventData.getContentId(), e);
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Test details could not be fetched");
            return RuleDecisionKey.ON_FAILURE;
         }
         finally
         {
            if (rs != null)
            {
               try
               {
                  rs.close();
               }
               catch (Exception e)
               {
                  logger.error("Error while closing result set while fetching test details", e);
               }
            }
            DbConnectionManager.releaseConnection(dbConnName, conn);
         }
      }

      if (eventData.getChapterId() != null)
      {
         try
         {
            conn = DbConnectionManager.getConnectionByName(dbConnName);
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchChapterDetail");
            ps.setString(1, eventData.getChapterId());
            rs = ps.executeQuery();
            if (rs.next())
            {
               sb.append(" in chapter ").append(rs.getString("chaptername"));
            }
            else
            {
               if (logger.isDebugEnabled())
               {
                  logger.debug("Chapter name could not be found for id " + eventData.getChapterId());
               }
               response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Chapter details could not be found");
               return RuleDecisionKey.ON_FAILURE;
            }
         }
         catch (Exception e)
         {
            logger.error("Error while fetching chapter details for id " + eventData.getChapterId(), e);
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Chapter details could not be fetched");
            return RuleDecisionKey.ON_FAILURE;
         }
         finally
         {
            if (rs != null)
            {
               try
               {
                  rs.close();
               }
               catch (Exception e)
               {
                  logger.error("Error while closing result set while fetching chapter details", e);
               }
            }
            DbConnectionManager.releaseConnection(dbConnName, conn);
         }
      }

      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchCourseDetail");
         ps.setString(1, eventData.getCourseId());
         rs = ps.executeQuery();
         if (rs.next())
         {
            sb.append(" in course ").append(rs.getString("coursename"));
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Course name could not be found for id " + eventData.getCourseId());
            }
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Course details could not be found");
            return RuleDecisionKey.ON_FAILURE;
         }
      }
      catch (Exception e)
      {
         logger.error("Error while fetching course details for id " + eventData.getCourseId(), e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Course details could not be fetched");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
               logger.error("Error while closing result set while fetching course details", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
      eventData.setStreamString(sb.toString());
      return RuleDecisionKey.ON_SUCCESS;
   }
}
