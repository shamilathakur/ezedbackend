package com.intelli.ezed.data;

/*
 * This class is used by com.intelli.ezed.db.ContentDetailsFromChapterFetcherM 
 * To get list of content ids for a chapter along with the contentshowflag for a
 * particular chapterid
 */
public class ContentDataForChapter  {

	
	private String contentId;
	private String contentName;
	private String contentLink;
	private int contentType;
	private String contentshowflag;
	private int sequenceno;
	
	/**
	 * @return the contentId
	 */
	public String getContentId() {
		return contentId;
	}
	/**
	 * @param contentId the contentId to set
	 */
	public void setContentId(String contentId) {
		this.contentId = contentId;
	}
	/**
	 * @return the contentName
	 */
	public String getContentName() {
		return contentName;
	}
	/**
	 * @param contentName the contentName to set
	 */
	public void setContentName(String contentName) {
		this.contentName = contentName;
	}
	/**
	 * @return the contentType
	 */
	public int getContentType() {
		return contentType;
	}
	/**
	 * @param contentType the contentType to set
	 */
	public void setContentType(int contentType) {
		this.contentType = contentType;
	}
	/**
	 * @return the contentLink
	 */
	public String getContentLink() {
		return contentLink;
	}
	/**
	 * @param contentLink the contentLink to set
	 */
	public void setContentLink(String contentLink) {
		this.contentLink = contentLink;
	}
	/**
	 * @return the contentshowflag
	 */
	public String getContentshowflag() {
		return contentshowflag;
	}
	/**
	 * @param contentshowflag the contentshowflag to set
	 */
	public void setContentshowflag(String contentshowflag) {
		this.contentshowflag = contentshowflag;
	}
	/**
	 * @return the sequenceno
	 */
	public int getSequenceno() {
		return sequenceno;
	}
	/**
	 * @param sequenceno the sequenceno to set
	 */
	public void setSequenceno(int sequenceno) {
		this.sequenceno = sequenceno;
	}
	
}
