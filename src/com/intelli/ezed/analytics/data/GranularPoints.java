package com.intelli.ezed.analytics.data;

import com.intelli.ezed.data.ResponseObject;

public class GranularPoints extends ResponseObject
{
   private String name;
   private int pointsEarnedNow;
   private int pointsEarnedPrevious;

   public GranularPoints(String name, int pointsEarnedNow, int pointsEarnedPreviously)
   {
      this.name = name;
      this.pointsEarnedNow = pointsEarnedNow;
      this.pointsEarnedPrevious = pointsEarnedPreviously;
   }

   public String getName()
   {
      return name;
   }

   public void setName(String name)
   {
      this.name = name;
   }

   public int getPointsEarnedNow()
   {
      return pointsEarnedNow;
   }

   public void setPointsEarnedNow(int pointsEarnedNow)
   {
      this.pointsEarnedNow = pointsEarnedNow;
   }

   public int getPointsEarnedPrevious()
   {
      return pointsEarnedPrevious;
   }

   public void setPointsEarnedPrevious(int pointsEarnedPrevious)
   {
      this.pointsEarnedPrevious = pointsEarnedPrevious;
   }

   @Override
   public void prepareResponse() throws Exception
   {
      int[] dataValues = new int[2];
      dataValues[0] = getPointsEarnedNow();
      dataValues[1] = getPointsEarnedPrevious();
      this.put("name", getName());
      this.put("data", dataValues);
   }

}
