package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.FrontendFetchData;
import com.intelli.ezed.data.Response;

public class CourseFilterListFetcher extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public CourseFilterListFetcher(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      String univName = messageData.getParameter("univname");
      if (univName == null || univName.compareTo("") == 0 || univName.compareToIgnoreCase("null") == 0)
      {
         univName = null;
      }

      String degree = messageData.getParameter("degree");
      if (degree == null || degree.compareTo("") == 0 || degree.compareToIgnoreCase("null") == 0)
      {
         degree = null;
      }

      String stream = messageData.getParameter("stream");
      if (stream == null || stream.compareTo("") == 0 || stream.compareToIgnoreCase("null") == 0)
      {
         stream = null;
      }

      String year = messageData.getParameter("year");
      if (year == null || year.compareTo("") == 0 || year.compareToIgnoreCase("null") == 0)
      {
         year = null;
      }

      String semester = messageData.getParameter("semester");
      if (semester == null || semester.compareTo("") == 0 || semester.compareToIgnoreCase("null") == 0)
      {
         semester = null;
      }
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      String[] courseArr = null;
      FrontendFetchData courseData = new FrontendFetchData();
      response.setResponseObject(courseData);
      response.setStatusCodeSend(false);
      courseData.setAttribName("aaData");
      ArrayList<String[]> courses = new ArrayList<String[]>();
      int publishFlag = 0;
      try
      {
         if (univName == null)
         {
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchCourseListAdmin");
         }
         else if (degree == null)
         {
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchCourseListAdminFilterUniversity");
            ps.setString(1, univName);
         }
         else if (stream == null)
         {
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchCourseListAdminFilterDegree");
            ps.setString(1, univName);
            ps.setString(2, degree);
         }
         else if (year == null)
         {
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchCourseListAdminFilterStream");
            ps.setString(1, univName);
            ps.setString(2, degree);
            ps.setString(3, stream);
         }
         else if (semester == null)
         {
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchCourseListAdminFilterYear");
            ps.setString(1, univName);
            ps.setString(2, degree);
            ps.setString(3, stream);
            ps.setString(4, year);
         }
         else
         {
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchCourseListAdminFilter");
            ps.setString(1, univName);
            ps.setString(2, degree);
            ps.setString(3, stream);
            ps.setString(4, year);
            ps.setString(5, semester);
         }
         rs = ps.executeQuery();
         int i = 1;
         while (rs.next())
         {
            courseArr = new String[4];
            courseArr[0] = Integer.toString(i);
            courseArr[1] = rs.getString("courseid");
            courseArr[2] = rs.getString("coursename");
            publishFlag = rs.getInt("publishflag");
            if (publishFlag == 0)
            {
               courseArr[3] = "INACTIVE";
            }
            else
            {
               courseArr[3] = "ACTIVE";
            }
            i++;
            courses.add(courseArr);
         }
         courseData.setAttribList(courses);
         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
      }
      catch (SQLException e)
      {
         logger.error("DB error while fetching list of courses", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "DB error while fetching courses");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching list of courses", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching courses");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing result set while fetching course list", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
      return RuleDecisionKey.ON_SUCCESS;
   }
}
