package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.FrontendFetchData;
import com.intelli.ezed.data.Response;

public class ScratchCardsListFetcher extends SingletonRule {

	private String dbConnName;
	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat sourceFormat1 = new SimpleDateFormat("yyyyMM");
	SimpleDateFormat source = new SimpleDateFormat("yyyyMMddHHmmss");
	SimpleDateFormat format2 = new SimpleDateFormat("MM-yyyy");

	private static final String SCRATCH_CARD_LIST_QUERY = "SELECT sc.srno,sc.scratchcardid,sc.courseid,sc.coursetype,"
			+ "sc.validtill,sc.assignedstatus,sc.usedstatus,sc.price,cour.coursename,p.partnername,p.status,pscm.partnerid "
			+ "from scratchcardlist sc left join courselist cour on sc.courseid=cour.courseid left join "
			+ "partnerscratchcardmapping pscm on sc.scratchcardid=pscm.scratchcardid left join partnerlist p "
			+ "on pscm.partnerid=p.partnerid ";

	public ScratchCardsListFetcher(String ruleName, String dbConnName) {
		super(ruleName);
		this.dbConnName = dbConnName;
	}

	@Override
	public RuleDecisionKey executeRule(FlowContext flowContext) {
		Response response = (Response) flowContext
				.getFromContext(EzedKeyConstants.EZED_RESPONSE);
		Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
		MessageData messageData = (MessageData) flowContext
				.getFromContext(MessageData.class.getName());
		PreparedStatement ps = null;
		ResultSet rs = null;
		String finalQuery = SCRATCH_CARD_LIST_QUERY + "";
		StringBuilder sb = new StringBuilder("");

		String startdate = messageData.getParameter("startdate");
		String enddate = messageData.getParameter("enddate");
		if ((startdate != null && startdate.compareTo("null") != 0 && startdate
				.compareTo("") != 0) && enddate.compareTo("null") == 0) {
			enddate = source.format(new Date());
		}
		String coursetype = messageData.getParameter("coursetype");
		String courseid = messageData.getParameter("courseid");
		String createdBy = messageData.getParameter("createdby");
		String partnerid = messageData.getParameter("partnerid");
		int cardstatus = Integer.parseInt(messageData
				.getParameter("cardstatus"));
		int statustype = Integer.parseInt(messageData
				.getParameter("statustype"));

		String qryDate = " sc.generatedate between '" + startdate + "' and '"
				+ enddate + "'";
		String qryCourseId = " sc.courseid='" + courseid + "'";
		String qryAdmin = " sc.generatedby='" + createdBy + "'";
		String qryCourseType = " sc.coursetype='" + coursetype + "'";
		String qryActiveCard = " pscm.activestatus='" + cardstatus + "'";
		String qryAssignedCard = " sc.assignedstatus='" + cardstatus + "'";
		String qryUsedCard = " sc.usedstatus='" + cardstatus + "'";
		String qryExpiredCard = " sc.validtill <'"
				+ sourceFormat1.format(new Date()) + "'";
		String qryExpiredCard1 = " sc.validtill >'"
				+ sourceFormat1.format(new Date()) + "'";
		String qryPartnerId = " p.partnerid= '" + partnerid + "'";

		if (statustype != 0) {
			if (statustype == 1) {
				if (!(sb.length() > 0)) {
					sb.append(" where");
					sb.append(qryActiveCard);
				} else {
					sb.append(" and");
					sb.append(qryActiveCard);
				}
			} else if (statustype == 2) {
				if (!(sb.length() > 0)) {
					sb.append(" where");
					sb.append(qryAssignedCard);
				} else {
					sb.append(" and");
					sb.append(qryAssignedCard);
				}
			} else if (statustype == 3) {
				if (!(sb.length() > 0)) {
					sb.append(" where");
					sb.append(qryUsedCard);
				} else {
					sb.append(" and");
					sb.append(qryUsedCard);
				}
			} else if (statustype == 4) {
				if (cardstatus == 0) {
					if (!(sb.length() > 0)) {
						sb.append(" where");
						sb.append(qryExpiredCard1);
					} else {
						sb.append(" and");
						sb.append(qryExpiredCard1);
					}

				} else if (cardstatus == 1) {
					if (!(sb.length() > 0)) {
						sb.append(" where");
						sb.append(qryExpiredCard);
					} else {
						sb.append(" and");
						sb.append(qryExpiredCard);
					}

				}
			}
		}

		if (startdate != null && startdate.compareTo("null") != 0
				&& startdate.compareTo("") != 0) {
			if (!(sb.length() > 0)) {
				sb.append(" where");
				sb.append(qryDate);
			} else {
				sb.append(" and");
				sb.append(qryDate);
			}
		}
		if (createdBy.compareTo("null") != 0 && createdBy.compareTo("") != 0
				&& createdBy != null) {
			if (!(sb.length() > 0)) {
				sb.append(" where");
				sb.append(qryAdmin);
			} else {
				sb.append(" and");
				sb.append(qryAdmin);
			}
		}
		if (coursetype.compareTo("null") != 0 && coursetype.compareTo("") != 0
				&& coursetype != null) {
			if (!(sb.length() > 0)) {
				sb.append(" where");
				sb.append(qryCourseType);
			} else {
				sb.append(" and");
				sb.append(qryCourseType);
			}
		}
		if (courseid.compareTo("null") != 0 && courseid.compareTo("") != 0
				&& courseid != null) {
			if (!(sb.length() > 0)) {
				sb.append(" where");
				sb.append(qryCourseId);
			} else {
				sb.append(" and");
				sb.append(qryCourseId);
			}
		}
		if (partnerid.compareTo("null") != 0 && partnerid.compareTo("") != 0
				&& partnerid != null) {
			if (!(sb.length() > 0)) {
				sb.append(" where");
				sb.append(qryPartnerId);
			} else {
				sb.append(" and");
				sb.append(qryPartnerId);
			}
		}

		finalQuery = finalQuery + sb.toString();

		System.out.println("Final Query : " + finalQuery);
		String scratchCardData[];
		FrontendFetchData scratchCardListObj = new FrontendFetchData();
		response.setResponseObject(scratchCardListObj);
		response.setStatusCodeSend(false);
		scratchCardListObj.setAttribName("aaData");
		ArrayList<String[]> scratchCardDataList = new ArrayList<String[]>();
		try {
			ps = conn.prepareStatement(finalQuery);
			rs = ps.executeQuery();
			while (rs.next()) {
				scratchCardData = new String[10];
				scratchCardData[0] = rs.getInt("srno") + "";
				scratchCardData[1] = rs.getString("scratchcardid");
				scratchCardData[2] = rs.getString("coursename");
				int type = rs.getInt("coursetype");
				if (type == 1) {
					scratchCardData[3] = "Content Only";
				} else if (type == 2) {
					scratchCardData[3] = "Test Only";
				} else if (type == 3) {
					scratchCardData[3] = "Full Course";
				}
				/*
				 * System.out.println("Before foratting : " +
				 * rs.getString("validtill"));
				 */
				Date validTill = sourceFormat1.parse(rs.getString("validtill")
						.substring(0, 6));
				scratchCardData[4] = format2.format(validTill);
				/*
				 * System.out.println("After Formatting : " +
				 * scratchCardData[4]);
				 */
				int assignedstatus = rs.getInt("assignedstatus");
				if (assignedstatus == 0) {
					scratchCardData[5] = "Unassigned";
					scratchCardData[8] = "";
				} else {
					scratchCardData[5] = "Assigned";
					scratchCardData[8] = rs.getString("partnername");
				}
				int usedstatus = rs.getInt("usedstatus");
				if (usedstatus == 0) {
					scratchCardData[6] = "Unused";
				} else {
					scratchCardData[6] = "Used";
				}
				scratchCardData[7] = rs.getInt("price") + "";
				int partnerStatus = rs.getInt("status");
				if(partnerStatus == 0)
				{
					scratchCardData[9] = "Inactive";
				}
				else
				{
					scratchCardData[9] = "Active";
				}

				scratchCardDataList.add(scratchCardData);
			}
			scratchCardListObj.setAttribList(scratchCardDataList);
			response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
		} catch (SQLException e) {
			logger.error(
					"Error while fetching Scratch card details from db : ", e);
			response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED,
					"Error while fetching Scratch Card details");
			return RuleDecisionKey.ON_FAILURE;
		} catch (Exception e) {
			logger.error(
					"Unknown Error while fetching scratch card details : ", e);
			response.setParameters(EzedTransactionCodes.INTERNAL_ERROR,
					"Error while fetching scratch card details");
			return RuleDecisionKey.ON_FAILURE;
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					logger.error(
							"Error while closing result set while fetching scratch card list",
							e);
				}
			}
			DbConnectionManager.releaseConnection(dbConnName, conn);
		}

		return RuleDecisionKey.ON_SUCCESS;
	}
}