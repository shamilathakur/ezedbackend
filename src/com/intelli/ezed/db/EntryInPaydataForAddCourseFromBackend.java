package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.ManageUserCourseFromBackendBean;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.PaymentIdData;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.UserProfile;

public class EntryInPaydataForAddCourseFromBackend extends SingletonRule {

	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
	private String dbConnName;
	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

	public EntryInPaydataForAddCourseFromBackend(String ruleName,
			String dbConnName) {
		super(ruleName);
		this.dbConnName = dbConnName;
	}

	@Override
	public RuleDecisionKey executeRule(FlowContext flowContext) {

		Response response = (Response) flowContext
				.getFromContext(EzedKeyConstants.EZED_RESPONSE);
		ManageUserCourseFromBackendBean data = (ManageUserCourseFromBackendBean) flowContext
				.getFromContext(EzedKeyConstants.ADD_COURSE_TO_USER_FROM_ADMIN);
		PaymentIdData data1 = (PaymentIdData) flowContext
				.getFromContext(EzedKeyConstants.CART_PAYMENT_DATA);
		UserProfile userProfile = new UserProfile();
		userProfile.setUserid(data.getUserId());

		System.out.println("In Class : " + this.getClass().getName());
		System.out.print("USERID : " + data.getUserId() + "  Sessionid : "
				+ data.getSessionId());

		Connection conn = DbConnectionManager.getConnectionByName(dbConnName);

		PreparedStatement ps = null;
		int rs;

		try {
			ps = DbConnectionManager.getPreparedStatement(dbConnName, conn,
					"EntryInPaydataForAddCourseFromBackend");
			ps.setString(1, data1.getAppId());
			ps.setString(2, data1.getPaymentId());
			ps.setString(3, data.getUserId());
			ps.setString(4, data.getSessionId());
			ps.setString(5, sdf.format(new Date()));
			ps.setString(6, "1"); // set payment status here..

			StringBuffer sb = new StringBuffer();
			sb.append(data.getCourseId());
			sb.append(",");
			sb.append(data.getCourseType());
			sb.append(",");
			sb.append(data.getStartDate());
			sb.append(",");
			sb.append(data.getEndDate());

			ps.setString(7, sb.toString());
			ps.setString(8, "1"); // set cartstatus here ..
			ps.setInt(9, data.getPaidAmount());
			ps.setString(10, data.getAddedBy());

			System.out
					.println("SQLQyuery in EntryInPaydataForAddCourseFromBackend class : \n"
							+ ps);

			rs = ps.executeUpdate();
			if (rs > 0) {
				logger.info("Entry in the paydata table successful !");
				System.out.println("Entry in the paydata table successful !");
				userProfile.populateUserProfile(dbConnName, logger);
			} else {
				logger.info("Something went wrong while making the entry in paydata table!");
				System.out
						.println("Something went wrong while making the entry in paydata table!");
			}
		} catch (SQLException e) {
			System.out.println("SQL Error In Class : "
					+ this.getClass().getName());
			if (logger.isDebugEnabled()) {
				logger.error("SQL Error while making the entry in the paydata table for paymentId : "
						+ data1.getPaymentId());
			}
			response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED,
					"SQL Error while making the entry in the paydata table");
			return RuleDecisionKey.ON_FAILURE;
		} catch (Exception e) {
			if (logger.isDebugEnabled()) {
				logger.error("Internal Error while making the entry in the paydata table for paymentid : "
						+ data1.getPaymentId());
			}
			response.setParameters(EzedTransactionCodes.INTERNAL_ERROR,
					"Internal Error while making the entry in the paydata table");
			return RuleDecisionKey.ON_FAILURE;
		} finally {
			DbConnectionManager.releaseConnection(dbConnName, conn);
		}

		String paymentid = data1.getAppId() + data1.getPaymentId();
		System.out.println("paymentid : " + paymentid);
		flowContext.putIntoContext("paymentid", paymentid);
		flowContext.putIntoContext(EzedKeyConstants.USER_DETAILS, userProfile);
		return RuleDecisionKey.ON_SUCCESS;

	}

}
