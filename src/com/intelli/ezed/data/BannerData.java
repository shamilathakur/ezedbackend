package com.intelli.ezed.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;

import com.intelli.ezed.utils.PrepareBannerIdBackend;

public class BannerData extends ResponseObject
{
   public static final String PUBLISHED_BANNER = "PUBLISHED";
   public static final String NOT_PUBLISHED_BANNER = "NOTPUBLISHED";

   public static final int BANNER_IMAGE_TYPE = 1;
   public static final int BANNER_VIDEO_TYPE = 2;
   public static final int BANNER_YOUTUBE_TYPE = 3;

   PrepareBannerIdBackend bannerIdBackend = new PrepareBannerIdBackend("BAN2", "ezeddb");

   private String bannerid;
   private String bannertitle;
   private String bannerdesc;
   private String bannerimage;
   private String bannervideo;
   private String bannerstatus;
   private int bannerType;
   private String bannerUrl;

   public String getBannerid()
   {
      return bannerid;
   }
   public void setBannerid(String bannerid)
   {
      this.bannerid = bannerid;
   }
   public String getBannertitle()
   {
      return bannertitle;
   }
   public void setBannertitle(String bannertitle)
   {
      this.bannertitle = bannertitle;
   }
   public String getBannerdesc()
   {
      return bannerdesc;
   }
   public void setBannerdesc(String bannerdesc)
   {
      this.bannerdesc = bannerdesc;
   }
   public String getBannerimage()
   {
      return bannerimage;
   }
   public void setBannerimage(String bannerimage)
   {
      this.bannerimage = bannerimage;
   }
   public String getBannervideo()
   {
      return bannervideo;
   }
   public void setBannervideo(String bannervideo)
   {
      this.bannervideo = bannervideo;
   }
   public String getBannerstatus()
   {
      return bannerstatus;
   }
   public void setBannerstatus(String bannerstatus)
   {
      this.bannerstatus = bannerstatus;
   }

   public int insertIntoDatabase(String dbConnName) throws SQLException, Exception
   {
      Connection connection = DbConnectionManager.getConnectionByName(dbConnName);

      try
      {
         //insert into bannerdata values (?,?,?,?,?,?)
         PreparedStatement preparedStatement = DbConnectionManager.getPreparedStatement(dbConnName, connection, "InsertIntoBannerData");

         String bannerId = bannerIdBackend.getNextId();
         setBannerid(bannerId);

         preparedStatement.setString(1, getBannerid());
         preparedStatement.setString(2, getBannertitle());
         preparedStatement.setString(3, getBannerdesc());
         preparedStatement.setString(4, getBannerimage());
         preparedStatement.setInt(5, getBannerType());

         if (getBannerstatus().compareTo(PUBLISHED_BANNER) == 0)
         {
            preparedStatement.setInt(6, 0);
         }
         else
         {
            preparedStatement.setInt(6, 1);
         }
         int i = preparedStatement.executeUpdate();

         if (i == 1)
         {
            return 0;
         }
         else
         {
            return 1;
         }
      }
      catch (SQLException e)
      {
         throw e;
      }
      catch (Exception e)
      {
         throw e;
      }
   }

   @Override
   public void prepareResponse() throws Exception
   {
      this.put("bannerid", getBannerid());
      this.put("bannertitle", getBannertitle());
      this.put("bannerdesc", getBannerdesc());
      this.put("bannerdata", getBannerimage());
      this.put("bannertype", getBannerType());
      this.put("bannerstatus", getBannerstatus());
      this.put("bannerimageurl", getBannerUrl());
   }
   public int getBannerType()
   {
      return bannerType;
   }
   public void setBannerType(int bannerType)
   {
      this.bannerType = bannerType;
   }
   public String getBannerUrl()
   {
      return bannerUrl;
   }
   public void setBannerUrl(String bannerUrl)
   {
      this.bannerUrl = bannerUrl;
   }
}
