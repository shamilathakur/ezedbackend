package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.DataFiller;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.FrontendFetchData;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.utils.EzedHelper;

public class TestListFetcher extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private SimpleDateFormat sdfDb = new SimpleDateFormat("HHmmss");
   private SimpleDateFormat sdfView = new SimpleDateFormat("HH:mm:ss");

   public TestListFetcher(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      String[] testArr = null;
      FrontendFetchData testData = new FrontendFetchData();
      response.setResponseObject(testData);
      response.setStatusCodeSend(false);
      testData.setAttribName("aaData");
      ArrayList<String[]> tests = new ArrayList<String[]>();
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchTestList");
         rs = ps.executeQuery();
         int i = 1;
         String chapterId = null;
         while (rs.next())
         {
            testArr = new String[8];
            testArr[0] = Integer.toString(i);
            testArr[1] = rs.getString(2);
            testArr[2] = Integer.toString(rs.getInt(3));
            try
            {
               testArr[3] = sdfView.format(sdfDb.parse(DataFiller.getLeftZeroPadding(rs.getString(4), 6)));
            }
            catch (Exception e)
            {
               testArr[3] = "";
            }
            chapterId = rs.getString(6);
            if (chapterId == null)
            {
               testArr[4] = "COURSE";
               testArr[6] = "";
            }
            else
            {
               testArr[4] = "CHAPTER";
               testArr[6] = EzedHelper.fetchChapterNameForChapterId(dbConnName, chapterId, logger);
            }
            testArr[5] = rs.getString(7);
            testArr[7] = rs.getString(1);

            i++;
            tests.add(testArr);
         }
         testData.setAttribList(tests);
         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
      }
      catch (SQLException e)
      {
         logger.error("DB error while fetching list of tests", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "DB error while fetching tests");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching list of tests", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching tests");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing result set while fetching test list", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
      return RuleDecisionKey.ON_SUCCESS;
   }
}
