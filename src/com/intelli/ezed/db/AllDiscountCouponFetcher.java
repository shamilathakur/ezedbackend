package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.FrontendFetchData;
import com.intelli.ezed.data.Response;

/**
 * @author Kunal Description : This class is used to retrieve the details of all
 *         the generated coupons so far .. ServiceId : 3016
 */
public class AllDiscountCouponFetcher extends SingletonRule {

	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
	private String db_name;
	SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyyMMddHHmmss");
	SimpleDateFormat formatTo = new SimpleDateFormat("yyyy-MM-dd");

	public AllDiscountCouponFetcher(String ruleName, String dbname) {
		super(ruleName);
		this.db_name = dbname;
	}

	@Override
	public RuleDecisionKey executeRule(FlowContext flowContext) {

		Response response = (Response) flowContext
				.getFromContext(EzedKeyConstants.EZED_RESPONSE);

		MessageData messageData = (MessageData) flowContext
				.getFromContext(MessageData.class.getName());
		String start = messageData.getParameter("startdate");
		String end = messageData.getParameter("enddate");
		if (( start != null &&  start.compareTo("null") != 0 && start.compareTo("") != 0)
				&& (end.compareTo("null") == 0 || end == null || end.trim()
						.compareTo("") != 0)) {
			end = sourceFormat.format(new Date());
		}

		Connection conn = DbConnectionManager.getConnectionByName(db_name);
		PreparedStatement ps = null;
		ResultSet rs = null;
		String[] discData = null;
		FrontendFetchData couponData = new FrontendFetchData();
		response.setResponseObject(couponData);
		response.setStatusCodeSend(false);
		couponData.setAttribName("aaData");
		ArrayList<String[]> couponList = new ArrayList<String[]>();

		if ((start != null &&  start.compareTo("null") != 0 && start.compareTo("") != 0)) {
			ps = DbConnectionManager.getPreparedStatement(db_name, conn,
					"SelectAllCoupons");
			try {
				ps.setString(1, start);
				ps.setString(2, end);
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} else {
			System.out
					.println("Both are null .. fetching without filter ....................");
			ps = DbConnectionManager.getPreparedStatement(db_name, conn,
					"SelectAllCouponsWithoutFilter");
		}
		/*
		 * String discData[] = null; Connection conn = null; PreparedStatement
		 * ps = null; ResultSet rs = null; AllDiscountCouponResponseObject
		 * couponData = new AllDiscountCouponResponseObject();
		 * couponData.setAttribName("aaData");
		 * response.setResponseObject(couponData); ArrayList<String[]>
		 * couponList = new ArrayList<String[]>();
		 */

		int srno = 0;

		try {
			System.out.println(ps);
			rs = ps.executeQuery();
			while (rs.next()) {
				discData = new String[7];
				srno++;
				discData[0] = srno + "";

				discData[1] = rs.getString("couponCode");

				discData[2] = formatTo.format(sourceFormat.parse(rs
						.getString("starttime")));

				discData[3] = formatTo.format(sourceFormat.parse(rs
						.getString("endtime")));

				discData[4] = rs.getInt("maxvaluediscount") + "";

				discData[5] = rs.getString("percentagediscount");

				discData[6] = rs.getInt("valuediscount") + "";
				couponList.add(discData);
			}
			couponData.setAttribList(couponList);
			response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
		} catch (SQLException e) {
			logger.error(
					"Error while fetching discount details for all coupons", e);
			response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED,
					"Error while fetching discount details");
			return RuleDecisionKey.ON_FAILURE;
		} catch (Exception e) {
			logger.error(
					"Error while fetching discount details for all coupons", e);
			response.setParameters(EzedTransactionCodes.INTERNAL_ERROR,
					"Error while fetching discount details");
			return RuleDecisionKey.ON_FAILURE;
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					logger.error(
							"Error while closing result set while fetching Coupon list",
							e);
				}
			}
			DbConnectionManager.releaseConnection(db_name, conn);
		}
		return RuleDecisionKey.ON_SUCCESS;
	}
}
