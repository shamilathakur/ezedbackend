package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class TestResultGetter extends SingletonRule
{

   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;

   public TestResultGetter(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {

      String challengTestSessionId = (String) flowContext.getFromContext(EzedKeyConstants.TEST_SESSION_ID);
      String challengeId = (String) flowContext.getFromContext(EzedKeyConstants.CHALLENGE_ID);
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      String originalTestSessionId = "";
      
      Connection connection = DbConnectionManager.getConnectionByName(dbConnName);
      ResultSet resultSet = null;

      try
      {
         //select * from challengetest where challengeid = ?  
         PreparedStatement preparedStatement = DbConnectionManager.getPreparedStatement(dbConnName, connection, "GetPriviousTestSessionID");
         preparedStatement.setString(1, challengeId);
         resultSet = preparedStatement.executeQuery();
         
         if(resultSet.next())
         {
            originalTestSessionId = resultSet.getString("testsessionid");
         }
         else
         {
            logger.debug("No original test id found for challengeId : " + challengeId);
            response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Please check the challenge id.");
            return RuleDecisionKey.ON_SUCCESS;
         }
      }
      catch (SQLException e)
      {
         logger.error("Database Exception occoured.", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Internal Error. Please try again later.");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Exception occoured.", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal Error. Please try again later.");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         try
         {
            resultSet.close();
         }
         catch (Exception e)
         {
            e.printStackTrace();
         }
         DbConnectionManager.releaseConnection(dbConnName, connection);
      }
      
      
      

      connection = DbConnectionManager.getConnectionByName(dbConnName);
      resultSet = null;

      try
      {
         //select * from testsummery where testsessionid = ? ,   
         PreparedStatement preparedStatement = DbConnectionManager.getPreparedStatement(dbConnName, connection, "GetTestResult");
         preparedStatement.setString(1, challengeId);
         resultSet = preparedStatement.executeQuery();
         
         if(resultSet.next())
         {
            originalTestSessionId = resultSet.getString("testsessionid");
         }
         else
         {
            logger.debug("No original test id found for challengeId : " + challengeId);
            response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Please check the challenge id.");
            return RuleDecisionKey.ON_SUCCESS;
         }
      }
      catch (SQLException e)
      {
         logger.error("Database Exception occoured.", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Internal Error. Please try again later.");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Exception occoured.", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal Error. Please try again later.");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         try
         {
            resultSet.close();
         }
         catch (Exception e)
         {
            e.printStackTrace();
         }
         DbConnectionManager.releaseConnection(dbConnName, connection);
      }

      return null;
   }

}
