package com.intelli.ezed.utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;

public class SymlinkPrinter
{
   private Process process;
   private PrintWriter out;
   private InputStream in;

   public SymlinkPrinter(String path) throws IOException
   {
      process = Runtime.getRuntime().exec(path);
      out = new PrintWriter(process.getOutputStream());
      in = new BufferedInputStream(process.getInputStream());
   }

   public void print(String path)
   {
      out.println(path);
      out.flush();
   }

   public String readResult()
   {
      StringBuffer sb = new StringBuffer();
      byte[] readBytes = new byte[1024];
      boolean endOfStream = true;
      int totalRead = 0;
      int read = 0;
      try
      {
         while (!endOfStream)
         {
            totalRead = 0;
            while (totalRead < readBytes.length)
            {
               read = in.read(readBytes, totalRead, readBytes.length - totalRead);
               if (read == -1)
               {
                  endOfStream = true;
                  break;
               }
               totalRead = totalRead + read;
            }
            sb.append(new String(readBytes, 0, totalRead));
         }
      }
      catch (IOException e)
      {
         e.printStackTrace();
      }
      return sb.toString();
   }

   public void terminate() throws InterruptedException, IOException
   {
      out.close();
      in.close();
      process.waitFor();
      process.destroy();
   }

   public static void main(String[] args)
   {
      SymlinkPrinter linkPrinter = null;
      try
      {
         File file = new File("lib/symlinker.exe");
         System.out.println(file.getAbsolutePath());
         linkPrinter = new SymlinkPrinter("C:\\intelliswiftee\\Ezed\\lib\\symlinker.exe");
         linkPrinter.print(args[0]);
         System.out.println("Java result : " + linkPrinter.readResult());
      }
      catch (IOException e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      finally
      {
         try
         {
            linkPrinter.terminate();
         }
         catch (InterruptedException e)
         {
            // TODO Auto-generated catch block
            e.printStackTrace();
         }
         catch (IOException e)
         {
            // TODO Auto-generated catch block
            e.printStackTrace();
         }
      }
   }
}