package com.intelli.ezed.data;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.intelli.ezed.bulkupload.data.ChapterDetails;
import com.intelli.ezed.utils.EzedHelper;

public class CartData
{
   private String courseId;
   private String courseType;
   private Date startDate;
   private Date endDate;
   private ArrayList<GoalData> goalDatas;
   private SimpleDateFormat sdfIn = new SimpleDateFormat("yyyyMMddHHmmss");
   private SimpleDateFormat sdfOut = new SimpleDateFormat("yyyyMMdd");
   private int statusCode;
   private int amount;

   public static final String ALL_COURSE = "3";
   public static final String CONTENT_ONLY = "1";
   public static final String TEST_ONLY = "2";

   private boolean isFetchContentOnly = false;
   private boolean isFetchTestOnly = false;
   private boolean isFetchAllCourse = false;
   private boolean isDeleteExistingGoals = false;
   private boolean isUpdateUserCourse = false;
   private boolean isUpdateStartDate = false;

   public boolean isUpdateStartDate()
   {
      return isUpdateStartDate;
   }

   public void setUpdateStartDate(boolean isUpdateStartDate)
   {
      this.isUpdateStartDate = isUpdateStartDate;
   }

   public boolean isUpdateUserCourse()
   {
      return isUpdateUserCourse;
   }

   public void setUpdateUserCourse(boolean isUpdateUserCourse)
   {
      this.isUpdateUserCourse = isUpdateUserCourse;
   }

   public boolean isFetchContentOnly()
   {
      return isFetchContentOnly;
   }

   public void setFetchContentOnly(boolean isFetchContentOnly)
   {
      this.isFetchContentOnly = isFetchContentOnly;
   }

   public boolean isFetchTestOnly()
   {
      return isFetchTestOnly;
   }

   public void setFetchTestOnly(boolean isFetchTestOnly)
   {
      this.isFetchTestOnly = isFetchTestOnly;
   }

   public boolean isFetchAllCourse()
   {
      return isFetchAllCourse;
   }

   public void setFetchAllCourse(boolean isFetchAllCourse)
   {
      this.isFetchAllCourse = isFetchAllCourse;
   }

   public boolean isDeleteExistingGoals()
   {
      return isDeleteExistingGoals;
   }

   public void setDeleteExistingGoals(boolean isDeleteExistingGoals)
   {
      this.isDeleteExistingGoals = isDeleteExistingGoals;
   }

   public int getAmount()
   {
      return amount;
   }

   public void setAmount(int amount)
   {
      this.amount = amount;
   }

   public int getStatusCode()
   {
      return statusCode;
   }

   public void setStatusCode(int statusCode)
   {
      this.statusCode = statusCode;
   }

   public CartData(String courseId, String courseType, Date date)
   {
      this.courseId = courseId;
      this.courseType = courseType;
      try
      {
         this.startDate = sdfOut.parse(sdfOut.format(date));
      }
      catch (ParseException e)
      {
         this.startDate = date;
      }
   }

   public CartData(String courseId, String courseType, Date date, Date endDate)
   {
      this.courseId = courseId;
      this.courseType = courseType;
      try
      {
         this.startDate = sdfOut.parse(sdfOut.format(date));
         this.endDate = sdfOut.parse(sdfOut.format(endDate));
      }
      catch (ParseException e)
      {
         this.startDate = date;
         this.endDate = endDate;
      }
   }

   public CartData(String courseId, int courseType, Date date, Date endDate)
   {
      this(courseId, Integer.toString(courseType), date, endDate);
   }

   public CartData(String courseId, int courseType, Date date)
   {
      this(courseId, Integer.toString(courseType), date);
   }

   public Date getEndDate()
   {
      return endDate;
   }

   public void setEndDate(Date endDate)
   {
      this.endDate = endDate;
   }

   public ArrayList<GoalData> getGoalDatas()
   {
      return goalDatas;
   }

   public void setGoalDatas(ArrayList<GoalData> goalDatas)
   {
      this.goalDatas = goalDatas;
   }

   public Date getStartDate()
   {
      return startDate;
   }

   public void setStartDate(Date startDate)
   {
      this.startDate = startDate;
   }

   public String getCourseId()
   {
      return courseId;
   }

   public void setCourseId(String courseId)
   {
      this.courseId = courseId;
   }

   public String getCourseType()
   {
      return courseType;
   }

   public void setCourseType(String courseType)
   {
      this.courseType = courseType;
   }

   public void calculateTimeLine(float avgNoOfMinutes)
   {
      float fractionOfMinutesPerDay = EzedHelper.calculateEndDateViaDefHours(avgNoOfMinutes, getGoalDatas(), startDate, endDate);
      if (fractionOfMinutesPerDay == 0)
      {
         //start date before enddate
         for (int j = 0; j < goalDatas.size(); j++)
         {
            goalDatas.get(j).setEndDate(startDate);
         }
         return;
      }
      int noOfMinutesPerDay = (int) fractionOfMinutesPerDay;
      if (noOfMinutesPerDay < fractionOfMinutesPerDay)
      {
         noOfMinutesPerDay++;
      }

      Calendar calendar = Calendar.getInstance();
      calendar.setTime(getStartDate());
      calendar.roll(Calendar.SECOND, false);
      calendar.roll(Calendar.MINUTE, false);
      calendar.roll(Calendar.HOUR_OF_DAY, false);
      int j = 0;
      int initMinutes = getGoalDatas().get(j).getHours();
      int minutesOfDay = 0;
      int days = 0;
      int k = 0;
      int totalHours = EzedHelper.getTotalMinutes(getGoalDatas());
      for (int i = 0; i < totalHours; i++)
      {
         initMinutes--;
         minutesOfDay++;
         if (initMinutes == 0)
         {
            calendar.add(Calendar.DATE, days);
            k = j;
            if (getGoalDatas().get(j).getContentShowFlag().compareTo(ChapterDetails.HIDE_CONTENT) == 0)
            {
               k = getLastNonHiddenIndex(j);
            }
            getGoalDatas().get(k).setEndDate(calendar.getTime());
            j++;
            if (j == getGoalDatas().size())
            {
               break;
            }
            initMinutes = getGoalDatas().get(j).getHours();
            calendar.setTime(getStartDate());
            calendar.roll(Calendar.SECOND, false);
            calendar.roll(Calendar.MINUTE, false);
            calendar.roll(Calendar.HOUR_OF_DAY, false);
         }
         if (minutesOfDay == noOfMinutesPerDay)
         {
            minutesOfDay = 0;
            days++;
         }
      }
   }

   private int getLastNonHiddenIndex(int index)
   {
      for (int i = index; i >= 0; i--)
      {
         if (getGoalDatas().get(i).getContentShowFlag().compareTo(ChapterDetails.SHOW_CONTENT) == 0)
         {
            return i;
         }
      }
      return 0;
   }
   
   public static void main(String[] args) throws ParseException
   {
      SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
      Date date = sdf.parse("20130923000000");
      Calendar calendar = Calendar.getInstance();
      calendar.setTime(date);
      System.out.println(calendar.getTime());
   }
}
