package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.AddScratchCardToUserData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.ScratchCardErrorLogData;
import com.intelli.ezed.utils.EndDateCalculator;
import com.intelli.ezed.utils.ScratchCardHelper;

public class CourseDetailsForScratchCardFetcher extends SingletonRule {
	private String dbConnName;
	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
	private final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
	private final SimpleDateFormat sdf1 = new SimpleDateFormat("MM/dd/yyyy");

	public CourseDetailsForScratchCardFetcher(String rulename, String dbConnName) {
		super(rulename);
		this.dbConnName = dbConnName;
	}

	public RuleDecisionKey executeRule(FlowContext flowContext) {
		int dayBufferBeforeEnd = (Integer) flowContext
				.getFromContext(EzedKeyConstants.BUFFER_END_DATE);
		Response response = (Response) flowContext
				.getFromContext(EzedKeyConstants.EZED_RESPONSE);
		AddScratchCardToUserData data = (AddScratchCardToUserData) flowContext
				.getFromContext(EzedKeyConstants.ADD_COURSE_USING_SCRATCHCARD_DETAILS);
		ScratchCardErrorLogData errordata = (ScratchCardErrorLogData) flowContext
				.getFromContext(EzedKeyConstants.SCRATCH_CARD_ERROR_LOG_DB);
		System.out.println("In Class : " + this.getClass().getName());
		System.out.print("USERID : " + data.getUserid() + "  Sessionid : "
				+ data.getSessionid());

		Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
		PreparedStatement ps = null;
		ResultSet rs = null;
		Calendar calendar = Calendar.getInstance();
		Date startDate = new Date();
		data.setCoursestartdate(sdf.format(startDate));

		try {
			ps = DbConnectionManager.getPreparedStatement(dbConnName, conn,
					"FetchCourseDetailsByCourseId");
			ps.setString(1, data.getCourseid());
			rs = ps.executeQuery();
			if (rs.next()) {
				String commaSeparatedEndMonths = rs.getString("enddate");
				Date courseEndDate = EndDateCalculator.findEndDate(
						commaSeparatedEndMonths, startDate);
				calendar.setTime(courseEndDate);
				calendar.add(Calendar.DATE, -dayBufferBeforeEnd);
				Date endDate = calendar.getTime();
				data.setCourseenddate(sdf.format(endDate));
			}
		} catch (SQLException e) {
			System.out.println("SQL Error In Class : "
					+ this.getClass().getName());
			if (logger.isDebugEnabled()) {
				logger.error(
						"SQL Error occured while fetching course details for scratch card : "
								+ data.getScratchcardid(), e);
			}
			errordata.setClassname(this.getClass().getName());
			ScratchCardHelper.callDbError(errordata,dbConnName,logger);
			response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED,
					"SQL error while fetching course details.");
			return RuleDecisionKey.ON_FAILURE;
		} catch (Exception e) {
			if (logger.isDebugEnabled()) {
				logger.error(
						"Internal Error occured while fetching course details for scratch card : "
								+ data.getScratchcardid(), e);
			}
			errordata.setClassname(this.getClass().getName());
			ScratchCardHelper.callInternalError(errordata,dbConnName,logger);
			response.setParameters(EzedTransactionCodes.INTERNAL_ERROR,
					"Internal error while fetching course details.");
			return RuleDecisionKey.ON_FAILURE;
		} finally {
			DbConnectionManager.releaseConnection(dbConnName, conn);
		}
		flowContext.putIntoContext(
				EzedKeyConstants.ADD_COURSE_USING_SCRATCHCARD_DETAILS, data);
		return RuleDecisionKey.ON_SUCCESS;
	}

}
