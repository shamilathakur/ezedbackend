package com.intelli.ezed.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

import org.sabre.db.DbConnectionManager;
import org.slf4j.Logger;

import com.intelli.ezed.utils.AccountStatusHelper;

public class UserProfile extends ResponseObject
{
   private static final String POPULATE_PS_NAME = "GetUserDetails";
   private static final String COMMIT_PS_NAME = "SetUserDetails";
   private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
   private static final SimpleDateFormat sdfCompare = new SimpleDateFormat("yyyyMMdd");
   private static final SimpleDateFormat sdfFrontendEndDates = new SimpleDateFormat("MM/dd/yyyy");
   public static int endDateBuffer = 0;

   private String userid;
   private String mobileNumber;
   private String firstName;
   private String middleName;
   private String lastName;
   private String gender;
   private String dateOfBirth;
   private String streetAdress;
   private String landmark;
   private String pincode;
   private String univName;
   private String collegeName;
   private String city;
   private String state;
   private String country;
   private String profileImage;
   private String invitedBy;
   private String stream;
   private String degree;
   private String goalsAlertStatus;

   private String password;
   private int chkflag;
   private int userLevel;

   private String loginWith;
   private String regWith;
   private int userPoints;
   private String fbId;

   /* 
    * This integer type is used determine the login or registration type of a user.
    * it can take values as follows
    *  0 - registration vi Ezed i.e by entering the userid and password at ezed site.  
    *  1 - registration vi facebook or gmail or any other site.
    */
   private int regLoginType;

   /*
    * List containing courseIds of all the purchased courses.
    * List will be fetched from 'usercourse' table. 
    * It could be objects of CourseDetails or Front end can fetch them directly from
    * database. 
    */


   private Vector<String> courseList = new Vector<String>();
   private Vector<String> courseTypeList = new Vector<String>();
   private Vector<Integer> coursePointsList = new Vector<Integer>();
   private Vector<Date> courseStartDates = new Vector<Date>();
   private Vector<Date> courseEndDates = new Vector<Date>();
   private Vector<Integer> validFlags = new Vector<Integer>();


   public Vector<Integer> getValidFlags()
   {
      return validFlags;
   }

   public void setValidFlags(Vector<Integer> validFlags)
   {
      this.validFlags = validFlags;
   }

   public Vector<String> getCourseTypeList()
   {
      return courseTypeList;
   }

   public void setCourseTypeList(Vector<String> courseTypeList)
   {
      this.courseTypeList = courseTypeList;
   }

   public Vector<Integer> getCoursePointsList()
   {
      return coursePointsList;
   }

   public void setCoursePointsList(Vector<Integer> coursePointsList)
   {
      this.coursePointsList = coursePointsList;
   }

   public Vector<Date> getCourseStartDates()
   {
      return courseStartDates;
   }

   public void setCourseStartDates(Vector<Date> courseStartDates)
   {
      this.courseStartDates = courseStartDates;
   }

   public Vector<Date> getCourseEndDates()
   {
      return courseEndDates;
   }

   public void setCourseEndDates(Vector<Date> courseEndDates)
   {
      this.courseEndDates = courseEndDates;
   }

   public int getUserPoints()
   {
      return userPoints;
   }

   public void setUserPoints(int userPoints)
   {
      this.userPoints = userPoints;
   }

   public String getProfileImage()
   {
      return profileImage;
   }

   public void setProfileImage(String profileImage)
   {
      this.profileImage = profileImage;
   }

   public String getCity()
   {
      return city;
   }

   public void setCity(String city)
   {
      this.city = city;
   }

   public String getState()
   {
      return state;
   }

   public void setState(String state)
   {
      this.state = state;
   }

   public String getCountry()
   {
      return country;
   }

   public void setCountry(String country)
   {
      this.country = country;
   }

   public String getMiddleName()
   {
      return middleName;
   }

   public void setMiddleName(String middleName)
   {
      this.middleName = middleName;
   }

   public String getLoginWith()
   {
      return loginWith;
   }

   public int getRegLoginType()
   {
      return regLoginType;
   }

   public void setRegLoginType(int regLoginType)
   {
      this.regLoginType = regLoginType;
   }

   public void setLoginWith(String loginWith)
   {
      this.loginWith = loginWith;
   }

   public int getChkflag()
   {
      return chkflag;
   }

   public void setChkflag(int chkflag)
   {
      this.chkflag = chkflag;
   }

   public String getPassword()
   {
      return password;
   }

   public void setPassword(String password)
   {
      this.password = password;
   }

   public Vector<String> getCourseList()
   {
      return courseList;
   }

   public void setCourseList(Vector<String> courseList)
   {
      this.courseList = courseList;
   }

   public String getUserid()
   {
      return userid;
   }
   public String getMobileNumber()
   {
      return mobileNumber;
   }
   public String getFirstName()
   {
      return firstName;
   }
   public String getLastName()
   {
      return lastName;
   }
   public int getUserLevel()
   {
      return userLevel;
   }
   public void setUserLevel(int userLevel)
   {
      this.userLevel = userLevel;
   }
   public String getGender()
   {
      return gender;
   }
   public String getDateOfBirth()
   {
      return dateOfBirth;
   }
   public String getStreetAdress()
   {
      return streetAdress;
   }
   public String getLandmark()
   {
      return landmark;
   }
   public String getPincode()
   {
      return pincode;
   }

   public String getUnivName()
   {
      return univName;
   }
   public String getCollegeName()
   {
      return collegeName;
   }

   public void setUserid(String userid)
   {
      this.userid = userid.toLowerCase();
   }

   public void setMobileNumber(String mobileNumber)
   {
      this.mobileNumber = mobileNumber;
   }

   public void setFirstName(String firstName)
   {
      this.firstName = firstName;
   }

   public void setLastName(String lastName)
   {
      this.lastName = lastName;
   }

   public void setGender(String gender)
   {
      this.gender = gender;
   }

   public void setDateOfBirth(String dateOfBirth)
   {
      this.dateOfBirth = dateOfBirth;
   }

   public void setStreetAdress(String streetAdress)
   {
      this.streetAdress = streetAdress;
   }

   public void setLandmark(String landmark)
   {
      this.landmark = landmark;
   }


   public String getRegWith()
   {
      return regWith;
   }

   public void setRegWith(String regWith)
   {
      this.regWith = regWith;
   }

   public void setPincode(String pincode)
   {
      this.pincode = pincode;
   }

   public void setUnivName(String univName)
   {
      this.univName = univName;
   }

   public void setCollegeName(String collegeName)
   {
      this.collegeName = collegeName;
   }

   @Override
   public void prepareResponse() throws Exception
   {
      //      StringBuffer sb = new StringBuffer();
      //      
      //      sb.append("|userid").append(EzedKeyConstants.RESPONSE_SEPARATOR).append(getUserid());
      //      sb.append("|firstname").append(EzedKeyConstants.RESPONSE_SEPARATOR).append(getFirstName());
      //      sb.append("|lastname").append(EzedKeyConstants.RESPONSE_SEPARATOR).append(getLastName());
      //      
      //      return sb.toString();
      this.put("userid", getUserid());
      this.put("firstname", getFirstName());
      this.put("lastname", getLastName());
      this.put("middlename", getMiddleName());
      this.put("dob", getDateOfBirth());
      this.put("mobilenumber", getMobileNumber());
      this.put("gender", getGender());
      this.put("streetaddress", getStreetAdress());
      this.put("landmark", getLandmark());
      this.put("pincode", getPincode());
      this.put("university", getUnivName());
      this.put("college", getCollegeName());
      this.put("profileimage", getProfileImage());
      this.put("city", getCity());
      this.put("state", getState());
      this.put("country", getCountry());
      this.put("stream", getStream());
      this.put("degree", getDegree());
      this.put("fbid", getFbId());

      if (courseList.size() != 0)
      {
         StringBuffer sb = new StringBuffer();
         for (String str : courseList)
         {
            sb.append("'").append(str).append("'").append(",");
         }
         sb.deleteCharAt(sb.length() - 1);
         this.put("courselist", sb.toString());
      }
      else
      {
         this.put("courselist", "");
      }

      if (courseTypeList.size() != 0)
      {
         StringBuffer sb = new StringBuffer();
         for (String str : courseTypeList)
         {
            sb.append(str).append(",");
         }
         sb.deleteCharAt(sb.length() - 1);
         this.put("coursetypelist", sb.toString());
      }
      else
      {
         this.put("coursetypelist", "");
      }

      int totalPoints = 0;
      totalPoints += getUserPoints();
      //      if (coursePointsList.size() != 0)
      //      {
      //         for (Integer individualCoursePoints : coursePointsList)
      //         {
      //            totalPoints += individualCoursePoints;
      //         }
      //      }

      this.put("points", totalPoints);

      if (AccountStatusHelper.isValidate(getChkflag()))
      {
         this.put("showprofilepage", 0);
      }
      else
      {
         this.put("showprofilepage", 1);
      }

      if (validFlags.size() != 0)
      {
         StringBuffer sb = new StringBuffer();
         for (Integer singleValidFlag : validFlags)
         {
            sb.append(singleValidFlag).append(",");
         }
         sb.deleteCharAt(sb.length() - 1);
         this.put("coursevalidity", sb.toString());
      }
      else
      {
         this.put("coursevalidity", "");
      }

      if (courseEndDates.size() != 0)
      {
         StringBuffer sb = new StringBuffer();
         Calendar calendar = Calendar.getInstance();
         for (Date singleEndDate : courseEndDates)
         {
            calendar.setTime(singleEndDate);
            calendar.roll(Calendar.DATE, endDateBuffer);
            sb.append(sdfFrontendEndDates.format(calendar.getTime())).append(",");
         }
         sb.deleteCharAt(sb.length() - 1);
         this.put("enddates", sb.toString());
      }
      else
      {
         this.put("enddates", "");
      }

      if (courseStartDates.size() != 0)
      {
         StringBuffer sb = new StringBuffer();
         for (Date singleStartDate : courseStartDates)
         {
            sb.append(sdfFrontendEndDates.format(singleStartDate)).append(",");
         }
         sb.deleteCharAt(sb.length() - 1);
         this.put("startdates", sb.toString());
      }
   }

   public int populateUserProfile(String dbConnName, Logger logger) throws Exception
   {
      if (getUserid() == null)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("userid is null. Details could not be populated");
         }
         return EzedTransactionCodes.INTERNAL_ERROR;
      }
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, POPULATE_PS_NAME);
         ps.setString(1, getUserid());
         rs = ps.executeQuery();
         if (rs != null && rs.next())
         {
            String value = rs.getString("mobilenumber");
            if (value != null && value.trim().compareTo("") != 0)
            {
               //               setMobileNumber(EzedHelper.getTenDigMobNo(value));
               setMobileNumber(value);
            }

            setPassword(rs.getString("password"));
            setChkflag(rs.getInt("chkflag"));
            setFirstName(rs.getString("firstname"));
            setLastName(rs.getString("lastname"));

            value = rs.getString("middlename");
            if (value == null || value.trim().compareTo("") == 0)
            {
               value = null;
            }
            setMiddleName(value);

            value = rs.getString("dob");
            if (value == null || value.trim().compareTo("") == 0)
            {
               value = null;
            }
            this.setDateOfBirth(value);

            value = rs.getString("gender");
            if (value == null || value.trim().compareTo("") == 0)
            {
               value = null;
            }
            this.setGender(value);

            value = rs.getString("streetaddress");
            if (value == null || value.trim().compareTo("") == 0)
            {
               value = null;
            }
            this.setStreetAdress(value);

            value = rs.getString("landmark");
            if (value == null || value.trim().compareTo("") == 0)
            {
               value = null;
            }
            this.setLandmark(value);

            value = rs.getString("cityname");
            if (value == null || value.trim().compareTo("") == 0)
            {
               value = null;
            }
            this.setCity(value);

            value = rs.getString("statename");
            if (value == null || value.trim().compareTo("") == 0)
            {
               value = null;
            }
            this.setState(value);

            value = rs.getString("countryname");
            if (value == null || value.trim().compareTo("") == 0)
            {
               value = null;
            }
            this.setCountry(value);

            value = rs.getString("pincode");
            if (value == null || value.trim().compareTo("") == 0)
            {
               value = null;
            }
            this.setPincode(value);

            value = rs.getString("univname");
            if (value == null || value.trim().compareTo("") == 0)
            {
               value = null;
            }
            this.setUnivName(value);

            value = rs.getString("collegename");
            if (value == null || value.trim().compareTo("") == 0)
            {
               value = null;
            }
            this.setCollegeName(value);

            value = rs.getString("profileimage");
            if (value == null || value.trim().compareTo("") == 0)
            {
               value = null;
            }
            this.setProfileImage(value);
            this.setUserPoints(rs.getInt("points"));
            this.setDegree(rs.getString("degree"));
            this.setStream(rs.getString("stream"));
            this.setFbId(rs.getString("fbid"));

            //return EzedTransactionCOdes.SUCCESS;
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("userid details not found in DB");
            }
            return EzedTransactionCodes.INTERNAL_ERROR;
         }
      }
      catch (SQLException e)
      {
         throw e;
      }
      catch (Exception e)
      {
         throw e;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
               logger.error("Error while closing resultset while populating user profile", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      conn = DbConnectionManager.getConnectionByName(dbConnName);

      try
      {
         //select courseid from usercours where userid = ?
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchCourseList");
         ps.setString(1, userid);
         rs = ps.executeQuery();
         Date today = new Date();
         Date startDate = null;
         Date endDate = null;
         while (rs.next())
         {
            courseList.add(rs.getString("courseid"));
            courseTypeList.add(rs.getString("coursetype"));
            coursePointsList.add(rs.getInt("points"));
            startDate = sdf.parse(rs.getString("startdate"));
            endDate = sdf.parse(rs.getString("enddate"));
            courseStartDates.add(startDate);
            courseEndDates.add(endDate);
            if (today.before(startDate) || today.after(endDate))
            {
               validFlags.add(0);
            }
            else
            {
               validFlags.add(1);
            }
         }
      }
      catch (Exception e)
      {
         throw e;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
               logger.error("Error while closing resultset while populating user profile", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }


      return EzedTransactionCodes.SUCCESS;
   }

   public int commitUserProfile(String dbConnName, Logger logger) throws Exception
   {
      if (getUserid() == null)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("userid is null. Details could not be committed");
         }
         return EzedTransactionCodes.INTERNAL_ERROR;
      }
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, COMMIT_PS_NAME);
         //ps.setString(1, MobileNumberFormatter.getFormattedMobileNumber(getMobileNumber()));
         ps.setInt(1, getChkflag());
         ps.setString(2, getPassword());
         ps.setString(3, getUserid());

         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows updated while committing details in DB for userid " + getUserid());
         }
         return EzedTransactionCodes.SUCCESS;
      }
      catch (SQLException e)
      {
         throw e;
      }
      catch (Exception e)
      {
         throw e;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   public boolean isFreshPurchase(String courseId, String courseType, Date endDate)
   {
      if (courseType.compareTo(CartData.ALL_COURSE) == 0)
      {
         return true;
      }
      for (int i = 0; i < getCourseList().size(); i++)
      {
         if (courseId.compareTo(getCourseList().get(i)) == 0)
         {
            if (sdfCompare.format(getCourseEndDates().get(i)).compareTo(sdfCompare.format(endDate)) == 0)
            {
               return false;
            }
         }
      }
      return true;
   }

   public boolean isCourseAlreadyRunning(String courseId, Date endDate)
   {
      for (int i = 0; i < getCourseList().size(); i++)
      {
         if (courseId.compareTo(getCourseList().get(i)) == 0 && sdfCompare.format(getCourseEndDates().get(i)).compareTo(sdfCompare.format(endDate)) == 0)
         {
            Date now = new Date();
            if (now.after(getCourseStartDates().get(i)))
            {
               return true;
            }
            return false;
         }
      }
      return false;
   }

   public Date getNewStartDate(String courseId, Date endDate, Date startDate)
   {
      for (int i = 0; i < getCourseList().size(); i++)
      {
         if (courseId.compareTo(getCourseList().get(i)) == 0 && sdfCompare.format(getCourseEndDates().get(i)).compareTo(sdfCompare.format(endDate)) == 0)
         {
            Date alreadyPurchasedStartdaDate = getCourseStartDates().get(i);
            if (alreadyPurchasedStartdaDate.after(startDate))
            {
               return startDate;
            }
            return alreadyPurchasedStartdaDate;
         }
      }
      return startDate;
   }

   public Date getMaxGoalDate(String dbConnName, Logger logger, String courseId, Date endDate) throws SQLException, Exception
   {
      Date startDateCourse = null;
      SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
      for (int i = 0; i < getCourseList().size(); i++)
      {
         if (courseId.compareTo(getCourseList().get(i)) == 0 && sdfCompare.format(getCourseEndDates().get(i)).compareTo(sdfCompare.format(endDate)) == 0)
         {
            startDateCourse = getCourseStartDates().get(i);
            break;
         }
      }
      if (startDateCourse == null)
      {
         throw new Exception("No start date could be found for course " + courseId);
      }
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      Date maxDate = startDateCourse;
      try
      {
         //select endtime from userpoints where userid=? and courseid=? and to_date(?,'YYYYMMDDHH24MISS') < to_date(endtime,'YYYYMMDDHH24MISS') and to_date(?,'YYYYMMDDHH24MISS') > to_date(endtime,'YYYYMMDDHH24MISS') 
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "SelectGoalsWithinPeriod");
         ps.setString(1, getUserid());
         ps.setString(2, courseId);
         ps.setString(3, sdf.format(startDateCourse));
         ps.setString(4, sdf.format(endDate));
         rs = ps.executeQuery();
         String endTimeString = null;
         Date endTimeDate = null;
         while (rs.next())
         {
            endTimeString = rs.getString("endtime");
            endTimeDate = sdf.parse(endTimeString);
            if (maxDate.before(endTimeDate))
            {
               maxDate = endTimeDate;
            }
         }
         Date today = sdfCompare.parse(sdfCompare.format(new Date()));
         if (maxDate.before(today))
         {
            return today;
         }
         Calendar cal = Calendar.getInstance();
         cal.setTime(maxDate);
         cal.roll(Calendar.DATE, 1);
         return cal.getTime();
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
               logger.error("Error while fetching max goal date for course " + courseId, e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   public void deleteExistingGoals(String dbConnName, Logger logger, String courseId, Date endDate) throws SQLException, Exception
   {
      Date startDateCourse = null;
      SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
      for (int i = 0; i < getCourseList().size(); i++)
      {
         if (courseId.compareTo(getCourseList().get(i)) == 0 && sdfCompare.format(getCourseEndDates().get(i)).compareTo(sdfCompare.format(endDate)) == 0)
         {
            startDateCourse = getCourseStartDates().get(i);
            break;
         }
      }
      if (startDateCourse == null)
      {
         throw new Exception("No start date could be found for course " + courseId);
      }
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         //delete from userpoints where userid=? and courseid=? and to_date(?,'YYYYMMDDHH24MISS') < to_date(endtime,'YYYYMMDDHH24MISS') and to_date(?,'YYYYMMDDHH24MISS') > to_date(endtime,'YYYYMMDDHH24MISS') 
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "DeleteGoalsWithinPeriod");
         ps.setString(1, getUserid());
         ps.setString(2, courseId);
         ps.setString(3, sdf.format(startDateCourse));
         ps.setString(4, sdf.format(endDate));
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows delete from db while deleting already existing goals for course id " + courseId);
         }
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   public boolean isNewCourseValid(String courseId, Date endDate, String courseType)
   {
      for (int i = 0; i < getCourseList().size(); i++)
      {
         if (courseId.compareTo(getCourseList().get(i)) == 0 && sdfCompare.format(getCourseEndDates().get(i)).compareTo(sdfCompare.format(endDate)) == 0)
         {
            if (getCourseTypeList().get(i).compareTo(CartData.ALL_COURSE) == 0 || courseType.compareTo(CartData.ALL_COURSE) == 0)
            {
               return false;
            }
            if (getCourseTypeList().get(i).compareTo(courseType) == 0)
            {
               return false;
            }
         }
      }
      return true;
   }

   public static void main(String[] args)
   {
      Date date = new Date();
      Date date1 = new Date();
      System.out.println(date.compareTo(date1));
   }

   public String getInvitedBy()
   {
      return invitedBy;
   }

   public void setInvitedBy(String invitedBy)
   {
      this.invitedBy = invitedBy;
   }

   public String getStream()
   {
      return stream;
   }

   public void setStream(String stream)
   {
      this.stream = stream;
   }

   public String getDegree()
   {
      return degree;
   }

   public void setDegree(String degree)
   {
      this.degree = degree;
   }

   public String getFbId()
   {
      return fbId;
   }

   public void setFbId(String fbId)
   {
      this.fbId = fbId;
   }

   public String getName()
   {
      String name = getFirstName();
      if (name == null)
      {
         name = getLastName();
         if (name == null)
         {
            name = getUserid();
         }
      }
      else
      {
         if (getLastName() != null)
         {
            name = name + " " + getLastName();
         }
      }
      return name;
   }

   public String getGoalsAlertStatus()
   {
      return goalsAlertStatus;
   }

   public void setGoalsAlertStatus(String goalsAlertStatus)
   {
      this.goalsAlertStatus = goalsAlertStatus;
   }
}