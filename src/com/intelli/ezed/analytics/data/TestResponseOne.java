package com.intelli.ezed.analytics.data;

import com.intelli.ezed.data.ResponseObject;

public class TestResponseOne extends ResponseObject
{
   private String name;
   private float[] values;
   
   public String getName()
   {
      return name;
   }
   public void setName(String name)
   {
      this.name = name;
   }
   public float[] getValues()
   {
      return values;
   }
   public void setValues(float[] values)
   {
      this.values = values;
   }
   @Override
   public void prepareResponse() throws Exception
   {
      this.put("name", getName());
      this.put("data", getValues());
   }
}
