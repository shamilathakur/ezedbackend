package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.CollegeData;

public class AddOtherCollegePersister extends SingletonRule{

	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
	private String dbConnName;

	public AddOtherCollegePersister(String ruleName, String dbconnName) {
		super(ruleName);
		this.dbConnName = dbconnName;
	}

	@Override
	public RuleDecisionKey executeRule(FlowContext flowContext) {
		CollegeData collegeData = (CollegeData) flowContext
				.getFromContext(EzedKeyConstants.COLLEGE_DATA);

		Connection conn = DbConnectionManager.getConnectionByName(dbConnName);

		Response response = (Response) flowContext
				.getFromContext(EzedKeyConstants.EZED_RESPONSE);
		PreparedStatement ps = null;

		try {
			ps = DbConnectionManager.getPreparedStatement(dbConnName, conn,
					"InsertOtherCollege");
			
			ps.setString(1, collegeData.getCollege());
			ps.setString(2,collegeData.getUnivName());
			ps.setString(3,collegeData.getPhoneNo());
			ps.setString(4,collegeData.getWebsite());
			ps.setInt(5,collegeData.getShowFlag());
			ps.setString(6,collegeData.getCreatedBy());
			ps.setInt(7,collegeData.getUserFlag());
			ps.setString(8,collegeData.getCreatedOn());
			int rows = ps.executeUpdate();
			if (logger.isDebugEnabled()) {
				logger.debug(rows + " rows inserted in database when " + collegeData.getCollege() +"was added.");
			}
			response.setParameters(EzedTransactionCodes.SUCCESS, "Success");
			return RuleDecisionKey.ON_SUCCESS;
		} catch (SQLException e) {
			logger.error("DB error while inserting other college name by user in db", e);
			response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED,
					"Error while processing add other college request by "+ collegeData.getCreatedBy());
			return RuleDecisionKey.ON_FAILURE;
		} catch (Exception e) {
			logger.error("error while inserting other college name by user in db", e);
			response.setParameters(EzedTransactionCodes.INTERNAL_ERROR,
					"Error while processing add other college request");
			return RuleDecisionKey.ON_FAILURE;
		} finally {
			DbConnectionManager.releaseConnection(dbConnName, conn);
		}
	
}
}
