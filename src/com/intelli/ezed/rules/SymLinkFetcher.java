package com.intelli.ezed.rules;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.bulkupload.data.ContentDetails;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.SymLinkObject;
import com.intelli.ezed.utils.EzedHelper;
import com.intelli.ezed.utils.SymLinkManager;

public class SymLinkFetcher extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String newLinkLocationNotes;
   private String newLinkLocationVideos;
   private String notesStartPoint;
   private String vidStartPoint;
   private boolean betaOverride = false;

   public SymLinkFetcher(String ruleName, String newLinkLoc, String newLinkVideos, String notesStartPoint,
                         String vidStartPoint)
   {
      super(ruleName);
      this.newLinkLocationNotes = newLinkLoc;
      this.newLinkLocationVideos = newLinkVideos;
      this.notesStartPoint = notesStartPoint;
      this.vidStartPoint = vidStartPoint;
   }

   public SymLinkFetcher(String ruleName, String newLinkLoc, String newLinkVideos, String notesStartPoint,
                         String vidStartPoint, String betaOverride)
   {
      super(ruleName);
      this.newLinkLocationNotes = newLinkLoc;
      this.newLinkLocationVideos = newLinkVideos;
      this.notesStartPoint = notesStartPoint;
      this.vidStartPoint = vidStartPoint;
      this.betaOverride = Boolean.parseBoolean(betaOverride);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      String contentId = messageData.getParameter("contentid");
      if (contentId == null || contentId.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Contentid is invalid");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Content id missing or invalid");
         return RuleDecisionKey.ON_FAILURE;
      }

      String contentlink = messageData.getParameter("contentlink");
      if (contentlink == null || contentlink.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("content link is invalid");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Content link is missing or invalid");
         return RuleDecisionKey.ON_FAILURE;
      }

      String contentType = messageData.getParameter("contenttype");
      if (contentType == null || contentType.trim().compareTo("") == 0 || !EzedHelper.isNumeric(contentType.trim()))
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("content type is invalid " + contentType);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Content type is improper");
         return RuleDecisionKey.ON_FAILURE;
      }

      if (betaOverride)
      {
         SymLinkObject linkObj = new SymLinkObject();
         linkObj.setLink(contentlink);
         response.setResponseObject(linkObj);
         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
         return RuleDecisionKey.ON_SUCCESS;
      }
      int actualContentType = Integer.parseInt(contentType.trim());
      if (actualContentType != ContentDetails.CONTENT_NOTE && actualContentType != ContentDetails.CONTENT_PDF && actualContentType != ContentDetails.CONTENT_IMAGE && actualContentType != ContentDetails.CONTENT_AUDIO && actualContentType != ContentDetails.CONTENT_VIDEO)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("No link has to be sent for this contenttype");
         }
         SymLinkObject linkObj = new SymLinkObject();
         linkObj.setLink(contentlink);
         response.setResponseObject(linkObj);
         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
         return RuleDecisionKey.ON_SUCCESS;
      }
      String linkLocation = null;
      boolean isContent = false;
      if (actualContentType == ContentDetails.CONTENT_NOTE || actualContentType == ContentDetails.CONTENT_PDF || actualContentType == ContentDetails.CONTENT_IMAGE || actualContentType == ContentDetails.CONTENT_IMAGE_1)
         
      {
         linkLocation = newLinkLocationNotes;
         isContent = true;
      }
      else
      {
         linkLocation = newLinkLocationVideos;
      }

      SymLinkManager symLinkManager = (SymLinkManager) flowContext.getFromContext(EzedKeyConstants.SYMLINK_MANAGER);
      //      String sessionId = (String) flowContext.getFromContext(EzedKeyConstants.SESSION_ID);
      String userId = (String) flowContext.getFromContext(EzedKeyConstants.USER_ID);

      try
      {
         //session id is userid
         String newLink = symLinkManager.getSymLink(userId, contentId, contentlink, logger, linkLocation);
         if (isContent)
         {
            int index = newLink.indexOf(notesStartPoint);
            newLink = newLink.substring(index + 1);
         }
         else
         {
            int index = newLink.indexOf(vidStartPoint);
            newLink = newLink.substring(index + 1);
            String fileType = newLink.substring(newLink.lastIndexOf('.') + 1);
            if (fileType.compareToIgnoreCase("mov") == 0)
            {
               fileType = "mp4";
            }
            newLink = fileType + ":" + newLink;
         }
         SymLinkObject linkObj = new SymLinkObject();
         linkObj.setLink(newLink);
         response.setResponseObject(linkObj);
         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
      }
      catch (NumberFormatException e)
      {
         logger.error("Error while creating sym link", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal error. Please try again later");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (IllegalBlockSizeException e)
      {
         logger.error("Error while creating sym link", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal error. Please try again later");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (BadPaddingException e)
      {
         logger.error("Error while creating sym link", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal error. Please try again later");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while creating sym link", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal error. Please try again later");
         return RuleDecisionKey.ON_FAILURE;
      }
      return RuleDecisionKey.ON_SUCCESS;
   }

   public static void main(String[] args)
   {
      //      String abc = "/opt/apache-tomcat-7.0.67/webapps/EzEd/content/abcd/content.htm";
      //      int index = abc.indexOf("/content");
      //      System.out.println(abc.substring(index + 1));

      String newLink = "/content/wowzaContent/vid/abcd/abcd.mov";
      int index = newLink.indexOf("/vid");
      newLink = newLink.substring(index + 1);
      String fileType = newLink.substring(newLink.lastIndexOf('.') + 1);
      if (fileType.compareToIgnoreCase("mov") == 0)
      {
         fileType = "mp4";
      }
      newLink = fileType + ":" + newLink;
      System.out.println(newLink);
   }

}
