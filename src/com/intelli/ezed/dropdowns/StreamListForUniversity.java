package com.intelli.ezed.dropdowns;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import bsh.org.objectweb.asm.Constants;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.dropdowns.data.DropDownListData;
import com.intelli.ezed.dropdowns.data.SingleDropDownData;
import com.intelli.ezed.utils.Globals;

public class StreamListForUniversity extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;

   public StreamListForUniversity(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
	  MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      
      String univname = messageData.getParameter("univname");
      String degree = messageData.getParameter("degree");
      if (univname == null || univname.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("University name is invalid in request " + univname);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "University name is invalid");
         return RuleDecisionKey.ON_FAILURE;
      }
      
      if (degree == null || degree.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("degree name is invalid in request " + degree);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "degree name is invalid");
         return RuleDecisionKey.ON_FAILURE;
      }
      
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      
      
      PreparedStatement ps = null;
      ResultSet rs = null;
      DropDownListData dropDownListData = new DropDownListData();
      dropDownListData.setName("StreamIdMapList");
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "GetStreamListForUniversity");
         ps.setString(1,univname);
         ps.setString(2,degree);
         rs = ps.executeQuery();
         SingleDropDownData data = null;
         while (rs.next())
         {
            data = new SingleDropDownData();
            data.setParam(rs.getString("stream"));
            data.setValue(rs.getString("stream"));
            dropDownListData.getDropDownDatas().add(data);
         }
         response.setStatusCodeSend(false);
         response.setResponseObject(dropDownListData);
         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
         return RuleDecisionKey.ON_SUCCESS;
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching stream for universities and degree drop downs", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while populating stream list drop down");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching degrees for streams drop downs", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while populating streams list drop down");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing result set while populating drop down for streams");
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
}
