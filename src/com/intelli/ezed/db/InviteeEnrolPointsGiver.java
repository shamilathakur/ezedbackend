package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.ActionData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.MasterActionRecords;
import com.intelli.ezed.data.PaymentData;
import com.intelli.ezed.data.UserProfile;

public class InviteeEnrolPointsGiver extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String actionId;
   private static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

   public InviteeEnrolPointsGiver(String ruleName, String dbConnName, String actionId)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
      this.actionId = actionId;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      PaymentData payData = (PaymentData) flowContext.getFromContext(EzedKeyConstants.PAYMENT_DATA);
      UserProfile profile = (UserProfile) flowContext.getFromContext(EzedKeyConstants.USER_DETAILS);

      if (payData.getPaymentStatus() != Integer.parseInt(PaymentData.CART_PAID) && payData.getPaymentStatus() != Integer.parseInt(PaymentData.CART_ADDED_TO_USER))
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Payment was not successful. Not giving points or adding to stream");
         }
         return RuleDecisionKey.ON_FAILURE;
      }

      Connection conn = null;
      PreparedStatement ps = null;
      ResultSet rs = null;
      String invitee = null;

      //checking if the user was invited and this is the first time
      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchFirstEnrolInvitee");
         ps.setString(1, profile.getUserid());
         rs = ps.executeQuery();
         if (rs.next())
         {
            invitee = rs.getString("userid");
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Either user was not invited or this is not first time purchase");
            }
            return RuleDecisionKey.ON_FAILURE;
         }
      }
      catch (Exception e)
      {
         logger.error("Error while fetching invitee details for user " + profile.getUserid(), e);
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing resultset while fetching invite friend details", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      UserProfile userProfile = new UserProfile();
      userProfile.setUserid(invitee);
      try
      {
         userProfile.populateUserProfile(dbConnName, logger);
      }
      catch (Exception e)
      {
         logger.error("Error while populating invitee user profile " + invitee, e);
         return RuleDecisionKey.ON_FAILURE;
      }

      StringBuffer sb = new StringBuffer();
      sb.append(profile.getName());
      sb.append(" invited by ");
      sb.append(userProfile.getName());
      sb.append("made a purchase on EzEd");
      String inviteePurchaseStream = sb.toString();

      ActionData actionData = MasterActionRecords.fetchRecord(actionId);

      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdateUserPoints");
         ps.setInt(1, actionData.getPoints());
         ps.setString(2, userProfile.getUserid());
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows updated in db while incrementing points for userid " + userProfile.getUserid());
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while incrementing points for userid " + userProfile.getUserid(), e);
      }
      catch (Exception e)
      {
         logger.error("Error while incrementing points for userid " + userProfile.getUserid(), e);
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertUserStreamAll");
         ps.setString(1, inviteePurchaseStream);
         ps.setString(2, userProfile.getUserid());
         ps.setString(3, sdf.format(new Date()));
         ps.setString(4, null);
         ps.setString(5, null);
         ps.setString(6, null);
         ps.setString(7, actionId);
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in db while inserting stream for user " + userProfile.getUserid());
         }
      }
      catch (Exception e)
      {
         logger.error("Error inserting stream entry for " + userProfile.getUserid(), e);
         logger.error("INSERT FAILED " + inviteePurchaseStream);
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
      return RuleDecisionKey.ON_SUCCESS;
   }
}
