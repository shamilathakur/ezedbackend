package com.intelli.ezed.rules;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.UserProfile;
import com.intelli.ezed.utils.AccountStatusHelper;

public class UserStatusUpdater extends SingletonRule
{
   Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private int statusToUpdate;

   public UserStatusUpdater(String ruleName, String statusToUpdate)
   {
      super(ruleName);
      this.statusToUpdate = Integer.parseInt(statusToUpdate);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      UserProfile userProfile = (UserProfile) flowContext.getFromContext(EzedKeyConstants.USER_DETAILS);
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      if (userProfile == null)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("UserProfile is null.");
         }
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "User profile not found to be updated");
         return RuleDecisionKey.ON_FAILURE;
      }
      int chkflag = userProfile.getChkflag();

      switch (statusToUpdate)
      {
         //Set Registered
         case 1 :
            chkflag = AccountStatusHelper.setRegistered(chkflag);
            break;

         //Set validated
         case 2 :
            chkflag = AccountStatusHelper.setValidate(chkflag);
            break;

         //Set forgot password
         case 3 :
            chkflag = AccountStatusHelper.setForgotPassword(chkflag);
      }
      userProfile.setChkflag(chkflag);
      return RuleDecisionKey.ON_SUCCESS;
   }
}
