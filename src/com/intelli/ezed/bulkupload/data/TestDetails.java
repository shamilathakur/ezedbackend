package com.intelli.ezed.bulkupload.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.slf4j.Logger;

import com.intelli.ezed.utils.PrepareTestIdBackend;

public class TestDetails
{
   private static PrepareTestIdBackend prepareTestId = new PrepareTestIdBackend("TES2", "ezeddb");
   private String testId;
   private int noOfQuestions;
   private String courseId;
   private String chapterId;
   private String testTime;
   private String description;
   private String testName;

   public String getTestName()
   {
      return testName;
   }
   public void setTestName(String testName)
   {
      this.testName = testName;
   }
   public String getDescription()
   {
      return description;
   }
   public void setDescription(String description)
   {
      this.description = description;
   }
   public String getTestTime()
   {
      return testTime;
   }
   public void setTestTime(String testTime)
   {
      this.testTime = testTime;
   }
   public String getTestId()
   {
      return testId;
   }
   public void setTestId(String testId)
   {
      this.testId = testId;
   }
   public void setNewTestId()
   {
      this.testId = fetchNewTestId();
   }
   public int getNoOfQuestions()
   {
      return noOfQuestions;
   }
   public void setNoOfQuestions(int noOfQuestions)
   {
      this.noOfQuestions = noOfQuestions;
   }
   public String getCourseId()
   {
      return courseId;
   }
   public void setCourseId(String courseId)
   {
      this.courseId = courseId;
   }
   public String getChapterId()
   {
      return chapterId;
   }
   public void setChapterId(String chapterId)
   {
      this.chapterId = chapterId;
   }

   public static String fetchNewTestId()
   {
      return prepareTestId.getNextId();
   }

   public void persistTest(String dbConnName, Logger logger) throws Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertTest");
         ps.setString(1, getTestId());
         ps.setString(2, getCourseId());
         ps.setString(3, getChapterId());
         ps.setInt(4, getNoOfQuestions());
         ps.setString(5, getTestTime());
         ps.setString(6, getTestName());

         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in db while inserting test");
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while inserting test in db", e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while inserting test in db", e);
         throw e;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   public void persistTestAsContent(String dbConnName, Logger logger) throws Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertContent");
         ps.setString(1, getTestId());
         ps.setString(2, getTestName());
         ps.setString(3, getDescription());
         ps.setInt(4, ContentDetails.CONTENT_TEST);
         //put process to calculate hours from minutes(testtime)
         int hours = getHours(getTestTime());
         ps.setInt(5, hours);
         ps.setString(6, "");
         ps.setInt(7, 0);

         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in db while inserting test in contentlist");
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while inserting test in contentlist", e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while inserting test in contentlist", e);
         throw e;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   public void persistTestInLearningPath(String dbConnName, Logger logger, int seqNo) throws Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertLearningPath");
         ps.setString(1, getChapterId());
         ps.setString(2, getTestId());
         ps.setInt(3, seqNo);
         ps.setString(4, ChapterDetails.SHOW_CONTENT);

         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in db while inserting test in learning path");
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while inserting test in learning path", e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while inserting test in learning path", e);
         throw e;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   public void persistTestInCoursePath(String dbConnName, Logger logger, int seqNo) throws Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertCoursePath");
         ps.setString(1, getCourseId());
         ps.setString(2, null);
         ps.setInt(3, seqNo);
         ps.setString(4, getTestId());

         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in db while inserting test in course path");
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while inserting test in course path", e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while inserting test in course path", e);
         throw e;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   private static int getHours(String testTime) throws ParseException
   {
      SimpleDateFormat sdf = new SimpleDateFormat("HHmmss");
      Date date = sdf.parse(testTime);
      Calendar cal = Calendar.getInstance();
      cal.setTime(date);
      int min = cal.get(Calendar.MINUTE);
      int hours = cal.get(Calendar.HOUR_OF_DAY);
      if (min > 0)
      {
         return hours + 1;
      }
      return hours;
   }


   //   public static void main(String[] args) throws ParseException
   //   {
   //      System.out.println(getHours("120400"));
   //   }
}
