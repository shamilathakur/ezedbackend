package com.intelli.ezed.rules;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EventData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.UserProfile;

public class ActionEventPopulator extends SingletonRule
{
   private String actionId;
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public ActionEventPopulator(String ruleName, String dbConnName, String actionId)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
      this.actionId = actionId;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      UserProfile userProfile = (UserProfile) flowContext.getFromContext(EzedKeyConstants.USER_DETAILS);
      if (userProfile == null)
      {
         String userId = (String) flowContext.getFromContext(EzedKeyConstants.USER_ID);
         userProfile = new UserProfile();
         userProfile.setUserid(userId);
         try
         {
            userProfile.populateUserProfile(dbConnName, logger);
         }
         catch (Exception e)
         {
            logger.error("Error while populating user profile from db for userid while populating action event " + userId, e);
            return RuleDecisionKey.ON_FAILURE;
         }
      }

      EventData eventData = new EventData();
      eventData.setActionId(actionId);
      try
      {
         UserEventProcessor.processUserStream(eventData, userProfile, new Response());
      }
      catch (Exception e)
      {
         logger.error("Error while populating action event", e);
         return RuleDecisionKey.ON_FAILURE;
      }
      return RuleDecisionKey.ON_SUCCESS;
   }

}
