package com.intelli.ezed.db;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.CartDataObject;
import com.intelli.ezed.data.CartDataList;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class CartDataForUserFetcherM extends SingletonRule{

	public CartDataForUserFetcherM(String ruleName, String dbConnName) {
		super(ruleName);
		this.dbConnName = dbConnName;
	}
	private String dbConnName;
	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
	@Override
	public RuleDecisionKey executeRule(FlowContext flowContext) {

		Response response = (Response) flowContext
				.getFromContext(EzedKeyConstants.EZED_RESPONSE);
		MessageData messageData = (MessageData) flowContext
				.getFromContext(MessageData.class.getName());
		
		String userid = messageData.getParameter("email");
	     if(userid == null || userid.trim().compareTo("")==0){
	    	 if(logger.isDebugEnabled()){
	    		 logger.debug("user id in request is invalid");
	    	 }
	    	 response.setParameters(EzedTransactionCodes.PROCESSING_REQUEST, "Invalid User Id");
	    	 return RuleDecisionKey.ON_FAILURE;
	     }
	     	 ArrayList<CartDataObject> cartdataobjectList = new ArrayList<CartDataObject>();
			 CartDataList cartdatalist = new CartDataList();
			 Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
				PreparedStatement ps = null;
			 	ResultSet rs = null;
				try {
					ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchCartDataForUser");
					ps.setString(1, userid);
					System.out.println("Final Query : " + ps);
					rs = ps.executeQuery();
					while (rs.next()) {
						CartDataObject cartdataobj = new CartDataObject();
						cartdataobj.setCartid(rs.getString("id"));
						cartdataobj.setCourseid(rs.getString("courseid"));
						cartdataobj.setContentonlyamt(rs.getString("contentonlyamt"));
						cartdataobj.setTestonlyamt(rs.getString("testonlyamt"));
						cartdataobj.setCoursename(rs.getString("coursename"));
						cartdataobj.setEnddate(rs.getString("enddate"));
						cartdataobj.setFullcourseamt(rs.getString("fullcourseamt"));
						cartdataobj.setStartdate(rs.getString("startdate"));
						cartdataobj.setCoursetype(rs.getString("coursetype"));
						cartdataobjectList.add(cartdataobj);
					}
					cartdatalist.setCartobjectlist(cartdataobjectList);
				  }
				 catch (SQLException e)
		         {
		            logger.error("Error while fetching CoursesWithPaging", e);
		            response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "PL SQL Error while fetching CoursesWithPaging : " + e.getMessage());
		            return RuleDecisionKey.ON_FAILURE;
		         }
		         catch (Exception e)
		         {
		            logger.error("Error while fetching test details", e);
		            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching CoursesWithPaging : " + e.getMessage());
		            return RuleDecisionKey.ON_FAILURE;
		         }
		         finally
		         {
		            if (rs != null)
		            {
		               try
		               {
		                  rs.close();
		               }
		               catch (SQLException e)
		               {
		               }
		            }
		            DbConnectionManager.releaseConnection(dbConnName, conn);
		         }
				 response.setResponseObject(cartdatalist);
			     response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
				 return RuleDecisionKey.ON_SUCCESS;
	}
}
