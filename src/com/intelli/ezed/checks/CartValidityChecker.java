package com.intelli.ezed.checks;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.CartData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.UserProfile;

public class CartValidityChecker extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public CartValidityChecker(String ruleName)
   {
      super(ruleName);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      UserProfile userProfile = (UserProfile) flowContext.getFromContext(EzedKeyConstants.USER_DETAILS);
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      CartData[] cart = (CartData[]) flowContext.getFromContext(EzedKeyConstants.CART_DETAILS);

      for (int i = 0; i < cart.length; i++)
      {
         if (!userProfile.isNewCourseValid(cart[i].getCourseId(), cart[i].getEndDate(), cart[i].getCourseType()))
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Course in cart no " + (i + 1) + " is not valid for update since");
            }
            response.setParameters(EzedTransactionCodes.CART_NOT_VALID, "Item no " + (i + 1) + " is not valid for purchase");
            return RuleDecisionKey.ON_FAILURE;
         }
      }
      return RuleDecisionKey.ON_SUCCESS;
   }

}
