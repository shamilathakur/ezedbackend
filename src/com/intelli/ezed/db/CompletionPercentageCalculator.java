package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.PointsProgressData;

public class CompletionPercentageCalculator extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public CompletionPercentageCalculator(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      String userId = (String) flowContext.getFromContext(EzedKeyConstants.USER_ID);

      String courseId = (String) flowContext.getFromContext(EzedKeyConstants.COURSE_ID);
      PointsProgressData data = new PointsProgressData();

      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchCourseViewCount");
         ps.setString(1, courseId);
         ps.setString(2, userId);
         int viewCount = 0;
         rs = ps.executeQuery();
         while (rs.next())
         {
            viewCount = rs.getInt(1);
            if (viewCount == 0)
            {
               data.setTotalUnseetCount(data.getTotalUnseetCount() + rs.getInt(2));
            }
            else
            {
               data.setTotalViewedCount(data.getTotalViewedCount() + rs.getInt(2));
            }
         }
      }
      catch (SQLException e)
      {
         logger.error("DB error while fetching view count for course " + courseId + " and user " + userId, e);
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching view count for course " + courseId + " and user " + userId, e);
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Errow while closing resultset while fetching course view count", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      try
      {
         data.calculateProgress();
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdateCompletionPercentage");
         ps.setInt(1, data.getProgress());
         ps.setString(2, userId);
         ps.setString(3, courseId);
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows updated in db while updating completion percentage as " + data.getProgress() + " for userid " + userId + " and courseid " + courseId);
         }
      }
      catch (Exception e)
      {
         logger.error("Completion percentage could not be updated for userid " + userId + " and courseid " + courseId, e);
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "DeleteMarkedCourses");
         ps.setString(1, userId);
         ps.setString(2, courseId);
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows updated in db while deleting change marked course for userid " + userId + " and courseid " + courseId);
         }
      }
      catch (Exception e)
      {
         logger.error("Change marked row could not be deleted for userid " + userId + " and courseid " + courseId, e);
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }


      return RuleDecisionKey.ON_SUCCESS;
   }
}
