package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.FrontendFetchData;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.utils.EzedHelper;

public class ScratchCardErrorReportGenerator extends SingletonRule {

	private String dbConnName;
	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
	SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyyMMddHHmmss");

	public ScratchCardErrorReportGenerator(String ruleName, String dbConnName) {
		super(ruleName);
		this.dbConnName = dbConnName;
	}

	@Override
	public RuleDecisionKey executeRule(FlowContext flowContext) {

		Response response = (Response) flowContext
				.getFromContext(EzedKeyConstants.EZED_RESPONSE);
		MessageData messageData = (MessageData) flowContext
				.getFromContext(MessageData.class.getName());
		final String query = "SELECT "
				+ "er.scratchcardid,er.userid,er.errordesc,er.errorcode,"
				+ "sc.courseid,sc.coursetype,sc.price,sc.srno,"
				+ "pscm.partnerid "
				+ "from scratchcarderrorlogtable er "
				+ "left join scratchcardlist sc on er.scratchcardid=sc.scratchcardid "
				+ "left join partnerscratchcardmapping pscm on er.scratchcardid = pscm.scratchcardid";

		StringBuilder sb = new StringBuilder("");

		System.out.println("String buffer : " + sb.toString());

		String courseid = messageData.getParameter("courseid");
		if (courseid != null && courseid.trim().compareTo("") != 0
				&& courseid.compareTo("null") != 0) {
			String qryCourseId = " sc.courseid = '" + courseid + "'";
			if (!(sb.length() > 0)) {
				sb.append(" where");
				sb.append(qryCourseId);
			} else {
				sb.append(" and");
				sb.append(qryCourseId);
			}
		}

		System.out.println("String buffer : " + sb.toString());

		String startdate = messageData.getParameter("startdate");
		String enddate = messageData.getParameter("enddate");
		if ((startdate.compareTo("null") != 0 && startdate.compareTo("") != 0 && startdate != null)
				&& enddate.compareTo("null") == 0) {
			enddate = sourceFormat.format(new Date());
		}

		if (startdate.compareTo("null") != 0 && startdate.compareTo("") != 0
				&& startdate != null) {
			String qryDate = " er.errordate between '" + startdate + "' and '"
					+ enddate + "'";
			if (!(sb.length() > 0)) {
				sb.append(" where");
				sb.append(qryDate);
			} else {
				sb.append(" and");
				sb.append(qryDate);
			}
		}

		System.out.println("String buffer : " + sb.toString());

		String partnerid = messageData.getParameter("partnerid");
		if (partnerid != null && partnerid.trim().compareTo("") != 0
				&& partnerid.compareTo("null") != 0) {
			String qryPartnerId = " pscm.partnerid= '" + partnerid + "'";
			if (!(sb.length() > 0)) {
				sb.append(" where");
				sb.append(qryPartnerId);
			} else {
				sb.append(" and");
				sb.append(qryPartnerId);
			}
		}

		String coursetype = messageData.getParameter("coursetype");
		if (coursetype != null && coursetype.trim().compareTo("") != 0
				&& coursetype.compareTo("null") != 0) {
			String qryCourseType = " sc.coursetype = '" + coursetype + "'";
			if (!(sb.length() > 0)) {
				sb.append(" where");
				sb.append(qryCourseType);
			} else {
				sb.append(" and");
				sb.append(qryCourseType);
			}
		}

		String errorcode = messageData.getParameter("errorcode");
		if (errorcode != null && errorcode.trim().compareTo("") != 0
				&& errorcode.compareTo("null") != 0) {
			String qryErrorCode = " er.errorcode = '" + errorcode + "'";
			if (!(sb.length() > 0)) {
				sb.append(" where");
				sb.append(qryErrorCode);
			} else {
				sb.append(" and");
				sb.append(qryErrorCode);
			}
		}

		String finalQuery = query;

		if (!sb.equals("")) {
			finalQuery = finalQuery + sb.toString();
		}
		Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
		PreparedStatement ps = null;
		ResultSet rs = null;

		String[] reportData = null;
		FrontendFetchData ScratchCardErrorReport = new FrontendFetchData();
		response.setResponseObject(ScratchCardErrorReport);
		response.setStatusCodeSend(false);
		ScratchCardErrorReport.setAttribName("aaData");
		ArrayList<String[]> reportList = new ArrayList<String[]>();

		try {
			ps = conn.prepareStatement(finalQuery);
			System.out.println("Final Query : " + ps);
			rs = ps.executeQuery();
			while (rs.next()) {
				reportData = new String[8];
				if (rs.getInt("errorcode") == 252) {
					reportData[0] = "-";
					reportData[1] = rs.getString("userid");
					reportData[2] = rs.getString("scratchcardid");
					reportData[3] = rs.getString("errordesc");
					reportData[4] = "-";
					reportData[5] = "-";
					reportData[6] = "-";
					reportData[7] = "-";
				} else {
					reportData[0] = rs.getInt("srno") + "";
					reportData[1] = rs.getString("userid");
					reportData[2] = rs.getString("scratchcardid");
					reportData[3] = rs.getString("errordesc");
					reportData[4] = EzedHelper.fetchCourseName(dbConnName,
							rs.getString("courseid"), logger);
					int type = rs.getInt("coursetype");
					if (type == 1) {
						reportData[5] = "Content Only";
					} else if (type == 2) {
						reportData[5] = "Test Only";
					} else if (type == 3) {
						reportData[5] = "Full Course";
					}
					reportData[6] = rs.getInt("price") + "";

					reportData[7] = rs.getString("partnerid");
				}
				reportList.add(reportData);
			}
			ScratchCardErrorReport.setAttribList(reportList);
			response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
		} catch (SQLException e) {
			logger.error("Error while fetching SC error details from db : ", e);
			response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED,
					"Error while fetching SC error details");
			return RuleDecisionKey.ON_FAILURE;
		} catch (Exception e) {
			logger.error("Internal Error while fetching SC error details : ", e);
			response.setParameters(EzedTransactionCodes.INTERNAL_ERROR,
					"Error while fetching SC error details");
			return RuleDecisionKey.ON_FAILURE;
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					logger.error(
							"Error while closing result set while fetching SC error list",
							e);
				}
			}
			DbConnectionManager.releaseConnection(dbConnName, conn);
		}
		return RuleDecisionKey.ON_SUCCESS;
	}
}
