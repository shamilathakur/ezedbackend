package com.intelli.ezed.rules;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.ActionData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.MasterActionRecords;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.TestDetails;
import com.intelli.ezed.data.TestResultResponse;
import com.intelli.ezed.data.TestResultResponseWithPieChart;
import com.intelli.ezed.data.UserProfile;

public class TestEndResultCreator extends SingletonRule
{

   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;
   private static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
   private String actionIdMega = "12";

   public TestEndResultCreator(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   public TestEndResultCreator(String ruleName, String dbConnName, String actionIdMega)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
      this.actionIdMega = actionIdMega;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      UserProfile userProfile = (UserProfile) flowContext.getFromContext(EzedKeyConstants.USER_DETAILS);
      TestDetails testDetails = (TestDetails) flowContext.getFromContext(TestDetails.TEST_DETAILS);

      String testSessionId = (String) flowContext.getFromContext(EzedKeyConstants.TEST_SESSION_ID);
      ActionData megaActionData = MasterActionRecords.fetchRecord(actionIdMega);
      Integer diffWisePoints = (Integer) flowContext.getFromContext(EzedKeyConstants.DIFF_WISE_POINTS);
      Integer maxPoints = (Integer) flowContext.getFromContext(EzedKeyConstants.MAX_TEST_POINTS);
      Boolean megaValid = (Boolean) flowContext.getFromContext(EzedKeyConstants.MEGA_TEST_POINTS);

      String userid = userProfile.getUserid();
      String testId = testDetails.getTestid();
      int numberOfQuestions = testDetails.getNumberOfQuestions();
      int attemptedQuestions = testDetails.getCurrentQuestionNumber();
      int correctAnsGiven = 0;

      ResultSet resultSet = null;
      int isTestComplete = 0; //0 -> not : 1 -> yes

      Connection connection = DbConnectionManager.getConnectionByName(dbConnName);
      try
      {
         //select * from ongoingtest where testsessionid = ? and userid = ? order by qserialnumber;
         PreparedStatement preparedStatement = DbConnectionManager.getPreparedStatement(dbConnName, connection, "GetTestResult");
         preparedStatement.setString(1, testSessionId);
         preparedStatement.setString(2, userid);

         resultSet = preparedStatement.executeQuery();


         String correctAns = null;
         String receivedCorrectAnsString = null;
         while (resultSet.next())
         {
            correctAns = resultSet.getString("correctans");
            receivedCorrectAnsString = resultSet.getString("receivedcorrectansstring");
            if (correctAns != null)
            {
               correctAns = correctAns.replaceAll(" ", "");
            }
            if (receivedCorrectAnsString != null)
            {
               receivedCorrectAnsString = receivedCorrectAnsString.replaceAll(" ", "");
            }
            if (correctAns.compareToIgnoreCase(receivedCorrectAnsString) == 0)
            {
               correctAnsGiven = correctAnsGiven + 1;
            }
         }
         testDetails.setCorrectAnsGiven(correctAnsGiven);

         if (attemptedQuestions == numberOfQuestions)
         {
            isTestComplete = 1;
         }

         //insert into testsummery (testid,userid,testsessionid,numberofquestions,attempted,correct,testcompleted) values (?,?,?,?,?,?,?)
         PreparedStatement insertIntoTestSummary = DbConnectionManager.getPreparedStatement(dbConnName, connection, "InsetTestSummeryInDatabase");
         insertIntoTestSummary.setString(1, testId);
         insertIntoTestSummary.setString(2, userid);
         insertIntoTestSummary.setString(3, testSessionId);
         insertIntoTestSummary.setInt(4, numberOfQuestions);
         insertIntoTestSummary.setInt(5, attemptedQuestions);
         insertIntoTestSummary.setInt(6, correctAnsGiven);
         insertIntoTestSummary.setInt(7, isTestComplete);
         insertIntoTestSummary.setString(8, sdf.format(new Date()));
         insertIntoTestSummary.setLong(9, testDetails.getTestDuration());
         int points = 0;
         points += diffWisePoints;
         if (megaValid)
         {
            points += megaActionData.getPoints();
         }
         insertIntoTestSummary.setInt(10, points);
         insertIntoTestSummary.setInt(11, maxPoints == null ? 0 : maxPoints);

         int i = insertIntoTestSummary.executeUpdate();

         if (i != 1)
         {
            logger.debug("No test summary is created in database. Number of rows inserted in testSummary table are : " + i);
         }

      }
      catch (SQLException e)
      {
         logger.debug("Database exception while trying to create test summary : ", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Database exception occored. Please try again.");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.debug("Exception while trying to create test summary : ", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal Error. Please try again.");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         try
         {
            resultSet.close();
         }
         catch (Exception e)
         {
            logger.error("Exception occoure when trying to close the result set.", e);
         }
         DbConnectionManager.releaseConnection(dbConnName, connection);
      }


      response.setParameters(EzedTransactionCodes.TEST_END_REACHED, "Test Questions finished.");
      //      TestResultResponse testResultResponse = new TestResultResponse();
      TestResultResponseWithPieChart testResultResponseWithPieChart = (TestResultResponseWithPieChart) response.getResponseObject();
      TestResultResponse testResultResponse = testResultResponseWithPieChart.getTestResultResponse();
      testResultResponse.setCorrectAns(Integer.toString(correctAnsGiven));
      testResultResponse.setFinalScore(Integer.toString(correctAnsGiven));

      //      response.setResponseObject(testResultResponse);

      return RuleDecisionKey.ON_SUCCESS;
   }
}
