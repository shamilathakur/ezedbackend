package com.intelli.ezed.install;

import java.io.FileInputStream;
import java.util.Properties;

import org.sabre.exceptions.SabreInitializationException;
import org.sabre.workflow.LinkedProcessorManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MbcLoggerNames;
import com.a.helper.session.SessionManager;
import com.a.helper.transaction.TransactionPersister;
import com.a.helper.utils.StringSplitter;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.UserProfile;
import com.intelli.ezed.rules.ActionRecordCreator;
import com.intelli.ezed.utils.EzedAdminSessionTimeoutHandler;
import com.intelli.ezed.utils.EzedSessionTimeoutHandler;
import com.intelli.ezed.utils.EzedTestSessionTimeoutHandler;
import com.intelli.ezed.utils.Globals;
import com.intelli.ezed.utils.PasswordHelper;
import com.intelli.ezed.utils.PrepareCityId;
import com.intelli.ezed.utils.SymLinkManager;
import com.intelli.ezed.utils.SymLinkTimeoutHandler;
import com.intelli.ezed.utils.SymlinkEncryptionHandler;

public class Install
{
   static Logger logger = LoggerFactory.getLogger(MbcLoggerNames.SERVICE_LOG);
   public static void main(String[] args)
   {
      try
      {
         String propertiesFile = "conf/ezed/EZED.properties";
         new org.sabre.install.Install().install(propertiesFile);

         Properties properties = new Properties();
         try
         {
            //to check if its running on box
            String isBoxString = properties.getProperty("box");
            if (isBoxString != null)
            {
               Globals.isBox = Boolean.parseBoolean(isBoxString);
            }

            //to make sure it's static method is called during install itself
            ActionRecordCreator actionRecordCreator;
            properties.load(new FileInputStream(propertiesFile));

            //initializing session
            String sessionTimeout = properties.getProperty("session.timeout.period");
            TransactionPersister persister = new TransactionPersister(Long.parseLong(sessionTimeout), new EzedSessionTimeoutHandler());
            String sessionGeneratorName = properties.getProperty("session.generator.name");
            int sessionIdLength = Integer.parseInt(properties.getProperty("session.id.length"));
            SessionManager sessionManager = new SessionManager(persister, sessionGeneratorName, sessionIdLength);

            //initializing test session
            String testSessionTimeout = properties.getProperty("test.session.timeout.period");
            TransactionPersister testSessionPersister = new TransactionPersister(Long.parseLong(testSessionTimeout), new EzedTestSessionTimeoutHandler());
            String testSessionGeneratorName = properties.getProperty("test.session.generator.name");
            int testSessionIdLength = Integer.parseInt(properties.getProperty("test.session.id.length"));
            SessionManager testSessionManager = new SessionManager(testSessionPersister, testSessionGeneratorName, testSessionIdLength);

            //initializing admin session
            String adminSessionTimeout = properties.getProperty("admin.session.timeout.period");
            TransactionPersister adminSessionPersister = new TransactionPersister(Long.parseLong(adminSessionTimeout), new EzedAdminSessionTimeoutHandler());
            String adminSessionGeneratorName = properties.getProperty("admin.session.generator.name");
            int adminSessionIdLength = Integer.parseInt(properties.getProperty("admin.session.id.length"));
            SessionManager adminSessionManager = new SessionManager(adminSessionPersister, adminSessionGeneratorName, adminSessionIdLength);

            PrepareCityId prepareCityId = new PrepareCityId("CIT1", "ezeddb");

            //initializing sym link manager
            String symLinkGenName = properties.getProperty("symlink.generator.name");
            int linkSize = Integer.parseInt(properties.getProperty("symlink.link.size"));
            long longTimeout = Long.parseLong(properties.getProperty("symlink.long.timeout"));
            long shortTimeout = Long.parseLong(properties.getProperty("symlink.short.timeout"));
            long nonSessionTimeout = Long.parseLong(properties.getProperty("symlink.nonsession.timeout"));
            SymLinkManager symLinkManager = new SymLinkManager(symLinkGenName, linkSize, longTimeout, shortTimeout, nonSessionTimeout);

            SymlinkEncryptionHandler.initializeCipher();

            int endDateBuffer = Integer.parseInt(properties.getProperty("course.enddate.buffer"));
            UserProfile.endDateBuffer = endDateBuffer;

            String key = properties.getProperty("cipher.key");
            if (key != null && key.compareTo("") != 0)
            {
               PasswordHelper.initializeCipher(key);
            }
            else
            {
               logger.error("Error initialising ciphers since key is not present in properties file. Please put correct key against key : cipher.key");
               System.exit(1);
            }

            String[] inputProcessors = StringSplitter.splitString(properties.getProperty("inputProcessor.names"), ",");
            for (String processor : inputProcessors)
            {
               LinkedProcessorManager.updateProcessorContext(processor, EzedKeyConstants.SESSION_MANAGER, sessionManager);
               LinkedProcessorManager.updateProcessorContext(processor, EzedKeyConstants.TEST_SESSION_MANAGER, testSessionManager);
               LinkedProcessorManager.updateProcessorContext(processor, EzedKeyConstants.ADMIN_SESSION_MANAGER, adminSessionManager);
               LinkedProcessorManager.updateProcessorContext(processor, EzedKeyConstants.CITY_ID_GENERATOR, prepareCityId);
               LinkedProcessorManager.updateProcessorContext(processor, EzedKeyConstants.SYMLINK_MANAGER, symLinkManager);
               LinkedProcessorManager.updateProcessorContext(processor, EzedKeyConstants.BUFFER_END_DATE, endDateBuffer);
            }

            SymLinkTimeoutHandler.sessionManager = sessionManager;
            SymLinkTimeoutHandler.symLinkManager = symLinkManager;
         }
         catch (Exception e)
         {
            logger.error("Could not initialize  :", e);
            System.exit(1);
         }

      }
      catch (SabreInitializationException e)
      {
         logger.error("Error initialising", e);
         System.exit(1);
      }
   }

}
