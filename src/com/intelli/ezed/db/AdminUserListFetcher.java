package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.FrontendFetchData;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.utils.AccountStatusHelper;

public class AdminUserListFetcher extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private SimpleDateFormat sdfDb = new SimpleDateFormat("yyyyMMddHHmmss");
   private SimpleDateFormat sdfPage = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

   public AdminUserListFetcher(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      String[] adminUserArr = null;
      FrontendFetchData userData = new FrontendFetchData();
      response.setResponseObject(userData);
      response.setStatusCodeSend(false);
      userData.setAttribName("aaData");
      ArrayList<String[]> users = new ArrayList<String[]>();
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchAdminUserList");
         rs = ps.executeQuery();
         int i = 1;
         String name = null;
         int checkFlag;
         while (rs.next())
         {
            adminUserArr = new String[6];
            adminUserArr[0] = Integer.toString(i);
            adminUserArr[1] = rs.getString(1);
            name = rs.getString(2);
            if (name == null)
            {
               adminUserArr[2] = "";
            }
            else
            {
               adminUserArr[2] = name;
            }
            adminUserArr[3] = rs.getString(3);
            if (adminUserArr[3] == null)
            {
               adminUserArr[3] = "";
            }
            try
            {
               adminUserArr[5] = sdfPage.format(sdfDb.parse(rs.getString(4)));
            }
            catch (Exception e)
            {
               adminUserArr[5] = "";
            }
            checkFlag = rs.getInt(5);
            if (AccountStatusHelper.isRegistered(checkFlag))
            {
               if (AccountStatusHelper.isLock(checkFlag))
               {
                  adminUserArr[4] = "BLOCKED";
               }
               else
               {
                  adminUserArr[4] = "ACTIVE";
               }
            }
            else
            {
               adminUserArr[4] = "INACTIVE";
            }
            //            adminUserArr[5] = fetchUserCourse(dbConnName, adminUserArr[1], logger);
            i++;
            users.add(adminUserArr);
         }
         userData.setAttribList(users);
         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
      }
      catch (SQLException e)
      {
         logger.error("DB error while fetching list of users", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "DB error while fetching users");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching list of users", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching users");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing result set while fetching user list", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      return RuleDecisionKey.ON_SUCCESS;
   }

   private String fetchAdminUserRoles(String dbConnName, String userId, Logger logger) throws Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchAdminRoles");
         ps.setString(1, userId);
         rs = ps.executeQuery();
         StringBuffer sb = new StringBuffer();
         while (rs.next())
         {
            sb.append(rs.getString(1));
         }
         if (sb.length() > 0)
         {
            sb.deleteCharAt(sb.length() - 1);
         }
         return sb.toString();
      }
      catch (Exception e)
      {
         throw e;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
               logger.error("Error while closing resultset while fetching courses of users", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
}
