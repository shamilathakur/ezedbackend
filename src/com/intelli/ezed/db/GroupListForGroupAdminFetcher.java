package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.analytics.data.GenericChartData;
import com.intelli.ezed.data.AdminProfile;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.SingleGroupData;

public class GroupListForGroupAdminFetcher extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public GroupListForGroupAdminFetcher(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      AdminProfile profile = (AdminProfile) flowContext.getFromContext(EzedKeyConstants.ADMIN_PROFILE);
      String userId = profile.getUserid();
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;

      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchGroupAdminGroups");
         ps.setString(1, userId);
         rs = ps.executeQuery();
         SingleGroupData groupData = null;
         ArrayList<SingleGroupData> groups = new ArrayList<SingleGroupData>(10);
         while (rs.next())
         {
            groupData = new SingleGroupData();
            groupData.setGroupId(rs.getString("groupid"));
            groupData.setGroupName(rs.getString("groupname"));
            groupData.setCourseId(rs.getString("courseid"));
            groups.add(groupData);
         }

         GenericChartData data = new GenericChartData();
         JSONArray array = new JSONArray();
         for (int i = 0; i < groups.size(); i++)
         {
            groups.get(i).prepareResponse();
            array.put(groups.get(i));
         }
         data.getText().add("groups");
         data.getValues().add(array);
         response.setResponseObject(data);
         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
         return RuleDecisionKey.ON_SUCCESS;
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching list of groups", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching list of groups");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching list of groups", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching list of groups");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing result set while fetching list of groups for group admin ", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
}
