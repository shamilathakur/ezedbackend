package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class ContentToChapterAdder extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   public static final Object chapterLock = new Object();

   public ContentToChapterAdder(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());

      String chapterId = messageData.getParameter("chapterid");
      if (chapterId == null || chapterId.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Chapter id in request in invalid");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid chapter id");
         return RuleDecisionKey.ON_FAILURE;
      }
      chapterId = chapterId.trim().toUpperCase();

      String contentId = messageData.getParameter("contentid");
      if (contentId == null || contentId.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Content id in request is invalid");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid content id");
         return RuleDecisionKey.ON_FAILURE;
      }
      contentId = contentId.trim().toUpperCase();
      int currentSeqNo = 1;
      Map<String, String> contentMap = new HashMap<String, String>();

      //      try
      //      {
      //         courseLock.wait();
      //      }
      //      catch (InterruptedException e1)
      //      {
      //      }
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchChapterContents");
         ps.setString(1, chapterId);
         rs = ps.executeQuery();
         String singleContentId = null;
         while (rs.next())
         {
            singleContentId = rs.getString("contentid");
            contentMap.put(singleContentId.intern(), singleContentId);
            currentSeqNo++;
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while inserting content in db for chapter id " + chapterId, e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while adding new content");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while inserting content in db for chapter id " + chapterId, e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while adding new content");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing resultset while adding content to chapter", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      if (contentMap.get(contentId.intern()) != null)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Content id to be added already exists in db");
         }
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Content id already exists");
         return RuleDecisionKey.ON_FAILURE;
      }

      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "AddChapterContent");
         ps.setString(1, chapterId);
         ps.setString(2, contentId);
         ps.setInt(3, currentSeqNo);
         int rows = ps.executeUpdate();
         if (rows > 0)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug(rows + " rows inserted in db while adding content to chapter " + chapterId + ". Added content " + contentId);
            }
            response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
            return RuleDecisionKey.ON_SUCCESS;
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("No rows were updated");
            }
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "New content could not be added");
            return RuleDecisionKey.ON_FAILURE;
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while inserting content in db for chapter id " + chapterId, e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while adding new content");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while inserting content in db for chapter id " + chapterId, e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while adding new content");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
}