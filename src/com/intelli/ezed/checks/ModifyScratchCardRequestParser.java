package com.intelli.ezed.checks;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.ScratchCardData;

public class ModifyScratchCardRequestParser extends SingletonRule {

	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

	public ModifyScratchCardRequestParser(String ruleName) {
		super(ruleName);
	}

	@Override
	public RuleDecisionKey executeRule(FlowContext flowContext) {

		Response response = (Response) flowContext
				.getFromContext(EzedKeyConstants.EZED_RESPONSE);
		MessageData messageData = (MessageData) flowContext
				.getFromContext(MessageData.class.getName());

		ScratchCardData data = new ScratchCardData();

		String value;

		value = messageData.getParameter("courseid");
		if (value == null || value.trim().compareTo("") == 0
				|| value.trim().compareToIgnoreCase("null") == 0) {
			if (logger.isDebugEnabled()) {
				logger.debug("CourseID is invalid");
			}
			response.setParameters(
					EzedTransactionCodes.INVALID_MESSAGE_PARAMETER,
					"Course id is invalid");
			return RuleDecisionKey.ON_FAILURE;
		}
		data.setCourseId(value);

		value = messageData.getParameter("coursetype");
		if (value == null || value.trim().compareTo("") == 0
				|| value.trim().compareToIgnoreCase("null") == 0 || value.compareTo("default") ==0) {
			if (logger.isDebugEnabled()) {
				logger.debug("Course Type is invalid");
			}
			response.setParameters(
					EzedTransactionCodes.INVALID_MESSAGE_PARAMETER,
					"Course Type is invalid");
			return RuleDecisionKey.ON_FAILURE;
		}
		data.setCourseType(Integer.parseInt(value));

		value = messageData.getParameter("validtill");
		if (value == null || value.trim().compareTo("") == 0
				|| value.trim().compareToIgnoreCase("null") == 0) {
			if (logger.isDebugEnabled()) {
				logger.debug("Valid till date is invalid");
			}
			response.setParameters(
					EzedTransactionCodes.INVALID_MESSAGE_PARAMETER,
					"Valid till date is invalid");
			return RuleDecisionKey.ON_FAILURE;
		}
		data.setValidTill(value);

		value = messageData.getParameter("price");
		if (value == null || value.trim().compareTo("") == 0
				|| value.trim().compareToIgnoreCase("null") == 0) {
			if (logger.isDebugEnabled()) {
				logger.debug("Price is invalid");
			}
			response.setParameters(
					EzedTransactionCodes.INVALID_MESSAGE_PARAMETER,
					"Price is invalid");
			return RuleDecisionKey.ON_FAILURE;
		}
		data.setPrice(Integer.parseInt(value));

		value = messageData.getParameter("assignedstatus");
		if (value == null || value.trim().compareTo("") == 0
				|| value.trim().compareToIgnoreCase("null") == 0) {
			if (logger.isDebugEnabled()) {
				logger.debug("Assigned Status is invalid");
			}
			response.setParameters(
					EzedTransactionCodes.INVALID_MESSAGE_PARAMETER,
					"Assighned status is invalid");
			return RuleDecisionKey.ON_FAILURE;
		}
		data.setAssignedStatus(Integer.parseInt(value));

		value = messageData.getParameter("flag");
		if (value == null || value.trim().compareTo("") == 0
				|| value.trim().compareToIgnoreCase("null") == 0) {
			if (logger.isDebugEnabled()) {
				logger.debug("Flag is invalid");
			}
			response.setParameters(
					EzedTransactionCodes.INVALID_MESSAGE_PARAMETER,
					"Flag is invalid");
			return RuleDecisionKey.ON_FAILURE;
		}
		int flag = Integer.parseInt(value); // This flag is used to indentify
											// what kind of update status it is
											// .. sent from front end .. The 3
											// types of values ecpalined in next
											// Rule in the RuleChain for Update
											// scratch card ..

		if (data.getAssignedStatus() == 1) {
			value = messageData.getParameter("partnerid");
			if (value == null || value.trim().compareTo("") == 0
					|| value.trim().compareToIgnoreCase("null") == 0) {
				if (logger.isDebugEnabled()) {
					logger.debug("Partner id is invalid");
				}
				response.setParameters(
						EzedTransactionCodes.INVALID_MESSAGE_PARAMETER,
						"Partner id is invalid");
				return RuleDecisionKey.ON_FAILURE;
			}
			data.setAssignedTo(value);
		}

		value = messageData.getParameter("scratchcardid");
		if (value == null || value.trim().compareTo("") == 0
				|| value.trim().compareToIgnoreCase("null") == 0) {
			if (logger.isDebugEnabled()) {
				logger.debug("scratch card id is invalid");
			}
			response.setParameters(
					EzedTransactionCodes.INVALID_MESSAGE_PARAMETER,
					"scratch card id is invalid");
			return RuleDecisionKey.ON_FAILURE;
		}
		data.setScratchCardId(value);

		value = messageData.getParameter("srno");
		if (value == null || value.trim().compareTo("") == 0
				|| value.trim().compareToIgnoreCase("null") == 0) {
			if (logger.isDebugEnabled()) {
				logger.debug("Srno is invalid");
			}
			response.setParameters(
					EzedTransactionCodes.INVALID_MESSAGE_PARAMETER,
					"Srno is invalid");
			return RuleDecisionKey.ON_FAILURE;
		}
		data.setSrno(Integer.parseInt(value));

		String lastModified = sdf.format(new Date());
		data.setLastModified(lastModified);

		String lastModifiedBy = messageData.getParameter("email");
		data.setLastModifiedBy(lastModifiedBy);

		data.setCardActiveStatus(1);

		flowContext.putIntoContext(EzedKeyConstants.SCRATCH_CARD_OBJECT, data);
		flowContext.putIntoContext("FLAG", flag);
		return RuleDecisionKey.ON_SUCCESS;
	}

}
