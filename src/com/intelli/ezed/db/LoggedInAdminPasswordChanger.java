package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.AdminProfile;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class LoggedInAdminPasswordChanger extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;
   public LoggedInAdminPasswordChanger(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());

      String newPassword = messageData.getParameter("newpassword");
      AdminProfile profile = (AdminProfile) flowContext.getFromContext(EzedKeyConstants.ADMIN_PROFILE);
      String userid = profile.getUserid();

      if (newPassword == null || newPassword.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("new password is not valid " + newPassword);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid new password");
         return RuleDecisionKey.ON_FAILURE;
      }

      Connection connection = DbConnectionManager.getConnectionByName(dbConnName);

      //      try
      //      {
      //         newPassword = PasswordHelper.getEncryptedData(newPassword);
      //      }
      //      catch (IllegalBlockSizeException e1)
      //      {
      //         logger.error("Error while changing password", e1);
      //         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while changing password admin");
      //         return RuleDecisionKey.ON_FAILURE;
      //      }
      //      catch (BadPaddingException e1)
      //      {
      //         logger.error("Error while changing password", e1);
      //         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while changing password admin");
      //         return RuleDecisionKey.ON_FAILURE;
      //      }

      try
      {
         //update adminusers set password = ? where userid = ?
         PreparedStatement updatePassword = DbConnectionManager.getPreparedStatement(dbConnName, connection, "ChangePasswordAdmin");
         updatePassword.setString(1, newPassword);
         updatePassword.setString(2, userid);
         int i = updatePassword.executeUpdate();

         if (i == 1)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Change password successful. : " + userid);
            }
            response.setParameters(EzedTransactionCodes.SUCCESS, "Change password successful.");
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Change password fail for user : " + userid);
            }
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Password change fail. Please try again.");
            return RuleDecisionKey.ON_FAILURE;
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while updating password", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while updating password.");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while updating password", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal Error please try again.");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, connection);
      }
      return RuleDecisionKey.ON_SUCCESS;
   }
}
