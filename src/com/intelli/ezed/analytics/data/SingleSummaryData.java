package com.intelli.ezed.analytics.data;


public class SingleSummaryData
{
   private int noOfQuestions;
   private boolean isTestCompleted;
   private int noOfQuestionsAttempted;
   private int noOfCorrectAnswers;

   public int getNoOfQuestions()
   {
      return noOfQuestions;
   }
   public void setNoOfQuestions(int noOfQuestions)
   {
      this.noOfQuestions = noOfQuestions;
   }
   public boolean isTestCompleted()
   {
      return isTestCompleted;
   }
   public void setTestCompleted(boolean isTestCompleted)
   {
      this.isTestCompleted = isTestCompleted;
   }
   public int getNoOfQuestionsAttempted()
   {
      return noOfQuestionsAttempted;
   }
   public void setNoOfQuestionsAttempted(int noOfQuestionsAttempted)
   {
      this.noOfQuestionsAttempted = noOfQuestionsAttempted;
   }
   public int getNoOfCorrectAnswers()
   {
      return noOfCorrectAnswers;
   }
   public void setNoOfCorrectAnswers(int noOfCorrectAnswers)
   {
      this.noOfCorrectAnswers = noOfCorrectAnswers;
   }
   public float getCorrectPercent()
   {
      if (noOfQuestions == 0)
      {
         return 0;
      }
      float percent = (float) noOfCorrectAnswers / (float) noOfQuestions * 100f;
      return percent;
   }
   
   public float getAttemptPercent()
   {
      if (noOfQuestions == 0)
      {
         return 0;
      }
      float percent = (float) noOfQuestionsAttempted / (float) noOfQuestions * 100f;
      return percent;
   }
}
