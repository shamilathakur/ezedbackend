package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.UserProfile;

public class InvitedRegTimeUpdater extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

   public InvitedRegTimeUpdater(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      UserProfile userProfile = (UserProfile) flowContext.getFromContext(EzedKeyConstants.USER_DETAILS);
      String userId = userProfile.getUserid();
      String inviteeid = userProfile.getInvitedBy();
      if (inviteeid == null)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("User was not invited by anyone. Not updating reg time in invite list");
         }
         return RuleDecisionKey.ON_FAILURE;
      }

      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdateInvitedRegTime");
         ps.setString(1, sdf.format(new Date()));
         ps.setString(2, inviteeid);
         ps.setString(3, userId);
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows updated in db while updating reg time for invited friend");
         }
      }
      catch (Exception e)
      {
         logger.error("Error while updating reg time for invited friend", e);
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
      return RuleDecisionKey.ON_SUCCESS;
   }

}
