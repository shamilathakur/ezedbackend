package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.utils.PrepareCityId;

public class CityAddDeleter extends SingletonRule
{

   private static final int ADD_CITY = 1;
   private static final int MOD_CITY = 2;
   private static final int DEL_CITY = 3;

   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;
   private int actionType;

   public CityAddDeleter(String ruleName, String dbConnName, String actionType)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
      this.actionType = Integer.parseInt(actionType);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      String countryName = messageData.getParameter("country");
      String stateName = messageData.getParameter("state");
      String cityName = messageData.getParameter("city");

      if (countryName == null || countryName.equals("") || countryName.equals(" "))
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("country Name not provided.");
         }
         response.setParameters(EzedTransactionCodes.WRONG_COUNTRY, "Please enter correct country Name.");
         return RuleDecisionKey.ON_FAILURE;
      }

      if (stateName == null || stateName.equals("") || stateName.equals(" "))
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("state Name not provided.");
         }
         response.setParameters(EzedTransactionCodes.WRONG_STATE, "Please enter correct state Name.");
         return RuleDecisionKey.ON_FAILURE;
      }
      if (cityName == null || cityName.equals("") || cityName.equals(" "))
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("city Name not provided.");
         }
         response.setParameters(EzedTransactionCodes.WRONG_CITY, "Please enter correct city Name.");
         return RuleDecisionKey.ON_FAILURE;
      }

      Boolean status = false;

      if (actionType == ADD_CITY)
      {
         status = addCity(countryName, stateName, cityName, response, flowContext);
      }
      else if (actionType == DEL_CITY)
      {
         status = deleteCity(countryName, stateName, cityName, response);
      }
      //CB0023
      else if (actionType == MOD_CITY)
      {
         String oldCountryName = messageData.getParameter("oldcountry");
         String oldState = messageData.getParameter("oldstate");
         String oldcityName = messageData.getParameter("oldcity");

         if (oldcityName == null || oldcityName.equals("") || oldcityName.equals(" "))
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("old city Name not provided.");
            }
            response.setParameters(EzedTransactionCodes.WRONG_CITY, "Please enter correct old city Name.");
            return RuleDecisionKey.ON_FAILURE;
         }

         if (oldCountryName == null || oldCountryName.equals("") || oldCountryName.equals(" "))
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("old country Name not provided.");
            }
            response.setParameters(EzedTransactionCodes.WRONG_COUNTRY, "Please enter correct old country Name.");
            return RuleDecisionKey.ON_FAILURE;
         }

         if (oldState == null || oldState.equals("") || oldState.equals(" "))
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("old state Name not provided.");
            }
            response.setParameters(EzedTransactionCodes.WRONG_STATE, "Please enter correct old state Name.");
            return RuleDecisionKey.ON_FAILURE;
         }

         status = deleteCity(oldCountryName, oldState, oldcityName, response);
         if (status == true)
         {
            status = addCity(countryName, stateName, cityName, response, flowContext);
         }
      }


      if (status == true)
      {
         return RuleDecisionKey.ON_SUCCESS;
      }
      else
      {
         return RuleDecisionKey.ON_FAILURE;
      }
   }

   private Boolean deleteCity(String countryName, String stateName, String cityName, Response response)
   {
      Connection connection = DbConnectionManager.getConnectionByName(dbConnName);

      try
      {
         //Delete from citylist where statename = ? and countryname = ? and cityname = ?
         PreparedStatement deleteState = DbConnectionManager.getPreparedStatement(dbConnName, connection, "deleteCityFromDb");
         deleteState.setString(1, stateName);
         deleteState.setString(2, countryName);
         deleteState.setString(3, cityName);
         int i = deleteState.executeUpdate();

         if (logger.isDebugEnabled())
         {
            logger.debug("Number of rows deleted : " + i + " while deleting city : " + cityName + stateName + "," + countryName);
         }
         if (i == 0)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("No state found in db with state name : " + cityName + "," + countryName);
            }
            response.setParameters(EzedTransactionCodes.SUCCESS, cityName + " not found in system");
            return true;
         }

      }
      catch (SQLException e)
      {
         //e.printStackTrace();
         if (e.getMessage().contains("foreign key constraint"))
         {
            if (logger.isInfoEnabled())
            {
               logger.info("Can not delete city : " + cityName + " from database it is refereced from some other table.", e);
            }
            response.setParameters(EzedTransactionCodes.INVALID_OPRATION, "City is being used in application can not delete.");
         }
         else
         {
            logger.error("Database exception occoured.", e);
            response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Database exception occoured while deleting the city.");
         }
         return false;
      }
      catch (Exception e)
      {
         logger.error("exception occoured.", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal Error while adding the city.");
         return false;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, connection);
      }

      response.setParameters(EzedTransactionCodes.SUCCESS, "city : " + cityName + " deleted from database.");
      return true;
   }

   private Boolean addCity(String countryName, String stateName, String cityName, Response response, FlowContext flowContext)
   {
      Connection connection = DbConnectionManager.getConnectionByName(dbConnName);
      PrepareCityId prepareCityId = (PrepareCityId) flowContext.getFromContext(EzedKeyConstants.CITY_ID_GENERATOR);
      String cityId = prepareCityId.getNextId();

      try
      {
         //insert into citylist (countryname,statename,cityname,cityid) values (?,?,?,?)
         PreparedStatement addCity = DbConnectionManager.getPreparedStatement(dbConnName, connection, "AddCityInDb");
         addCity.setString(1, countryName);
         addCity.setString(2, stateName);
         addCity.setString(3, cityName);
         addCity.setString(4, cityId);

         int i = addCity.executeUpdate();

         if (logger.isDebugEnabled())
         {
            logger.debug(i + " rows added in database.");
         }

      }
      catch (SQLException e)
      {
         if (e.getMessage().contains("foreign key constraint"))
         {
            if (logger.isInfoEnabled())
            {
               logger.info("Invalid State or Country name provided. ", e);
            }
            response.setParameters(EzedTransactionCodes.WRONG_COUNTRY, "State or country name not present in system.");
            return false;
         }
         else if (e.getMessage().contains("unique constraint"))
         {
            if (logger.isInfoEnabled())
            {
               logger.info("City name already present in system.", e);
            }
            response.setParameters(EzedTransactionCodes.DUPLICATE_CITY, "City name already added in system.");
            return false;
         }

         logger.error("Database exception occoured while trying to add city", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Database exception occoured while trying to add city.");
         return false;
      }
      catch (Exception e)
      {
         logger.error("exception occoured.", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal Error while adding the city.");
         return false;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, connection);
      }

      response.setParameters(EzedTransactionCodes.SUCCESS, "city : " + cityName + " added to database.");
      return true;
   }
}
