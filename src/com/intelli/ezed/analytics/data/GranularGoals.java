package com.intelli.ezed.analytics.data;

import com.intelli.ezed.data.ResponseObject;

public class GranularGoals extends ResponseObject
{
   private String name;
   private int goalsAchievedNow;
   private int goalsFailedNow;
   private int goalsAchievedPrevious;
   private int goalsFailedPrevious;

   public GranularGoals(String name, int goalsAchNow, int goalsFailedNow, int goalsAchPrev, int goalsFailedPrev)
   {
      this.name = name;
      this.goalsAchievedNow = goalsAchNow;
      this.goalsFailedNow = goalsFailedNow;
      this.goalsAchievedPrevious = goalsAchPrev;
      this.goalsFailedPrevious = goalsFailedPrev;
   }

   public String getName()
   {
      return name;
   }

   public void setName(String name)
   {
      this.name = name;
   }

   public int getGoalsAchievedNow()
   {
      return goalsAchievedNow;
   }

   public void setGoalsAchievedNow(int goalsAchievedNow)
   {
      this.goalsAchievedNow = goalsAchievedNow;
   }

   public int getGoalsFailedNow()
   {
      return goalsFailedNow;
   }

   public void setGoalsFailedNow(int goalsFailedNow)
   {
      this.goalsFailedNow = goalsFailedNow;
   }

   public int getGoalsAchievedPrevious()
   {
      return goalsAchievedPrevious;
   }

   public void setGoalsAchievedPrevious(int goalsAchievedPrevious)
   {
      this.goalsAchievedPrevious = goalsAchievedPrevious;
   }

   public int getGoalsFailedPrevious()
   {
      return goalsFailedPrevious;
   }

   public void setGoalsFailedPrevious(int goalsFailedPrevious)
   {
      this.goalsFailedPrevious = goalsFailedPrevious;
   }

   @Override
   public void prepareResponse() throws Exception
   {
      int[] dataValues = new int[4];
      dataValues[0] = getGoalsAchievedNow();
      dataValues[1] = getGoalsFailedNow();
      dataValues[2] = getGoalsAchievedPrevious();
      dataValues[3] = getGoalsFailedPrevious();
      this.put("name", getName());
      this.put("data", dataValues);
   }

}
