package com.intelli.ezed.rules;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.AddScratchCardToUserData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.PaymentIdData;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.utils.GenericTicketKey;
import com.intelli.ezed.utils.TicketGenerator;

public class PreparePaymentId extends SingletonRule
{
   private String connName = null;
   private String appId = null;
   private static TicketGenerator ticketGenerator = null;
   private static GenericTicketKey genericTicketKey = null;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public PreparePaymentId(String ruleName, String connName, String appId)
   {
      super(ruleName);
      this.connName = connName;
      this.appId = appId;
      long initialValue = getStartUpTransactionId(appId);

      PreparePaymentId.ticketGenerator = TicketGenerator.getInstance(9999999999L, 0, initialValue);
      PreparePaymentId.genericTicketKey = new GenericTicketKey(0, false, 10);
   }

   private long getStartUpTransactionId(String appId)
   {
      Connection connection = DbConnectionManager.getConnectionByName(connName);
      PreparedStatement preparedStatement = DbConnectionManager.getPreparedStatement(connName, connection, "GetMaxPayId");
      try
      {
         preparedStatement.setString(1, appId);
         ResultSet resultSet = preparedStatement.executeQuery();
         if (resultSet.next())
         {
            long value = Long.parseLong(resultSet.getString(1));
            resultSet.close();
            resultSet = null;

            if (value == 0)
            {
               value = (System.currentTimeMillis() / 10000);
               if (logger.isInfoEnabled())
               {
                  logger.info("Generated new initial transaction id ::" + value);
               }
               return value;
            }

            if (logger.isInfoEnabled())
            {
               logger.info("New initial transaction id received from db");
               logger.info(Long.toString(value));
            }
            return value + 1;
         }
      }
      catch (Exception e)
      {
         logger.error("Exception occured :: ", e);
      }
      finally
      {
         DbConnectionManager.releaseConnection(connName, connection);
      }
      return 0;
   }


   public RuleDecisionKey executeRule(FlowContext flowContext) {

		int transactionType = (Integer) flowContext
				.getFromContext(EzedKeyConstants.PAYMENT_ID_FOR_TRANSACTION_TYPE);
		
		if (transactionType == EzedKeyConstants.PAYMENT_ID_FOR_TRANSACTION_TYPE_ONLINE_PAYMENT) {
			System.out.println("In online payment type .. ");
			Response response = (Response) flowContext
					.getFromContext(EzedKeyConstants.EZED_RESPONSE);
			String ticket = ticketGenerator.getNextEdgeTicketFor(genericTicketKey);
			logger.info("Payment Id::" + appId + ticket);
			PaymentIdData data = new PaymentIdData();
			data.setAppId(appId);
			data.setPaymentId(ticket);
			response.setResponseObject(data);
			flowContext
					.putIntoContext(EzedKeyConstants.CART_PAYMENT_DATA, data);
		}
		else
			if(transactionType == EzedKeyConstants.PAYMENT_ID_FOR_TRANSACTION_TYPE_SCRATCHCARD)
			{
				
				System.out.println("In buying from scratch card type ......");
				String ticket = ticketGenerator.getNextEdgeTicketFor(genericTicketKey);
				logger.info("Payment Id::" + appId + ticket);
				AddScratchCardToUserData data = (AddScratchCardToUserData) flowContext
						.getFromContext(EzedKeyConstants.ADD_COURSE_USING_SCRATCHCARD_DETAILS);

				System.out.println("In Class : " + this.getClass().getName());
				System.out.print("USERID : " + data.getUserid() + "  Sessionid : "
						+ data.getSessionid());

				data.setAppId(appId);
				data.setPaymentId(ticket);
				flowContext.putIntoContext(
						EzedKeyConstants.ADD_COURSE_USING_SCRATCHCARD_DETAILS, data);
			}
		return RuleDecisionKey.ON_SUCCESS;
	}
}
