package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.CartDetails;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.utils.PaymentLockManager;

public class CartStatusUpdater extends SingletonRule
{
   private String dbConnName;
   private String processedCartStatus;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public CartStatusUpdater(String ruleName, String dbConnName, String processedCartStatus)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
      this.processedCartStatus = processedCartStatus;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      CartDetails cartObject = (CartDetails) flowContext.getFromContext(EzedKeyConstants.CART_DETAILS);
      String userId = (String) flowContext.getFromContext(EzedKeyConstants.USER_ID);

      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdateProcessedCartStatus");
         ps.setString(1, processedCartStatus);
         ps.setString(2, userId);
         ps.setString(3, cartObject.getAppId());
         ps.setString(4, cartObject.getPaymentId());
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows updated in database while updating status of processed cart for payment id " + cartObject.getAppId() + cartObject.getPaymentId());
         }
         PaymentLockManager.removeFromList(cartObject.getAppId() + cartObject.getPaymentId());
         return RuleDecisionKey.ON_SUCCESS;
      }
      catch (SQLException e)
      {
         logger.error("Error while updating processed cart status for payment id " + cartObject.getAppId() + cartObject.getPaymentId(), e);
         PaymentLockManager.removeFromList(cartObject.getAppId() + cartObject.getPaymentId());
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while updating processed cart status for payment id " + cartObject.getAppId() + cartObject.getPaymentId(), e);
         PaymentLockManager.removeFromList(cartObject.getAppId() + cartObject.getPaymentId());
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
}
