package com.intelli.ezed.checks;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.TestDetails;
import com.intelli.ezed.data.UserProfile;

public class TestTimeEndChecker extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   public TestTimeEndChecker(String ruleName)
   {
      super(ruleName);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      TestDetails testDetails = (TestDetails) flowContext.getFromContext(TestDetails.TEST_DETAILS);
      UserProfile userProfile = (UserProfile) flowContext.getFromContext(EzedKeyConstants.USER_DETAILS);

      if (testDetails.isTimeEnd())
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Test time end. " + testDetails.getTestid() + " " + userProfile.getUserid());
         }
         return RuleDecisionKey.ON_FAILURE;
      }

      return RuleDecisionKey.ON_SUCCESS;
   }

}
