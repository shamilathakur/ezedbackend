package com.intelli.ezed.data;

import java.util.ArrayList;

public class EnrolledCoursePath extends ResponseObject {

	private String courseId; 
	
	
	private ArrayList<EnrolledCourseChapter > coursepath;
	

	/**
	 * @return the courseId
	 */
	public String getCourseId() {
		return courseId;
	}

	/**
	 * @param courseId the courseId to set
	 */
	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	
	@Override
	public void prepareResponse() throws Exception {
		
		EnrolledCourseChapter [] chapterLists = new EnrolledCourseChapter[getCoursepath().size()];
		
		for(int i=0;i<chapterLists.length;i++){
			
			chapterLists[i] = getCoursepath().get(i);
			
		
		}
		this.put("coursepath", chapterLists);
	      this.put("courseid", getCourseId());
	     
		
	}

	/**
	 * @return the coursepath
	 */
	public ArrayList<EnrolledCourseChapter> getCoursepath() {
		return coursepath;
	}

	/**
	 * @param coursepath the coursepath to set
	 */
	public void setCoursepath(ArrayList<EnrolledCourseChapter > coursepath) {
		this.coursepath = coursepath;
	}

	
}
