package com.intelli.ezed.data;

public class EzedTransactionCodes {
	public static final int SUCCESS = 0;

	public static final int INVALID_MESSAGE_KEYWORD = 201;
	public static final int INVALID_MESSAGE_PARAMETER = 202;
	public static final int INVALID_USERID = 203;
	public static final int INVALID_PASSWORD = 204;
	public static final int INVALID_CHKFLAG = 205;
	public static final int USERID_NOT_REGISTERED = 206;
	public static final int PAYMENT_NOT_FOUND = 207;
	public static final int INVALID_CHAPTER_ID = 208;
	public static final int INVALID_OPRATION = 209;
	public static final int WRONG_COUNTRY = 210;
	public static final int WRONG_STATE = 211;
	public static final int WRONG_CITY = 212;
	public static final int POST_EVENT_ALREADY_RECORDED = 213;
	public static final int DISCOUNT_INVALID = 214;
	public static final int CART_NOT_VALID = 215;
	public static final int USER_NOT_VERIFIED = 216;
	public static final int USER_NOT_FORGOTTEN_PASSWORD = 217;
	public static final int USERID_ALREADY_PRESENT = 218;
	public static final int FRIEND_ALREADY_INVITED = 219;
	public static final int USER_BLOCKED = 220;
	public static final int COURSE_CANNOT_BE_PUBLISHED = 221;
	public static final int CANNOT_CHANGE_LOGGEDIN_USER_STATUS = 222;
	public static final int DUPLICATE_CITY = 223;
	public static final int DUPLICATE_COUNTRY = 224;
	public static final int DUPLICATE_STATE = 225;
	public static final int WEBSITE_NAME_REPEATED = 226;
	public static final int PH_NO_REPEATED = 227;
	public static final int INVALID_QUESTION_ID = 228;
	public static final int INVALID_COURSE_ID=229;

	public static final int TEST_END_REACHED = 401;
	public static final int INVALID_TEST_ID = 402;
	public static final int NO_QUESTIONS_IN_BANK = 403;

	public static final int DB_EXCEPTION_OCCURRED = 301;
	public static final int INTERNAL_ERROR = 302;
	public static final int SESSION_ERROR_OCCURRED = 303;

	public static final int PROCESSING_REQUEST = 399;

	public static final int DISCOUNT_NOT_YET_STARTED = 250;
	public static final int DISCOUNT_EXPIRED = 251;

	// added by kunal on 16/02/2015
	public static final int INVALID_SCRATCH_CARD_ID = 252;
	public static final int SCRATCH_CARD_ALREADY_USED = 253;
	public static final int SCRATCH_CARD_INACTIVE = 254;
	public static final int COURSE_ALREADY_OWNED = 255;
	public static final int SCRATCH_CARD_EXPIRED = 256;
	//added by saroj on 11th July 2015
    public static final int UNIVERSITY_ALREADY_ADDED = 257;
    public static final int COLLEGE_ALREADY_ADDED = 258;
 
}
