package com.intelli.ezed.rules;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;

import com.intelli.ezed.data.CartData;
import com.intelli.ezed.data.CartDetails;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.PaymentIdData;

public class CartObjectCreator extends SingletonRule
{
   public CartObjectCreator(String ruleName)
   {
      super(ruleName);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      CartData[] cart = (CartData[]) flowContext.getFromContext(EzedKeyConstants.CART_DETAILS);
      PaymentIdData payData = (PaymentIdData) flowContext.getFromContext(EzedKeyConstants.CART_PAYMENT_DATA);
      CartDetails cartDetails = new CartDetails();
      cartDetails.setAppId(payData.getAppId());
      cartDetails.setPaymentId(payData.getPaymentId());
      cartDetails.setCartDatas(cart);
      flowContext.putIntoContext(EzedKeyConstants.CART_DETAILS, cartDetails);
      return RuleDecisionKey.ON_SUCCESS;
   }

}
