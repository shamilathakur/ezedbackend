package com.intelli.ezed.rules;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;

import com.intelli.ezed.analytics.data.GenericChartData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.Response;

public class UserVerifFlagResponseConstructor extends SingletonRule
{

   public UserVerifFlagResponseConstructor(String ruleName)
   {
      super(ruleName);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      GenericChartData data = new GenericChartData();
      data.getText().add("showprofilepage");
      data.getValues().add(new Integer(0));
      response.setResponseObject(data);
      return RuleDecisionKey.ON_SUCCESS;
   }

}
