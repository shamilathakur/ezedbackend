package com.intelli.ezed.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicLong;

import org.sabre.db.DbConnectionManager;

import com.a.helper.data.DataFiller;

public class PrepareChallengeIdBackend
{
   //keep calling getnextid() to get new note ID's. Note ID's will be
   //in the format NOT1, NOT2, NOT3, and so on...
   private AtomicLong atomicLong = null;
   private String dbConnName;
   private String startString;

   public PrepareChallengeIdBackend(String startString, String dbConnName)
   {
      this.dbConnName = dbConnName;
      this.startString = startString;
      long initialValue = getStartUpTransactionId();
      if (initialValue == 0)
      {
         initialValue = (System.currentTimeMillis() / 10000000);
      }
      atomicLong = new AtomicLong(initialValue);
   }

   private long getStartUpTransactionId()
   {
      //change how you get your connection here.
      Connection connection = getDbConnection();
      if (connection == null)
      {
         return 0;
      }

      //change how you get your prepared statement here.
      PreparedStatement preparedStatement = getStatement(connection);
      if (preparedStatement == null)
      {
         return 0;
      }
      try
      {
         preparedStatement.setString(1, startString + "%");
         ResultSet resultSet = preparedStatement.executeQuery();
         if (resultSet.next())
         {
            String valueString = resultSet.getString(1);
            valueString = valueString.substring(startString.length());
            long value = Long.parseLong(valueString);
            resultSet.close();
            resultSet = null;

            if (value == 0)
            {
               value = 1;
               return value;
            }

            return value + 1;
         }
         return 1;
      }
      catch (Exception e)
      {
         return 0;
      }
      finally
      {
         //if there is any code to release the prepared statements
         //or connections back to pool, write them here.
         try
         {
            preparedStatement.close();
         }
         catch (SQLException e)
         {
         }
         try
         {
            connection.close();
         }
         catch (SQLException e)
         {
         }
      }
   }


   public String getNextId()
   {
      return startString + DataFiller.getLeftZeroPadding(Long.toString(this.atomicLong.incrementAndGet()), 16);
   }

   private Connection getDbConnection()
   {
      try
      {
         return DbConnectionManager.getConnectionByName(dbConnName);
      }
      catch (Exception e)
      {
         return null;
      }
   }

   private PreparedStatement getStatement(Connection conn)
   {
      try
      {
         return DbConnectionManager.getPreparedStatement(dbConnName, conn, "SelectMaxChallengeId");
      }
      catch (Exception e)
      {
         return null;
      }
   }
}
