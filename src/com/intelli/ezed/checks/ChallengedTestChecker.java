package com.intelli.ezed.checks;

import java.util.HashMap;

import org.sabre.exceptions.InputProcessorException;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.LinkedProcessorManager;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.TestDetails;

public class ChallengedTestChecker extends SingletonRule
{

   public Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public ChallengedTestChecker(String ruleName)
   {
      super(ruleName);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      TestDetails testDetails = (TestDetails) flowContext.getFromContext(TestDetails.TEST_DETAILS);
      String testSessionId = (String) flowContext.getFromContext(EzedKeyConstants.TEST_SESSION_ID);

      String challengeId = testDetails.getChallengeId();
      
      if (challengeId == null)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Test is not challenged.");
         }
         return RuleDecisionKey.ON_SUCCESS;
      }

      HashMap<Object, Object> data = new HashMap<Object, Object>();
      data.put(EzedKeyConstants.TEST_SESSION_ID, testSessionId);
      data.put(EzedKeyConstants.CHALLENGE_ID, challengeId);

      try
      {
         LinkedProcessorManager.addTask("ChallengeHandler", data);
      }
      catch (InputProcessorException e)
      {
         logger.error("Error occored while adding task to linked processor : ChallengeHandler.", e);
      }
      catch (Exception e)
      {
         logger.error("Error occored while adding task to linked processor : ChallengeHandler.", e);
      }
      
      
      return RuleDecisionKey.ON_SUCCESS;
   }
}
