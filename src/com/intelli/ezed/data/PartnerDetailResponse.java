package com.intelli.ezed.data;

public class PartnerDetailResponse extends ResponseObject {

	private String sessionId;
	private PartnerData partnerData;

	public PartnerData getPartnerData() {
		return partnerData;
	}

	public void setPartnerData(PartnerData partnerData) {
		this.partnerData = partnerData;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	@Override
	public void prepareResponse() throws Exception {
		this.put("sessionid", getSessionId());
		getPartnerData().prepareResponse();
		this.put("profile", getPartnerData());
	}
}
