package com.intelli.ezed.checks;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.db.EzedRequestData;

public class ChangeMpinDataChecker extends SingletonRule
{

   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public ChangeMpinDataChecker(String ruleName)
   {
      super(ruleName);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      String userid = messageData.getParameter("email");
      String pwd = messageData.getParameter("password");
      String newPwd = messageData.getParameter("newpassword");

      if (userid == null || userid.equals("") || userid.equals(" "))
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Email address not provided.");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Please enter correct email id.");
         return RuleDecisionKey.ON_FAILURE;
      }


      if (pwd == null || pwd.equals("") || pwd.equals(" "))
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Email password not provided.");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Please enter password id.");
         return RuleDecisionKey.ON_FAILURE;
      }
      
      if (newPwd == null || newPwd.equals("") || newPwd.equals(" "))
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Email new password not provided.");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Please enter new password correctly.");
         return RuleDecisionKey.ON_FAILURE;
      }


      //Creating this new Data object so that we can keep incoming request data in this object and compare it with the fetched userprofile data
      EzedRequestData incomingRequestData = new EzedRequestData();
      incomingRequestData.setUserid(userid);
      incomingRequestData.setPassword(pwd);
      incomingRequestData.setNewPassword(newPwd);
      flowContext.putIntoContext(EzedRequestData.EZED_REQUEST_DATA, incomingRequestData);
      //Setting user id so that we can fetch user profile later
      flowContext.putIntoContext(EzedKeyConstants.USER_ID, userid);

      if (logger.isDebugEnabled())
      {
         logger.debug("Entered email id is : " + userid + " and password is : " + pwd);
      }
      return RuleDecisionKey.ON_SUCCESS;
   }

}
