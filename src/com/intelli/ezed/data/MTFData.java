package com.intelli.ezed.data;

import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.collections.BidiMap;
import org.apache.commons.collections.bidimap.DualHashBidiMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.utils.StringSplitter;
import com.intelli.ezed.utils.Randomizer;

public class MTFData extends TestQuestion
{
   private static Map<Object, Object> numberAlphaMap = new HashMap<Object, Object>();
   private static Map<Object, Object> alphaNumberMap = new HashMap<Object, Object>();
   static
   {
      numberAlphaMap.put(1, "A");
      numberAlphaMap.put(2, "B");
      numberAlphaMap.put(3, "C");
      numberAlphaMap.put(4, "D");
      numberAlphaMap.put(5, "E");
      numberAlphaMap.put(6, "F");

      alphaNumberMap.put("A", 1);
      alphaNumberMap.put("B", 2);
      alphaNumberMap.put("C", 3);
      alphaNumberMap.put("D", 4);
      alphaNumberMap.put("E", 5);
      alphaNumberMap.put("F", 6);

      alphaNumberMap.put("a", 1);
      alphaNumberMap.put("b", 2);
      alphaNumberMap.put("c", 3);
      alphaNumberMap.put("d", 4);
      alphaNumberMap.put("e", 5);
      alphaNumberMap.put("f", 6);

      alphaNumberMap.put("1", 1);
      alphaNumberMap.put("2", 2);
      alphaNumberMap.put("3", 3);
      alphaNumberMap.put("4", 4);
      alphaNumberMap.put("5", 5);
      alphaNumberMap.put("6", 6);
   }
   private QuestionContent question;
   private ArrayList<QuestionContent> questions = new ArrayList<QuestionContent>();
   private ArrayList<QuestionContent> answers = new ArrayList<QuestionContent>();
   private BidiMap correctAnswerMap = new DualHashBidiMap();
   private String correctAnswer;
   private static Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public String getCorrectAnswer()
   {
      return correctAnswer;
   }
   public int setCorrectAnswer(String correctAnswer) throws Exception
   {
      try
      {
         if (correctAnswer != null)
         {
            this.correctAnswer = correctAnswer.replaceAll(" ", "").toUpperCase();
            String[] oneAnswer = StringSplitter.splitString(this.correctAnswer, ",");
            if (oneAnswer.length != questions.size())
            {
               if (logger.isDebugEnabled())
               {
                  logger.debug("No of modules of correct ans does not match no of questions");
               }
               return 1;
            }
            if (oneAnswer.length != answers.size())
            {
               if (logger.isDebugEnabled())
               {
                  logger.debug("No of modules of correct ans does not match no of answers");
               }
               return 1;
            }
            Integer lhsOption = 0;
            Integer rhsOption = 0;
            for (String eachAnswer : oneAnswer)
            {
               String[] oneOption = StringSplitter.splitString(eachAnswer, "-");
               lhsOption = Integer.parseInt(oneOption[0]);
               if (lhsOption > questions.size())
               {
                  if (logger.isDebugEnabled())
                  {
                     logger.debug("LHS option " + lhsOption + " is greater than a valid question");
                  }
                  return 1;
               }
               rhsOption = (Integer) alphaNumberMap.get(oneOption[1]);
               if (rhsOption > questions.size())
               {
                  if (logger.isDebugEnabled())
                  {
                     logger.debug("RHS option " + rhsOption + " is greater than a valid question");
                  }
                  return 1;
               }
               getCorrectAnswerMap().put(lhsOption, rhsOption);
            }
         }
         return 0;
      }
      catch (Exception e)
      {
         return 1;
      }
   }
   public ArrayList<QuestionContent> getAnswers()
   {
      return answers;
   }
   public void setAnswers(ArrayList<QuestionContent> answers)
   {
      this.answers = answers;
   }
   public void addAnswers(QuestionContent answer)
   {
      getAnswers().add(answer);
   }

   public QuestionContent getQuestion()
   {
      return question;
   }
   public void setQuestion(QuestionContent question)
   {
      this.question = question;
   }
   public ArrayList<QuestionContent> getQuestions()
   {
      return questions;
   }
   public void setQuestions(ArrayList<QuestionContent> questions)
   {
      this.questions = questions;
   }
   public void addQuestions(QuestionContent question)
   {
      getQuestions().add(question);
   }
   public BidiMap getCorrectAnswerMap()
   {
      return correctAnswerMap;
   }
   public void setCorrectAnswerMap(BidiMap correctAnswerMap)
   {
      this.correctAnswerMap = correctAnswerMap;
   }
   public void prepareForRandomization()
   {
      if (getCorrectAnswer() == null || getCorrectAnswer().trim().compareTo("") == 0)
      {
         return;
      }
      String[] oneAnswer = StringSplitter.splitString(correctAnswer, ",");
      for (String eachAnswer : oneAnswer)
      {
         String[] oneOption = StringSplitter.splitString(eachAnswer, "-");
         getCorrectAnswerMap().put(Integer.parseInt(oneOption[0]), alphaNumberMap.get(oneOption[1]));
      }
      ArrayList<QuestionContent> orderedAnswer = new ArrayList<QuestionContent>();
      for (int i = 1; i <= getCorrectAnswerMap().size(); i++)
      {
         orderedAnswer.add(getAnswers().get(((Integer) getCorrectAnswerMap().get(i)) - 1));
      }
      setAnswers(orderedAnswer);
      setCorrectAnswerMap(new DualHashBidiMap());
   }
   public ArrayList<TestQuestion> getPermutedQuestions(int noOfRandomQuestions, Randomizer randomizer)
   {
      prepareForRandomization();
      ArrayList<String> questionCombo = randomizer.getSpacedPermutations(noOfRandomQuestions);
      ArrayList<String> answerCombo = randomizer.getSpacedPermutations(noOfRandomQuestions);
      String oneQuestionCombo;
      String oneAnswerCombo;
      int questionIndex;
      int answerIndex;
      MTFData nextData;
      ArrayList<TestQuestion> permutedQuestions = new ArrayList<TestQuestion>();
      for (int i = 0; i < questionCombo.size(); i++)
      {
         oneQuestionCombo = questionCombo.get(i);
         oneAnswerCombo = answerCombo.get(i);
         nextData = new MTFData();
         for (int j = 0; j < oneQuestionCombo.length(); j++)
         {
            nextData.setQuestion(this.getQuestion());
            questionIndex = Integer.parseInt(Character.toString(oneQuestionCombo.charAt(j)));
            answerIndex = Integer.parseInt(Character.toString(oneAnswerCombo.charAt(j)));

            nextData.getQuestions().add(this.getQuestions().get(questionIndex - 1));
            nextData.getAnswers().add(this.getAnswers().get(answerIndex - 1));

            nextData.getCorrectAnswerMap().put(j + 1, oneAnswerCombo.indexOf(Integer.toString(questionIndex)) + 1);
            nextData.setExplanation(this.getExplanation());
         }
         permutedQuestions.add(nextData);
      }
      Collections.shuffle(permutedQuestions);

      return permutedQuestions;
   }
   @Override
   public String toString()
   {
      StringBuffer sb = new StringBuffer();
      if (getQuestion().getType() == QuestionContent.STRING_TYPE)
      {
         sb.append("Question : ").append(getQuestion().getContentString()).append("\n");
      }
      else
      {
         sb.append("Question : ").append(getQuestion().getContentLoc()).append("\n");
      }

      QuestionContent questContent = null;
      QuestionContent ansContent = null;
      for (int i = 0; i < getAnswers().size(); i++)
      {
         sb.append(i + 1).append(" ");
         questContent = getQuestions().get(i);
         if (questContent.getType() == QuestionContent.STRING_TYPE)
         {
            sb.append(questContent.getContentString()).append("\t");
         }
         else
         {
            sb.append(questContent.getContentLoc()).append("\t");
         }

         sb.append(i + 1).append(" ");
         ansContent = getAnswers().get(i);
         if (ansContent.getType() == QuestionContent.STRING_TYPE)
         {
            sb.append(ansContent.getContentString()).append("\n");
         }
         else
         {
            sb.append(ansContent.getContentLoc()).append("\n");
         }
      }

      sb.append("Correct Answer : ");
      Iterator<?> correctAnswerKeys = getCorrectAnswerMap().keySet().iterator();
      Object currentKey = null;
      while (correctAnswerKeys.hasNext())
      {
         currentKey = correctAnswerKeys.next();
         sb.append(currentKey).append(" - ").append(getCorrectAnswerMap().get(currentKey)).append(",");
      }
      return sb.toString();
   }

   public static void main(String[] args)
   {
      //      Map<Integer, Integer> testMap = new HashMap<Integer, Integer>();
      //      testMap.put(1, 1);
      //      testMap.put(2, 4);
      //      testMap.put(3, 9);
      //      BidiMap map = new DualHashBidiMap();
      //      map.put(1, 1);
      //      map.put(3, 9);
      //      map.put(2, 9);
      //      System.out.println(map.getKey(9));
      //      System.out.println(map.get(3));
      MTFData mtfData = new MTFData();
      QuestionContent question = new QuestionContent();
      question.setType(QuestionContent.STRING_TYPE);
      question.setContentString("Match state/UT with capital");
      mtfData.setQuestion(question);

      QuestionContent answer = new QuestionContent();
      answer.setType(QuestionContent.STRING_TYPE);
      answer.setContentString("Delhi");
      mtfData.getAnswers().add(answer);

      answer = new QuestionContent();
      answer.setType(QuestionContent.STRING_TYPE);
      answer.setContentString("Maharashtra");
      mtfData.getAnswers().add(answer);

      answer = new QuestionContent();
      answer.setType(QuestionContent.STRING_TYPE);
      answer.setContentString("Uttar Pradesh");
      mtfData.getAnswers().add(answer);

      answer = new QuestionContent();
      answer.setType(QuestionContent.STRING_TYPE);
      answer.setContentString("West Bengal");
      mtfData.getAnswers().add(answer);

      answer = new QuestionContent();
      answer.setType(QuestionContent.STRING_TYPE);
      answer.setContentString("Karnataka");
      mtfData.getAnswers().add(answer);

      answer = new QuestionContent();
      answer.setType(QuestionContent.STRING_TYPE);
      answer.setContentString("Delhi");
      mtfData.getQuestions().add(answer);

      answer = new QuestionContent();
      answer.setType(QuestionContent.STRING_TYPE);
      answer.setContentString("Mumbai");
      mtfData.getQuestions().add(answer);

      answer = new QuestionContent();
      answer.setType(QuestionContent.STRING_TYPE);
      answer.setContentString("Lucknow");
      mtfData.getQuestions().add(answer);

      answer = new QuestionContent();
      answer.setType(QuestionContent.STRING_TYPE);
      answer.setContentString("Kolkata");
      mtfData.getQuestions().add(answer);

      answer = new QuestionContent();
      answer.setType(QuestionContent.STRING_TYPE);
      answer.setContentString("Bangalore");
      mtfData.getQuestions().add(answer);

      System.out.println("Original question : \n");
      System.out.println(mtfData.toString());
      ArrayList<TestQuestion> randomPermutedQuestions = mtfData.getPermutedQuestions(8, new Randomizer(5));
      for (int i = 0; i < randomPermutedQuestions.size(); i++)
      {
         System.out.println("\n\nQuestion " + (i + 1) + " \n");
         System.out.println(randomPermutedQuestions.get(i).toString());
      }
   }
   @Override
   public void persistQuestion(PreparedStatement ps, int startPoint) throws Exception
   {
      ps.setInt(startPoint + 1, getQuestion().getType());
      StringBuffer mtfQuestion = new StringBuffer();
      if (getQuestion().getType() == QuestionContent.STRING_TYPE)
      {
         mtfQuestion.append(getQuestion().getContentString()).append(";");
      }
      else
      {
         mtfQuestion.append(getQuestion().getContentLoc()).append(";");
      }
      mtfQuestion.append(getQuestions().get(0).getType()).append(";");
      if (getQuestions().get(0).getType() == QuestionContent.STRING_TYPE)
      {
         for (int i = 0; i < getQuestions().size(); i++)
         {
            mtfQuestion.append(getQuestions().get(i).getContentString()).append(";");
         }
      }
      else
      {
         for (int i = 0; i < getQuestions().size(); i++)
         {
            mtfQuestion.append(getQuestions().get(i).getContentLoc()).append(";");
         }
      }

      mtfQuestion.deleteCharAt(mtfQuestion.length() - 1);

      if (getQuestion().getType() == QuestionContent.STRING_TYPE)
      {
         ps.setString(startPoint + 2, mtfQuestion.toString());
         ps.setString(startPoint + 3, null);
      }
      else
      {
         ps.setString(startPoint + 2, null);
         ps.setString(startPoint + 3, mtfQuestion.toString());
      }

      int j = 0;
      int i = 0;
      for (; i < getAnswers().size(); i++, j += 3)
      {
         ps.setInt(startPoint + 4 + j, getAnswers().get(i).getType());
         if (getAnswers().get(i).getType() == QuestionContent.STRING_TYPE)
         {
            ps.setString(startPoint + 5 + j, getAnswers().get(i).getContentString());
            ps.setString(startPoint + 6 + j, null);
         }
         else
         {
            ps.setString(startPoint + 5 + j, null);
            ps.setString(startPoint + 6 + j, getAnswers().get(i).getContentLoc());
         }
      }

      for (; i < 6; i++, j += 3)
      {
         ps.setInt(startPoint + 4 + j, 0);
         ps.setString(startPoint + j + 5, null);
         ps.setString(startPoint + j + 6, null);
      }

      StringBuffer correctAnswer = new StringBuffer();
      Iterator<?> correctAnswerKeys = getCorrectAnswerMap().keySet().iterator();
      Object currentKey = null;
      while (correctAnswerKeys.hasNext())
      {
         currentKey = correctAnswerKeys.next();
         correctAnswer.append(currentKey).append(" - ").append(numberAlphaMap.get(getCorrectAnswerMap().get(currentKey))).append(";");
      }
      correctAnswer.deleteCharAt(correctAnswer.length() - 1);
      ps.setString(startPoint + j + 4, correctAnswer.toString());
      ps.setInt(startPoint + j + 5, TestQuestionData.MTF_QUESTION_TYPE);
      ps.setInt(startPoint + j + 6, getExplanation().getType());
      ps.setString(startPoint + j + 7, getExplanation().getContentString());
      ps.setString(startPoint + j + 8, getExplanation().getContentLoc());
   }

   @Override
   public int getNoOfAnswers()
   {
      return getQuestions().size();
   }
   @Override
   public void persistForBulkUpload(PreparedStatement ps, int startPoint) throws Exception
   {
      ps.setInt(startPoint + 1, getQuestion().getType());
      StringBuffer mtfQuestion = new StringBuffer();
      if (getQuestion().getType() == QuestionContent.STRING_TYPE)
      {
         mtfQuestion.append(getQuestion().getContentString()).append(";");
      }
      else
      {
         mtfQuestion.append(getQuestion().getContentLoc()).append(";");
      }
      mtfQuestion.append(getQuestions().get(0).getType()).append(";");
      if (getQuestions().get(0).getType() == QuestionContent.STRING_TYPE)
      {
         for (int i = 0; i < getQuestions().size(); i++)
         {
            mtfQuestion.append(getQuestions().get(i).getContentString()).append(";");
         }
      }
      else
      {
         for (int i = 0; i < getQuestions().size(); i++)
         {
            mtfQuestion.append(getQuestions().get(i).getContentLoc()).append(";");
         }
      }

      mtfQuestion.deleteCharAt(mtfQuestion.length() - 1);

      if (getQuestion().getType() == QuestionContent.STRING_TYPE)
      {
         ps.setString(startPoint + 2, mtfQuestion.toString());
         ps.setString(startPoint + 3, null);
      }
      else
      {
         ps.setString(startPoint + 2, null);
         ps.setString(startPoint + 3, mtfQuestion.toString());
      }

      int j = 0;
      int i = 0;
      for (; i < getAnswers().size(); i++, j += 3)
      {
         ps.setInt(startPoint + 4 + j, getAnswers().get(i).getType());
         if (getAnswers().get(i).getType() == QuestionContent.STRING_TYPE)
         {
            ps.setString(startPoint + 5 + j, getAnswers().get(i).getContentString());
            ps.setString(startPoint + 6 + j, null);
         }
         else
         {
            ps.setString(startPoint + 5 + j, null);
            ps.setString(startPoint + 6 + j, getAnswers().get(i).getContentLoc());
         }
      }

      for (; i < 6; i++, j += 3)
      {
         ps.setInt(startPoint + 4 + j, 0);
         ps.setString(startPoint + j + 5, null);
         ps.setString(startPoint + j + 6, null);
      }
      ps.setString(startPoint + j + 4, getCorrectAnswer());
      ps.setInt(startPoint + j + 5, TestQuestionData.MTF_QUESTION_TYPE);
   }

   @Override
   public void writeToFile(FileOutputStream fos) throws IOException
   {
      StringBuffer sb = new StringBuffer();
      sb.append("<question type=\"MTF\">\n");
      if (getQuestionid() != null)
      {
         sb.append("<id>");
         sb.append(getQuestionid());
         sb.append("</id>\n");
      }
      sb.append("<questionstatement>\n");
      sb.append("<type>");
      sb.append(getQuestion().getType());
      sb.append("</type>\n");
      if (getQuestion().getType() == QuestionContent.STRING_TYPE)
      {
         sb.append("<string>");
         sb.append(getQuestion().getContentString());
         sb.append("</string>\n");
      }
      else
      {
         sb.append("<content>");
         sb.append(getQuestion().getContentLoc());
         sb.append("</content>\n");
      }
      sb.append("</questionstatement>\n");
      for (int i = 0; i < getQuestions().size(); i++)
      {
         sb.append("<question" + (i + 1) + "statement>");
         sb.append("<type>");
         sb.append(getQuestions().get(i).getType());
         sb.append("</type>\n");
         if (getQuestions().get(i).getType() == QuestionContent.STRING_TYPE)
         {
            sb.append("<string>");
            sb.append(getQuestions().get(i).getContentString());
            sb.append("</string>\n");
         }
         else
         {
            sb.append("<content>");
            sb.append(getQuestions().get(i).getContentLoc());
            sb.append("</content>\n");
         }
         sb.append("</question" + (i + 1) + "statement>\n");
      }
      for (int i = 0; i < getAnswers().size(); i++)
      {
         sb.append("<answer" + (i + 1) + "statement>");
         sb.append("<type>");
         sb.append(getAnswers().get(i).getType());
         sb.append("</type>\n");
         if (getAnswers().get(i).getType() == QuestionContent.STRING_TYPE)
         {
            sb.append("<string>");
            sb.append(getAnswers().get(i).getContentString());
            sb.append("</string>\n");
         }
         else
         {
            sb.append("<content>");
            sb.append(getAnswers().get(i).getContentLoc());
            sb.append("</content>\n");
         }
         sb.append("</answer" + (i + 1) + "statement>\n");
      }
      if (getDifficultyLevel() != 0)
      {
         sb.append("<difficultylevel>");
         sb.append(getDifficultyLevel());
         sb.append("</difficultylevel>\n");
      }
      sb.append("<correctanswer>");
      sb.append(getCorrectAnswer());
      sb.append("</correctanswer>\n");
      sb.append("<explanation>\n");
      sb.append("<type>");
      sb.append(getExplanation().getType());
      sb.append("</type>\n");
      if (getExplanation().getType() == QuestionContent.STRING_TYPE)
      {
         sb.append("<string>");
         sb.append(getExplanation().getContentString());
         sb.append("</string>\n");
      }
      else
      {
         sb.append("<content>");
         sb.append(getExplanation().getContentLoc());
         sb.append("</content>\n");
      }
      sb.append("</explanation>\n");
      sb.append("</question>");
      fos.write(sb.toString().getBytes());
   }

   @Override
   public void replaceContentIds(Map<String, String> contentDirIdMap) throws Exception
   {
      QuestionContent questionContent = getQuestion();
      if (questionContent.getType() == QuestionContent.CONTENT_TYPE)
      {
         String questionContentTemp = contentDirIdMap.get(questionContent.getContentLoc());
         if (questionContentTemp == null)
         {
            throw new Exception("Question content could not be found");
         }
         questionContent.setContentLoc(questionContentTemp);
      }
      String lhsContentTemp = null;
      for (int i = 0; i < getQuestions().size(); i++)
      {
         questionContent = getQuestions().get(i);
         if (questionContent.getType() == QuestionContent.CONTENT_TYPE)
         {
            lhsContentTemp = contentDirIdMap.get(questionContent.getContentLoc());
            if (lhsContentTemp == null)
            {
               throw new Exception("LHS content could not be found");
            }
            questionContent.setContentLoc(lhsContentTemp);
         }
      }
      String answerContentTemp = null;
      for (int i = 0; i < getAnswers().size(); i++)
      {
         questionContent = getAnswers().get(i);
         if (questionContent.getType() == QuestionContent.CONTENT_TYPE)
         {
            answerContentTemp = contentDirIdMap.get(questionContent.getContentLoc());
            if (answerContentTemp == null)
            {
               throw new Exception("Answer content could not be found");
            }
            questionContent.setContentLoc(answerContentTemp);
         }
      }
      questionContent = getExplanation();
      if (questionContent.getType() == QuestionContent.CONTENT_TYPE)
      {
         questionContent.setContentLoc(contentDirIdMap.get(questionContent.getContentLoc()));
      }
   }
}
