package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.PaymentData;
import com.intelli.ezed.data.Response;

public class PaymentDetailsUpdater extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private Logger reconLogger = LoggerFactory.getLogger(EzedLoggerName.RECON_LOG);
   private String dbConnName;

   public PaymentDetailsUpdater(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      PaymentData payData = (PaymentData) flowContext.getFromContext(EzedKeyConstants.PAYMENT_DATA);
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      Connection con = DbConnectionManager.getConnectionByName(dbConnName);
      RuleDecisionKey ruleDecisionKey = RuleDecisionKey.ON_FAILURE;
      try
      {
         PreparedStatement ps = DbConnectionManager.getPreparedStatement(dbConnName, con, "UpdatePaymentDetails");
         ps.setString(1, Integer.toString(payData.getPaymentStatus()));
         ps.setString(2, payData.getMerchantRef());
         if (payData.getPaymentStatus() == 1)
         {
            ps.setString(3, PaymentData.CART_PAID);
         }
         else
         {
            ps.setString(3, PaymentData.CART_PAYMENT_FAILED_TIMEOUT);
         }
         ps.setString(4, payData.getAppId());
         ps.setString(5, payData.getPaymentId());
         ps.setString(6, PaymentData.CART_ADDED_TO_USER);
         int rows = ps.executeUpdate();
         if (rows == 0)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Payment id not found in database");
            }
            response.setParameters(EzedTransactionCodes.PAYMENT_NOT_FOUND, "Payment id not found in database");
            ruleDecisionKey = RuleDecisionKey.ON_FAILURE;
         }
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + "rows updated in database while updating payment status for payment id " + payData.getPaymentId());
         }
         response.setParameters(EzedTransactionCodes.SUCCESS, "Payment updated Successfully. Items from cart will be added shortly.");
         ruleDecisionKey = RuleDecisionKey.ON_SUCCESS;
      }
      catch (SQLException e)
      {
         logger.error("Error while updating payment status", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while processing payment");
         ruleDecisionKey = RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while updating payment status", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while processing payment");
         ruleDecisionKey = RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, con);
         if (payData.getPaymentStatus() == 1)
         {
            if (ruleDecisionKey == RuleDecisionKey.ON_FAILURE)
            {
               if (logger.isDebugEnabled())
               {
                  logger.debug("Payment was successful but could not be processed. Needs to be reversed");
               }
               if (reconLogger.isInfoEnabled())
               {
                  StringBuffer sb = new StringBuffer();
                  sb.append(payData.getAppId()).append(payData.getPaymentId()).append(",");
                  sb.append(payData.getPaymentStatus()).append(",");
                  sb.append(payData.getMerchantRef());
                  reconLogger.info(sb.toString());
               }
               ruleDecisionKey = RuleDecisionKey.ON_PARTIAL;
            }
            else
            {
               ruleDecisionKey = RuleDecisionKey.ON_SUCCESS;
            }
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Payment was unsuccessful and also could not be processed");
            }
            ruleDecisionKey = RuleDecisionKey.ON_FAILURE;
         }
      }
      return ruleDecisionKey;
   }
}
