package com.intelli.ezed.bulkupload.rules;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.apache.commons.digester3.Digester;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import com.intelli.ezed.bulkupload.data.ChapterDetails;
import com.intelli.ezed.bulkupload.data.CourseDetails;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;

public class ChapterDetailsHandler extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.BULK_UPLOAD_LOG);
   private String chapterPropertiesFileName;
   private Digester chapterDigester = new Digester();
   private static Object lockObject = new Object();
   private String dbConnName;

   public ChapterDetailsHandler(String ruleName, String dbConnName, String chapterPropertiesFileName,
                                String chapDetailsClassFQCN)
   {
      super(ruleName);
      initDigester(chapDetailsClassFQCN);
      this.dbConnName = dbConnName;
      this.chapterPropertiesFileName = chapterPropertiesFileName;
   }

   private void initDigester(String chapDetailsClassFQCN)
   {
      chapterDigester.setValidating(false);
      chapterDigester.addObjectCreate("chapterdetails", chapDetailsClassFQCN);
      chapterDigester.addCallMethod("chapterdetails/name", "setName", 0);
      chapterDigester.addCallMethod("chapterdetails/learningpath", "setLearningPath", 0);
      chapterDigester.addObjectCreate("chapterdetails/test", "com.intelli.ezed.bulkupload.data.TestDetails");
      chapterDigester.addSetNext("chapterdetails/test", "addTestDetails", "com.intelli.ezed.bulkupload.data.TestDetails");
      chapterDigester.addCallMethod("chapterdetails/test/time", "setTestTime", 0);
      chapterDigester.addCallMethod("chapterdetails/test/noofquestions", "setNoOfQuestions", 0, new Class<?>[]{Integer.class});
      chapterDigester.addCallMethod("chapterdetails/test/description", "setDescription", 0);
      chapterDigester.addCallMethod("chapterdetails/test/name", "setTestName", 0);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      CourseDetails courseDetails = (CourseDetails) flowContext.getFromContext(EzedKeyConstants.BULK_UPLOAD_COURSEOBJECT);
      File chapterDirFile = (File) flowContext.getFromContext(EzedKeyConstants.BULK_UPLOAD_CHAPTERFILE);
      String chapterPropFile = chapterDirFile.getAbsolutePath() + "/" + chapterPropertiesFileName;
      Map<String, String> contentDirIdMap = (Map<String, String>) flowContext.getFromContext(EzedKeyConstants.BULK_UPLOAD_CONTENTMAP);
      ChapterDetails chapDetails = null;

      try
      {
         synchronized (lockObject)
         {
            chapDetails = chapterDigester.parse(new File(chapterPropFile));
            chapDetails.setNewChapterId();
         }
         chapDetails.persistChapter(dbConnName, logger);
         chapDetails.persistEnclosedTests(dbConnName, logger, courseDetails.getCourseId());
         chapDetails.persistLearningPath(dbConnName, logger, contentDirIdMap);
         courseDetails.putChapterDirIdMap(chapterDirFile.getName(), chapDetails.getChapterId(), dbConnName, logger, contentDirIdMap);
         flowContext.putIntoContext(EzedKeyConstants.BULK_UPLOAD_CHAPTEROBJECT, chapDetails);
         flowContext.putIntoContext(EzedKeyConstants.BULK_UPLOAD_QBANK_DIR_FILE, chapterDirFile);
      }
      catch (IOException e)
      {
         logger.error("Error while reading chapter details properties file", e);
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (SAXException e)
      {
         logger.error("Error while reading chapter details properties file", e);
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while processing chapter details after reading properly from properties file", e);
         return RuleDecisionKey.ON_FAILURE;
      }
      return RuleDecisionKey.ON_SUCCESS;
   }
}
