package com.intelli.ezed.utils;

import org.apache.axis.encoding.Base64;

//indraneel : created class 17-03-11
public class RandomNumberGenerator
{
   private static String arr[] = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};
   public static String random(int totalLength)
   {
      //to store
      StringBuffer s = new StringBuffer("");

      //calling totalLength times to get 1 character at a time
      for (int i = 0; i < totalLength; i++)
         s.append(randomDigit());

      return (s.toString());
   }

   private static String randomDigit()
   {
      String str;

      int digit = (int) Math.floor(Math.random() * 10);

      if (digit != 0)
         str = "" + digit;
      else
         str = "0";

      return (str);
   }

   private static String randomHex()
   {
      int digit = (int) Math.floor(Math.random() * 16);
      return Integer.toHexString(digit).toUpperCase();
   }

   private static String randomCharNum()
   {
      int digit = (int) Math.floor(Math.random() * 36);
      return arr[digit].toUpperCase();
   }

   private static String generateRandomHex(int totalLength)
   {
      StringBuffer s = new StringBuffer("");

      for (int i = 0; i < totalLength; i++)
         s.append(randomHex());

      return (s.toString());
   }

   public static String generateRandomAlphaNum(int totalLength)
   {
      StringBuffer s = new StringBuffer("");

      for (int i = 0; i < totalLength; i++)
         s.append(randomCharNum());

      return (s.toString());
   }

   public static void main(String[] args)
   {
      System.out.println("Random hex value (length 16) : " + generateRandomHex(16));
      System.out.println("Base 64 encoding of random hex value (length 16) : " + Base64.encode(generateRandomHex(16).getBytes()));
      System.out.println(Base64.encode(Base64.encode("1rctc5indian".getBytes()).getBytes()));
      System.out.println(new String(Base64.decode("YTc5NDZmYzIyNGI4NTNjZQ==")));
      System.out.println(generateRandomAlphaNum(10));
   }
}
