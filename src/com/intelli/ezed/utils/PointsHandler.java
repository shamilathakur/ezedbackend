package com.intelli.ezed.utils;

import org.sabre.workflow.context.FlowContext;
import org.sabre.workflow.processor.InputProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedLoggerName;

public class PointsHandler extends InputProcessor
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   @Override
   public void initialize(FlowContext flowContext)
   {
      if(logger.isDebugEnabled())
      {
         logger.debug("Points handler processor initialized");
      }
   }

   @Override
   public void start()
   {
      if(logger.isDebugEnabled())
      {
         logger.debug("Points handler processor started");
      }
   }

}
