package com.intelli.ezed.checks;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.AddScratchCardToUserData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.ScratchCardErrorLogData;

public class AddCourseUsingScratchCardRequestParser extends SingletonRule {

	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

	public AddCourseUsingScratchCardRequestParser(String ruleName) {
		super(ruleName);
	}

	@Override
	public RuleDecisionKey executeRule(FlowContext flowContext) {

		Response response = (Response) flowContext
				.getFromContext(EzedKeyConstants.EZED_RESPONSE);
		MessageData messageData = (MessageData) flowContext
				.getFromContext(MessageData.class.getName());
		AddScratchCardToUserData data = new AddScratchCardToUserData();
		ScratchCardErrorLogData errorData = new ScratchCardErrorLogData();

		String userid = messageData.getParameter("email");
		String sessionid = messageData.getParameter("sessionid");

		String scratchcardid = messageData.getParameter("scratchcardid");
		if (scratchcardid.trim().compareTo("") == 0
				|| scratchcardid.compareTo("null") == 0
				|| scratchcardid == null) {
			if (logger.isDebugEnabled()) {
				logger.debug("Scratch card id is invalid" + scratchcardid);
			}
			response.setParameters(
					EzedTransactionCodes.INVALID_MESSAGE_PARAMETER,
					"Scratch card id is invalid" + scratchcardid);
			return RuleDecisionKey.ON_FAILURE;
		}
		data.setScratchcardid(scratchcardid);
		data.setUserid(userid);
		data.setSessionid(sessionid);

		errorData.setScratchcardid(scratchcardid);
		errorData.setUserid(userid);

		System.out.println("In Class : " + this.getClass().getName());
		System.out.print("USERID : " + data.getUserid() + "  Sessionid : "
				+ data.getSessionid());
		flowContext.putIntoContext(
				EzedKeyConstants.ADD_COURSE_USING_SCRATCHCARD_DETAILS, data);
		flowContext.putIntoContext(EzedKeyConstants.SCRATCH_CARD_ERROR_LOG_DB,
				errorData);

		return RuleDecisionKey.ON_SUCCESS;
	}

}
