package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.FrontendFetchData;
import com.intelli.ezed.data.Response;

public class GroupUserFetcher extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public GroupUserFetcher(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      String groupId = messageData.getParameter("groupid");
      if (groupId == null || groupId.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Group id is invalid " + groupId);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid group id in request");
         return RuleDecisionKey.ON_FAILURE;
      }
      PreparedStatement ps = null;
      ResultSet rs = null;
      String[] groupUserArr = null;
      FrontendFetchData groupUserData = new FrontendFetchData();
      response.setResponseObject(groupUserData);
      response.setStatusCodeSend(false);
      groupUserData.setAttribName("aaData");
      ArrayList<String[]> groupUsers = new ArrayList<String[]>();
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchGroupUsersWithName");
         ps.setString(1, groupId);
         rs = ps.executeQuery();
         int i = 1;
         String firstName = null;
         String lastName = null;
         while (rs.next())
         {
            groupUserArr = new String[3];
            groupUserArr[0] = Integer.toString(i);
            groupUserArr[1] = rs.getString("userid");
            firstName = rs.getString("firstname");
            if (firstName == null)
            {
               firstName = "";
            }
            lastName = rs.getString("lastname");
            if (lastName == null)
            {
               lastName = "";
            }
            groupUserArr[2] = firstName + " " + lastName;
            i++;
            groupUsers.add(groupUserArr);
         }
         groupUserData.setAttribList(groupUsers);
         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
      }
      catch (SQLException e)
      {
         logger.error("DB error while fetching list of group users", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "DB error while fetching group user list");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching list of group users", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching group user list");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing result set while fetching college list", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
      return RuleDecisionKey.ON_SUCCESS;
   }
}
