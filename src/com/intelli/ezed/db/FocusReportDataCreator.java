package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.analytics.data.TestResponseOne;
import com.intelli.ezed.analytics.data.TestResposeTwo;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.utils.EzedHelper;

public class FocusReportDataCreator extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;

   public FocusReportDataCreator(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      String userid = (String) flowContext.getFromContext(EzedKeyConstants.USER_ID);
      String courseid = (String) flowContext.getFromContext(EzedKeyConstants.COURSE_ID);

      Connection connection = DbConnectionManager.getConnectionByName(dbConnName);
      ResultSet rs = null;

      Map<String, ArrayList<Integer>> testResults = new HashMap<String, ArrayList<Integer>>();
      /*
       * select t.userid, l.chapterid, t.numberofquestions, t.correct from testsummery t, testlist l where 
       * l.courseid = 'GRO2137416' and
       * t.userid = 'indra.smart@gmail.com' and
       * l.chapterid is not null and 
       * l.testid = t.testid;
       */
      try
      {
         PreparedStatement preparedStatement = DbConnectionManager.getPreparedStatement(dbConnName, connection, "GetListOfChaptersForTestSummery");
         preparedStatement.setString(1, userid);
         preparedStatement.setString(2, courseid);
         rs = preparedStatement.executeQuery();
         int percentage = 0;

         if (rs != null)
         {
            while (rs.next())
            {
               String chapterId = rs.getString("chapterid");
               float totalNumberOfQuestions = rs.getInt("numberofquestions");
               if (totalNumberOfQuestions == 0f)
               {
                  percentage = 0;
               }
               else
               {
                  float correctAns = rs.getInt("correct");
                  percentage = (int) (((float) (correctAns / totalNumberOfQuestions)) * 100f);
               }
               ArrayList<Integer> temp = testResults.get(chapterId);
               if (temp == null)
               {
                  temp = new ArrayList<Integer>();
                  testResults.put(chapterId, temp);
               }
               temp.add(percentage);
            }
         }
      }
      catch (SQLException e)
      {
         //e.printStackTrace();
         logger.error("Database Exception occoured while trying get list of tests from ongoing list.", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Internal Error. Please try again.");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error(" Exception occoured while trying get list of tests from ongoing list.", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal Error. Please try again.");
         return RuleDecisionKey.ON_FAILURE;
      }

      Set<String> keys = testResults.keySet();
      String[][] focusArray = new String[testResults.size()][];
      Map<String, Integer> testAvgResult = new HashMap<String, Integer>();
      for (String key : keys)
      {
         ArrayList<Integer> temp = testResults.get(key);
         int total = 0;
         for (int val : temp)
         {
            total = total + val;
         }
         int avg = total / temp.size();
         testAvgResult.put(key, avg);
      }

      Set<String> avgKeys = testAvgResult.keySet();
      int totalOfAverages = 0;
      for (String key : avgKeys)
      {
         totalOfAverages = totalOfAverages + testAvgResult.get(key);
      }

      Map<String, Float> testPercentResult = new HashMap<String, Float>();
      float percent = 0;
      for (String key : avgKeys)
      {
         percent = ((((float) testAvgResult.get(key)) / (float) totalOfAverages) * 100f);
         testPercentResult.put(key, percent);
      }

      float divider = (float) avgKeys.size() - 1;

      int i = 0;
      for (String key : avgKeys)
      {
         focusArray[i] = new String[2];
         focusArray[i][0] = EzedHelper.fetchChapterName(dbConnName, key, logger);
         focusArray[i][1] = Float.toString((100f - testPercentResult.get(key)) / divider);
         i++;
      }

      TestResponseOne one = new TestResponseOne();
      float[] f1 = new float[focusArray.length];
      String[] cats = new String[focusArray.length];

      for (int j = 0; j < focusArray.length; j++)
      {
         f1[j] = Float.parseFloat(focusArray[j][1]);
         cats[j] = focusArray[j][0];
      }
      one.setValues(f1);
      one.setName("Focus Report");


      TestResposeTwo testResposeTwo = new TestResposeTwo();
      TestResponseOne[] testResponseOnes = new TestResponseOne[1];
      testResponseOnes[0] = one;
      testResposeTwo.setTestResponseOnes(testResponseOnes);

      String chartType = "bar";
      String yAxisDisplayText = "Focus Percentage";
      String xAxisDisplayText = "Chapters";
      String yAxisDisplayLable = "Lable";
      String xAxisDisplayLable = "Lable";


      testResposeTwo.setGraphTitle("Chapter Wise Course Focus Report");
      testResposeTwo.setCats(cats);
      testResposeTwo.setxAxisDisplayLable(xAxisDisplayLable);
      testResposeTwo.setxAxisDisplayText(xAxisDisplayText);
      testResposeTwo.setyAxisDisplayLable(yAxisDisplayLable);
      testResposeTwo.setyAxisDisplayText(yAxisDisplayText);
      testResposeTwo.setChartType(chartType);

      response.setResponseObject(testResposeTwo);
      response.setStatusCodeSend(false);

      flowContext.putIntoContext(EzedKeyConstants.USER_FOCUS_REPORT_DATA, focusArray);
      response.setParameters(EzedTransactionCodes.SUCCESS, "Focus report created for user " + userid);
      return RuleDecisionKey.ON_SUCCESS;
   }
}
