package com.intelli.ezed.rules;

import java.util.HashMap;
import java.util.Map;

import org.sabre.exceptions.InputProcessorException;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.LinkedProcessorManager;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EventData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.UserProfile;

public class UserStreamProcessor extends SingletonRule
{

   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private EventData eventData = null;

   public UserStreamProcessor(String ruleName)
   {
      super(ruleName);
   }

   public UserStreamProcessor(String ruleName, String actionId)
   {
      super(ruleName);
      this.eventData = new EventData();
      eventData.setActionId(actionId);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      EventData flowEventData = this.eventData;
      if (flowEventData == null)
      {
         flowEventData = (EventData) flowContext.getFromContext(EzedKeyConstants.EVENT_OBJECT);
      }

      if (flowEventData == null)
      {
         logger.error("Event data could not be found. Stream processor not started");
         return RuleDecisionKey.ON_FAILURE;
      }

      Map<Object, Object> map = new HashMap<Object, Object>();
      map.put(EzedKeyConstants.EVENT_OBJECT, flowEventData);
      map.put(EzedKeyConstants.USER_DETAILS, flowContext.getFromContext(EzedKeyConstants.USER_DETAILS));

      try
      {
         LinkedProcessorManager.addTask("StreamEntryRecorder", map);
      }
      catch (InputProcessorException e)
      {
         logger.error("Stream entry processor could not be started", e);
      }
      return RuleDecisionKey.ON_SUCCESS;
   }

   public static void processUserStream(EventData eventData, UserProfile userProfile) throws Exception
   {
      Map<Object, Object> map = new HashMap<Object, Object>();
      map.put(EzedKeyConstants.EVENT_OBJECT, eventData);
      map.put(EzedKeyConstants.USER_DETAILS, userProfile);
      LinkedProcessorManager.addTask("StreamEntryRecorder", map);
   }
}
