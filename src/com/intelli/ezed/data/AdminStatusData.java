package com.intelli.ezed.data;

public class AdminStatusData extends ResponseObject
{

   private String userid;
   private String roll;
   private String statusId;
   private String name;
   private int chkflag;
   private String createdBy;
   private String createdByName;
   private String university;
   private String college;
   private String micUserId;
   private String micUserName;

   public String getUserid()
   {
      return userid;
   }
   public void setUserid(String userid)
   {
      this.userid = userid;
   }
   public int getChkflag()
   {
      return chkflag;
   }
   public void setChkflag(int chkflag)
   {
      this.chkflag = chkflag;
   }
   public String getRoll()
   {
      return roll;
   }
   public void setRoll(String roll)
   {
      this.roll = roll;
   }
   public String getStatusId()
   {
      return statusId;
   }
   public void setStatusId(String statusId)
   {
      this.statusId = statusId;
   }
   public String getName()
   {
      return name;
   }
   public void setName(String name)
   {
      this.name = name;
   }
   @Override
   public void prepareResponse() throws Exception
   {
      this.put("userid", getUserid());
      this.put("role", getRoll());
      this.put("status", getStatusId());
      this.put("name", getName());
      this.put("createdbyid", getCreatedBy());
      this.put("createdbyname", getCreatedByName());
      this.put("college", getCollege());
      this.put("university", getUniversity());
      this.put("micid", getMicUserId());
      this.put("micname", getMicUserName());
   }
   public String getCreatedBy()
   {
      return createdBy;
   }
   public void setCreatedBy(String createdBy)
   {
      this.createdBy = createdBy;
   }
   public String getCreatedByName()
   {
      return createdByName;
   }
   public void setCreatedByName(String createdByName)
   {
      this.createdByName = createdByName;
   }
   public String getUniversity()
   {
      return university;
   }
   public void setUniversity(String university)
   {
      this.university = university;
   }
   public String getCollege()
   {
      return college;
   }
   public void setCollege(String college)
   {
      this.college = college;
   }
   public String getMicUserId()
   {
      return micUserId;
   }
   public void setMicUserId(String micUserId)
   {
      this.micUserId = micUserId;
   }
   public String getMicUserName()
   {
      return micUserName;
   }
   public void setMicUserName(String micUserName)
   {
      this.micUserName = micUserName;
   }
}
