package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.AddScratchCardToUserData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.ScratchCardErrorLogData;
import com.intelli.ezed.utils.EzedHelper;
import com.intelli.ezed.utils.ScratchCardHelper;

public class ScratchCardStatusUpdator extends SingletonRule {

	private String dbConnName;
	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

	public ScratchCardStatusUpdator(String ruleName, String dbConnName) {
		super(ruleName);
		this.dbConnName = dbConnName;
	}

	@Override
	public RuleDecisionKey executeRule(FlowContext flowContext) {
		Response response = (Response) flowContext
				.getFromContext(EzedKeyConstants.EZED_RESPONSE);
		AddScratchCardToUserData data = (AddScratchCardToUserData) flowContext
				.getFromContext(EzedKeyConstants.ADD_COURSE_USING_SCRATCHCARD_DETAILS);
		ScratchCardErrorLogData errordata = (ScratchCardErrorLogData) flowContext
				.getFromContext(EzedKeyConstants.SCRATCH_CARD_ERROR_LOG_DB);

		System.out.println("In Class : " + this.getClass().getName());
		System.out.print("USERID : " + data.getUserid() + "  Sessionid : "
				+ data.getSessionid());

		Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
		PreparedStatement ps = null;
		int rs;

		try {
			ps = DbConnectionManager.getPreparedStatement(dbConnName, conn,
					"UpdateScratchCardStatus");
			ps.setInt(1, 1);
			ps.setString(2, data.getUserid());
			ps.setString(3, sdf.format(new Date()));
			ps.setString(4, data.getScratchcardid());
			rs = ps.executeUpdate();
			if (rs > 0) {
				logger.info("Scratch card is marked as used for scid : "
						+ data.getScratchcardid());
			} else {
				logger.info("Some error occured while updating the status of scratch card as marked : "
						+ data.getScratchcardid());
			}
		} catch (SQLException e) {

			System.out.println("SQL Error In Class : "
					+ this.getClass().getName());
			if (logger.isDebugEnabled()) {
				logger.error("SQL Error occured while updating the status of scratch card as marked : "
						+ data.getScratchcardid());
			}
			errordata.setClassname(this.getClass().getName());
			ScratchCardHelper.callDbError(errordata,dbConnName,logger);
			response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED,
					"DB exception occured while updating the scratch card.");
			return RuleDecisionKey.ON_FAILURE;
		} catch (Exception e) {
			if (logger.isDebugEnabled()) {
				logger.error("Internal Error occured while updating the status of scratch card as marked : "
						+ data.getScratchcardid());
			}
			errordata.setClassname(this.getClass().getName());
			ScratchCardHelper.callInternalError(errordata,dbConnName,logger);
			response.setParameters(EzedTransactionCodes.INTERNAL_ERROR,
					"Internal exception occured while updating the scratch card.");
			return RuleDecisionKey.ON_FAILURE;
		}

		logger.info("Entry In paymentdetails table will be done now.");
		
		int rs1;
		PreparedStatement ps1 = null;
		
		try {
			ps1 = DbConnectionManager.getPreparedStatement(dbConnName, conn,
					"EntryInPaymentDetails");
			ps1.setString(1, EzedHelper.getPaymentDetailId(dbConnName,logger));
			ps1.setString(2, data.getUserid());
			ps1.setString(3, (data.getAppId()+data.getPaymentId()));
			ps1.setInt(4, data.getPrice());
			ps1.setString(5, "0");
			ps1.setString(6, "Scratch Card");
			ps1.setString(7, "Scratch Card");
			ps1.setString(8, "Y");
			rs1 = ps1.executeUpdate();
			if (rs1 > 0) {
				logger.info("Entry successful in paymentdetails table."
						+ data.getScratchcardid());
			} else {
				logger.info("Some error occured while making the payment entry using scratch card. "
						+ data.getScratchcardid());
			}
		} catch (SQLException e) {

			System.out.println("SQL Error In Class : "
					+ this.getClass().getName());
			if (logger.isDebugEnabled()) {
				logger.error("SQL Error occured while making the payment entry using scratch card : "
						+ data.getScratchcardid());
			}
			errordata.setClassname(this.getClass().getName());
			ScratchCardHelper.callDbError(errordata,dbConnName,logger);
			response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED,
					"DB exception occured while making the payment entry using scratch card.");
			return RuleDecisionKey.ON_FAILURE;
		} catch (Exception e) {
			if (logger.isDebugEnabled()) {
				logger.error("Internal Error occured while making the payment entry using scratch card :"
						+ data.getScratchcardid());
			}
			errordata.setClassname(this.getClass().getName());
			ScratchCardHelper.callInternalError(errordata,dbConnName,logger);
			response.setParameters(EzedTransactionCodes.INTERNAL_ERROR,
					"Internal exception occured while making the payment entry using scratch card.");
			return RuleDecisionKey.ON_FAILURE;
		}

		return RuleDecisionKey.ON_SUCCESS;
	}

}
