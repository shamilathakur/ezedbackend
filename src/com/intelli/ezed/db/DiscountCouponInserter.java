package com.intelli.ezed.db;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.AdminProfile;
import com.intelli.ezed.data.DiscountCouponData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.utils.EzedHelper;

public class DiscountCouponInserter extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private int retrialCount;
   private int couponLength;

   public DiscountCouponInserter(String ruleName, String dbConnName, String retrialCount, String couponLength)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
      this.retrialCount = Integer.parseInt(retrialCount);
      this.couponLength = Integer.parseInt(couponLength);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      AdminProfile adminProfile = (AdminProfile) flowContext.getFromContext(EzedKeyConstants.ADMIN_PROFILE);

      String valueDiscountString = messageData.getParameter("valuediscount");
      if (valueDiscountString == null || valueDiscountString.trim().compareTo("") == 0)
      {
         valueDiscountString = "0";
      }
      if (!EzedHelper.isNumeric(valueDiscountString.trim()))
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Value discount is improper " + valueDiscountString);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid value discount");
         return RuleDecisionKey.ON_FAILURE;
      }

      int valueDiscount = Integer.parseInt(valueDiscountString);

      String percentDiscountString = messageData.getParameter("percentdiscount");
      if (percentDiscountString == null || percentDiscountString.trim().compareTo("") == 0)
      {
         percentDiscountString = "0";
      }
      float percentDiscount = 0f;
      try
      {
         percentDiscount = Float.parseFloat(percentDiscountString);
      }
      catch (Exception e)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Error while parsing percent discount as float " + percentDiscountString);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid percent discount");
         return RuleDecisionKey.ON_FAILURE;
      }

      if (valueDiscount == 0 && percentDiscount == 0f)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Both percent and float discounts are 0");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Both value and percentage discounts cannot be 0");
         return RuleDecisionKey.ON_FAILURE;
      }

      String maxValueDiscountString = messageData.getParameter("maxvaluediscount");
      if (maxValueDiscountString == null || maxValueDiscountString.trim().compareTo("") == 0 || !EzedHelper.isNumeric(maxValueDiscountString.trim()) || Integer.parseInt(maxValueDiscountString.trim()) == 0)
      {
         maxValueDiscountString = "0";
      }

      int maxValueDiscount = Integer.parseInt(maxValueDiscountString);

      if (valueDiscount == 0 && maxValueDiscount == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Max value discount not specified for percent discount");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Improper max value discount");
         return RuleDecisionKey.ON_FAILURE;
      }
      String startTimeString = messageData.getParameter("starttime");
      if (startTimeString == null || startTimeString.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Start time of discount coupon is invalid " + startTimeString);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid start time");
         return RuleDecisionKey.ON_FAILURE;
      }

      SimpleDateFormat sdfFront = new SimpleDateFormat("MM/dd/yyyy");
      Date startDate = null;
      try
      {
         startDate = sdfFront.parse(startTimeString);
      }
      catch (ParseException e)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Start time of discount coupon is invalid " + startTimeString, e);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid start time");
         return RuleDecisionKey.ON_FAILURE;
      }


      String endTimeString = messageData.getParameter("endtime");
      if (endTimeString == null || endTimeString.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("End time of discount coupon is invalid " + endTimeString);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid end time");
         return RuleDecisionKey.ON_FAILURE;
      }

      Date endDate = null;
      try
      {
         endDate = sdfFront.parse(endTimeString);
      }
      catch (ParseException e)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("End time of discount coupon is invalid " + endTimeString, e);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid end time");
         return RuleDecisionKey.ON_FAILURE;
      }

      String maxCountString = messageData.getParameter("maxcount");
      if (maxCountString == null || maxCountString.trim().compareTo("") == 0)
      {
         maxCountString = "0";
      }
      if (!EzedHelper.isNumeric(maxCountString))
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Max count of discount coupons is invalid " + maxCountString);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid max discount coupon count");
         return RuleDecisionKey.ON_FAILURE;
      }
      int maxCount = Integer.parseInt(maxCountString);

      String assignedAdmin = messageData.getParameter("assignedadmin");
      if (assignedAdmin == null || assignedAdmin.trim().compareTo("") == 0)
      {
         assignedAdmin = null;
      }
      //      else
      //      {
      //         assignedAdmin = assignedAdmin.toUpperCase();
      //      }

      DiscountCouponData data = new DiscountCouponData(retrialCount, couponLength);
      data.setValueDiscount(valueDiscount);
      data.setPercentDiscount(percentDiscount);
      data.setMaxValueDiscount(maxValueDiscount);
      data.setStartTime(startDate);
      data.setEndTime(endDate);
      data.setMaxCount(maxCount);
      data.setAssignedAdmin(assignedAdmin);
      data.setCreateAdmin(adminProfile.getUserid());
      data.setCreateTime(new Date());
      data.setValidFlag(DiscountCouponData.DISCOUNT_VALID);
      try
      {
         data.handleCreateCoupon(dbConnName, logger, 0);
      }
      catch (SQLException e)
      {
         logger.error("DB error while creating coupon", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while creating coupon");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("DB error while creating coupon", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while creating coupon");
         return RuleDecisionKey.ON_FAILURE;
      }
      response.setResponseObject(data);
      response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
      return RuleDecisionKey.ON_SUCCESS;
   }
}