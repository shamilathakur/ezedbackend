package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.AdminProfile;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.FrontendFetchData;
import com.intelli.ezed.data.Response;

public class GroupAdminForDropDownFetcher extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public GroupAdminForDropDownFetcher(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      AdminProfile profile = (AdminProfile) flowContext.getFromContext(EzedKeyConstants.ADMIN_PROFILE);
      String adminUserId = profile.getUserid();
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      String[] groupAdminArr = null;
      FrontendFetchData groupAdminData = new FrontendFetchData();
      response.setResponseObject(groupAdminData);
      response.setStatusCodeSend(false);
      groupAdminData.setAttribName("aaData");
      ArrayList<String[]> groupAdmins = new ArrayList<String[]>();
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchGroupAdminUsersForMIC");
         ps.setString(1, adminUserId);
         rs = ps.executeQuery();
         int i = 1;
         while (rs.next())
         {
            groupAdminArr = new String[3];
            groupAdminArr[0] = Integer.toString(i);
            groupAdminArr[1] = rs.getString("userid");
            groupAdminArr[2] = rs.getString("name");
            groupAdmins.add(groupAdminArr);
            i++;
         }
         groupAdminData.setAttribList(groupAdmins);
         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
         return RuleDecisionKey.ON_SUCCESS;
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching list of group admins", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching list of group admins");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching list of group admins", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching list of group admins");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing resultset while fetching group admin list", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
}
