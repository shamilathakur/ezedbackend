package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.PaymentIdData;
import com.intelli.ezed.data.Response;

public class DiscountAndTaxPersister extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public DiscountAndTaxPersister(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      PaymentIdData payData = (PaymentIdData) response.getResponseObject();

      Connection conn = null;
      PreparedStatement ps = null;
      Float discountAmount = (Float) flowContext.getFromContext(EzedKeyConstants.DISCOUNT_DB_AMT);
      Float cGST = (Float) flowContext.getFromContext(EzedKeyConstants.CGST_DB_AMT);
      Float sGST = (Float) flowContext.getFromContext(EzedKeyConstants.SGST_DB_AMT);
     // Float higherEduCess = (Float) flowContext.getFromContext(EzedKeyConstants.HIGHER_EDU_CESS_DB_AMT);

      if (discountAmount != null && discountAmount != 0f)
      {
         try
         {
            conn = DbConnectionManager.getConnectionByName(dbConnName);
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertAdditionTransactionAmounts");
            ps.setString(1, payData.getAppId() + payData.getPaymentId());
            ps.setString(2, "DISCOUNT");
            ps.setFloat(3, (float) Math.round(discountAmount * 100) / 100);
            int rows = ps.executeUpdate();
            if (logger.isDebugEnabled())
            {
               logger.debug(rows + " rows inserted for discount amount of " + discountAmount);
            }
         }
         catch (Exception e)
         {
            logger.error("Error while inserting discount amount in db");
            response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while inserting discount");
            return RuleDecisionKey.ON_FAILURE;
         }
         finally
         {
            DbConnectionManager.releaseConnection(dbConnName, conn);
         }
      }

      if (cGST != null && cGST != 0f)
      {
         try
         {
            conn = DbConnectionManager.getConnectionByName(dbConnName);
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertAdditionTransactionAmounts");
            ps.setString(1, payData.getAppId() + payData.getPaymentId());
            ps.setString(2, "CGST");
            ps.setFloat(3, (float) Math.round(cGST * 100) / 100);
            int rows = ps.executeUpdate();
            if (logger.isDebugEnabled())
            {
               logger.debug(rows + " rows inserted for CGST amount of " + discountAmount);
            }
         }
         catch (Exception e)
         {
            logger.error("Error while inserting CGST tax amount in db", e);
            response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while inserting CGST tax");
            return RuleDecisionKey.ON_FAILURE;
         }
         finally
         {
            DbConnectionManager.releaseConnection(dbConnName, conn);
         }
      }

      if (sGST != null && sGST != 0f)
      {
         try
         {
            conn = DbConnectionManager.getConnectionByName(dbConnName);
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertAdditionTransactionAmounts");
            ps.setString(1, payData.getAppId() + payData.getPaymentId());
            ps.setString(2, "SGST");
            ps.setFloat(3, (float) Math.round(sGST * 100) / 100);
            int rows = ps.executeUpdate();
            if (logger.isDebugEnabled())
            {
               logger.debug(rows + " rows inserted for SGST  amount of " + discountAmount);
            }
         }
         catch (Exception e)
         {
            logger.error("Error while inserting SGST  amount in db", e);
            response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while inserting SGST ");
            return RuleDecisionKey.ON_FAILURE;
         }
         finally
         {
            DbConnectionManager.releaseConnection(dbConnName, conn);
         }
      }

     
      return RuleDecisionKey.ON_SUCCESS;
   }
}
