package com.intelli.ezed.rules;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.AdminLoginResponse;
import com.intelli.ezed.data.AdminProfile;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.utils.AccountStatusHelper;

public class AdminUserAuthenticator extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   public AdminUserAuthenticator(String ruleName)
   {
      super(ruleName);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      AdminProfile adminProfile = (AdminProfile) flowContext.getFromContext(EzedKeyConstants.ADMIN_PROFILE);
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      int chkflag = adminProfile.getChkFlag();
      String password = adminProfile.getPassword();
      String userPass = messageData.getParameter("password");

      if (password.compareTo(userPass) != 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Passwords do not match");
         }
         response.setParameters(EzedTransactionCodes.INVALID_PASSWORD, "Invalid admin password. Login denied");
         return RuleDecisionKey.ON_FAILURE;
      }

      if (!AccountStatusHelper.isRegistered(chkflag))
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Userid not registered. " + adminProfile.getUserid());
         }
         response.setParameters(EzedTransactionCodes.USERID_NOT_REGISTERED, "Admin user not registered.");
         return RuleDecisionKey.ON_FAILURE;
      }
      if (AccountStatusHelper.isLock(chkflag))
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Userid is blocked. " + adminProfile.getUserid());
         }
         response.setParameters(EzedTransactionCodes.USER_BLOCKED, "Admin user is blocked");
         return RuleDecisionKey.ON_FAILURE;
      }

      if (logger.isDebugEnabled())
      {
         logger.debug("Admin user registered.");
      }

      AdminLoginResponse adminLoginResponse = new AdminLoginResponse();
      adminLoginResponse.setUserProfile(adminProfile);
      response.setResponseObject(adminLoginResponse);
      response.setParameters(EzedTransactionCodes.SUCCESS, "Admin login successful.");
      return RuleDecisionKey.ON_SUCCESS;
   }
}
