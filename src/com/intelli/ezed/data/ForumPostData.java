package com.intelli.ezed.data;

import java.util.ArrayList;

public class ForumPostData extends ResponseObject
{
   public static final int ADMIN_POST = 1;
   public static final int USER_POST = 0;

   private String postId;
   private String parentPostId;
   private int indent;
   private ArrayList<ForumPostData> childNodes = new ArrayList<ForumPostData>();

   private String userId;
   private String postData;
   private int abuseCount;
   private int likeCount;
   private String createTime;
   private String imagePath;
   private int replyCount;
   private String profileImage;
   private boolean adminFlag;
   private String name;
   private boolean isLiked = false;
   private boolean isAbused = false;
   private String status;

   public String getProfileImage()
   {
      return profileImage;
   }
   public void setProfileImage(String profileImage)
   {
      this.profileImage = profileImage;
   }
   public boolean isAdminFlag()
   {
      return adminFlag;
   }
   public void setAdminFlag(boolean adminFlag)
   {
      this.adminFlag = adminFlag;
   }
   public void setAdminFlag(int adminFlag)
   {
      if (adminFlag == ADMIN_POST)
      {
         setAdminFlag(true);
      }
      else
      {
         setAdminFlag(false);
      }
   }

   public String getUserId()
   {
      return userId;
   }
   public void setUserId(String userId)
   {
      this.userId = userId;
   }
   public String getPostData()
   {
      return postData;
   }
   public void setPostData(String postData)
   {
      this.postData = postData;
   }
   public int getAbuseCount()
   {
      return abuseCount;
   }
   public void setAbuseCount(int abuseCount)
   {
      this.abuseCount = abuseCount;
   }
   public int getLikeCount()
   {
      return likeCount;
   }
   public void setLikeCount(int likeCount)
   {
      this.likeCount = likeCount;
   }
   public String getCreateTime()
   {
      return createTime;
   }
   public void setCreateTime(String createTime)
   {
      this.createTime = createTime;
   }
   public String getImagePath()
   {
      return imagePath;
   }
   public void setImagePath(String imagePath)
   {
      this.imagePath = imagePath;
   }
   public int getReplyCount()
   {
      return replyCount;
   }
   public void setReplyCount(int replyCount)
   {
      this.replyCount = replyCount;
   }
   public ArrayList<ForumPostData> getChildNodes()
   {
      return childNodes;
   }
   public void setChildNodes(ArrayList<ForumPostData> childNodes)
   {
      this.childNodes = childNodes;
   }
   public int getIndent()
   {
      return indent;
   }
   public void setIndent(int indent)
   {
      this.indent = indent;
   }
   public String getPostId()
   {
      return postId;
   }
   public void setPostId(String postId)
   {
      this.postId = postId;
   }
   public String getParentPostId()
   {
      return parentPostId;
   }
   public void setParentPostId(String parentPostId)
   {
      this.parentPostId = parentPostId;
   }

   public void calculateIndents(int indent)
   {
      this.setIndent(indent);
      for (ForumPostData childUnits : childNodes)
      {
         childUnits.calculateIndents(indent + 1);
      }
   }

   public void print()
   {
      System.out.println(getPostId() + "   " + getIndent());
      for (ForumPostData childUnits : childNodes)
      {
         childUnits.print();
      }
   }

   public void finalizeTree(ArrayList<ForumPostData> finalTree)
   {
      finalTree.add(this);
      for (ForumPostData childUnits : childNodes)
      {
         childUnits.finalizeTree(finalTree);
      }
   }
   @Override
   public void prepareResponse() throws Exception
   {
      this.put("userid", getUserId());
      this.put("postid", getPostId());
      this.put("postdata", getPostData());
      this.put("createtime", getCreateTime());
      this.put("imagepath", getImagePath());
      this.put("likecount", getLikeCount());
      this.put("abusecount", getAbuseCount());
      this.put("parentid", getParentPostId());
      this.put("indent", getIndent());
      this.put("replycount", childNodes.size());
      this.put("profilepic", getProfileImage());
      this.put("adminpost", isAdminFlag());
      this.put("name", getName());
      this.put("liked", isLiked());
      this.put("abused", isAbused());
      this.put("status", getStatus());
   }
   public String getName()
   {
      return name;
   }
   public void setName(String name)
   {
      this.name = name;
   }
   public boolean isLiked()
   {
      return isLiked;
   }
   public void setLiked(boolean isLiked)
   {
      this.isLiked = isLiked;
   }
   public boolean isAbused()
   {
      return isAbused;
   }
   public void setAbused(boolean isAbused)
   {
      this.isAbused = isAbused;
   }
   public String getStatus()
   {
      return status;
   }
   public void setStatus(String status)
   {
      this.status = status;
   }
}
