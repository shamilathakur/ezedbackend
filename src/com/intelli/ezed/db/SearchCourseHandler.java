package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.GenericCourseListForFrontend;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.UserProfile;

public class SearchCourseHandler extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;
   private int failureExceptionCount;

   public SearchCourseHandler(String ruleName, String dbConnName, String failureExceptionCount)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
      this.failureExceptionCount = Integer.parseInt(failureExceptionCount);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      String searchString = messageData.getParameter("searchstring");
      if (searchString == null || searchString.trim().compareTo("") == 0 || searchString.trim().length() < 3)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Invalid search string received in request " + searchString);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid search string");
         return RuleDecisionKey.ON_FAILURE;
      }
      searchString = new StringBuffer("%").append(searchString.trim().toUpperCase()).append("%").toString();

      GenericCourseListForFrontend genericCourseListForFrontend = new GenericCourseListForFrontend(failureExceptionCount);
      response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
      response.setResponseObject(genericCourseListForFrontend);

      Connection conn = null;
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         //select courseid from courselist where coursename like ?
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "SearchCourseNames");
         ps.setString(1, searchString);
         rs = ps.executeQuery();
         while (rs.next())
         {
            genericCourseListForFrontend.addCourse(rs.getString("courseid"), "Course name matches your search");
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching course list while searching course names " + searchString, e);
      }
      catch (Exception e)
      {
         logger.error("Error while fetching course list while searching course names " + searchString, e);
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing result set while searching coursenames for " + searchString, e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      try
      {
         //select distinct(c.courseid) courseid from courselist c, coursepath cp where c.courseid = cp.courseid and cp.chapterid in(select chapterid from chapterlist where chaptername like '%PPM%');
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "SearchChapterNames");
         ps.setString(1, searchString);
         rs = ps.executeQuery();
         while (rs.next())
         {
            genericCourseListForFrontend.addCourse(rs.getString("courseid"), "One of the chapters in the course matches your search");
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching course list while searching chapter names " + searchString, e);
      }
      catch (Exception e)
      {
         logger.error("Error while fetching course list while searching chapter names " + searchString, e);
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing result set while searching chapter names for " + searchString, e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }


      if (genericCourseListForFrontend.getCourseIdList().size() == 0 && response.getResponseCode() == EzedTransactionCodes.SUCCESS)
      {
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "No courses could be found");
         return RuleDecisionKey.ON_FAILURE;
      }
      else if (genericCourseListForFrontend.getCourseIdList().size() != 0 && response.getResponseCode() == EzedTransactionCodes.SUCCESS)
      {
         UserProfile userProfile = (UserProfile) flowContext.getFromContext(EzedKeyConstants.USER_DETAILS);
         if (userProfile != null)
         {
            genericCourseListForFrontend.removeAlreadyPurchasedCourse(userProfile.getCourseList(), userProfile.getCourseTypeList());
         }
         if (genericCourseListForFrontend.getCourseIdList().size() == 0)
         {
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "No courses can be found matching your search criteria");
            return RuleDecisionKey.ON_FAILURE;
         }
         try
         {
            genericCourseListForFrontend.fetchCourseDetails(dbConnName, logger);
         }
         catch (SQLException e)
         {
            logger.error("Error while fetching course details of suggested course", e);
            response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching suggested courses");
            return RuleDecisionKey.ON_FAILURE;
         }
         catch (Exception e)
         {
            logger.error("Error while fetching course details of suggested course", e);
            response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching suggested courses");
            return RuleDecisionKey.ON_FAILURE;
         }
         return RuleDecisionKey.ON_SUCCESS;
      }
      else
      {
         return RuleDecisionKey.ON_FAILURE;
      }
   }

}
