package com.intelli.ezed.data;

public class ScratchCardDetailResponse extends ResponseObject {

	private String sessionId;
	private ScratchCardData scratchCardData;

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public ScratchCardData getScratchCardData() {
		return scratchCardData;
	}

	public void setScratchCardData(ScratchCardData scratchCardData) {
		this.scratchCardData = scratchCardData;
	}

	@Override
	public void prepareResponse() throws Exception {
		this.put("sessionid", getSessionId());
		getScratchCardData().prepareResponse();
		this.put("details", getScratchCardData());
	}
}
