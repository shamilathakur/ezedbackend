package com.intelli.ezed.analytics.data;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;

import org.json.JSONArray;

import com.intelli.ezed.data.ResponseObject;

public class TestResultPieChartData extends ResponseObject
{
   private GenericChartData title = new GenericChartData();
   private ArrayList<Boolean> correctAnswers = new ArrayList<Boolean>();

   @Override
   public void prepareResponse() throws Exception
   {
      title.getText().add("text");
      title.getValues().add("User Test Report");
      title.prepareResponse();

      JSONArray jsonArray = new JSONArray();
      GenericChartData typeData = new GenericChartData();
      typeData.getText().add("type");
      typeData.getValues().add("pie");
      //      typeData.prepareResponse();
      //      jsonArray.put(typeData);
      //
      //      GenericChartData nameData = new GenericChartData();
      //      nameData.getText().add("name");
      //      nameData.getValues().add("Test Result");
      //      nameData.prepareResponse();
      //      jsonArray.put(nameData);
      typeData.getText().add("name");
      typeData.getValues().add("Test Result");


      JSONArray colorArray = new JSONArray();
      JSONArray oneDataArray = new JSONArray();

      JSONArray singleDataArray = null;
      DecimalFormat df = new DecimalFormat("#.#");
      df.setRoundingMode(RoundingMode.HALF_EVEN);

      float singlePart = 100f / (float) correctAnswers.size();
      singlePart = Float.parseFloat(df.format(singlePart));
      for (int i = 0; i < correctAnswers.size(); i++)
      {
         singleDataArray = new JSONArray();
         if (correctAnswers.get(i) == true)
         {
            //answer is correct
            singleDataArray.put("Question no. " + Integer.toString(i + 1) + "(Correct)");
            colorArray.put("#30AF70");
         }
         else
         {
            singleDataArray.put("Question no. " + Integer.toString(i + 1) + "(Incorrect)");
            colorArray.put("#FF0404");
         }
         singleDataArray.put((int) singlePart);
         oneDataArray.put(singleDataArray);
      }

      //      GenericChartData dataData = new GenericChartData();
      //      dataData.getText().add("data");
      //      dataData.getValues().add(oneDataArray);
      //      dataData.prepareResponse();
      //      jsonArray.put(dataData);
      typeData.getText().add("data");
      typeData.getValues().add(oneDataArray);
      typeData.prepareResponse();
      jsonArray.put(typeData);

      this.put("title", title);
      this.put("colors", colorArray);
      this.put("series", jsonArray);
   }


   public ArrayList<Boolean> getCorrectAnswers()
   {
      return correctAnswers;
   }


   public void setCorrectAnswers(ArrayList<Boolean> correctAnswers)
   {
      this.correctAnswers = correctAnswers;
   }

   public static void main(String[] args)
   {
      DecimalFormat df = new DecimalFormat("#.##");
      df.setRoundingMode(RoundingMode.HALF_EVEN);
      float singlePart = 100f / (float) 8;
      System.out.println(Float.parseFloat(df.format(singlePart)));
   }
}
