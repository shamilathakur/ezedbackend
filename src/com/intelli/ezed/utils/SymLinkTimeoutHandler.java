package com.intelli.ezed.utils;

import org.sabre.utils.map.TimedMap.MapTimeoutHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.session.SessionManager;
import com.intelli.ezed.bulkupload.data.ActiveSessionList;
import com.intelli.ezed.data.EzedLoggerName;

public class SymLinkTimeoutHandler implements MapTimeoutHandler<String, Object>
{
   public static SessionManager sessionManager;
   public static SymLinkManager symLinkManager;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   @Override
   public void handleTimeout(String persistId, Object symLink)
   {
      if (logger.isTraceEnabled())
      {
         logger.trace("Persist id : " + persistId + " timed out");
      }
      if (logger.isDebugEnabled())
      {
         logger.debug("Persist id : " + persistId + " timed out");
      }
      SymLinkObject symLinkObject = (SymLinkObject) symLink;
      String sessionId = symLinkObject.getSessionId();
      String contentId = symLinkObject.getContentId();

      try
      {
         //                  sessionManager.checkDataFromSession(sessionId, new HashSet<String>());
         if (sessionId != null)
         {
            if (ActiveSessionList.checkIfSessionActive(sessionId))
            {
               if (logger.isTraceEnabled())
               {
                  logger.trace("Session " + sessionId + " is still active. Renewing symlink for short timeout");
               }
               if (logger.isDebugEnabled())
               {
                  logger.debug("Session " + sessionId + " is still active. Renewing symlink for short timeout");
               }
               symLinkManager.addSymLinkShortTimeout(sessionId, contentId, symLinkObject.getContentLink(), logger);
            }
            else
            {
               if (logger.isTraceEnabled())
               {
                  logger.trace("Session id " + sessionId + " is expired. Removing sym link");
               }
               if (logger.isDebugEnabled())
               {
                  logger.debug("Session id " + sessionId + " is expired. Removing sym link");
               }
               symLinkManager.removeSymLink(sessionId, contentId, logger);
            }
         }
         //adding for testing
         //         symLinkManager.removeSymLink(sessionId, contentId, logger);
      }
      //      catch (SessionException e)
      //      {
      //         if (logger.isTraceEnabled())
      //         {
      //            logger.trace("Session id " + sessionId + " is expired. Removing sym link", e);
      //         }
      //         if (logger.isDebugEnabled())
      //         {
      //            logger.debug("Session id " + sessionId + " is expired. Removing sym link", e);
      //         }
      //         symLinkManager.removeSymLink(sessionId, contentId, logger);
      //      }
      //      catch (NumberFormatException e)
      //      {
      //         logger.error("Error while renewing sym link", e);
      //      }
      //      catch (IllegalBlockSizeException e)
      //      {
      //         logger.error("Error while renewing sym link", e);
      //      }
      //      catch (BadPaddingException e)
      //      {
      //         logger.error("Error while renewing sym link", e);
      //      }
      catch (Exception e)
      {
         logger.error("Error while renewing sym link", e);
      }
   }
}
