package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.UserProfile;

public class UserProfileUpdater extends SingletonRule
{
   private String dbConnName;
   private String defaultCityId;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public UserProfileUpdater(String ruleName, String dbConnName, String defaultCityId)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
      this.defaultCityId = defaultCityId;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      UserProfile userProfile = (UserProfile) flowContext.getFromContext(EzedKeyConstants.UPDATED_USER_PROFILE);
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      String userId = (String) flowContext.getFromContext(EzedKeyConstants.USER_ID);
      Boolean isVerifCompletedFlag = (Boolean) flowContext.getFromContext(EzedKeyConstants.USER_VERIF_COMPLETED);

      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         //update users set mobilenumber=?, firstname=?, middlename=?, lastname=?, gender=?, dob=?, streetaddress=?, landmark=?, pincode=?, univname=?, collegename=?, cityid = (select cityid from citylist where cityname=? and statename=?' and countryname=?) where userid=?
         if (userProfile.getCity() != null)
         {
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdateUserMyProfile");
         }
         else
         {
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdateUserMyProfileDefaultCity");
         }
         ps.setString(1, userProfile.getMobileNumber());
         ps.setString(2, userProfile.getFirstName());
         ps.setString(3, userProfile.getMiddleName());
         ps.setString(4, userProfile.getLastName());
         ps.setString(5, userProfile.getGender());
         ps.setString(6, userProfile.getDateOfBirth());
         ps.setString(7, userProfile.getStreetAdress());
         ps.setString(8, userProfile.getLandmark());
         ps.setLong(9, userProfile.getPincode() == null ? 0l : Long.parseLong(userProfile.getPincode()));
         ps.setString(10, userProfile.getUnivName());
         ps.setString(11, userProfile.getCollegeName());
         ps.setString(12, userProfile.getProfileImage());
         if (userProfile.getCity() != null)
         {
            ps.setString(13, userProfile.getCity());
            ps.setString(14, userProfile.getState());
            ps.setString(15, userProfile.getCountry());
            ps.setString(16, userProfile.getDegree());
            ps.setString(17, userProfile.getStream());
            ps.setString(18, userId);
         }
         else
         {
            ps.setString(13, defaultCityId);
            ps.setString(14, userProfile.getDegree());
            ps.setString(15, userProfile.getStream());
            ps.setString(16, userId);
         }
         int rows = ps.executeUpdate();
         if (rows > 0)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug(rows + " rows updated in db while updating user profile for userid " + userId);
            }
            response.setParameters(EzedTransactionCodes.SUCCESS, "Profile updated successfully");
            if (isVerifCompletedFlag)
            {
               if (logger.isDebugEnabled())
               {
                  logger.debug("User has completed his verification");
               }
               return RuleDecisionKey.IF_PROFILECOMPLETE;
            }
            return RuleDecisionKey.ON_SUCCESS;
         }
         else
         {
            logger.error("Profile could not be completed for user " + userId);
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "User profile could not be updated");
            return RuleDecisionKey.ON_FAILURE;
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while updating my profile in db for user " + userId, e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error updating user profile");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while updating my profile in db for user " + userId, e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error updating user profile");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
}
