package com.intelli.ezed.data;

public class CoursePercentagePointData extends ResponseObject
{
   private String courseId;
   private String courseName;
   private String points;
   private String completionPercentage;
   private String courseImage;

   @Override
   public void prepareResponse() throws Exception
   {
      this.put("courseid", getCourseId());
      this.put("coursename", getCourseName());
      this.put("points", getPoints());
      this.put("percentage", getCompletionPercentage());
      this.put("courseimage", getCourseImage());
   }

   public String getCourseId()
   {
      return courseId;
   }

   public void setCourseId(String courseId)
   {
      this.courseId = courseId;
   }

   public String getCourseName()
   {
      return courseName;
   }

   public void setCourseName(String courseName)
   {
      this.courseName = courseName;
   }

   public String getPoints()
   {
      return points;
   }

   public void setPoints(String points)
   {
      this.points = points;
   }

   public String getCompletionPercentage()
   {
      return completionPercentage;
   }

   public void setCompletionPercentage(String completionPercentage)
   {
      this.completionPercentage = completionPercentage;
   }

   public String getCourseImage()
   {
      return courseImage;
   }

   public void setCourseImage(String courseImage)
   {
      this.courseImage = courseImage;
   }

}
