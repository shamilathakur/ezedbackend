package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class FriendAlreadyInvitedChecker extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public FriendAlreadyInvitedChecker(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      String userid = (String) flowContext.getFromContext(EzedKeyConstants.USER_ID);
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      String friendId = (String) flowContext.getFromContext(EzedKeyConstants.INVITED_FRIEND);
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      PreparedStatement ps = null;
      ResultSet rs = null;

      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "GetInvitedFriend");
         ps.setString(1, userid);
         ps.setString(2, friendId);
         rs = ps.executeQuery();
         if (rs.next())
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Userid " + userid + " has already invited friend " + friendId);
            }
            response.setParameters(EzedTransactionCodes.FRIEND_ALREADY_INVITED, "Friend already invited");
            return RuleDecisionKey.ON_FAILURE;
         }
         return RuleDecisionKey.ON_SUCCESS;
      }
      catch (SQLException e)
      {
         logger.error("Error while checking if user has already invited friend", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while checking if friend has already been invited");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while checking if user has already invited friend", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while checking if friend has already been invited");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing result set while checking if friend has already been invited", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
}
