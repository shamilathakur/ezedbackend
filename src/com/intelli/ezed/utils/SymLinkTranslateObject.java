package com.intelli.ezed.utils;

import java.util.HashMap;
import java.util.Map;

public class SymLinkTranslateObject
{
   private Map<String, Map<String, SymLinkObject>> symLinkMap = new HashMap<String, Map<String, SymLinkObject>>();

   public SymLinkObject getExistingSymLinkPresent(String sessionId, String contentId)
   {
      if (sessionId == null || contentId == null)
      {
         return null;
      }
      Map<String, SymLinkObject> sessionSymLinkMap = symLinkMap.get(sessionId);
      if (sessionSymLinkMap == null)
      {
         return null;
      }

      SymLinkObject contentSymLink = sessionSymLinkMap.get(contentId);
      return contentSymLink;
   }

   public SymLinkObject putSymLink(String sessionId, String contentId, String origLink, String newLink, String fileName) throws Exception
   {
      if (sessionId == null || contentId == null || origLink == null || newLink == null || fileName == null)
      {
         return null;
      }

      Map<String, SymLinkObject> sessionSymLinkMap = symLinkMap.get(sessionId);
      if (sessionSymLinkMap == null)
      {
         sessionSymLinkMap = new HashMap<String, SymLinkObject>();
         symLinkMap.put(sessionId, sessionSymLinkMap);
      }

      SymLinkObject contentSymLink = new SymLinkObject();
      contentSymLink.setContentId(contentId);
      contentSymLink.setContentLink(origLink);
      contentSymLink.setTranslatedContentLink(newLink);
      contentSymLink.setSessionId(sessionId);
      contentSymLink.setFileName(fileName);
      sessionSymLinkMap.put(contentId, contentSymLink);
      return contentSymLink;
   }

   public SymLinkObject removeSymlinkForSession(String sessionId, String contentId)
   {
      if (sessionId == null || contentId == null)
      {
         return null;
      }

      Map<String, SymLinkObject> sessionSymLinkMap = symLinkMap.get(sessionId);
      if (sessionSymLinkMap == null)
      {
         return null;
      }

      return sessionSymLinkMap.remove(contentId);
   }

   public boolean removeAllSymLinksForSession(String sessionId)
   {
      if (sessionId == null)
      {
         return false;
      }

      symLinkMap.remove(sessionId);
      return true;
   }
}
