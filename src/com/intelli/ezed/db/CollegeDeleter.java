package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.AdminProfile;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.utils.MasterDeleteUpdateHelper;

public class CollegeDeleter extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public CollegeDeleter(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      AdminProfile adminProfile = (AdminProfile) flowContext.getFromContext(EzedKeyConstants.ADMIN_PROFILE);
      String userId = adminProfile.getUserid();

      String college = messageData.getParameter("college");
      if (college == null || college.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("College name is invalid " + college);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "college name is invalid");
         return RuleDecisionKey.ON_FAILURE;
      }

      String univ = messageData.getParameter("university");
      if (univ == null || univ.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("University name is invalid " + univ);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "university name is invalid");
         return RuleDecisionKey.ON_FAILURE;
      }

      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         MasterDeleteUpdateHelper.insertDeletedCollege(dbConnName, logger, univ, college, userId);
         MasterDeleteUpdateHelper.insertUpdatedUsersForCollege(dbConnName, logger, univ, college, userId);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "DeleteCollege");
         ps.setString(1, college);
         ps.setString(2, univ);
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows deleted from db while deleting college " + college);
         }
         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
         return RuleDecisionKey.ON_SUCCESS;
      }
      catch (SQLException e)
      {
         logger.error("Error while deleting college from db " + college, e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "DB error while deleting college");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while deleting college from db " + college, e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while deleting college");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
}
