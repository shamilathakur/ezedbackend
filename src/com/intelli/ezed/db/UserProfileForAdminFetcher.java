package com.intelli.ezed.db;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.UserProfileForAdmin;

public class UserProfileForAdminFetcher extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public UserProfileForAdminFetcher(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      String userId = messageData.getParameter("userid");
      if (userId == null || userId.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Userid in request is invalid " + userId);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid userid in request");
         return RuleDecisionKey.ON_FAILURE;
      }

      UserProfileForAdmin profile = new UserProfileForAdmin();
      profile.setUserid(userId);
      try
      {
         profile.populateUserProfile(dbConnName, logger);
      }
      catch (Exception e)
      {
         logger.error("Error while fetching user profile", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while populating user profile from db");
         return RuleDecisionKey.ON_FAILURE;
      }
      response.setResponseObject(profile);
      response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
      return RuleDecisionKey.ON_SUCCESS;
   }
}
