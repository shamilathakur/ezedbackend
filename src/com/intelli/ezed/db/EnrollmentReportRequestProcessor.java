/*package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EnrollmentReportRequestData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.utils.EzedHelper;

public class EnrollmentReportRequestProcessor extends SingletonRule {

	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

	String dbConnName;

	public EnrollmentReportRequestProcessor(String ruleName, String dbConnName) {
		super(ruleName);
		this.dbConnName = dbConnName;
	}

	@Override
	public RuleDecisionKey executeRule(FlowContext flowContext) {

		Response response = (Response) flowContext
				.getFromContext(EzedKeyConstants.EZED_RESPONSE);
		Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
		PreparedStatement ps = null;
		ResultSet rs = null;

		EnrollmentReportRequestData data = (EnrollmentReportRequestData) flowContext
				.getFromContext(EzedKeyConstants.ENROLLMENT_DATA);

		String dynaQuery = "SELECT users.firstname || users.lastname as studentname,users.userid,to_char(to_date(substring(users.createtime,0,9), 'yyyymmdd'),'yyyy-mm-dd') as registered_date,to_char(to_date(substring(usercourse.startdate,0,9), 'yyyymmdd'),'yyyy-mm-dd') as enrolled_date,courselist.coursename,courselist.coursetype,univname,collegename,citylist.cityname FROM usercourse LEFT JOIN users ON usercourse.userid = users.userid LEFT JOIN courselist on usercourse.courseid=courselist.courseid INNER JOIN citylist on users.cityid=citylist.cityid";
		String dynaQueryFromBackup = "SELECT users.firstname || users.lastname as studentname,users.userid,to_char(to_date(substring(users.createtime,0,9), 'yyyymmdd'),'yyyy-mm-dd') as registered_date,to_char(to_date(substring(usercoursebackup.startdate,0,9), 'yyyymmdd'),'yyyy-mm-dd') as enrolled_date,courselist.coursename,univname,collegename,citylist.cityname FROM usercoursebackup LEFT JOIN users ON usercoursebackup.userid = users.userid LEFT JOIN courselist on usercoursebackup.courseid=courselist.courseid INNER JOIN citylist on users.cityid=citylist.cityid";
		String finalQuery = "";
		String date = data.getStartdate();
		String enddate = data.getEnddate();
		String month = data.getMonth();
		String endmonth = data.getEndmonth();

		String univname = data.getUniversity();
		String college = data.getCollege();
		String cityid = data.getCity();

		System.out.println("Data : date - " + data.getStartdate()
				+ " enddate - " + data.getEnddate() + " month - "
				+ data.getMonth() + " endmonth - " + data.getEndmonth()
				+ " univname - " + data.getUniversity() + " college - "
				+ data.getCollege() + "cityid - " + data.getCity());

		String qryDate = "";
		String qrycity = "";
		String qryUniv = "";
		String qryCollege = "";

		String yearMonth = EzedHelper.getMonthYear();
		String yearMonthDate = EzedHelper.getDateMonthYear();

		if (date != null && date.trim() != "") {
			if (enddate != null && enddate.trim() != "") {
				enddate = yearMonthDate;
			}
			qryDate = " AND substring(usercourse.startdate,0,9) between '"
					+ date.substring(0, 8) + "' and '"
					+ enddate.substring(0, 8) + "'";
		} 
		if (month != null && month.trim() != "") {
			if (endmonth != null && endmonth.trim() != "") {
				endmonth = yearMonth;
			}
			qryDate = " AND substring(usercourse.startdate,0,7) between '"
					+ month + "' and '" + endmonth + "'";
		}

		if (univname != null && univname.trim() != "") {
			qryUniv = " AND users.univname ='" + univname + "'";

		}

		if (college != null && college.trim() != "") {
			qryCollege = " AND users.collegename ='"
					+ college.replaceAll("'", "''") + "'";
		}

		if (cityid != null && cityid.trim() != ""
				&& !cityid.equalsIgnoreCase("default")) {
			qrycity = " AND users.cityid ='" + cityid + "'";
		}

		dynaQuery = dynaQuery + qryUniv + qrycity + qryDate + qryCollege;

		String qryDate1 = "";

		if (date != null && date.trim() != "") {
			if (enddate != null && enddate.trim() != "") {
				enddate = yearMonthDate;
			}
			qryDate1 = " AND substring(usercoursebackup.startdate,0,9) between '"
					+ date.substring(0, 8)
					+ "' and '"
					+ enddate.substring(0, 8) + "'";
		}

		if (month != null && month.trim() != "") {
			if (endmonth != null && endmonth.trim() != "") {
				endmonth = yearMonth;
			}
			qryDate1 = " AND substring(usercoursebackup.startdate,0,7) between '"
					+ month + "' and '" + endmonth + "'";
		}

		dynaQueryFromBackup = dynaQueryFromBackup + qryUniv + qrycity
				+ qryDate1 + qryCollege;

		finalQuery = dynaQuery + " UNION ALL " + dynaQueryFromBackup;
		try {
			ps = conn.prepareStatement(finalQuery);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// ps = DbConnectionManager.getPreparedStatement(dbConnName, conn,
		// dynaQuery);
		try {
			System.out.println(ps);
			rs = ps.executeQuery();
			int count = 0;
			while (rs.next()) {
				count++;
			}
			System.out
					.println("Rows are received ......................................... : "
							+ count);
			response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");

		} catch (Exception e) {

		}
		return RuleDecisionKey.ON_SUCCESS;
	}
}*/