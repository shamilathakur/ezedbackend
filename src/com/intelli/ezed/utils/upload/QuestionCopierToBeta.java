package com.intelli.ezed.utils.upload;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class QuestionCopierToBeta
{

   public static void main(String[] args)
   {
      Connection liveConn = null;
      Connection betaConn = null;
      ResultSet rs = null;
      PreparedStatement livePs = null;
      PreparedStatement betaPs = null;
      try
      {
         liveConn = getDbConnectionLivePostgres();
         betaConn = getDbConnectionBetaPostgres();
         livePs = getSelectLivePostStatement(liveConn);
         rs = livePs.executeQuery();
         while (rs.next())
         {
            String link = rs.getString("link");
            String contentid = rs.getString("contentid");
            betaPs = getUpdateBetaPostStatement(betaConn);
            betaPs.setString(1, link);
            betaPs.setString(2, contentid);
            int rows = betaPs.executeUpdate();
            System.out.println(contentid + " " + rows);
         }
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      finally
      {
         try
         {
            rs.close();
         }
         catch (SQLException e)
         {
            e.printStackTrace();
         }
         try
         {
            livePs.close();
         }
         catch (SQLException e)
         {
            e.printStackTrace();
         }
         try
         {
            betaPs.close();
         }
         catch (SQLException e)
         {
            e.printStackTrace();
         }
         try
         {
            liveConn.close();
         }
         catch (SQLException e)
         {
            e.printStackTrace();
         }
         try
         {
            betaConn.close();
         }
         catch (SQLException e)
         {
            e.printStackTrace();
         }
      }
   }


   private static PreparedStatement getSelectLivePostStatement(Connection conn)
   {
      try
      {
         return (conn.prepareStatement("select * from contentlist where contentid in (select ans6content from questionbank where courseid='COU20000000137935724' and ans6type=2)"));
      }
      catch (Exception e)
      {
         System.out.println("Error initializing postgres statment");
         System.exit(1);
         return null;
      }
   }

   private static PreparedStatement getUpdateBetaPostStatement(Connection conn)
   {
      try
      {
         return (conn.prepareStatement("update contentlist set link = ? where contentid=?"));
      }
      catch (Exception e)
      {
         System.out.println("Error initializing postgres statment");
         System.exit(1);
         return null;
      }
   }

   private static Connection getDbConnectionLivePostgres()
   {
      try
      {
         String url = "jdbc:postgresql://localhost:15432/ezed";
         Class.forName("org.postgresql.Driver");
         return DriverManager.getConnection(url, "postgres", "postgres");
      }
      catch (Exception e)
      {
         e.printStackTrace();
         System.out.println("Error initialising Postgres connection");
         System.exit(1);
         return null;
      }
   }

   private static Connection getDbConnectionBetaPostgres()
   {
      try
      {
         String url = "jdbc:postgresql://localhost:25432/ezedlive";
         Class.forName("org.postgresql.Driver");
         return DriverManager.getConnection(url, "postgres", "postgres");
      }
      catch (Exception e)
      {
         e.printStackTrace();
         System.out.println("Error initialising Postgres connection");
         System.exit(1);
         return null;
      }
   }

}
