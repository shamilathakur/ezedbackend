package com.intelli.ezed.utils;

public class AdminRoles
{
   public static final int SUPER_ADMIN = 1;
   public static final int GROUP_ADMIN = 2;
   public static final int COURSE_ADMIN = 3;
   public static final int FORUM_ADMIN = 4;
   public static final int REPORT_ADMIN = 5;
   public static final int TEST_ADMIN = 6;
   public static final int SOCIAL_ADMIN = 7;
   public static final int ACCOUNT_ADMIN = 8;
   public static final int STUDENT_ADMIN = 9;
   public static final int MIC_ADMIN = 10;
}
