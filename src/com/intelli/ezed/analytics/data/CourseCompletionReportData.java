package com.intelli.ezed.analytics.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.sabre.db.DbConnectionManager;
import org.slf4j.Logger;

import com.intelli.ezed.bulkupload.data.ContentDetails;
import com.intelli.ezed.data.ResponseObject;

public class CourseCompletionReportData extends ResponseObject
{
   private String userId;
   private String courseId;
   private ArrayList<String> testIdList = new ArrayList<String>();
   private int totalNotes = 0;
   private int totalAudios = 0;
   private int totalVideos = 0;
   private int totalTests = 0;
   private int notesCompleted = 0;
   private int audiosCompleted = 0;
   private int videosCompleted = 0;
   private int totalTestsAttempted = 0;
   private int totalTestsCompleted = 0;
   private int uniqueTestsAttempted = 0;
   private int uniqueTestsCompleted = 0;
   private int totalQuiz = 0;
   private int quizzesCompleted = 0;
   private TotalCourseCountData totalCourseCountData = null;
   private CompletedCourseCountData completedCourseCountData = null;
   private GenericChartData title = new GenericChartData();
   private GenericChartData xAxis = new GenericChartData();
   private GenericChartData yAxis = new GenericChartData();
   private GenericChartData type = new GenericChartData();

   public void prepareChart() throws Exception
   {
      type.getText().add("type");
      type.getValues().add("bar");
      type.prepareResponse();

      title.getText().add("text");
      title.getValues().add("Course Completion Report");
      title.prepareResponse();

      xAxis.getText().add("categories");
      String[] cats = {"Notes", "Videos", "Audios", "Total Tests", "Attempted Tests", "Unique Attempted Tests", "Quizzes"};
      xAxis.getValues().add(cats);
      xAxis.getText().add("title");
      GenericChartData xAxisText = new GenericChartData();
      xAxisText.getText().add("text");
      xAxisText.getValues().add("Course contents");
      xAxis.getValues().add(xAxisText);
      xAxisText.prepareResponse();
      xAxis.prepareResponse();

      yAxis.getText().add("min");
      yAxis.getValues().add(0);
      yAxis.getText().add("title");
      GenericChartData yAxisText = new GenericChartData();
      yAxisText.getText().add("text");
      yAxisText.getValues().add("Total number of Views");
      yAxis.getValues().add(yAxisText);

      yAxisText.prepareResponse();
      yAxis.prepareResponse();
   }

   //new TotalCourseCountData(getTotalNotes(), getTotalAudios(), getTotalVideos(), getTotalTests(), getTotalTestsAttempted(), getUniqueTestsAttempted(), getTotalQuiz());


   public int getUniqueTestsAttempted()
   {
      return uniqueTestsAttempted;
   }

   public void setUniqueTestsAttempted(int uniqueTestsAttempted)
   {
      this.uniqueTestsAttempted = uniqueTestsAttempted;
   }

   public int getUniqueTestsCompleted()
   {
      return uniqueTestsCompleted;
   }

   public void setUniqueTestsCompleted(int uniqueTestsCompleted)
   {
      this.uniqueTestsCompleted = uniqueTestsCompleted;
   }

   public String getUserId()
   {
      return userId;
   }

   public void setUserId(String userId)
   {
      this.userId = userId;
   }

   public int getTotalQuiz()
   {
      return totalQuiz;
   }

   public void setTotalQuiz(int totalQuiz)
   {
      this.totalQuiz = totalQuiz;
   }

   public int getQuizzesCompleted()
   {
      return quizzesCompleted;
   }

   public void setQuizzesCompleted(int quizzesCompleted)
   {
      this.quizzesCompleted = quizzesCompleted;
   }

   public String getCourseId()
   {
      return courseId;
   }

   public void setCourseId(String courseId)
   {
      this.courseId = courseId;
   }

   public int getTotalNotes()
   {
      return totalNotes;
   }

   public void setTotalNotes(int totalNotes)
   {
      this.totalNotes = totalNotes;
   }

   public int getTotalAudios()
   {
      return totalAudios;
   }

   public void setTotalAudios(int totalAudios)
   {
      this.totalAudios = totalAudios;
   }

   public int getTotalVideos()
   {
      return totalVideos;
   }

   public void setTotalVideos(int totalVideos)
   {
      this.totalVideos = totalVideos;
   }

   public int getTotalTests()
   {
      return totalTests;
   }

   public void setTotalTests(int totalTests)
   {
      this.totalTests = totalTests;
   }

   public int getNotesCompleted()
   {
      return notesCompleted;
   }

   public void setNotesCompleted(int notesCompleted)
   {
      this.notesCompleted = notesCompleted;
   }

   public int getAudiosCompleted()
   {
      return audiosCompleted;
   }

   public void setAudiosCompleted(int audiosCompleted)
   {
      this.audiosCompleted = audiosCompleted;
   }

   public int getVideosCompleted()
   {
      return videosCompleted;
   }

   public void setVideosCompleted(int videosCompleted)
   {
      this.videosCompleted = videosCompleted;
   }

   public int getTotalTestsAttempted()
   {
      return totalTestsAttempted;
   }

   public void setTotalTestsAttempted(int totalTestsAttempted)
   {
      this.totalTestsAttempted = totalTestsAttempted;
   }

   public int getTotalTestsCompleted()
   {
      return totalTestsCompleted;
   }

   public void setTotalTestsCompleted(int totalTestsCompleted)
   {
      this.totalTestsCompleted = totalTestsCompleted;
   }

   public void populateCompletionReport(String dbConnName, Logger logger) throws SQLException, Exception
   {
      fetchCourseContentCount(dbConnName, logger);
      fetchCourseTests(dbConnName, logger);
      fetchTestSummary(dbConnName, logger);
      this.totalCourseCountData = new TotalCourseCountData(getTotalNotes() - getNotesCompleted(), getTotalAudios() - getAudiosCompleted(), getTotalVideos() - getVideosCompleted(), getTotalTests() - getUniqueTestsCompleted(), getTotalTestsAttempted() - getTotalTestsCompleted(), getUniqueTestsAttempted() - getUniqueTestsCompleted(), getTotalQuiz() - getQuizzesCompleted());
      this.completedCourseCountData = new CompletedCourseCountData(getNotesCompleted(), getAudiosCompleted(), getVideosCompleted(), getTotalTestsCompleted(), getUniqueTestsCompleted(), getQuizzesCompleted());
   }

   private void fetchTestSummary(String dbConnName, Logger logger) throws SQLException, Exception
   {
      //select testcompleted from testsummery where testid=? and userid=?
      Connection conn = null;
      PreparedStatement ps = null;
      ResultSet rs = null;
      int isTestCompleted = 0;
      boolean isUniqueTestAttemptCounted = false;
      boolean isUniqueTestCompletionCounted = false;
      for (String testId : testIdList)
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         isTestCompleted = 0;
         isUniqueTestAttemptCounted = false;
         isUniqueTestCompletionCounted = false;
         try
         {
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchTestSummary");
            ps.setString(1, testId);
            ps.setString(2, getUserId());
            rs = ps.executeQuery();
            while (rs.next())
            {
               //existence of row means test was attempted
               if (!isUniqueTestAttemptCounted)
               {
                  this.setUniqueTestsAttempted(getUniqueTestsAttempted() + 1);
                  isUniqueTestAttemptCounted = true;
               }
               this.setTotalTestsAttempted(getTotalTestsAttempted() + 1);

               isTestCompleted = rs.getInt("testcompleted");
               if (isTestCompleted != 0)
               {
                  if (!isUniqueTestCompletionCounted)
                  {
                     this.setUniqueTestsCompleted(getUniqueTestsCompleted() + 1);
                     isUniqueTestCompletionCounted = true;
                  }
                  this.setTotalTestsCompleted(getTotalTestsCompleted() + 1);
               }
            }
         }
         catch (SQLException e)
         {
            logger.error("DB Error while fetching test summary for user course content analytics", e);
            throw e;
         }
         catch (Exception e)
         {
            logger.error("Error while fetching test summary for user course content analytics", e);
            throw e;
         }
         finally
         {
            if (rs != null)
            {
               try
               {
                  rs.close();
               }
               catch (Exception e)
               {
                  logger.error("Error while closing result set while fetching test summary for user analytics");
               }
            }
            DbConnectionManager.releaseConnection(dbConnName, conn);
         }
      }
   }

   private void fetchCourseTests(String dbConnName, Logger logger) throws SQLException, Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchTestsCourseOnly");
         ps.setString(1, getCourseId());
         rs = ps.executeQuery();
         while (rs.next())
         {
            this.testIdList.add(rs.getString("testid"));
            this.setTotalTests(getTotalTests() + 1);
         }
      }
      catch (SQLException e)
      {
         logger.error("DB Error while fetching tests for user course content analytics", e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching tests for user course content analytics", e);
         throw e;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
               logger.error("Error while closing result set while fetching tests for user analytics");
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   private void fetchCourseContentCount(String dbConnName, Logger logger) throws SQLException, Exception
   {
      //select up.contentid,count,contenttype from userpoints up, contentlist c where userid=? and courseid=? and up.contentid = c.contentid
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchCourseContentCount");
         ps.setString(1, getUserId());
         ps.setString(2, getCourseId());
         rs = ps.executeQuery();
         int contentType = 0;
         int count = 0;
         while (rs.next())
         {
            contentType = rs.getInt("contenttype");
            count = rs.getInt("count");
            if (contentType == ContentDetails.CONTENT_NOTE || contentType == ContentDetails.CONTENT_PDF || contentType == ContentDetails.CONTENT_IMAGE)
            {
               setTotalNotes(getTotalNotes() + 1);
               if (count > 0)
               {
                  setNotesCompleted(getNotesCompleted() + 1);
               }
               //               setNotesCompleted(getNotesCompleted() + rs.getInt("count"));
            }
            else if (contentType == ContentDetails.CONTENT_AUDIO)
            {
               setTotalAudios(getTotalAudios() + 1);
               if (count > 0)
               {
                  setAudiosCompleted(getAudiosCompleted() + 1);
               }
               //               setAudiosCompleted(getAudiosCompleted() + rs.getInt("count"));
            }
            else if (contentType == ContentDetails.CONTENT_VIDEO || contentType == ContentDetails.CONTENT_YOUTUBE)
            {
               setTotalVideos(getTotalVideos() + 1);
               if (count > 0)
               {
                  setVideosCompleted(getVideosCompleted() + 1);
               }
               //               setVideosCompleted(getVideosCompleted() + rs.getInt("count"));
            }
            else if (contentType == ContentDetails.CONTENT_QUIZ)
            {
               setTotalQuiz(getTotalQuiz() + 1);
               if (count > 0)
               {
                  setQuizzesCompleted(getQuizzesCompleted() + 1);
               }
               //               setQuizzesCompleted(getQuizzesCompleted() + rs.getInt("count"));
            }
            else if (contentType == ContentDetails.CONTENT_TEST || contentType == ContentDetails.CONTENT_TEST_1)
            {
               setTotalTests(getTotalTests() + 1);
               this.testIdList.add(rs.getString("contentid"));
            }
         }
      }
      catch (SQLException e)
      {
         logger.error("DB Error while fetching user course content analytics", e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching user course content analytics", e);
         throw e;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
               logger.error("Error while closing result set while fetching user analytics");
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   @Override
   public void prepareResponse() throws Exception
   {
      //      this.put("totalnotes", getTotalNotes());
      //      this.put("totalaudios", getTotalAudios());
      //      this.put("totalvideos", getTotalVideos());
      //      this.put("totalquiz", getTotalQuiz());
      //      this.put("notescompleted", getNotesCompleted());
      //      this.put("audioscompleted", getAudiosCompleted());
      //      this.put("videoscompleted", getVideosCompleted());
      //      this.put("quizzescompleted", getQuizzesCompleted());
      //      this.put("totaltests", getTotalTests());
      //      this.put("totaltestattempts", getTotalTestsAttempted());
      //      this.put("totaltestscompleted", getTotalTestsCompleted());
      //      this.put("totaluniquetestattempts", getUniqueTestsAttempted());
      //      this.put("totaluniquetestcompleted", getUniqueTestsCompleted());
      this.put("title", title);
      this.put("chart", type);
      this.put("xAxis", xAxis);
      this.put("yAxis", yAxis);
      JSONArray jsonArray = new JSONArray();
      this.totalCourseCountData.prepareResponse();
      this.completedCourseCountData.prepareResponse();
      jsonArray.put(this.totalCourseCountData);
      jsonArray.put(this.completedCourseCountData);
      this.put("series", jsonArray);
   }
}
