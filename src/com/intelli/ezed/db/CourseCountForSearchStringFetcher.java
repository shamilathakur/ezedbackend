package com.intelli.ezed.db;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class CourseCountForSearchStringFetcher extends SingletonRule{
	
	public CourseCountForSearchStringFetcher(String ruleName ,String dbConnName) {
		super(ruleName);
		this.dbConnName = dbConnName;
	}
	private String dbConnName;
    private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
	@Override
	public RuleDecisionKey executeRule(FlowContext flowContext) {
		 Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
	     MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
	     
	     final String query = 
					
	    		" SELECT "
	    		 +" count(distinct cl.courseid) as totalcount "
	    		 +" from courselist cl , coursepath cp "
	    		  +"  where cl.publishflag =1  "
	    		 +"   and cl.courseid = cp.courseid "
					+ "and (cl.coursename ~* '"+messageData.getParameter("searchString")+"'"
					 +" OR cp.chapterid in  "
					 + "( select chapterid from chapterlist where chaptername  ~* '"+messageData.getParameter("searchString")+"'))";
			
			
		
			
			final String queryForAdmin = 
					" SELECT "
				    		 +" count(distinct cl.courseid) as totalcount "
				    		 +" from courselist cl , coursepath cp "
				    		 +"   and cl.courseid = cp.courseid "
								+ "and (cl.coursename ~* '"+messageData.getParameter("searchString")+"'"
								 +" OR cp.chapterid in  "
								 + "( select chapterid from chapterlist where chaptername  ~* '"+messageData.getParameter("searchString")+"'))";
						
			
			StringBuilder sb = new StringBuilder("");
			System.out.println("String buffer : " + sb.toString());
			String finalQuery = null;
		     String requesttype = messageData.getParameter("requesttype") ;

			
		    if(requesttype !=null && requesttype.equalsIgnoreCase("admin")){
				finalQuery = queryForAdmin;
			}
			else{
				finalQuery = query;
			}
		     String userid = messageData.getParameter("email");

			if (userid != null){
				finalQuery = finalQuery + " and cl.courseid NOT IN ( select courseid from usercourse where userid = '" + userid + "') ";
			}
			if (!sb.equals("")) {
				finalQuery = finalQuery + sb.toString();
			}
			System.out.println("finalquery "+ finalQuery);
			Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
			PreparedStatement ps = null;
			ResultSet rs = null;
int totalCount =0;
			
			try {
					ps = conn.prepareStatement(finalQuery);
					System.out.println("Final Query : " + ps);
					rs = ps.executeQuery();
					while (rs.next()) {
						
			         	totalCount = rs.getInt("totalcount");
					}
				
					
			  }
			 catch (SQLException e)
	         {
	            logger.error("Error while fetching CoursesWithPaging", e);
	            response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "PL SQL Error while fetching CoursesWithPaging : " + e.getMessage());
	            return RuleDecisionKey.ON_FAILURE;
	         }
	         catch (Exception e)
	         {
	            logger.error("Error while fetching test details", e);
	            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching CoursesWithPaging : " + e.getMessage());
	            return RuleDecisionKey.ON_FAILURE;
	         }
	         finally
	         {
	            if (rs != null)
	            {
	               try
	               {
	                  rs.close();
	               }
	               catch (SQLException e)
	               {
	               }
	            }
	            DbConnectionManager.releaseConnection(dbConnName, conn);
	         }
			response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
			response.setResponseMessage(Integer.toString(totalCount));
		     
			 return RuleDecisionKey.ON_SUCCESS;
	}
	   

}
