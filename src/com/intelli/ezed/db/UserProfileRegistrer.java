package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.utils.RandomNumberGenerator;
import com.intelli.ezed.data.ActionData;
import com.intelli.ezed.data.AdminProfile;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.MasterActionRecords;
import com.intelli.ezed.data.RegistrationResponse;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.UserProfile;
import com.intelli.ezed.utils.Globals;
import com.intelli.ezed.utils.PasswordHelper;

public class UserProfileRegistrer extends SingletonRule
{
   private String dbConnName;
   private String defaultCityId;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
   private String actionId;

   public UserProfileRegistrer(String ruleName, String dbConnName, String defaultCityId, String actionId)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
      this.defaultCityId = defaultCityId;
      this.actionId = actionId;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      UserProfile userProfile = (UserProfile) flowContext.getFromContext(EzedKeyConstants.UPDATED_USER_PROFILE);
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      //String userId = (String) flowContext.getFromContext(EzedKeyConstants.USER_ID);
      Boolean isVerifCompletedFlag = (Boolean) flowContext.getFromContext(EzedKeyConstants.USER_VERIF_COMPLETED);
      AdminProfile adminProfile = (AdminProfile) flowContext.getFromContext(EzedKeyConstants.ADMIN_PROFILE);

      Connection connection = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;

      //Created verification key
      String registrationVerificationKey = RandomNumberGenerator.generateRandomHex(15);
      String defaultPass = null;
      if (userProfile.getPassword() == null)
      {
         defaultPass = RandomNumberGenerator.generateRandomHex(7);
         userProfile.setPassword(defaultPass);
      }
      else
      {
         defaultPass = userProfile.getPassword();
      }
      try
      {
         userProfile.setPassword(PasswordHelper.getEncryptedData(userProfile.getPassword()));
      }
      catch (IllegalBlockSizeException e2)
      {
         logger.error("Error while encrypting password", e2);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while handling password");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (BadPaddingException e2)
      {
         logger.error("Error while encrypting password", e2);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while handling password");
         return RuleDecisionKey.ON_FAILURE;
      }
      String userId = userProfile.getUserid();

      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, connection, "SelectCityIdBasedOnLocation");
         ps.setString(1, userProfile.getCountry());
         ps.setString(2, userProfile.getState());
         ps.setString(3, userProfile.getCity());
         rs = ps.executeQuery();
         if (rs.next())
         {
            userProfile.setCity(rs.getString("cityid"));
         }
         else
         {
            userProfile.setCity(null);
         }
      }
      catch (Exception e)
      {
         logger.error("Error while fetching city details from location. Setting default city in db", e);
         userProfile.setCity(null);
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
               logger.trace("Error while closing result set", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, connection);
      }

      ActionData actionData = MasterActionRecords.fetchRecord(actionId);
      try
      {
         //mobilenumber,firstname,middlename,lastname,gender,dob,streetaddress,
         //landmark,pincode,univname,collegename,cityid,userid,chkflag,password
         connection = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, connection, "RegisterUserProfile");
         ps.setString(1, userProfile.getMobileNumber());
         ps.setString(2, userProfile.getFirstName());
         ps.setString(3, userProfile.getMiddleName());
         ps.setString(4, userProfile.getLastName());
         ps.setString(5, userProfile.getGender());
         ps.setString(6, userProfile.getDateOfBirth());
         ps.setString(7, userProfile.getStreetAdress());
         ps.setString(8, userProfile.getLandmark());
         ps.setLong(9, userProfile.getPincode() == null ? 0l : Long.parseLong(userProfile.getPincode()));
         ps.setString(10, userProfile.getUnivName());
         ps.setString(11, userProfile.getCollegeName());
         ps.setString(12, userProfile.getCity() == null ? defaultCityId : userProfile.getCity());
         ps.setString(13, userId);
         if (Globals.isBox && isVerifCompletedFlag)
         {
            ps.setInt(14, 6); //Check flag is set to zero as user is being redirected to forgot mpin state.
         }
         else if (Globals.isBox && !isVerifCompletedFlag)
         {
            ps.setInt(14, 2);
         }
         else if (!Globals.isBox && isVerifCompletedFlag)
         {
            ps.setInt(14, 4);
         }
         else
         {
            ps.setInt(14, 0);
         }
         ps.setString(15, userProfile.getPassword());
         ps.setString(16, userProfile.getDegree());
         ps.setString(17, userProfile.getStream());
         ps.setString(18, adminProfile.getUserid());
         ps.setString(19, sdf.format(new Date()));
         ps.setString(20, userProfile.getProfileImage());
         ps.setInt(21, actionData.getPoints());
         int rows = ps.executeUpdate();

         if (rows > 0)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug(rows + " rows entered into database while registring userid : " + userId + " ");
            }
         }
         else
         {
            logger.error("Profile could not be completed for user " + userId);
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "User profile could not be updated");
            return RuleDecisionKey.ON_FAILURE;
         }
      }
      catch (SQLException e)
      {
         if (e.getMessage().contains("users_pkey1"))
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Userid already present in database.", e);
               response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Userid already present in database.");
               //CB001
               //Need to add some check that if userid already present but not registered then make some arrangement to send verification email again.
               return RuleDecisionKey.ON_FAILURE;
            }
         }
         logger.error("Error while registring my profile in db for user " + userId, e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error registring user profile");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while registring my profile in db for user " + userId, e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error registring user profile");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, connection);
      }
      
      Connection conn = null;
      String userRegStream = userProfile.getName() + " registered on EzEd";
      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertUserStreamAll");
         ps.setString(1, userRegStream);
         ps.setString(2, userProfile.getUserid());
         ps.setString(3, sdf.format(new Date()));
         ps.setString(4, null);
         ps.setString(5, null);
         ps.setString(6, null);
         ps.setString(7, actionId);
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in db while inserting stream for user " + userProfile.getUserid());
         }
      }
      catch (Exception e)
      {
         logger.error("Error inserting stream entry for " + userProfile.getUserid(), e);
         logger.error("INSERT FAILED " + userRegStream);
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }


      //Generating a Hex Key for verification
      RegistrationResponse regResponse = new RegistrationResponse();
      String requestInTime = (String) flowContext.getFromContext(EzedKeyConstants.REQUEST_IN_TIME);
      connection = DbConnectionManager.getConnectionByName(dbConnName);

      try
      {
         //insert into userverif (userid,verifid,intime) values (?,?,?)
         PreparedStatement insertIntoUserverif = DbConnectionManager.getPreparedStatement(dbConnName, connection, "InsertInUserverif");
         insertIntoUserverif.setString(1, userId);
         insertIntoUserverif.setString(2, registrationVerificationKey);
         insertIntoUserverif.setString(3, requestInTime);
         int i = insertIntoUserverif.executeUpdate();

         if (i == 0)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("No rows inserted into table. RegisterUSer.java");
            }
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal Error. Please try again.");
            return RuleDecisionKey.ON_FAILURE;
         }

      }
      catch (SQLException e)
      {
         logger.error("Database Exception.", e);
         if (e.getMessage().contains("userverif_uniq"))
         {
            logger.debug("Need to replace the verification key");
            try
            {
               //update userverif set verifid = ?, intime = ? where userid = ?
               PreparedStatement updateIntoUserverif = DbConnectionManager.getPreparedStatement(dbConnName, connection, "UpdateInUserverif");
               updateIntoUserverif.setString(1, registrationVerificationKey);
               updateIntoUserverif.setString(2, requestInTime);
               updateIntoUserverif.setString(3, userId);
               int j = updateIntoUserverif.executeUpdate();
               if (j == 0)
               {
                  if (logger.isDebugEnabled())
                  {
                     logger.debug("No rows inserted into table while updating userveirf table. RegisterUSer.java");
                  }
                  response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal Error. Please try again.");
                  return RuleDecisionKey.ON_FAILURE;
               }
            }
            catch (Exception e1)
            {
               logger.error("error", e1);
               response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Database exception occourred. Please try again.");
               return RuleDecisionKey.ON_FAILURE;
            }
         }
         else
         {
            response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Database exception occourred. Please try again.");
            return RuleDecisionKey.ON_FAILURE;
         }
      }
      catch (Exception e)
      {
         logger.error("Exception.", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal Error. Please try again.");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, connection);
      }

      regResponse.setRegistrationVerificaitonKey(registrationVerificationKey);
      if (logger.isDebugEnabled())
      {
         logger.debug("User with email id : " + userId + " is added into database successfully");
      }
      response.setParameters(EzedTransactionCodes.SUCCESS, "Userid : " + userId + " registered Successfully.");
      regResponse.setDefaultPassword(defaultPass);
      response.setResponseObject(regResponse);
      return RuleDecisionKey.ON_SUCCESS;
   }
}
