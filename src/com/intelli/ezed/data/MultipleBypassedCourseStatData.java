package com.intelli.ezed.data;

import java.util.ArrayList;

import org.json.JSONArray;

public class MultipleBypassedCourseStatData extends ResponseObject
{
   private ArrayList<CourseBypassedStatData> dataList = new ArrayList<CourseBypassedStatData>();

   @Override
   public void prepareResponse() throws Exception
   {
      JSONArray jsonArray = new JSONArray();
      for (int i = 0; i < dataList.size(); i++)
      {
         dataList.get(i).prepareResponse();
         jsonArray.put(dataList.get(i));
      }
      this.put("coursestats", jsonArray);
   }

   public ArrayList<CourseBypassedStatData> getDataList()
   {
      return dataList;
   }

   public void setDataList(ArrayList<CourseBypassedStatData> dataList)
   {
      this.dataList = dataList;
   }

}
