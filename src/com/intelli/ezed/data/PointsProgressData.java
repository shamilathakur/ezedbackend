package com.intelli.ezed.data;

public class PointsProgressData extends ResponseObject
{
   private int points;
   private int progress;
   private int totalViewedCount = 0;
   private int totalUnseetCount = 0;

   public int getTotalViewedCount()
   {
      return totalViewedCount;
   }

   public void setTotalViewedCount(int totalViewedCount)
   {
      this.totalViewedCount = totalViewedCount;
   }

   public int getTotalUnseetCount()
   {
      return totalUnseetCount;
   }

   public void setTotalUnseetCount(int totalUnseetCount)
   {
      this.totalUnseetCount = totalUnseetCount;
   }

   public int getPoints()
   {
      return points;
   }

   public void setPoints(int points)
   {
      this.points = points;
   }

   public int getProgress()
   {
      return progress;
   }

   public void setProgress(int progress)
   {
      this.progress = progress;
   }

   public void calculateProgress()
   {
      setProgress((int) (((float) getTotalViewedCount() / ((float) getTotalUnseetCount() + (float) getTotalViewedCount())) * 100f));
   }

   @Override
   public void prepareResponse() throws Exception
   {
      this.put("points", getPoints());
      //      if (getTotalViewedCount() == 0 && getTotalUnseetCount() == 0)
      //      {
      //         this.put("progess", 0);
      //         return;
      //      }
      //      calculateProgress();
      this.put("progress", getProgress());
   }
}
