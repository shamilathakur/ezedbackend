package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.analytics.data.GenericChartData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class CourseChapterFetcher extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public CourseChapterFetcher(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());

      String courseId = messageData.getParameter("courseid");
      if (courseId == null || courseId.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Course id in request in invalid");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid course id");
         return RuleDecisionKey.ON_FAILURE;
      }

      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      GenericChartData data = new GenericChartData();
      ArrayList<String> chapterList = new ArrayList<String>();
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchAllCourseChapters");
         ps.setString(1, courseId);
         rs = ps.executeQuery();
         boolean onceAdded = false;
         while (rs.next())
         {
            if (!onceAdded)
            {
               data.getText().add("defaultchapter");
               data.getValues().add(rs.getString("defchapter"));
               data.getText().add("lmrchapter");
               data.getValues().add(rs.getString("lmr"));
               data.getText().add("questionpaper");
               data.getValues().add(rs.getString("questionpaperid"));
               data.getText().add("questionsolution");
               data.getValues().add(rs.getString("solvedpaperid"));
               onceAdded = true;
            }
            chapterList.add(rs.getString("chapterid"));
         }
         if (chapterList.size() > 0)
         {
            String[] chapterArr = new String[chapterList.size()];
            for (int i = 0; i < chapterArr.length; i++)
            {
               chapterArr[i] = chapterList.get(i);
            }
            data.getText().add("chapters");
            data.getValues().add(chapterArr);
         }
         response.setResponseObject(data);
         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
         return RuleDecisionKey.ON_SUCCESS;
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching chapter details from db for course id " + courseId);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching course details");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching chapter details from db for course id " + courseId);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching course details");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing result set while fetching chapter details for course", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
}
