package com.intelli.ezed.utils;

import org.sabre.utils.map.TimedMap.MapTimeoutHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.transaction.PersistedObjects;
import com.intelli.ezed.bulkupload.data.ActiveSessionList;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.UserProfile;

public class EzedSessionTimeoutHandler implements MapTimeoutHandler<String, Object>
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public EzedSessionTimeoutHandler()
   {
   }

   @Override
   public void handleTimeout(String emailid, Object object)
   {
      PersistedObjects persistedObjects = (PersistedObjects) object;
      UserProfile profile = (UserProfile) persistedObjects.getObject(EzedKeyConstants.USER_DETAILS);
      ActiveSessionList.removeFromActiveSession(profile.getUserid());
      if (logger.isDebugEnabled())
      {
         logger.debug("Session id " + profile.getUserid() + " removed from list of active sessions");
      }
   }

}
