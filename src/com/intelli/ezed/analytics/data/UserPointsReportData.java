package com.intelli.ezed.analytics.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.json.JSONArray;
import org.sabre.db.DbConnectionManager;
import org.slf4j.Logger;

import com.intelli.ezed.data.ResponseObject;

public class UserPointsReportData extends ResponseObject
{
   private String userId;
   private int currentPoints;
   private int dayBeforePoints;
   private int dayToDayBeforePoints;
   private int weekBeforePoints;
   private int weekToWeekBeforePoints;
   private int monthBeforePoints;
   private int monthToMonthBeforePoints;
   private int sixMonthsBeforePoints;
   private int yearBeforePoints;

   private int pointsEarnedToday;
   private int pointsEarnedYesterday;
   private int pointsEarnedThisWeek;
   private int pointsEarnedLastWeek;
   private int pointsEarnedThisMonth;
   private int pointsEarnedLastMonth;
   private int pointsEarnedThisHalfYear;
   private int pointsEarnedLastHalfYear;
   private GenericChartData title = new GenericChartData();
   private GenericChartData xAxis = new GenericChartData();
   private GenericChartData yAxis = new GenericChartData();
   private GenericChartData type = new GenericChartData();

   public void prepareChart() throws Exception
   {
      type.getText().add("type");
      type.getValues().add("bar");
      type.prepareResponse();
      
      title.getText().add("text");
      title.getValues().add("Single User Points Report");
      title.prepareResponse();

      xAxis.getText().add("categories");
      String[] cats = {"Current Period", "Previous Period"};
      xAxis.getValues().add(cats);
      xAxis.getText().add("title");
      GenericChartData xAxisTitleData = new GenericChartData();
      xAxisTitleData.getText().add("text");
      xAxisTitleData.getValues().add("Time periods");
      xAxisTitleData.prepareResponse();
      xAxis.getValues().add(xAxisTitleData);
      xAxis.prepareResponse();

      yAxis.getText().add("min");
      yAxis.getValues().add(0);
      yAxis.getText().add("title");
      GenericChartData yAxisText = new GenericChartData();
      yAxisText.getText().add("text");
      yAxisText.getValues().add("No of points earned");
      yAxisText.getText().add("align");
      yAxisText.getValues().add("high");
      yAxis.getValues().add(yAxisText);
      yAxis.getText().add("labels");
      GenericChartData yAxisLabels = new GenericChartData();
      yAxisLabels.getText().add("overflow");
      yAxisLabels.getValues().add("justify");
      yAxis.getValues().add(yAxisLabels);

      yAxisLabels.prepareResponse();
      yAxisText.prepareResponse();
      yAxis.prepareResponse();
   }

   public int getPointsEarnedToday()
   {
      return pointsEarnedToday;
   }
   public void setPointsEarnedToday(int pointsEarnedToday)
   {
      this.pointsEarnedToday = pointsEarnedToday;
   }
   public int getPointsEarnedYesterday()
   {
      return pointsEarnedYesterday;
   }
   public void setPointsEarnedYesterday(int pointsEarnedYesterday)
   {
      this.pointsEarnedYesterday = pointsEarnedYesterday;
   }
   public int getPointsEarnedThisWeek()
   {
      return pointsEarnedThisWeek;
   }
   public void setPointsEarnedThisWeek(int pointsEarnedThisWeek)
   {
      this.pointsEarnedThisWeek = pointsEarnedThisWeek;
   }
   public int getPointsEarnedLastWeek()
   {
      return pointsEarnedLastWeek;
   }
   public void setPointsEarnedLastWeek(int pointsEarnedLastWeek)
   {
      this.pointsEarnedLastWeek = pointsEarnedLastWeek;
   }
   public int getPointsEarnedThisMonth()
   {
      return pointsEarnedThisMonth;
   }
   public void setPointsEarnedThisMonth(int pointsEarnedThisMonth)
   {
      this.pointsEarnedThisMonth = pointsEarnedThisMonth;
   }
   public int getPointsEarnedLastMonth()
   {
      return pointsEarnedLastMonth;
   }
   public void setPointsEarnedLastMonth(int pointsEarnedLastMonth)
   {
      this.pointsEarnedLastMonth = pointsEarnedLastMonth;
   }
   public int getPointsEarnedThisHalfYear()
   {
      return pointsEarnedThisHalfYear;
   }
   public void setPointsEarnedThisHalfYear(int pointsEarnedThisHalfYear)
   {
      this.pointsEarnedThisHalfYear = pointsEarnedThisHalfYear;
   }
   public int getPointsEarnedLastHalfYear()
   {
      return pointsEarnedLastHalfYear;
   }
   public void setPointsEarnedLastHalfYear(int pointsEarnedLastHalfYear)
   {
      this.pointsEarnedLastHalfYear = pointsEarnedLastHalfYear;
   }
   public String getUserId()
   {
      return userId;
   }
   public void setUserId(String userId)
   {
      this.userId = userId;
   }
   public int getCurrentPoints()
   {
      return currentPoints;
   }
   public void setCurrentPoints(int currentPoints)
   {
      this.currentPoints = currentPoints;
   }
   public int getDayBeforePoints()
   {
      return dayBeforePoints;
   }
   public void setDayBeforePoints(int dayBeforePoints)
   {
      this.dayBeforePoints = dayBeforePoints;
   }
   public int getDayToDayBeforePoints()
   {
      return dayToDayBeforePoints;
   }
   public void setDayToDayBeforePoints(int dayToDayBeforePoints)
   {
      this.dayToDayBeforePoints = dayToDayBeforePoints;
   }
   public int getWeekBeforePoints()
   {
      return weekBeforePoints;
   }
   public void setWeekBeforePoints(int weekBeforePoints)
   {
      this.weekBeforePoints = weekBeforePoints;
   }
   public int getWeekToWeekBeforePoints()
   {
      return weekToWeekBeforePoints;
   }
   public void setWeekToWeekBeforePoints(int weekToWeekBeforePoints)
   {
      this.weekToWeekBeforePoints = weekToWeekBeforePoints;
   }
   public int getMonthBeforePoints()
   {
      return monthBeforePoints;
   }
   public void setMonthBeforePoints(int monthBeforePoints)
   {
      this.monthBeforePoints = monthBeforePoints;
   }
   public int getMonthToMonthBeforePoints()
   {
      return monthToMonthBeforePoints;
   }
   public void setMonthToMonthBeforePoints(int monthToMonthBeforePoints)
   {
      this.monthToMonthBeforePoints = monthToMonthBeforePoints;
   }
   public int getSixMonthsBeforePoints()
   {
      return sixMonthsBeforePoints;
   }
   public void setSixMonthsBeforePoints(int sixMonthsBeforePoints)
   {
      this.sixMonthsBeforePoints = sixMonthsBeforePoints;
   }
   public int getYearBeforePoints()
   {
      return yearBeforePoints;
   }
   public void setYearBeforePoints(int yearBeforePoints)
   {
      this.yearBeforePoints = yearBeforePoints;
   }

   public void calculateHistoryPoints(String dbConnName, Logger logger) throws SQLException, Exception
   {
      this.fetchAllHistoryPointsForUser(dbConnName, logger);
      this.setPointsEarnedToday(getCurrentPoints() - getDayBeforePoints());
      this.setPointsEarnedYesterday(getDayBeforePoints() - getDayToDayBeforePoints());
      this.setPointsEarnedThisWeek(getCurrentPoints() - getWeekBeforePoints());
      this.setPointsEarnedLastWeek(getWeekBeforePoints() - getWeekToWeekBeforePoints());
      this.setPointsEarnedThisMonth(getCurrentPoints() - getMonthBeforePoints());
      this.setPointsEarnedLastMonth(getMonthBeforePoints() - getMonthToMonthBeforePoints());
      this.setPointsEarnedThisHalfYear(getCurrentPoints() - getSixMonthsBeforePoints());
      this.setPointsEarnedLastHalfYear(getSixMonthsBeforePoints() - getYearBeforePoints());
   }

   private void fetchAllHistoryPointsForUser(String dbConnName, Logger logger) throws SQLException, Exception
   {
      setCurrentPoints(fetchPointsFromUserTables(dbConnName, "FetchUserPoints", logger));
      setDayBeforePoints(fetchPointsFromUserTables(dbConnName, "FetchYesterdayPoints", logger));
      setDayToDayBeforePoints(fetchPointsFromUserTables(dbConnName, "FetchDayBeforeYesterdayPoints", logger));
      setWeekBeforePoints(fetchPointsFromUserTables(dbConnName, "FetchLastWeekPoints", logger));
      setWeekToWeekBeforePoints(fetchPointsFromUserTables(dbConnName, "FetchLastFortnightPoints", logger));
      setMonthBeforePoints(fetchPointsFromUserTables(dbConnName, "FetchLastMonthPoints", logger));
      setMonthToMonthBeforePoints(fetchPointsFromUserTables(dbConnName, "FetchLastTwoMonthPoints", logger));
      setSixMonthsBeforePoints(fetchPointsFromUserTables(dbConnName, "FetchLastSixMonthPoints", logger));
      setYearBeforePoints(fetchPointsFromUserTables(dbConnName, "FetchLastYearPoints", logger));
   }

   private int fetchPointsFromUserTables(String dbConnName, String prepStmtName, Logger logger) throws SQLException, Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, prepStmtName);
         ps.setString(1, getUserId());
         rs = ps.executeQuery();
         if (rs.next())
         {
            int points = rs.getInt(1);
            return points;
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("No entry found for user in history table for prep stmt name " + prepStmtName);
            }
            return 0;
         }

      }
      catch (SQLException e)
      {
         logger.error("Error while fetching points from one of users history tables for prep stmt name " + prepStmtName, e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching points from one of users history tables for prep stmt name " + prepStmtName, e);
         throw e;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
               logger.error("Error while closing result set while fetching points from one of users table");
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
   @Override
   public void prepareResponse() throws Exception
   {
      //      this.put("pointsearnedtoday", getPointsEarnedToday());
      //      this.put("pointsearnedyesterday", getPointsEarnedYesterday());
      //      this.put("pointsearnedthisweek", getPointsEarnedThisWeek());
      //      this.put("pointsearnedlastweek", getPointsEarnedLastWeek());
      //      this.put("pointsearnedthismonth", getPointsEarnedThisMonth());
      //      this.put("pointsearnedlastmonth", getPointsEarnedLastMonth());
      //      this.put("pointsearnedthishalfyear", getPointsEarnedThisHalfYear());
      //      this.put("pointsearnedlasthalfyear", getPointsEarnedLastHalfYear());
      //      UserCoursePointsReportData[] coursedatas = new UserCoursePointsReportData[this.userCoursePoints.size()];
      //      for(int i = 0;i<this.userCoursePoints.size();i++)
      //      {
      //         this.userCoursePoints.get(i);
      //         coursedatas[i] = this.userCoursePoints.get(i);
      //      }
      //      this.put("coursepoints", coursedatas);
      JSONArray jsonArray = new JSONArray();
      GranularPoints granularPoints = new GranularPoints("Day wise", getPointsEarnedToday(), getPointsEarnedYesterday());
      granularPoints.prepareResponse();
      jsonArray.put(granularPoints);

      granularPoints = new GranularPoints("Week wise", getPointsEarnedThisWeek(), getPointsEarnedLastWeek());
      granularPoints.prepareResponse();
      jsonArray.put(granularPoints);

      granularPoints = new GranularPoints("Month wise", getPointsEarnedThisMonth(), getPointsEarnedLastMonth());
      granularPoints.prepareResponse();
      jsonArray.put(granularPoints);

      granularPoints = new GranularPoints("Half year wise", getPointsEarnedThisHalfYear(), getPointsEarnedLastHalfYear());
      granularPoints.prepareResponse();
      jsonArray.put(granularPoints);

      this.put("chart", type);
      this.put("title", title);
      this.put("xAxis", xAxis);
      this.put("yAxis", yAxis);
      this.put("series", jsonArray);
   }
}
