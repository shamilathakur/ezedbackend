package com.intelli.ezed.rules;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.session.SessionManager;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.LoginResponse;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.UserProfile;

public class SetResponseObject extends SingletonRule
{

   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   
   public SetResponseObject(String ruleName)
   {
      super(ruleName);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      UserProfile userProfile = (UserProfile)flowContext.getFromContext(EzedKeyConstants.USER_DETAILS);
     // SessionManager sessionManager = (SessionManager) flowContext.getFromContext(EzedKeyConstants.SESSION_MANAGER);
      String sessionId = (String) flowContext.getFromContext(EzedKeyConstants.SESSION_ID);
    
      LoginResponse loginResponse = new LoginResponse();
      loginResponse.setSessionId(sessionId);
      loginResponse.setUserProfile(userProfile);
      response.setResponseObject(loginResponse);
      
      
      return RuleDecisionKey.ON_SUCCESS;
   }

}
