package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.CourseData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.utils.EndDateCalculator;

public class CourseDetailsFetcher extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public CourseDetailsFetcher(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());

      String courseId = messageData.getParameter("courseid");
      if (courseId == null || courseId.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Course id in request in invalid");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid course id");
         return RuleDecisionKey.ON_FAILURE;
      }

      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchCourseDetail");
         ps.setString(1, courseId);
         rs = ps.executeQuery();
         if (rs.next())
         {
            CourseData courseData = new CourseData();
            courseData.setCourseid(rs.getString("courseid"));
            courseData.setCoursename(rs.getString("coursename"));
            courseData.setShortdesc(rs.getString("shortdesc"));
            courseData.setIntrovideo(rs.getString("introvideo"));
            courseData.setDefchapter(rs.getString("defchapter"));
            courseData.setLmrchapter(rs.getString("lmr"));
            courseData.setContentonlyamt(rs.getString("contentonlyamt"));
            courseData.setTestonlyamt(rs.getString("testonlyamt"));
            courseData.setFullcourseamt(rs.getString("fullcourseamt"));
            courseData.setLongdesctype(Integer.toString(rs.getInt("longdesctype")));
            courseData.setLongdescstring(rs.getString("longdescstring"));
            courseData.setLongdesccontent(rs.getString("longdesccontent"));
            courseData.setQuestionpaperid(rs.getString("questionpaperid"));
            courseData.setSolvedpaperid(rs.getString("solvedpaperid"));

            String endMonths = rs.getString("enddate");
            courseData.setEnddate(EndDateCalculator.getEndDatesFromEndMonths(endMonths, new Date()));

            courseData.setCourseImage(rs.getString("courseimage"));

            response.setResponseObject(courseData);
            response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
            return RuleDecisionKey.ON_SUCCESS;
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("No record found in DB for courseid " + courseId);
            }
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Course not found");
            return RuleDecisionKey.ON_FAILURE;
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching course details from db for course id " + courseId);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching course details");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching course details from db for course id " + courseId);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching course details");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing result set while fetching course detail", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
}
