package com.intelli.ezed.frontend.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedLoggerName;

public class QuizQuestionDbInserter
{

   //use your own logger name at this position
   private String loggerName = EzedLoggerName.SERVICE_LOG;
   private Logger logger = LoggerFactory.getLogger(loggerName);

   private String quizId;
   private String courseid;
   private String chapterid;
   private int questionNumber;
   private int questiontype;
   private int questioncontenttype;
   private String questionstring;
   private String questioncontent;
   private int ans1type;
   private int ans2type;
   private int ans3type;
   private int ans4type;
   private int ans5type;
   private int ans6type;
   private String ans1string;
   private String ans2string;
   private String ans3string;
   private String ans4string;
   private String ans5string;
   private String ans6string;
   private String ans1content;
   private String ans2content;
   private String ans3content;
   private String ans4content;
   private String ans5content;
   private String ans6content;
   
   private String correctans;


   public String getQuizId()
   {
      return quizId;
   }
   public void setQuizId(String questionId)
   {
      this.quizId = questionId;
   }
   public String getCourseid()
   {
      return courseid;
   }
   public int getQuestionNumber()
   {
      return questionNumber;
   }
   public void setQuestionNumber(int questionNumber)
   {
      this.questionNumber = questionNumber;
   }
   public void setCourseid(String courseid)
   {
      this.courseid = courseid;
   }
   public String getChapterid()
   {
      return chapterid;
   }
   public void setChapterid(String chapterid)
   {
      this.chapterid = chapterid;
   }
   public int getQuestioncontenttype()
   {
      return questioncontenttype;
   }
   public void setQuestioncontenttype(int questioncontenttype)
   {
      this.questioncontenttype = questioncontenttype;
   }
   public String getQuestionstring()
   {
      return questionstring;
   }
   public void setQuestionstring(String questionstring)
   {
      this.questionstring = questionstring;
   }
   public String getQuestioncontent()
   {
      return questioncontent;
   }
   public void setQuestioncontent(String questioncontent)
   {
      this.questioncontent = questioncontent;
   }
   public int getAns1type()
   {
      return ans1type;
   }
   public void setAns1type(int ans1type)
   {
      this.ans1type = ans1type;
   }
   public int getAns2type()
   {
      return ans2type;
   }
   public void setAns2type(int ans2type)
   {
      this.ans2type = ans2type;
   }
   public int getAns3type()
   {
      return ans3type;
   }
   public void setAns3type(int ans3type)
   {
      this.ans3type = ans3type;
   }
   public int getAns4type()
   {
      return ans4type;
   }
   public void setAns4type(int ans4type)
   {
      this.ans4type = ans4type;
   }
   public int getAns5type()
   {
      return ans5type;
   }
   public void setAns5type(int ans5type)
   {
      this.ans5type = ans5type;
   }
   public int getAns6type()
   {
      return ans6type;
   }
   public void setAns6type(int ans6type)
   {
      this.ans6type = ans6type;
   }
   public String getAns1string()
   {
      return ans1string;
   }
   public void setAns1string(String ans1string)
   {
      this.ans1string = ans1string;
   }
   public String getAns2string()
   {
      return ans2string;
   }
   public void setAns2string(String ans2string)
   {
      this.ans2string = ans2string;
   }
   public String getAns3string()
   {
      return ans3string;
   }
   public void setAns3string(String ans3string)
   {
      this.ans3string = ans3string;
   }
   public String getAns4string()
   {
      return ans4string;
   }
   public void setAns4string(String ans4string)
   {
      this.ans4string = ans4string;
   }
   public String getAns5string()
   {
      return ans5string;
   }
   public void setAns5string(String ans5string)
   {
      this.ans5string = ans5string;
   }
   public String getAns6string()
   {
      return ans6string;
   }
   public void setAns6string(String ans6string)
   {
      this.ans6string = ans6string;
   }
   public String getAns1content()
   {
      return ans1content;
   }
   public void setAns1content(String ans1content)
   {
      this.ans1content = ans1content;
   }
   public String getAns2content()
   {
      return ans2content;
   }
   public void setAns2content(String ans2content)
   {
      this.ans2content = ans2content;
   }
   public String getAns3content()
   {
      return ans3content;
   }
   public void setAns3content(String ans3content)
   {
      this.ans3content = ans3content;
   }
   public String getAns4content()
   {
      return ans4content;
   }
   public void setAns4content(String ans4content)
   {
      this.ans4content = ans4content;
   }
   public String getAns5content()
   {
      return ans5content;
   }
   public void setAns5content(String ans5content)
   {
      this.ans5content = ans5content;
   }
   public String getAns6content()
   {
      return ans6content;
   }
   public void setAns6content(String ans6content)
   {
      this.ans6content = ans6content;
   }
   public int getQuestiontype()
   {
      return questiontype;
   }
   public void setQuestiontype(int questiontype)
   {
      this.questiontype = questiontype;
   }
   public String getCorrectans()
   {
      return correctans;
   }
   public void setCorrectans(String correctans)
   {
      this.correctans = correctans;
   }

   public int insertIntoDatabase()
   {
      //change how you get your connection here. you can use your own database connection pool or whatever.
      Connection connection = getDbConnection();
      if (connection == null)
      {
         return EzedFrontEndTransactionCodes.NO_CONNECTION;
      }

      //change how you get your prepared statement here. you can put the connection in some configuration files for any other setup.
      PreparedStatement preparedStatement = getStatement(connection);
      if (preparedStatement == null)
      {
         return EzedFrontEndTransactionCodes.NO_STATEMENT;
      }

      try
      {
         /*
          * courseid,chapterid,questioncontenttype,questionstring,questioncontent,
          * ans1type,ans2type,ans3type,ans4type,ans5type,ans6type,
          * ans1string,ans2string,ans3string,ans4string,ans5string,ans6string,
          * ans1content,ans2content,ans3content,ans4content,ans5content,ans6content,
          * difficultylevel,processed,questiontype,correctans,
          */
         preparedStatement.setString(1, getQuizId());
         preparedStatement.setString(2, getCourseid());
         preparedStatement.setString(3, getChapterid());
         preparedStatement.setInt(4, getQuestioncontenttype());
         preparedStatement.setString(5, getQuestionstring());
         preparedStatement.setString(6, getQuestioncontent());
         preparedStatement.setInt(7, getAns1type());
         preparedStatement.setInt(8, getAns2type());
         preparedStatement.setInt(9, getAns3type());
         preparedStatement.setInt(10, getAns4type());
         preparedStatement.setInt(11, getAns5type());
         preparedStatement.setInt(12, getAns6type());
         preparedStatement.setString(13, getAns1string());
         preparedStatement.setString(14, getAns2string());
         preparedStatement.setString(15, getAns3string());
         preparedStatement.setString(16, getAns4string());
         preparedStatement.setString(17, getAns5string());
         preparedStatement.setString(18, getAns6string());
         preparedStatement.setString(19, getAns1content());
         preparedStatement.setString(20, getAns2content());
         preparedStatement.setString(21, getAns3content());
         preparedStatement.setString(22, getAns4content());
         preparedStatement.setString(23, getAns5content());
         preparedStatement.setString(24, getAns6content());
         preparedStatement.setInt(26, getQuestiontype());
         preparedStatement.setString(27, getCorrectans());

         boolean i = preparedStatement.execute();

         if (i == true)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Course added in database.");
            }
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("no data inserted in database status : " + i);
            }
            return EzedFrontEndTransactionCodes.NO_ROWS_INSERTED;
         }
      }
      catch (SQLException e)
      {
         logger.error("Database exception : ", e);
         return EzedFrontEndTransactionCodes.DB_EXCEPTION_OCCURRED;
      }
      catch (Exception e)
      {
         logger.error("Exception occoured while inserting content in database.", e);
         return EzedFrontEndTransactionCodes.INTERNAL_ERROR;
      }

      return EzedFrontEndTransactionCodes.SUCCESS;
   }

   public int fetchFromDatabase()
   {

      //change how you get your connection here. you can use your own database connection pool or whatever.
      Connection connection = getDbConnection();
      if (connection == null)
      {
         return EzedFrontEndTransactionCodes.NO_CONNECTION;
      }

      //change how you get your prepared statement here. you can put the connection in some configuration files for any other setup.
      PreparedStatement preparedStatement = getFetchStatement(connection);
      if (preparedStatement == null)
      {
         return EzedFrontEndTransactionCodes.NO_STATEMENT;
      }

      ResultSet resultSet = null;

      try
      {
         preparedStatement.setString(1, getQuizId());
         resultSet = preparedStatement.executeQuery();


         setCourseid(resultSet.getString("courseid"));
         setChapterid(resultSet.getString("chapterid"));
         setQuestioncontenttype(resultSet.getInt("questioncontenttype"));
         setQuestionstring(resultSet.getString("questionstring"));
         setQuestioncontent(resultSet.getString("questioncontent"));
         setAns1type(resultSet.getInt("ans1type"));
         setAns2type(resultSet.getInt("ans2type"));
         setAns3type(resultSet.getInt("ans3type"));
         setAns4type(resultSet.getInt("ans4type"));
         setAns5type(resultSet.getInt("ans5type"));
         setAns6type(resultSet.getInt("ans6type"));
         setAns1string(resultSet.getString("ans1string"));
         setAns2string(resultSet.getString("ans2string"));
         setAns3string(resultSet.getString("ans3string"));
         setAns4string(resultSet.getString("ans4string"));
         setAns5string(resultSet.getString("ans5string"));
         setAns6string(resultSet.getString("ans6string"));
         setAns1content(resultSet.getString("ans1content"));
         setAns2content(resultSet.getString("ans2content"));
         setAns3content(resultSet.getString("ans3content"));
         setAns4content(resultSet.getString("ans4content"));
         setAns5content(resultSet.getString("ans5content"));
         setAns6content(resultSet.getString("ans6content"));
         setQuestiontype(resultSet.getInt("questiontype"));
         setCorrectans(resultSet.getString("correctans"));

      }
      catch (SQLException e)
      {
         logger.error("Database Exception occoured while fetching question from database.", e);
         return EzedFrontEndTransactionCodes.DB_EXCEPTION_OCCURRED;
      }
      catch (Exception e)
      {
         logger.error("Exception occoured while fetching question from database.", e);
         return EzedFrontEndTransactionCodes.INTERNAL_ERROR;
      }
      finally
      {
         try
         {
            resultSet.close();
         }
         catch (Exception e)
         {
            logger.error("Exception occoured while closing result set.", e);
         }
      }


      return EzedFrontEndTransactionCodes.SUCCESS;
   }

   public int persisteIntoDatabase()
   {
      //change how you get your connection here. you can use your own database connection pool or whatever.
      Connection connection = getDbConnection();
      if (connection == null)
      {
         return EzedFrontEndTransactionCodes.NO_CONNECTION;
      }

      //change how you get your prepared statement here. you can put the connection in some configuration files for any other setup.
      PreparedStatement preparedStatement = getUpdateStatement(connection);
      if (preparedStatement == null)
      {
         return EzedFrontEndTransactionCodes.NO_STATEMENT;
      }

      try
      {
         /*
          * courseid,chapterid,questioncontenttype,questionstring,questioncontent,
          * ans1type,ans2type,ans3type,ans4type,ans5type,ans6type,
          * ans1string,ans2string,ans3string,ans4string,ans5string,ans6string,
          * ans1content,ans2content,ans3content,ans4content,ans5content,ans6content,
          * difficultylevel,processed,questiontype,correctans,
          */

         preparedStatement.setString(1, getCourseid());
         preparedStatement.setString(2, getChapterid());
         preparedStatement.setInt(3, getQuestioncontenttype());
         preparedStatement.setString(4, getQuestionstring());
         preparedStatement.setString(5, getQuestioncontent());
         preparedStatement.setInt(6, getAns1type());
         preparedStatement.setInt(7, getAns2type());
         preparedStatement.setInt(8, getAns3type());
         preparedStatement.setInt(9, getAns4type());
         preparedStatement.setInt(10, getAns5type());
         preparedStatement.setInt(11, getAns6type());
         preparedStatement.setString(12, getAns1string());
         preparedStatement.setString(13, getAns2string());
         preparedStatement.setString(14, getAns3string());
         preparedStatement.setString(15, getAns4string());
         preparedStatement.setString(16, getAns5string());
         preparedStatement.setString(17, getAns6string());
         preparedStatement.setString(18, getAns1content());
         preparedStatement.setString(19, getAns2content());
         preparedStatement.setString(20, getAns3content());
         preparedStatement.setString(21, getAns4content());
         preparedStatement.setString(22, getAns5content());
         preparedStatement.setString(23, getAns6content());
         preparedStatement.setInt(26, getQuestiontype());
         preparedStatement.setString(27, getCorrectans());
         preparedStatement.setString(28, getQuizId());

         boolean i = preparedStatement.execute();

         if (i == true)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Course added in database.");
            }
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("no data inserted in database status : " + i);
            }
            return EzedFrontEndTransactionCodes.NO_ROWS_INSERTED;
         }
      }
      catch (SQLException e)
      {
         logger.error("Database exception : ", e);
         return EzedFrontEndTransactionCodes.DB_EXCEPTION_OCCURRED;
      }
      catch (Exception e)
      {
         logger.error("Exception occoured while inserting content in database.", e);
         return EzedFrontEndTransactionCodes.INTERNAL_ERROR;
      }

      return EzedFrontEndTransactionCodes.SUCCESS;
   }

   private Connection getDbConnection()
   {
      try
      {
         String url = "jdbc:postgresql://localhost:5432/ezed";
         Class.forName("org.postgresql.Driver");
         return DriverManager.getConnection(url, "postgres", "postgres");
      }
      catch (Exception e)
      {
         return null;
      }
   }

   private PreparedStatement getStatement(Connection conn)
   {
      try
      {
         return (conn.prepareStatement("insert into quizquestionlist (quizid,chapterid,questioncontenttype,questionstring,questioncontent,ans1type,ans2type,ans3type,ans4type,ans5type,ans6type,ans1string,ans2string,ans3string,ans4string,ans5string,ans6string,ans1content,ans2content,ans3content,ans4content,ans5content,ans6content,questiontype,correctans) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"));
      }
      catch (Exception e)
      {
         return null;
      }
   }

   private PreparedStatement getFetchStatement(Connection conn)
   {
      try
      {
         return (conn.prepareStatement("select * from  quizquestionlist where quizid = ?"));
      }
      catch (Exception e)
      {
         return null;
      }
   }

   private PreparedStatement getUpdateStatement(Connection conn)
   {
      try
      {
         return (conn.prepareStatement("update quizquestionlist set chapterid=?,questioncontenttype=?,questionstring=?,questioncontent=?,ans1type=?,ans2type=?,ans3type=?,ans4type=?,ans5type=?,ans6type=?,ans1string=?,ans2string=?,ans3string=?,ans4string=?,ans5string=?,ans6string=?,ans1content=?,ans2content=?,ans3content=?,ans4content=?,ans5content=?,ans6content=?,questiontype=?,correctans=? where quizid = ?"));
      }
      catch (Exception e)
      {
         return null;
      }
   }
}
