package com.ezed.utils.dbutil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicLong;

public class PreparePostId
{

   //keep calling getnextid() to get new note ID's. Note ID's will be
   //in the format NOT1, NOT2, NOT3, and so on...
   private AtomicLong atomicLong = null;
   private Connection connection = null;
   static PreparePostId genId = null;
   private String startString = "POS1";


   private PreparePostId()
   {
      long initialValue = getStartUpTransactionId();
      if (initialValue == 0)
      {
         initialValue = (System.currentTimeMillis() / 10000);
      }
      atomicLong = new AtomicLong(initialValue);
   }

   public static PreparePostId getPrepareThreadId()
   {
      if (genId == null)
      {
         genId = new PreparePostId();
         return genId;
      }
      else
      {
         return genId;
      }


   }

   public PreparePostId(Connection connection)
   {
      this.connection = connection;

      long initialValue = getStartUpTransactionId();
      if (initialValue == 0)
      {
         initialValue = (System.currentTimeMillis() / 10000);
      }
      atomicLong = new AtomicLong(initialValue);
   }

   private long getStartUpTransactionId()
   {
      //change how you get your connection here.
      if (connection == null)
      {
         return 0;
      }

      //change how you get your prepared statement here.
      PreparedStatement preparedStatement = getStatement(connection);
      if (preparedStatement == null)
      {
         return 0;
      }
      try
      {
         preparedStatement.setString(1, startString + "%");
         ResultSet resultSet = preparedStatement.executeQuery();
         if (resultSet.next())
         {
            String valueString = resultSet.getString(1);
            valueString = valueString.substring(startString.length());
            long value = Long.parseLong(valueString);
            resultSet.close();
            resultSet = null;

            if (value == 0)
            {
               value = 1;
               return value;
            }

            return value + 1;
         }
         return 1;
      }
      catch (Exception e)
      {
         return 0;
      }
      finally
      {
         try
         {
            preparedStatement.close();
            // resultSet.close();
         }
         catch (SQLException e)
         {
         }
         try
         {
            //  connection.close();
         }
         catch (Exception e)
         {
         }
      }
   }


   public String getNextId()
   {
      return startString + DataFiller.getLeftZeroPadding(Long.toString(this.atomicLong.incrementAndGet()), 16);
   }

   //   public String getPostId()
   //   {
   //      return "POS" + Long.toString(this.atomicLong.incrementAndGet()).toString();
   //   }

   private static PreparedStatement getStatement(Connection conn)
   {
      try
      {
         return (conn.prepareStatement("select max(postid) from forumpost where postid like ?"));
      }
      catch (Exception e)
      {
         return null;
      }
   }
}
