package com.intelli.ezed.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.utils.map.TimedMap.MapTimeoutHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.transaction.PersistedObjects;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.TestDetails;
import com.intelli.ezed.data.UserProfile;

public class EzedTestSessionTimeoutHandler implements MapTimeoutHandler<String, Object>
{

   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName = "ezeddb";
   private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

   @Override
   public void handleTimeout(String emailId, Object object)
   {
      PersistedObjects persistedObjects = (PersistedObjects) object;

      UserProfile userProfile = (UserProfile) persistedObjects.getObject(EzedKeyConstants.USER_DETAILS);
      TestDetails testDetails = (TestDetails) persistedObjects.getObject(TestDetails.TEST_DETAILS);
      String testSessionId = (String) persistedObjects.getObject(EzedKeyConstants.TEST_SESSION_ID);

      String userid = userProfile.getUserid();
      String testId = testDetails.getTestid();
      int numberOfQuestions = testDetails.getNumberOfQuestions();
      int attemptedQuestions = testDetails.getCurrentQuestionNumber();
      int correctAnsGiven = 0;

      ResultSet resultSet = null;
      int isTestComplete = 0; //0 -> not : 1 -> yes

      Connection connection = DbConnectionManager.getConnectionByName(dbConnName);
      try
      {
         //select * from ongoingtest where testsessionid = ? and userid = ? order by qserialnumber;
         PreparedStatement preparedStatement = DbConnectionManager.getPreparedStatement(dbConnName, connection, "GetTestResult");
         preparedStatement.setString(1, testSessionId);
         preparedStatement.setString(2, userid);

         resultSet = preparedStatement.executeQuery();

         while (resultSet.next())
         {
            String correctAns = resultSet.getString("correctans");
            String receivedCorrectAnsString = resultSet.getString("receivedcorrectansstring");

            if (receivedCorrectAnsString == null)
            {
               if (logger.isDebugEnabled())
               {
                  logger.debug("Question Not attempted by uses thats why no ans found in database.");
               }
               continue;
            }

            if (correctAns.compareTo(receivedCorrectAnsString) == 0)
            {
               correctAnsGiven = correctAnsGiven + 1;
            }
         }
         testDetails.setCorrectAnsGiven(correctAnsGiven);

         if (attemptedQuestions == numberOfQuestions)
         {
            isTestComplete = 1;
         }

         //insert into testsummery (testid,userid,testsessionid,numberofquestions,attempted,correct,testcompleted) values (?,?,?,?,?,?,?)
         PreparedStatement insertIntoTestSummary = DbConnectionManager.getPreparedStatement(dbConnName, connection, "InsetTestSummeryInDatabase");
         insertIntoTestSummary.setString(1, testId);
         insertIntoTestSummary.setString(2, userid);
         insertIntoTestSummary.setString(3, testSessionId);
         insertIntoTestSummary.setInt(4, numberOfQuestions);
         insertIntoTestSummary.setInt(5, attemptedQuestions);
         insertIntoTestSummary.setInt(6, correctAnsGiven);
         insertIntoTestSummary.setInt(7, isTestComplete);
         insertIntoTestSummary.setString(8, sdf.format(new Date()));
         insertIntoTestSummary.setLong(9, EzedHelper.getTimeInMillisec(testDetails.getTestTimeLimit()));

         int i = insertIntoTestSummary.executeUpdate();

         if (i != 1)
         {
            logger.debug("No test summary is created in database. Number of rows inserted in testSummary table are : " + i);
         }

      }
      catch (SQLException e)
      {
         logger.debug("Database exception while trying to create test summary : ", e);
      }
      catch (Exception e)
      {
         logger.debug("Exception while trying to create test summary : ", e);
      }
      finally
      {
         try
         {
            resultSet.close();
         }
         catch (Exception e)
         {
            logger.error("Exception occoure when trying to close the result set.", e);
         }
         DbConnectionManager.releaseConnection(dbConnName, connection);
      }
   }
}
