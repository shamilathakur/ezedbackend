package com.intelli.ezed.utils;

import java.util.HashMap;

import org.sabre.io.transport.http.data.HttpTransactionData;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.UserProfile;

public class PromotionEmailConstructor extends SingletonRule {

	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

	public PromotionEmailConstructor(String ruleName) {
		super(ruleName);
	}

	@Override
	public RuleDecisionKey executeRule(FlowContext flowContext) {
		UserProfile profile = (UserProfile) flowContext
				.getFromContext(EzedKeyConstants.USER_DETAILS);
		String email = (String) flowContext
				.getFromContext(EzedKeyConstants.USER_ID);
		int noOfDay = (Integer) flowContext
				.getFromContext(EzedKeyConstants.DAY_NUMBER) + 1;
		String firstname = profile.getFirstName();

		HttpTransactionData transData = new HttpTransactionData();
		transData.setRequestType(HttpTransactionData.POST);
		transData.setParaMap(new HashMap<String, String>());
		transData.addParam("userid", email);
		transData.addParam("username", firstname);
		transData.addParam("daynumber", (noOfDay + ""));
		if (logger.isDebugEnabled()) {
			logger.debug("Promotion mail number for new registration to be sent to userid "
					+ email + " is " + noOfDay);
		}

		flowContext.putIntoContext(HttpTransactionData.WRITE_REQ_DATA,
				transData);
		return RuleDecisionKey.ON_SUCCESS;
	}

}
