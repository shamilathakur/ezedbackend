package com.intelli.ezed.rules;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.sabre.io.transport.http.data.HttpTransactionData;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.session.SessionException;
import com.a.helper.session.SessionManager;
import com.intelli.ezed.data.CartData;
import com.intelli.ezed.data.CartDetails;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;

public class FrontendCourseUpdateRequestConstructor extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public FrontendCourseUpdateRequestConstructor(String ruleName)
   {
      super(ruleName);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      SessionManager sessionManager = (SessionManager) flowContext.getFromContext(EzedKeyConstants.SESSION_MANAGER);
      String userId = (String) flowContext.getFromContext(EzedKeyConstants.USER_ID);
      CartDetails cartObject = (CartDetails) flowContext.getFromContext(EzedKeyConstants.CART_DETAILS);
      CartData[] cartData = cartObject.getCartDatas();
      StringBuffer courseList = new StringBuffer();
      StringBuffer courseType = new StringBuffer();

      for (CartData singleCartData : cartData)
      {
         if (singleCartData.getStatusCode() == EzedTransactionCodes.SUCCESS)
         {
            courseList.append(singleCartData.getCourseId()).append(",");
            courseType.append(singleCartData.getCourseType()).append(",");
         }
      }
      if (courseList.length() > 0)
      {
         courseList.deleteCharAt(courseList.length() - 1);
         courseType.deleteCharAt(courseType.length() - 1);
      }
      else
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("None of the courses were successfully added to user to be updated to frontend for userid " + userId);
         }
         return RuleDecisionKey.ON_FAILURE;
      }

      Set<String> keySet = new HashSet<String>();
      keySet.add(EzedKeyConstants.SESSION_ID);
      String sessionIdInPersister = null;
      try
      {
         sessionIdInPersister = (String) sessionManager.checkDataFromSession(userId, keySet).get(EzedKeyConstants.SESSION_ID);
         if (sessionIdInPersister == null || sessionIdInPersister.trim().compareTo("") == 0)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Session id is null in persister for email id " + userId + ". Not updating frontend");
            }
            return RuleDecisionKey.ON_FAILURE;
         }
      }
      catch (SessionException e)
      {
         logger.error("Error while checking for active session against userid " + userId, e);
         return RuleDecisionKey.ON_FAILURE;
      }

      HttpTransactionData transData = new HttpTransactionData();
      transData.setRequestType(HttpTransactionData.GET);
      transData.setParaMap(new HashMap<String, String>());
      transData.addParam("sessionId", sessionIdInPersister);
      transData.addParam("userId", userId);
      transData.addParam("courseIdList", courseList.toString());
      transData.addParam("courseTypeList", courseType.toString());

      flowContext.putIntoContext(HttpTransactionData.WRITE_REQ_DATA, transData);
      if (logger.isDebugEnabled())
      {
         logger.debug("Request to be sent to frontend for update : " + transData);
      }
      return RuleDecisionKey.ON_SUCCESS;
   }

}
