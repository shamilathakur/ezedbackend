package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.utils.StringSplitter;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.FrontendFetchData;
import com.intelli.ezed.data.Response;

public class TypeSpecificContentFromChapterListFetcher extends SingletonRule
{
   private String dbConnName;
   private String[] contentType;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public TypeSpecificContentFromChapterListFetcher(String ruleName, String dbConnName, String contentType)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
      this.contentType = StringSplitter.splitString(contentType, ",");
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      //      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      String[] chapterIdList = (String[]) flowContext.getFromContext(EzedKeyConstants.CHAPTER_ID_LIST);
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      String[] contentArr = null;
      FrontendFetchData contentData = new FrontendFetchData();
      response.setResponseObject(contentData);
      response.setStatusCodeSend(false);
      contentData.setAttribName("aaData");
      ArrayList<String[]> contents = new ArrayList<String[]>();
      Map<String, String> contentMap = new HashMap<String, String>();
      int i = 1;
      String chapterId = null;
      for (int j = 0; j < chapterIdList.length; j++)
      {
         for (int k = 0; k < contentType.length; k++)
         {
            chapterId = chapterIdList[j];
            try
            {
               conn = DbConnectionManager.getConnectionByName(dbConnName);
               ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchChapterContentWithDetail");
               ps.setString(1, chapterId);
               ps.setInt(2, Integer.parseInt(contentType[k]));
               rs = ps.executeQuery();
               while (rs.next())
               {
                  contentArr = new String[4];
                  contentArr[0] = Integer.toString(i);
                  contentArr[1] = rs.getString("contentid");
                  contentArr[2] = rs.getString("contentname");
                  contentArr[3] = rs.getString("chaptername");
                  if (contentMap.get(contentArr[1]) == null)
                  {
                     contentMap.put(contentArr[1], contentArr[1]);
                     contents.add(contentArr);
                     i++;
                  }
               }
            }
            catch (SQLException e)
            {
               logger.error("Error while fetching chapter content details", e);
               response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching chapter contents");
               return RuleDecisionKey.ON_FAILURE;
            }
            catch (Exception e)
            {
               logger.error("Error while fetching chapter content details", e);
               response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching chapter contents");
               return RuleDecisionKey.ON_FAILURE;
            }
            finally
            {
               if (rs != null)
               {
                  try
                  {
                     rs.close();
                  }
                  catch (SQLException e)
                  {
                  }
               }
               DbConnectionManager.releaseConnection(dbConnName, conn);
            }
         }
      }
      contentData.setAttribList(contents);
      response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
      return RuleDecisionKey.ON_SUCCESS;
   }
}
