package com.intelli.ezed.rules;

import java.sql.SQLException;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.analytics.data.RegVsEnrollReportData;
import com.intelli.ezed.analytics.data.TestResponseOne;
import com.intelli.ezed.analytics.data.TestResposeTwo;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class ResponseCreator extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;

   public ResponseCreator(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = new Response();
      flowContext.putIntoContext(EzedKeyConstants.EZED_RESPONSE, response);

      RegVsEnrollReportData enrollmentReportData = new RegVsEnrollReportData();
      try
      {
         enrollmentReportData.calculateUserRegCounts(dbConnName, logger);
      }
      catch (SQLException e)
      {
         logger.error("Database Exception occoured while fetching user flags", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Please check the userid and try again.");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Exception occoured while fetching user flags", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal Error please try again later.");
         return RuleDecisionKey.ON_FAILURE;
      }


      String[] cats = {"Total", "Yesterday", "Last Week", "Last Month", "Last Year"};


      String chartType = "bar";
      String yAxisDisplayText = "No of Users";
      String xAxisDisplayText = "Count as of";

      String yAxisDisplayLable = "Label";
      String xAxisDisplayLable = "Label";

      TestResponseOne one = new TestResponseOne();
      float[] f1 = new float[5];
      f1[0] = enrollmentReportData.getTotalUsersRegistered();
      f1[1] = enrollmentReportData.getYesterdayUsersRegistered();
      f1[2] = enrollmentReportData.getLastWeekUsersRegistered();
      f1[3] = enrollmentReportData.getLastMonthUsersRegistered();
      f1[4] = enrollmentReportData.getLastYearUsersRegistered();
      one.setValues(f1);
      one.setName("Active");


      TestResponseOne two = new TestResponseOne();
      f1 = new float[5];
      f1[0] = enrollmentReportData.getTotalUsersEnrolled();
      f1[1] = enrollmentReportData.getYesterdayUsersEnrolled();
      f1[2] = enrollmentReportData.getLastWeekUsersEnrolled();
      f1[3] = enrollmentReportData.getLastMonthUsersEnrolled();
      f1[4] = enrollmentReportData.getLastYearUsersEnrolled();
      two.setValues(f1);
      two.setName("Validated");


      TestResposeTwo testResposeTwo = new TestResposeTwo();

      testResposeTwo.setCats(cats);
      testResposeTwo.setxAxisDisplayLable(xAxisDisplayLable);
      testResposeTwo.setxAxisDisplayText(xAxisDisplayText);
      testResposeTwo.setyAxisDisplayLable(yAxisDisplayLable);
      testResposeTwo.setyAxisDisplayText(yAxisDisplayText);
      testResposeTwo.setChartType(chartType);
      testResposeTwo.setGraphTitle("Active vs Validated User Report");

      TestResponseOne[] testResponseOnes = new TestResponseOne[2];
      testResponseOnes[0] = one;
      testResponseOnes[1] = two;
      testResposeTwo.setTestResponseOnes(testResponseOnes);

      response.setResponseObject(testResposeTwo);
      response.setStatusCodeSend(false);

      return RuleDecisionKey.ON_SUCCESS;
   }

}
