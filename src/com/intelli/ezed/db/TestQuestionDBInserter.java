package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.QuizRequestResponse;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.TestDetails;
import com.intelli.ezed.data.TestRequestResponse;
import com.intelli.ezed.data.UserProfile;

public class TestQuestionDBInserter extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;

   public TestQuestionDBInserter(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      UserProfile userProfile = (UserProfile) flowContext.getFromContext(EzedKeyConstants.USER_DETAILS);
      TestDetails testDetails = (TestDetails) flowContext.getFromContext(TestDetails.TEST_DETAILS);

      Connection connection = DbConnectionManager.getConnectionByName(dbConnName);
      String userid = userProfile.getUserid();
      QuizRequestResponse quizRequestResponse = (QuizRequestResponse) response.getResponseObject();
      //changed to add object in response
      TestRequestResponse testRequestResponse = quizRequestResponse.getQuizQuestions().get(0);
      String questionId = testRequestResponse.getQuestionId();
      String correctAns = testRequestResponse.getCorrectAnswer();
      int questionNumber = testRequestResponse.getQuestionNumberOfRandomQuestion();
      int currentQuestionNumber = testRequestResponse.getCurrentQuestionNumber();
      String testId = testDetails.getTestid();
      String testSessionId = (String) flowContext.getFromContext(EzedKeyConstants.TEST_SESSION_ID);

      try
      {
         /*
          * insert into ongoingtest (questionid,questionnumber,ans1type,ans2type,ans3type,ans4type,ans1string,ans2string,ans3string,ans4string,userid,testid,testsessionid,qserialnumber) 
          * select questionid,questionnumber,ans1type,ans2type,ans3type,ans4type,ans1string,ans2string,ans3string,ans4string,?,?,?,? 
          * from randomizedquestionbank where questionid=? and questionnumber=?
          */
         PreparedStatement preparedStatement = DbConnectionManager.getPreparedStatement(dbConnName, connection, "InsertQuestionIntoOngoingTest");

         preparedStatement.setString(1, userid);
         preparedStatement.setString(2, testId);
         preparedStatement.setString(3, testSessionId);
         preparedStatement.setInt(4, currentQuestionNumber);
         preparedStatement.setString(5, correctAns);
         preparedStatement.setString(6, questionId);
         preparedStatement.setInt(7, questionNumber);


         int i = preparedStatement.executeUpdate();

         logger.debug("Number of rows inserted  : " + i);
      }
      catch (SQLException e)
      {
         e.printStackTrace();
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, connection);
      }

      return RuleDecisionKey.ON_SUCCESS;
   }

}
