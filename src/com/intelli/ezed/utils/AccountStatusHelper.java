package com.intelli.ezed.utils;

/*
 * Author: Chetan Burse 
 * This class is used to set the Account status
 * 32-bit integer is divided to left most 16-bits for counter of the 
 *  right most 1st digit is for Disabled for day
 *  right most 2nd digit is for Account is Active or not
 *  right most 3rd digit is for Account is Valid or not
 *  right most 4th digit is for Permanently lock the account
 */
public class AccountStatusHelper
{


  

   /**
    *  This method is used to set the "Disabled for day" flag inside accountStatus
    *  just does the OR with 1 of accountStatus means set the right most digit
    * @param accountStatus integer value
    * @return accountStatus 
    */
   public static int setDisabledForDay(int accountStatus)
   {
      //OR with 1 
      accountStatus = accountStatus | 1;

      return accountStatus;
   }


   /**
    *  This method is used to set the "Active" flag inside accountStatus
    *  just does the OR with 2 of accountStatus means set the 2nd right most digit
    * @param accountStatus integer value
    * @return accountStatus 
    */
   public static int setRegistered(int accountStatus)
   {
      //OR with 2 
      accountStatus = accountStatus | 2;

      return accountStatus;

   }

   /**
    *  This method is used to set the "Validate" flag inside accountStatus
    *  just does the OR with 4 of accountStatus means set the 3rd right most digit
    * @param accountStatus integer value
    * @return accountStatus 
    */
   public static int setValidate(int accountStatus)
   {
      //OR with 4 
      accountStatus = accountStatus | 4;

      return accountStatus;

   }


   /**
    *  This method is used to set the "Lock" flag inside accountStatus
    *  just does the OR with 8 of accountStatus means set the 4th right most digit
    * @param accountStatus integer value
    * @return accountStatus 
    */
   public static int setLock(int accountStatus)
   {
      //OR with 8
      accountStatus = accountStatus | 8;

      return accountStatus;

   }
   
   
   /**
    *  This method is used to set the "Lock" flag inside accountStatus
    *  just does the OR with 16 of accountStatus means set the 5th right most digit
    * @param accountStatus integer value
    * @return accountStatus 
    */
   public static int setForgotPassword(int accountStatus)
   {
      //OR with 16
      accountStatus = accountStatus | 16;

      return accountStatus;
   }
   
   
   /**
    *  This method is used to set the "Lock" flag inside accountStatus
    *  just does the OR with 32 of accountStatus means set the 6th right most digit
    * @param accountStatus integer value
    * @return accountStatus 
    */
   public static int setWapActive(int accountStatus)
   {
      //OR with 32
      accountStatus = accountStatus | 32;

      return accountStatus;
   }
   
   
   
   
   
   
   
   
   
   

   /**
    *  This method is used to reset the "Disabled for day" flag inside accountStatus
    *  just does the AND with 0xFFFFFFFE of accountStatus means reset the right most digit
    * @param accountStatus integer value
    * @return accountStatus 
    */
   public static int resetDisabledForDay(int accountStatus)
   {

      //AND with 0xFFFFFFFE
      return (accountStatus & 0xFFFFFFFE);
   }


   /**
    *  This method is used to reset the "Active" flag inside accountStatus
    *  just does the AND with 0xFFFFFFFD of accountStatus means reset the 2nd right most digit
    * @param accountStatus integer value
    * @return accountStatus 
    */
   public static int resetRegistered(int accountStatus)
   {
      //AND with 0xFFFFFFFD
      return (accountStatus & 0xFFFFFFFD);

   }

   public static int resetForgotPassword(int accountStatus)
   {
      //AND with 0xFFFFFFFD
      return (accountStatus & 0xFFFFFFEF);

   }

   
   public static int resetWapActive(int accountStatus)
   {
      //AND with 0xFFFFFFDF
      return (accountStatus & 0xFFFFFFDF);
   }
   
   
   /**
    *  This method is used to reset the "Validate" flag inside accountStatus
    *  just does the AND with 0xFFFFFFFB of accountStatus means reset the 3rd right most digit
    * @param accountStatus integer value
    * @return accountStatus 
    */
   public static int resetValidate(int accountStatus)
   {
      //AND with 0xFFFFFFFB
      return (accountStatus & 0xFFFFFFFB);

   }

   /**
    *  This method is used to reset the "Lock" flag inside accountStatus
    *  just does the And with 0xFFFFFFF7 of accountStatus means reset the 4th right most digit
    * @param accountStatus integer value
    * @return accountStatus 
    */
   public static int resetLock(int accountStatus)
   {
      //AND with 0xFFFFFFF7
      return (accountStatus & 0xFFFFFFF7);

   }

   /**
    *  This method is used to get the "Disabled for Day" flag inside accountStatus
    * @param accountStatus integer value
    * @return true or false  
    */
   public static boolean isDisabledForDay(int accountStatus)
   {

      return ((accountStatus & 1) > 0 ? true : false);

   }

   /**
    *  This method is used to get the "Active" flag inside accountStatus
    *  
    * @param accountStatus integer value
    * @return  true or false 
    */
   public static boolean isRegistered(int accountStatus)
   {

      return ((accountStatus & 2) > 0 ? true : false);

   }

   /**
    *  This method is used to get the "Validate" flag inside accountStatus
    *  
    * @param accountStatus integer value
    * @return  true or false  
    */
   public static boolean isValidate(int accountStatus)
   {
      return ((accountStatus & 4) > 0 ? true : false);

   }


   /**
    *  This method is used to get the "Lock" flag inside accountStatus
    * 
    * @param accountStatus integer value
    * @return  true or false  
    */
   public static boolean isLock(int accountStatus)
   {
      return ((accountStatus & 8) > 0 ? true : false);
   }


   public static boolean isPasswordForgotten(int accountStatus)
   {
      return ((accountStatus & 16) > 0 ? true : false);
   }


   public static boolean isWapActivated(int accountStatus)
   {
      return ((accountStatus & 32) > 0 ? true : false);
   }

   
   /**
    *  This method is used to get the counter inside accountStatus
    * 
    * @param accountStatus integer value
    * @return  the accountStatus's first 16-digit
    */
   public static int getCounter(int accountStatus)
   {

      accountStatus = accountStatus >> 16;
      return (accountStatus);
   }

   /**
    *  This method is used to increment the counter inside accountStatus
    * 
    * @param accountStatus integer value
    * @return   accountStatus
    */
   public static int incrementCounter(int accountStatus)
   {
      //first shift left most 16 bits to 16 right most bits then increment it by 1 
      //then shift right most 16-bits to the 16 bits left side 
      //then OR with the last 16 bits so it does not effected
      accountStatus = ((((accountStatus & 0xFFFF0000) >> 16) + 1) << 16) | (accountStatus & 0x0000FFFF);
      return (accountStatus);
   }

   /**
    *  This method is used to set the counter inside accountStatus
    * 
    * @param accountStatus integer value and valueToSet is the value for the counter to set
    * @return  the accountStatus's first 16-digit
    */
   public static int setCounter(int accountStatus, int valueToSet)
   {
      accountStatus = (valueToSet << 16) | (accountStatus & 0x0000FFFF);
      return (accountStatus);
   }

   /**
    *  This method is used to set the counter inside accountStatus
    * 
    * @param accountStatus integer value and valueToSet is the value for the counter to set
    * @return  the accountStatus's first 16-digit
    */
   public static int SetData(int accountStatus, int counter, boolean disabledForDay, boolean active, boolean valid, boolean Lock)
   {

      //setting the counter
      accountStatus = setCounter(accountStatus, counter);

      //checking and setting or reseting "disabled for day" flag
      if (disabledForDay)
         setDisabledForDay(accountStatus);
      else
         resetDisabledForDay(accountStatus);

      //checking and setting or reseting "active" flag
      if (active)
         setRegistered(accountStatus);
      else
         resetRegistered(accountStatus);

      //checking and setting or reseting "valid" flag
      if (valid)
         setValidate(accountStatus);
      else
         resetValidate(accountStatus);

      //checking and setting or reseting "Lock" flag
      if (Lock)
         setLock(accountStatus);
      else
         resetLock(accountStatus);
       
      //return status
      return accountStatus;
   }
   
   public static void main(String[] args)
   {
      System.out.println(18 & 0xFFFFFFEF);
      System.out.println((2 & 16) > 0 ? true : false);
      System.out.println(16|2);
   }

}
