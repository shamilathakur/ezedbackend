package com.intelli.ezed.checks;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.UserProfile;

public class RegistrationDataChecker extends SingletonRule
{

   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public RegistrationDataChecker(String ruleName)
   {
      super(ruleName);
   }

   //@ST ( 17/12/1014): puts all parameters passed into the user profile in flow context
   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      String email = messageData.getParameter("email");
      String key = messageData.getParameter("password");
      String type = messageData.getParameter("type");
      String regWith = messageData.getParameter("regwith"); //facebook, gmail
      String firstName = messageData.getParameter("firstname");
      String lastName = messageData.getParameter("lastname");
      String gender = messageData.getParameter("gender");
      String dateOfBirth = messageData.getParameter("dob");
      String invitedBy = messageData.getParameter("invitedby");
      String fbId = messageData.getParameter("fbid");
      String profileImage = messageData.getParameter("profileimage");

      if (email == null || email.equals("") || email.equals(" "))
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Email address not provided.");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Please enter correct email id.");
         return RuleDecisionKey.ON_FAILURE;
      }

      if (type == null || type.equals("") || type.equals(" "))
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Type not provided.");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Please enter type.");
         return RuleDecisionKey.ON_FAILURE;
      }

      int loginType = Integer.parseInt(type);

      if (loginType == 0)
      {
         if (key == null || key.equals("") || key.equals(" "))
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("key not provided.");
            }
            response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Please enter password.");
            return RuleDecisionKey.ON_FAILURE;
         }
      }

      if (loginType == 1)
      {
         if (regWith == null || regWith.equals("") || regWith.equals(" "))
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("regWith site name not provided.");
            }
            response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Please enter name of site.");
            return RuleDecisionKey.ON_FAILURE;
         }
      }

      if (firstName == null || firstName.trim().compareTo("") == 0 || firstName.trim().compareTo("null") == 0)
      {
         firstName = null;
      }
      else
      {
         firstName = firstName.trim();
      }

      if (lastName == null || lastName.trim().compareTo("") == 0 || lastName.trim().compareTo("null") == 0)
      {
         lastName = null;
      }
      else
      {
         lastName = lastName.trim();
      }

      if (gender == null || gender.trim().compareTo("") == 0 || gender.trim().compareTo("null") == 0)
      {
         gender = null;
      }
      else
      {
         if (gender.trim().compareToIgnoreCase("male") == 0 || gender.trim().compareToIgnoreCase("m") == 0)
         {
            gender = "M";
         }
         else
         {
            gender = "F";
         }
      }

      if (dateOfBirth == null || dateOfBirth.trim().compareTo("") == 0 || dateOfBirth.trim().compareTo("null") == 0)
      {
         dateOfBirth = null;
      }
      else
      {
         dateOfBirth = dateOfBirth.trim();
      }

      if (invitedBy == null || invitedBy.trim().compareTo("") == 0)
      {
         invitedBy = null;
      }

      if (fbId == null || fbId.trim().compareTo("") == 0)
      {
         fbId = null;
      }

      if (profileImage == null || profileImage.trim().compareTo("") == 0 || profileImage.trim().compareToIgnoreCase("null") == 0)
      {
         profileImage = null;
      }

      UserProfile userProfile = new UserProfile();
      userProfile.setUserid(email);
      userProfile.setPassword(key);
      userProfile.setRegWith(regWith);
      userProfile.setRegLoginType(loginType);
      userProfile.setFirstName(firstName);
      userProfile.setLastName(lastName);
      userProfile.setGender(gender);
      userProfile.setDateOfBirth(dateOfBirth);
      userProfile.setInvitedBy(invitedBy);
      userProfile.setFbId(fbId);
      userProfile.setProfileImage(profileImage);
      flowContext.putIntoContext(EzedKeyConstants.USER_DETAILS, userProfile);

      if (logger.isDebugEnabled())
      {
         logger.debug("Entered email id is : " + email + " and password is : " + key);
      }
      return RuleDecisionKey.ON_SUCCESS;
   }
}
