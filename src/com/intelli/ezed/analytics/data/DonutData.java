package com.intelli.ezed.analytics.data;

import org.json.JSONArray;

import com.intelli.ezed.data.ResponseObject;

public class DonutData extends ResponseObject
{
   private String[] categoris;
   private String name;
   private DonutOuterData[] data;

   public String[] getCategoris()
   {
      return categoris;
   }
   public void setCategoris(String[] categoris)
   {
      this.categoris = categoris;
   }
   public String getName()
   {
      return name;
   }
   public void setName(String name)
   {
      this.name = name;
   }
   public DonutOuterData[] getData()
   {
      return data;
   }
   public void setData(DonutOuterData[] data)
   {
      this.data = data;
   }

   private GenericChartData title = new GenericChartData();
   private GenericChartData xAxis = new GenericChartData();
   private GenericChartData yAxis = new GenericChartData();
   private GenericChartData type = new GenericChartData();
   
   
   public GenericChartData getTitle()
   {
      return title;
   }
   public void setTitle(GenericChartData title)
   {
      this.title = title;
   }
   public GenericChartData getxAxis()
   {
      return xAxis;
   }
   public void setxAxis(GenericChartData xAxis)
   {
      this.xAxis = xAxis;
   }
   public GenericChartData getyAxis()
   {
      return yAxis;
   }
   public void setyAxis(GenericChartData yAxis)
   {
      this.yAxis = yAxis;
   }
   public GenericChartData getType()
   {
      return type;
   }
   public void setType(GenericChartData type)
   {
      this.type = type;
   }
   
   public void prepareChart() throws Exception
   {
      type.prepareResponse();
      title.prepareResponse();
      yAxis.prepareResponse();
   }
   
   @Override
   public void prepareResponse() throws Exception
   {
      this.put("name", getName());
      this.put("categories", getCategoris());

      DonutOuterData[] donutOuterData = getData();
      for (int i = 0; i < donutOuterData.length; i++)
      {
         donutOuterData[i].prepareResponse();
      }

      this.put("data", donutOuterData);
      
      prepareChart();
      
      this.put("chart", type);
      this.put("title", title);
      this.put("yAxis", yAxis);

      JSONArray array = new JSONArray();
      
      GenericChartData one = new GenericChartData();
      GenericChartData two = new GenericChartData();
      
      one.getText().add("name");
      one.getValues().add("Chapters");
      one.getText().add("data");
      one.getValues().add("chapterData");
      one.getText().add("size");
      one.getValues().add("60%");
      one.prepareResponse();
      
      array.put(one);
      
      two.getText().add("innersize");
      two.getValues().add("60%");
      two.getText().add("name");
      two.getValues().add("questions");
      two.getText().add("data");
      two.getValues().add("questionsData");
      two.getText().add("size");
      two.getValues().add("80%");
      two.prepareResponse();
      
      array.put(two);
      
      this.put("series", array);
      
   }
}
