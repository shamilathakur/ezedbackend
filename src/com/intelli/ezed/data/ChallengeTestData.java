package com.intelli.ezed.data;

public class ChallengeTestData extends ResponseObject
{
   private String challengeId;
   private String challengeUserId;
   private int userStatus;

   public String getChallengeId()
   {
      return challengeId;
   }
   public void setChallengeId(String challengeId)
   {
      this.challengeId = challengeId;
   }
   public String getChallengeUserId()
   {
      return challengeUserId;
   }
   public void setChallengeUserId(String challengeUserId)
   {
      this.challengeUserId = challengeUserId;
   }
   public int getUserStatus()
   {
      return userStatus;
   }
   public void setUserStatus(int userStatus)
   {
      this.userStatus = userStatus;
   }
   @Override
   public void prepareResponse() throws Exception
   {
      this.put("challengeid", getChallengeId());
      this.put("challengeuser", getChallengeUserId());
      this.put("userstatus", getUserStatus());
   }
}
