package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class GroupAdminUsersAdder extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;

   public GroupAdminUsersAdder(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      String groupAdminId = messageData.getParameter("groupadminid");
      if (groupAdminId == null || groupAdminId.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Group admin id is invalid " + groupAdminId);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid group admin id");
         return RuleDecisionKey.ON_FAILURE;
      }

      String collegeName = messageData.getParameter("collegename");
      if (collegeName == null || collegeName.trim().compareTo("") == 0)
      {
         collegeName = null;
      }

      String univName = messageData.getParameter("univname");
      if (univName == null || univName.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("University name is invalid " + univName);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid university name");
         return RuleDecisionKey.ON_FAILURE;
      }

      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         if (collegeName != null)
         {
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertGroupUsersCollege");
            ps.setString(1, groupAdminId);
            ps.setString(2, collegeName);
         }
         else
         {
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertGroupUsersUniversity");
            ps.setString(1, groupAdminId);
            ps.setString(2, univName);
         }
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in db while inserting users for group admin " + groupAdminId);
         }
         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
         return RuleDecisionKey.ON_SUCCESS;
      }
      catch (SQLException e)
      {
         logger.error("Error while insering users for group admin " + groupAdminId, e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while adding users to group admin");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while insering users for group admin " + groupAdminId, e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while adding users to group admin");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
}
