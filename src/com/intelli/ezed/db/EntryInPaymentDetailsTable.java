package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.ManageUserCourseFromBackendBean;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.PaymentIdData;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.utils.EzedHelper;

public class EntryInPaymentDetailsTable extends SingletonRule {

	private String dbConnName;
	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

	public EntryInPaymentDetailsTable(String ruleName, String dbConnName) {
		super(ruleName);
		this.dbConnName = dbConnName;
	}

	@Override
	public RuleDecisionKey executeRule(FlowContext flowContext) {

		Response response = (Response) flowContext
				.getFromContext(EzedKeyConstants.EZED_RESPONSE);
		ManageUserCourseFromBackendBean data = (ManageUserCourseFromBackendBean) flowContext
				.getFromContext(EzedKeyConstants.ADD_COURSE_TO_USER_FROM_ADMIN);
		PaymentIdData data1 = (PaymentIdData) flowContext
				.getFromContext(EzedKeyConstants.CART_PAYMENT_DATA);

		System.out.println("In Class : " + this.getClass().getName());
		System.out.print("USERID : " + data.getUserId() + "  Sessionid : "
				+ data.getSessionId());

		logger.info("Entry In paymentdetails table will be done now.");

		int rs;
		PreparedStatement ps = null;
		Connection conn = DbConnectionManager.getConnectionByName(dbConnName);

		try {
			ps = DbConnectionManager.getPreparedStatement(dbConnName, conn,
					"EntryInPaymentDetails");
			ps.setString(1, EzedHelper.getPaymentDetailId(dbConnName, logger));
			ps.setString(2, data.getUserId());
			ps.setString(3, (data1.getAppId() + data1.getPaymentId()));
			ps.setInt(4, data.getPaidAmount());
			ps.setString(5, "0");
			ps.setString(6, "Added From Backend");
			ps.setString(7, "Added From Backend");
			ps.setString(8, "Y");
			System.out
					.println("Query for making entry in paymentdetails table : "
							+ ps);
			rs = ps.executeUpdate();
			if (rs > 0) {
				logger.info("Entry successful in paymentdetails table."
						+ data1.getPaymentId());
			} else {
				logger.info("Some error occured while making the payment entry for paymentid : "
						+ data1.getPaymentId());
			}
		} catch (SQLException e) {

			System.out.println("SQL Error In Class : "
					+ this.getClass().getName());
			if (logger.isDebugEnabled()) {
				logger.error("SQL Error occured while making the entry in paymentdetail table for paymentid : "
						+ data1.getPaymentId());
			}
			response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED,
					"DB exception occured while making the payment entry for BackendAddCourse");
			return RuleDecisionKey.ON_FAILURE;
		} catch (Exception e) {
			if (logger.isDebugEnabled()) {
				logger.error("Internal Error occured while making entry in paymentdetail table for paymentid : "
						+ data1.getPaymentId());
			}
			response.setParameters(EzedTransactionCodes.INTERNAL_ERROR,
					"Internal exception occured while making entry in paymentdetail table.");
			return RuleDecisionKey.ON_FAILURE;
		}

		return RuleDecisionKey.ON_SUCCESS;

	}
}
