/*
 * Last changed by           : $Author: indraneeld $
 * Last check in             : $Date: 2012/06/26 06:46:53 $
 * CVS version number        : $Revision: 1.1 $
 * CVS archive file location : $Archive$
 *
 * Copyright Notice
 * ================
 * Copyright (c) 2006 - 2007 SABRE Development Group
 * Copying or reproduction without prior written approval is prohibited.
 * SABRE Group PROPRIETARY/CONFIDENTIAL. Use is subject to approval from the SABRE group.
 * 
 */
package com.intelli.ezed.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MbcLoggerNames;

public class TicketGenerator extends GenericTicketGenerator {

	private static Logger logger = LoggerFactory
			.getLogger(MbcLoggerNames.SERVICE_LOG);
	private static TicketGenerator ticketGeneratorInstance = null;
	private static TicketGenerator ticketGeneratorInstance1 = null;

	private TicketGenerator(long rolloverVal, int nodeId, long initValue) {
		super(rolloverVal, nodeId, initValue);

	}

	public static TicketGenerator getInstance(long rollOverVal, int nodeId,
			long initValue) {
		if (ticketGeneratorInstance == null) {
			ticketGeneratorInstance = new TicketGenerator(rollOverVal, nodeId,
					initValue);
		}
		return ticketGeneratorInstance;
	}

	

	/** Added by kunal on 26/02/2015 for scratch card module */

	public static void main(String[] args) {

		// byte[][] s = new byte[1000000][];
		TicketGenerator gtg = TicketGenerator.getInstance(9999999999L, 1, 100);
		// long startTime = System.nanoTime();
		// long startTimeM = System.currentTimeMillis();
		GenericTicketKey key = new GenericTicketKey(21, false, 25);

		for (int i = 0; i < 100; i++) {
			// s[i] = gtg.getNextEdgeTicketFor(key).getBytes();
			if (logger.isInfoEnabled()) {
				logger.info(gtg.getNextEdgeTicketFor(key));
			}
		}
		// long endTime = System.nanoTime();
		// long endTimeM = System.currentTimeMillis();
		//
		// for(int i=0; i<1000000; i++)
		// {
		// System.out.println(s[i]);
		//
		// }
		//
		// System.out.println(" Total time = "+(endTime - startTime));
		// System.out.println(" Total time millis= "+(endTimeM - startTimeM));
	}

}
