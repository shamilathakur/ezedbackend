package com.intelli.ezed.db;


import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.UserProfile;
import com.intelli.ezed.data.UserStatusData;
import com.intelli.ezed.utils.AccountStatusHelper;

public class UserStatusFlagsFetcher extends SingletonRule
{

   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;

   private String REG = "REG";
   private String NOTREG = "NOTREG";
   private String AUTH = "AUTH";
   private String UNAUTH = "UNAUTH";
   private String BLOCK = "BLOCK";
   private String UNBLOCK = "UNBLOCK";

   public UserStatusFlagsFetcher(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      UserProfile userProfile = new UserProfile();

      String fetchUser = messageData.getParameter("fetchuser");

      userProfile.setUserid(fetchUser);

      try
      {
         userProfile.populateUserProfile(dbConnName, logger);
      }
      catch (Exception e)
      {
         logger.error("Exception orrcoured, ", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Please check the userid.");
         return RuleDecisionKey.ON_FAILURE;
      }

      
      int chkflag = userProfile.getChkflag();
      logger.debug("CHKFLAG : " + chkflag);

      StringBuffer sb = new StringBuffer();

      if(AccountStatusHelper.isRegistered(chkflag) == true)
      {
         sb.append(REG);
      }
      else
      {
         sb.append(NOTREG);
      }
      
      if (AccountStatusHelper.isValidate(chkflag) == true)
      {
         sb.append(",").append(AUTH);
      }
      else
      {
         sb.append(",").append(UNAUTH);
      }

      if (AccountStatusHelper.isLock(chkflag) == true)
      {
         sb.append(",").append(BLOCK);
      }
      else
      {
         sb.append(",").append(UNBLOCK);
      }

      UserStatusData userStatusData = new UserStatusData();
      userStatusData.setUserid(fetchUser);
      userStatusData.setRoll(sb.toString());
      response.setResponseObject(userStatusData);
      response.setParameters(EzedTransactionCodes.SUCCESS, "Status returned.");

      return RuleDecisionKey.ON_SUCCESS;
   }

   public static void main(String[] args)
   {
      int chkflag = 2;
      if (AccountStatusHelper.isValidate(chkflag) == true)
      {
         System.out.println("AUTH");
      }
      else
      {
         System.out.println("unAUTH");
      }

      if (AccountStatusHelper.isLock(chkflag) == true)
      {
         System.out.println("BLOCK");
      }
      else
      {
         System.out.println("UNBLOCK");
      }

   }
   
}
