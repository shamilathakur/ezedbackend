package com.intelli.ezed.analytics.data;

import java.util.ArrayList;

import com.intelli.ezed.data.ResponseObject;

public class SingleTestSummaryData extends ResponseObject
{
   private String testId;
   private String testName;
   private ArrayList<SingleSummaryData> testSummaries = new ArrayList<SingleSummaryData>();

   public ArrayList<SingleSummaryData> getTestSummaries()
   {
      return testSummaries;
   }
   public void setTestSummaries(ArrayList<SingleSummaryData> testSummaries)
   {
      this.testSummaries = testSummaries;
   }

   public String getTestId()
   {
      return testId;
   }
   public void setTestId(String testId)
   {
      this.testId = testId;
   }
   @Override
   public void prepareResponse() throws Exception
   {
      this.put("name", "Correct percentage for " + getTestName());
      float datas[] = new float[getTestSummaries().size()];
      for (int i = 0; i < datas.length; i++)
      {
         datas[i] = getTestSummaries().get(i).getCorrectPercent();
      }
      this.put("data", datas);
   }
   public String getTestName()
   {
      return testName;
   }
   public void setTestName(String testName)
   {
      this.testName = testName;
   }
}
