package com.intelli.ezed.data;

public class ContentData extends ResponseObject{
	
	String contentId;
	String contentName;
	String contentLink;
	String contentDescription;
	int contentType;
	
	public String getContentId() {
		return contentId;
	}
	public void setContentId(String contentId) {
		this.contentId = contentId;
	}
	public String getContentName() {
		return contentName;
	}
	public void setContentName(String contentName) {
		this.contentName = contentName;
	}
	public String getContentLink() {
		return contentLink;
	}
	public void setContentLink(String contentLink) {
		this.contentLink = contentLink;
	}
	public String getContentDescription() {
		return contentDescription;
	}
	public void setContentDescription(String contentDescription) {
		this.contentDescription = contentDescription;
	}
	public int getContentType() {
		return contentType;
	}
	public void setContentType(int contentType) {
		this.contentType = contentType;
	}
	@Override
	   public void prepareResponse() throws Exception
	   {
	      this.put("contentid", getContentId());
	      this.put("contentname", getContentName());
	      this.put("link", getContentLink());
	      this.put("description", getContentDescription());
	      this.put("contenttype", getContentType());
	   }
	
	
}
