package com.intelli.ezed.data;

public class TestResultResponse extends ResponseObject
{
   private String correctAns;
   private String finalScore;
   
   QuizRequestResponse requestResponse = new QuizRequestResponse();
   
   
   
   public QuizRequestResponse getRequestResponse()
   {
      return requestResponse;
   }

   public void setRequestResponse(QuizRequestResponse requestResponse)
   {
      this.requestResponse = requestResponse;
   }

   public String getCorrectAns()
   {
      return correctAns;
   }

   public void setCorrectAns(String correctAns)
   {
      this.correctAns = correctAns;
   }

   public String getFinalScore()
   {
      return finalScore;
   }

   public void setFinalScore(String finalScore)
   {
      this.finalScore = finalScore;
   }

   @Override
   public void prepareResponse() throws Exception
   {
      this.put("correctAnswers", getCorrectAns());
      this.put("finalScore", getFinalScore());
      
      QuizRequestResponse quizRequestResponse = getRequestResponse();
      quizRequestResponse.prepareResponse();
      
      this.put("testresultdetails", quizRequestResponse);
   }
}
