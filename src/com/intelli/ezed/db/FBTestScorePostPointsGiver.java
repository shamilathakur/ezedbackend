package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.ActionData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.MasterActionRecords;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.UserProfile;
import com.intelli.ezed.utils.EzedHelper;

public class FBTestScorePostPointsGiver extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String actionId;
   private static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

   public FBTestScorePostPointsGiver(String ruleName, String dbConnName, String actionId)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
      this.actionId = actionId;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      UserProfile userProfile = (UserProfile) flowContext.getFromContext(EzedKeyConstants.USER_DETAILS);
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      String testId = messageData.getParameter("testid");
      if (testId == null || testId.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Test id is null " + testId);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Test id is invalid");
         return RuleDecisionKey.ON_FAILURE;
      }

      String courseId = messageData.getParameter("courseid");
      if (courseId == null || courseId.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Course id is null or invalid " + courseId);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid course id");
         return RuleDecisionKey.ON_FAILURE;
      }

      ActionData actionData = MasterActionRecords.fetchRecord(actionId);

      String testName = EzedHelper.fetchTestNameForTestId(dbConnName, testId, logger);
      String courseName = EzedHelper.fetchCourseNameForCourseId(dbConnName, courseId, logger);

      StringBuffer sb = new StringBuffer();
      sb.append(userProfile.getName());
      sb.append("posted score on Facebook of ");
      sb.append(testName);
      sb.append(" test in course ");
      sb.append(courseName);
      String fbPostScoreStream = sb.toString();

      Connection conn = null;
      PreparedStatement ps = null;
      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdateCoursePoints");
         ps.setInt(1, actionData.getPoints());
         ps.setString(2, userProfile.getUserid());
         ps.setString(3, courseId);
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows updated in db while incrementing points for userid " + userProfile.getUserid() + " and course " + courseId);
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while incrementing points for userid " + userProfile.getUserid() + " and course " + courseId, e);
      }
      catch (Exception e)
      {
         logger.error("Error while incrementing points for userid " + userProfile.getUserid() + " and course " + courseId, e);
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }


      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdateUserPoints");
         ps.setInt(1, actionData.getPoints());
         ps.setString(2, userProfile.getUserid());
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows updated in db while incrementing points for userid " + userProfile.getUserid());
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while incrementing points for userid " + userProfile.getUserid(), e);
      }
      catch (Exception e)
      {
         logger.error("Error while incrementing points for userid " + userProfile.getUserid(), e);
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertUserStreamAll");
         ps.setString(1, fbPostScoreStream);
         ps.setString(2, userProfile.getUserid());
         ps.setString(3, sdf.format(new Date()));
         ps.setString(4, courseId);
         ps.setString(5, null);
         ps.setString(6, testId);
         ps.setString(7, actionId);
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in db while inserting stream for user " + userProfile.getUserid());
         }
      }
      catch (Exception e)
      {
         logger.error("Error inserting stream entry for " + userProfile.getUserid(), e);
         logger.error("INSERT FAILED " + fbPostScoreStream);
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
      return RuleDecisionKey.ON_SUCCESS;
   }

}
