package com.intelli.ezed.bulkupload.rules;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.bulkupload.data.CourseDetails;
import com.intelli.ezed.bulkupload.data.TestDetails;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.TestQuestion;

public class CourseReaderDumper extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.BULK_UPLOAD_LOG);
   private String dbConnName;
   private String rootUnivDir;
   private String courseDetailsFileName;
   private String chapterDetailFileName;
   private String questionBankFileName;

   public CourseReaderDumper(String ruleName, String dbConnName, String rootUnivDir, String courseDetailsFileName,
                             String chapterDetailFileName, String questionBankFileName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
      this.rootUnivDir = rootUnivDir;
      this.courseDetailsFileName = courseDetailsFileName;
      this.chapterDetailFileName = chapterDetailFileName;
      this.questionBankFileName = questionBankFileName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;

      File file = new File(rootUnivDir);
      if (!file.exists())
      {
         file.mkdir();
      }
      ArrayList<CourseDetails> datas = new ArrayList<CourseDetails>();
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchCourseUnivDetails");
         rs = ps.executeQuery();
         CourseDetails data = null;
         while (rs.next())
         {
            data = new CourseDetails();
            data.setCourseId(rs.getString("courseid"));
            data.setCourseName(rs.getString("coursename"));
            data.setShortDesc(rs.getString("shortdesc"));
            data.setIntroVid(rs.getString("introvideo"));
            data.setDefChapter(rs.getString("defchapter"));
            data.setLmr(rs.getString("lmr"));
            data.setContentCost(rs.getString("contentonlyamt"));
            data.setTestCost(rs.getString("testonlyamt"));
            data.setTotalCost(rs.getString("fullcourseamt"));
            data.setUnivQuestPaper(rs.getString("questionpaperid"));
            data.setUnivSolution(rs.getString("solvedpaperid"));
            data.setEndDate(rs.getString("enddate"));
            data.setLongDescType(rs.getInt("longdesctype"));
            data.setLongDescString(rs.getString("longdescstring"));
            data.setLongDescContent(rs.getString("longdesccontent"));
            data.setDegree(rs.getString("degree"));
            data.setStream(rs.getString("stream"));
            data.setYear(rs.getString("year"));
            data.setSemester(rs.getString("semester"));
            data.setUnivname(rs.getString("univname"));
            datas.add(data);
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching course details from db", e);
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching course details from db", e);
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      CourseDetails data = null;
      for (int i = 0; i < datas.size(); i++)
      {
         data = datas.get(i);
         try
         {
            data.populateChaptersFromDb(dbConnName, logger);
            ArrayList<TestDetails> tests = data.populateTestsFromDb(dbConnName, logger);
            data.writeToFile(rootUnivDir, courseDetailsFileName, tests);
            data.fetchAllMemberChapterDetails(dbConnName, logger, chapterDetailFileName, questionBankFileName);
            ArrayList<TestQuestion> testQuestions = data.populateQuestionBankFromDb(dbConnName, logger);
            if (testQuestions.size() != 0)
            {
               data.writeQuestions(data.getCourseDirName(), questionBankFileName, testQuestions);
            }
         }
         catch (SQLException e)
         {
            logger.error("Error while fetching course details from db", e);
            return RuleDecisionKey.ON_FAILURE;
         }
         catch (Exception e)
         {
            logger.error("Error while fetching course details from db", e);
            return RuleDecisionKey.ON_FAILURE;
         }
      }
      return RuleDecisionKey.ON_SUCCESS;
   }

}
