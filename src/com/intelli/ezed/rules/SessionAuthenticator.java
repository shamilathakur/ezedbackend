package com.intelli.ezed.rules;

import java.util.HashSet;
import java.util.Set;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.session.SessionException;
import com.a.helper.session.SessionManager;
import com.a.helper.utils.StringLockerUtil;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.utils.Globals;

public class SessionAuthenticator extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public SessionAuthenticator(String ruleName)
   {
      super(ruleName);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      SessionManager sessionManager = (SessionManager) flowContext.getFromContext(EzedKeyConstants.SESSION_MANAGER);
      String emailid = (String) flowContext.getFromContext(EzedKeyConstants.USER_ID);
      String sessionId = (String) flowContext.getFromContext(EzedKeyConstants.SESSION_ID);
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      if (sessionId == null || sessionId.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Session id is null for email id " + emailid);
         }
         response.setParameters(EzedTransactionCodes.SESSION_ERROR_OCCURRED, "Session id received for auth is null");
         return RuleDecisionKey.ON_FAILURE;
      }
      try
      {
         StringLockerUtil.lockString(sessionId, Globals.USER_SESSION_LOCK_SET);
      }
      catch (InterruptedException e1)
      {
         logger.error("Error while locking session. ID " + sessionId, e1);
         response.setParameters(EzedTransactionCodes.SESSION_ERROR_OCCURRED, "Invalid session id.");
         return RuleDecisionKey.ON_FAILURE;
      }
      Set<String> keySet = new HashSet<String>();
      keySet.add(EzedKeyConstants.SESSION_ID);
      try
      {
         String sessionIdInPersister = (String) sessionManager.checkDataFromSession(emailid, keySet).get(EzedKeyConstants.SESSION_ID);
         if (sessionIdInPersister == null || sessionIdInPersister.trim().compareTo("") == 0)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Session id is null in persister for email id " + emailid);
            }
            response.setParameters(EzedTransactionCodes.SESSION_ERROR_OCCURRED, "Session id received for auth is null");
            StringLockerUtil.unlockString(sessionId, Globals.USER_SESSION_LOCK_SET);
            return RuleDecisionKey.ON_FAILURE;
         }
         if (sessionId.compareToIgnoreCase(sessionIdInPersister) == 0)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Session id matches for received request and persisted session");
            }
            Set<String> contextKeySet = new HashSet<String>();
            contextKeySet.add(EzedKeyConstants.REQUEST_IN_TIME);
            contextKeySet.add(EzedKeyConstants.USER_DETAILS);
            contextKeySet.add(EzedKeyConstants.SESSION_ID);
            sessionManager.getDataFromSession(emailid, contextKeySet, flowContext);
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Session id does not match " + emailid);
            }
            response.setParameters(EzedTransactionCodes.SESSION_ERROR_OCCURRED, "Invalid session id.");
            StringLockerUtil.unlockString(sessionId, Globals.USER_SESSION_LOCK_SET);
            return RuleDecisionKey.ON_FAILURE;
         }
      }
      catch (SessionException e)
      {
         logger.error("Session error occurred for email id " + emailid, e);
         response.setParameters(EzedTransactionCodes.SESSION_ERROR_OCCURRED, "Session not found");
         StringLockerUtil.unlockString(sessionId, Globals.USER_SESSION_LOCK_SET);
         return RuleDecisionKey.ON_FAILURE;
      }
      return RuleDecisionKey.ON_SUCCESS;
   }

}
