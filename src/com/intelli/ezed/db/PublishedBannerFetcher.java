package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.BannerData;
import com.intelli.ezed.data.BannerDataList;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class PublishedBannerFetcher extends SingletonRule
{

   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public PublishedBannerFetcher(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;

      ArrayList<BannerData> banners = new ArrayList<BannerData>();

      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchBannerDetails");
         rs = ps.executeQuery();
         int bannerStatus = 0;
         while (rs.next())
         {
            BannerData bannerData = new BannerData();

            bannerStatus = rs.getInt("bannerstatus");
            if (bannerStatus == 0)
            {
               bannerData.setBannerstatus(BannerData.PUBLISHED_BANNER);
            }
            else
            {
               continue;
               //bannerData.setBannerstatus(BannerData.NOT_PUBLISHED_BANNER);
            }


            bannerData.setBannerdesc(rs.getString("bannerdesc"));
            bannerData.setBannerid(rs.getString("bannerid"));
            bannerData.setBannertitle(rs.getString("bannertitle"));
            bannerData.setBannerimage(rs.getString("bannerimage"));
            bannerData.setBannerType(rs.getInt("bannertype"));
            bannerData.setBannerUrl(rs.getString("bannerurl"));

            //            if (bannerImg == null)
            //            {
            //               bannerData.setBannerimage("VIDEO");
            //            }
            //            else
            //            {
            //               bannerData.setBannerimage("IMAGE");
            //            }

            banners.add(bannerData);
         }
         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
      }
      catch (SQLException e)
      {
         logger.error("DB error while fetching list of banners", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "DB error while fetching banners");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching list of banners", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching banners");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing result set while fetching banners list", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      BannerDataList bannerDataList = new BannerDataList();
      bannerDataList.setBannerData(banners);
      response.setResponseObject(bannerDataList);

      return RuleDecisionKey.ON_SUCCESS;
   }

}
