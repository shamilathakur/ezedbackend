package com.intelli.ezed.db;

import java.sql.SQLException;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.AdminProfile;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class AdminProfileGetter extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public AdminProfileGetter(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      String userid = messageData.getParameter("email");
      if (userid == null || userid.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Email id is invalid " + userid);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid email id");
         return RuleDecisionKey.ON_FAILURE;
      }
      //      String pwd = messageData.getParameter("password");
      //      if (pwd == null || pwd.trim().compareTo("") == 0)
      //      {
      //         if (logger.isDebugEnabled())
      //         {
      //            logger.debug("Password is invalid " + pwd);
      //         }
      //         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid password");
      //         return RuleDecisionKey.ON_FAILURE;
      //      }

      AdminProfile adminProfile = new AdminProfile();
      adminProfile.setUserid(userid);
      //      adminProfile.setPassword(pwd);
      try
      {
         adminProfile.fetchAdminProfile(dbConnName, logger);
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching admin profile", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching admin profile");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching admin profile", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching admin profile");
         return RuleDecisionKey.ON_FAILURE;
      }
      flowContext.putIntoContext(EzedKeyConstants.ADMIN_PROFILE, adminProfile);


      if (logger.isDebugEnabled())
      {
         logger.debug("Fetched user profile successfully for userid :" + userid);
      }
      response.setResponseMessage("Admin profile fetched from db.");
      return RuleDecisionKey.ON_SUCCESS;
   }
}
