package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.GoalData;

public class GoalFailedMarker extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

   public GoalFailedMarker(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      String userId = (String) flowContext.getFromContext(EzedKeyConstants.USER_ID);
      GoalData goalData = (GoalData) flowContext.getFromContext(EzedKeyConstants.EXPIRED_GOAL);

      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         if (goalData.getChapterId() != null)
         {
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdateGoalFailed");
            ps.setInt(1, GoalData.GOAL_FAILED);
            ps.setString(2, userId);
            ps.setString(3, goalData.getCourseId());
            ps.setString(4, goalData.getChapterId());
            ps.setString(5, goalData.getContentId());
         }
         else
         {
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdateGoalFailedTest");
            ps.setInt(1, GoalData.GOAL_FAILED);
            ps.setString(2, userId);
            ps.setString(3, goalData.getCourseId());
            ps.setString(4, goalData.getContentId());
         }
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows updated in db while updating goal as failed for user id " + userId);
         }
      }
      catch (Exception e)
      {
         logger.error("Error while updating goal as failed in db", e);
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertFailedGoals");
         ps.setString(1, userId);
         ps.setString(2, goalData.getCourseId());
         ps.setString(3, goalData.getChapterId());
         ps.setString(4, goalData.getContentId());
         ps.setString(5, sdf.format(goalData.getEndDate()));
         ps.setString(6, sdf.format(new Date()));
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in db while inserting failed goals");
         }
      }
      catch (Exception e)
      {
         logger.error("Error while inserting failed goal in db", e);
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      return RuleDecisionKey.ON_SUCCESS;
   }

}
