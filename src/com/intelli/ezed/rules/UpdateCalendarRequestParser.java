package com.intelli.ezed.rules;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.a.helper.utils.StringSplitter;
import com.intelli.ezed.data.CourseCalendarData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.GoalData;
import com.intelli.ezed.data.Response;

public class UpdateCalendarRequestParser extends SingletonRule
{
   private SimpleDateFormat sdf = new SimpleDateFormat("MM~dd~yyyy");
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public UpdateCalendarRequestParser(String ruleName)
   {
      super(ruleName);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      String descList = messageData.getParameter("desc");
      if (descList == null || descList.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Goal Descriptions in request is missing or blank");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid goal description");
         return RuleDecisionKey.ON_FAILURE;
      }

      String dateList = messageData.getParameter("date");
      if (dateList == null || dateList.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Date List in request is missing or blank");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid goal dates");
         return RuleDecisionKey.ON_FAILURE;
      }

      String[] desc = StringSplitter.splitString(descList, "|");
      String compareDesc = desc[0];
      for (String oneDesc : desc)
      {
         //         if (oneDesc.trim().length() < 60)
         //         {
         //            if (logger.isDebugEnabled())
         //            {
         //               logger.debug("One of the goal descriptions is invalid " + oneDesc);
         //            }
         //            response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid goal description");
         //            return RuleDecisionKey.ON_FAILURE;
         //         }

         try
         {
            if (oneDesc.substring(0, oneDesc.indexOf("CHA")).compareTo(compareDesc.substring(0, compareDesc.indexOf("CHA"))) != 0)
            {
               if (logger.isDebugEnabled())
               {
                  logger.debug("Course Id's cannot be different across same update request");
               }
               response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Different course id's in same request");
               return RuleDecisionKey.ON_FAILURE;
            }
         }
         catch (Exception e)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("One of the goal descriptions is invalid " + oneDesc);
            }
            response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid goal description");
            return RuleDecisionKey.ON_FAILURE;
         }
      }

      String[] datesString = StringSplitter.splitString(dateList, "|");
      Date[] dates = new Date[datesString.length];
      String singleDateString = null;
      for (int i = 0; i < datesString.length; i++)
      {
         singleDateString = datesString[i];
         try
         {
            dates[i] = sdf.parse(singleDateString.trim());
         }
         catch (ParseException e)
         {
            logger.error("One of the dates is in improper format " + singleDateString, e);
            response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid date");
            return RuleDecisionKey.ON_FAILURE;
         }
      }

      if (dates.length != desc.length)
      {
         logger.error("No of dates and descriptions in request do not match");
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Improper request. No of dates and descriptions do not match");
         return RuleDecisionKey.ON_FAILURE;
      }

      GoalData singleGoalData = null;
      ArrayList<GoalData> goalDatas = new ArrayList<GoalData>();
      CourseCalendarData courseCalendarData = new CourseCalendarData();
      String singleCourseId = null;
      String singleContentId = null;
      String singleChapterId = null;
      for (int i = 0; i < desc.length; i++)
      {
         try
         {
            singleCourseId = desc[i].substring(0, desc[i].indexOf("CHA"));
            if (desc[i].contains("TQU"))
            {
               singleChapterId = desc[i].substring(desc[i].indexOf("CHA"), desc[i].indexOf("TQU"));
               singleContentId = desc[i].substring(desc[i].indexOf("TQU"), desc[i].length());
            }
            else
            {
               singleChapterId = desc[i].substring(desc[i].indexOf("CHA"), desc[i].indexOf("CON"));
               singleContentId = desc[i].substring(desc[i].indexOf("CON"), desc[i].length());
            }
            if (singleChapterId.compareToIgnoreCase("CHACCCCCCC") == 0)
            {
               singleChapterId = null;
            }
         }
         catch (Exception e)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("One of the goal descriptions is invalid " + desc[i]);
            }
            response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid goal description");
            return RuleDecisionKey.ON_FAILURE;
         }
         singleGoalData = new GoalData();
         singleGoalData.setCourseId(singleCourseId);
         singleGoalData.setChapterId(singleChapterId);
         singleGoalData.setContentId(singleContentId);
         singleGoalData.setEndDate(dates[i]);
         goalDatas.add(singleGoalData);
      }
      courseCalendarData.setCourseId(singleCourseId);
      courseCalendarData.setGoalDatas(goalDatas);
      flowContext.putIntoContext(EzedKeyConstants.COURSE_CALENDAR, courseCalendarData);
      return RuleDecisionKey.ON_SUCCESS;
   }

   public static void main(String[] args)
   {
      String abc = "COU123CHA123CON123";
      System.out.println(abc.substring(0, abc.indexOf("CHA")));
      System.out.println(abc.substring(abc.indexOf("CHA"), abc.indexOf("CON")));
      System.out.println(abc.substring(abc.indexOf("CON"), abc.length()));
   }
}
