package com.intelli.ezed.checks;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.ScratchCardData;

public class GenerateScratchCardRequestParser extends SingletonRule {

	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

	public GenerateScratchCardRequestParser(String ruleName) {
		super(ruleName);
	}

	@Override
	public RuleDecisionKey executeRule(FlowContext flowContext) {
		MessageData messageData = (MessageData) flowContext
				.getFromContext(MessageData.class.getName());
		Response response = (Response) flowContext
				.getFromContext(EzedKeyConstants.EZED_RESPONSE);
		ScratchCardData data = new ScratchCardData();
		String value;

		value = messageData.getParameter("courseid");
		if (value == null || value.trim().compareTo("") == 0
				|| value.trim().compareToIgnoreCase("null") == 0) {
			if (logger.isDebugEnabled()) {
				logger.debug("CourseID is invalid");
			}
			response.setParameters(
					EzedTransactionCodes.INVALID_MESSAGE_PARAMETER,
					"Course id is invalid");
			return RuleDecisionKey.ON_FAILURE;
		}
		data.setCourseId(value);

		value = messageData.getParameter("coursetype");
		if (value == null || value.trim().compareTo("") == 0
				|| value.trim().compareToIgnoreCase("null") == 0) {
			if (logger.isDebugEnabled()) {
				logger.debug("Course Type is invalid");
			}
			response.setParameters(
					EzedTransactionCodes.INVALID_MESSAGE_PARAMETER,
					"Course Type is invalid");
			return RuleDecisionKey.ON_FAILURE;
		}
		data.setCourseType(Integer.parseInt(value));

		value = messageData.getParameter("validtill");
		if (value == null || value.trim().compareTo("") == 0
				|| value.trim().compareToIgnoreCase("null") == 0) {
			if (logger.isDebugEnabled()) {
				logger.debug("Valid till date is invalid");
			}
			response.setParameters(
					EzedTransactionCodes.INVALID_MESSAGE_PARAMETER,
					"Valid till date is invalid");
			return RuleDecisionKey.ON_FAILURE;
		}
		data.setValidTill(value);

		value = messageData.getParameter("price");
		if (value == null || value.trim().compareTo("") == 0
				|| value.trim().compareToIgnoreCase("null") == 0) {
			if (logger.isDebugEnabled()) {
				logger.debug("Price is invalid");
			}
			response.setParameters(
					EzedTransactionCodes.INVALID_MESSAGE_PARAMETER,
					"Price is invalid");
			return RuleDecisionKey.ON_FAILURE;
		}
		data.setPrice(Integer.parseInt(value));

		value = messageData.getParameter("noofcards");
		if (value == null || value.trim().compareTo("") == 0
				|| value.trim().compareToIgnoreCase("null") == 0) {
			if (logger.isDebugEnabled()) {
				logger.debug("No of cards is invalid");
			}
			response.setParameters(
					EzedTransactionCodes.INVALID_MESSAGE_PARAMETER,
					"No of cards is invalid");
			return RuleDecisionKey.ON_FAILURE;
		}
		data.setNoOfCards(Integer.parseInt(value));

		value = messageData.getParameter("assignedstatus");
		if (value == null || value.trim().compareTo("") == 0
				|| value.trim().compareToIgnoreCase("null") == 0) {
			if (logger.isDebugEnabled()) {
				logger.debug("Assigned Status is invalid");
			}
			response.setParameters(
					EzedTransactionCodes.INVALID_MESSAGE_PARAMETER,
					"Assighned status is invalid");
			return RuleDecisionKey.ON_FAILURE;
		}
		data.setAssignedStatus(Integer.parseInt(value));

		if (data.getAssignedStatus() == 1) {
			value = messageData.getParameter("partnerid");
			if (value == null || value.trim().compareTo("") == 0
					|| value.trim().compareToIgnoreCase("null") == 0) {
				if (logger.isDebugEnabled()) {
					logger.debug("Partner id is invalid");
				}
				response.setParameters(
						EzedTransactionCodes.INVALID_MESSAGE_PARAMETER,
						"Partner id is invalid");
				return RuleDecisionKey.ON_FAILURE;
			}
			data.setAssignedTo(value);
		}

		String generateDate = sdf.format(new Date());
		data.setGenerateDate(generateDate);
		data.setLastModified(generateDate);

		data.setCardActiveStatus(1);
		
		String generatedBy = messageData.getParameter("email");
		data.setGeneratedBy(generatedBy);
		
		data.setLastModifiedBy(messageData.getParameter("email"));

		flowContext.putIntoContext(EzedKeyConstants.SCRATCH_CARD_OBJECT, data);
		return RuleDecisionKey.ON_SUCCESS;
	}
}