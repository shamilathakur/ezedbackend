package com.intelli.ezed.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.slf4j.Logger;

import com.intelli.ezed.bulkupload.data.ChapterDetails;
import com.intelli.ezed.utils.EzedHelper;

public class SingleCourseGoalModificationData
{
   private String actionId;
   private String userId;
   private String courseId;
   private String courseType;
   private Date startDate;
   private Date endDate;
   private ArrayList<GoalData> goalDatas = new ArrayList<GoalData>();
   private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
   private static final SimpleDateFormat sdfCompare = new SimpleDateFormat("yyyyMMdd");

   public String getUserId()
   {
      return userId;
   }
   public void setUserId(String userId)
   {
      this.userId = userId;
   }
   public String getCourseId()
   {
      return courseId;
   }
   public void setCourseId(String courseId)
   {
      this.courseId = courseId;
   }
   public String getCourseType()
   {
      return courseType;
   }
   public void setCourseType(String courseType)
   {
      this.courseType = courseType;
   }
   public Date getStartDate()
   {
      return startDate;
   }
   public void setStartDate(Date startDate)
   {
      this.startDate = startDate;
   }
   public Date getEndDate()
   {
      return endDate;
   }
   public void setEndDate(Date endDate)
   {
      this.endDate = endDate;
   }
   public ArrayList<GoalData> getGoalDatas()
   {
      return goalDatas;
   }
   public void setGoalDatas(ArrayList<GoalData> goalDatas)
   {
      this.goalDatas = goalDatas;
   }

   public void handleNewGoals(String dbConnName, Logger logger, float avgNoOfHours) throws SQLException, Exception
   {
      getMaxGoalDate(dbConnName, logger);
      calculateTimeLine(avgNoOfHours);
      persistModifiedGoals(dbConnName, logger);
      try
      {
         persistForCompletionPercentReCalculation(dbConnName, logger);
      }
      catch (Exception e)
      {
         logger.error("Error while persisting for completion recalculation. No forwarding exception", e);
      }

   }

   private void calculateTimeLine(float avgNoOfMinutes)
   {
      float fractionOfHoursPerDay = EzedHelper.calculateEndDateViaDefHours(avgNoOfMinutes, getGoalDatas(), startDate, endDate);
      if (fractionOfHoursPerDay == 0)
      {
         //start date before enddate
         for (int j = 0; j < goalDatas.size(); j++)
         {
            goalDatas.get(j).setEndDate(startDate);
         }
         return;
      }
      int noOfHoursPerDay = (int) fractionOfHoursPerDay;
      if (noOfHoursPerDay < fractionOfHoursPerDay)
      {
         noOfHoursPerDay++;
      }

      Calendar calendar = Calendar.getInstance();
      calendar.setTime(getStartDate());
      int j = 0;
      int initHours = getGoalDatas().get(j).getHours();
      int hoursOfDay = 0;
      int days = 0;
      int k = 0;
      int totalHours = EzedHelper.getTotalMinutes(getGoalDatas());
      for (int i = 0; i < totalHours; i++)
      {
         initHours--;
         hoursOfDay++;
         if (initHours == 0)
         {
            calendar.add(Calendar.DATE, days);
            k = j;
            if (getGoalDatas().get(j).getContentShowFlag().compareTo(ChapterDetails.HIDE_CONTENT) == 0)
            {
               k = getLastNonHiddenIndex(j);
            }
            getGoalDatas().get(k).setEndDate(calendar.getTime());
            j++;
            if (j == getGoalDatas().size())
            {
               break;
            }
            initHours = getGoalDatas().get(j).getHours();
            calendar.setTime(getStartDate());
         }
         if (hoursOfDay == noOfHoursPerDay)
         {
            hoursOfDay = 0;
            days++;
         }
      }
   }

   private Date getMaxGoalDate(String dbConnName, Logger logger) throws SQLException, Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      Date maxDate = startDate;
      try
      {
         //select endtime from userpoints where userid=? and courseid=? and to_date(?,'YYYYMMDDHH24MISS') < to_date(endtime,'YYYYMMDDHH24MISS') and to_date(?,'YYYYMMDDHH24MISS') > to_date(endtime,'YYYYMMDDHH24MISS') 
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "SelectGoalsWithinPeriod");
         ps.setString(1, getUserId());
         ps.setString(2, courseId);
         ps.setString(3, sdf.format(startDate));
         ps.setString(4, sdf.format(endDate));
         rs = ps.executeQuery();
         String endTimeString = null;
         Date endTimeDate = null;
         while (rs.next())
         {
            endTimeString = rs.getString("endtime");
            endTimeDate = sdf.parse(endTimeString);
            if (maxDate.before(endTimeDate))
            {
               maxDate = endTimeDate;
            }
         }
         Date today = sdfCompare.parse(sdfCompare.format(new Date()));
         if (maxDate.before(today))
         {
            setStartDate(today);
            return today;
         }
         Calendar cal = Calendar.getInstance();
         cal.setTime(maxDate);
         cal.roll(Calendar.DATE, 1);
         setStartDate(cal.getTime());
         return cal.getTime();
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
               logger.error("Error while fetching max goal date for course " + courseId, e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   private int getLastNonHiddenIndex(int index)
   {
      for (int i = index; i >= 0; i--)
      {
         if (getGoalDatas().get(i).getContentShowFlag().compareTo(ChapterDetails.SHOW_CONTENT) == 0)
         {
            return i;
         }
      }
      return 0;
   }

   private void persistModifiedGoals(String dbConnName, Logger logger) throws SQLException, Exception
   {
      GoalData goalDataUse = null;
      for (int j = 0; j < getGoalDatas().size(); j++)
      {
         if (getGoalDatas().get(j).getContentShowFlag().compareTo(ChapterDetails.HIDE_CONTENT) == 0)
         {
            continue;
         }
         Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
         PreparedStatement ps = null;
         try
         {
            goalDataUse = getGoalDatas().get(j);
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertPurchasedCoursePointsTime");
            ps.setString(1, userId);
            ps.setString(2, actionId);
            ps.setString(3, getCourseId());
            ps.setString(4, goalDataUse.getChapterId());
            ps.setString(5, goalDataUse.getContentId());
            ps.setString(6, sdf.format(goalDataUse.getEndDate()));
            ps.setString(7, sdf.format(getEndDate()));
            int rows = ps.executeUpdate();
            if (logger.isDebugEnabled())
            {
               logger.debug(rows + " rows inserted in db while inserting contents for points for courseid " + getCourseId() + " and contentid " + goalDataUse.getContentId() + " for userid " + getUserId());
            }
         }
         catch (Exception e)
         {
            logger.error("Error while inserting in db for points for course id " + getCourseId() + " and content id " + goalDataUse.getContentId(), e);
            throw e;
         }
         finally
         {
            DbConnectionManager.releaseConnection(dbConnName, conn);
         }
      }
   }
   public String getActionId()
   {
      return actionId;
   }
   public void setActionId(String actionId)
   {
      this.actionId = actionId;
   }

   public void persistForCompletionPercentReCalculation(String dbConnName, Logger logger)
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "MarkGoalChanged");
         ps.setString(1, userId);
         ps.setString(2, courseId);
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in db while marking course view as changed for userid " + userId);
         }
      }
      catch (SQLException e)
      {
         if (logger.isTraceEnabled())
         {
            logger.trace("Cannot insert course in db while marking for change", e);
         }
         if (logger.isDebugEnabled())
         {
            logger.debug("Cannot insert course in db while marking course as changed for userid " + userId, e);
         }
      }
      catch (Exception e)
      {
         if (logger.isTraceEnabled())
         {
            logger.trace("Cannot insert course in db while marking for change", e);
         }
         if (logger.isDebugEnabled())
         {
            logger.debug("Cannot insert course in db while marking course as changed for userid " + userId, e);
         }
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
}
