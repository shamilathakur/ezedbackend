package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.axis.encoding.Base64;
import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.utils.StringSplitter;
import com.intelli.ezed.bulkupload.data.ContentDetails;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.QuestionContent;
import com.intelli.ezed.data.QuizRequestResponse;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.SymLinkObject;
import com.intelli.ezed.data.TestCorrectAnsTracker;
import com.intelli.ezed.data.TestDetails;
import com.intelli.ezed.data.TestQuestionData;
import com.intelli.ezed.data.TestRequestResponse;
import com.intelli.ezed.data.UserProfile;
import com.intelli.ezed.utils.Globals;
import com.intelli.ezed.utils.SymLinkManager;

public class QuestionSelector extends SingletonRule
{

   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;
   private boolean whetherFirstQuestion;
   private int correctAnsCount;
   private String correctAnsActionId;
   private String newLinkLocationNotes;
   private String newLinkLocationVideos;
   private String notesStartPoint;
   private String vidStartPoint;
   private boolean betaOverride = false;

   public QuestionSelector(String ruleName, String dbConnName, String whetherFirstQuestion, String correctAnsCount,
                           String correctAnsActionId, String newLinkLoc, String newLinkVideos, String notesStartPoint,
                           String vidStartPoint, String betaOverride)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
      this.whetherFirstQuestion = Boolean.parseBoolean(whetherFirstQuestion);
      this.correctAnsCount = Integer.parseInt(correctAnsCount);
      this.correctAnsActionId = correctAnsActionId;
      this.newLinkLocationNotes = newLinkLoc;
      this.newLinkLocationVideos = newLinkVideos;
      this.notesStartPoint = notesStartPoint;
      this.vidStartPoint = vidStartPoint;
      this.betaOverride = Boolean.parseBoolean(betaOverride);
   }

   public QuestionSelector(String ruleName, String dbConnName, String whetherFirstQuestion, String correctAnsCount,
                           String correctAnsActionId, String newLinkLoc, String newLinkVideos, String notesStartPoint,
                           String vidStartPoint)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
      this.whetherFirstQuestion = Boolean.parseBoolean(whetherFirstQuestion);
      this.correctAnsCount = Integer.parseInt(correctAnsCount);
      this.correctAnsActionId = correctAnsActionId;
      this.newLinkLocationNotes = newLinkLoc;
      this.newLinkLocationVideos = newLinkVideos;
      this.notesStartPoint = notesStartPoint;
      this.vidStartPoint = vidStartPoint;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      UserProfile userProfile = (UserProfile) flowContext.getFromContext(EzedKeyConstants.USER_DETAILS);
      TestDetails testDetails = (TestDetails) flowContext.getFromContext(TestDetails.TEST_DETAILS);
      EzedRequestData requestData = (EzedRequestData) flowContext.getFromContext(EzedRequestData.EZED_REQUEST_DATA);
      TestCorrectAnsTracker tracker = (TestCorrectAnsTracker) flowContext.getFromContext(EzedKeyConstants.CORRECT_ANSWER_TRACKER);

      String testId = testDetails.getTestid();
      testDetails.setTestid(testId);
      //testDetails.setDifficultyLevel(userProfile.getUserLevel());
      int qNo = testDetails.getNextQuestionNumber();
      logger.debug("GetQuestionId : " + qNo);
      if (qNo == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Test end. " + testDetails.getTestid() + " " + userProfile.getUserid());
         }
         return RuleDecisionKey.ON_PARTIAL;
      }

      if (testDetails.isTimeEnd())
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Test time end. " + testDetails.getTestid() + " " + userProfile.getUserid());
         }
         return RuleDecisionKey.ON_PARTIAL;
      }

      Connection connection = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement getNextQuestion;
      String queryTag = "1a";

      if (!whetherFirstQuestion)
      {
         testDetails.setUserAns(requestData.getUserTestAns());

         //Handling Difficulty Level
         if (testDetails.getLastQuestionCorrectAns().compareToIgnoreCase(testDetails.getUserAns()) == 0)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("User ans is correct. Increasing difficulty level.");
            }
            queryTag = "1a";
            //testDetails.increaseDifficultyLevel();
            try
            {
               tracker.increaseCorrectAnsCount(correctAnsCount, correctAnsActionId, userProfile, dbConnName, logger, testDetails.getTestid(), testDetails.getCourseId());
            }
            catch (Exception e)
            {
               logger.error("Error while counting correct answers", e);
               response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal error. Please try again later");
               return RuleDecisionKey.ON_FAILURE;
            }
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("User ans is wrong. Decreasing difficulty level.");
            }
            queryTag = "2a";
            //testDetails.decreaseDifficultyLevel();
         }
      }
      int difficultyLevel = testDetails.getDifficultyLevel();
      String chapterId = testDetails.getChapterId();

      if (chapterId != null)
      {
         getNextQuestion = DbConnectionManager.getPreparedStatement(dbConnName, connection, "GetNextQuestion" + queryTag);
      }
      else
      {
         getNextQuestion = DbConnectionManager.getPreparedStatement(dbConnName, connection, "GetNextQuestionChapterNull" + queryTag);
      }

      TestRequestResponse testResponse = new TestRequestResponse();
      String testSessionId = (String) flowContext.getFromContext(EzedKeyConstants.TEST_SESSION_ID);

      ResultSet resultSet = null;
      SymLinkManager symLinkManager = (SymLinkManager) flowContext.getFromContext(EzedKeyConstants.SYMLINK_MANAGER);
      String userId = (String) flowContext.getFromContext(EzedKeyConstants.USER_ID);
      String explanationContent = null;
      boolean isNotFound = false;
      try
      {
         /*
          *  select * from randomizedquestionbank 
          *  where chapterid = ? 
          *  and difficultylevel > ? 
          *  and questionid not in (select questionid from ongoingtest where userid = ? and testid = ?) 
          *  ORDER BY random() limit 1
          */

         if (chapterId == null)
         {
            getNextQuestion.setString(1, testDetails.getCourseId());
            getNextQuestion.setString(2, testDetails.getCourseId());
            getNextQuestion.setInt(3, difficultyLevel);
            getNextQuestion.setString(4, userProfile.getUserid());
            getNextQuestion.setString(5, testId);
            getNextQuestion.setString(6, testSessionId);
            getNextQuestion.setString(7, userProfile.getUserid());
            getNextQuestion.setString(8, testId);
            getNextQuestion.setString(9, testSessionId);
         }
         else
         {
            getNextQuestion.setString(1, testDetails.getCourseId());
            getNextQuestion.setString(2, chapterId);
            getNextQuestion.setString(3, testDetails.getCourseId());
            getNextQuestion.setString(4, chapterId);
            getNextQuestion.setInt(5, difficultyLevel);
            getNextQuestion.setString(6, userProfile.getUserid());
            getNextQuestion.setString(7, testId);
            getNextQuestion.setString(8, testSessionId);
            getNextQuestion.setString(9, userProfile.getUserid());
            getNextQuestion.setString(10, testId);
            getNextQuestion.setString(11, testSessionId);
         }
         resultSet = getNextQuestion.executeQuery();

         if (resultSet != null && resultSet.next())
         {
            testDetails.setDifficultyLevel(resultSet.getInt("difficultylevel"));
            int questionType = resultSet.getInt("questionType");
            testResponse.setQuestiontype(questionType);
            testResponse.setQuestionId(resultSet.getString("questionid"));
            testResponse.setQuestionNumberOfRandomQuestion(resultSet.getInt("questionnumber"));
            testResponse.setExplanation(new QuestionContent());
            testResponse.getExplanation().setType(resultSet.getInt("explanationtype"));
            testResponse.getExplanation().setContentString(resultSet.getString("explanation"));
            explanationContent = resultSet.getString("explanationcontent");
            if (explanationContent != null)
            {
               testResponse.getExplanation().setContentLoc(fetchSymLinkForContent(explanationContent, dbConnName, symLinkManager, userId));
            }

            //have to create correct answer string for FIB because no data in correctAns column in table.
            StringBuffer correctAnsForFillInTheBlanks = new StringBuffer();

            //select question 
            QuestionContent question = new QuestionContent();
            int i = resultSet.getInt("questioncontenttype");
            question.setType(i); //type 0 = no data , 1 = string, 2 = content
            if (i == QuestionContent.CONTENT_ABSENT)
            {
               //set No content
            }
            else if (i == QuestionContent.STRING_TYPE)
            {
               question.setContentString(resultSet.getString("questionstring"));
            }
            else if (i == QuestionContent.CONTENT_TYPE)
            {
               question.setContentString(resultSet.getString("questioncontent"));
            }
            //Setting the question into the response object
            testResponse.setQuestion(question);

            //Creating a array list to put all the answers inside it
            ArrayList<QuestionContent> answers = new ArrayList<QuestionContent>();

            if (questionType == TestQuestionData.MTF_QUESTION_TYPE)
            {
               //Match state with capital;1;Delhi;West Bengal;Tamil Nadu;Maharashtra
               String mtfQuestionString = question.getContentString();
               String[] tokens = StringSplitter.splitString(mtfQuestionString, ";");

               //Setting new question string to the question part
               if (question.getType() == QuestionContent.CONTENT_TYPE)
               {
                  question.setContentString(fetchSymLinkForContent(tokens[0], dbConnName, symLinkManager, userId));
               }
               else
               {
                  question.setContentString(tokens[0]);
               }

               String mtfContentType = tokens[1];

               for (int k = 1; k <= 6; k++)
               {
                  QuestionContent ans = new QuestionContent();
                  i = resultSet.getInt("ans" + k + "type");
                  ans.setType(Integer.parseInt(mtfContentType));
                  if (i == QuestionContent.CONTENT_ABSENT)
                  {
                     //set No content
                  }
                  else
                  {
                     if (i == QuestionContent.STRING_TYPE)
                     {
                        String t1 = resultSet.getString("ans" + k + "string");
                        //                        ans.setContentString((mtfContentType + "*" + tokens[k + 1] + "*" + i + "*" + t1));
                        ans.setContentString((tokens[k + 1] + "~" + i + "~" + Base64.encode(t1.getBytes("UTF-8"))));
                     }
                     else if (i == QuestionContent.CONTENT_TYPE)
                     {
                        String t1 = resultSet.getString("ans" + k + "content");
                        t1 = fetchSymLinkForContent(t1, dbConnName, symLinkManager, userId);
                        //                        ans.setContentString((mtfContentType + "*" + tokens[k + 1] + "*" + "*" + t1));
                        ans.setContentString((tokens[k + 1] + "~" + i + "~" + t1));
                     }
                     //adding answers to array list
                     answers.add(ans);
                  }
               }

            }
            else
            {
               if (i == QuestionContent.CONTENT_TYPE)
               {
                  question.setContentString(fetchSymLinkForContent(question.getContentString(), dbConnName, symLinkManager, userId));
               }
               for (int k = 1; k <= 6; k++)
               {
                  QuestionContent ans = new QuestionContent();
                  i = resultSet.getInt("ans" + k + "type");
                  ans.setType(i);
                  if (i == QuestionContent.CONTENT_ABSENT)
                  {
                     //set No content
                  }
                  else
                  {
                     if (i == QuestionContent.STRING_TYPE)
                     {
                        String temp = resultSet.getString("ans" + k + "string");
                        ans.setContentString(temp);
                        if (questionType == TestQuestionData.FIB_QUESTION_TYPE)
                        {
                           correctAnsForFillInTheBlanks.append(temp).append(",");
                        }
                     }
                     else if (i == QuestionContent.CONTENT_TYPE)
                     {
                        ans.setContentString(resultSet.getString("ans" + k + "content"));
                        ans.setContentString(fetchSymLinkForContent(ans.getContentString(), dbConnName, symLinkManager, userId));
                     }
                     //adding answers to array list
                     answers.add(ans);
                  }
               }
            }
            //Adding Arraylist to Response
            testResponse.setAnswers(answers);

            StringBuffer sb = new StringBuffer();
            logger.debug("FIB ONE : " + correctAnsForFillInTheBlanks.toString());
            if ((testResponse.getQuestiontype() == TestQuestionData.FIB_QUESTION_TYPE))
            {
               for (int u = 0; u < testResponse.getAnswers().size(); u++)
               {
                  QuestionContent temp = testResponse.getAnswers().get(u);
                  sb.append(temp.getContentString()).append(Globals.FIB_SEPARATOR);
               }

               //               sb.deleteCharAt(sb.length() - Globals.FIB_SEPARATOR.length());
               if (sb.length() > 1)
               {
                  for (int k = 1; k <= Globals.FIB_SEPARATOR.length(); k++)
                  {
                     sb.deleteCharAt(sb.length() - 1);
                  }
               }
               logger.debug("Correct ans for FIB : " + sb.toString());
               testResponse.setCorrectAnswer(sb.toString());
               testDetails.setLastQuestionCorrectAns(testResponse.getCorrectAnswer());

            }
            else
            {
               testResponse.setCorrectAnswer(resultSet.getString("correctans"));
               testDetails.setLastQuestionCorrectAns(testResponse.getCorrectAnswer());
            }

            testResponse.setCurrentQuestionNumber(testDetails.getCurrentQuestionNumber());
         }
         else
         {
            isNotFound = true;
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while getting test request", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while processing test request");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while getting test request", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while processing test request");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         try
         {
            resultSet.close();
         }
         catch (SQLException e)
         {
            logger.error("Error while closing resultset", e);
         }
         DbConnectionManager.releaseConnection(dbConnName, connection);
      }
      try
      {
         connection = DbConnectionManager.getConnectionByName(dbConnName);
         resultSet = null;
         if (isNotFound)
         {
            if (chapterId != null)
            {
               getNextQuestion = DbConnectionManager.getPreparedStatement(dbConnName, connection, "GetNextQuestion" + queryTag + "b");
            }
            else
            {
               getNextQuestion = DbConnectionManager.getPreparedStatement(dbConnName, connection, "GetNextQuestionChapterNull" + queryTag + "b");
            }

            if (chapterId == null)
            {
               getNextQuestion.setString(1, testDetails.getCourseId());
               getNextQuestion.setString(2, testDetails.getCourseId());
               getNextQuestion.setInt(3, difficultyLevel);
               getNextQuestion.setString(4, userProfile.getUserid());
               getNextQuestion.setString(5, testId);
               getNextQuestion.setString(6, testSessionId);
               getNextQuestion.setString(7, userProfile.getUserid());
               getNextQuestion.setString(8, testId);
               getNextQuestion.setString(9, testSessionId);
            }
            else
            {
               getNextQuestion.setString(1, testDetails.getCourseId());
               getNextQuestion.setString(2, chapterId);
               getNextQuestion.setString(3, testDetails.getCourseId());
               getNextQuestion.setString(4, chapterId);
               getNextQuestion.setInt(5, difficultyLevel);
               getNextQuestion.setString(6, userProfile.getUserid());
               getNextQuestion.setString(7, testId);
               getNextQuestion.setString(8, testSessionId);
               getNextQuestion.setString(9, userProfile.getUserid());
               getNextQuestion.setString(10, testId);
               getNextQuestion.setString(11, testSessionId);
            }
            resultSet = getNextQuestion.executeQuery();

            if (resultSet != null && resultSet.next())
            {
               testDetails.setDifficultyLevel(resultSet.getInt("difficultylevel"));
               int questionType = resultSet.getInt("questionType");
               testResponse.setQuestiontype(questionType);
               testResponse.setQuestionId(resultSet.getString("questionid"));
               testResponse.setQuestionNumberOfRandomQuestion(resultSet.getInt("questionnumber"));
               testResponse.setExplanation(new QuestionContent());
               testResponse.getExplanation().setType(resultSet.getInt("explanationtype"));
               testResponse.getExplanation().setContentString(resultSet.getString("explanation"));
               explanationContent = resultSet.getString("explanationcontent");
               if (explanationContent != null)
               {
                  testResponse.getExplanation().setContentLoc(fetchSymLinkForContent(explanationContent, dbConnName, symLinkManager, userId));
               }

               //have to create correct answer string for FIB because no data in correctAns column in table.
               StringBuffer correctAnsForFillInTheBlanks = new StringBuffer();

               //select question 
               QuestionContent question = new QuestionContent();
               int i = resultSet.getInt("questioncontenttype");
               question.setType(i); //type 0 = no data , 1 = string, 2 = content
               if (i == QuestionContent.CONTENT_ABSENT)
               {
                  //set No content
               }
               else if (i == QuestionContent.STRING_TYPE)
               {
                  question.setContentString(resultSet.getString("questionstring"));
               }
               else if (i == QuestionContent.CONTENT_TYPE)
               {
                  question.setContentString(resultSet.getString("questioncontent"));
               }
               //Setting the question into the response object
               testResponse.setQuestion(question);

               //Creating a array list to put all the answers inside it
               ArrayList<QuestionContent> answers = new ArrayList<QuestionContent>();

               if (questionType == TestQuestionData.MTF_QUESTION_TYPE)
               {
                  //Match state with capital;1;Delhi;West Bengal;Tamil Nadu;Maharashtra
                  String mtfQuestionString = question.getContentString();
                  String[] tokens = StringSplitter.splitString(mtfQuestionString, ";");

                  //Setting new question string to the question part
                  if (question.getType() == QuestionContent.CONTENT_TYPE)
                  {
                     question.setContentString(fetchSymLinkForContent(tokens[0], dbConnName, symLinkManager, userId));
                  }
                  else
                  {
                     question.setContentString(tokens[0]);
                  }

                  String mtfContentType = tokens[1];

                  for (int k = 1; k <= 6; k++)
                  {
                     QuestionContent ans = new QuestionContent();
                     i = resultSet.getInt("ans" + k + "type");
                     ans.setType(Integer.parseInt(mtfContentType));
                     if (i == QuestionContent.CONTENT_ABSENT)
                     {
                        //set No content
                     }
                     else
                     {
                        if (i == QuestionContent.STRING_TYPE)
                        {
                           String t1 = resultSet.getString("ans" + k + "string");
                           //                        ans.setContentString((mtfContentType + "*" + tokens[k + 1] + "*" + i + "*" + t1));
                           ans.setContentString((tokens[k + 1] + "~" + i + "~" + Base64.encode(t1.getBytes("UTF-8"))));
                        }
                        else if (i == QuestionContent.CONTENT_TYPE)
                        {
                           String t1 = resultSet.getString("ans" + k + "content");
                           t1 = fetchSymLinkForContent(t1, dbConnName, symLinkManager, userId);
                           //                        ans.setContentString((mtfContentType + "*" + tokens[k + 1] + "*" + "*" + t1));
                           ans.setContentString((tokens[k + 1] + "~" + i + "~" + t1));
                        }
                        //adding answers to array list
                        answers.add(ans);
                     }
                  }

               }
               else
               {
                  if (i == QuestionContent.CONTENT_TYPE)
                  {
                     question.setContentString(fetchSymLinkForContent(question.getContentString(), dbConnName, symLinkManager, userId));
                  }
                  for (int k = 1; k <= 6; k++)
                  {
                     QuestionContent ans = new QuestionContent();
                     i = resultSet.getInt("ans" + k + "type");
                     ans.setType(i);
                     if (i == QuestionContent.CONTENT_ABSENT)
                     {
                        //set No content
                     }
                     else
                     {
                        if (i == QuestionContent.STRING_TYPE)
                        {
                           String temp = resultSet.getString("ans" + k + "string");
                           ans.setContentString(temp);
                           if (questionType == TestQuestionData.FIB_QUESTION_TYPE)
                           {
                              correctAnsForFillInTheBlanks.append(temp).append(",");
                           }
                        }
                        else if (i == QuestionContent.CONTENT_TYPE)
                        {
                           ans.setContentString(resultSet.getString("ans" + k + "content"));
                           ans.setContentString(fetchSymLinkForContent(ans.getContentString(), dbConnName, symLinkManager, userId));
                        }
                        //adding answers to array list
                        answers.add(ans);
                     }
                  }
               }
               //Adding Arraylist to Response
               testResponse.setAnswers(answers);

               StringBuffer sb = new StringBuffer();
               logger.debug("FIB ONE : " + correctAnsForFillInTheBlanks.toString());
               if ((testResponse.getQuestiontype() == TestQuestionData.FIB_QUESTION_TYPE))
               {
                  for (int u = 0; u < testResponse.getAnswers().size(); u++)
                  {
                     QuestionContent temp = testResponse.getAnswers().get(u);
                     sb.append(temp.getContentString()).append(Globals.FIB_SEPARATOR);
                  }
                  for (int k = 1; k <= Globals.FIB_SEPARATOR.length(); k++)
                  {
                     sb.deleteCharAt(sb.length() - 1);
                  }
                  logger.debug("Correct ans for FIB : " + sb.toString());
                  testResponse.setCorrectAnswer(sb.toString());
                  testDetails.setLastQuestionCorrectAns(testResponse.getCorrectAnswer());

               }
               else
               {
                  testResponse.setCorrectAnswer(resultSet.getString("correctans"));
                  testDetails.setLastQuestionCorrectAns(testResponse.getCorrectAnswer());
               }
               testResponse.setCurrentQuestionNumber(testDetails.getCurrentQuestionNumber());
            }
            else
            {
               if (whetherFirstQuestion)
               {
                  if (logger.isDebugEnabled())
                  {
                     logger.debug("No Question in ramdomized question bank.");
                  }
                  response.setParameters(EzedTransactionCodes.NO_QUESTIONS_IN_BANK, "Error while fetching first question please check the question bank.");
                  return RuleDecisionKey.ON_FAILURE;
               }
               if (logger.isDebugEnabled())
               {
                  logger.debug("No more questions in question bank. Number of questions set for this test is more than number of questions in question bank.");
               }
               return RuleDecisionKey.ON_PARTIAL;
            }
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while getting test request", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while processing test request");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while getting test request", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while processing test request");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         try
         {
            if (resultSet != null)
            {
               resultSet.close();
            }
         }
         catch (SQLException e)
         {
            logger.error("Error while closing resultset", e);
         }
         DbConnectionManager.releaseConnection(dbConnName, connection);
      }

      QuizRequestResponse quizRequestResponse = new QuizRequestResponse();
      ArrayList<TestRequestResponse> list = new ArrayList<TestRequestResponse>();
      list.add(testResponse);
      quizRequestResponse.setQuizQuestions(list);

      response.setResponseObject(quizRequestResponse);
      testDetails.setSentQuestionDetails(testResponse);
      response.setParameters(EzedTransactionCodes.SUCCESS, "Sending first question in response.");
      return RuleDecisionKey.ON_SUCCESS;
   }

   public String fetchSymLinkForContent(String contentId, String dbConnName, SymLinkManager symLinkManager, String userId) throws Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchContentDetailsForId");
      ResultSet rs = null;
      String newLink = null;
      try
      {
         ps.setString(1, contentId);
         rs = ps.executeQuery();
         if (rs.next())
         {
            String contentlink = rs.getString("link");
            int actualContentType = rs.getInt("contenttype");
            if (betaOverride)
            {
               return contentlink;
            }
            String linkLocation = null;
            boolean isContent = false;
            if (actualContentType == ContentDetails.CONTENT_NOTE || actualContentType == ContentDetails.CONTENT_PDF || actualContentType == ContentDetails.CONTENT_IMAGE || actualContentType == ContentDetails.CONTENT_IMAGE_1)
            {
               linkLocation = newLinkLocationNotes;
               isContent = true;
            }
            else
            {
               linkLocation = newLinkLocationVideos;
            }

            //session id is userid
            newLink = symLinkManager.getSymLink(userId, contentId, contentlink, logger, linkLocation);
            if (isContent)
            {
               int index = newLink.indexOf(notesStartPoint);
               newLink = newLink.substring(index + 1);
            }
            else
            {
               int index = newLink.indexOf(vidStartPoint);
               newLink = newLink.substring(index + 1);
               String fileType = newLink.substring(newLink.lastIndexOf('.') + 1);
               if (fileType.compareToIgnoreCase("mov") == 0)
               {
                  fileType = "mp4";
               }
               newLink = fileType + ":" + newLink;
            }
            SymLinkObject linkObj = new SymLinkObject();
            linkObj.setLink(newLink);
         }
         return newLink;
      }
      catch (Exception e)
      {
         throw e;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
}