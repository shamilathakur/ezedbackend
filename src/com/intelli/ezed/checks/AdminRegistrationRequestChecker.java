package com.intelli.ezed.checks;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class AdminRegistrationRequestChecker extends SingletonRule
{

   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public AdminRegistrationRequestChecker(String ruleName)
   {
      super(ruleName);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      /*
       * email=
       * sessionid=
       * newemailid=
       * roll=
       * password=
       */

      String newUserEmailId = messageData.getParameter("newemailid");
      String role = messageData.getParameter("role");
      //String password = messageData.getParameter("password");
      String name = messageData.getParameter("name");

      if (name == null || name.compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("name is invalid " + name);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "name is invalid");
         return RuleDecisionKey.ON_FAILURE;
      }

      if (newUserEmailId == null || newUserEmailId.equals("") || newUserEmailId.equals(" "))
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Email address not provided.");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Please enter correct email id.");
         return RuleDecisionKey.ON_FAILURE;
      }

      if (role == null || role.equals("") || role.equals(" "))
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("role id not provided.");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Please enter correct roll id.");
         return RuleDecisionKey.ON_FAILURE;
      }

      //      if (password == null || password.equals("") || password.equals(" "))
      //      {
      //         if (logger.isDebugEnabled())
      //         {
      //            logger.debug("password not provided.");
      //         }
      //         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Please enter correct password.");
      //         return RuleDecisionKey.ON_FAILURE;
      //      }
      return RuleDecisionKey.ON_SUCCESS;
   }
}
