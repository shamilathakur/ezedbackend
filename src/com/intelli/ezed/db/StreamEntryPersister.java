package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.UserProfile;

public class StreamEntryPersister extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.STREAM_LOG);
   private String dbConnName;

   public StreamEntryPersister(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      String streamEntry = (String) flowContext.getFromContext(EzedKeyConstants.STREAM_ENTRY_STRING);
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      UserProfile userProfile = (UserProfile) flowContext.getFromContext(EzedKeyConstants.USER_DETAILS);
      SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertUserStream");
         ps.setString(1, streamEntry);
         ps.setString(2, userProfile.getUserid());
         ps.setString(3, sdf.format(new Date(System.currentTimeMillis())));
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in database while making stream entry for " + userProfile.getUserid());
         }
         if (rows == 0)
         {
            logger.error("INSERT FAILED " + streamEntry);
         }
      }
      catch (SQLException e)
      {
         logger.error("Database error inserting stream entry for " + userProfile.getUserid(), e);
         logger.error("INSERT FAILED " + streamEntry);
      }
      catch (Exception e)
      {
         logger.error("Error inserting stream entry for " + userProfile.getUserid(), e);
         logger.error("INSERT FAILED " + streamEntry);
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      return RuleDecisionKey.ON_SUCCESS;
   }
}
