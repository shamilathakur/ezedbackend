package com.intelli.ezed.bulkupload.data;

import org.apache.commons.digester3.ObjectCreateRule;
import org.xml.sax.Attributes;

import com.intelli.ezed.data.FIBData;
import com.intelli.ezed.data.MCQData;
import com.intelli.ezed.data.MTFData;

public class TestQuestionConditionRule extends ObjectCreateRule
{
   public TestQuestionConditionRule()
   {
      super(Object.class);
   }

   @Override
   public void begin(String namespace, String name, Attributes attributes) throws Exception
   {
      String type = attributes.getValue("type");
      if (type.toUpperCase().compareTo("MCQ") == 0)
      {
         QuestionBankDetails questionBankDetails = (QuestionBankDetails) getDigester().pop();
         MCQData mcqData = new MCQData();
         questionBankDetails.addQuestion(mcqData);
         getDigester().push(questionBankDetails);
         getDigester().push(mcqData);
      }
      else if (type.toUpperCase().compareTo("MTF") == 0)
      {
         QuestionBankDetails questionBankDetails = (QuestionBankDetails) getDigester().pop();
         MTFData mtfData = new MTFData();
         questionBankDetails.addQuestion(mtfData);
         getDigester().push(questionBankDetails);
         getDigester().push(mtfData);
      }
      else if (type.toUpperCase().compareTo("FIB") == 0)
      {
         QuestionBankDetails questionBankDetails = (QuestionBankDetails) getDigester().pop();
         FIBData fibData = new FIBData();
         questionBankDetails.addQuestion(fibData);
         getDigester().push(questionBankDetails);
         getDigester().push(fibData);
      }
      else
      {
         throw new Exception("Proper question type not mentioned");
      }
   }

   //   @Override
   //   public void end() throws Exception
   //   {
   //      digester.pop();
   //   }
   @Override
   public void end(String namespace, String name) throws Exception
   {
      super.end(namespace, name);
   }
}
