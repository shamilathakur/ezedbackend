package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.AdminProfile;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class GroupAdminHandler extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

   public GroupAdminHandler(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      String adminUserId = (String) flowContext.getFromContext(EzedKeyConstants.ADMIN_USER_ID);
      AdminProfile adminProfile = (AdminProfile) flowContext.getFromContext(EzedKeyConstants.ADMIN_PROFILE);
      String micAdminUserId = null;
      String micAdminFromList = messageData.getParameter("micadminid");
      if (micAdminFromList == null || micAdminFromList.trim().compareTo("") == 0 || micAdminFromList.trim().compareToIgnoreCase("default") == 0 || micAdminFromList.trim().compareToIgnoreCase("null") == 0)
      {
         micAdminUserId = adminProfile.getUserid();
      }
      else
      {
         micAdminUserId = micAdminFromList;
      }
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertGroupDetails");
         ps.setString(1, adminUserId);
         ps.setString(2, micAdminUserId);
         ps.setString(3, sdf.format(new Date()));
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in db while inserting group details");
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while inserting group admin details", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while handling group details");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while inserting group admin details", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while handling group details");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
      return RuleDecisionKey.ON_SUCCESS;
   }

}
