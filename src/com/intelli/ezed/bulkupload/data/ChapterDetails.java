package com.intelli.ezed.bulkupload.data;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.sabre.db.DbConnectionManager;
import org.slf4j.Logger;

import com.a.helper.utils.StringSplitter;
import com.intelli.ezed.data.FIBData;
import com.intelli.ezed.data.MCQData;
import com.intelli.ezed.data.MTFData;
import com.intelli.ezed.data.QuestionContent;
import com.intelli.ezed.data.TestQuestion;
import com.intelli.ezed.data.TestQuestionData;
import com.intelli.ezed.utils.EzedHelper;
import com.intelli.ezed.utils.PrepareChapterIdBackend;

public class ChapterDetails
{
   public static final String SHOW_CONTENT = "1";
   public static final String HIDE_CONTENT = "0";
   private String name;
   private String chapterId;
   private static PrepareChapterIdBackend prepareChapterId = new PrepareChapterIdBackend("CHA2", "ezeddb");
   private String[] learningPath;
   private String[] showFlag;
   private ArrayList<TestDetails> testDetails = new ArrayList<TestDetails>();
   private Map<String, TestDetails> testDetailMap = new HashMap<String, TestDetails>();

   public void populateTestDetailsMap()
   {
      for (TestDetails singleTestDetails : testDetails)
      {
         if (singleTestDetails.getTestName() != null)
         {
            testDetailMap.put(singleTestDetails.getTestName(), singleTestDetails);
         }
      }
   }

   public void addTestDetails(TestDetails testDetails)
   {
      this.testDetails.add(testDetails);
   }

   public String[] getLearningPath()
   {
      return learningPath;
   }

   public void setLearningPath(String[] learningPath)
   {
      this.learningPath = learningPath;
   }

   public void setShowFlags(String[] showFlags)
   {
      this.showFlag = showFlags;
   }

   public void setLearningPath(String commaSeparatedLearningPath)
   {
      String[] learningPathStrings = StringSplitter.splitString(commaSeparatedLearningPath, ",");
      String[] showFlagStrings = new String[learningPathStrings.length];
      String singleContent = null;
      String flag = SHOW_CONTENT;
      for (int i = 0; i < learningPathStrings.length; i++)
      {
         flag = SHOW_CONTENT;
         if (learningPathStrings[i].contains(":"))
         {
            singleContent = learningPathStrings[i];
            learningPathStrings[i] = singleContent.substring(0, singleContent.indexOf(":"));
            if (singleContent.substring(singleContent.indexOf(":") + 1, singleContent.length()).compareTo(HIDE_CONTENT) == 0)
            {
               flag = HIDE_CONTENT;
            }
         }
         showFlagStrings[i] = flag;
      }
      learningPath = learningPathStrings;
      this.showFlag = showFlagStrings;
   }

   public String getChapterId()
   {
      return chapterId;
   }

   public void setChapterId(String chapterId)
   {
      this.chapterId = chapterId;
   }

   public void setNewChapterId()
   {
      setChapterId(fetchNewChapterId());
   }

   public String getName()
   {
      return name;
   }

   public void setName(String name)
   {
      this.name = EzedHelper.handleBulkUploadStringUpperCase(name, 50);
   }

   private String fetchNewChapterId()
   {
      return prepareChapterId.getNextId();
   }

   public void persistEnclosedTests(String dbConnName, Logger logger, String courseId) throws Exception
   {
      for (int i = 0; i < testDetails.size(); i++)
      {
         testDetails.get(i).setTestId(ContentDetails.fetchNewContentId());
         testDetails.get(i).setCourseId(courseId);
         testDetails.get(i).setChapterId(this.getChapterId());
         testDetails.get(i).persistTest(dbConnName, logger);
      }
   }

   public void persistChapter(String dbConnName, Logger logger) throws Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertChapter");
         ps.setString(1, getChapterId());
         ps.setString(2, getName());

         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in db while inserting chapter");
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while inserting chapter in db", e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while inserting chapter in db", e);
         throw e;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   public void persistLearningPath(String dbConnName, Logger logger, Map<String, String> contentDirIdMap) throws Exception
   {
      populateTestDetailsMap();
      Connection conn = null;
      PreparedStatement ps = null;
      TestDetails testDetails = null;
      int i = 0;
      for (; i < learningPath.length; i++)
      {
         //         testDetails = testDetailMap.get(getLearningPath()[i]);
         //         if (testDetails != null)
         //         {
         //            testDetails.persistTestAsContent(dbConnName, logger);
         //            testDetails.persistTestInLearningPath(dbConnName, logger, i + 1);
         //            continue;
         //         }
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         try
         {
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertLearningPath");
            ps.setString(1, getChapterId());
            //putting actual content id from content map here
            ps.setString(2, contentDirIdMap.get(getLearningPath()[i]));
            ps.setInt(3, (i + 1));
            ps.setString(4, showFlag[i]);

            int rows = ps.executeUpdate();
            if (logger.isDebugEnabled())
            {
               logger.debug(rows + " rows inserted in db while inserting learning path");
            }
         }
         catch (SQLException e)
         {
            logger.error("Error while inserting learning path in db for chapterid " + name + " and content " + getLearningPath()[i], e);
            //            throw e;
         }
         catch (Exception e)
         {
            logger.error("Error while inserting learning path in db for chapterid " + name + " and content " + getLearningPath()[i], e);
            //            throw e;
         }
         finally
         {
            DbConnectionManager.releaseConnection(dbConnName, conn);
         }
      }

      Iterator<String> testNameIte = testDetailMap.keySet().iterator();
      while (testNameIte.hasNext())
      {
         i++;
         testDetails = testDetailMap.get(testNameIte.next());
         if (testDetails != null)
         {
            testDetails.persistTestAsContent(dbConnName, logger);
            testDetails.persistTestInLearningPath(dbConnName, logger, i);
            continue;
         }
      }
   }

   public void printDetails(Logger logger)
   {
      if (logger.isTraceEnabled())
      {
         logger.trace("Chapter Name " + getName());
         logger.trace("Chapter ID " + getChapterId());
      }
   }

   public static void main(String[] args)
   {
      String abc = "abc:d";
      System.out.println(abc.substring(0, abc.indexOf(":")));
      System.out.println(abc.substring(abc.indexOf(":") + 1, abc.length()));
   }

   public String[] getShowFlag()
   {
      return showFlag;
   }

   public void setShowFlag(String[] showFlag)
   {
      this.showFlag = showFlag;
   }

   public void writeToFile(String courseDirName, String chapterDetailFileName, ArrayList<TestDetails> tests) throws IOException
   {
      File file = new File(courseDirName + File.separator + getChapterId());
      if (!file.exists())
      {
         file.mkdir();
      }

      File chapterFile = new File(file.getAbsolutePath() + File.separator + chapterDetailFileName);
      if (!chapterFile.exists())
      {
         chapterFile.createNewFile();
      }
      FileOutputStream fos = null;
      try
      {
         fos = new FileOutputStream(chapterFile);
         StringBuffer sb = new StringBuffer();
         sb.append("<chapterdetails>\n");
         sb.append("<name>");
         sb.append(getName());
         sb.append("</name>\n");
         sb.append("<id>");
         sb.append(getChapterId());
         sb.append("</id>\n");
         sb.append("<learningpath>");
         sb.append(getCommaSeparatedLearningPath());
         sb.append("</learningpath>\n");
         for (int i = 0; i < tests.size(); i++)
         {
            sb.append("<test>\n");
            sb.append("<time>");
            sb.append(tests.get(i).getTestTime());
            sb.append("</time>\n");
            sb.append("<id>");
            sb.append(tests.get(i).getTestId());
            sb.append("</id>\n");
            sb.append("<noofquestions>");
            sb.append(tests.get(i).getNoOfQuestions());
            sb.append("</noofquestions>\n");
            sb.append("<name>");
            sb.append(tests.get(i).getTestName());
            sb.append("</name>\n</test>\n");
         }
         sb.append("</chapterdetails>");
         fos.write(sb.toString().getBytes());
      }
      finally
      {
         if (fos != null)
         {
            try
            {
               fos.close();
            }
            catch (IOException e)
            {

            }
         }
      }
   }

   private String getCommaSeparatedLearningPath()
   {
      StringBuffer sb = new StringBuffer();
      for (int i = 0; i < getLearningPath().length; i++)
      {
         sb.append(getLearningPath()[i]);
         sb.append(":");
         sb.append(getShowFlag()[i]);
         sb.append(",");
      }
      if (sb.length() > 0)
      {
         sb.deleteCharAt(sb.length() - 1);
      }
      return sb.toString();
   }

   public ArrayList<TestQuestion> populateQuestionBankFromDb(String dbConnName, Logger logger, String courseid) throws SQLException, Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      ArrayList<TestQuestion> testQuestion = new ArrayList<TestQuestion>();
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchCourseChapterQuestionBank");
         ps.setString(1, courseid);
         ps.setString(2, getChapterId());
         rs = ps.executeQuery();
         QuestionContent content = null;
         while (rs.next())
         {
            int questionType = rs.getInt("questiontype");
            if (questionType == TestQuestionData.MCQ_QUESTION_TYPE)
            {
               MCQData data = new MCQData();
               data.setQuestionid(rs.getString("questionid"));
               content = new QuestionContent();
               content.setType(rs.getInt("questioncontenttype"));
               if (content.getType() == QuestionContent.STRING_TYPE)
               {
                  content.setContentString(rs.getString("questionstring"));
               }
               else
               {
                  content.setContentLoc(rs.getString("questioncontent"));
               }
               data.setQuestion(content);

               for (int i = 1; i < 7; i++)
               {
                  content = new QuestionContent();
                  content.setType(rs.getInt("ans" + i + "type"));
                  if (content.getType() == QuestionContent.STRING_TYPE)
                  {
                     content.setContentString(rs.getString("ans" + i + "string"));
                     data.addAnswers(content);
                  }
                  else if (content.getType() == QuestionContent.CONTENT_TYPE)
                  {
                     content.setContentLoc(rs.getString("ans" + i + "content"));
                     data.addAnswers(content);
                  }
               }
               data.setDifficultyLevel(rs.getInt("difficultylevel"));
               data.setExplanation(new QuestionContent());
               data.getExplanation().setType(rs.getInt("explanationtype"));
               data.getExplanation().setContentString(rs.getString("explanation"));
               data.getExplanation().setContentLoc(rs.getString("explanationcontent"));
               data.setCorrectAnswer(rs.getString("correctans"));
               testQuestion.add(data);
            }
            else if (questionType == TestQuestionData.MTF_QUESTION_TYPE)
            {
               MTFData data = new MTFData();
               data.setQuestionid(rs.getString("questionid"));
               content = new QuestionContent();
               content.setType(rs.getInt("questioncontenttype"));
               String questionString = rs.getString("questionstring");
               String[] questions = StringSplitter.splitString(questionString, ";");
               if (content.getType() == QuestionContent.STRING_TYPE)
               {
                  content.setContentString(questions[0]);
               }
               else
               {
                  content.setContentLoc(questions[0]);
               }
               data.setQuestion(content);
               int type = Integer.parseInt(questions[1]);
               for (int i = 2; i < questions.length; i++)
               {
                  content = new QuestionContent();
                  content.setType(type);
                  if (type == QuestionContent.STRING_TYPE)
                  {
                     content.setContentString(questions[i]);
                  }
                  else
                  {
                     content.setContentLoc(questions[i]);
                  }
                  data.addQuestions(content);
               }

               for (int i = 1; i < 7; i++)
               {
                  content = new QuestionContent();
                  content.setType(rs.getInt("ans" + i + "type"));
                  if (content.getType() == QuestionContent.STRING_TYPE)
                  {
                     content.setContentString(rs.getString("ans" + i + "string"));
                     data.addAnswers(content);
                  }
                  else if (content.getType() == QuestionContent.CONTENT_TYPE)
                  {
                     content.setContentLoc(rs.getString("ans" + i + "content"));
                     data.addAnswers(content);
                  }
               }
               data.setDifficultyLevel(rs.getInt("difficultylevel"));
               data.setExplanation(new QuestionContent());
               data.getExplanation().setType(rs.getInt("explanationtype"));
               data.getExplanation().setContentString(rs.getString("explanation"));
               data.getExplanation().setContentLoc(rs.getString("explanationcontent"));
               data.setCorrectAnswer(rs.getString("correctans"));
               testQuestion.add(data);
            }
            else
            {
               FIBData data = new FIBData();
               data.setQuestionid(rs.getString("questionid"));
               content = new QuestionContent();
               content.setType(rs.getInt("questioncontenttype"));
               if (content.getType() == QuestionContent.STRING_TYPE)
               {
                  content.setContentString(rs.getString("questionstring"));
               }
               else
               {
                  content.setContentLoc(rs.getString("questioncontent"));
               }
               data.setQuestion(content);

               for (int i = 1; i < 7; i++)
               {
                  content = new QuestionContent();
                  content.setType(rs.getInt("ans" + i + "type"));
                  if (content.getType() == QuestionContent.STRING_TYPE)
                  {
                     content.setContentString(rs.getString("ans" + i + "string"));
                     data.addAnswers(content);
                  }
                  else if (content.getType() == QuestionContent.CONTENT_TYPE)
                  {
                     content.setContentLoc(rs.getString("ans" + i + "content"));
                     data.addAnswers(content);
                  }
               }
               data.setDifficultyLevel(rs.getInt("difficultylevel"));
               data.setExplanation(new QuestionContent());
               data.getExplanation().setType(rs.getInt("explanationtype"));
               data.getExplanation().setContentString(rs.getString("explanation"));
               data.getExplanation().setContentLoc(rs.getString("explanationcontent"));
               testQuestion.add(data);
            }
         }
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
      return testQuestion;
   }

   public ArrayList<TestDetails> populateTestsFromDb(String dbConnName, Logger logger, String courseId) throws SQLException, Exception
   {
      ArrayList<TestDetails> datas = new ArrayList<TestDetails>();
      TestDetails data = null;
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchCourseChapterTests");
         ps.setString(1, courseId);
         ps.setString(2, getChapterId());
         rs = ps.executeQuery();
         while (rs.next())
         {
            data = new TestDetails();
            data.setTestId(rs.getString("testid"));
            data.setTestTime(rs.getString("testtime"));
            data.setNoOfQuestions(rs.getInt("numberofquestion"));
            data.setTestName(rs.getString("testname"));
            datas.add(data);
         }
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
      return datas;
   }
}
