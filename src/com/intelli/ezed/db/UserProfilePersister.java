package com.intelli.ezed.db;

import java.sql.SQLException;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.UserProfile;

public class UserProfilePersister extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public UserProfilePersister(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      UserProfile userProfile = (UserProfile) flowContext.getFromContext(EzedKeyConstants.USER_DETAILS);
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      try
      {
         int status = userProfile.commitUserProfile(dbConnName, logger);
         if (status != EzedTransactionCodes.SUCCESS)
         {
            response.setParameters(status, "Error while persisting user profile");
            if (logger.isDebugEnabled())
            {
               logger.debug("Error while persisting user profile " + status);
            }
            return RuleDecisionKey.ON_FAILURE;
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while persisting user profile in DB", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Failed to persist user profile");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while persist user profile from DB", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Failed to persist user profile");
         return RuleDecisionKey.ON_FAILURE;
      }
      return RuleDecisionKey.ON_SUCCESS;
   }

}
