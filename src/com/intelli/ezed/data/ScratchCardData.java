package com.intelli.ezed.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import org.sabre.db.DbConnectionManager;
import org.slf4j.Logger;

import com.intelli.ezed.utils.RandomNumberGenerator;

public class ScratchCardData extends ResponseObject {

	private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
	private int srno;
	private String scratchCardId;
	private String courseId;
	private int courseType;
	private String generateDate;
	private String validTill;
	private int assignedStatus; // if the card is assigned to partner or not
								// assigned=1,unassigned=0
	private int usedStatus; // used=1,unused=0
	private String lastModified;
	private int price;
	private int noOfCards;
	private int serialNumber = 0;
	private String generatedBy;
	private String assignedTo;
	private int cardActiveStatus; // will be used while deactivating the card ..
									// also set in modify scratch card

	private String lastModifiedBy;
	private String UnivName;

	public String getUnivName() {
		return UnivName;
	}

	public void setUnivName(String univName) {
		UnivName = univName;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public String getGeneratedBy() {
		return generatedBy;
	}

	public void setGeneratedBy(String generatedBy) {
		this.generatedBy = generatedBy;
	}

	public int getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(int serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getAssignedTo() {
		return assignedTo;
	}

	public void setAssignedTo(String assignedTo) {
		this.assignedTo = assignedTo;
	}

	public int getNoOfCards() {
		return noOfCards;
	}

	public void setNoOfCards(int noOfCards) {
		this.noOfCards = noOfCards;
	}

	public int getCardActiveStatus() {
		return cardActiveStatus;
	}

	public void setCardActiveStatus(int cardActiveStatus) {
		this.cardActiveStatus = cardActiveStatus;
	}

	public int getSrno() {
		return srno;
	}

	public void setSrno(int srno) {
		this.srno = srno;
	}

	public String getScratchCardId() {
		return scratchCardId;
	}

	public void setScratchCardId(String scratchCardId) {
		this.scratchCardId = scratchCardId;
	}

	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	public int getCourseType() {
		return courseType;
	}

	public void setCourseType(int courseType) {
		this.courseType = courseType;
	}

	public String getGenerateDate() {
		return generateDate;
	}

	public void setGenerateDate(String generateDate) {
		this.generateDate = generateDate;
	}

	public String getValidTill() {
		return validTill;
	}

	public void setValidTill(String validTill) {
		this.validTill = validTill + "00000000";
	}

	public int getAssignedStatus() {
		return assignedStatus;
	}

	public void setAssignedStatus(int assignedStatus) {
		this.assignedStatus = assignedStatus;
	}

	public int getUsedStatus() {
		return usedStatus;
	}

	public void setUsedStatus(int usedStatus) {
		this.usedStatus = usedStatus;
	}

	public String getLastModified() {
		return lastModified;
	}

	public void setLastModified(String lastModified) {
		this.lastModified = lastModified;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public void handleCreateCard(String dbConnName, Logger logger,
			int serialNumber) throws SQLException, Exception {
		setSerialNumber(serialNumber);
		setScratchCardId(RandomNumberGenerator.generateRandomAlphaNum(10));
		this.persistScratchCardDetails(dbConnName, logger);
	}

	public void persistMappingDetails(String dbConnName, Logger logger) {
		Connection connection = DbConnectionManager
				.getConnectionByName(dbConnName);
		PreparedStatement ps = null;
		int rs;
		try {

			ps = DbConnectionManager.getPreparedStatement(dbConnName,
					connection, "AddNewScratchCardPartnerMapping");
			ps.setString(1, getScratchCardId());
			ps.setString(2, getAssignedTo());
			ps.setInt(3, getCardActiveStatus());
			ps.setString(4, getLastModified());
			ps.setString(5, getLastModifiedBy());
			ps.setString(6, getLastModified());
			ps.setString(7, getLastModifiedBy());
			rs = ps.executeUpdate();
			if (rs > 0) {
				if (logger.isDebugEnabled()) {
					logger.debug("Partner-SC mapping is added into database with partner id : "
							+ getAssignedTo());
				}
			}
		} catch (Exception e) {
			logger.debug("Something went wrong while adding mapping record ! ",
					e);
		}

		finally {
			DbConnectionManager.releaseConnection(dbConnName, connection);
		}

	}

	private void persistScratchCardDetails(String dbConnName, Logger logger)
			throws SQLException, Exception {
		Connection connection = DbConnectionManager
				.getConnectionByName(dbConnName);
		PreparedStatement ps = null;
		int rs;
		try {

			ps = DbConnectionManager.getPreparedStatement(dbConnName,
					connection, "AddNewScratchCard");
			ps.setInt(1, getSerialNumber());
			ps.setString(2, getScratchCardId());
			ps.setString(3, getCourseId());
			ps.setInt(4, getCourseType());
			ps.setString(5, getGenerateDate());
			ps.setString(6, getValidTill());
			ps.setInt(7, getAssignedStatus());
			ps.setInt(8, 0); // set it as unused
			ps.setString(9, getLastModified());
			ps.setInt(10, getPrice());
			ps.setString(11, getGeneratedBy());

			rs = ps.executeUpdate();
			if (rs > 0) {
				if (logger.isDebugEnabled()) {
					logger.debug("Scratch card is added into database with scratchcard id : "
							+ getScratchCardId());
				}
				if (getAssignedStatus() == 1) {
					this.persistMappingDetails(dbConnName, logger);
				}
			}
		} catch (SQLException e) {
			if (e.getMessage().contains("scratchcardlist_pkey")) {
				if (logger.isDebugEnabled()) {
					logger.debug(
							"ScratchCardId already present in database. Genrating again.",
							e);
					this.handleCreateCard(dbConnName, logger, getSerialNumber());
				}
			}
			logger.error(
					"Error while adding new scratch card in db for serial Number : "
							+ getSerialNumber() + "Trying again", e);
			this.handleCreateCard(dbConnName, logger, getSerialNumber());

		} catch (Exception e) {
			logger.error("Error while adding scratch card  "
					+ getSerialNumber(), e);
			this.handleCreateCard(dbConnName, logger, getSerialNumber());
		} finally {
			DbConnectionManager.releaseConnection(dbConnName, connection);
		}
	}

	public void updateScratchCard(String dbConnName, Logger logger) {
		Connection connection = DbConnectionManager
				.getConnectionByName(dbConnName);
		PreparedStatement ps = null;
		int rs;
		try {

			ps = DbConnectionManager.getPreparedStatement(dbConnName,
					connection, "UpdateScratchCard");

			ps.setString(1, getCourseId());
			ps.setInt(2, getCourseType());
			ps.setString(3, getValidTill());
			ps.setInt(4, getAssignedStatus());
			ps.setString(5, getLastModified());
			ps.setInt(6, getPrice());
			ps.setString(7, getLastModifiedBy());
			ps.setInt(8, getSrno());

			rs = ps.executeUpdate();
			if (rs > 0) {
				if (logger.isDebugEnabled()) {
					logger.debug("Scratch card is updated into database with scratchcard id : "
							+ getScratchCardId());
				}
			}
		} catch (SQLException e) {
			logger.error(
					"SQLException occured while updating the scratch card details with srno : "
							+ getSrno(), e);
		} catch (Exception e) {
			logger.error("Updating scratchcard failed ! with srno : "
					+ getSrno(), e);
		}

		finally {
			DbConnectionManager.releaseConnection(dbConnName, connection);
		}
	}

	@Override
	public void prepareResponse() throws Exception {
		this.put("srno", this.getSrno());
		this.put("scratchcardid", this.getScratchCardId());
		this.put("courseid", this.getCourseId());
		this.put("coursetype", this.getCourseType());
		this.put("univname", this.getUnivName());
		this.put("validtill", this.getValidTill());
		this.put("assignedstatus", this.getAssignedStatus());
		this.put("usedstatus", this.getUsedStatus());
		this.put("price", this.getPrice());
		if (this.getAssignedStatus() == 1) {
			this.put("partnerid", this.getAssignedTo());
		}
	}

	public void updateAssignedPartnerDetails(String dbConnName, Logger logger) {
		Connection connection = DbConnectionManager
				.getConnectionByName(dbConnName);
		PreparedStatement ps = null;
		int rs;
		try {

			ps = DbConnectionManager.getPreparedStatement(dbConnName,
					connection, "UpdateScratchCardPartnerMapping");
			ps.setString(1, getAssignedTo());
			ps.setInt(2, getCardActiveStatus());// can be used to deactivate the
												// cards assigned to the partner
			ps.setString(3, getLastModified());
			ps.setString(4, getLastModifiedBy());
			ps.setString(5, getScratchCardId());
			rs = ps.executeUpdate();
			if (rs > 0) {
				if (logger.isDebugEnabled()) {
					logger.debug("Partner-SC mapping is updated into database with partner id : "
							+ getAssignedTo());
				}
			}
		} catch (SQLException e) {
			logger.error(
					"SQLException occured while updating the mapping record with partner id  : "
							+ getAssignedTo(), e);
		} catch (Exception e) {
			logger.debug(
					"Something went wrong while updating mapping record ! ", e);
		}

		finally {
			DbConnectionManager.releaseConnection(dbConnName, connection);
		}
	}

	public void unAssignPartner(String dbConnName, Logger logger) {
		Connection connection = DbConnectionManager
				.getConnectionByName(dbConnName);
		PreparedStatement ps = null;
		int rs;
		try {
			ps = DbConnectionManager.getPreparedStatement(dbConnName,
					connection, "DeleteScratchCardPartnerMapping");
			ps.setString(1, getScratchCardId());
			rs = ps.executeUpdate();
			if (rs > 0) {
				if (logger.isDebugEnabled()) {
					logger.debug("Partner-SC mapping is deleted from database with partner id : "
							+ getAssignedTo());
				}
			}
		} catch (SQLException e) {
			logger.error(
					"SQLException occured while deleting the mapping record with partner id  : "
							+ getAssignedTo(), e);
		} catch (Exception e) {
			logger.debug(
					"Something went wrong while deleting mapping record ! ", e);
		} finally {
			DbConnectionManager.releaseConnection(dbConnName, connection);
		}

	}

	public int fetchUnivName(String dbConnName, Logger logger) {
		Connection connection = DbConnectionManager
				.getConnectionByName(dbConnName);
		PreparedStatement ps = null;
		ResultSet rs;
		try {

			ps = DbConnectionManager.getPreparedStatement(dbConnName,
					connection, "FetchUnivNameByCourseId");
			ps.setString(1, this.getCourseId());
			rs = ps.executeQuery();
			if (rs.next()) {
				this.setUnivName(rs.getString("univname"));
			}
		} catch (SQLException e) {
			logger.error(
					"SQLException occured while deleting the mapping record with partner id  : "
							+ getAssignedTo(), e);
			return 1;
		} catch (Exception e) {
			logger.debug(
					"Something went wrong while deleting mapping record ! ", e);
			return 1;
		}

		finally {
			DbConnectionManager.releaseConnection(dbConnName, connection);
		}
		return 0;
	}
}
