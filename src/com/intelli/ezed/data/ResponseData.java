package com.intelli.ezed.data;

public interface ResponseData
{
   public void prepareResponse() throws Exception;
}
