package com.intelli.ezed.data;

import org.sabre.logging.LoggerNames;

public class EzedLoggerName extends LoggerNames
{
   public static final String SERVICE_LOG = "sabre.service.log";
   public static final String RECON_LOG = "ezed.recon.log";
   public static final String CART_LOG = "ezed.cart.log";
   public static final String RANDOM_QUESTION_LOG = "ezed.randomquest.log";
   public static final String POINTS_ERROR_LOG = "ezed.pointserror.log";
   public static final String STREAM_LOG = "ezed.stream.log";
   public static final String BULK_UPLOAD_LOG = "ezed.bulkupload.log";
   public static final String ANALYTIC_BACKUP_LOG = "ezed.analyticbackup.log";
}
