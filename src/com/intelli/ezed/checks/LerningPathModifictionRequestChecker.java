package com.intelli.ezed.checks;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.a.helper.utils.StringSplitter;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.LearningPathData;
import com.intelli.ezed.data.Response;

public class LerningPathModifictionRequestChecker extends SingletonRule
{

   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   public LerningPathModifictionRequestChecker(String ruleName)
   {
      super(ruleName);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      //email,sessionid,chapterid,lerningpath (~) separated

      String userid = messageData.getParameter("email");
      String chapterid = messageData.getParameter("chapterid");
      String lerningPath = messageData.getParameter("learningpath");
//      String showFlags = messageData.getParameter("showflags");

      if (userid == null || userid.equals("") || userid.equals(" "))
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Email address not provided.");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Please enter correct email id.");
         return RuleDecisionKey.ON_FAILURE;
      }
      if (chapterid == null || chapterid.equals("") || chapterid.equals(" "))
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Chapter provided is not correct. ");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Please enter chaperid correctly.");
         return RuleDecisionKey.ON_FAILURE;
      }

      if (lerningPath == null || lerningPath.equals("") || lerningPath.equals(" "))
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Lerning path provided is not correct.");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Please enter learning path correctly.");
         return RuleDecisionKey.ON_FAILURE;
      }

//      if (showFlags == null || showFlags.trim().compareTo("") == 0)
//      {
//         if (logger.isDebugEnabled())
//         {
//            logger.debug("Content show flags provided were incorrect");
//         }
//         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid content show flags");
//         return RuleDecisionKey.ON_FAILURE;
//      }

      String[] contentIds = null;
      String[] showFlagsArr = null;
      try
      {
         String[] oneContent = StringSplitter.splitString(lerningPath, "~");
         contentIds = new String[oneContent.length];
         showFlagsArr = new String[oneContent.length];
         String[] contentFlagSeparated = null;
         for (int i = 0; i < oneContent.length; i++)
         {
            contentFlagSeparated = StringSplitter.splitString(oneContent[i], ":");
            contentIds[i] = contentFlagSeparated[0];
            showFlagsArr[i] = contentFlagSeparated[1];
         }
      }
      catch (Exception e)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Error parsing learning path", e);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Contents and their show flags improper");
         return RuleDecisionKey.ON_FAILURE;
      }

      if (contentIds.length != showFlagsArr.length)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Length of show flags doesn't match that of content ids in learning path");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Contents and their flags improper");
         return RuleDecisionKey.ON_FAILURE;
      }

      if (logger.isDebugEnabled())
      {
         logger.debug("Change Learning path request recived form userid : " + userid + " for chapterid : " + chapterid);
      }

      LearningPathData data = new LearningPathData();
      data.setChapterId(chapterid);
      data.setContentIdList(contentIds);
      data.setContentShowFlagList(showFlagsArr);
      flowContext.putIntoContext(EzedKeyConstants.LEARNING_PATH_DATA, data);
      return RuleDecisionKey.ON_SUCCESS;
   }
}
