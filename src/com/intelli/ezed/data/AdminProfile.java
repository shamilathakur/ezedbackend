package com.intelli.ezed.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.sabre.db.DbConnectionManager;
import org.slf4j.Logger;

public class AdminProfile extends ResponseObject
{
   private String userid;
   private String password;
   private int chkFlag;
   private ArrayList<Integer> rollId = new ArrayList<Integer>();
   private String name;

   private String sessionid;

   public String getSessionid()
   {
      return sessionid;
   }
   public void setSessionid(String sessionid)
   {
      this.sessionid = sessionid;
   }
   public String getUserid()
   {
      return userid;
   }
   public void setUserid(String userid)
   {
      this.userid = userid;
   }
   public String getPassword()
   {
      return password;
   }
   public void setPassword(String password)
   {
      this.password = password;
   }
   public int getChkFlag()
   {
      return chkFlag;
   }
   public void setChkFlag(int chkFlag)
   {
      this.chkFlag = chkFlag;
   }
   public ArrayList<Integer> getRollId()
   {
      return rollId;
   }
   public void setRollId(ArrayList<Integer> rollId)
   {
      this.rollId = rollId;
   }

   public void fetchAdminProfile(String dbConnName, Logger logger) throws SQLException, Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         //select * form adminusers where userid = ?
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "GetAdminProfile");
         ps.setString(1, getUserid());
         rs = ps.executeQuery();

         if (rs != null && rs.next())
         {
            this.setChkFlag(rs.getInt("chkflag"));
            this.setPassword(rs.getString("password"));
            this.setUserid(rs.getString("userid"));
            this.setName(rs.getString("name"));
         }
         else
         {
            throw new Exception("Admin profile could not be fetched");
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while getting admin profile.", e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while getting admin profile.", e);
         throw e;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
               logger.error("Error while closing resultset while fetching admin profile", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchAdminRoles");
         ps.setString(1, userid);
         rs = ps.executeQuery();
         while (rs.next())
         {
            this.getRollId().add(rs.getInt("roleid"));
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching roles of admin " + getUserid(), e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching roles of admin " + getUserid(), e);
         throw e;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
               logger.error("Error while closing resultset while fetching admin roles", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   public int commitAdminProfile(String dbConnName, Logger logger) throws Exception
   {
      if (getUserid() == null)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("userid is null. Details could not be committed");
         }
         return EzedTransactionCodes.INTERNAL_ERROR;
      }
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "SetAdminChkFlag");
         //ps.setString(1, MobileNumberFormatter.getFormattedMobileNumber(getMobileNumber()));
         ps.setInt(1, getChkFlag());
         ps.setString(2, getPassword());
         ps.setString(3, getUserid());

         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows updated while committing details in DB for admin userid " + getUserid());
         }
         return EzedTransactionCodes.SUCCESS;
      }
      catch (SQLException e)
      {
         throw e;
      }
      catch (Exception e)
      {
         throw e;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   @Override
   public void prepareResponse() throws Exception
   {
      this.put("userid", getUserid());
      Integer[] roles = new Integer[rollId.size()];
      for (int i = 0; i < rollId.size(); i++)
      {
         roles[i] = rollId.get(i);
      }
      this.put("rollid", roles);
      this.put("name", getName());
      //      StringBuffer sb = new StringBuffer();
      //      for (int i = 0; i < rollId.size(); i++)
      //      {
      //         sb.append(rollId.get(i)).append(",");
      //      }
      //      if (sb.length() > 1)
      //      {
      //         sb.deleteCharAt(sb.length() - 1);
      //      }
      //      this.put("rollid", sb.toString());
   }
   public String getName()
   {
      return name;
   }
   public void setName(String name)
   {
      this.name = name;
   }
}
