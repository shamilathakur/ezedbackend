package com.intelli.ezed.bulkupload.rules;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.LinkedProcessorManager;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.bulkupload.data.CourseDetails;
import com.intelli.ezed.bulkupload.data.UniversityDetails;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;

public class UniversityOfferingSorter extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.BULK_UPLOAD_LOG);

   public UniversityOfferingSorter(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      File univFile = (File) flowContext.getFromContext(EzedKeyConstants.BULK_UPLOAD_UNIVFILE);
      UniversityDetails univDetails = (UniversityDetails) flowContext.getFromContext(EzedKeyConstants.BULK_UPLOAD_UNIVOBJECT);
      Map<Object, Object> processorMap = null;
      File[] degreeFiles = univFile.listFiles();
      for (File singleDegreeFile : degreeFiles)
      {
         if (singleDegreeFile.isDirectory())
         {
            File[] streamFiles = singleDegreeFile.listFiles();
            for (File singleStreamFile : streamFiles)
            {
               if (singleStreamFile.isDirectory())
               {
                  File[] yearFiles = singleStreamFile.listFiles();
                  for (File singleYearFile : yearFiles)
                  {
                     if (singleYearFile.isDirectory())
                     {
                        File[] semesterFiles = singleYearFile.listFiles();
                        for (File singleSemesterFile : semesterFiles)
                        {
                           if (singleSemesterFile.isDirectory())
                           {
                              File[] courseFiles = singleSemesterFile.listFiles();
                              for (File singleCourseFile : courseFiles)
                              {
                                 if (singleCourseFile.isDirectory())
                                 {
                                    CourseDetails courseDetails = new CourseDetails();
                                    courseDetails.setUnivname(univDetails.getUniversity());
                                    courseDetails.setDegree(singleDegreeFile.getName());
                                    courseDetails.setStream(singleStreamFile.getName());
                                    courseDetails.setYear(singleYearFile.getName());
                                    courseDetails.setSemester(singleSemesterFile.getName());
                                    courseDetails.setCourseFile(singleCourseFile);
                                    try
                                    {
                                       courseDetails.setOfferingIdCalculated(dbConnName, logger);
                                       processorMap = new HashMap<Object, Object>();
                                       processorMap.put(EzedKeyConstants.BULK_UPLOAD_COURSEOBJECT, courseDetails);
                                       processorMap.put(EzedKeyConstants.BULK_UPLOAD_CONTENTMAP, flowContext.getFromContext(EzedKeyConstants.BULK_UPLOAD_CONTENTMAP));
                                       LinkedProcessorManager.addTask("BulkUploadCourseHandler", processorMap);
                                    }
                                    catch (Exception e)
                                    {
                                       logger.error("Error while processing offering id for course " + singleCourseFile.getAbsolutePath(), e);
                                       continue;
                                    }
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }
      return RuleDecisionKey.ON_SUCCESS;
   }

}
