package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.utils.RandomNumberGenerator;
import com.intelli.ezed.data.AdminProfile;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.RegistrationResponse;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.utils.AccountStatusHelper;
import com.intelli.ezed.utils.PasswordHelper;

public class ResetAdminUser extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;

   public ResetAdminUser(String ruleName, String dbConString)
   {
      super(ruleName);
      this.dbConnName = dbConString;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      AdminProfile adminProfile = (AdminProfile) flowContext.getFromContext(EzedKeyConstants.ADMIN_PROFILE);
      String requestInTime = (String) flowContext.getFromContext(EzedKeyConstants.REQUEST_IN_TIME);

      String userid = adminProfile.getUserid();

      //check if user is blocked. If blocked, dont allow forgot password as well.
      if (AccountStatusHelper.isLock(adminProfile.getChkFlag()))
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("User is blocked. Cannot allow forgot password");
         }
         response.setParameters(EzedTransactionCodes.USER_BLOCKED, "User is blocked");
         return RuleDecisionKey.ON_FAILURE;
      }

      //Created verification key
      String registrationVerificationKey = RandomNumberGenerator.generateRandomHex(15);
      String defaultPass = RandomNumberGenerator.generateRandomHex(7);
      try
      {
         adminProfile.setPassword(PasswordHelper.getEncryptedData(defaultPass));
      }
      catch (IllegalBlockSizeException e2)
      {
         logger.error("Exception while encrypting default password", e2);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal Error. Please try again.");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (BadPaddingException e2)
      {
         logger.error("Exception while encrypting default password", e2);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal Error. Please try again.");
         return RuleDecisionKey.ON_FAILURE;
      }

      //Changing chkflag to zero and default password
      int chkFlag = AccountStatusHelper.resetRegistered(adminProfile.getChkFlag());
      chkFlag = AccountStatusHelper.setForgotPassword(chkFlag);
      adminProfile.setChkFlag(chkFlag);

      Connection connection = DbConnectionManager.getConnectionByName(dbConnName);
      try
      {
         //insert into userverif (userid,verifid,intime) values (?,?,?)
         PreparedStatement insertIntoUserverif = DbConnectionManager.getPreparedStatement(dbConnName, connection, "InsertInUserverif");
         insertIntoUserverif.setString(1, userid);
         insertIntoUserverif.setString(2, registrationVerificationKey);
         insertIntoUserverif.setString(3, requestInTime);
         int i = insertIntoUserverif.executeUpdate();

         if (i == 0)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("No rows inserted into table userverif. RegisterUSer.java");
            }
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal Error. Please try again.");
            return RuleDecisionKey.ON_FAILURE;
         }
         //commiting userProfile to update chkflag into database
         adminProfile.commitAdminProfile(dbConnName, logger);
      }
      catch (SQLException e)
      {
         logger.error("Database Exception.", e);

         if (e.getMessage().contains("userverif_uniq"))
         {
            logger.debug("Need to replace the verification key");
            try
            {
               //update userverif set verifid = ?, intime = ? where userid = ?
               PreparedStatement updateIntoUserverif = DbConnectionManager.getPreparedStatement(dbConnName, connection, "UpdateInUserverif");
               updateIntoUserverif.setString(1, registrationVerificationKey);
               updateIntoUserverif.setString(2, requestInTime);
               updateIntoUserverif.setString(3, userid);
               int j = updateIntoUserverif.executeUpdate();
               if (j == 0)
               {
                  if (logger.isDebugEnabled())
                  {
                     logger.debug("No rows inserted into table while updating userveirf table. RegisterUSer.java");
                  }
                  response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal Error. Please try again.");
                  return RuleDecisionKey.ON_FAILURE;
               }
            }
            catch (Exception e1)
            {
               logger.error("error", e1);
               response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Database exception occourred. Please try again.");
               return RuleDecisionKey.ON_FAILURE;
            }
         }
         else
         {
            response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Database exception occourred. Please try again.");
            return RuleDecisionKey.ON_FAILURE;
         }
      }
      catch (Exception e)
      {
         logger.error("Exception.", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal Error. Please try again.");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, connection);
      }

      RegistrationResponse registrationResponse = new RegistrationResponse();
      registrationResponse.setFirstname(adminProfile.getName());
      registrationResponse.setRegistrationVerificaitonKey(registrationVerificationKey);
      registrationResponse.setDefaultPassword(defaultPass);
      response.setResponseObject(registrationResponse);
      response.setParameters(EzedTransactionCodes.SUCCESS, "Userid Reset.");
      return RuleDecisionKey.ON_SUCCESS;
   }
}
