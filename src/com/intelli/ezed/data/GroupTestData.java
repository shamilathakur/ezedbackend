package com.intelli.ezed.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.slf4j.Logger;

import com.a.helper.data.DataFiller;
import com.intelli.ezed.bulkupload.data.TestDetails;

public class GroupTestData
{
   private String groupId;
   private String testId;
   private String questionBankId;
   private String testName;
   private int noOfQuestions;
   private String testTime;

   public void setTestTime(int hours, int minutes, int seconds)
   {
      StringBuffer sb = new StringBuffer();
      sb.append(DataFiller.getLeftZeroPadding(Integer.toString(hours), 2));
      sb.append(DataFiller.getLeftZeroPadding(Integer.toString(minutes), 2));
      sb.append(DataFiller.getLeftZeroPadding(Integer.toString(seconds), 2));
      setTestTime(sb.toString());
   }

   public String getTestName()
   {
      return testName;
   }
   public void setTestName(String testName)
   {
      this.testName = testName;
   }
   public int getNoOfQuestions()
   {
      return noOfQuestions;
   }
   public void setNoOfQuestions(int noOfQuestions)
   {
      this.noOfQuestions = noOfQuestions;
   }
   public String getTestTime()
   {
      return testTime;
   }
   public void setTestTime(String testTime)
   {
      this.testTime = testTime;
   }
   public String getGroupId()
   {
      return groupId;
   }
   public void setGroupId(String groupId)
   {
      this.groupId = groupId;
   }
   public String getTestId()
   {
      return testId;
   }
   public void setTestId(String testId)
   {
      this.testId = testId;
   }
   public void setNewTestId()
   {
      setTestId(TestDetails.fetchNewTestId());
   }
   public String getQuestionBankId()
   {
      return questionBankId;
   }
   public void setQuestionBankId(String questionBankId)
   {
      this.questionBankId = questionBankId;
   }

   public void persistTest(String dbConnName, Logger logger) throws Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertTest");
         ps.setString(1, getTestId());
         ps.setString(2, getGroupId());
         ps.setString(3, getQuestionBankId());
         ps.setInt(4, getNoOfQuestions());
         ps.setString(5, getTestTime());
         ps.setString(6, getTestName());
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in db while inserting test for " + groupId);
         }
      }
      catch (SQLException e)
      {
         logger.error("DB error while inserting test for group id " + getGroupId(), e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while inserting test for group id " + getGroupId(), e);
         throw e;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   public void persistTestToUsers(String dbConnName, Logger logger) throws Exception
   {
      //insert into groupusertest (groupid,testid,userid) select 'GRO22','TES20000000136514958',userid from groupusers where groupid='GRO22';
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertGroupUserTest");
         ps.setString(1, getGroupId());
         ps.setString(2, getTestId());
         ps.setString(3, getGroupId());
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug("Added test id " + getTestId() + " to " + rows + " users of group " + getGroupId());
         }
      }
      catch (SQLException e)
      {
         logger.error("DB error while inserting test to users for group id " + getGroupId(), e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while inserting test to users for group id " + getGroupId(), e);
         throw e;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
}
