package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
public class CourseCountForFilteredPurchasedM extends SingletonRule{
	
	public CourseCountForFilteredPurchasedM(String ruleName, String dbConnName) {
		super(ruleName);
		this.dbConnName = dbConnName;
	}
	private String dbConnName;
    private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
	@Override
	public RuleDecisionKey executeRule(FlowContext flowContext) {
		 Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
	     MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
	     
	     String userId = messageData.getParameter("email");
		  if(userId == null || userId.trim().compareTo("")==0){
		    	 if(logger.isDebugEnabled()){
		    		 logger.debug("user id in request is invalid");
		    	 }
		    	 response.setParameters(EzedTransactionCodes.PROCESSING_REQUEST, "Invalid user id");
		    	 return RuleDecisionKey.ON_FAILURE;
		     }
	     final String query = 
					
					"SELECT "
					+ "COUNT(cl.courseid) as totalcount "
					+ " from courselist cl, universitycourse uc,universityofferings uo, coursestatlist csl"
					+ "  where cl.publishflag =1 "
					+ "  and csl.courseid in( select courseid from usercourse"
					+ "  where userid ='"+userId+"')"
				
					+ "and uo.univofferingid = uc.univofferingid "
					+ "and uc.courseid = csl.courseid and cl.courseid = csl.courseid ";
					
			StringBuilder sb = new StringBuilder("");
			System.out.println("String buffer : " + sb.toString());
	        
			
			/*
			 * Filters are: univName, degree, stream , year, semester
			 */
			
			String univName = messageData.getParameter("univName");
			if (univName != null && univName.trim().compareTo("") != 0 && univName.compareTo("null") != 0) {
				String qryCourseId = " and uo.univname = '" + univName + "'";
				sb.append(qryCourseId);
			}
			
				String degree = messageData.getParameter("degree");
				if (degree != null && degree.trim().compareTo("") != 0 && degree.compareTo("null") != 0) {
					String qryCourseId = " and uo.degree = '" + degree + "'";
					sb.append(qryCourseId);
				}
				
				String stream = messageData.getParameter("stream");
				if (stream != null && stream.trim().compareTo("") != 0 && stream.compareTo("null") != 0) {
					String qryCourseId = " and uo.stream = '" + stream + "'";
					sb.append(qryCourseId);
				} 
			
				String year = messageData.getParameter("year");
				if (year != null && year.trim().compareTo("") != 0 && year.compareTo("null") != 0) {
					String qryCourseId = " and uo.year = '" + year + "'";
					sb.append(qryCourseId);
				} 
			
				String semester = messageData.getParameter("semester");
				if (semester != null && semester.trim().compareTo("") != 0 && semester.compareTo("null") != 0) {
					String qryCourseId = " and uo.semester = '" + semester + "'";
					sb.append(qryCourseId);
				}

			
				System.out.println("String buffer : " + sb.toString());
			String finalQuery = query;
			
			if (!sb.equals("")) {
				finalQuery = finalQuery + sb.toString();
			}
			System.out.println("finalquery "+ finalQuery);
			Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
			PreparedStatement ps = null;
			ResultSet rs = null;
			int totalCount =0;
			
			try {
					ps = conn.prepareStatement(finalQuery);
					System.out.println("Final Query : " + ps);
					rs = ps.executeQuery();
					while (rs.next()) {
						
			         	totalCount = rs.getInt("totalcount");
					}
				
					
			  }
			 catch (SQLException e)
	         {
	            logger.error("Error while fetching CoursesWithPaging", e);
	            response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "PL SQL Error while fetching CoursesWithPaging : " + e.getMessage());
	            return RuleDecisionKey.ON_FAILURE;
	         }
	         catch (Exception e)
	         {
	            logger.error("Error while fetching test details", e);
	            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching CoursesWithPaging : " + e.getMessage());
	            return RuleDecisionKey.ON_FAILURE;
	         }
	         finally
	         {
	            if (rs != null)
	            {
	               try
	               {
	                  rs.close();
	               }
	               catch (SQLException e)
	               {
	               }
	            }
	            DbConnectionManager.releaseConnection(dbConnName, conn);
	         }
			response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
			response.setResponseMessage(Integer.toString(totalCount));
		     
			 return RuleDecisionKey.ON_SUCCESS;

	}

}
