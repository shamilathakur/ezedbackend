package com.intelli.ezed.rules;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.QuizRequestResponse;
import com.intelli.ezed.data.Response;

public class QuizContentLinkFetcher extends SingletonRule
{

   public QuizContentLinkFetcher(String ruleName)
   {
      super(ruleName);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      QuizRequestResponse quizResponse = (QuizRequestResponse) response.getResponseObject();
      return null;
   }

}
