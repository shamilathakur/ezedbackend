package com.intelli.ezed.data;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.intelli.ezed.bulkupload.data.ChapterDetails;

public class GoalData extends ResponseObject
{
   public static final int GOAL_COMPLETE = 1;
   public static final int GOAL_INCOMPLETE = 0;
   public static final int GOAL_FAILED = 2;

   public static int GOAL_ALREADY_COMPLETE = 1;
   public static int GOAL_VALID_FOR_UPDATE = 0;
   public static int GOAL_BEFORE_STARTDATE = 2;
   public static int GOAL_AFTER_ENDDATE = 3;
   public static int GOAL_BEFORE_TODAY = 4;
   private static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
   private static SimpleDateFormat sdfFrontend = new SimpleDateFormat("yyyy-MM-dd");
   private static SimpleDateFormat sdfEmail = new SimpleDateFormat("dd-MM-yyyy");

   private String courseId;
   private String chapterId;
   private String contentId;
   private int hours;
   private Date endDate;
   private boolean isGoalComplete;
   private boolean isGoalFailed;
   private boolean isGoalFailedShadow;
   private int validity;
   private String contentname;
   private String coursename;
   private String contentShowFlag = ChapterDetails.SHOW_CONTENT;
   private int customFlag = 0;

   public boolean isGoalFailed()
   {
      return isGoalFailed;
   }
   public String getContentname()
   {
      return contentname;
   }
   public void setContentname(String contentname)
   {
      this.contentname = contentname;
   }
   public int getValidity()
   {
      return validity;
   }
   public void setValidity(int validity)
   {
      this.validity = validity;
   }
   public boolean isGoalComplete()
   {
      return isGoalComplete;
   }
   public void setGoalComplete(int isGoalComplete)
   {
      if (isGoalComplete == GOAL_COMPLETE)
      {
         this.isGoalComplete = true;
         this.isGoalFailed = false;
      }
      else if (isGoalComplete == GOAL_FAILED)
      {
         this.isGoalComplete = false;
         this.isGoalFailed = true;
      }
      else
      {
         this.isGoalComplete = false;
         this.isGoalFailed = false;
      }
   }
   public String getCourseId()
   {
      return courseId;
   }
   public void setCourseId(String courseId)
   {
      this.courseId = courseId;
   }
   public String getChapterId()
   {
      return chapterId;
   }
   public void setChapterId(String chapterId)
   {
      this.chapterId = chapterId;
   }
   public String getContentId()
   {
      return contentId;
   }
   public void setContentId(String contentId)
   {
      this.contentId = contentId;
   }
   public int getHours()
   {
      return hours;
   }
   public void setHours(int hours)
   {
      this.hours = hours;
   }
   public Date getEndDate()
   {
      return endDate;
   }
   public void setEndDate(Date endDate)
   {
      this.endDate = endDate;
   }

   @Override
   public void prepareResponse() throws Exception
   {
      //      this.put("chapterid", getChapterId());
      //      this.put("contentid", getContentId());
      //      this.put("enddate", sdf.format(getEndDate()));
      //      this.put("iscompleted", isGoalComplete());
      StringBuffer sb = new StringBuffer();
      if (getChapterId() == null)
      {
         setChapterId("CHACCCCCCC");
      }
      sb.append("EventID").append("|").append(getCourseId()).append(getChapterId()).append(getContentId()).append("~");
      sb.append("StartDateTime").append("|").append(sdfFrontend.format(getEndDate())).append("~");
      sb.append("Title").append("|").append(getContentname()).append("~");
      sb.append("URL").append("|").append("").append("~");
      sb.append("Description").append("|").append("");
      this.put("calendarstring", sb.toString());
   }

   public String getString(int seqNo)
   {
      StringBuffer sb = new StringBuffer();
      sb.append("!EventID!").append("|").append(seqNo).append("~");
      sb.append("!StartDateTime!").append("|").append("!").append(sdfFrontend.format(getEndDate())).append("!").append("~");
      sb.append("!Title!").append("|").append("!").append(getContentname()).append("!").append("~");
      sb.append("!URL!").append("|").append("!!").append("~");
      if (getChapterId() == null)
      {
         setChapterId("CHACCCCCCC");
      }
      sb.append("!Description!").append("|").append("!").append(getCourseId()).append(getChapterId()).append(getContentId()).append("!").append("~");
      if (this.isGoalFailedShadow())
      {
         sb.append("!CssClass!").append("|").append("!").append("Yellow").append("!");
      }
      else if (this.isGoalComplete())
      {
         sb.append("!CssClass!").append("|").append("!").append("Green").append("!");
      }
      else if (this.isGoalFailed())
      {
         sb.append("!CssClass!").append("|").append("!").append("Red").append("!");
      }
      else
      {
         sb.append("!CssClass!").append("|").append("!").append("Blue").append("!");
      }
      return sb.toString();
   }
   public String getContentShowFlag()
   {
      return contentShowFlag;
   }
   public void setContentShowFlag(String contentShowFlag)
   {
      this.contentShowFlag = contentShowFlag;
   }
   public boolean isGoalFailedShadow()
   {
      return isGoalFailedShadow;
   }
   public void setGoalFailedShadow(boolean isGoalFailedShadow)
   {
      this.isGoalFailedShadow = isGoalFailedShadow;
   }
   public String getCoursename()
   {
      return coursename;
   }
   public void setCoursename(String coursename)
   {
      this.coursename = coursename;
   }

   public String getFailedEmailMessage()
   {
      StringBuffer sb = new StringBuffer();
      sb.append("The goal to complete viewing ").append(getContentname()).append(" scheduled to be completed by ").append(sdfEmail.format(getEndDate()));
      sb.append(" for course ").append(getCoursename()).append("could not be achieved. ");
      sb.append("Please go to goals section of course and reschedule the goal to achieve it in future");
      return sb.toString();
   }
   public int getCustomFlag()
   {
      return customFlag;
   }
   public void setCustomFlag(int customFlag)
   {
      this.customFlag = customFlag;
   }
}