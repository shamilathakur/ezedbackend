package com.intelli.ezed.data;

public class SymLinkObject extends ResponseObject
{

   private String link;

   public String getLink()
   {
      return link;
   }

   public void setLink(String link)
   {
      this.link = link;
   }

   @Override
   public void prepareResponse() throws Exception
   {
      this.put("link", getLink());
   }

}
