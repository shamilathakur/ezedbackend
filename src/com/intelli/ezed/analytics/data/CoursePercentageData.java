package com.intelli.ezed.analytics.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.slf4j.Logger;

import com.intelli.ezed.data.ResponseObject;

public class CoursePercentageData extends ResponseObject
{
   private String courseId;
   private String userId;
   private String coursename;
   private int completionPercent;
   private int avgCompPercentSameUniv;
   private int avgCompPercentSameCollege;
   private int avgCompPercentSameCity;

   public String getCoursename()
   {
      return coursename;
   }
   public void setCoursename(String coursename)
   {
      this.coursename = coursename;
   }
   public String getCourseId()
   {
      return courseId;
   }
   public String getUserId()
   {
      return userId;
   }
   public void setUserId(String userId)
   {
      this.userId = userId;
   }
   public void setCourseId(String courseId)
   {
      this.courseId = courseId;
   }
   public int getCompletionPercent()
   {
      return completionPercent;
   }
   public void setCompletionPercent(int completionPercent)
   {
      this.completionPercent = completionPercent;
   }
   public int getAvgCompPercentSameUniv()
   {
      return avgCompPercentSameUniv;
   }
   public void setAvgCompPercentSameUniv(int avgCompPercentSameUniv)
   {
      this.avgCompPercentSameUniv = avgCompPercentSameUniv;
   }
   public int getAvgCompPercentSameCollege()
   {
      return avgCompPercentSameCollege;
   }
   public void setAvgCompPercentSameCollege(int avgCompPercentSameCollege)
   {
      this.avgCompPercentSameCollege = avgCompPercentSameCollege;
   }
   public int getAvgCompPercentSameCity()
   {
      return avgCompPercentSameCity;
   }
   public void setAvgCompPercentSameCity(int avgCompPercentSameCity)
   {
      this.avgCompPercentSameCity = avgCompPercentSameCity;
   }

   public void fetchAverageValues(String cityId, String univName, String collegeName, String dbConnName, Logger logger) throws SQLException, Exception
   {
      this.fetchCompletionPercent(dbConnName, logger);
      this.fetchAvgCompletionPercentSameCity(dbConnName, logger, cityId);
      this.fetchAvgCompletionPercentSameUniv(dbConnName, logger, univName);
      this.fetchAvgCompletionPercentSameCollege(dbConnName, logger, collegeName);
   }

   private void fetchCompletionPercent(String dbConnName, Logger logger) throws SQLException, Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchAverageCourseCompletionPercent");
         ps.setString(1, getUserId());
         ps.setString(2, getCourseId());
         rs = ps.executeQuery();
         if (rs.next())
         {
            setCompletionPercent(rs.getInt("percentcomplete"));
            setCoursename(rs.getString("coursename"));
         }
         else
         {
            throw new Exception("Course not found for user");
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching completion percentage for userid " + getUserId(), e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching completion percentage for userid " + getUserId(), e);
         throw e;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
               logger.error("Error while closing result set while fetching average completion percentage in same city", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   private void fetchAvgCompletionPercentSameCity(String dbConnName, Logger logger, String cityId) throws SQLException, Exception
   {
      if (cityId == null)
      {
         this.setAvgCompPercentSameCity(0);
         return;
      }
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchAverageCompPercentSameCity");
         ps.setString(1, cityId);
         ps.setString(2, getCourseId());
         rs = ps.executeQuery();
         if (rs.next())
         {
            float val = rs.getFloat(1);
            int intVal = (int) val;
            if (intVal < val)
            {
               intVal++;
            }
            setAvgCompPercentSameCity(intVal);
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching average completion percentage in city " + cityId, e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching average completion percentage in city " + cityId, e);
         throw e;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
               logger.error("Error while closing result set while fetching average completion percentage in same city", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   private void fetchAvgCompletionPercentSameUniv(String dbConnName, Logger logger, String univName) throws SQLException, Exception
   {
      if (univName == null)
      {
         this.setAvgCompPercentSameUniv(0);
         return;
      }
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchAverageCompPercentSameUniv");
         ps.setString(1, univName);
         ps.setString(2, getCourseId());
         rs = ps.executeQuery();
         if (rs.next())
         {
            float val = rs.getFloat(1);
            int intVal = (int) val;
            if (intVal < val)
            {
               intVal++;
            }
            setAvgCompPercentSameUniv(intVal);
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching average completion percentage in univ " + univName, e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching average completion percentage in univ " + univName, e);
         throw e;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
               logger.error("Error while closing result set while fetching average completion percentage in same univ", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   private void fetchAvgCompletionPercentSameCollege(String dbConnName, Logger logger, String collegeName) throws SQLException, Exception
   {
      if (collegeName == null)
      {
         this.setAvgCompPercentSameCollege(0);
         return;
      }
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchAverageCompPercentSameCollege");
         ps.setString(1, collegeName);
         ps.setString(2, getCourseId());
         rs = ps.executeQuery();
         if (rs.next())
         {
            float val = rs.getFloat(1);
            int intVal = (int) val;
            if (intVal < val)
            {
               intVal++;
            }
            setAvgCompPercentSameCollege(intVal);
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching average completion percentage in college " + collegeName, e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching average completion percentage in college " + collegeName, e);
         throw e;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
               logger.error("Error while closing result set while fetching average completion percentage in same college", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
   @Override
   public void prepareResponse() throws Exception
   {
      this.put("name", getCoursename());
      int[] datas = new int[4];
      datas[0] = getCompletionPercent();
      datas[1] = getAvgCompPercentSameCity();
      datas[2] = getAvgCompPercentSameUniv();
      datas[3] = getAvgCompPercentSameCollege();
      this.put("data", datas);
      //      this.put("completionpercent", getCompletionPercent());
      //      this.put("averagecompletionpercentcity", getAvgCompPercentSameCity());
      //      this.put("averagecompletionpercentuniv", getAvgCompPercentSameUniv());
      //      this.put("averagecompletionpercentcollege", getAvgCompPercentSameCollege());
   }
}
