package com.intelli.ezed.utils;

import java.util.regex.Pattern;

public class Globals
{
   public static boolean isBox = false;
   public static final String FIB_SEPARATOR = "~~";
   public static final Pattern emailPattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
   public static final String USER_SESSION_LOCK_SET = "UserSessionLockSet";
   public static final String ADMIN_SESSION_LOCK_SET = "AdminSessionLockSet";
   public static final int UNIVERSITY_CONFIRMED = 1;//created by Saroj on 10th July, 2015
   public static final int COLLEGE_CONFIRMED = 1;//created by Saroj on 10th July, 2015
   public static final String DB_IN_OUT_DATE_FORMAT="yyyyMMddHHmmss";//Created By Saroj on 24 August 
   public static final String DISPLAY_DATE_FORMAT="E, dd MMM yyyy HH:mm:ss";//Created By Saroj on 24 August 
   public static final String DISPLAY_DATE_FORMAT_WITHOUT_TIME="E, dd MMM yyyy";//Created By Saroj on 24 August 
   
}
