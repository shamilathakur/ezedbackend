package com.intelli.ezed.utils;

import java.io.File;
import java.util.HashSet;

import org.sabre.utils.map.TimedMap;
import org.slf4j.Logger;

import com.a.helper.data.DataFiller;

public class SymLinkManager
{
   private SymlinkNameGenerator nameGenerator;
   private HashSet<String> activeSymLinks = null;
   private TimedMap<String, Object> longTimedMap;
   private TimedMap<String, Object> shortTimedMap;
   private TimedMap<String, Object> nonSessionTimedMap;
   private SymLinkTranslateObject allSymLinks;
   private SymLinkTranslateObjectNoSession allSymLinksNoSession;
   private HashSet<String> activeNonSessionSymLinks = null;

   public SymLinkManager(String seqNoGenName, int linkSize, long longTimeout, long shortTimeout, long nonSessionTimeout)
   {
      nameGenerator = new SymlinkNameGenerator(seqNoGenName, linkSize);
      activeSymLinks = new HashSet<String>();
      activeNonSessionSymLinks = new HashSet<String>();
      nonSessionTimedMap = new TimedMap<String, Object>(25, nonSessionTimeout, new SymLinkTimeoutHandler());
      longTimedMap = new TimedMap<String, Object>(25, longTimeout, new SymLinkTimeoutHandler());
      shortTimedMap = new TimedMap<String, Object>(25, shortTimeout, new SymLinkTimeoutHandler());
      allSymLinks = new SymLinkTranslateObject();
      allSymLinksNoSession = new SymLinkTranslateObjectNoSession();
   }

   public String addSymLinkShortTimeout(String sessionId, String contentId, String origLink, Logger logger) throws Exception
   {
      String sessionIdUsage = DataFiller.getLeftZeroPadding(sessionId, 20);
      String contentIdUsage = DataFiller.getLeftZeroPadding(contentId, 20);
      //      File origFile = new File(origLink);
      //      String origLinkOnly = origFile.getParentFile().getAbsolutePath();
      //      String origFileName = origFile.getName();
      String persistId = sessionIdUsage + contentIdUsage;
      synchronized (activeSymLinks)
      {
         while (activeSymLinks.contains(persistId.intern()))
         {
            try
            {
               activeSymLinks.wait();
            }
            catch (InterruptedException e)
            {
               if (logger.isDebugEnabled())
               {
                  logger.debug("caught while adding rrn data to rrn list", e);
               }
            }
         }
         activeSymLinks.add(persistId.intern());
         //         rrnLockObject.setLockedState(true);
         if (logger.isTraceEnabled())
         {
            logger.trace("String " + persistId + " locked");
         }
         SymLinkObject linkObject = allSymLinks.getExistingSymLinkPresent(sessionId, contentIdUsage);
         //         if (linkObject == null)
         //         {
         //            String newLink = nameGenerator.getEncryptedSymLinkName();
         //            linkObject = allSymLinks.putSymLink(sessionId, contentIdUsage, origLinkOnly, newLink);
         //         }
         //outside because in case of short timeout, it will always be an already present link
         shortTimedMap.put(persistId, linkObject);

         activeSymLinks.remove(persistId.intern());
         activeSymLinks.notify();
         if (logger.isTraceEnabled())
         {
            logger.trace("String " + persistId + " unlocked");
         }

         return linkObject.getTranslatedContentLink() + "/" + linkObject.getFileName();
      }
   }

   public String getSymLinkNoSession(String contentId, String origLink, Logger logger, String newLinkLocation) throws Exception
   {
      String contentIdUsage = DataFiller.getLeftZeroPadding(contentId, 20);
      File origFile = new File(origLink);
      String origLinkOnly = origFile.getParentFile().getAbsolutePath();
      String origFileName = origFile.getName();

      synchronized (activeNonSessionSymLinks)
      {
         while (activeNonSessionSymLinks.contains(contentIdUsage.intern()))
         {
            try
            {
               activeNonSessionSymLinks.wait();
            }
            catch (InterruptedException e)
            {
               if (logger.isDebugEnabled())
               {
                  logger.debug("caught while adding contentid data to active sym link contentid's", e);
               }
            }
         }
         activeNonSessionSymLinks.add(contentIdUsage.intern());
         if (logger.isTraceEnabled())
         {
            logger.trace("String " + contentIdUsage + " locked");
         }
         SymLinkObject linkObject = allSymLinksNoSession.getExistingSymLinkPresent(contentIdUsage);
         if (linkObject == null)
         {
            String newLink = nameGenerator.getEncryptedSymLinkName();
            //            SymLinker.getInstance().createSymLink(origLinkOnly, newLinkLocation + "/" + newLink);
            if (logger.isDebugEnabled())
            {
               logger.debug("Trying to create symlink for orig link " + origLinkOnly + " at new link " + newLinkLocation + "/" + newLink);
            }
            SymLinker.getInstance().createSymLink(origLinkOnly, newLinkLocation + "/" + newLink, logger);
            linkObject = allSymLinksNoSession.putSymLink(contentIdUsage, origLinkOnly, newLinkLocation + "/" + newLink, origFileName);
            nonSessionTimedMap.put(contentIdUsage, linkObject);
         }

         activeNonSessionSymLinks.remove(contentIdUsage.intern());
         activeNonSessionSymLinks.notify();
         if (logger.isTraceEnabled())
         {
            logger.trace("String " + contentIdUsage + " unlocked");
         }

         return linkObject.getTranslatedContentLink() + "/" + origFileName;
      }
   }

   public String getSymLink(String sessionId, String contentId, String origLink, Logger logger, String newLinkLocation) throws Exception
   {
      String sessionIdUsage = DataFiller.getLeftZeroPadding(sessionId, 20);
      String contentIdUsage = DataFiller.getLeftZeroPadding(contentId, 20);
      String persistId = sessionIdUsage + contentIdUsage;
      File origFile = new File(origLink);
      String origLinkOnly = origFile.getParentFile().getAbsolutePath();
      String origFileName = origFile.getName();

      synchronized (activeSymLinks)
      {
         while (activeSymLinks.contains(persistId.intern()))
         {
            try
            {
               activeSymLinks.wait();
            }
            catch (InterruptedException e)
            {
               if (logger.isDebugEnabled())
               {
                  logger.debug("caught while adding rrn data to rrn list", e);
               }
            }
         }
         activeSymLinks.add(persistId.intern());
         //         rrnLockObject.setLockedState(true);
         if (logger.isTraceEnabled())
         {
            logger.trace("String " + persistId + " locked");
         }
         SymLinkObject linkObject = allSymLinks.getExistingSymLinkPresent(sessionId, contentIdUsage);
         if (linkObject == null)
         {
            String newLink = nameGenerator.getEncryptedSymLinkName();
            //            SymLinker.generateSymLink(newLinkLocation + "/" + newLink, origLinkOnly);
            if (logger.isDebugEnabled())
            {
               logger.debug("Trying to create symlink for orig link " + origLinkOnly + " at new link " + newLinkLocation + "/" + newLink);
            }
            SymLinker.getInstance().createSymLink(origLinkOnly, newLinkLocation + "/" + newLink, logger);
            linkObject = allSymLinks.putSymLink(sessionId, contentIdUsage, origLinkOnly, newLinkLocation + "/" + newLink, origFileName);
            longTimedMap.put(persistId, linkObject);
         }

         activeSymLinks.remove(persistId.intern());
         activeSymLinks.notify();
         if (logger.isTraceEnabled())
         {
            logger.trace("String " + persistId + " unlocked");
         }

         return linkObject.getTranslatedContentLink() + "/" + origFileName;
      }
   }

   public boolean removeSymLinkNoSession(String contentId, Logger logger)
   {
      String contentIdUsage = DataFiller.getLeftZeroPadding(contentId, 20);
      String persistId = contentIdUsage;
      synchronized (activeNonSessionSymLinks)
      {
         while (activeNonSessionSymLinks.contains(persistId.intern()))
         {
            try
            {
               activeNonSessionSymLinks.wait();
            }
            catch (InterruptedException e)
            {
               if (logger.isDebugEnabled())
               {
                  logger.debug("caught while adding content id from not is session data to list of locked contentid's", e);
               }
            }
         }
         activeNonSessionSymLinks.add(persistId.intern());
         if (logger.isTraceEnabled())
         {
            logger.trace("String " + persistId + " locked");
         }

         boolean result = false;
         SymLinkObject linkObj = allSymLinksNoSession.removeSymlinkForSession(contentIdUsage);
         if (linkObj != null)
         {
            SymLinker.getInstance().removeSymLink(linkObj.getTranslatedContentLink(), logger);
         }

         nonSessionTimedMap.remove(persistId);

         activeNonSessionSymLinks.remove(persistId.intern());
         activeNonSessionSymLinks.notify();
         if (logger.isTraceEnabled())
         {
            logger.trace("String " + persistId + " unlocked");
         }
         return result;
      }
   }

   public boolean removeSymLink(String sessionId, String contentId, Logger logger)
   {
      String sessionIdUsage = DataFiller.getLeftZeroPadding(sessionId, 20);
      String contentIdUsage = DataFiller.getLeftZeroPadding(contentId, 20);
      String persistId = sessionIdUsage + contentIdUsage;
      synchronized (activeSymLinks)
      {
         while (activeSymLinks.contains(persistId.intern()))
         {
            try
            {
               activeSymLinks.wait();
            }
            catch (InterruptedException e)
            {
               if (logger.isDebugEnabled())
               {
                  logger.debug("caught while adding rrn data to rrn list", e);
               }
            }
         }
         activeSymLinks.add(persistId.intern());
         if (logger.isTraceEnabled())
         {
            logger.trace("String " + persistId + " locked");
         }

         boolean result = false;
         SymLinkObject linkObj = allSymLinks.removeSymlinkForSession(sessionId, contentIdUsage);
         if (linkObj != null)
         {
            //            SymLinker.removeSymLink(linkObj.getTranslatedContentLink());
            SymLinker.getInstance().removeSymLink(linkObj.getTranslatedContentLink(), logger);
         }

         longTimedMap.remove(persistId);
         shortTimedMap.remove(persistId);

         activeSymLinks.remove(persistId.intern());
         activeSymLinks.notify();
         if (logger.isTraceEnabled())
         {
            logger.trace("String " + persistId + " unlocked");
         }
         return result;
      }
   }

   public boolean removeSymLinkForSession(String sessionId)
   {
      //      String sessionIdUsage = DataFiller.getLeftZeroPadding(sessionId, 20);
      return allSymLinks.removeAllSymLinksForSession(sessionId);
   }
}
