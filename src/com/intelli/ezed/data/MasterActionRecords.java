package com.intelli.ezed.data;

import java.util.HashMap;
import java.util.Map;

public class MasterActionRecords
{
   private static Map<String, ActionData> actionRecords = new HashMap<String, ActionData>();
   private static LockObject lockObject = new LockObject();

   public static void getLock()
   {
      synchronized (lockObject)
      {
         while (lockObject.lockingValue == true)
         {
            try
            {
               lockObject.wait();
            }
            catch (InterruptedException e)
            {
               System.out.println("Caught while taking lock in action records");
               e.printStackTrace();
            }
         }
         lockObject.lockingValue = true;
      }
   }

   public static void releaseLock()
   {
      synchronized (lockObject)
      {
         lockObject.lockingValue = false;
         lockObject.notify();
      }
   }

   public static void refreshRecords()
   {
      actionRecords = new HashMap<String, ActionData>();
   }

   public static boolean addRecord(ActionData actionData) throws Exception
   {
      actionRecords.put(actionData.getActionId(), actionData);
      return true;
   }

   public static ActionData fetchRecord(String actionId)
   {
      ActionData singleActionRecord = null;
      synchronized (lockObject)
      {
         getLock();
         String key = actionId;
         singleActionRecord = actionRecords.get(key.intern());
         releaseLock();
      }
      return singleActionRecord;
   }

   public static boolean isRecordPresent(String actionId)
   {
      boolean isPresent = false;
      synchronized (lockObject)
      {
         getLock();
         String key = actionId;
         isPresent = actionRecords.keySet().contains(key);
         releaseLock();
      }
      return isPresent;
   }
}
