package com.intelli.ezed.data;

public class LoginResponse extends ResponseObject
{

   private String sessionId;
   private UserProfile userProfile;

   public String getSessionId()
   {
      return sessionId;
   }

   public void setSessionId(String sessionId)
   {
      this.sessionId = sessionId;
   }

   public UserProfile getUserProfile()
   {
      return userProfile;
   }

   public void setUserProfile(UserProfile userProfile)
   {
      this.userProfile = userProfile;
   }

   @Override
   public void prepareResponse() throws Exception
   {
      this.put("sessionid", getSessionId());
      getUserProfile().prepareResponse();
      this.put("profile", getUserProfile());
   }
}
