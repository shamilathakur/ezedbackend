package com.intelli.ezed.utils.upload;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class QuestionDuplicator
{

   /**
    * @param args
    */
   public static void main(String[] args)
   {
      String[] origCourseId = {"COU20000000137935723","COU20000000137935723","COU20000000137935723","COU20000000137935723","COU20000000137935723","COU20000000137935723","COU20000000137935723","COU20000000137935723","COU20000000137935723","COU20000000137935723","COU20000000137935723","COU20000000137935723","COU20000000137935723","COU20000000137935723","COU10000000138121047","COU20000000137935723","COU20000000137935723","COU20000000137935723","COU20000000137935723","COU20000000137935723","COU20000000137935723","COU20000000137935723"};
      String[] origChapterId = {"CHA20000000137935841","CHA20000000137935829","CHA20000000137935802","CHA20000000137935788","CHA20000000137935800","CHA20000000137935789","CHA20000000137935782","CHA20000000137935801","CHA20000000137935803","CHA20000000137935794","CHA20000000137935831","CHA20000000137935796","CHA20000000137935840","CHA20000000137935784","CHA10000000141446870","CHA20000000137935786","CHA20000000137935797","CHA20000000137935793","CHA20000000137935825","CHA20000000137935783","CHA20000000137935799","CHA20000000137935833"};
      String[] newCourseId = {"COU10000000141501649","COU10000000141501649","COU10000000141501649","COU10000000141501649","COU10000000141501649","COU10000000141501649","COU10000000141501649","COU10000000141501649","COU10000000141501649","COU10000000141501649","COU10000000141501649","COU10000000141501649","COU10000000141501649","COU10000000141501649","COU10000000141501649","COU10000000141501649","COU10000000141501649","COU10000000141501649","COU10000000141501649","COU10000000141501649","COU10000000141501649","COU10000000141501649"};
      String[] newChapterId = {"CHA10000000141501681","CHA10000000141501682","CHA10000000141501683","CHA10000000141501684","CHA10000000141501686","CHA10000000141501685","CHA10000000141501687","CHA10000000141501688","CHA10000000141501689","CHA10000000141501690","CHA10000000141501691","CHA10000000141501692","CHA10000000141501693","CHA10000000141501694","CHA10000000141501704","CHA10000000141501695","CHA10000000141501696","CHA10000000141501697","CHA10000000141501698","CHA10000000141501699","CHA10000000141501700","CHA10000000141501701"};
      Connection conn = null;
      PreparedStatement ps = null;
      ResultSet rs = null;
      int count = 0;
      for (int k = 0; k < origChapterId.length; k++)
      {
         try
         {
            rs = null;
            count = 0;
            conn = getDbConnectionPostgres();
            ps = getCountPostStatement(conn);
            ps.setString(1, origCourseId[k]);
            ps.setString(2, origChapterId[k]);
            rs = ps.executeQuery();
            if (rs.next())
            {
               count = rs.getInt(1);
            }
         }
         catch (Exception e)
         {
            e.printStackTrace();
         }
         finally
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
               // TODO Auto-generated catch block
               e.printStackTrace();
            }

            try
            {
               ps.close();
            }
            catch (SQLException e)
            {
               // TODO Auto-generated catch block
               e.printStackTrace();
            }

            try
            {
               conn.close();
            }
            catch (SQLException e)
            {
               // TODO Auto-generated catch block
               e.printStackTrace();
            }
         }

         int rowCount = 0;
         for (int i = 0; i < count; i++)
         {
            conn = getDbConnectionPostgres();
            ps = getSelectPostStatement(conn);
            try
            {
               ps.setString(1, newCourseId[k]);
               ps.setString(2, newChapterId[k]);
               ps.setString(3, origCourseId[k]);
               ps.setString(4, origChapterId[k]);
               ps.setInt(5, i);
               int rows = ps.executeUpdate();
               rowCount++;
               System.out.println(rows + " rows inserted in db");
            }
            catch (SQLException e)
            {
               // TODO Auto-generated catch block
               e.printStackTrace();
            }
            finally
            {
               try
               {
                  ps.close();
               }
               catch (SQLException e)
               {
                  // TODO Auto-generated catch block
                  e.printStackTrace();
               }
               try
               {
                  conn.close();
               }
               catch (SQLException e)
               {
                  // TODO Auto-generated catch block
                  e.printStackTrace();
               }
            }
         }
         System.out.println("Total rows inserted " + rowCount + " for course " + newCourseId[k] + " and chapter " + newChapterId[k]);
      }
   }
   private static Connection getDbConnectionPostgres()
   {
      try
      {
         String url = "jdbc:postgresql://localhost:15432/ezed";
         Class.forName("org.postgresql.Driver");
         return DriverManager.getConnection(url, "postgres", "postgres");
      }
      catch (Exception e)
      {
         e.printStackTrace();
         System.out.println("Error initialising Postgres connection");
         System.exit(1);
         return null;
      }
   }

   private static PreparedStatement getSelectPostStatement(Connection conn)
   {
      try
      {
         return (conn.prepareStatement("insert into questionbank (questionid,courseid,chapterid,questioncontenttype,questionstring,questioncontent,ans1type,ans2type,ans3type,ans4type,ans1string,ans2string,ans3string,ans4string,ans4content,ans1content,ans2content,ans3content,difficultylevel,processed,questiontype,correctans,ans6string,ans5content,ans6content,ans5type,ans6type,ans5string,explanation,explanationtype,explanationcontent) (select (select 'QUE2'||lpad(cast(cast(substring(max(questionid) from 5 for 17) as numeric) + 1 as text),16,'0') from questionbank where questionid like '%QUE2%'),?,?,questioncontenttype,questionstring,questioncontent,ans1type,ans2type,ans3type,ans4type,ans1string,ans2string,ans3string,ans4string,ans4content,ans1content,ans2content,ans3content,difficultylevel,0,questiontype,correctans,ans6string,ans5content,ans6content,ans5type,ans6type,ans5string,explanation,explanationtype,explanationcontent from questionbank where courseid=? and chapterid=? order by questionid limit 1 offset ?)"));
      }
      catch (Exception e)
      {
         System.out.println("Error initializing postgres statment");
         System.exit(1);
         return null;
      }
   }

   private static PreparedStatement getCountPostStatement(Connection conn)
   {
      try
      {
         return (conn.prepareStatement("select count(*) from questionbank where courseid=? and chapterid=?"));
      }
      catch (Exception e)
      {
         System.out.println("Error initializing postgres statment");
         System.exit(1);
         return null;
      }
   }
}
