package com.intelli.ezed.data;

public class CartDataObject {
	
	private String courseid;
	private String userid;
	private String cartid;
	private String coursetype;
	private String startdate;
	private String enddate;
	private String coursename;
	private String contentonlyamt;
	private String testonlyamt;
	private String fullcourseamt;
	private int isfree;
	
	/**
	 * @return the courseid
	 */
	public String getCourseid() {
		return courseid;
	}
	/**
	 * @param courseid the courseid to set
	 */
	public void setCourseid(String courseid) {
		this.courseid = courseid;
	}
	/**
	 * @return the userid
	 */
	public String getUserid() {
		return userid;
	}
	/**
	 * @param userid the userid to set
	 */
	public void setUserid(String userid) {
		this.userid = userid;
	}
	/**
	 * @return the cartid
	 */
	public String getCartid() {
		return cartid;
	}
	/**
	 * @param cartid the cartid to set
	 */
	public void setCartid(String cartid) {
		this.cartid = cartid;
	}
	/**
	 * @return the coursetype
	 */
	public String getCoursetype() {
		return coursetype;
	}
	/**
	 * @param coursetype the coursetype to set
	 */
	public void setCoursetype(String coursetype) {
		this.coursetype = coursetype;
	}
	/**
	 * @return the startdate
	 */
	public String getStartdate() {
		return startdate;
	}
	/**
	 * @param startdate the startdate to set
	 */
	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
	/**
	 * @return the enddate
	 */
	public String getEnddate() {
		return enddate;
	}
	/**
	 * @param enddate the enddate to set
	 */
	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}
	/**
	 * @return the coursename
	 */
	public String getCoursename() {
		return coursename;
	}
	/**
	 * @param coursename the coursename to set
	 */
	public void setCoursename(String coursename) {
		this.coursename = coursename;
	}
	/**
	 * @return the contentonlyamt
	 */
	public String getContentonlyamt() {
		return contentonlyamt;
	}
	/**
	 * @param contentonlyamt the contentonlyamt to set
	 */
	public void setContentonlyamt(String contentonlyamt) {
		this.contentonlyamt = contentonlyamt;
	}
	/**
	 * @return the testonlyamt
	 */
	public String getTestonlyamt() {
		return testonlyamt;
	}
	/**
	 * @param testonlyamt the testonlyamt to set
	 */
	public void setTestonlyamt(String testonlyamt) {
		this.testonlyamt = testonlyamt;
	}
	/**
	 * @return the fullcourseamt
	 */
	public String getFullcourseamt() {
		return fullcourseamt;
	}
	/**
	 * @param fullcourseamt the fullcourseamt to set
	 */
	public void setFullcourseamt(String fullcourseamt) {
		this.fullcourseamt = fullcourseamt;
	}
	/**
	 * @return the isfree
	 */
	public int getIsfree() {
		return isfree;
	}
	/**
	 * @param isfree the isfree to set
	 */
	public void setIsfree(int isfree) {
		this.isfree = isfree;
	}

}
