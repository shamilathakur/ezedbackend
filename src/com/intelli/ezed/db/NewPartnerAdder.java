package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.PartnerData;
import com.intelli.ezed.data.Response;

public class NewPartnerAdder extends SingletonRule {

	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
	private String dbConnName;

	public NewPartnerAdder(String ruleName, String dbConnName) {
		super(ruleName);
		this.dbConnName = dbConnName;
	}

	@Override
	public RuleDecisionKey executeRule(FlowContext flowContext) {
		PartnerData data = (PartnerData) flowContext
				.getFromContext(EzedKeyConstants.PARTNER_DATA_OBJECT);
		Response response = (Response) flowContext
				.getFromContext(EzedKeyConstants.EZED_RESPONSE);
		Connection connection = DbConnectionManager
				.getConnectionByName(dbConnName);
		int serialNumber = 0;
		PreparedStatement ps = null;
		ResultSet rs1;
		ps = DbConnectionManager.getPreparedStatement(dbConnName, connection,
				"MaxSrNoFromPartner");
		try {
			rs1 = ps.executeQuery();

			System.out.println("Max Count returned : " + serialNumber);
			if (!rs1.next()) {
				serialNumber = 1;
			} else {
				serialNumber = rs1.getInt("count");
				serialNumber = serialNumber + 1;
			}
			System.out.println("Final Max Count : " + serialNumber);
			data.setPartnerId((EzedKeyConstants.PARTNER_ID_GENERATOR + (serialNumber)));
		} catch (SQLException e1) {
			e1.printStackTrace();
		} catch (Exception e) {
			System.out.println("Something went wrong ... !!!");
		}

		int rs;

		try {
			ps = DbConnectionManager.getPreparedStatement(dbConnName,
					connection, "AddNewPartner");
			ps.setString(1, data.getPartnerId());
			ps.setString(2, data.getContactPerson());
			ps.setString(3, data.getPartnerName());
			ps.setString(4, data.getAddress());
			ps.setString(5, data.getState());
			ps.setString(6, data.getCity());
			ps.setString(7, data.getContactNumber());
			ps.setString(8, data.getDateOfJoining());
			ps.setInt(9, data.getStatus());
			ps.setString(10, data.getLastmodified());
			ps.setInt(11, serialNumber);
			ps.setString(12, data.getCountry());
			ps.setString(13, data.getEmailaddress());
			ps.setString(14, data.getAddedBy());

			rs = ps.executeUpdate();
			if (rs > 0) {
				if (logger.isDebugEnabled()) {
					logger.debug("Partner is added into database with partner id : "
							+ data.getPartnerId());
				}
			} else {
				logger.error("Profile could not be completed for partner "
						+ data.getPartnerId());
				response.setParameters(EzedTransactionCodes.INTERNAL_ERROR,
						"partner profile could not be added");
				return RuleDecisionKey.ON_FAILURE;
			}
		} catch (SQLException e) {
			if (e.getMessage().contains("partnerlist_pkey")) {
				if (logger.isDebugEnabled()) {
					logger.debug("PartnerId already present in database.", e);
					response.setParameters(
							EzedTransactionCodes.DB_EXCEPTION_OCCURRED,
							"Partnerid already present in database.");
					return RuleDecisionKey.ON_FAILURE;
				}
			}
			logger.error("Error while adding new partner in db for partner "
					+ data.getPartnerId(), e);
			response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED,
					"Error registring partner profile");
			return RuleDecisionKey.ON_FAILURE;
		} catch (Exception e) {
			logger.error("Error while adding partner  " + data.getPartnerId(),
					e);
			response.setParameters(EzedTransactionCodes.INTERNAL_ERROR,
					"Error registring partner profile");
			return RuleDecisionKey.ON_FAILURE;
		} finally {
			DbConnectionManager.releaseConnection(dbConnName, connection);
		}
		response.setParameters(EzedTransactionCodes.SUCCESS,
				"Partner added to the database successful.");
		return RuleDecisionKey.ON_SUCCESS;
	}
}
