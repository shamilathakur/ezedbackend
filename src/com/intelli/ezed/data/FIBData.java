package com.intelli.ezed.data;

import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Map;

import com.intelli.ezed.utils.Randomizer;

public class FIBData extends TestQuestion
{
   private QuestionContent question;
   private ArrayList<QuestionContent> answers = new ArrayList<QuestionContent>();
   private String correctAnswer;

   public QuestionContent getQuestion()
   {
      return question;
   }
   public void setQuestion(QuestionContent question)
   {
      this.question = question;
   }
   public ArrayList<QuestionContent> getAnswers()
   {
      return answers;
   }
   public void setAnswers(ArrayList<QuestionContent> answers)
   {
      this.answers = answers;
   }
   public void addAnswers(QuestionContent answer)
   {
      this.getAnswers().add(answer);
   }

   public ArrayList<TestQuestion> getPermutedQuestions(int noOfRandomQuestions, Randomizer randomizer)
   {
      ArrayList<TestQuestion> permutedQuestions = new ArrayList<TestQuestion>();
      permutedQuestions.add(this);
      return permutedQuestions;
   }

   @Override
   public String toString()
   {
      StringBuffer sb = new StringBuffer();
      if (getQuestion().getType() == QuestionContent.STRING_TYPE)
      {
         sb.append("Question : ").append(getQuestion().getContentString()).append("\n");
      }
      else
      {
         sb.append("Question : ").append(getQuestion().getContentLoc()).append("\n");
      }

      QuestionContent questContent = null;
      for (int i = 0; i < getAnswers().size(); i++)
      {
         sb.append("Answer").append(i + 1).append(" : ");
         questContent = getAnswers().get(i);
         if (questContent.getType() == QuestionContent.STRING_TYPE)
         {
            sb.append(questContent.getContentString()).append("\n");
         }
         else
         {
            sb.append(questContent.getContentLoc()).append("\n");
         }
      }
      return sb.toString();
   }
   @Override
   public void persistQuestion(PreparedStatement ps, int startPoint) throws Exception
   {
      ps.setInt(startPoint + 1, getQuestion().getType());
      if (getQuestion().getType() == QuestionContent.STRING_TYPE)
      {
         ps.setString(startPoint + 2, getQuestion().getContentString());
         ps.setString(startPoint + 3, null);
      }
      else
      {
         ps.setString(startPoint + 2, null);
         ps.setString(startPoint + 3, getQuestion().getContentLoc());
      }

      int j = 0;
      int i = 0;
      for (; i < getAnswers().size(); i++, j += 3)
      {
         ps.setInt(startPoint + 4 + j, getAnswers().get(i).getType());
         if (getAnswers().get(i).getType() == QuestionContent.STRING_TYPE)
         {
            ps.setString(startPoint + 5 + j, getAnswers().get(i).getContentString());
            ps.setString(startPoint + 6 + j, null);
         }
         else
         {
            ps.setString(startPoint + 5 + j, null);
            ps.setString(startPoint + 6 + j, getAnswers().get(i).getContentLoc());
         }
      }

      for (; i < 6; i++, j += 3)
      {
         ps.setInt(startPoint + 4 + j, 0);
         ps.setString(startPoint + j + 5, null);
         ps.setString(startPoint + j + 6, null);
      }

      ps.setString(startPoint + j + 4, null);
      ps.setInt(startPoint + j + 5, TestQuestionData.FIB_QUESTION_TYPE);
      ps.setInt(startPoint + j + 6, getExplanation().getType());
      ps.setString(startPoint + j + 7, getExplanation().getContentString());
      ps.setString(startPoint + j + 8, getExplanation().getContentLoc());
   }
   @Override
   public int getNoOfAnswers()
   {
      return getAnswers().size();
   }
   @Override
   public void persistForBulkUpload(PreparedStatement ps, int startPoint) throws Exception
   {
      ps.setInt(startPoint + 1, getQuestion().getType());
      if (getQuestion().getType() == QuestionContent.STRING_TYPE)
      {
         ps.setString(startPoint + 2, getQuestion().getContentString());
         ps.setString(startPoint + 3, null);
      }
      else
      {
         ps.setString(startPoint + 2, null);
         ps.setString(startPoint + 3, getQuestion().getContentLoc());
      }

      int j = 0;
      int i = 0;
      for (; i < getAnswers().size(); i++, j += 3)
      {
         ps.setInt(startPoint + 4 + j, getAnswers().get(i).getType());
         if (getAnswers().get(i).getType() == QuestionContent.STRING_TYPE)
         {
            ps.setString(startPoint + 5 + j, getAnswers().get(i).getContentString());
            ps.setString(startPoint + 6 + j, null);
         }
         else
         {
            ps.setString(startPoint + 5 + j, null);
            ps.setString(startPoint + 6 + j, getAnswers().get(i).getContentLoc());
         }
      }

      for (; i < 6; i++, j += 3)
      {
         ps.setInt(startPoint + 4 + j, 0);
         ps.setString(startPoint + j + 5, null);
         ps.setString(startPoint + j + 6, null);
      }

      ps.setString(startPoint + j + 4, null);
      ps.setInt(startPoint + j + 5, TestQuestionData.FIB_QUESTION_TYPE);
   }

   @Override
   public void writeToFile(FileOutputStream fos) throws IOException
   {
      StringBuffer sb = new StringBuffer();
      sb.append("<question type=\"FIB\">\n");
      if (getQuestionid() != null)
      {
         sb.append("<id>");
         sb.append(getQuestionid());
         sb.append("</id>\n");
      }
      sb.append("<questionstatement>\n");
      sb.append("<type>");
      sb.append(getQuestion().getType());
      sb.append("</type>\n");
      if (getQuestion().getType() == QuestionContent.STRING_TYPE)
      {
         sb.append("<string>");
         sb.append(getQuestion().getContentString());
         sb.append("</string>\n");
      }
      else
      {
         sb.append("<content>");
         sb.append(getQuestion().getContentLoc());
         sb.append("</content>\n");
      }
      sb.append("</questionstatement>\n");
      for (int i = 0; i < getAnswers().size(); i++)
      {
         sb.append("<answer" + (i + 1) + "statement>");
         sb.append("<type>");
         sb.append(getAnswers().get(i).getType());
         sb.append("</type>\n");
         if (getAnswers().get(i).getType() == QuestionContent.STRING_TYPE)
         {
            sb.append("<string>");
            sb.append(getAnswers().get(i).getContentString());
            sb.append("</string>\n");
         }
         else
         {
            sb.append("<content>");
            sb.append(getAnswers().get(i).getContentLoc());
            sb.append("</content>\n");
         }
         sb.append("</answer" + (i + 1) + "statement>\n");
      }
      if (getDifficultyLevel() != 0)
      {
         sb.append("<difficultylevel>");
         sb.append(getDifficultyLevel());
         sb.append("</difficultylevel>\n");
      }
      sb.append("<correctanswer></correctanswer>");
      sb.append("<explanation>\n");
      sb.append("<type>");
      sb.append(getExplanation().getType());
      sb.append("</type>\n");
      if (getExplanation().getType() == QuestionContent.STRING_TYPE)
      {
         sb.append("<string>");
         sb.append(getExplanation().getContentString());
         sb.append("</string>\n");
      }
      else
      {
         sb.append("<content>");
         sb.append(getExplanation().getContentLoc());
         sb.append("</content>\n");
      }
      sb.append("</explanation>\n");
      sb.append("</question>");
      fos.write(sb.toString().getBytes());
   }
   public String getCorrectAnswer()
   {
      return correctAnswer;
   }
   public void setCorrectAnswer(String correctAnswer)
   {
      this.correctAnswer = correctAnswer;
   }

   @Override
   public void replaceContentIds(Map<String, String> contentDirIdMap) throws Exception
   {
      QuestionContent questionContent = getQuestion();
      if (questionContent.getType() == QuestionContent.CONTENT_TYPE)
      {
         String questionContentTemp = contentDirIdMap.get(questionContent.getContentLoc());
         if (questionContentTemp == null)
         {
            throw new Exception("Question content could not be found");
         }
         questionContent.setContentLoc(questionContentTemp);
      }
      questionContent = getExplanation();
      if (questionContent.getType() == QuestionContent.CONTENT_TYPE)
      {
         questionContent.setContentLoc(contentDirIdMap.get(questionContent.getContentLoc()));
      }
   }
}
