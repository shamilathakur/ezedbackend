package com.intelli.ezed.db;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.AdminStatusData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.utils.AccountStatusHelper;

public class AdminStatusFlagsFetcher extends SingletonRule
{

   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;

   private String AUTH = "AUTH";
   private String UNAUTH = "UNAUTH";
   private String BLOCK = "BLOCK";
   private String UNBLOCK = "UNBLOCK";

   public AdminStatusFlagsFetcher(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      String adminUserid = messageData.getParameter("fetchadmin");
      if (adminUserid == null || adminUserid.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Admin user id of whose status and role is to be fetched is invalid " + adminUserid);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid admin user id");
         return RuleDecisionKey.ON_FAILURE;
      }

      AdminStatusData userStatusData = new AdminStatusData();
      Connection connection = DbConnectionManager.getConnectionByName(dbConnName);
      ResultSet resultSet = null;

      try
      {
         //select * from adminusers where userid = ?
         PreparedStatement preparedStatement = DbConnectionManager.getPreparedStatement(dbConnName, connection, "GetAdminStatusWithCreatedByDetails");
         preparedStatement.setString(1, adminUserid);
         resultSet = preparedStatement.executeQuery();

         if (resultSet != null && resultSet.next())
         {
            userStatusData.setChkflag(resultSet.getInt("chkflag"));
            userStatusData.setName(resultSet.getString("name"));
            userStatusData.setCreatedBy(resultSet.getString("addedby"));
            userStatusData.setCreatedByName(resultSet.getString("addedname"));
         }
         else
         {
            logger.error("Error while getting admin profile.");
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "No admin profile found.");
            return RuleDecisionKey.ON_FAILURE;
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while getting admin profile.", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while getting admin profile.");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while getting admin profile.", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while getting admin profile.");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         try
         {
            resultSet.close();
         }
         catch (SQLException e)
         {
            logger.error("Error while closing resultset", e);
         }
         catch (Exception e)
         {
            logger.error("Error while closing resultset", e);
         }
         DbConnectionManager.releaseConnection(dbConnName, connection);
      }

      connection = DbConnectionManager.getConnectionByName(dbConnName);
      resultSet = null;
      StringBuffer adminRoll = new StringBuffer();
      try
      {
         //select * form adminrepo where userid = ?
         PreparedStatement preparedStatement = DbConnectionManager.getPreparedStatement(dbConnName, connection, "GetAdminRole");
         preparedStatement.setString(1, adminUserid);
         resultSet = preparedStatement.executeQuery();

         while (resultSet.next())
         {
            int temp = resultSet.getInt("roleid");
            adminRoll.append(temp).append(",");
         }

         if (adminRoll.length() > 0)
         {
            adminRoll.deleteCharAt(adminRoll.length() - 1);
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while getting admin role.", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while getting admin role.");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while getting admin role.", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while getting admin role.");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         try
         {
            resultSet.close();
         }
         catch (SQLException e)
         {
            logger.error("Error while closing resultset", e);
         }
         catch (Exception e)
         {
            logger.error("Error while closing resultset", e);
         }
         DbConnectionManager.releaseConnection(dbConnName, connection);
      }


      int chkflag = userStatusData.getChkflag();
      logger.debug("CHKFLAG : " + chkflag);

      StringBuffer adminUserStatus = new StringBuffer();
      if (AccountStatusHelper.isValidate(chkflag) == true)
      {
         adminUserStatus.append(AUTH);
      }
      else
      {
         adminUserStatus.append(UNAUTH);
      }

      if (AccountStatusHelper.isLock(chkflag) == true)
      {
         adminUserStatus.append(",").append(BLOCK);
      }
      else
      {
         adminUserStatus.append(",").append(UNBLOCK);
      }

      userStatusData.setUserid(adminUserid);
      userStatusData.setStatusId(adminUserStatus.toString());
      userStatusData.setRoll(adminRoll.toString());
      flowContext.putIntoContext(EzedKeyConstants.ADMIN_STATUS_DATA, userStatusData);
      //      response.setResponseObject(userStatusData);
      //      response.setParameters(EzedTransactionCodes.SUCCESS, "Status returned.");

      return RuleDecisionKey.ON_SUCCESS;
   }
}
