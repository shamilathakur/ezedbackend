package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.ManageUserCourseFromBackendBean;
import com.intelli.ezed.data.Response;

public class DeleteUserCourseByAdmin extends SingletonRule {

	private String dbConnName;
	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

	public DeleteUserCourseByAdmin(String ruleName, String dbConnName) {
		super(ruleName);
		this.dbConnName = dbConnName;
	}

	@Override
	public RuleDecisionKey executeRule(FlowContext flowContext) {
		Response response = (Response) flowContext
				.getFromContext(EzedKeyConstants.EZED_RESPONSE);
		ManageUserCourseFromBackendBean data = (ManageUserCourseFromBackendBean) flowContext
				.getFromContext(EzedKeyConstants.ADD_COURSE_TO_USER_FROM_ADMIN);

		Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
		PreparedStatement ps = null;
		int rs;
		try {
			ps = DbConnectionManager.getPreparedStatement(dbConnName, conn,
					"DeleteUserCourseEntry");
			ps.setString(1, data.getUserId());
			ps.setString(2, data.getCourseId());
			ps.setString(3, data.getCourseType());
			rs = ps.executeUpdate();
			if (rs > 0) {
				System.out
						.println("The usercourse has been deleted :: userid : "
								+ data.getUserId() + " :: courseid : "
								+ data.getCourseId());
				if (logger.isDebugEnabled()) {
					logger.info("The usercourse has been deleted :: userid : "
							+ data.getUserId() + " :: courseid : "
							+ data.getCourseId());
				}
			} else {
				System.out
						.println("Something went wrong while deleting usercourse :: userid : "
								+ data.getUserId()
								+ " :: courseid : "
								+ data.getCourseId());
				if (logger.isDebugEnabled()) {
					logger.info("Something went wrong while deleting usercourse :: userid : "
							+ data.getUserId()
							+ " :: courseid : "
							+ data.getCourseId());
				}
			}
		} catch (SQLException e) {
			System.out
					.println("SQLException occured while deleting usercourse from the db :: userid : "
							+ data.getUserId()
							+ " :: courseid : "
							+ data.getCourseId() + e);
			if (logger.isDebugEnabled()) {
				logger.error(
						"Something went wrong and SQLException occured while deleting usercourse :: userid : "
								+ data.getUserId()
								+ " :: courseid : "
								+ data.getCourseId(), e);
			}
			response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED,
					"DB Exception occured !");
			return RuleDecisionKey.ON_FAILURE;
		} catch (Exception e) {
			System.out
					.println("Exception occured while deleting usercourse from the db :: userid : "
							+ data.getUserId()
							+ " :: courseid : "
							+ data.getCourseId() + e);
			if (logger.isDebugEnabled()) {
				logger.error(
						"Something went wrong and Exception occured while deleting usercourse :: userid : "
								+ data.getUserId()
								+ " :: courseid : "
								+ data.getCourseId(), e);
			}
			response.setParameters(EzedTransactionCodes.INTERNAL_ERROR,
					"Internal Exception occured !");
			return RuleDecisionKey.ON_FAILURE;
		} finally {
			DbConnectionManager.releaseConnection(dbConnName, conn);
		}

		return RuleDecisionKey.ON_SUCCESS;
	}
}
