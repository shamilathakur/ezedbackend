package com.intelli.ezed.bulkupload.rules;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import au.com.bytecode.opencsv.CSVReader;

import com.intelli.ezed.bulkupload.data.ContentDetails;
import com.intelli.ezed.bulkupload.data.QuizDetails;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.FIBData;
import com.intelli.ezed.data.MCQData;
import com.intelli.ezed.data.MTFData;
import com.intelli.ezed.data.QuestionContent;

public class QuizCSVDetailsHandler extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.BULK_UPLOAD_LOG);
   private String quizFileName;
   //   private Digester digester = new Digester();
   private static Object lockObject = new Object();
   private String dbConnName;


   public QuizCSVDetailsHandler(String ruleName, String dbConnName, String quizFileName)
   {
      super(ruleName);
      //      initDigester(quizDetailsClassFQCN);
      this.dbConnName = dbConnName;
      this.quizFileName = quizFileName;
   }

   //   private void initDigester(String quizDetailsClassFQCN)
   //   {
   //      digester.addObjectCreate("quizquestions", quizDetailsClassFQCN);
   //      digester.addRule("quizquestions/question", new QuestionConditionRule());
   //
   //      digester.addObjectCreate("quizquestions/question/questionstatement", "com.intelli.ezed.data.QuestionContent");
   //      digester.addCallMethod("quizquestions/question/questionstatement/type", "setType", 0, new Class<?>[]{Integer.class});
   //      digester.addCallMethod("quizquestions/question/questionstatement/string", "setContentString", 0);
   //      digester.addCallMethod("quizquestions/question/questionstatement/content", "setContentLoc", 0);
   //      digester.addSetNext("quizquestions/question/questionstatement/content", "setQuestion", "com.intelli.ezed.data.QuestionContent");
   //
   //      digester.addObjectCreate("quizquestions/question/answer1statement", "com.intelli.ezed.data.QuestionContent");
   //      digester.addCallMethod("quizquestions/question/answer1statement/type", "setType", 0, new Class<?>[]{Integer.class});
   //      digester.addCallMethod("quizquestions/question/answer1statement/string", "setContentString", 0);
   //      digester.addCallMethod("quizquestions/question/answer1statement/content", "setContentLoc", 0);
   //      digester.addSetNext("quizquestions/question/answer1statement/content", "addAnswers", "com.intelli.ezed.data.QuestionContent");
   //
   //      digester.addObjectCreate("quizquestions/question/question1statement", "com.intelli.ezed.data.QuestionContent");
   //      digester.addCallMethod("quizquestions/question/question1statement/type", "setType", 0, new Class<?>[]{Integer.class});
   //      digester.addCallMethod("quizquestions/question/question1statement/string", "setContentString", 0);
   //      digester.addCallMethod("quizquestions/question/question1statement/content", "setContentLoc", 0);
   //      digester.addSetNext("quizquestions/question/question1statement/content", "addQuestions", "com.intelli.ezed.data.QuestionContent");
   //
   //      digester.addObjectCreate("quizquestions/question/answer2statement", "com.intelli.ezed.data.QuestionContent");
   //      digester.addCallMethod("quizquestions/question/answer2statement/type", "setType", 0, new Class<?>[]{Integer.class});
   //      digester.addCallMethod("quizquestions/question/answer2statement/string", "setContentString", 0);
   //      digester.addCallMethod("quizquestions/question/answer2statement/content", "setContentLoc", 0);
   //      digester.addSetNext("quizquestions/question/answer2statement/content", "addAnswers", "com.intelli.ezed.data.QuestionContent");
   //
   //      digester.addObjectCreate("quizquestions/question/question2statement", "com.intelli.ezed.data.QuestionContent");
   //      digester.addCallMethod("quizquestions/question/question2statement/type", "setType", 0, new Class<?>[]{Integer.class});
   //      digester.addCallMethod("quizquestions/question/question2statement/string", "setContentString", 0);
   //      digester.addCallMethod("quizquestions/question/question2statement/content", "setContentLoc", 0);
   //      digester.addSetNext("quizquestions/question/question2statement/content", "addQuestions", "com.intelli.ezed.data.QuestionContent");
   //
   //      digester.addObjectCreate("quizquestions/question/answer3statement", "com.intelli.ezed.data.QuestionContent");
   //      digester.addCallMethod("quizquestions/question/answer3statement/type", "setType", 0, new Class<?>[]{Integer.class});
   //      digester.addCallMethod("quizquestions/question/answer3statement/string", "setContentString", 0);
   //      digester.addCallMethod("quizquestions/question/answer3statement/content", "setContentLoc", 0);
   //      digester.addSetNext("quizquestions/question/answer3statement/content", "addAnswers", "com.intelli.ezed.data.QuestionContent");
   //
   //      digester.addObjectCreate("quizquestions/question/question3statement", "com.intelli.ezed.data.QuestionContent");
   //      digester.addCallMethod("quizquestions/question/question3statement/type", "setType", 0, new Class<?>[]{Integer.class});
   //      digester.addCallMethod("quizquestions/question/question3statement/string", "setContentString", 0);
   //      digester.addCallMethod("quizquestions/question/question3statement/content", "setContentLoc", 0);
   //      digester.addSetNext("quizquestions/question/question3statement/content", "addQuestions", "com.intelli.ezed.data.QuestionContent");
   //
   //      digester.addObjectCreate("quizquestions/question/answer4statement", "com.intelli.ezed.data.QuestionContent");
   //      digester.addCallMethod("quizquestions/question/answer4statement/type", "setType", 0, new Class<?>[]{Integer.class});
   //      digester.addCallMethod("quizquestions/question/answer4statement/string", "setContentString", 0);
   //      digester.addCallMethod("quizquestions/question/answer4statement/content", "setContentLoc", 0);
   //      digester.addSetNext("quizquestions/question/answer4statement/content", "addAnswers", "com.intelli.ezed.data.QuestionContent");
   //
   //      digester.addObjectCreate("quizquestions/question/question4statement", "com.intelli.ezed.data.QuestionContent");
   //      digester.addCallMethod("quizquestions/question/question4statement/type", "setType", 0, new Class<?>[]{Integer.class});
   //      digester.addCallMethod("quizquestions/question/question4statement/string", "setContentString", 0);
   //      digester.addCallMethod("quizquestions/question/question4statement/content", "setContentLoc", 0);
   //      digester.addSetNext("quizquestions/question/question4statement/content", "addQuestions", "com.intelli.ezed.data.QuestionContent");
   //
   //      digester.addObjectCreate("quizquestions/question/answer5statement", "com.intelli.ezed.data.QuestionContent");
   //      digester.addCallMethod("quizquestions/question/answer5statement/type", "setType", 0, new Class<?>[]{Integer.class});
   //      digester.addCallMethod("quizquestions/question/answer5statement/string", "setContentString", 0);
   //      digester.addCallMethod("quizquestions/question/answer5statement/content", "setContentLoc", 0);
   //      digester.addSetNext("quizquestions/question/answer5statement/content", "addAnswers", "com.intelli.ezed.data.QuestionContent");
   //
   //      digester.addObjectCreate("quizquestions/question/question5statement", "com.intelli.ezed.data.QuestionContent");
   //      digester.addCallMethod("quizquestions/question/question5statement/type", "setType", 0, new Class<?>[]{Integer.class});
   //      digester.addCallMethod("quizquestions/question/question5statement/string", "setContentString", 0);
   //      digester.addCallMethod("quizquestions/question/question5statement/content", "setContentLoc", 0);
   //      digester.addSetNext("quizquestions/question/question5statement/content", "addQuestions", "com.intelli.ezed.data.QuestionContent");
   //
   //      digester.addObjectCreate("quizquestions/question/answer6statement", "com.intelli.ezed.data.QuestionContent");
   //      digester.addCallMethod("quizquestions/question/answer6statement/type", "setType", 0, new Class<?>[]{Integer.class});
   //      digester.addCallMethod("quizquestions/question/answer6statement/string", "setContentString", 0);
   //      digester.addCallMethod("quizquestions/question/answer6statement/content", "setContentLoc", 0);
   //      digester.addSetNext("quizquestions/question/answer6statement/content", "addAnswers", "com.intelli.ezed.data.QuestionContent");
   //
   //      digester.addObjectCreate("quizquestions/question/question6statement", "com.intelli.ezed.data.QuestionContent");
   //      digester.addCallMethod("quizquestions/question/question6statement/type", "setType", 0, new Class<?>[]{Integer.class});
   //      digester.addCallMethod("quizquestions/question/question6statement/string", "setContentString", 0);
   //      digester.addCallMethod("quizquestions/question/question6statement/content", "setContentLoc", 0);
   //      digester.addSetNext("quizquestions/question/question6statement/content", "addQuestions", "com.intelli.ezed.data.QuestionContent");
   //
   //      digester.addObjectCreate("quizquestions/question/explanation", "com.intelli.ezed.data.QuestionContent");
   //      digester.addCallMethod("quizquestions/question/explanation/type", "setType", 0, new Class<?>[]{Integer.class});
   //      digester.addCallMethod("quizquestions/question/explanation/string", "setContentString", 0);
   //      digester.addCallMethod("quizquestions/question/explanation/content", "setContentLoc", 0);
   //      digester.addSetNext("quizquestions/question/explanation/content", "setExplanation", "com.intelli.ezed.data.QuestionContent");
   //
   //      digester.addCallMethod("quizquestions/question/correctanswer", "setCorrectAnswer", 0);
   //   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      BulkUploadInitiator initiator = (BulkUploadInitiator) flowContext.getFromContext(EzedKeyConstants.BULK_UPLOAD_STARTER);
      //      Map<String, String> contentDirIdMap = (Map<String, String>) flowContext.getFromContext(EzedKeyConstants.BULK_UPLOAD_CONTENTMAP);
      String contentDirFileName = (String) flowContext.getFromContext(EzedKeyConstants.BULK_UPLOAD_CONTENTFILE);
      File contentDirFile = new File(contentDirFileName);
      Map<String, String> quizDirIdMap = new HashMap<String, String>();

      ContentDetails contentDetails = null;
      File[] files = contentDirFile.listFiles(new ContentFileNameFilter());
      try
      {
         for (int i = 0; i < files.length; i++)
         {
            contentDetails = new ContentDetails();
            contentDetails.setContentName(files[i].getName().substring(0, files[i].getName().lastIndexOf('.')));
            contentDetails.setDesc("");
            contentDetails.setHours(1);
            contentDetails.setContentTypeFromName(files[i].getName());
            contentDetails.setContentLink(files[i].getAbsolutePath());
            contentDetails.setqFlag(1);
            synchronized (lockObject)
            {
               contentDetails.setNewContentId();
            }
            contentDetails.persistContent(dbConnName, logger);
            quizDirIdMap.put(contentDetails.getContentName(), contentDetails.getContentId());
         }
      }
      catch (Exception e)
      {
         logger.error("Error while processing individual contents of quiz", e);
         return RuleDecisionKey.ON_FAILURE;
      }


      String quizFile = contentDirFile.getAbsolutePath() + File.separator + quizFileName;
      String contentId = (String) flowContext.getFromContext(EzedKeyConstants.BULK_UPLOAD_CONTENTID);
      QuizDetails quizDetails = new QuizDetails();

      try
      {
         CSVReader csvReader = null;
         try
         {
            csvReader = new CSVReader(new FileReader(quizFile));
         }
         catch (FileNotFoundException e1)
         {
            logger.error(quizFileName + " was not found. Trying Questions.csv");
            quizFile = contentDirFile.getAbsolutePath() + "/" + "Questions.csv";
            csvReader = new CSVReader(new FileReader(quizFile));
         }
         csvReader.readNext();
         String[] quizDetailRow = null;
         int i = 1;
         String questionContent = null;
         while (true)
         {
            quizDetailRow = csvReader.readNext();
            if (quizDetailRow == null || quizDetailRow[0] == null || quizDetailRow[0].compareTo("") == 0)
            {
               break;
            }

            QuestionContent question = new QuestionContent();
            if (quizDetailRow[1] == null || quizDetailRow[1].compareTo("") == 0)
            {
               question.setType(QuestionContent.CONTENT_TYPE);
               questionContent = quizDirIdMap.get("Q" + Integer.toString(i));

               if (questionContent == null)
               {
                  throw new Exception("Question content not found for question no " + i + " for file " + quizFile);
               }
               question.setContentLoc(questionContent);
            }
            else
            {
               question.setType(QuestionContent.STRING_TYPE);
               question.setContentString(quizDetailRow[1]);
            }

            String explanationContentId = quizDirIdMap.get("E" + Integer.toString(i));
            QuestionContent explanation = null;
            if (explanationContentId != null)
            {
               explanation = new QuestionContent();
               explanation.setType(QuestionContent.CONTENT_TYPE);
               explanation.setContentLoc(explanationContentId);
            }
            else
            {
               explanation = new QuestionContent();
               explanation.setType(QuestionContent.CONTENT_ABSENT);
               explanation.setContentLoc(null);
               explanation.setContentString(null);
            }

            if (quizDetailRow[0].compareToIgnoreCase("MCQ") == 0)
            {
               MCQData mcqData = new MCQData();
               mcqData.setQuestion(question);

               String answerType = quizDetailRow[2];
               if (answerType.compareToIgnoreCase("S") == 0)
               {
                  if (quizDetailRow[3] != null && quizDetailRow[3].compareTo("") != 0)
                  {
                     QuestionContent answer = new QuestionContent();
                     answer.setType(QuestionContent.STRING_TYPE);
                     answer.setContentString(quizDetailRow[3]);
                     mcqData.addAnswers(answer);
                  }
                  else
                  {
                     logger.error("No string options found for quiz for file " + quizFile + " and option no " + i);
                     i++;
                     continue;
                  }
                  if (quizDetailRow[4] != null && quizDetailRow[4].compareTo("") != 0)
                  {
                     QuestionContent answer = new QuestionContent();
                     answer.setType(QuestionContent.STRING_TYPE);
                     answer.setContentString(quizDetailRow[4]);
                     mcqData.addAnswers(answer);
                  }
                  if (quizDetailRow[5] != null && quizDetailRow[5].compareTo("") != 0)
                  {
                     QuestionContent answer = new QuestionContent();
                     answer.setType(QuestionContent.STRING_TYPE);
                     answer.setContentString(quizDetailRow[5]);
                     mcqData.addAnswers(answer);
                  }
                  if (quizDetailRow[6] != null && quizDetailRow[6].compareTo("") != 0)
                  {
                     QuestionContent answer = new QuestionContent();
                     answer.setType(QuestionContent.STRING_TYPE);
                     answer.setContentString(quizDetailRow[6]);
                     mcqData.addAnswers(answer);
                  }
                  if (quizDetailRow[7] != null && quizDetailRow[7].compareTo("") != 0)
                  {
                     QuestionContent answer = new QuestionContent();
                     answer.setType(QuestionContent.STRING_TYPE);
                     answer.setContentString(quizDetailRow[7]);
                     mcqData.addAnswers(answer);
                  }
                  if (quizDetailRow[8] != null && quizDetailRow[8].compareTo("") != 0)
                  {
                     QuestionContent answer = new QuestionContent();
                     answer.setType(QuestionContent.STRING_TYPE);
                     answer.setContentString(quizDetailRow[8]);
                     mcqData.addAnswers(answer);
                  }
               }
               else
               {
                  QuestionContent answer = new QuestionContent();
                  String optionContentId = quizDirIdMap.get("O" + Integer.toString(i) + "_1");
                  if (optionContentId == null)
                  {
                     answer.setType(QuestionContent.CONTENT_ABSENT);
                     logger.error("No content found for option 1 in HTML file for " + quizFile + " in question no " + i);
                     i++;
                     continue;
                  }
                  else
                  {
                     answer.setType(QuestionContent.CONTENT_TYPE);
                     answer.setContentLoc(optionContentId);
                  }
                  mcqData.addAnswers(answer);
                  for (int j = 2; j < 7; j++)
                  {
                     answer = new QuestionContent();
                     optionContentId = quizDirIdMap.get("O" + Integer.toString(i) + "_" + Integer.toString(j));
                     if (optionContentId == null)
                     {
                        //                        answer.setType(QuestionContent.CONTENT_ABSENT);
                     }
                     else
                     {
                        answer.setType(QuestionContent.CONTENT_TYPE);
                        answer.setContentLoc(optionContentId);
                        mcqData.addAnswers(answer);
                     }
                  }
               }
               int status = mcqData.setCorrectAnswer(quizDetailRow[9]);
               if (status == 1)
               {
                  logger.error("Error while reading correct ans for file " + quizFile + " and question no " + i);
               }
               mcqData.setExplanation(explanation);
               quizDetails.addQuestion(mcqData);
            }
            else if (quizDetailRow[0].compareToIgnoreCase("MTF") == 0)
            {
               MTFData mtfData = new MTFData();
               mtfData.setQuestion(question);

               String answerType = quizDetailRow[2];
               if (answerType.compareToIgnoreCase("S") == 0)
               {
                  if (quizDetailRow[3] != null && quizDetailRow[3].compareTo("") != 0)
                  {
                     QuestionContent answer = new QuestionContent();
                     answer.setType(QuestionContent.STRING_TYPE);
                     answer.setContentString(quizDetailRow[3]);
                     mtfData.addAnswers(answer);
                  }
                  else
                  {
                     logger.error("No string options found for quiz for file " + quizFile + " and option no " + i);
                     i++;
                     continue;
                  }
                  if (quizDetailRow[4] != null && quizDetailRow[4].compareTo("") != 0)
                  {
                     QuestionContent answer = new QuestionContent();
                     answer.setType(QuestionContent.STRING_TYPE);
                     answer.setContentString(quizDetailRow[4]);
                     mtfData.addAnswers(answer);
                  }
                  if (quizDetailRow[5] != null && quizDetailRow[5].compareTo("") != 0)
                  {
                     QuestionContent answer = new QuestionContent();
                     answer.setType(QuestionContent.STRING_TYPE);
                     answer.setContentString(quizDetailRow[5]);
                     mtfData.addAnswers(answer);
                  }
                  if (quizDetailRow[6] != null && quizDetailRow[6].compareTo("") != 0)
                  {
                     QuestionContent answer = new QuestionContent();
                     answer.setType(QuestionContent.STRING_TYPE);
                     answer.setContentString(quizDetailRow[6]);
                     mtfData.addAnswers(answer);
                  }
                  if (quizDetailRow[7] != null && quizDetailRow[7].compareTo("") != 0)
                  {
                     QuestionContent answer = new QuestionContent();
                     answer.setType(QuestionContent.STRING_TYPE);
                     answer.setContentString(quizDetailRow[7]);
                     mtfData.addAnswers(answer);
                  }
                  if (quizDetailRow[8] != null && quizDetailRow[8].compareTo("") != 0)
                  {
                     QuestionContent answer = new QuestionContent();
                     answer.setType(QuestionContent.STRING_TYPE);
                     answer.setContentString(quizDetailRow[8]);
                     mtfData.addAnswers(answer);
                  }
               }
               else
               {
                  QuestionContent answer = new QuestionContent();
                  String optionContentId = quizDirIdMap.get("O" + Integer.toString(i) + "_1");
                  if (optionContentId == null)
                  {
                     answer.setType(QuestionContent.CONTENT_ABSENT);
                     logger.error("No content found for option 1 in HTML file for " + quizFile + " in question no " + i);
                     i++;
                     continue;
                  }
                  else
                  {
                     answer.setType(QuestionContent.CONTENT_TYPE);
                     answer.setContentLoc(optionContentId);
                     mtfData.addAnswers(answer);
                  }
                  for (int j = 2; j < 7; j++)
                  {
                     answer = new QuestionContent();
                     optionContentId = quizDirIdMap.get("O" + Integer.toString(i) + "_" + Integer.toString(j));
                     if (optionContentId == null)
                     {
                        //                        answer.setType(QuestionContent.CONTENT_ABSENT);
                     }
                     else
                     {
                        answer.setType(QuestionContent.CONTENT_TYPE);
                        answer.setContentLoc(optionContentId);
                        mtfData.addAnswers(answer);
                     }
                  }
               }

               String questionType = quizDetailRow[11];
               if (questionType.compareToIgnoreCase("S") == 0)
               {
                  if (quizDetailRow[12] != null && quizDetailRow[12].compareTo("") != 0)
                  {
                     QuestionContent answer = new QuestionContent();
                     answer.setType(QuestionContent.STRING_TYPE);
                     answer.setContentString(quizDetailRow[12]);
                     mtfData.addQuestions(answer);
                  }
                  else
                  {
                     logger.error("No string LHS options found for quiz for file " + quizFile + " and option no " + i);
                     i++;
                     continue;
                  }
                  if (quizDetailRow[13] != null && quizDetailRow[13].compareTo("") != 0)
                  {
                     QuestionContent answer = new QuestionContent();
                     answer.setType(QuestionContent.STRING_TYPE);
                     answer.setContentString(quizDetailRow[13]);
                     mtfData.addQuestions(answer);
                  }
                  if (quizDetailRow[14] != null && quizDetailRow[14].compareTo("") != 0)
                  {
                     QuestionContent answer = new QuestionContent();
                     answer.setType(QuestionContent.STRING_TYPE);
                     answer.setContentString(quizDetailRow[14]);
                     mtfData.addQuestions(answer);
                  }
                  if (quizDetailRow[15] != null && quizDetailRow[15].compareTo("") != 0)
                  {
                     QuestionContent answer = new QuestionContent();
                     answer.setType(QuestionContent.STRING_TYPE);
                     answer.setContentString(quizDetailRow[15]);
                     mtfData.addQuestions(answer);
                  }
                  if (quizDetailRow[16] != null && quizDetailRow[16].compareTo("") != 0)
                  {
                     QuestionContent answer = new QuestionContent();
                     answer.setType(QuestionContent.STRING_TYPE);
                     answer.setContentString(quizDetailRow[16]);
                     mtfData.addQuestions(answer);
                  }
                  if (quizDetailRow[17] != null && quizDetailRow[17].compareTo("") != 0)
                  {
                     QuestionContent answer = new QuestionContent();
                     answer.setType(QuestionContent.STRING_TYPE);
                     answer.setContentString(quizDetailRow[17]);
                     mtfData.addQuestions(answer);
                  }
               }
               else
               {
                  QuestionContent answer = new QuestionContent();
                  String optionContentId = quizDirIdMap.get("Q" + Integer.toString(i) + "_1");
                  if (optionContentId == null)
                  {
                     answer.setType(QuestionContent.CONTENT_ABSENT);
                     logger.error("No content found for LHS option 1 in HTML file for " + quizFile + " in question no " + i);
                     i++;
                     continue;
                  }
                  else
                  {
                     answer.setType(QuestionContent.CONTENT_TYPE);
                     answer.setContentLoc(optionContentId);
                  }
                  mtfData.addQuestions(answer);
                  for (int j = 2; j < 7; j++)
                  {
                     answer = new QuestionContent();
                     optionContentId = quizDirIdMap.get("Q" + Integer.toString(i) + "_" + Integer.toString(j));
                     if (optionContentId == null)
                     {
                        //                        answer.setType(QuestionContent.CONTENT_ABSENT);
                     }
                     else
                     {
                        answer.setType(QuestionContent.CONTENT_TYPE);
                        answer.setContentLoc(optionContentId);
                        mtfData.addQuestions(answer);
                     }
                  }
               }
               int status = mtfData.setCorrectAnswer(quizDetailRow[9]);
               if (status == 1)
               {
                  logger.error("Error while reading correct ans for file " + quizFile + " and question no " + i);
               }
               mtfData.setExplanation(explanation);
               quizDetails.addQuestion(mtfData);
            }
            else if (quizDetailRow[0].compareToIgnoreCase("FIB") == 0)
            {
               FIBData fibData = new FIBData();
               fibData.setQuestion(question);

               String answerType = quizDetailRow[2];
               if (answerType.compareToIgnoreCase("S") == 0)
               {
                  if (quizDetailRow[3] != null && quizDetailRow[3].compareTo("") != 0)
                  {
                     QuestionContent answer = new QuestionContent();
                     answer.setType(QuestionContent.STRING_TYPE);
                     answer.setContentString(quizDetailRow[3]);
                     fibData.addAnswers(answer);
                  }
                  else
                  {
                     logger.error("No answers found for FIB for quiz for file " + quizFile + " and option no " + i);
                     i++;
                     continue;
                  }
                  if (quizDetailRow[4] != null && quizDetailRow[4].compareTo("") != 0)
                  {
                     QuestionContent answer = new QuestionContent();
                     answer.setType(QuestionContent.STRING_TYPE);
                     answer.setContentString(quizDetailRow[4]);
                     fibData.addAnswers(answer);
                  }
                  if (quizDetailRow[5] != null && quizDetailRow[5].compareTo("") != 0)
                  {
                     QuestionContent answer = new QuestionContent();
                     answer.setType(QuestionContent.STRING_TYPE);
                     answer.setContentString(quizDetailRow[5]);
                     fibData.addAnswers(answer);
                  }
                  if (quizDetailRow[6] != null && quizDetailRow[6].compareTo("") != 0)
                  {
                     QuestionContent answer = new QuestionContent();
                     answer.setType(QuestionContent.STRING_TYPE);
                     answer.setContentString(quizDetailRow[6]);
                     fibData.addAnswers(answer);
                  }
                  if (quizDetailRow[7] != null && quizDetailRow[7].compareTo("") != 0)
                  {
                     QuestionContent answer = new QuestionContent();
                     answer.setType(QuestionContent.STRING_TYPE);
                     answer.setContentString(quizDetailRow[7]);
                     fibData.addAnswers(answer);
                  }
                  if (quizDetailRow[8] != null && quizDetailRow[8].compareTo("") != 0)
                  {
                     QuestionContent answer = new QuestionContent();
                     answer.setType(QuestionContent.STRING_TYPE);
                     answer.setContentString(quizDetailRow[8]);
                     fibData.addAnswers(answer);
                  }
               }
               else
               {
                  throw new Exception("Correct answers cannot be contents in case of Fill in the Blanks");
               }
               fibData.setCorrectAnswer(quizDetailRow[9]);
               fibData.setExplanation(explanation);
               quizDetails.addQuestion(fibData);
            }
            else
            {
               if (logger.isDebugEnabled())
               {
                  logger.debug("Questions.csv for question bank has the wrong value in column no. 1 for file " + quizFile);
               }
            }
            i++;
         }
      }
      catch (FileNotFoundException e)
      {
         logger.error("Error while reading quiz file " + quizFile + " " + e.getMessage(), e);
         initiator.decrementQuizCount();
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (IOException e)
      {
         logger.error("Error while reading quiz file " + quizFile + " " + e.getMessage(), e);
         initiator.decrementQuizCount();
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while reading quiz file " + quizFile + " " + e.getMessage(), e);
         initiator.decrementQuizCount();
         return RuleDecisionKey.ON_FAILURE;
      }

      try
      {
         quizDetails.setQuizId(contentId);
         //         quizDetails.replaceContentIds(contentDirIdMap);
         quizDetails.persistQuiz(dbConnName, logger);
      }
      catch (IOException e)
      {
         logger.error("Error while reading quiz file " + quizFile + " " + e.getMessage(), e);
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (SAXException e)
      {
         logger.error("Error while reading quiz file " + quizFile + " " + e.getMessage(), e);
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while processing quiz after reading properly from file " + quizFile + " " + e.getMessage(), e);
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         initiator.decrementQuizCount();
      }
      return RuleDecisionKey.ON_SUCCESS;
   }
}
