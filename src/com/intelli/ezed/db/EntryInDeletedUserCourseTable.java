package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.ManageUserCourseFromBackendBean;
import com.intelli.ezed.data.Response;

public class EntryInDeletedUserCourseTable extends SingletonRule {

	private String dbConnName;
	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

	public EntryInDeletedUserCourseTable(String ruleName, String dbConnName) {
		super(ruleName);
		this.dbConnName = dbConnName;
	}

	@Override
	public RuleDecisionKey executeRule(FlowContext flowContext) {
		Response response = (Response) flowContext
				.getFromContext(EzedKeyConstants.EZED_RESPONSE);
		ManageUserCourseFromBackendBean data = (ManageUserCourseFromBackendBean) flowContext
				.getFromContext(EzedKeyConstants.ADD_COURSE_TO_USER_FROM_ADMIN);
		Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
		PreparedStatement ps = null;
		int rs;
		try {// userid,courseid,coursetype,points,startdate,goalsachieved,goalsfailed,percentcomplete,enddate,deletedon,deletedby
			ps = DbConnectionManager.getPreparedStatement(dbConnName, conn,
					"EntryInDeletedUserCourseTable");
			ps.setString(1, data.getUserId());
			ps.setString(2, data.getCourseId());
			ps.setString(3, data.getCourseType());
			ps.setInt(4, data.getPoints());
			ps.setString(5, data.getStartDate());
			ps.setInt(6, data.getGoalsachieved());
			ps.setInt(7, data.getGoalsfailed());
			ps.setInt(8, data.getPercentcomplete());
			ps.setString(9, data.getEndDate());
			ps.setString(10, sdf.format(new Date()));
			ps.setString(11, data.getAddedBy());
			rs = ps.executeUpdate();
			if (rs > 0) {
				logger.info("Deleted course was successfully moved to deleted user course table");
			}
		} catch (SQLException e) {
			System.out.println("SQLException in class :: "
					+ this.getClass().getName() + "   " + e);
			if (logger.isDebugEnabled()) {
				logger.error("SQLException in class :: "
						+ this.getClass().getName() + "   " + e);
			}
			response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED,
					"DB Exception occured !");
			return RuleDecisionKey.ON_FAILURE;
		} catch (Exception e) {
			System.out.println("Internal Exception in class :: "
					+ this.getClass().getName() + "   " + e);
			if (logger.isDebugEnabled()) {
				logger.error("Exception in class :: "
						+ this.getClass().getName() + "   " + e);
			}
			response.setParameters(EzedTransactionCodes.INTERNAL_ERROR,
					"Internal Exception occured !");
			return RuleDecisionKey.ON_FAILURE;
		} finally {
			DbConnectionManager.releaseConnection(dbConnName, conn);
		}
		response.setParameters(EzedTransactionCodes.SUCCESS,
				"User Course Deleted Successfully !");
		return RuleDecisionKey.ON_SUCCESS;
	}

}
