package com.intelli.ezed.bulkupload.data;

import java.util.ArrayList;

public class ActiveSessionList
{
   private static ArrayList<String> activeSessions = new ArrayList<String>(100);
   private static Object lockObject = new Object();

   public static void addToActiveSession(String sessionId)
   {
      synchronized (lockObject)
      {
          System.out.println(activeSessions);

    	  if(activeSessions!=null && activeSessions.contains(sessionId)){
    		  activeSessions.remove(sessionId);
    	  }
         activeSessions.add(sessionId.intern());
         System.out.println(activeSessions);

      }
   }

   public static void removeFromActiveSession(String sessionId)
   {
      synchronized (lockObject)
      {
    	  System.out.println(activeSessions.size());
          System.out.println(activeSessions);

         activeSessions.remove(sessionId.intern());
   	  		
         System.out.println(activeSessions.size());
         System.out.println(activeSessions);

      }
   }

   public static Boolean checkIfSessionActive(String sessionId)
   {
      synchronized (lockObject)
      {
         return activeSessions.contains(sessionId.intern());
      }
   }
}
