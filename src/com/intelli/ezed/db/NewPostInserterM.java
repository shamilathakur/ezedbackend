package com.intelli.ezed.db;

/*
 * Class used for inserting new post for threadid. Used for mobile implementation.
 */

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.ezed.utils.dbutil.PrepareForumPostId;
import com.ezed.utils.dbutil.PreparePostId;
import com.intelli.ezed.data.PostData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.utils.Globals;

public class NewPostInserterM extends SingletonRule{

	private String dbConnName;
	
    private Logger log = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
	
	public NewPostInserterM(String ruleName, String dbConnName)
	   {
	      super(ruleName);
	      this.dbConnName = dbConnName;
	   }

	   @Override
	   public RuleDecisionKey executeRule(FlowContext flowContext)
	   {
		  Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
	      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());

		  ResultSet resultSet=null;
		  Connection connection=null;
		  connection = DbConnectionManager.getConnectionByName(dbConnName);
		  PreparedStatement preparedStatement=null;
	      try{
		        String threadId = messageData.getParameter("threadId");
		        if (threadId == null || threadId.trim().compareTo("") == 0)
		        {
		        	if (log.isDebugEnabled()) {
						log.debug("ThreadId id received in request is invalid");
					}
					response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER,"Invalid thread id");
					return RuleDecisionKey.ON_FAILURE;
		        }
		        
		        String postData = messageData.getParameter("postData");
		        if (postData == null || postData.trim().compareTo("") == 0)
		        {
		        	if (log.isDebugEnabled()) {
						log.debug("postData received in request is invalid");
					}
					response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER,"Invalid postdata");
					return RuleDecisionKey.ON_FAILURE;
		        }
		        
		        String email = messageData.getParameter("email");
		        if (email == null || email.trim().compareTo("") == 0)
		        {
		        	if (log.isDebugEnabled()) {
						log.debug("userid received in request is invalid");
					}
					response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER,"Invalid userid");
					return RuleDecisionKey.ON_FAILURE;
		        }
		        
		        String parentPostId = messageData.getParameter("parentPostId");
		        if (parentPostId == null || parentPostId.trim().compareTo("") == 0)
		        {
		        	parentPostId = "";
		        }
		        
		        String postImagePath = messageData.getParameter("postImagePath");
		        if (postImagePath == null || postImagePath.trim().compareTo("") == 0)
		        {
		        	postImagePath = "";
		        }
		        
		        String profileImagePath = messageData.getParameter("profileImagePath");
		        if (profileImagePath == null || profileImagePath.trim().compareTo("") == 0)
		        {
		        	profileImagePath = "";
		        }
		        
		        String createTime = null;
				try {
					SimpleDateFormat dbInFormat=new SimpleDateFormat(Globals.DB_IN_OUT_DATE_FORMAT);
					createTime = dbInFormat.format(new java.util.Date());
				} catch (Exception e) {
					log.error(
							"Error in NewPostInserter while getting date for forum post!",
							e);
				}// end of catch
		        
				String postId="";
				postId = getPostId();
				
		        preparedStatement= DbConnectionManager.getPreparedStatement(dbConnName, connection, "InsertNewPost");
		     	preparedStatement.setString(1, threadId);
				preparedStatement.setString(2, email);
				preparedStatement.setString(3, postId);
				preparedStatement.setString(4, postData);
				preparedStatement.setString(5, createTime);
				preparedStatement.setString(6, postImagePath);
				preparedStatement.setString(7, parentPostId);
				preparedStatement.setInt(8, 0);
				preparedStatement.setString(9, profileImagePath);
			    int rows = preparedStatement.executeUpdate();
			
			    if(rows>0){
				    if (log.isDebugEnabled()) {
						log.debug(rows + " rows inserted in database when " + postId +"was added.");
					}
			    }
			    else{
			    	response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error");
					return RuleDecisionKey.ON_FAILURE;
			    }
			    
			    preparedStatement= DbConnectionManager.getPreparedStatement(dbConnName, connection, "IncrementPostCount");
			    preparedStatement.setString(1, threadId);
			    rows = preparedStatement.executeUpdate();
			    
			    if(rows>0){
				    if (log.isDebugEnabled()) {
						log.debug("PostCount incremented when adding postId"+postId+ "for threadId"+threadId);
						response.setParameters(EzedTransactionCodes.SUCCESS, "Success");
						return RuleDecisionKey.ON_SUCCESS;
				    }
				    else{
				    	response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error");
						return RuleDecisionKey.ON_FAILURE;
				    }
			    }
			    response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error");
				return RuleDecisionKey.ON_FAILURE;
			}
	      catch (SQLException e) {
				log.error("DB error while inserting new post by user in db", e);
				response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED,
						"Error while processing insert new post request by "+ messageData.getParameter("email"));
				return RuleDecisionKey.ON_FAILURE;
			}	
		  
	      catch (Exception e) {
				log.error("DB error while inserting new post by user in db", e);
				response.setParameters(EzedTransactionCodes.INTERNAL_ERROR,
						"Error while processing insert new post request");
				return RuleDecisionKey.ON_FAILURE;
			}
	      
		    finally
		      {
		         if (resultSet != null)
		         {
		            try
		            {
		            	resultSet.close();
		            }
		            catch (SQLException e)
		            {
		               log.error("Error while closing result set while fetching posts list", e);
		            }
		         }
		         DbConnectionManager.releaseConnection(dbConnName, connection);
		      }
	      
	   }

	   public String getPostId() {

	  		PreparePostId preparePostId = PreparePostId
	  				.getPrepareThreadId();
	  		if (preparePostId != null) {
	  			return preparePostId.getNextId();
	  		} else {
	  			log.error("Error while generating forumPostId !");
	  			return null;
	  		}// end of else

	  	}// end of getPostId

		
	   
}
