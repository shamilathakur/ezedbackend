package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.PartnerData;
import com.intelli.ezed.data.Response;

public class PartnerDetailsUpdater extends SingletonRule {
	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
	private String dbConnName;

	public PartnerDetailsUpdater(String ruleName, String dbConnName) {
		super(ruleName);
		this.dbConnName = dbConnName;
	}

	@Override
	public RuleDecisionKey executeRule(FlowContext flowContext) {
		PartnerData data = (PartnerData) flowContext
				.getFromContext(EzedKeyConstants.PARTNER_DATA_OBJECT);
		Response response = (Response) flowContext
				.getFromContext(EzedKeyConstants.EZED_RESPONSE);
		Connection connection = DbConnectionManager
				.getConnectionByName(dbConnName);
		int flag = (Integer) flowContext.getFromContext("flag");
		PreparedStatement ps = null;
		int rs;

		try {
			ps = DbConnectionManager.getPreparedStatement(dbConnName,
					connection, "UpdatePartner");
			ps.setString(1, data.getContactPerson());
			ps.setString(2, data.getPartnerName());
			ps.setString(3, data.getAddress());
			ps.setString(4, data.getState());
			ps.setString(5, data.getCity());
			ps.setString(6, data.getContactNumber());
			ps.setInt(7, data.getStatus());
			ps.setString(8, data.getLastmodified());
			ps.setString(9, data.getCountry());
			ps.setString(10, data.getEmailaddress());
			ps.setString(11, data.getPartnerId());

			rs = ps.executeUpdate();
			if (rs > 0) {
				if (logger.isDebugEnabled()) {
					logger.debug("Partner is updated with partner id : "
							+ data.getPartnerId());
				}

				/*
				 * remove the if condition in the case : when the partner is
				 * being activated and you want to activate all the inactive
				 * scratch card associated with that partner id
				 */
				if (flag == 1) {
					if (data.getStatus() == 0) {
						updateMappedScratchCards(data.getStatus(),
								data.getPartnerId());
					}
				}
			} else {
				logger.error("Profile could not be completed for partner "
						+ data.getPartnerId());
				response.setParameters(EzedTransactionCodes.INTERNAL_ERROR,
						"partner profile could not be updated");
				return RuleDecisionKey.ON_FAILURE;
			}
		} catch (Exception e) {
			logger.error(
					"Error while updating partner  " + data.getPartnerId(), e);
			response.setParameters(EzedTransactionCodes.INTERNAL_ERROR,
					"Error updating partner profile");
			return RuleDecisionKey.ON_FAILURE;
		} finally {
			DbConnectionManager.releaseConnection(dbConnName, connection);
		}

		response.setParameters(EzedTransactionCodes.SUCCESS,
				"Partner updated in the database successful.");

		return RuleDecisionKey.ON_SUCCESS;
	}

	private void updateMappedScratchCards(int status, String partnerid) {
		Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
		PreparedStatement ps = null;
		int rs;
		try {
			ps = DbConnectionManager.getPreparedStatement(dbConnName, conn,
					"UpdateMappedScratchCards");
			ps.setInt(1, status);
			ps.setString(2, partnerid);
			rs = ps.executeUpdate();
			if (rs > 0) {
				if (logger.isDebugEnabled()) {
					logger.debug("Partner-SC mapping status is updated with partner id : "
							+ partnerid);
				}

			} else {
				logger.debug("Error occured while updating Partner-SC mapping status with partner id : "
						+ partnerid);
			}
		} catch (SQLException e) {
			logger.error(
					"SQL Exception while updating partner-sc mapping status with partnerid: "
							+ partnerid, e);

		} catch (Exception e) {
			logger.error(
					"Unknown Exception while updating partner-sc mapping status with partnerid: "
							+ partnerid, e);

		} finally {
			DbConnectionManager.releaseConnection(dbConnName, conn);
		}
	}
}
