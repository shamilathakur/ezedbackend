package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EventData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.GoalData;

public class GoalAchievedUpdater extends SingletonRule
{

   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public GoalAchievedUpdater(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      EventData eventData = (EventData) flowContext.getFromContext(EzedKeyConstants.EVENT_OBJECT);
      String userid = (String) flowContext.getFromContext(EzedKeyConstants.USER_ID);
      SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
      String inTime = sdf.format(new Date(System.currentTimeMillis()));

      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;

      try
      {
         if (eventData.getChapterId() != null)
         {
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdateGoalComplete");
            ps.setInt(1, GoalData.GOAL_COMPLETE);
            ps.setString(2, inTime);
            ps.setString(3, userid);
            ps.setString(4, eventData.getCourseId());
            ps.setString(5, eventData.getChapterId());
            ps.setString(6, eventData.getContentId());
            ps.setString(7, eventData.getActionId());
         }
         else
         {
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdateGoalCompleteCourseTest");
            ps.setInt(1, GoalData.GOAL_COMPLETE);
            ps.setString(2, inTime);
            ps.setString(3, userid);
            ps.setString(4, eventData.getCourseId());
            ps.setString(5, eventData.getContentId());
            ps.setString(6, eventData.getActionId());
         }
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows updated in db for completing goal for user id " + userid);
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while marking goal as completed for userid " + userid, e);
      }
      catch (Exception e)
      {
         logger.error("Error while marking goal as completed for userid " + userid, e);
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "IncreaseGoalAchievedCount");
         ps.setString(1, userid);
         ps.setString(2, eventData.getCourseId());
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows updated in db while increasing count of goal achieved for user and course");
         }
         return RuleDecisionKey.ON_SUCCESS;
      }
      catch (Exception e)
      {
         logger.error("Error while increasing count of goal achieved in db. Still continuing", e);
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
      return RuleDecisionKey.ON_SUCCESS;
   }

}
