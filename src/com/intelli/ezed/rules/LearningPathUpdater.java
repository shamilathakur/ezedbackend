package com.intelli.ezed.rules;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.LearningPathData;
import com.intelli.ezed.data.Response;

public class LearningPathUpdater extends SingletonRule
{

   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;
   private String processorName;
   private String deleteProcessorName;

   public LearningPathUpdater(String ruleName, String dbConnName, String processorName, String deleteProcessorName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
      this.processorName = processorName;
      this.deleteProcessorName = deleteProcessorName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      LearningPathData data = (LearningPathData) flowContext.getFromContext(EzedKeyConstants.LEARNING_PATH_DATA);
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      //AdminProfile adminProfile = (AdminProfile)flowContext.getFromContext(EzedKeyConstants.ADMIN_PROFILE);

      String chapterId = data.getChapterId();

      try
      {
         data.fetchExistingLearningPathDetails(dbConnName, logger);
      }
      catch (SQLException e1)
      {
         logger.error("Exception occurred while fetching old learning path from the database.", e1);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while processing change learning path");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e1)
      {
         logger.error("Exception occurred while fetching old learning path from the database.", e1);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while processing change learning path");
         return RuleDecisionKey.ON_FAILURE;
      }

      Connection connection = DbConnectionManager.getConnectionByName(dbConnName);

      try
      {
         //delete * from learningpath where chapterid = ?;
         PreparedStatement deleteLerningPath = DbConnectionManager.getPreparedStatement(dbConnName, connection, "DeleteLerningPath");
         deleteLerningPath.setString(1, chapterId);
         int i = deleteLerningPath.executeUpdate();
         if (i == 0)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("No learning path found for chapter id = " + chapterId);
            }
         }
      }
      catch (Exception e)
      {
         //e.printStackTrace();
         logger.error("Exception occoured while deleting old lerning path from the database.", e);
      }

      try
      {
         int j = 1;
         String contentId = null;
         String contentFlag = null;
         for (int i = 0; i < data.getContentIdList().size(); i++)
         {
            contentId = data.getContentIdList().get(i);
            contentFlag = data.getContentShowFlagList().get(i);
            //Insert into learningpath (chapterid,contentid,sequenceno) values(?,?,?)
            PreparedStatement addLearningPath = DbConnectionManager.getPreparedStatement(dbConnName, connection, "InserLearningPath");
            addLearningPath.setString(1, chapterId);
            addLearningPath.setString(2, contentId);
            addLearningPath.setInt(3, j++);
            addLearningPath.setString(4, contentFlag);
            int k = addLearningPath.executeUpdate();

            if (k != 1)
            {
               if (logger.isInfoEnabled())
               {
                  logger.info("Some problem while adding learning path to the database : " + chapterId + " content id : " + contentId);
               }
            }
            else
            {
               if (logger.isInfoEnabled())
               {
                  logger.info("Added content : " + contentId + " to the learningpath of chapter : " + chapterId + " at : " + j);
               }
            }
         }
         data.startGoalModificationHandler(processorName);
         data.startGoalDeletionHandler(deleteProcessorName);
      }
      catch (SQLException e)
      {
         logger.error("Exception occoured : ", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Database exception occoured while updating the learning path.");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Exception occoured : ", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal Error occoured while updating the learning path.");
         return RuleDecisionKey.ON_FAILURE;
      }


      response.setParameters(EzedTransactionCodes.SUCCESS, "Learning Path updated successfully for chapterid : " + chapterId);

      return RuleDecisionKey.ON_SUCCESS;
   }

}
