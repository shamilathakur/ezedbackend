package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.AddScratchCardToUserData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.ScratchCardErrorLogData;
import com.intelli.ezed.utils.ScratchCardHelper;

public class ScratchCardValidityChecker extends SingletonRule {

	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
	private String dbConnName;
	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

	public ScratchCardValidityChecker(String ruleName, String dbConnName) {
		super(ruleName);
		this.dbConnName = dbConnName;
	}

	@SuppressWarnings("deprecation")
	@Override
	public RuleDecisionKey executeRule(FlowContext flowContext) {

		Response response = (Response) flowContext
				.getFromContext(EzedKeyConstants.EZED_RESPONSE);
		// String scratchCardId = (String)
		// flowContext.getFromContext("scratchcardid");
		AddScratchCardToUserData data = (AddScratchCardToUserData) flowContext
				.getFromContext(EzedKeyConstants.ADD_COURSE_USING_SCRATCHCARD_DETAILS);
		ScratchCardErrorLogData errordata = (ScratchCardErrorLogData) flowContext
				.getFromContext(EzedKeyConstants.SCRATCH_CARD_ERROR_LOG_DB);

		System.out.println("In Class : " + this.getClass().getName());
		System.out.print("USERID : " + data.getUserid() + "  Sessionid : "
				+ data.getSessionid());

		Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			ps = DbConnectionManager.getPreparedStatement(dbConnName, conn,
					"FetchScratchCardDetails");
			ps.setString(1, data.getScratchcardid());
			rs = ps.executeQuery();
			if (rs.next()) {
				// checkCardValidity(rs);
				data.setActivestatus(Integer.parseInt(rs
						.getString("activestatus")));
				data.setUsedstatus(Integer.parseInt(rs.getString("usedstatus")));
				errordata.setCourseid(rs.getString("courseid"));
				errordata.setPartnerid(rs.getString("partnerid"));
				Date validTill = rs.getDate("validtill");
				Date newValidTill = validTill;
				newValidTill.setMonth(validTill.getMonth() + 1);
				newValidTill.setDate(validTill.getDate() - 1);
				Date now = new Date();

				System.out.print("Valid till before: "
						+ rs.getString("validtill"));
				System.out.print("Valid till after: " + validTill);

				if (data.getActivestatus() == 1) {
					if (data.getUsedstatus() == 0) {
						if (newValidTill.after(now)) // check if the card is
														// expired ..
						{
							// not expired .. course can be added
							data.setCourseid(rs.getString("courseid"));
							data.setCoursetype(Integer.parseInt(rs
									.getString("coursetype")));
							data.setPrice(Integer.parseInt(rs
									.getString("price")));
						} else {
							// The card is expired ..
							if (logger.isDebugEnabled()) {
								logger.info("Scratch Card found and is Expired"
										+ data.getScratchcardid());
							}
							// callErrorEntry(EzedTransactionCodes.SCRATCH_CARD_EXPIRED);
							errordata.setClassname(this.getClass().getName());
							ScratchCardHelper.callExpiredScratchCard(errordata,
									dbConnName, logger);
							response.setParameters(
									EzedTransactionCodes.SCRATCH_CARD_EXPIRED,
									"Scratch card is Expired !");
							return RuleDecisionKey.ON_FAILURE;
						}
					} else {
						// card is already used ..
						if (logger.isDebugEnabled()) {
							logger.info("Scratch Card found and is is already USED : "
									+ data.getScratchcardid());
						}
						errordata.setClassname(this.getClass().getName());
						ScratchCardHelper.callUsedScratchCard(errordata,
								dbConnName, logger);
						// callErrorEntry(EzedTransactionCodes.SCRATCH_CARD_ALREADY_USED);
						response.setParameters(
								EzedTransactionCodes.SCRATCH_CARD_ALREADY_USED,
								"Scratch card already used !");
						return RuleDecisionKey.ON_FAILURE;
					}
				} else {
					// card is not valid .. either the partner is marked as
					// invalid or that particular card is marked as invalid by
					// the admin ..

					if (logger.isDebugEnabled()) {
						logger.info("Scratch Card found and is INACTIVATED BY ADMIN : "
								+ data.getScratchcardid());
					}
					errordata.setClassname(this.getClass().getName());
					ScratchCardHelper.callInactivatedScratchCard(errordata,
							dbConnName, logger);
					// callErrorEntry(EzedTransactionCodes.SCRATCH_CARD_INACTIVE);
					response.setParameters(
							EzedTransactionCodes.SCRATCH_CARD_INACTIVE,
							"Scratch card is inactivated !");
					return RuleDecisionKey.ON_FAILURE;

				}
			} else {
				if (logger.isDebugEnabled()) {
					logger.info("No scratch Card found for scratchcardid : "
							+ data.getScratchcardid());
				}
				errordata.setClassname(this.getClass().getName());
				ScratchCardHelper.callInvalidScratchCard(errordata, dbConnName,
						logger);
				// callErrorEntry(EzedTransactionCodes.INVALID_SCRATCH_CARD_ID);
				response.setParameters(
						EzedTransactionCodes.INVALID_SCRATCH_CARD_ID,
						"No scratch Card found");
				return RuleDecisionKey.ON_FAILURE;
			}
		} catch (SQLException e) {
			System.out.println("SQL Error In Class : "
					+ this.getClass().getName());
			logger.error("DB Error while checking card validity from db :"
					+ data.getScratchcardid(), e);
			// callErrorEntry(EzedTransactionCodes.DB_EXCEPTION_OCCURRED);
			errordata.setClassname(this.getClass().getName());
			ScratchCardHelper.callDbError(errordata, dbConnName, logger);
			response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED,
					"DB Error while checking card validity from db");
			return RuleDecisionKey.ON_FAILURE;
		} catch (Exception e) {
			logger.error(
					"Internal Error while checking card validity :"
							+ data.getScratchcardid(), e);
			// callErrorEntry(EzedTransactionCodes.INTERNAL_ERROR);
			errordata.setClassname(this.getClass().getName());
			ScratchCardHelper.callInternalError(errordata, dbConnName, logger);
			response.setParameters(EzedTransactionCodes.INTERNAL_ERROR,
					"Error while checking card validity.");
			return RuleDecisionKey.ON_FAILURE;
		} finally {
			DbConnectionManager.releaseConnection(dbConnName, conn);
		}
		flowContext.putIntoContext(
				EzedKeyConstants.ADD_COURSE_USING_SCRATCHCARD_DETAILS, data);
		flowContext.putIntoContext(EzedKeyConstants.SCRATCH_CARD_ERROR_LOG_DB,
				errordata);
		return RuleDecisionKey.ON_SUCCESS;
	}
}
