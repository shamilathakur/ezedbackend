package com.intelli.ezed.analytics.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.sabre.db.DbConnectionManager;
import org.slf4j.Logger;

import com.a.helper.utils.StringSplitter;
import com.intelli.ezed.data.ResponseObject;

public class CourseBreakupReportDataBar extends ResponseObject
{
   private String courseId;
   private String[] chapterId;
   private ArrayList<GranularChapterCounts> datas = null;

   private GenericChartData title = new GenericChartData();
   private GenericChartData xAxis = new GenericChartData();
   private GenericChartData yAxis = new GenericChartData();
   private GenericChartData type = new GenericChartData();

   public void prepareChart() throws Exception
   {
      type.getText().add("type");
      type.getValues().add("bar");
      type.prepareResponse();

      title.getText().add("text");
      title.getValues().add("Course wise breakup report");
      title.prepareResponse();

      xAxis.getText().add("categories");
      String[] cats = {"Video", "Audio", "Note", "Quiz", "Test", "YouTube"};
      xAxis.getValues().add(cats);
      xAxis.getText().add("title");
      GenericChartData xAxisTitleData = new GenericChartData();
      xAxisTitleData.getText().add("text");
      xAxisTitleData.getValues().add("Content Types");
      xAxisTitleData.prepareResponse();
      xAxis.getValues().add(xAxisTitleData);
      xAxis.prepareResponse();

      yAxis.getText().add("min");
      yAxis.getValues().add(0);
      yAxis.getText().add("title");
      GenericChartData yAxisText = new GenericChartData();
      yAxisText.getText().add("text");
      yAxisText.getValues().add("Count");
      yAxisText.getText().add("align");
      yAxisText.getValues().add("high");
      yAxis.getValues().add(yAxisText);
      yAxis.getText().add("labels");
      GenericChartData yAxisLabels = new GenericChartData();
      yAxisLabels.getText().add("overflow");
      yAxisLabels.getValues().add("justify");
      yAxis.getValues().add(yAxisLabels);

      yAxisLabels.prepareResponse();
      yAxisText.prepareResponse();
      yAxis.prepareResponse();
   }

   public void fetchReport(String dbConnName, Logger logger) throws SQLException, Exception
   {
      if (this.getChapterId() == null)
      {
         fetchChapters(dbConnName, logger);
      }
      this.datas = new ArrayList<GranularChapterCounts>(getChapterId().length);
      GranularChapterCounts data = null;
      for (int i = 0; i < getChapterId().length; i++)
      {
         data = new GranularChapterCounts();
         data.setChapterId(getChapterId()[i]);
         data.fetchCounts(dbConnName, logger);
         this.datas.add(data);
      }
   }

   public void fetchChapters(String dbConnName, Logger logger) throws SQLException, Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      ArrayList<String> chapterIdAl = new ArrayList<String>();
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchCoursePathChaptersOnly");
         ps.setString(1, getCourseId());
         rs = ps.executeQuery();
         while (rs.next())
         {
            chapterIdAl.add(rs.getString("chapterid"));
         }

         this.chapterId = new String[chapterIdAl.size()];
         for (int i = 0; i < chapterIdAl.size(); i++)
         {
            this.chapterId[i] = chapterIdAl.get(i);
         }
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
               logger.error("Error while closing resultset while fetching course chapters", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
   @Override
   public void prepareResponse() throws Exception
   {
      JSONArray jsonArray = new JSONArray();

      for (int i = 0; i < datas.size(); i++)
      {
         datas.get(i).prepareResponse();
         jsonArray.put(datas.get(i));
      }
      this.put("chart", type);
      this.put("title", title);
      this.put("xAxis", xAxis);
      this.put("yAxis", yAxis);
      this.put("series", jsonArray);
   }

   public String getCourseId()
   {
      return courseId;
   }

   public void setCourseId(String courseId)
   {
      this.courseId = courseId;
   }

   public String[] getChapterId()
   {
      return chapterId;
   }

   public void setChapterId(String[] chapterId)
   {
      this.chapterId = chapterId;
   }

   public void setChapterId(String chapterId)
   {
      if (chapterId != null)
      {
         this.chapterId = StringSplitter.splitString(chapterId, ",");
      }
   }
}
