package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.FrontendFetchData;
import com.intelli.ezed.data.Response;

public class TestListFromChapterFetcher extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public TestListFromChapterFetcher(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      String courseId = (String) flowContext.getFromContext(EzedKeyConstants.COURSE_ID_LIST);
      String[] chapterIdList = (String[]) flowContext.getFromContext(EzedKeyConstants.CHAPTER_ID_LIST);
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      String[] testArr = null;
      FrontendFetchData testData = new FrontendFetchData();
      response.setResponseObject(testData);
      response.setStatusCodeSend(false);
      testData.setAttribName("aaData");
      ArrayList<String[]> tests = new ArrayList<String[]>();
      int i = 1;

      for (int j = 0; j < chapterIdList.length; j++)
      {
         try
         {
            conn = DbConnectionManager.getConnectionByName(dbConnName);
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchChapterLevelTest");
            ps.setString(1, courseId);
            ps.setString(2, chapterIdList[j]);
            rs = ps.executeQuery();
            while (rs.next())
            {
               testArr = new String[7];
               testArr[0] = Integer.toString(i);
               testArr[1] = rs.getString("testname");
               testArr[2] = Integer.toString(rs.getInt("numberofquestion"));
               testArr[3] = rs.getString("testtime");
               testArr[4] = rs.getString("coursename");
               testArr[5] = rs.getString("chaptername");
               testArr[6] = rs.getString("testid");
               tests.add(testArr);
               i++;
            }
         }
         catch (SQLException e)
         {
            logger.error("Error while fetching test details", e);
            response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching tests");
            return RuleDecisionKey.ON_FAILURE;
         }
         catch (Exception e)
         {
            logger.error("Error while fetching test details", e);
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching tests");
            return RuleDecisionKey.ON_FAILURE;
         }
         finally
         {
            if (rs != null)
            {
               try
               {
                  rs.close();
               }
               catch (SQLException e)
               {
               }
            }
            DbConnectionManager.releaseConnection(dbConnName, conn);
         }
      }
      testData.setAttribList(tests);
      response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
      return RuleDecisionKey.ON_SUCCESS;
   }
}
