package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.FrontendFetchData;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.utils.AccountStatusHelper;

public class UserListFetcher extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private SimpleDateFormat sdfDb = new SimpleDateFormat("yyyyMMddHHmmss");
   private SimpleDateFormat sdfPage = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
   private SimpleDateFormat sdfRequest = new SimpleDateFormat("MM/dd/yyyy");
   private SimpleDateFormat sdfCompareDb = new SimpleDateFormat("yyyyMMdd");

   public UserListFetcher(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      String regDateString = messageData.getParameter("regdate");
      Date fromRegDate = null;
      if (regDateString == null || regDateString.trim().compareTo("") == 0)
      {
         regDateString = null;
      }
      if (regDateString != null)
      {
         try
         {
            fromRegDate = sdfRequest.parse(regDateString);
         }
         catch (ParseException e)
         {
            logger.error("Error while parsing from date in request " + regDateString);
            response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid from date format");
            return RuleDecisionKey.ON_FAILURE;
         }
      }

      regDateString = messageData.getParameter("todate");
      Date toRegDate = null;
      if (regDateString == null || regDateString.trim().compareTo("") == 0)
      {
         regDateString = null;
      }
      if (regDateString != null)
      {
         try
         {
            toRegDate = sdfRequest.parse(regDateString);
         }
         catch (ParseException e)
         {
            logger.error("Error while parsing to date in request " + regDateString);
            response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid to date format");
            return RuleDecisionKey.ON_FAILURE;
         }
      }

      if (toRegDate == null && fromRegDate != null)
      {
         toRegDate = fromRegDate;
      }

      if (fromRegDate == null && toRegDate != null)
      {
         fromRegDate = toRegDate;
      }

      String courseId = messageData.getParameter("courseid");
      if (courseId == null || courseId.trim().compareTo("") == 0)
      {
         courseId = null;
      }
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      String[] userArr = null;
      FrontendFetchData userData = new FrontendFetchData();
      response.setResponseObject(userData);
      response.setStatusCodeSend(false);
      userData.setAttribName("aaData");
      ArrayList<String[]> users = new ArrayList<String[]>();
      try
      {
         if (toRegDate == null && courseId == null)
         {
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchUserRegDetails");
            rs = ps.executeQuery();
         }
         else if (toRegDate == null)
         {
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchUserRegDetailsByCourse");
            ps.setString(1, courseId);
            ps.setString(2, courseId);
            rs = ps.executeQuery();
         }
         else if (courseId == null)
         {
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchUserRegDetailsByDate");
            ps.setString(1, sdfCompareDb.format(fromRegDate) + "000000");
            ps.setString(2, sdfCompareDb.format(toRegDate) + "235959");
            rs = ps.executeQuery();
         }
         else
         {
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchUserRegDetailsByDateAndCourse");
            ps.setString(1, sdfCompareDb.format(fromRegDate) + "000000");
            ps.setString(2, sdfCompareDb.format(toRegDate) + "235959");
            ps.setString(3, courseId);
            ps.setString(4, courseId);
            rs = ps.executeQuery();
         }
         String firstName = null;
         String lastName = null;
         String userid = null;
         int checkFlag;
         while (rs.next())
         {
            userArr = new String[7];
            userArr[0] = Integer.toString(rs.getInt("seqno"));
            userid = rs.getString("userid");
            userArr[1] = userid;
            firstName = rs.getString("firstname");
            lastName = rs.getString("lastname");
            if (firstName == null || lastName == null)
            {
               userArr[2] = "";
            }
            else
            {
               userArr[2] = firstName + " " + lastName;
            }
            userArr[3] = rs.getString("addedby");
            try
            {
               userArr[6] = sdfPage.format(sdfDb.parse(rs.getString("createtime")));
            }
            catch (Exception e)
            {
               userArr[6] = "";
            }
            checkFlag = rs.getInt("chkflag");
            if (AccountStatusHelper.isRegistered(checkFlag))
            {
               if (AccountStatusHelper.isLock(checkFlag))
               {
                  userArr[4] = "BLOCKED";
               }
               else if (AccountStatusHelper.isValidate(checkFlag))
               {
                  userArr[4] = "VALIDATED";
                  userArr[5] = "NO";
                  String userCourse = rs.getString("usercourse");
                  if (userCourse == null || userCourse.trim().compareTo("") == 0)
                  {
                     String userCourseBackup = rs.getString("usercoursebackup");
                     if (userCourseBackup == null || userCourseBackup.trim().compareTo("") == 0)
                     {

                     }
                     else
                     {
                        userArr[4] = "HADPURCHASED";
                     }
                  }
                  else
                  {
                     userArr[4] = "PURCHASED";
                     userArr[5] = "YES";
                  }
               }
               else
               {
                  userArr[4] = "ACTIVE";
               }
            }
            else
            {
               userArr[4] = "PRE-REG";
            }
            //            userArr[5] = fetchUserCourse(dbConnName, userArr[1], logger);
            users.add(userArr);
         }
         userData.setAttribList(users);
         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
      }
      catch (SQLException e)
      {
         logger.error("DB error while fetching list of users", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "DB error while fetching users");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching list of users", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching users");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing result set while fetching user list", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      return RuleDecisionKey.ON_SUCCESS;
   }

   //   private String getValidationStatus(String userid)
   //   {
   //      String status = "VALIDATED";
   //
   //      Connection connection = DbConnectionManager.getConnectionByName(dbConnName);
   //
   //      ResultSet rs = null;
   //      try
   //      {
   //         PreparedStatement preparedStatement = DbConnectionManager.getPreparedStatement(dbConnName, connection, "CheckInUserCourse");
   //         preparedStatement.setString(1, userid);
   //         rs = preparedStatement.executeQuery();
   //
   //         if (rs.next())
   //         {
   //            if (logger.isDebugEnabled())
   //            {
   //               logger.debug("User " + userid + " has a valid course.");
   //            }
   //            status = "PURCHASED";
   //         }
   //         else
   //         {
   //            preparedStatement = DbConnectionManager.getPreparedStatement(dbConnName, connection, "CheckUserInUserCourseBackup");
   //            preparedStatement.setString(1, userid);
   //            rs = preparedStatement.executeQuery();
   //
   //            if (rs.next())
   //            {
   //               if (logger.isDebugEnabled())
   //               {
   //                  logger.debug("User " + userid + " had a valid course which is now expired.");
   //               }
   //               status = "HADPURCHASED";
   //            }
   //            else
   //            {
   //
   //            }
   //         }
   //      }
   //      catch (SQLException e)
   //      {
   //         //e.printStackTrace();
   //         logger.error("Database Exception while trying to fetch data from usercourse.", e);
   //      }
   //      catch (Exception e)
   //      {
   //         logger.error("Exception while trying to fetch data from usercourse.", e);
   //      }
   //      finally
   //      {
   //         DbConnectionManager.releaseConnection(dbConnName, connection);
   //         try
   //         {
   //            rs.close();
   //         }
   //         catch (Exception e)
   //         {
   //            //e.printStackTrace();
   //            logger.error("Exception occoured while trying to close the result set.", e);
   //         }
   //      }
   //      return status;
   //   }

   //   private String fetchUserCourse(String dbConnName, String userId, Logger logger) throws Exception
   //   {
   //      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
   //      PreparedStatement ps = null;
   //      ResultSet rs = null;
   //      try
   //      {
   //         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchCourseList");
   //         ps.setString(1, userId);
   //         rs = ps.executeQuery();
   //         if (rs.next())
   //         {
   //            return "YES";
   //         }
   //         return "NO";
   //      }
   //      catch (Exception e)
   //      {
   //         throw e;
   //      }
   //      finally
   //      {
   //         if (rs != null)
   //         {
   //            try
   //            {
   //               rs.close();
   //            }
   //            catch (Exception e)
   //            {
   //               logger.error("Error while closing resultset while fetching courses of users", e);
   //            }
   //         }
   //         DbConnectionManager.releaseConnection(dbConnName, conn);
   //      }
   //   }
}
