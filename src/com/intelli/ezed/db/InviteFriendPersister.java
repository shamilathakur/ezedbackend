package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class InviteFriendPersister extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;

   public InviteFriendPersister(String ruleName, String dbconnName)
   {
      super(ruleName);
      this.dbConnName = dbconnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      String userid = (String) flowContext.getFromContext(EzedKeyConstants.USER_ID);
      String inTime = (String) flowContext.getFromContext(EzedKeyConstants.REQUEST_IN_TIME);
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      String friendId = (String) flowContext.getFromContext(EzedKeyConstants.INVITED_FRIEND);
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      PreparedStatement ps = null;

      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertInviteFriend");
         ps.setString(1, userid);
         ps.setString(2, friendId);
         ps.setString(3, inTime);
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in database when " + userid + " invited " + friendId);
         }
         response.setParameters(EzedTransactionCodes.SUCCESS, "Success");
         return RuleDecisionKey.ON_SUCCESS;
      }
      catch (SQLException e)
      {
         logger.error("DB error while inserting invited friend in db", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while processing invite friend request");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while inserting invited friend in db", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while processing invite friend request");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
}
