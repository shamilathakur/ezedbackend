package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.utils.EzedHelper;

public class UserQuizResultUpdater extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public UserQuizResultUpdater(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      String userid = (String) flowContext.getFromContext(EzedKeyConstants.USER_ID);
      String inTime = (String) flowContext.getFromContext(EzedKeyConstants.REQUEST_IN_TIME);
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());

      String quizId = messageData.getParameter("quizid");
      if (quizId == null || quizId.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Quiz id is invalid " + quizId);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid quiz id");
         return RuleDecisionKey.ON_FAILURE;
      }
      quizId = quizId.trim();

      String correctAnsString = messageData.getParameter("correctanscount");
      if (correctAnsString == null || correctAnsString.trim().compareTo("") == 0 || !EzedHelper.isNumeric(correctAnsString.trim()))
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("No of correct ans is invalid " + correctAnsString);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid no of correct ans");
         return RuleDecisionKey.ON_FAILURE;
      }

      int correctAnsNo = Integer.parseInt(correctAnsString);
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdateUserQuizAns");
         ps.setString(1, inTime);
         ps.setInt(2, correctAnsNo);
         ps.setString(3, userid);
         ps.setString(4, quizId);
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows updated in user quiz trail for user id " + userid + " and quiz id " + quizId);
         }
         response.setResponseObject(null);
         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
      }
      catch (SQLException e)
      {
         logger.error("Error while updating questions in user quiz trail against the quiz id " + quizId, e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while updating quiz result");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while updating questions in user quiz trail against the quiz id " + quizId, e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while updating quiz result");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
      return RuleDecisionKey.ON_SUCCESS;
   }
}
