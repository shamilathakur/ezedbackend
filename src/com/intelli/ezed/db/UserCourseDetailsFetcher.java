package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.ManageUserCourseFromBackendBean;
import com.intelli.ezed.data.Response;

public class UserCourseDetailsFetcher extends SingletonRule {

	private String dbConnName;
	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

	public UserCourseDetailsFetcher(String ruleName, String dbConnName) {
		super(ruleName);
		this.dbConnName = dbConnName;
	}

	@Override
	public RuleDecisionKey executeRule(FlowContext flowContext) {
		Response response = (Response) flowContext
				.getFromContext(EzedKeyConstants.EZED_RESPONSE);
		ManageUserCourseFromBackendBean data = (ManageUserCourseFromBackendBean) flowContext
				.getFromContext(EzedKeyConstants.ADD_COURSE_TO_USER_FROM_ADMIN);
		Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = DbConnectionManager.getPreparedStatement(dbConnName, conn,
					"FetchUserCourseDetails");
			ps.setString(1, data.getUserId());
			ps.setString(2, data.getCourseId());
			ps.setString(3, data.getCourseType());
			rs = ps.executeQuery();
			if (rs.next()) {
				data.setStartDate(rs.getString("startdate"));
				data.setEndDate(rs.getString("enddate"));
				data.setGoalsachieved(Integer.parseInt(rs
						.getString("goalsachieved")));
				data.setGoalsfailed(Integer.parseInt(rs
						.getString("goalsfailed")));
				data.setPoints(Integer.parseInt(rs.getString("points")));
				data.setPercentcomplete(Integer.parseInt(rs
						.getString("percentcomplete")));
			}
		} catch (SQLException e) {
			System.out.println("SQLException occurred in class :: "
					+ this.getClass().getName() + " :: " + e);
			if (logger.isDebugEnabled()) {
				logger.error("SQLException occurred in class :: "
						+ this.getClass().getName() + " :: " + e);
			}
			response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED,
					"Database Exception occured !");
			return RuleDecisionKey.ON_FAILURE;
		} catch (Exception e) {
			System.out.println("Exception occurred in class :: "
					+ this.getClass().getName() + " :: " + e);
			if (logger.isDebugEnabled()) {
				logger.error("Exception occurred in class :: "
						+ this.getClass().getName() + " :: " + e);
			}
			response.setParameters(EzedTransactionCodes.INTERNAL_ERROR,
					"Internal Exception occured !");
			return RuleDecisionKey.ON_FAILURE;
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					logger.error(
							"Error while closing result set while fetching Partners list",
							e);
				}
			}
			DbConnectionManager.releaseConnection(dbConnName, conn);
		}

		flowContext.putIntoContext(
				EzedKeyConstants.ADD_COURSE_TO_USER_FROM_ADMIN, data);
		return RuleDecisionKey.ON_SUCCESS;
	}
}
