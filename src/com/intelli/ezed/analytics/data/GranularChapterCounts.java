package com.intelli.ezed.analytics.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.slf4j.Logger;

import com.intelli.ezed.bulkupload.data.ContentDetails;
import com.intelli.ezed.data.ResponseObject;

public class GranularChapterCounts extends ResponseObject
{
   private int videoCount = 0;
   private int audioCount = 0;
   private int noteCount = 0;
   private int quizCount = 0;
   private int testCount = 0;
   private int youtubeCount = 0;
   private String chapterName;
   private String chapterId;

   public void fetchCounts(String dbConnName, Logger logger) throws SQLException, Exception
   {
      Connection conn = null;
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchChapterNameContentCounts");
         ps.setString(1, getChapterId());
         rs = ps.executeQuery();
         int contentType = 0;
         while (rs.next())
         {
            setChapterName(rs.getString("chaptername"));
            contentType = rs.getInt("contenttype");
            if (contentType == ContentDetails.CONTENT_VIDEO)
            {
               setVideoCount(rs.getInt("count"));
            }
            else if (contentType == ContentDetails.CONTENT_AUDIO)
            {
               setAudioCount(rs.getInt("count"));
            }
            else if (contentType == ContentDetails.CONTENT_NOTE || contentType == ContentDetails.CONTENT_IMAGE || contentType == ContentDetails.CONTENT_IMAGE_1 || contentType == ContentDetails.CONTENT_PDF)
            {
               setNoteCount(rs.getInt("count"));
            }
            else if (contentType == ContentDetails.CONTENT_QUIZ)
            {
               setQuizCount(rs.getInt("count"));
            }
            else if (contentType == ContentDetails.CONTENT_TEST || contentType == ContentDetails.CONTENT_TEST_1)
            {
               setTestCount(rs.getInt("count"));
            }
            else if (contentType == ContentDetails.CONTENT_YOUTUBE)
            {
               setYoutubeCount(rs.getInt("count"));
            }
         }
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
               logger.error("Error while closing resultset while fetching chapter counts", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   @Override
   public void prepareResponse() throws Exception
   {
      int[] dataValues = new int[6];
      dataValues[0] = getVideoCount();
      dataValues[1] = getAudioCount();
      dataValues[2] = getNoteCount();
      dataValues[3] = getQuizCount();
      dataValues[4] = getTestCount();
      dataValues[5] = getYoutubeCount();
      this.put("name", getChapterName());
      this.put("data", dataValues);
   }

   public int getVideoCount()
   {
      return videoCount;
   }

   public void setVideoCount(int videoCount)
   {
      this.videoCount = videoCount;
   }

   public int getAudioCount()
   {
      return audioCount;
   }

   public void setAudioCount(int audioCount)
   {
      this.audioCount = audioCount;
   }

   public int getNoteCount()
   {
      return noteCount;
   }

   public void setNoteCount(int noteCount)
   {
      this.noteCount = noteCount;
   }

   public int getQuizCount()
   {
      return quizCount;
   }

   public void setQuizCount(int quizCount)
   {
      this.quizCount = quizCount;
   }

   public int getTestCount()
   {
      return testCount;
   }

   public void setTestCount(int testCount)
   {
      this.testCount = testCount;
   }

   public int getYoutubeCount()
   {
      return youtubeCount;
   }

   public void setYoutubeCount(int youtubeCount)
   {
      this.youtubeCount = youtubeCount;
   }

   public String getChapterName()
   {
      return chapterName;
   }

   public void setChapterName(String chapterName)
   {
      this.chapterName = chapterName;
   }

   public String getChapterId()
   {
      return chapterId;
   }

   public void setChapterId(String chapterId)
   {
      this.chapterId = chapterId;
   }

}
