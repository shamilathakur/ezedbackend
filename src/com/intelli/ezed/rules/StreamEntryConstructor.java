package com.intelli.ezed.rules;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.ActionData;
import com.intelli.ezed.data.EventData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.UserProfile;
import com.intelli.ezed.utils.EzedHelper;

public class StreamEntryConstructor extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.STREAM_LOG);

   public StreamEntryConstructor(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      EventData eventData = (EventData) flowContext.getFromContext(EzedKeyConstants.EVENT_OBJECT);
      ActionData actionData = (ActionData) flowContext.getFromContext(EzedKeyConstants.ACTION_DATA);
      UserProfile userProfile = (UserProfile) flowContext.getFromContext(EzedKeyConstants.USER_DETAILS);

      String streamString = actionData.getActionLog();
      if (streamString == null)
      {
         streamString = "";
      }
      //      if (userProfile.getFirstName() == null || userProfile.getLastName() == null)
      //      {
      streamString = streamString.replaceAll("<username>", userProfile.getName());
      //      }
      //      else
      //      {
      //         streamString = streamString.replaceAll("<username>", userProfile.getFirstName() + " " + userProfile.getLastName());
      //      }

      if (streamString.contains("<contentname>"))
      {
         String contentName = EzedHelper.fetchContentNameForContentId(dbConnName, eventData.getContentId(), logger);
         if (contentName != null)
         {
            streamString = streamString.replaceAll("<contentname>", contentName);
         }
      }

      if (streamString.contains("<chaptername>"))
      {
         String chapterName = EzedHelper.fetchChapterNameForChapterId(dbConnName, eventData.getChapterId(), logger);
         if (chapterName != null)
         {
            streamString = streamString.replaceAll("<chaptername>", chapterName);
         }
      }

      if (streamString.contains("<coursename>"))
      {
         String courseName = EzedHelper.fetchCourseNameForCourseId(dbConnName, eventData.getCourseId(), logger);
         if (courseName != null)
         {
            streamString = streamString.replaceAll("<coursename>", courseName);
         }
      }

      flowContext.putIntoContext(EzedKeyConstants.STREAM_ENTRY_STRING, streamString);
      return RuleDecisionKey.ON_SUCCESS;
   }

}
