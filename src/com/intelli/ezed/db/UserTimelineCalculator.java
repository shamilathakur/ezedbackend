package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.bulkupload.data.ContentDetails;
import com.intelli.ezed.data.CartData;
import com.intelli.ezed.data.CartDetails;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.GoalData;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.UserProfile;
import com.intelli.ezed.sheduler.PurchasedCartUpdater;
import com.intelli.ezed.utils.PaymentLockManager;

//Exception in thread "Flow_Controller_3288" java.lang.NullPointerException
//at com.intelli.ezed.db.UserTimelineCalculator.executeRule(UserTimelineCalculator.java:63)
//at org.sabre.rulengine.rulechain.RuleLinker.executeRule(src:139)
//at org.sabre.workflow.FlowController.processRule(FlowController.java:192)
//at org.sabre.workflow.FlowController$RuleExecutor.run(FlowController.java:268)
//at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1145)
//at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:615)
//at java.lang.Thread.run(Thread.java:722)


public class UserTimelineCalculator extends SingletonRule
{
   private String dbConnName;
   private float avgHours;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private Logger cartLogger = LoggerFactory.getLogger(EzedLoggerName.CART_LOG);

   public UserTimelineCalculator(String ruleName, String dbConnName, String avgHours)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
      this.avgHours = Float.parseFloat(avgHours);
   }

   //select hours,l.contentid,enddate from learningpath l,coursepath c, contentlist cl, courselist cour where l.chapterid in (select chapterid from coursepath where courseid ='1') and l.chapterid=c.chapterid and c.courseid='1' and l.contentid=cl.contentid and cour.courseid=c.courseid order by c.sequenceno,l.sequenceno;
   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      UserProfile userProfile = (UserProfile) flowContext.getFromContext(EzedKeyConstants.USER_DETAILS);
      String userId = userProfile.getUserid();
      CartDetails cartObject = (CartDetails) flowContext.getFromContext(EzedKeyConstants.CART_DETAILS);
      CartData[] cartData = cartObject.getCartDatas();
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      String cartDetails = (String) flowContext.getFromContext(EzedKeyConstants.CART_STRING);
      response.setParameters(EzedTransactionCodes.SUCCESS, "");
      StringBuffer sb = new StringBuffer();

      Connection conn = null;
      PreparedStatement ps = null;
      ResultSet rs = null;
      ArrayList<GoalData> goalDatas = null;
      GoalData singleGoalData = null;
      SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
      for (int i = 0; i < cartData.length; i++)
      {
         //         //First fetching course end date of each course to be added in cart
         //no need to fetch end date. It is already persisted in db
         //         conn = DbConnectionManager.getConnectionByName(dbConnName);
         //         try
         //         {
         //            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchCourseDetail");
         //            ps.setString(1, cartData[i].getCourseId());
         //            rs = ps.executeQuery();
         //            if (rs.next())
         //            {
         //               Calendar calendar = Calendar.getInstance();
         //               String commaSeparatedEndMonths = rs.getString(5);
         //               Date courseEndDate = EndDateCalculator.findEndDate(commaSeparatedEndMonths, cartData[i].getStartDate());
         //               calendar.setTime(courseEndDate);
         //               calendar.add(Calendar.DATE, -dayBufferBeforeEnd);
         //               cartData[i].setEndDate(calendar.getTime());
         //            }
         //            else
         //            {
         //               if (logger.isDebugEnabled())
         //               {
         //                  logger.debug("Cannot fetch end date of course " + cartData[i].getCourseId());
         //               }
         //               sb.append("|").append(cartData[i].getCourseId()).append(",").append(EzedTransactionCodes.DB_EXCEPTION_OCCURRED);
         //               PurchasedCartUpdater.logFailedCartProcessing(cartLogger, userId, cartDetails, response.getResponseCode(), response.getResponseMessage());
         //               return RuleDecisionKey.ON_FAILURE;
         //            }
         //         }
         //         catch (SQLException e)
         //         {
         //            if (logger.isDebugEnabled())
         //            {
         //               logger.debug("DB error while fetching end date of course " + cartData[i].getCourseId(), e);
         //            }
         //            sb.append("|").append(cartData[i].getCourseId()).append(",").append(EzedTransactionCodes.DB_EXCEPTION_OCCURRED);
         //            PurchasedCartUpdater.logFailedCartProcessing(cartLogger, userId, cartDetails, response.getResponseCode(), response.getResponseMessage());
         //            return RuleDecisionKey.ON_FAILURE;
         //         }
         //         catch (Exception e)
         //         {
         //            if (logger.isDebugEnabled())
         //            {
         //               logger.debug("Error while fetching end date of course " + cartData[i].getCourseId(), e);
         //            }
         //            sb.append("|").append(cartData[i].getCourseId()).append(",").append(EzedTransactionCodes.INTERNAL_ERROR);
         //            PurchasedCartUpdater.logFailedCartProcessing(cartLogger, userId, cartDetails, response.getResponseCode(), response.getResponseMessage());
         //            return RuleDecisionKey.ON_FAILURE;
         //         }
         //         finally
         //         {
         //            if (rs != null)
         //            {
         //               try
         //               {
         //                  rs.close();
         //               }
         //               catch (SQLException e)
         //               {
         //                  logger.error("Error while closing result set while fetching end date of course id " + cartData[i].getCourseId(), e);
         //               }
         //            }
         //            DbConnectionManager.releaseConnection(dbConnName, conn);
         //         }

         //Check if this purchase matches any previous purchases
         //For ex : if test is bought for a course for whom content has already been bought
         //or vice versa
         if (!userProfile.isFreshPurchase(cartData[i].getCourseId(), cartData[i].getCourseType(), cartData[i].getEndDate()))
         {
            cartData[i].setUpdateUserCourse(true);
            if (userProfile.isCourseAlreadyRunning(cartData[i].getCourseId(), cartData[i].getEndDate()))
            {
               cartData[i].setUpdateStartDate(false);
               try
               {
                  if (cartData[i].getCourseType().compareTo(CartData.CONTENT_ONLY) == 0)
                  {
                     cartData[i].setFetchContentOnly(true);
                     cartData[i].setStartDate(userProfile.getMaxGoalDate(dbConnName, logger, cartData[i].getCourseId(), cartData[i].getEndDate()));
                  }
                  else
                  {
                     cartData[i].setFetchTestOnly(true);
                     cartData[i].setStartDate(userProfile.getMaxGoalDate(dbConnName, logger, cartData[i].getCourseId(), cartData[i].getEndDate()));
                  }
               }
               catch (Exception e)
               {
                  if (logger.isDebugEnabled())
                  {
                     logger.debug("Error while calculating start date of course " + cartData[i].getCourseId(), e);
                  }
                  sb.append("|").append(cartData[i].getCourseId()).append(",").append(EzedTransactionCodes.INTERNAL_ERROR);
                  PurchasedCartUpdater.logFailedCartProcessing(cartLogger, userId, cartDetails, response.getResponseCode(), response.getResponseMessage());
                  return RuleDecisionKey.ON_FAILURE;
               }
            }
            else
            {
               cartData[i].setUpdateStartDate(true);
               cartData[i].setDeleteExistingGoals(true);
               cartData[i].setFetchAllCourse(true);
               cartData[i].setStartDate(userProfile.getNewStartDate(cartData[i].getCourseId(), cartData[i].getEndDate(), cartData[i].getStartDate()));
            }
            cartData[i].setCourseType(CartData.ALL_COURSE);
         }
         else
         {
            if (cartData[i].getCourseType().compareTo(CartData.CONTENT_ONLY) == 0)
            {
               cartData[i].setFetchContentOnly(true);
            }
            else if (cartData[i].getCourseType().compareTo(CartData.ALL_COURSE) == 0)
            {
               cartData[i].setFetchAllCourse(true);
            }
            else
            {
               cartData[i].setFetchTestOnly(true);
            }
         }

         try
         {
            conn = DbConnectionManager.getConnectionByName(dbConnName);
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "SelectContentForCourse");
            ps.setString(1, cartData[i].getCourseId());
            ps.setString(2, cartData[i].getCourseId());
            rs = ps.executeQuery();
            goalDatas = new ArrayList<GoalData>();
            //            boolean onceDone = false;
            //            Calendar calendar = Calendar.getInstance();
            int contentType = 0;
            if (cartData[i].isFetchAllCourse())
            {
               while (rs.next())
               {
                  singleGoalData = new GoalData();
                  singleGoalData.setCourseId(rs.getString("courseid"));
                  singleGoalData.setChapterId(rs.getString("chapterid"));
                  singleGoalData.setContentId(rs.getString("contentid"));
                  singleGoalData.setHours(rs.getInt("hours"));
                  if (singleGoalData.getHours() == 0)
                  {
                     singleGoalData.setHours(1);
                  }
                  singleGoalData.setContentShowFlag(rs.getString("contentshowflag"));
                  //               if (!onceDone)
                  //               {
                  //                  String commaSeparatedEndMonths = rs.getString(5);
                  //                  Date courseEndDate = EndDateCalculator.findEndDate(commaSeparatedEndMonths, cartData[i].getStartDate());
                  //                  calendar.setTime(courseEndDate);
                  //                  calendar.add(Calendar.DATE, -dayBufferBeforeEnd);
                  //                  cartData[i].setEndDate(calendar.getTime());
                  //                  onceDone = true;
                  //               }
                  goalDatas.add(singleGoalData);
               }
            }
            else if (cartData[i].isFetchContentOnly())
            {
               while (rs.next())
               {
                  contentType = rs.getInt("contenttype");
                  if (ContentDetails.isContentOnlyCourse(contentType))
                  {
                     singleGoalData = new GoalData();
                     singleGoalData.setCourseId(rs.getString("courseid"));
                     singleGoalData.setChapterId(rs.getString("chapterid"));
                     singleGoalData.setContentId(rs.getString("contentid"));
                     singleGoalData.setHours(rs.getInt("hours"));
                     if (singleGoalData.getHours() == 0)
                     {
                        singleGoalData.setHours(1);
                     }
                     singleGoalData.setContentShowFlag(rs.getString("contentshowflag"));
                     //               if (!onceDone)
                     //               {
                     //                  String commaSeparatedEndMonths = rs.getString(5);
                     //                  Date courseEndDate = EndDateCalculator.findEndDate(commaSeparatedEndMonths, cartData[i].getStartDate());
                     //                  calendar.setTime(courseEndDate);
                     //                  calendar.add(Calendar.DATE, -dayBufferBeforeEnd);
                     //                  cartData[i].setEndDate(calendar.getTime());
                     //                  onceDone = true;
                     //               }
                     goalDatas.add(singleGoalData);
                  }
               }
            }
            else
            {
               while (rs.next())
               {
                  contentType = rs.getInt("contenttype");
                  if (ContentDetails.isTestOnlyCourse(contentType))
                  {
                     singleGoalData = new GoalData();
                     singleGoalData.setCourseId(rs.getString("courseid"));
                     singleGoalData.setChapterId(rs.getString("chapterid"));
                     singleGoalData.setContentId(rs.getString("contentid"));
                     singleGoalData.setHours(rs.getInt("hours"));
                     if (singleGoalData.getHours() == 0)
                     {
                        singleGoalData.setHours(1);
                     }
                     singleGoalData.setContentShowFlag(rs.getString("contentshowflag"));
                     //               if (!onceDone)
                     //               {
                     //                  String commaSeparatedEndMonths = rs.getString(5);
                     //                  Date courseEndDate = EndDateCalculator.findEndDate(commaSeparatedEndMonths, cartData[i].getStartDate());
                     //                  calendar.setTime(courseEndDate);
                     //                  calendar.add(Calendar.DATE, -dayBufferBeforeEnd);
                     //                  cartData[i].setEndDate(calendar.getTime());
                     //                  onceDone = true;
                     //               }
                     goalDatas.add(singleGoalData);
                  }
               }
            }
         }
         catch (SQLException e)
         {
            logger.error("DB Error while fetching all contents against course id for managing goals", e);
            response.setResponseCode(EzedTransactionCodes.DB_EXCEPTION_OCCURRED);
            sb.append("|").append(cartData[i].getCourseId()).append(",").append(EzedTransactionCodes.DB_EXCEPTION_OCCURRED);
            PurchasedCartUpdater.logFailedCartProcessing(cartLogger, userId, cartDetails, response.getResponseCode(), response.getResponseMessage());
            PaymentLockManager.removeFromList(cartObject.getAppId() + cartObject.getPaymentId());
            return RuleDecisionKey.ON_FAILURE;
         }
         catch (Exception e)
         {
            logger.error("Error while fetching all contents against course id for managing goals", e);
            response.setResponseCode(EzedTransactionCodes.INTERNAL_ERROR);
            sb.append("|").append(cartData[i].getCourseId()).append(",").append(EzedTransactionCodes.INTERNAL_ERROR);
            PurchasedCartUpdater.logFailedCartProcessing(cartLogger, userId, cartDetails, response.getResponseCode(), response.getResponseMessage());
            PaymentLockManager.removeFromList(cartObject.getAppId() + cartObject.getPaymentId());
            return RuleDecisionKey.ON_FAILURE;
         }
         finally
         {
            if (rs != null)
            {
               try
               {
                  rs.close();
               }
               catch (SQLException e)
               {
                  logger.error("Error while closing result set while fetching all contents for a course for managing goals", e);
               }
            }
            DbConnectionManager.releaseConnection(dbConnName, conn);
         }

         if (cartData[i].isFetchAllCourse() || cartData[i].isFetchTestOnly())
         {
            conn = DbConnectionManager.getConnectionByName(dbConnName);
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchTestsForCourse");
            try
            {
               ps.setString(1, cartData[i].getCourseId());
               rs = ps.executeQuery();
               String testTime = null;
               int hours = 0;
               int mins = 0;
               while (rs.next())
               {
                  singleGoalData = new GoalData();
                  singleGoalData.setCourseId(rs.getString("courseid"));
                  singleGoalData.setChapterId(null);
                  singleGoalData.setContentId(rs.getString("testid"));
                  testTime = rs.getString("testtime");
                  hours = Integer.parseInt(testTime.substring(0, 2));
                  mins = Integer.parseInt(testTime.substring(2, 4));
                  mins = mins + (hours * 60);
                  singleGoalData.setHours(mins);
                  if (singleGoalData.getHours() == 0)
                  {
                     singleGoalData.setHours(1);
                  }
                  singleGoalData.setContentShowFlag("1");
                  goalDatas.add(singleGoalData);
               }
               cartData[i].setGoalDatas(goalDatas);
               cartData[i].calculateTimeLine(avgHours);
            }
            catch (SQLException e)
            {
               logger.error("DB Error while fetching all contents against course id for managing goals", e);
               response.setResponseCode(EzedTransactionCodes.DB_EXCEPTION_OCCURRED);
               sb.append("|").append(cartData[i].getCourseId()).append(",").append(EzedTransactionCodes.DB_EXCEPTION_OCCURRED);
               PurchasedCartUpdater.logFailedCartProcessing(cartLogger, userId, cartDetails, response.getResponseCode(), response.getResponseMessage());
               PaymentLockManager.removeFromList(cartObject.getAppId() + cartObject.getPaymentId());
               return RuleDecisionKey.ON_FAILURE;
            }
            catch (Exception e)
            {
               logger.error("Error while fetching all contents against course id for managing goals", e);
               response.setResponseCode(EzedTransactionCodes.INTERNAL_ERROR);
               sb.append("|").append(cartData[i].getCourseId()).append(",").append(EzedTransactionCodes.INTERNAL_ERROR);
               PurchasedCartUpdater.logFailedCartProcessing(cartLogger, userId, cartDetails, response.getResponseCode(), response.getResponseMessage());
               PaymentLockManager.removeFromList(cartObject.getAppId() + cartObject.getPaymentId());
               return RuleDecisionKey.ON_FAILURE;
            }
         }
         else
         {
            cartData[i].setGoalDatas(goalDatas);
            cartData[i].calculateTimeLine(avgHours);
         }
      }
      PurchasedCartUpdater.logFailedCartProcessing(cartLogger, userId, cartDetails, response.getResponseCode(), response.getResponseMessage());
      return RuleDecisionKey.ON_SUCCESS;
   }
   public static void main(String[] args)
   {
      SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
      Calendar calendar = Calendar.getInstance();
      Date courseEndDate = null;
      try
      {
         courseEndDate = sdf.parse("20130319");
      }
      catch (ParseException e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
      calendar.setTime(courseEndDate);
      calendar.add(Calendar.DATE, -30);
      System.out.println(calendar.getTime());
   }
}
