package com.intelli.ezed.dropdowns;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.dropdowns.data.DropDownListData;
import com.intelli.ezed.dropdowns.data.SingleDropDownData;

public class GroupTestListForDropdownFetcher extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;

   public GroupTestListForDropdownFetcher(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      String groupId = messageData.getParameter("groupid");
      if (groupId == null || groupId.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Group id is invalid " + groupId);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid group id");
         return RuleDecisionKey.ON_FAILURE;
      }

      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      DropDownListData dropDownListData = new DropDownListData();
      dropDownListData.setName("testIdList");
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchGroupTestList");
         ps.setString(1, groupId);
         rs = ps.executeQuery();
         SingleDropDownData data = null;
         while (rs.next())
         {
            data = new SingleDropDownData();
            data.setParam(rs.getString("testid"));
            data.setValue(rs.getString("testname"));
            dropDownListData.getDropDownDatas().add(data);
         }
         response.setStatusCodeSend(false);
         response.setResponseObject(dropDownListData);
         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
         return RuleDecisionKey.ON_SUCCESS;
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching list of group admin drop downs", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while populating group admin list drop down");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching group admin for drop downs", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while populating group admin list drop down");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing result set while populating drop down for group admins");
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
}
