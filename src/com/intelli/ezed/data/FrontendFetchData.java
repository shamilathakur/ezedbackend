package com.intelli.ezed.data;

import java.util.ArrayList;

public class FrontendFetchData extends ResponseObject
{
   private ArrayList<String[]> attribList = new ArrayList<String[]>();
   private String attribName;

   public String getAttribName()
   {
      return attribName;
   }

   public void setAttribName(String attribName)
   {
      this.attribName = attribName;
   }

   public ArrayList<String[]> getAttribList()
   {
      return attribList;
   }

   public void setAttribList(ArrayList<String[]> cities)
   {
      this.attribList = cities;
   }

   @Override
   public void prepareResponse() throws Exception
   {
      String[][] attribArr = new String[getAttribList().size()][];
      for (int i = 0; i < getAttribList().size(); i++)
      {
         attribArr[i] = getAttribList().get(i);
      }
      this.put(getAttribName(), attribArr);
   }

}
