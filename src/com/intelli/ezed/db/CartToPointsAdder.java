package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.bulkupload.data.ChapterDetails;
import com.intelli.ezed.data.CartData;
import com.intelli.ezed.data.CartDetails;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.GoalData;
import com.intelli.ezed.data.UserProfile;

public class CartToPointsAdder extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private Logger pointsLogger = LoggerFactory.getLogger(EzedLoggerName.POINTS_ERROR_LOG);
   private String actionId;

   public CartToPointsAdder(String ruleName, String dbConnName, String actionId)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
      this.actionId = actionId;
   }

   //insert into userpoints(userid,actionid,courseid,chapterid,contentid,count) select ?,?,courseid,l.chapterid,contentid,0 from learningpath l,coursepath c where l.chapterid in (select chapterid from coursepath where courseid =?) and l.chapterid=c.chapterid and c.courseid=?;

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      UserProfile userProfile = (UserProfile) flowContext.getFromContext(EzedKeyConstants.USER_DETAILS);
      String userId = userProfile.getUserid();
      CartDetails cartObject = (CartDetails) flowContext.getFromContext(EzedKeyConstants.CART_DETAILS);
      CartData[] cartData = cartObject.getCartDatas();
      String inTime = (String) flowContext.getFromContext(EzedKeyConstants.REQUEST_IN_TIME);
      SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

      GoalData goalDataUse = null;
      for (int i = 0; i < cartData.length; i++)
      {
         //         if (cartData[i].getCourseType() == Integer.toString(CourseDetails.CONTENT_TEST_TYPE))
         //         {
         //            continue;
         //         }
         if (cartData[i].isDeleteExistingGoals())
         {
            try
            {
               userProfile.deleteExistingGoals(dbConnName, logger, cartData[i].getCourseId(), cartData[i].getEndDate());
            }
            catch (Exception e)
            {
               logger.error("Error while deleting from db existing goals for courseid " + cartData[i].getCourseId(), e);
               pointsLogger.info(userId + " " + inTime + " " + " " + cartData[i].getCourseId() + " Error deleting");
            }
         }
         for (int j = 0; j < cartData[i].getGoalDatas().size(); j++)
         {
            if (cartData[i].getGoalDatas().get(j).getContentShowFlag().compareTo(ChapterDetails.HIDE_CONTENT) == 0)
            {
               continue;
            }
            Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
            PreparedStatement ps = null;
            try
            {
               goalDataUse = cartData[i].getGoalDatas().get(j);
               ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertPurchasedCoursePointsTime");
               ps.setString(1, userId);
               ps.setString(2, actionId);
               ps.setString(3, cartData[i].getCourseId());
               ps.setString(4, goalDataUse.getChapterId());
               ps.setString(5, goalDataUse.getContentId());
               ps.setString(6, sdf.format(goalDataUse.getEndDate()));
               ps.setString(7, sdf.format(cartData[i].getEndDate()));
               int rows = ps.executeUpdate();
               if (logger.isDebugEnabled())
               {
                  logger.debug(rows + " rows inserted in db while inserting contents for points for courseid " + cartData[i].getCourseId() + " and contentid " + goalDataUse.getContentId());
               }
               if (rows == 0)
               {
                  pointsLogger.info(userId + " " + inTime + " " + actionId + " " + cartData[i].getCourseId());
               }
            }
            catch (Exception e)
            {
               logger.error("Error while inserting in db for points for course id " + cartData[i].getCourseId() + " and content id " + goalDataUse.getContentId(), e);
               pointsLogger.info(userId + " " + inTime + " " + actionId + " " + cartData[i].getCourseId());
            }
            finally
            {
               DbConnectionManager.releaseConnection(dbConnName, conn);
            }
         }
      }
      return RuleDecisionKey.ON_SUCCESS;
   }
}
