package com.intelli.ezed.bulkupload.rules;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;

import org.apache.commons.digester3.Digester;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import com.intelli.ezed.bulkupload.data.ContentDetails;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;

public class ContentDetailsHandler extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.BULK_UPLOAD_LOG);
   private String contentPropertiesFileName;
   private Digester contentDigester = new Digester();
   private static Object lockObject = new Object();
   private String dbConnName;
   private ContentFilenameFilter fileFilter = new ContentFilenameFilter();
   private String flipBookName;

   public ContentDetailsHandler(String ruleName, String dbConnName, String contentPropertiesFileName,
                                String contentDetailsClassFQCN, String flipBookName)
   {
      super(ruleName);
      initDigester(contentDetailsClassFQCN);
      this.dbConnName = dbConnName;
      this.contentPropertiesFileName = contentPropertiesFileName;
      this.flipBookName = flipBookName;
   }

   private void initDigester(String contentDetailsClassFQCN)
   {
      contentDigester.setValidating(false);
      contentDigester.addObjectCreate("contentdetails", contentDetailsClassFQCN);
      contentDigester.addCallMethod("contentdetails/name", "setContentName", 0);
      contentDigester.addCallMethod("contentdetails/desc", "setDesc", 0);
      contentDigester.addCallMethod("contentdetails/type", "setContentTypeString", 0);
      contentDigester.addCallMethod("contentdetails/hours", "setHours", 0, new Class<?>[]{Integer.class});
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      File contentDirFile = (File) flowContext.getFromContext(EzedKeyConstants.BULK_UPLOAD_CONTENTFILE);
      String chapterPropFile = contentDirFile.getAbsolutePath() + "/" + contentPropertiesFileName;
      ContentDetails contentDetails = null;
      BulkUploadInitiator initiator = (BulkUploadInitiator) flowContext.getFromContext(EzedKeyConstants.BULK_UPLOAD_STARTER);

      try
      {
         synchronized (lockObject)
         {
            contentDetails = contentDigester.parse(new File(chapterPropFile));
            contentDetails.setNewContentId();
         }
         contentDetails.setContentLink(getContentLink(contentDetails, contentDirFile));
         contentDetails.persistContent(dbConnName, logger);
         initiator.putContentInMap(contentDirFile.getName(), contentDetails.getContentId());
         flowContext.putIntoContext(EzedKeyConstants.BULK_UPLOAD_CONTENTOBJECT, contentDetails);
      }
      catch (IOException e)
      {
         logger.error("Error while reading content details properties file", e);
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (SAXException e)
      {
         logger.error("Error while reading content details properties file", e);
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while processing content details after reading properly from properties file", e);
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         boolean quizFlag = false;
         initiator.decrementContentCount();
         if (contentDetails != null)
         {
            if (contentDetails.getContentType() == ContentDetails.CONTENT_QUIZ)
            {
               initiator.putQuizInMap(contentDirFile.getAbsolutePath(), contentDetails.getContentId());
               quizFlag = true;
            }
            if (quizFlag)
            {
               return RuleDecisionKey.IF_QUIZ;
            }
         }
      }
      return RuleDecisionKey.ON_SUCCESS;
   }

   private String getContentLink(ContentDetails contentDetails, File contentDirFile)
   {
      if (contentDetails.getContentType() == ContentDetails.CONTENT_QUIZ)
      {
         //quiz. no link required in db
         return null;
      }
      File[] contentFiles = contentDirFile.listFiles(fileFilter);
      //      StringBuffer returnLink = new StringBuffer(bulkContentDirPath).append("/");
      StringBuffer returnLink = new StringBuffer(contentDirFile.getAbsolutePath()).append(File.separator);
      for (File singleContentFile : contentFiles)
      {
         if (!singleContentFile.isDirectory())
         {
            if (contentDetails.getContentType() == ContentDetails.CONTENT_AUDIO || contentDetails.getContentType() == ContentDetails.CONTENT_VIDEO)
            {
               returnLink.append(singleContentFile.getName());
               return returnLink.toString();
            }
            else
            {
               returnLink.append(singleContentFile.getName());
               return returnLink.toString();
            }
         }
         else
         {
            returnLink.append(singleContentFile.getName()).append(File.separator).append(flipBookName);
            return returnLink.toString();
         }
      }
      return null;
   }

   private class ContentFilenameFilter implements FilenameFilter
   {
      public boolean accept(File parentDirectory, String filename)
      {
         if (filename != null)
         {
            filename = filename.toLowerCase();
         }
         if (filename != null && (!filename.contains(contentPropertiesFileName)))
         {
            return true;
         }
         else
         {
            return false;
         }
      }
   }
}
