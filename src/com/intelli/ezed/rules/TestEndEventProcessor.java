package com.intelli.ezed.rules;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EventData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.TestDetails;
import com.intelli.ezed.data.UserProfile;

public class TestEndEventProcessor extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String actionId;

   public TestEndEventProcessor(String ruleName, String actionId)
   {
      super(ruleName);
      this.actionId = actionId;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      UserProfile userProfile = (UserProfile) flowContext.getFromContext(EzedKeyConstants.USER_DETAILS);
      TestDetails testDetails = (TestDetails) flowContext.getFromContext(TestDetails.TEST_DETAILS);

      EventData eventData = new EventData();
      eventData.setActionId(actionId);
      eventData.setCourseId(testDetails.getCourseId());
      eventData.setChapterId(testDetails.getChapterId());
      eventData.setContentId(testDetails.getTestid());

      try
      {
         UserEventProcessor.processUserStream(eventData, userProfile, new Response());
      }
      catch (Exception e)
      {
         logger.error("Error while trying to pass event data for test end event to increase view count for test in a course");
      }
      return RuleDecisionKey.ON_SUCCESS;
   }

}
