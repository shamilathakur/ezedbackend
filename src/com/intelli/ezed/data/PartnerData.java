package com.intelli.ezed.data;

public class PartnerData extends ResponseObject {
	private int srno;
	private String partnerId; // auto generate id
	private String contactPerson;
	private String partnerName;
	private String address;
	private String state;
	private String city;
	private String contactNumber;
	private String dateOfJoining;// todays date
	private int status; // by default active i.e 1
	private String lastmodified;
	private String country;
	private String emailAddress;

	public String getAddedBy() {
		return addedBy;
	}

	public void setAddedBy(String addedBy) {
		this.addedBy = addedBy;
	}

	private String addedBy;

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getEmailaddress() {
		return emailAddress;
	}

	public void setEmailaddress(String emailaddress) {
		this.emailAddress = emailaddress;
	}

	public int getSrno() {
		return srno;
	}

	public void setSrno(int srno) {
		this.srno = srno;
	}

	public String getLastmodified() {
		return lastmodified;
	}

	public void setLastmodified(String lastmodified) {
		this.lastmodified = lastmodified;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getDateOfJoining() {
		return dateOfJoining;
	}

	public void setDateOfJoining(String dateOfJoining) {
		this.dateOfJoining = dateOfJoining;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Override
	public void prepareResponse() throws Exception {

		this.put("srno", getSrno());
		this.put("partnerId", getPartnerId());
		this.put("contactPerson", getContactPerson());
		this.put("partnerName", getPartnerName());
		this.put("address", getAddress());
		this.put("state", getState());
		this.put("city", getCity());
		this.put("country", getCountry());
		this.put("contactNumber", getContactNumber());
		this.put("dateOfJoining", getDateOfJoining());
		this.put("status", getStatus());
		this.put("emailaddress", getEmailaddress());

	}

}
