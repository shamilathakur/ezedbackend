package com.intelli.ezed.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.sabre.db.DbConnectionManager;
import org.slf4j.Logger;

public class CourseIntroData extends ResponseObject
{
   private String courseName;
   private String courseId;
   private String shortDesc;
   private String introVidId;
   private int longDescType;
   private String longDescString;
   private String longDescContentId;
   private String introVidLink;
   private String longDescLink;
   private int chapterCount;
   private int videoCount;
   private int testCount;
   private int questionCount;
   private String endDate;
   private String introImage;
   private ArrayList<String> chapterList = new ArrayList<String>();
   private int introVidType;

   public int getChapterCount()
   {
      return chapterCount;
   }

   public void setChapterCount(int chapterCount)
   {
      this.chapterCount = chapterCount;
   }

   public int getVideoCount()
   {
      return videoCount;
   }

   public void setVideoCount(int videoCount)
   {
      this.videoCount = videoCount;
   }

   public int getTestCount()
   {
      return testCount;
   }

   public void setTestCount(int testCount)
   {
      this.testCount = testCount;
   }

   public int getQuestionCount()
   {
      return questionCount;
   }

   public void setQuestionCount(int questionCount)
   {
      this.questionCount = questionCount;
   }

   public String getEndDate()
   {
      return endDate;
   }

   public void setEndDate(String endDate)
   {
      this.endDate = endDate;
   }

   public String getCourseName()
   {
      return courseName;
   }
   public void setCourseName(String courseName)
   {
      this.courseName = courseName;
   }
   public String getCourseId()
   {
      return courseId;
   }
   public void setCourseId(String courseId)
   {
      this.courseId = courseId;
   }
   public String getShortDesc()
   {
      return shortDesc;
   }
   public void setShortDesc(String shortDesc)
   {
      this.shortDesc = shortDesc;
   }
   public String getIntroVidId()
   {
      return introVidId;
   }
   public void setIntroVidId(String introVidId)
   {
      this.introVidId = introVidId;
   }
   public int getLongDescType()
   {
      return longDescType;
   }
   public void setLongDescType(int longDescType)
   {
      this.longDescType = longDescType;
   }
   public String getLongDescString()
   {
      return longDescString;
   }
   public void setLongDescString(String longDescString)
   {
      this.longDescString = longDescString;
   }
   public String getLongDescContentId()
   {
      return longDescContentId;
   }
   public void setLongDescContentId(String longDescContentId)
   {
      this.longDescContentId = longDescContentId;
   }
   public String getIntroVidLink()
   {
      return introVidLink;
   }
   public void setIntroVidLink(String introVidLink)
   {
      this.introVidLink = introVidLink;
   }
   public String getLongDescLink()
   {
      return longDescLink;
   }
   public void setLongDescLink(String longDescLink)
   {
      this.longDescLink = longDescLink;
   }

   public void fetchIntroVidDetails(String dbConnName, Logger logger) throws SQLException, Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "GetContentNameForId");
         ps.setString(1, getIntroVidId());
         rs = ps.executeQuery();
         if (rs.next())
         {
            setIntroVidLink(rs.getString("link"));
            setIntroVidType(rs.getInt("contenttype"));
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("No intro video details found in db for content id " + getIntroVidId());
            }
            setIntroVidLink(null);
         }
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
               logger.error("Error while closing result set while fetching intro vid details", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   public void fetchLongDescDetails(String dbConnName, Logger logger) throws SQLException, Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "GetContentNameForId");
         ps.setString(1, getLongDescContentId());
         rs = ps.executeQuery();
         if (rs.next())
         {
            setLongDescLink(rs.getString("link"));
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("No long desc details found in db for content id " + getIntroVidId());
            }
            setLongDescLink(null);
         }
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
               logger.error("Error while closing result set while fetching intro vid details", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
   @Override
   public void prepareResponse() throws Exception
   {
      this.put("courseid", getCourseId());
      this.put("coursename", getCourseName());
      this.put("shortdesc", getShortDesc());
      this.put("longdesctype", getLongDescType());
      this.put("longdescstring", getLongDescString());
      this.put("longdesccontent", getLongDescLink());
      this.put("introvid", getIntroVidLink());
      this.put("courseid", getCourseId());
      this.put("chaptercount", getChapterCount());
      this.put("videocount", getVideoCount());
      this.put("testcount", getTestCount());
      this.put("questioncount", getQuestionCount());
      this.put("enddate", getEndDate());
      this.put("introimage", getIntroImage());
      if (getChapterList().size() != 0)
      {
         String[] chapterList = new String[getChapterList().size()];
         for (int i = 0; i < chapterList.length; i++)
         {
            chapterList[i] = getChapterList().get(i);
         }
         this.put("chaptertitlelist", chapterList);
      }
   }

   public ArrayList<String> getChapterList()
   {
      return chapterList;
   }

   public void setChapterList(ArrayList<String> chapterList)
   {
      this.chapterList = chapterList;
   }

   public String getIntroImage()
   {
      return introImage;
   }

   public void setIntroImage(String introImage)
   {
      this.introImage = introImage;
   }

   public int getIntroVidType()
   {
      return introVidType;
   }

   public void setIntroVidType(int introVidType)
   {
      this.introVidType = introVidType;
   }
}
