package com.intelli.ezed.install;

import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

//import org.apache.tools.ant.Project;

public class MyClassLoader extends ClassLoader
{

   public MyClassLoader()
   {
   }

   public MyClassLoader(ClassLoader parent)
   {
      super(parent);
   }

   @Override
   public Class<?> loadClass(String className) throws ClassNotFoundException
   {
      try
      {
         Class<?> clazz = super.loadClass(className);
         return clazz;
      }
      catch (ClassNotFoundException e)
      {
         className = className.replace('.', File.separatorChar) + ".class";
         className = "C:\\intelliswiftee\\Scratchpad\\bin\\" + className;
         System.out.println("Class name to be loaded by us : " + className);
         byte[] classBytes = null;
         try
         {
            // This loads the byte code data from the file
            classBytes = loadClassData(className);
            // defineClass is inherited from the ClassLoader class
            // and converts the byte array into a Class
            Class<?> theClass = defineClass("com.sgs.recon.rules.ReconDBProcessorStarter", classBytes, 0, classBytes.length);
            resolveClass(theClass);
            return theClass;
         }
         catch (IOException e1)
         {
            throw (new ClassNotFoundException("Class " + className + " not found"));
         }
      }
   }

   @Override
   protected String findLibrary(String libname)
   {
      String libFileName = super.findLibrary(libname);
      if (libFileName == null)
      {
         libFileName = "lib/" + libname;
         System.out.println(libFileName);
         return libFileName;
      }
      return libFileName;
   }

   private byte[] loadClassData(String name) throws IOException
   {
      // Opening the file
      System.out.println("class name to be loaded from load class data : " + name);
      //      InputStream stream = getClass().getClassLoader().getResourceAsStream(name);
      InputStream stream = getResourceAsStream(name);
      int size = stream.available();
      byte buff[] = new byte[size];
      DataInputStream in = new DataInputStream(stream);
      // Reading the binary data
      in.readFully(buff);
      in.close();
      return buff;
   }

   @Override
   protected URL findResource(String name)
   {
      URL url = super.findResource(name);
      if (url == null)
      {
         File file = new File(name);
         try
         {
            url = file.toURI().toURL();
            System.out.println("URL found from find resource : " + url);
            return url;
         }
         catch (MalformedURLException e)
         {
            e.printStackTrace();
         }
      }
      return null;
   }

   public static void main(String[] args)
   {
      System.load(new File("lib/symlink.so").getAbsolutePath());
   }

}
