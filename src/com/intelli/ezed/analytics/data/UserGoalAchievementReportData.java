package com.intelli.ezed.analytics.data;

import java.sql.SQLException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.slf4j.Logger;

import com.intelli.ezed.data.ResponseObject;

public class UserGoalAchievementReportData extends ResponseObject
{
   private String userId;
   private ArrayList<UserCourseGoalAchievementReportData> userCourseGoalAchievementData = new ArrayList<UserCourseGoalAchievementReportData>();

   private GenericChartData title = new GenericChartData();
   private GenericChartData xAxis = new GenericChartData();
   private GenericChartData yAxis = new GenericChartData();
   private GenericChartData type = new GenericChartData();

   public void prepareChart() throws Exception
   {
      type.getText().add("type");
      type.getValues().add("bar");
      type.prepareResponse();
      
      title.getText().add("text");
      title.getValues().add("User Goals report");
      title.prepareResponse();

      xAxis.getText().add("categories");
      String[] cats = {"Goals achieved in Current Period", "Goals failed in Current Period", "Goals achieved in Previous Period", "Goals failed in Previous Period"};
      xAxis.getValues().add(cats);
      xAxis.getText().add("title");
      GenericChartData xAxisTitleData = new GenericChartData();
      xAxisTitleData.getText().add("text");
      xAxisTitleData.getValues().add("Time line of Goals");
      xAxisTitleData.prepareResponse();
      xAxis.getValues().add(xAxisTitleData);
      xAxis.prepareResponse();

      yAxis.getText().add("min");
      yAxis.getValues().add(0);
      yAxis.getText().add("title");
      GenericChartData yAxisText = new GenericChartData();
      yAxisText.getText().add("text");
      yAxisText.getValues().add("Number of Goals");
      yAxisText.getText().add("align");
      yAxisText.getValues().add("high");
      yAxis.getValues().add(yAxisText);
      yAxis.getText().add("labels");
      GenericChartData yAxisLabels = new GenericChartData();
      yAxisLabels.getText().add("overflow");
      yAxisLabels.getValues().add("justify");
      yAxis.getValues().add(yAxisLabels);

      yAxisLabels.prepareResponse();
      yAxisText.prepareResponse();
      yAxis.prepareResponse();
   }

   public ArrayList<UserCourseGoalAchievementReportData> getUserCourseGoalAchievementData()
   {
      return userCourseGoalAchievementData;
   }

   public void setUserCourseGoalAchievementData(ArrayList<UserCourseGoalAchievementReportData> userCourseGoalAchievementData)
   {
      this.userCourseGoalAchievementData = userCourseGoalAchievementData;
   }

   public void calculateHistoryPoints(String dbConnName, Logger logger) throws SQLException, Exception
   {
      //      this.fetchAllCourseHistoryPointsForUser(dbConnName, logger);
      for (UserCourseGoalAchievementReportData courseData : this.userCourseGoalAchievementData)
      {
         courseData.calculateCourseHistoryPoints(dbConnName, logger);
      }
   }

   //   private void fetchAllCourseHistoryPointsForUser(String dbConnName, Logger logger) throws SQLException, Exception
   //   {
   //      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
   //      PreparedStatement ps = null;
   //      ResultSet rs = null;
   //      try
   //      {
   //         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchCourseDetailsForGoalsUser");
   //         ps.setString(1, userId);
   //         rs = ps.executeQuery();
   //         UserCourseGoalAchievementReportData courseData = null;
   //         while (rs.next())
   //         {
   //            courseData = new UserCourseGoalAchievementReportData();
   //            courseData.setUserId(getUserId());
   //            courseData.setCourseId(rs.getString(1));
   //            courseData.setCourseName(rs.getString(2));
   //            courseData.setCurrentGoalAchieved(rs.getInt(3));
   //            courseData.setCurrentGoalFailed(rs.getInt(4));
   //            this.userCourseGoalAchievementData.add(courseData);
   //         }
   //      }
   //      catch (SQLException e)
   //      {
   //         logger.error("Error while fetching user courses for userid " + getUserId(), e);
   //         throw e;
   //      }
   //      catch (Exception e)
   //      {
   //         logger.error("Error while fetching user courses for userid " + getUserId(), e);
   //         throw e;
   //      }
   //      finally
   //      {
   //         if (rs != null)
   //         {
   //            try
   //            {
   //               rs.close();
   //            }
   //            catch (Exception e)
   //            {
   //               logger.error("Error while closing result set while fetching courses of user");
   //            }
   //         }
   //         DbConnectionManager.releaseConnection(dbConnName, conn);
   //      }
   //   }

   @Override
   public void prepareResponse() throws Exception
   {
      //      UserCourseGoalAchievementReportData[] coursedatas = new UserCourseGoalAchievementReportData[this.userCourseGoalAchievementData.size()];
      //      for (int i = 0; i < this.userCourseGoalAchievementData.size(); i++)
      //      {
      //         this.userCourseGoalAchievementData.get(i).prepareResponse();
      //         coursedatas[i] = this.userCourseGoalAchievementData.get(i);
      //      }
      //      this.put("coursepoints", coursedatas);
      JSONArray jsonArray = new JSONArray();
      GranularGoals granularGoals = null;
      UserCourseGoalAchievementReportData singleData = null;
      for (int i = 0; i < getUserCourseGoalAchievementData().size(); i++)
      {
         singleData = getUserCourseGoalAchievementData().get(i);

         granularGoals = new GranularGoals("Day wise goals for " + singleData.getCourseName(), singleData.getGoalsAchievedToday(), singleData.getGoalsFailedToday(), singleData.getGoalsAchievedYesterday(), singleData.getGoalsFailedYesterday());
         granularGoals.prepareResponse();
         jsonArray.put(granularGoals);

         granularGoals = new GranularGoals("Week wise goals for " + singleData.getCourseName(), singleData.getGoalsAchievedThisWeek(), singleData.getGoalsFailedThisWeek(), singleData.getGoalsAchievedLastWeek(), singleData.getGoalsFailedLastWeek());
         granularGoals.prepareResponse();
         jsonArray.put(granularGoals);

         granularGoals = new GranularGoals("Month wise goals for " + singleData.getCourseName(), singleData.getGoalsAchievedThisMonth(), singleData.getGoalsFailedThisMonth(), singleData.getGoalsAchievedLastMonth(), singleData.getGoalsFailedLastMonth());
         granularGoals.prepareResponse();
         jsonArray.put(granularGoals);

         granularGoals = new GranularGoals("Half year wise goals for " + singleData.getCourseName(), singleData.getGoalsAchievedThisHalfYear(), singleData.getGoalsFailedThisHalfYear(), singleData.getGoalsAchievedLastHalfYear(), singleData.getGoalsFailedLastHalfYear());
         granularGoals.prepareResponse();
         jsonArray.put(granularGoals);
      }

      this.put("chart", type);
      this.put("title", title);
      this.put("xAxis", xAxis);
      this.put("yAxis", yAxis);
      this.put("series", jsonArray);
   }

   public String getUserId()
   {
      return userId;
   }

   public void setUserId(String userId)
   {
      this.userId = userId;
   }
}
