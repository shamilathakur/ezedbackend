package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.ForumPostData;
import com.intelli.ezed.data.ForumPostHandler;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.utils.EzedHelper;

public class PostsFetcher extends SingletonRule
{
   private String dbConnName;
   private String defaultProfilePic;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss");
   private SimpleDateFormat sdfOut = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");

   public PostsFetcher(String ruleName, String dbConnName, String defaultProfilePic)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
      this.defaultProfilePic = defaultProfilePic;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      String userId = (String) flowContext.getFromContext(EzedKeyConstants.USER_ID);

      String threadId = messageData.getParameter("threadid");
      if (threadId == null || threadId.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Thread id in request is improper");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid thread id");
         return RuleDecisionKey.ON_FAILURE;
      }

      String postCount = messageData.getParameter("startpoint");
      int postCountInt = 0;
      if (postCount == null || postCount.trim().compareTo("") == 0 || !EzedHelper.isNumeric(postCount.trim()))
      {
         postCount = "500";
      }
      postCountInt = Integer.parseInt(postCount);

      String offset = messageData.getParameter("offset");
      int offsetInt = 0;
      if (offset == null || offset.trim().compareTo("") == 0 || !EzedHelper.isNumeric(offset.trim()))
      {
         offset = "0";
      }
      offsetInt = Integer.parseInt(offset);

      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      String firstname = null;
      String lastname = null;
      String name = null;
      ForumPostHandler postHandler = new ForumPostHandler();
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchForumPosts");
         ps.setString(1, threadId);
         ps.setInt(2, postCountInt);
         ps.setInt(3, offsetInt);
         rs = ps.executeQuery();
         ForumPostData postData = null;
         String parentPostId = null;
         while (rs.next())
         {
            postData = new ForumPostData();
            postData.setPostId(rs.getString(1));
            postData.setUserId(rs.getString(2));
            parentPostId = rs.getString(3);
            if (parentPostId == null || parentPostId.trim().compareTo("") == 0)
            {
               parentPostId = null;
            }
            postData.setParentPostId(parentPostId);
            postData.setPostData(rs.getString(4));
            postData.setImagePath(rs.getString(5));
            postData.setAbuseCount(rs.getInt(6));
            postData.setLikeCount(rs.getInt(7));
            postData.setCreateTime(sdfOut.format(sdf.parse(rs.getString(8))));
            postData.setAdminFlag(rs.getInt(9));
            postData.setStatus(rs.getString("status"));
            if (postData.isAdminFlag())
            {
               name = rs.getString("name");
               if (name != null)
               {
                  postData.setName(name);
               }
               else
               {
                  postData.setName(postData.getUserId());
               }
            }
            else
            {
               firstname = rs.getString("firstname");
               lastname = rs.getString("lastname");
               if (firstname == null && lastname == null)
               {
                  postData.setName(postData.getUserId());
               }
               else if (firstname != null && lastname != null)
               {
                  postData.setName(firstname + " " + lastname);
               }
               else if (firstname != null)
               {
                  postData.setName(firstname);
               }
               else
               {
                  postData.setName(lastname);
               }
            }
            postHandler.addForumPostData(postData);
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching forum posts for thread id " + threadId, e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching forum posts");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (ParseException e)
      {
         logger.error("Error while parsing create date of forum posts for thread id " + threadId, e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching forum posts");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching forum posts for thread id " + threadId, e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching forum posts");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing resultset while fetching posts of thread for thread id " + threadId, e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchUserPostLikes");
         ps.setString(1, userId);
         rs = ps.executeQuery();
         String postId = null;
         while (rs.next())
         {
            postId = rs.getString("postid");
            postHandler.handleUserPostLike(postId);
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while handling if user has liked posts", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while handling if user has liked post");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while handling if user has liked posts", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while handling if user has liked post");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchUserPostAbuses");
         ps.setString(1, userId);
         rs = ps.executeQuery();
         String postId = null;
         while (rs.next())
         {
            postId = rs.getString("postid");
            postHandler.handleUserPostAbuse(postId);
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while handling if user has abused posts", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while handling if user has abused post");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while handling if user has abused posts", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while handling if user has abused post");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      try
      {
         postHandler.handleProfileImages(dbConnName, logger, defaultProfilePic);
         postHandler.createTree();
         postHandler.calculateIndents();
         postHandler.finalizeTree();
         response.setResponseObject(postHandler);
         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
         return RuleDecisionKey.ON_SUCCESS;
      }
      catch (Exception e)
      {
         logger.error("Error while doing forum tree calculations", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching and arranging forum posts");
         return RuleDecisionKey.ON_FAILURE;
      }
   }
}
