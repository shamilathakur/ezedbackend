package com.intelli.ezed.sheduler;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

import org.sabre.utils.threads.TimedTask;
import org.sabre.workflow.context.FlowContext;
import org.sabre.workflow.processor.InputProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.analytics.data.LastMarkData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;

public class RegularAnalyticsMarker extends InputProcessor
{
   private static final long MILLIS_IN_DAY = (long) 1000 * 60 * 60 * 24;
   private static final long MILLIS_IN_WEEK = (long) 1000 * 60 * 60 * 24 * 7;
   private static final long MILLIS_IN_MONTH = (long) 1000 * 60 * 60 * 24 * 30;
   private static final long MILLIS_IN_SIX_MONTH = (long) 1000 * 60 * 60 * 24 * 180;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.ANALYTIC_BACKUP_LOG);
   private BackupTaker backupTaker;
   private LastMarkData lastMarkData = new LastMarkData();
   private AtomicInteger atomicInteger;
   private FlowContext baseContext;
   private SimpleDateFormat sdf = new SimpleDateFormat("HHmmss");

   public RegularAnalyticsMarker(String startAtTime, String pollingTime, String dbConnName)
   {
      atomicInteger = new AtomicInteger(1);
      Calendar calendar = Calendar.getInstance();
      calendar.add(Calendar.MINUTE, 1);
      backupTaker = new BackupTaker(sdf.format(calendar.getTime()), Long.parseLong(pollingTime));
      try
      {
         lastMarkData.populateBackupDatesFromDB(dbConnName, logger);
      }
      catch (Exception e)
      {
         logger.error("Error while fetching backup dates. Setting all backup dates as yesterday", e);
         Date date = new Date(System.currentTimeMillis());
         SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
         try
         {
            date = sdf.parse(sdf.format(date));
            Calendar todayCal = Calendar.getInstance();
            todayCal.setTime(date);
            todayCal.add(Calendar.DATE, -1);
            lastMarkData.setLastDayBackupDate(todayCal.getTime());
            lastMarkData.setLastWeekBackupDate(todayCal.getTime());
            lastMarkData.setLastMonthBackupDate(todayCal.getTime());
            lastMarkData.setLastSixMonthBackupDate(todayCal.getTime());
         }
         catch (ParseException e1)
         {
            logger.error("Error while setting last backup dates", e1);
            System.exit(1);
         }
         try
         {
            lastMarkData.persistBackupDatesInDB(dbConnName, logger);
         }
         catch (Exception e1)
         {
            logger.error("Error while inserting last backup dates in db ", e1);
            System.exit(1);
         }
      }
   }

   class BackupTaker extends TimedTask
   {
      public BackupTaker(String startAtTime, long pollingTime)
      {
         super(startAtTime, pollingTime);
         this.setStartAtTime(startAtTime);
         this.setExecAtStartup(true);
         this.setOneTimeTask(false);
      }

      @Override
      public void doTimedTask()
      {
         takeBackup();
      }

      private void takeBackup()
      {
         logger.info("Starting processor to take backup");
         Date date = new Date(System.currentTimeMillis());
         if (date.getTime() - lastMarkData.getLastDayBackupDate().getTime() >= MILLIS_IN_DAY)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Millis in today " + date.getTime() + " millis in yesterday " + lastMarkData.getLastDayBackupDate().getTime());
            }
            FlowContext currentContext = baseContext.createSubContext(getSubContextName());
            currentContext.putIntoContext(EzedKeyConstants.LAST_BACKUP_DELETE_PREP_STMT_USERS, "DeleteyesterdayUsers");
            currentContext.putIntoContext(EzedKeyConstants.LAST_TO_LAST_BACKUP_DELETE_PREP_STMT_USERS, "DeletedaybeforeyesterdayUsers");
            currentContext.putIntoContext(EzedKeyConstants.LAST_BACKUP_DELETE_PREP_STMT_USER_COURSE, "DeleteyesterdayUserCourse");
            currentContext.putIntoContext(EzedKeyConstants.LAST_TO_LAST_BACKUP_DELETE_PREP_STMT_USER_COURSE, "DeletedaybeforeyesterdayUserCourse");
            currentContext.putIntoContext(EzedKeyConstants.LAST_BACKUP_PREP_STMT_USERS, "InsertyesterdayUsers");
            currentContext.putIntoContext(EzedKeyConstants.LAST_TO_LAST_BACKUP_PREP_STMT_USERS, "InsertdaybeforeyesterdayUsers");
            currentContext.putIntoContext(EzedKeyConstants.LAST_BACKUP_PREP_STMT_USER_COURSE, "InsertyesterdayUserCourse");
            currentContext.putIntoContext(EzedKeyConstants.LAST_TO_LAST_BACKUP_PREP_STMT_USER_COURSE, "InsertdaybeforeyesterdayUserCourse");
            currentContext.putIntoContext(EzedKeyConstants.TODAY_DATE, date);
            currentContext.putIntoContext(EzedKeyConstants.ANALYTIC_DATE_UPDATE_PREPSTMT, "UpdateBackupDayDate");
            lastMarkData.setLastDayBackupDate(date);
            process(currentContext);
         }
         if (date.getTime() - lastMarkData.getLastWeekBackupDate().getTime() >= MILLIS_IN_WEEK)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Millis in today " + date.getTime() + " millis in last week " + lastMarkData.getLastWeekBackupDate().getTime());
            }
            FlowContext currentContext = baseContext.createSubContext(getSubContextName());
            currentContext.putIntoContext(EzedKeyConstants.LAST_BACKUP_DELETE_PREP_STMT_USERS, "DeletelastweekUsers");
            currentContext.putIntoContext(EzedKeyConstants.LAST_TO_LAST_BACKUP_DELETE_PREP_STMT_USERS, "DeletelastfortnightUsers");
            currentContext.putIntoContext(EzedKeyConstants.LAST_BACKUP_DELETE_PREP_STMT_USER_COURSE, "DeletelastweekUserCourse");
            currentContext.putIntoContext(EzedKeyConstants.LAST_TO_LAST_BACKUP_DELETE_PREP_STMT_USER_COURSE, "DeletelastfortnightUserCourse");
            currentContext.putIntoContext(EzedKeyConstants.LAST_BACKUP_PREP_STMT_USERS, "InsertlastweekUsers");
            currentContext.putIntoContext(EzedKeyConstants.LAST_TO_LAST_BACKUP_PREP_STMT_USERS, "InsertlastfortnightUsers");
            currentContext.putIntoContext(EzedKeyConstants.LAST_BACKUP_PREP_STMT_USER_COURSE, "InsertlastweekUserCourse");
            currentContext.putIntoContext(EzedKeyConstants.LAST_TO_LAST_BACKUP_PREP_STMT_USER_COURSE, "InsertlastfortnightUserCourse");
            currentContext.putIntoContext(EzedKeyConstants.TODAY_DATE, date);
            currentContext.putIntoContext(EzedKeyConstants.ANALYTIC_DATE_UPDATE_PREPSTMT, "UpdateBackupWeekDate");
            lastMarkData.setLastWeekBackupDate(date);
            process(currentContext);
         }
         if (date.getTime() - lastMarkData.getLastMonthBackupDate().getTime() >= MILLIS_IN_MONTH)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Millis in today " + date.getTime() + " millis in last month " + lastMarkData.getLastMonthBackupDate().getTime());
            }
            FlowContext currentContext = baseContext.createSubContext(getSubContextName());
            currentContext.putIntoContext(EzedKeyConstants.LAST_BACKUP_DELETE_PREP_STMT_USERS, "DeletelastmonthUsers");
            currentContext.putIntoContext(EzedKeyConstants.LAST_TO_LAST_BACKUP_DELETE_PREP_STMT_USERS, "DeletelasttwomonthUsers");
            currentContext.putIntoContext(EzedKeyConstants.LAST_BACKUP_DELETE_PREP_STMT_USER_COURSE, "DeletelastmonthUserCourse");
            currentContext.putIntoContext(EzedKeyConstants.LAST_TO_LAST_BACKUP_DELETE_PREP_STMT_USER_COURSE, "DeletelasttwomonthUserCourse");
            currentContext.putIntoContext(EzedKeyConstants.LAST_BACKUP_PREP_STMT_USERS, "InsertlastmonthUsers");
            currentContext.putIntoContext(EzedKeyConstants.LAST_TO_LAST_BACKUP_PREP_STMT_USERS, "InsertlasttwomonthUsers");
            currentContext.putIntoContext(EzedKeyConstants.LAST_BACKUP_PREP_STMT_USER_COURSE, "InsertlastmonthUserCourse");
            currentContext.putIntoContext(EzedKeyConstants.LAST_TO_LAST_BACKUP_PREP_STMT_USER_COURSE, "InsertlasttwomonthUserCourse");
            currentContext.putIntoContext(EzedKeyConstants.TODAY_DATE, date);
            currentContext.putIntoContext(EzedKeyConstants.ANALYTIC_DATE_UPDATE_PREPSTMT, "UpdateBackupMonthDate");
            lastMarkData.setLastMonthBackupDate(date);
            process(currentContext);
         }
         if (date.getTime() - lastMarkData.getLastSixMonthBackupDate().getTime() >= MILLIS_IN_SIX_MONTH)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Millis in today " + date.getTime() + " millis in last 6 months " + lastMarkData.getLastSixMonthBackupDate().getTime());
            }
            FlowContext currentContext = baseContext.createSubContext(getSubContextName());
            currentContext.putIntoContext(EzedKeyConstants.LAST_BACKUP_DELETE_PREP_STMT_USERS, "DeletelastsixmonthUsers");
            currentContext.putIntoContext(EzedKeyConstants.LAST_TO_LAST_BACKUP_DELETE_PREP_STMT_USERS, "DeletelastyearUsers");
            currentContext.putIntoContext(EzedKeyConstants.LAST_BACKUP_DELETE_PREP_STMT_USER_COURSE, "DeletelastsixmonthUserCourse");
            currentContext.putIntoContext(EzedKeyConstants.LAST_TO_LAST_BACKUP_DELETE_PREP_STMT_USER_COURSE, "DeletelastyearUserCourse");
            currentContext.putIntoContext(EzedKeyConstants.LAST_BACKUP_PREP_STMT_USERS, "InsertlastsixmonthUsers");
            currentContext.putIntoContext(EzedKeyConstants.LAST_TO_LAST_BACKUP_PREP_STMT_USERS, "InsertlastyearUsers");
            currentContext.putIntoContext(EzedKeyConstants.LAST_BACKUP_PREP_STMT_USER_COURSE, "InsertlastsixmonthUserCourse");
            currentContext.putIntoContext(EzedKeyConstants.LAST_TO_LAST_BACKUP_PREP_STMT_USER_COURSE, "InsertlastyearUserCourse");
            currentContext.putIntoContext(EzedKeyConstants.TODAY_DATE, date);
            currentContext.putIntoContext(EzedKeyConstants.ANALYTIC_DATE_UPDATE_PREPSTMT, "UpdateBackupSixMonthDate");
            lastMarkData.setLastSixMonthBackupDate(date);
            process(currentContext);
         }
      }
   }

   @Override
   public void initialize(FlowContext flowContext)
   {
      this.baseContext = flowContext;
   }

   @Override
   public void start()
   {
      backupTaker.startTask(true);
   }

   //   public static void main(String[] args) throws Exception
   //   {
   //      SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
   //      Date date = new Date(System.currentTimeMillis());
   //      System.out.println("Date today " + sdf.format(date));
   //
   //      Calendar todayCal = Calendar.getInstance();
   //
   //      todayCal.setTime(date);
   //      todayCal.add(Calendar.DATE, -1);
   //      System.out.println("Date yesterday " + sdf.format(todayCal.getTime()));
   //
   //      todayCal.setTime(date);
   //      todayCal.add(Calendar.DATE, -7);
   //      System.out.println("Date a week before " + sdf.format(todayCal.getTime()));
   //
   //      todayCal.setTime(date);
   //      todayCal.add(Calendar.MONTH, -1);
   //      System.out.println("Date a month before " + sdf.format(todayCal.getTime()));
   //
   //      todayCal.setTime(date);
   //      todayCal.add(Calendar.MONTH, -6);
   //      System.out.println("Date a month before " + sdf.format(todayCal.getTime()));
   //
   //      Date yesterDay = sdf.parse("20130406000000");
   //      long millisInDay = 1000 * 60 * 60 * 24;
   //      if (date.getTime() - yesterDay.getTime() >= millisInDay)
   //      {
   //         System.out.println("yesterday millis : " + yesterDay.getTime());
   //         System.out.println("today millis : " + date.getTime());
   //         System.out.println("millis in a day : " + millisInDay);
   //         System.out.println(date.getTime() - yesterDay.getTime());
   //         System.out.println("It is before yesterday");
   //      }
   //   }

   public String getSubContextName()
   {
      return "AnalyticsMarkBackupTaker" + atomicInteger.getAndIncrement();
   }

   public static void main(String[] args)
   {
      System.out.println((long) 1000 * 60 * 60 * 24 * 180);
   }
}
