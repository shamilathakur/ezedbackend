package com.intelli.ezed.bulkupload.rules;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.bulkupload.data.UniversityDetails;
import com.intelli.ezed.data.EzedLoggerName;

public class UniversityCourseReaderDumper extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.BULK_UPLOAD_LOG);
   private String dbConnName;
   private String rootUnivDir;
   private String univDetailsFileName;

   public UniversityCourseReaderDumper(String ruleName, String dbConnName, String rootUnivDir,
                                       String univDetailsFileName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
      this.rootUnivDir = rootUnivDir;
      this.univDetailsFileName = univDetailsFileName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;

      File file = new File(rootUnivDir);
      if (!file.exists())
      {
         file.mkdir();
      }
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchUniversityDetails");
         rs = ps.executeQuery();
         ArrayList<UniversityDetails> datas = new ArrayList<UniversityDetails>();
         UniversityDetails data = null;
         while (rs.next())
         {
            data = new UniversityDetails();
            data.setUniversity(rs.getString("univname"));
            data.setWebsite(rs.getString("website"));
            data.setPhoneno(rs.getString("phonenumber"));
            data.setCountry(rs.getString("countryname"));
            data.setState(rs.getString("statename"));
            data.setCity(rs.getString("cityname"));
            data.writeToFile(rootUnivDir, univDetailsFileName);
            datas.add(data);
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching university details from db", e);
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (IOException e)
      {
         logger.error("Error while fetching university details from db", e);
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching university details from db", e);
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
      return RuleDecisionKey.ON_SUCCESS;
   }
}