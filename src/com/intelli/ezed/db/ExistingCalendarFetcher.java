package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.CourseCalendarData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.GoalData;
import com.intelli.ezed.data.Response;

public class ExistingCalendarFetcher extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public ExistingCalendarFetcher(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   //select cl.courseid,chapterid,contentid,endtime,goalachieved,enddate,startdate from userpoints u,courselist cl,usercourse uc where u.userid='indraneel.dey@intelliswift.co.in' and u.courseid='COU20000000136532823' and uc.userid='indraneel.dey@intelliswift.co.in' and uc.courseid='COU20000000136532823' and cl.courseid='COU20000000136532823';

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      CourseCalendarData courseCalendarData = (CourseCalendarData) flowContext.getFromContext(EzedKeyConstants.COURSE_CALENDAR);
      String userId = (String) flowContext.getFromContext(EzedKeyConstants.USER_ID);
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;

      //in case no goals are added as yet in the database, fail this query.
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchExistingUserCourseCalendar");
         ps.setString(1, userId);
         ps.setString(2, courseCalendarData.getCourseId());
         ps.setString(3, userId);
         ps.setString(4, courseCalendarData.getCourseId());
         ps.setString(5, courseCalendarData.getCourseId());
         rs = ps.executeQuery();
         CourseCalendarData existingCourseCalendarData = new CourseCalendarData();
         existingCourseCalendarData.setCourseId(courseCalendarData.getCourseId());
         ArrayList<GoalData> goalDatas = new ArrayList<GoalData>();
         GoalData singleGoalData = null;
         SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
         SimpleDateFormat contentSDF = new SimpleDateFormat("yyyyMMddHHmmss");
         boolean wentOnce = false;
         String contentEndDate = null;
         while (rs.next())
         {
            if (!wentOnce)
            {
               String endDate = rs.getString(6);
               String startDate = rs.getString(7);
               Date endingDate = contentSDF.parse(endDate);
               Date startingDate = contentSDF.parse(startDate);
               existingCourseCalendarData.setStartDate(startingDate);
               existingCourseCalendarData.setEndDate(endingDate);
               wentOnce = true;
            }
            singleGoalData = new GoalData();
            singleGoalData.setCourseId(rs.getString(1));
            singleGoalData.setChapterId(rs.getString(2));
            singleGoalData.setContentId(rs.getString(3));
            contentEndDate = rs.getString(4);
            singleGoalData.setEndDate(contentSDF.parse(contentEndDate));
            singleGoalData.setGoalComplete(rs.getInt(5));
            singleGoalData.setCustomFlag(rs.getInt("customgoal"));
            goalDatas.add(singleGoalData);
         }
         if (goalDatas.size() == 0)
         {
            logger.error("Existing caledar could not be fetched from db");
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching existing calendar");
            return RuleDecisionKey.ON_FAILURE;
         }
         existingCourseCalendarData.setGoalDatas(goalDatas);
         flowContext.putIntoContext(EzedKeyConstants.EXISTING_COURSE_CALENDAR, existingCourseCalendarData);
         if (logger.isDebugEnabled())
         {
            logger.debug("Fetched existing course calendar successfully for course id " + courseCalendarData.getCourseId());
         }
         return RuleDecisionKey.ON_SUCCESS;
      }
      catch (SQLException e)
      {
         logger.error("DB error while fetching existing course calendar details for course id " + courseCalendarData.getCourseId(), e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching course goal details");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (ParseException e)
      {
         logger.error("Error while parsing content and course ending dates from db", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while parsing dates");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching course calendar from db", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching course goal details");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing result set while fetching existing calendar", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
}
