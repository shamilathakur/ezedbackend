package com.intelli.ezed.rules;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.session.SessionException;
import com.a.helper.session.SessionManager;
import com.a.helper.utils.StringLockerUtil;
import com.intelli.ezed.bulkupload.data.ActiveSessionList;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.LoginResponse;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.UserProfile;
import com.intelli.ezed.utils.Globals;

public class SessionGenerator extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public SessionGenerator(String ruleName)
   {
      super(ruleName);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      SessionManager sessionManager = (SessionManager) flowContext.getFromContext(EzedKeyConstants.SESSION_MANAGER);
      String sessionId = null;
      try
      {
         sessionId = sessionManager.getSessionId();
         if (logger.isDebugEnabled())
         {
            logger.debug("Session id generated " + sessionId);
         }
         LoginResponse loginResponse = (LoginResponse) response.getResponseObject();
         loginResponse.setSessionId(sessionId);
         flowContext.putIntoContext(EzedKeyConstants.SESSION_ID, sessionId);
         UserProfile userProfile = (UserProfile) flowContext.getFromContext(EzedKeyConstants.USER_DETAILS);
         ActiveSessionList.addToActiveSession(userProfile.getUserid());
         if (logger.isDebugEnabled())
         {
            logger.debug("Session id " + userProfile.getUserid() + " added to active session list");
         }
      }
      catch (SessionException e)
      {
         logger.error("Session id could not be generated", e);
         response.setParameters(EzedTransactionCodes.SESSION_ERROR_OCCURRED, "Error while generating session");
         return RuleDecisionKey.ON_FAILURE;
      }
      try
      {
         StringLockerUtil.lockString(sessionId, Globals.USER_SESSION_LOCK_SET);
      }
      catch (InterruptedException e)
      {
         logger.error("Session could not be locked " + sessionId, e);
         response.setParameters(EzedTransactionCodes.SESSION_ERROR_OCCURRED, "Error while generating session");
         return RuleDecisionKey.ON_FAILURE;
      }
      return RuleDecisionKey.ON_SUCCESS;
   }
}
