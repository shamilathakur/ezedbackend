package com.intelli.ezed.rules;

import java.util.HashSet;
import java.util.Set;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.session.SessionException;
import com.a.helper.session.SessionManager;
import com.a.helper.utils.StringLockerUtil;
import com.intelli.ezed.data.AdminProfile;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.TestDetails;
import com.intelli.ezed.utils.Globals;

public class AdminSessionPersister extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public AdminSessionPersister(String ruleName)
   {
      super(ruleName);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      SessionManager sessionManager = (SessionManager) flowContext.getFromContext(EzedKeyConstants.ADMIN_SESSION_MANAGER);
      String sessionId = (String) flowContext.getFromContext(EzedKeyConstants.ADMIN_SESSION_ID);
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      Set<String> contextKeySet = new HashSet<String>();
      contextKeySet.add(EzedKeyConstants.REQUEST_IN_TIME);
      contextKeySet.add(EzedKeyConstants.ADMIN_PROFILE);
      contextKeySet.add(EzedKeyConstants.ADMIN_SESSION_ID);

      //adding test object to session data
      contextKeySet.add(TestDetails.TEST_DETAILS);

      AdminProfile userProfile = (AdminProfile) flowContext.getFromContext(EzedKeyConstants.ADMIN_PROFILE);
      if (userProfile == null)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("User profile null or blank for admin. Not persisting session");
         }
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Could not persist session");
         StringLockerUtil.unlockString(sessionId, Globals.ADMIN_SESSION_LOCK_SET);
         return RuleDecisionKey.ON_FAILURE;
      }
      String emailId = userProfile.getUserid();
      if (emailId == null || emailId.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Email id is null or blank. Not persisting session");
         }
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Could not persist session");
         StringLockerUtil.unlockString(sessionId, Globals.ADMIN_SESSION_LOCK_SET);
         return RuleDecisionKey.ON_FAILURE;
      }
      try
      {
         sessionManager.addDataToSession(emailId, flowContext, contextKeySet, logger);
         if (logger.isDebugEnabled())
         {
            logger.debug("Session persisted successfully for id " + emailId + " and session id " + sessionId);
         }
         StringLockerUtil.unlockString(sessionId, Globals.ADMIN_SESSION_LOCK_SET);
      }
      catch (SessionException e)
      {
         logger.error("Error while persisting session", e);
         response.setParameters(EzedTransactionCodes.SESSION_ERROR_OCCURRED, "Error while persisting session");
         StringLockerUtil.unlockString(sessionId, Globals.ADMIN_SESSION_LOCK_SET);
         return RuleDecisionKey.ON_FAILURE;
      }
      return RuleDecisionKey.ON_SUCCESS;
   }
}
