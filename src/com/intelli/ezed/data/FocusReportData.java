package com.intelli.ezed.data;


public class FocusReportData implements Comparable<FocusReportData>
{
   private String chapterName;
   private float value;

   public String getChapterName()
   {
      return chapterName;
   }

   public void setChapterName(String chapterName)
   {
      this.chapterName = chapterName;
   }

   public float getValue()
   {
      return value;
   }

   public void setValue(float value)
   {
      this.value = value;
   }

   @Override
   public int compareTo(FocusReportData o)
   {
      if (getValue() >= o.getValue())
      {
         return -1;
      }
      return 1;
   }

}
