package com.intelli.ezed.checks;

import java.util.ArrayList;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.AdminProfile;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;

public class CheckAdminRoll extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String userRoll;

   public CheckAdminRoll(String ruleName, String userRoll)
   {
      super(ruleName);
      this.userRoll = userRoll;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      AdminProfile adminProfile = (AdminProfile) flowContext.getFromContext(EzedKeyConstants.ADMIN_PROFILE);

      ArrayList<Integer> adminRolls = adminProfile.getRollId();

      for (int i : adminRolls)
      {
         String roll = String.valueOf(i);
         if(userRoll.contains(roll))
         {
            if(logger.isDebugEnabled())
            {
               logger.debug("Admin Roll found.");
            }
            return RuleDecisionKey.ON_SUCCESS;
         }
      }

      if(logger.isDebugEnabled())
      {
         logger.debug("Admin Roll does not match.");
      }
      return RuleDecisionKey.ON_FAILURE;
   }

}
