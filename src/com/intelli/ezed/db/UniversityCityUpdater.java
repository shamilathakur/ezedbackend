package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class UniversityCityUpdater extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String defaultCityId;

   public UniversityCityUpdater(String ruleName, String dbConnName, String defCityId)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
      this.defaultCityId = defCityId;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());

      String univname = messageData.getParameter("university");
      if (univname == null || univname.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("University name in request in invalid");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid university name");
         return RuleDecisionKey.ON_FAILURE;
      }

      String countryName = messageData.getParameter("country");
      if (countryName == null || countryName.trim().compareTo("") == 0 || countryName.trim().compareToIgnoreCase("default") == 0)
      {
         countryName = null;
      }

      String stateName = messageData.getParameter("state");
      if (stateName == null || stateName.trim().compareTo("") == 0 || stateName.trim().compareToIgnoreCase("default") == 0)
      {
         stateName = null;
      }

      String cityName = messageData.getParameter("city");
      if (cityName == null || cityName.trim().compareTo("") == 0 || cityName.trim().compareToIgnoreCase("default") == 0)
      {
         cityName = null;
      }

      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         if (countryName == null || stateName == null || cityName == null)
         {
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdateDefaultUnivCity");
            ps.setString(1, defaultCityId);
            ps.setString(2, univname);
         }
         else
         {
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdateUnivCity");
            ps.setString(1, countryName);
            ps.setString(2, stateName);
            ps.setString(3, cityName);
            ps.setString(4, univname);
         }
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows updated in db while updating city of univ " + univname + " to " + cityName);
         }
         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
         return RuleDecisionKey.ON_SUCCESS;
      }
      catch (SQLException e)
      {
         logger.error("Error while updating city details for univ name " + univname, e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while updating university details");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while updating city details for university name " + univname, e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while updating university details");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
}
