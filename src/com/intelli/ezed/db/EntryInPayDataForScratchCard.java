package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.AddScratchCardToUserData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.ScratchCardErrorLogData;
import com.intelli.ezed.utils.ScratchCardHelper;

public class EntryInPayDataForScratchCard extends SingletonRule {

	private String dbConnName;
	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

	public EntryInPayDataForScratchCard(String ruleName, String dbConnName) {
		super(ruleName);
		this.dbConnName = dbConnName;
	}

	public RuleDecisionKey executeRule(FlowContext flowContext) {

		Response response = (Response) flowContext
				.getFromContext(EzedKeyConstants.EZED_RESPONSE);
		AddScratchCardToUserData data = (AddScratchCardToUserData) flowContext
				.getFromContext(EzedKeyConstants.ADD_COURSE_USING_SCRATCHCARD_DETAILS);
		ScratchCardErrorLogData errordata = (ScratchCardErrorLogData) flowContext
				.getFromContext(EzedKeyConstants.SCRATCH_CARD_ERROR_LOG_DB);

		System.out.println("In Class : " + this.getClass().getName());
		System.out.print("USERID : " + data.getUserid() + "  Sessionid : "
				+ data.getSessionid());

		Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
		PreparedStatement ps = null;
		int rs;

		try {
			ps = DbConnectionManager.getPreparedStatement(dbConnName, conn,
					"EntryInPaydataForScratchCard");
			ps.setString(1, data.getAppId());
			ps.setString(2, data.getPaymentId());
			ps.setString(3, data.getUserid());
			ps.setString(4, data.getSessionid());
			ps.setString(5, sdf.format(new Date()));
			ps.setString(6, "1"); // set payment status here..

			StringBuffer sb = new StringBuffer();
			sb.append(data.getCourseid());
			sb.append(",");
			sb.append(data.getCoursetype());
			sb.append(",");
			sb.append(data.getCoursestartdate());
			sb.append(",");
			sb.append(data.getCourseenddate());

			ps.setString(7, sb.toString());
			ps.setString(8, "1"); // set cartstatus here ..
			ps.setInt(9, data.getPrice());
			ps.setString(10, data.getScratchcardid());

			System.out.println("SQLQyuery in EntryInPaydataTable class : \n"
					+ ps);

			rs = ps.executeUpdate();
			if (rs > 0) {
				logger.info("Entry in the paydata table successful !");
				System.out.println("Entry in the paydata table successful !");
			} else {
				logger.info("Something went wrong while making the entry in paydata table!");
				System.out
						.println("Something went wrong while making the entry in paydata table!");
			}
		} catch (SQLException e) {
			System.out.println("SQL Error In Class : "
					+ this.getClass().getName());
			if (logger.isDebugEnabled()) {

				logger.error("SQL Error while making the entry in the paydata table for scratch card id : "
						+ data.getScratchcardid());
			}
			errordata.setClassname(this.getClass().getName());
			ScratchCardHelper.callDbError(errordata,dbConnName,logger);
			response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED,
					"SQL Error while making the entry in the paydata table");
			return RuleDecisionKey.ON_FAILURE;
		} catch (Exception e) {
			if (logger.isDebugEnabled()) {
				logger.error("Internal Error while making the entry in the paydata table for scratch card id : "
						+ data.getScratchcardid());
			}
			errordata.setClassname(this.getClass().getName());
			ScratchCardHelper.callInternalError(errordata,dbConnName,logger);
			response.setParameters(EzedTransactionCodes.INTERNAL_ERROR,
					"Internal Error while making the entry in the paydata table");
			return RuleDecisionKey.ON_FAILURE;
		} finally {
			DbConnectionManager.releaseConnection(dbConnName, conn);
		}

		String paymentid = data.getAppId() + data.getPaymentId();
		System.out.println("paymentid : " + paymentid);
		flowContext.putIntoContext("paymentid", paymentid);
		return RuleDecisionKey.ON_SUCCESS;
	}
}
