package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.digester3.Rule;
import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.AddScratchCardToUserData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.ScratchCardErrorLogData;
import com.intelli.ezed.utils.ScratchCardHelper;

public class OwnedCourseChecker extends SingletonRule {

	private String dbConnName;
	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

	public OwnedCourseChecker(String ruleName, String dbConnName) {
		super(ruleName);
		this.dbConnName = dbConnName;
	}

	@Override
	public RuleDecisionKey executeRule(FlowContext flowContext) {
		Response response = (Response) flowContext
				.getFromContext(EzedKeyConstants.EZED_RESPONSE);
		AddScratchCardToUserData data = (AddScratchCardToUserData) flowContext
				.getFromContext(EzedKeyConstants.ADD_COURSE_USING_SCRATCHCARD_DETAILS);
		ScratchCardErrorLogData errordata = (ScratchCardErrorLogData) flowContext
				.getFromContext(EzedKeyConstants.SCRATCH_CARD_ERROR_LOG_DB);
		Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			ps = DbConnectionManager.getPreparedStatement(dbConnName, conn,
					"CheckIfCourseOwned");
			ps.setString(1, data.getCourseid());
			ps.setString(2, data.getUserid());
			rs = ps.executeQuery();
			if (rs.next()) {
				logger.info("Course is already owned. Process stopped. ");
				errordata.setClassname(this.getClass().getName());
				ScratchCardHelper.callAlreadyOwnCourse(errordata, dbConnName,
						logger);
				response.setParameters(
						EzedTransactionCodes.COURSE_ALREADY_OWNED,
						"Course is already purchased before.");
				return RuleDecisionKey.ON_FAILURE;
			}

		} catch (SQLException e) {
			System.out.println("SQL Error In Class : "
					+ this.getClass().getName());
			if (logger.isDebugEnabled()) {
				logger.debug(
						"SQL Exception occured while checking the owned course.",
						e);
			}
			errordata.setClassname(this.getClass().getName());
			ScratchCardHelper.callDbError(errordata, dbConnName, logger);
			response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED,
					"SQL Exception occured while checking the owned course.");
			return RuleDecisionKey.ON_FAILURE;
		} catch (Exception e) {
			System.out.println("Internal Error In Class : "
					+ this.getClass().getName());
			if (logger.isDebugEnabled()) {
				logger.debug(
						"Exception occured while checking the owned course.", e);
			}
			errordata.setClassname(this.getClass().getName());
			ScratchCardHelper.callInternalError(errordata, dbConnName, logger);
			response.setParameters(EzedTransactionCodes.INTERNAL_ERROR,
					"Exception occured while checking the owned course.");
			return RuleDecisionKey.ON_FAILURE;
		} finally {
			DbConnectionManager.releaseConnection(dbConnName, conn);
		}
		flowContext.putIntoContext(
				EzedKeyConstants.PAYMENT_ID_FOR_TRANSACTION_TYPE,
				EzedKeyConstants.PAYMENT_ID_FOR_TRANSACTION_TYPE_SCRATCHCARD);
		return RuleDecisionKey.ON_SUCCESS;
	}

}
