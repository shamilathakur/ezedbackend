package com.intelli.ezed.utils.bypassers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.a.helper.utils.StringSplitter;

public class CoursePathUpdater
{

   static ArrayList<Integer> arrayList = new ArrayList<Integer>();
   static int index = 0;

   public static void main(String[] args) throws IOException
   {
      String folderName = "samplebulkdata/university/PU/B.E/Mechanics/F.E/II/Engineering Mechanics";
      File folder = new File(folderName);
      File[] chapterFolders = folder.listFiles();
      File chapterXmlFile = null;
      BufferedReader chapterFileReader = null;
      String read = null;
      Map<String, String> chapterNameDirMap = new HashMap<String, String>();
      String[] coursePath = null;
      BufferedReader courseXmlFileReader = null;
      try
      {
         for (int i = 0; i < chapterFolders.length; i++)
         {
            if (chapterFolders[i].isDirectory())
            {
               chapterXmlFile = new File(chapterFolders[i].getAbsolutePath() + File.separator + "chapterdetails.xml");
               chapterFileReader = new BufferedReader(new FileReader(chapterXmlFile));
               read = chapterFileReader.readLine();
               while (read != null)
               {
                  if (read.trim().startsWith("<name>"))
                  {
                     read = read.trim().replaceAll("<name>", "");
                     read = read.replaceAll("</name>", "");
                     chapterNameDirMap.put(chapterFolders[i].getName(), read.trim());
                     break;
                  }
                  read = chapterFileReader.readLine();
               }
            }
         }
         courseXmlFileReader = new BufferedReader(new FileReader(new File(folderName + File.separator + "coursedetails.xml")));
         read = courseXmlFileReader.readLine();
         while (read != null)
         {
            if (read.trim().startsWith("<coursepath>"))
            {
               read = read.trim().replaceAll("<coursepath>", "");
               read = read.replaceAll("</coursepath>", "");
               coursePath = StringSplitter.splitString(read.trim(), ",");
            }
            read = courseXmlFileReader.readLine();
         }

         String chapter = null;
         String chapterName = null;
         Connection conn = null;
         PreparedStatement ps = null;
         ResultSet rs = null;
         String chapterId = null;
         Map<String, String> chapterDirIdMap = new HashMap<String, String>();
         for (int i = 0; i < coursePath.length; i++)
         {
            chapter = coursePath[i];
            chapterName = chapterNameDirMap.get(chapter);
            conn = getDbConnection();
            ps = getStatement(conn);
            try
            {
               ps.setString(1, "%" + chapterName + "%");
               rs = ps.executeQuery();
               if (rs.next())
               {
                  chapterId = rs.getString("chapterid");
                  chapterDirIdMap.put(chapter, chapterId);
               }
            }
            catch (SQLException e)
            {
            }
            finally
            {
               if (rs != null)
               {
                  try
                  {
                     rs.close();
                  }
                  catch (Exception e)
                  {
                     e.printStackTrace();
                  }
               }
               try
               {
                  ps.close();
               }
               catch (Exception e)
               {
                  e.printStackTrace();
               }
               try
               {
                  conn.close();
               }
               catch (Exception e)
               {
                  e.printStackTrace();
               }
            }
         }

         for (int i = 0; i < coursePath.length; i++)
         {
            chapter = coursePath[i];
            chapterId = chapterDirIdMap.get(chapter);
            chapterName = chapterNameDirMap.get(chapter);
            conn = getDbConnection();
            ps = getInsertStatement(conn);
            try
            {
               ps.setString(1, "COU20000000137751142");
               ps.setString(2, chapterId);
               ps.setInt(3, i + 1);
               ps.setString(4, null);
               int rows = ps.executeUpdate();
               System.out.println(rows + " rows inserted in db while inserting chapter " + chapter + " " + chapterName + " " + chapterId);
            }
            catch (SQLException e)
            {
               e.printStackTrace();
            }
            finally
            {
               try
               {
                  ps.close();
               }
               catch (Exception e)
               {
                  e.printStackTrace();
               }
               try
               {
                  conn.close();
               }
               catch (Exception e)
               {
                  e.printStackTrace();
               }
            }
         }
      }
      finally
      {
         chapterFileReader.close();
         courseXmlFileReader.close();
      }
   }

   private static PreparedStatement getStatement(Connection conn)
   {
      try
      {
         return (conn.prepareStatement("select chapterid from chapterlist where chaptername like ?"));
      }
      catch (Exception e)
      {
         System.exit(1);
         return null;
      }
   }

   private static PreparedStatement getInsertStatement(Connection conn)
   {
      try
      {
         return (conn.prepareStatement("insert into coursepath (courseid,chapterid,sequenceno,testid) values (?,?,?,?)"));
      }
      catch (Exception e)
      {
         System.exit(1);
         return null;
      }
   }


   private static Connection getDbConnection()
   {
      try
      {
         String url = "jdbc:postgresql://localhost:15432/ezed";
         Class.forName("org.postgresql.Driver");
         return DriverManager.getConnection(url, "postgres", "postgres");
      }
      catch (Exception e)
      {
         System.exit(1);
         return null;
      }
   }
}
