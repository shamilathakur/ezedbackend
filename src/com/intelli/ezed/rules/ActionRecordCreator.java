package com.intelli.ezed.rules;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.ActionData;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.MasterActionRecords;

public class ActionRecordCreator extends SingletonRule
{

   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);


   public void initActions()
   {
      MasterActionRecords.getLock();
      String dbConnName = "ezeddb";
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchActionDetails");
      ResultSet rs = null;
      MasterActionRecords.refreshRecords();

      ActionData actionData = null;
      try
      {
         rs = ps.executeQuery();
         while (rs.next())
         {
            actionData = new ActionData();
            actionData.setActionId(rs.getString(1));
            actionData.setDesc(rs.getString(2));
            actionData.setPoints(rs.getInt(3));
            actionData.setActionType(rs.getInt(4));
            actionData.setActionLog(rs.getString(5));
            MasterActionRecords.addRecord(actionData);
         }
      }
      catch (SQLException e)
      {
         logger.error("Commissions could not be initialized", e);
      }
      catch (Exception e)
      {
         logger.error("Error while initializing commissions", e);
      }
      finally
      {
         MasterActionRecords.releaseLock();
         try
         {
            rs.close();
         }
         catch (SQLException e)
         {
            logger.error("Result set could not be closed after populating transaction tablet", e);
         }
         catch (Exception e)
         {
            logger.error("Result set could not be closed", e);
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
   
   public ActionRecordCreator(String ruleName)
   {
      super(ruleName);
      initActions();
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      initActions();
      return RuleDecisionKey.ON_SUCCESS;
   }
}
