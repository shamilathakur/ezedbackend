package com.intelli.ezed.analytics.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.slf4j.Logger;

public class LastMarkData
{
   private Date lastDayBackupDate;
   private Date lastWeekBackupDate;
   private Date lastMonthBackupDate;
   private Date lastSixMonthBackupDate;

   public Date getLastDayBackupDate()
   {
      return lastDayBackupDate;
   }
   public void setLastDayBackupDate(Date lastDayBackupDate)
   {
      this.lastDayBackupDate = lastDayBackupDate;
   }
   public Date getLastWeekBackupDate()
   {
      return lastWeekBackupDate;
   }
   public void setLastWeekBackupDate(Date lastWeekBackupDate)
   {
      this.lastWeekBackupDate = lastWeekBackupDate;
   }
   public Date getLastMonthBackupDate()
   {
      return lastMonthBackupDate;
   }
   public void setLastMonthBackupDate(Date lastMonthBackupDate)
   {
      this.lastMonthBackupDate = lastMonthBackupDate;
   }
   public Date getLastSixMonthBackupDate()
   {
      return lastSixMonthBackupDate;
   }
   public void setLastSixMonthBackupDate(Date lastSixMonthBackupDate)
   {
      this.lastSixMonthBackupDate = lastSixMonthBackupDate;
   }

   public void populateBackupDatesFromDB(String dbConnName, Logger logger) throws SQLException, Exception
   {
      SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchBackupDates");
         rs = ps.executeQuery();
         if (rs.next())
         {
            String lastDay = rs.getString(1);
            String lastWeek = rs.getString(2);
            String lastMonth = rs.getString(3);
            String lastSixMonths = rs.getString(4);
            setLastDayBackupDate(sdf.parse(lastDay));
            setLastWeekBackupDate(sdf.parse(lastWeek));
            setLastMonthBackupDate(sdf.parse(lastMonth));
            setLastSixMonthBackupDate(sdf.parse(lastSixMonths));
            if (logger.isDebugEnabled())
            {
               logger.debug("Successfully fetched previous backup dates from DB");
            }
         }
         else
         {
            logger.error("No records found in db for backup dates");
            throw new Exception("No records found in db for backup dates");
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching previous backup dates from DB", e);
         throw e;
      }
      catch (ParseException e)
      {
         logger.error("Error while fetching previous backup dates from DB", e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching previous backup dates from DB", e);
         throw e;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
               logger.error("Error while closing result set while fetching backup dates");
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   public void persistBackupDatesInDB(String dbConnName, Logger logger) throws SQLException, Exception
   {
      SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertBackupDates");
         ps.setString(1, sdf.format(lastDayBackupDate));
         ps.setString(2, sdf.format(lastWeekBackupDate));
         ps.setString(3, sdf.format(lastMonthBackupDate));
         ps.setString(4, sdf.format(lastSixMonthBackupDate));
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in db while inserting entry for backup dates");
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while inserting backup dates in DB", e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while inserting backup dates in DB", e);
         throw e;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
}
