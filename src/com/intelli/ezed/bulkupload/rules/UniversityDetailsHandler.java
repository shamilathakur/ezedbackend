package com.intelli.ezed.bulkupload.rules;

import java.io.File;
import java.io.IOException;

import org.apache.commons.digester3.Digester;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import com.intelli.ezed.bulkupload.data.UniversityDetails;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;

public class UniversityDetailsHandler extends SingletonRule
{
   private static Object lockObject = new Object();
   private Digester univDigester = new Digester();
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.BULK_UPLOAD_LOG);
   private String univDetailsFileName;
   private String dbConnName;

   public UniversityDetailsHandler(String ruleName, String dbConnName, String univDetailsClassFQCN,
                                   String univDetailsFileName)
   {
      super(ruleName);
      initDigester(univDetailsClassFQCN);
      this.univDetailsFileName = univDetailsFileName;
      this.dbConnName = dbConnName;
   }

   private void initDigester(String univDetailsClassFQCN)
   {
      univDigester.setValidating(false);
      univDigester.addObjectCreate("universitydetails", univDetailsClassFQCN);
      univDigester.addCallMethod("universitydetails/country", "setCountry", 0);
      univDigester.addCallMethod("universitydetails/state", "setState", 0);
      univDigester.addCallMethod("universitydetails/city", "setCity", 0);
      univDigester.addCallMethod("universitydetails/university", "setUniversity", 0);
      univDigester.addCallMethod("universitydetails/website", "setWebsite", 0);
      univDigester.addCallMethod("universitydetails/phoneno", "setPhoneno", 0);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      File univFile = (File) flowContext.getFromContext(EzedKeyConstants.BULK_UPLOAD_UNIVFILE);
      try
      {
         String univPropFileName = univFile.getAbsolutePath() + "/" + this.univDetailsFileName;
         UniversityDetails univDetails = null;
         synchronized (lockObject)
         {
            univDetails = (UniversityDetails) univDigester.parse(new File(univPropFileName));
            univDetails.printDetails(logger);
            if (!univDetails.isUniversityPresent(dbConnName, logger))
            {
               univDetails.setCityIdCalculated(dbConnName, logger);
               univDetails.persistUniversity(dbConnName, logger);
            }
            flowContext.putIntoContext(EzedKeyConstants.BULK_UPLOAD_UNIVOBJECT, univDetails);
         }
      }
      catch (IOException e)
      {
         logger.error("Error while reading univ details properties file", e);
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (SAXException e)
      {
         logger.error("Error while reading univ details properties file", e);
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while persisting/reading univ details properties in/from db", e);
         return RuleDecisionKey.ON_FAILURE;
      }
      return RuleDecisionKey.ON_SUCCESS;
   }

}
