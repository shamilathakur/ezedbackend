package com.intelli.ezed.data;

public class ActionData
{
   public static final int REPEATABLE_ACTION = 1;
   public static final int ONE_TIME_ACTION = 2;

   private String actionId;
   private String desc;
   private int points;
   private int actionType;
   private String actionLog;

   public String getActionLog()
   {
      return actionLog;
   }
   public void setActionLog(String actionLog)
   {
      this.actionLog = actionLog;
   }
   public int getActionType()
   {
      return actionType;
   }
   public void setActionType(int actionType)
   {
      this.actionType = actionType;
   }
   public int getPoints()
   {
      return points;
   }
   public void setPoints(int points)
   {
      this.points = points;
   }
   public String getActionId()
   {
      return actionId;
   }
   public void setActionId(String actionId)
   {
      this.actionId = actionId;
   }
   public String getDesc()
   {
      return desc;
   }
   public void setDesc(String desc)
   {
      this.desc = desc;
   }
}
