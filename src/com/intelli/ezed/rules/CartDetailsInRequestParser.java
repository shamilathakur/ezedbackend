package com.intelli.ezed.rules;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.CartData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class CartDetailsInRequestParser extends SingletonRule
{

   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public CartDetailsInRequestParser(String ruleName)
   {
      super(ruleName);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      String couponCode = messageData.getParameter("couponcode");
      if (couponCode == null || couponCode.trim().compareTo("") == 0)
      {
         couponCode = null;
      }
      else
      {
         couponCode = couponCode.trim();
      }
      flowContext.putIntoContext(EzedKeyConstants.DISCOUNT_COUPON, couponCode);

      String token = messageData.getParameter("cartcontentno"); // where do you get cartcontentno from?
      if (token == null || token.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("No of contents in cart is missing");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "No of contents in cart is missing");
         return RuleDecisionKey.ON_FAILURE;
      }

      int cartContentNo = Integer.parseInt(token);
      if (cartContentNo == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Cart is empty");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Cart is empty");
         return RuleDecisionKey.ON_FAILURE;
      }

      CartData[] cart = new CartData[cartContentNo];
      String singleCourseName = null;
      String singleCourseType = null;
      String singleStartDate = null;
      Date startDate = null;
      SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
      for (int i = 0; i < cartContentNo; i++)
      {
         singleCourseName = messageData.getParameter("courseid" + Integer.toString(i + 1));
         if (singleCourseName == null || singleCourseName.trim().compareTo("") == 0)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Course name is missing for course no " + (i + 1));
            }
            response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Course name is invalid for course no " + (i + 1));
            return RuleDecisionKey.ON_FAILURE;
         }

         singleCourseType = messageData.getParameter("courseType" + Integer.toString(i + 1));// why is i+1 added to single course type?
         if (singleCourseType == null || singleCourseType.trim().compareTo("") == 0)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Course type is missing for course no " + (i + 1));
            }
            response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Course type is invalid for course no " + (i + 1));
            return RuleDecisionKey.ON_FAILURE;
         }

         singleStartDate = messageData.getParameter("startdate" + Integer.toString(i + 1));
         if (singleStartDate == null || singleStartDate.trim().compareTo("") == 0)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Start date is missing for course no " + (i + 1));
            }
            response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Start date is invalid for course no " + (i + 1));
            return RuleDecisionKey.ON_FAILURE;
         }

         try
         {
            startDate = sdf.parse(singleStartDate);
         }
         catch (ParseException e)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Start date is invalid for course no " + (i + 1));
            }
            response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Start date is invalid for course no " + (i + 1));
            return RuleDecisionKey.ON_FAILURE;
         }
         cart[i] = new CartData(singleCourseName, singleCourseType, startDate);
      }
      flowContext.putIntoContext(EzedKeyConstants.CART_DETAILS, cart);
      flowContext.putIntoContext(
				EzedKeyConstants.PAYMENT_ID_FOR_TRANSACTION_TYPE,
				EzedKeyConstants.PAYMENT_ID_FOR_TRANSACTION_TYPE_ONLINE_PAYMENT);
      return RuleDecisionKey.ON_SUCCESS;
   }

}
