package com.intelli.ezed.dropdowns;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.dropdowns.data.DropDownListData;
import com.intelli.ezed.dropdowns.data.SingleDropDownData;

public class ChapterListForCourseFetcher extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public ChapterListForCourseFetcher(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      String courseId = messageData.getParameter("courseid");
      if (courseId == null || courseId.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Course id is invalid in request " + courseId);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Course id is invalid");
         return RuleDecisionKey.ON_FAILURE;
      }

      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      DropDownListData dropDownListData = new DropDownListData();
      dropDownListData.setName("chapterlist");

      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchChapterNameIDForCourse");
         ps.setString(1, courseId);
         rs = ps.executeQuery();
         SingleDropDownData data = null;
         while (rs.next())
         {
            data = new SingleDropDownData();
            data.setParam(rs.getString("chapterid"));
            data.setValue(rs.getString("chaptername"));
            dropDownListData.getDropDownDatas().add(data);
         }
         response.setStatusCodeSend(false);
         response.setResponseObject(dropDownListData);
         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
         return RuleDecisionKey.ON_SUCCESS;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching chapter names for courseid " + courseId, e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching chapter names");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing resultset while fetching chapter names for course for dropdown", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
}
