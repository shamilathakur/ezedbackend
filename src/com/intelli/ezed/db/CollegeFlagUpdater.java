package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class CollegeFlagUpdater extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public CollegeFlagUpdater(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      //      response.setStringOnlySend(true);

      String collegeStatus = messageData.getParameter("status");
      if (collegeStatus == null || collegeStatus.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("New college status is invalid " + collegeStatus);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Status to be updated not found");
         return RuleDecisionKey.ON_FAILURE;
      }

      int collegeStatusInt = 0;
      if (collegeStatus.trim().compareToIgnoreCase("confirmed") == 0)
      {
         collegeStatusInt = 1;
      }

      String collegename = messageData.getParameter("college");
      if (collegename == null || collegename.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("College name in request is not proper " + collegename);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "College name is invalid");
         return RuleDecisionKey.ON_FAILURE;
      }

      String univname = messageData.getParameter("university");
      if (univname == null || univname.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("University name in request is not proper " + univname);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "University name is invalid");
         return RuleDecisionKey.ON_FAILURE;
      }

      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdateCollegeStatus");
         ps.setInt(1, collegeStatusInt);
         ps.setString(2, univname);
         ps.setString(3, collegename);
         int rows = ps.executeUpdate();
         if (rows > 0)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug(rows + " rows updated in db while updating college status for id " + collegename);
            }
            response.setParameters(EzedTransactionCodes.SUCCESS, collegeStatus);
            return RuleDecisionKey.ON_SUCCESS;
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("College status cannot be updated in request for college id " + collegename);
            }
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "College flag could not be updated");
            return RuleDecisionKey.ON_FAILURE;
         }
      }
      catch (SQLException e)
      {
         logger.error("DB error while updating college status for id " + collegename, e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "College flag could not be updated");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while updating college status for id " + collegename, e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "College flag could not be updated");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
}
