package com.intelli.ezed.checks;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.PartnerData;
import com.intelli.ezed.data.Response;

public class UpdatePartnerDetailsRequestParser extends SingletonRule {
	static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

	public UpdatePartnerDetailsRequestParser(String ruleName) {
		super(ruleName);
	}

	@Override
	public RuleDecisionKey executeRule(FlowContext flowContext) {
		MessageData messageData = (MessageData) flowContext
				.getFromContext(MessageData.class.getName());
		Response response = (Response) flowContext
				.getFromContext(EzedKeyConstants.EZED_RESPONSE);
		PartnerData data = new PartnerData();

		String partnerId = messageData.getParameter("partnerid");
		if (partnerId == null || partnerId.trim().compareTo("") == 0
				|| partnerId.trim().compareToIgnoreCase("null") == 0) {
			if (logger.isDebugEnabled()) {
				logger.debug("PartnerId in profile is invalid");
			}
			response.setParameters(
					EzedTransactionCodes.INVALID_MESSAGE_PARAMETER,
					"Partner id is invalid");
			return RuleDecisionKey.ON_FAILURE;
		}
		data.setPartnerId(partnerId);

		String contactPerson = messageData.getParameter("contactperson");
		if (contactPerson == null || contactPerson.trim().compareTo("") == 0
				|| contactPerson.trim().compareToIgnoreCase("null") == 0) {
			if (logger.isDebugEnabled()) {
				logger.debug("Contact Person in profile is invalid");
			}
			response.setParameters(
					EzedTransactionCodes.INVALID_MESSAGE_PARAMETER,
					"Contact Person is invalid");
			return RuleDecisionKey.ON_FAILURE;
		}
		data.setContactPerson(contactPerson);

		String partnerName = messageData.getParameter("partnername");
		if (partnerName == null || partnerName.trim().compareTo("") == 0
				|| partnerName.trim().compareToIgnoreCase("null") == 0) {
			if (logger.isDebugEnabled()) {
				logger.debug("Book store name in partner profile is invalid");
			}
			response.setParameters(
					EzedTransactionCodes.INVALID_MESSAGE_PARAMETER,
					"Book store name is invalid");
			return RuleDecisionKey.ON_FAILURE;
		}
		data.setPartnerName(partnerName);

		String address = messageData.getParameter("address");
		if (address == null || address.trim().compareTo("") == 0
				|| address.trim().compareToIgnoreCase("null") == 0) {
			if (logger.isDebugEnabled()) {
				logger.debug("Address in partner profile is invalid");
			}
			response.setParameters(
					EzedTransactionCodes.INVALID_MESSAGE_PARAMETER,
					"Address is invalid");
			return RuleDecisionKey.ON_FAILURE;
		}
		data.setAddress(address);

		String state = messageData.getParameter("state");
		if (state == null || state.trim().compareTo("") == 0
				|| state.trim().compareToIgnoreCase("null") == 0) {
			if (logger.isDebugEnabled()) {
				logger.debug("State in partner profile is invalid");
			}
			response.setParameters(
					EzedTransactionCodes.INVALID_MESSAGE_PARAMETER,
					"State is invalid");
			return RuleDecisionKey.ON_FAILURE;
		}
		data.setState(state);

		String city = messageData.getParameter("city");
		if (city == null || city.trim().compareTo("") == 0
				|| city.trim().compareToIgnoreCase("null") == 0) {
			if (logger.isDebugEnabled()) {
				logger.debug("City in partner profile is invalid");
			}
			response.setParameters(
					EzedTransactionCodes.INVALID_MESSAGE_PARAMETER,
					"City is invalid");
			return RuleDecisionKey.ON_FAILURE;
		}
		data.setCity(city);

		String contactNumber = messageData.getParameter("contactnumber") + "";
		if (contactNumber == null || contactNumber.trim().compareTo("") == 0
				|| contactNumber.trim().compareToIgnoreCase("null") == 0) {
			if (logger.isDebugEnabled()) {
				logger.debug("Contact number in partner profile is invalid");
			}
			response.setParameters(
					EzedTransactionCodes.INVALID_MESSAGE_PARAMETER,
					"Contact number is invalid");
			return RuleDecisionKey.ON_FAILURE;
		}
		data.setContactNumber(contactNumber);

		String status = messageData.getParameter("status") + "";
		if (status == null || status.trim().compareTo("") == 0
				|| status.trim().compareToIgnoreCase("null") == 0) {
			if (logger.isDebugEnabled()) {
				logger.debug("Status in partner profile is invalid");
			}
			response.setParameters(
					EzedTransactionCodes.INVALID_MESSAGE_PARAMETER,
					"Status is invalid");
			return RuleDecisionKey.ON_FAILURE;
		}
		int status1 = Integer.parseInt(status);
		data.setStatus(status1);

		String country = messageData.getParameter("country");
		if (country == null || country.trim().compareTo("") == 0
				|| country.trim().compareToIgnoreCase("null") == 0) {
			if (logger.isDebugEnabled()) {
				logger.debug("country in partner profile is invalid");
			}
			response.setParameters(
					EzedTransactionCodes.INVALID_MESSAGE_PARAMETER,
					"country is invalid");
			return RuleDecisionKey.ON_FAILURE;
		}
		data.setCountry(country);

		String emailaddress = messageData.getParameter("emailaddress");
		if (emailaddress == null || emailaddress.trim().compareTo("") == 0
				|| emailaddress.trim().compareToIgnoreCase("null") == 0) {
			if (logger.isDebugEnabled()) {
				logger.debug("emailaddress in partner profile is invalid");
			}
			response.setParameters(
					EzedTransactionCodes.INVALID_MESSAGE_PARAMETER,
					"emailaddress is invalid");
			return RuleDecisionKey.ON_FAILURE;
		}
		data.setEmailaddress(emailaddress);

		String flag1 = messageData.getParameter("flag");
		if (flag1 == null || flag1.trim().compareTo("") == 0
				|| flag1.trim().compareToIgnoreCase("null") == 0) {
			if (logger.isDebugEnabled()) {
				logger.debug("Flag in update partner profile is invalid");
			}
			response.setParameters(
					EzedTransactionCodes.INVALID_MESSAGE_PARAMETER,
					"Flag is invalid");
			return RuleDecisionKey.ON_FAILURE;
		}
		int flag = Integer.parseInt(flag1);

		String lastmodified = sdf.format(new Date());
		data.setLastmodified(lastmodified);

		flowContext.putIntoContext(EzedKeyConstants.PARTNER_DATA_OBJECT, data);
		flowContext.putIntoContext("flag", flag);

		System.out.println("Going to the updater ..............!!!!!!!");
		return RuleDecisionKey.ON_SUCCESS;
	}
}