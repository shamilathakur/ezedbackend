package com.intelli.ezed.utils.upload;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.ezed.utils.dbutil.DataFiller;

public class CityUploader
{

   public static void main(String[] args)
   {
      Connection conn = getDbConnectionPostgres();
      PreparedStatement ps = getSelectPostStatement(conn);

      String[] cities = {"MUMBAI", "PUNE", "AURANGABAD", "NAGPUR", "NASHIK", "KOLHAPUR", "THANE", "RATNAGIRI", "SANGLI", "SOLAPUR", "AMRAVATI", "BULDHANA", "CHANDRAPUR", "JALGAON"};
      try
      {
         ps = getSelectPostStatement(conn);
         for (int i = 0; i < cities.length; i++)
         {
            ps.setString(1, "INDIA");
            ps.setString(2, "MAHARASHTRA");
            ps.setString(3, cities[i]);
            ps.setString(4, "CIT1" + DataFiller.getLeftZeroPadding(Integer.toString(i + 2), 6));
            int rows = ps.executeUpdate();
         }
      }
      catch (SQLException e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
   }

   private static Connection getDbConnectionPostgres()
   {
      try
      {
         String url = "jdbc:postgresql://localhost:15432/ezed";
         Class.forName("org.postgresql.Driver");
         return DriverManager.getConnection(url, "postgres", "postgres");
      }
      catch (Exception e)
      {
         e.printStackTrace();
         System.out.println("Error initialising Postgres connection");
         System.exit(1);
         return null;
      }
   }

   private static PreparedStatement getSelectPostStatement(Connection conn)
   {
      try
      {
         return (conn.prepareStatement("insert into citylist values (?,?,?,?)"));
      }
      catch (Exception e)
      {
         System.out.println("Error initializing postgres statment");
         System.exit(1);
         return null;
      }
   }
}
