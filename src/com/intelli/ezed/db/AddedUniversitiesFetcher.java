package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.FrontendFetchData;
import com.intelli.ezed.data.Response;

public class AddedUniversitiesFetcher extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private SimpleDateFormat sdfDb = new SimpleDateFormat("yyyyMMddHHmmss");
   private SimpleDateFormat sdfPage = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

   public AddedUniversitiesFetcher(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      String[] univArr = null;
      FrontendFetchData univData = new FrontendFetchData();
      response.setResponseObject(univData);
      response.setStatusCodeSend(false);
      univData.setAttribName("aaData");
      ArrayList<String[]> universities = new ArrayList<String[]>();
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchUserAddedUniv");
         rs = ps.executeQuery();
         int i = 1;
         int showFlag = 0;
         while (rs.next())
         {
            univArr = new String[5];
            univArr[0] = Integer.toString(i);
            univArr[1] = rs.getString("univname");
            univArr[2] = rs.getString("addedby");
            try
            {
               univArr[3] = sdfPage.format(sdfDb.parse(rs.getString("createtime")));
            }
            catch (Exception e)
            {
               univArr[3] = "";
            }
            showFlag = rs.getInt("showflag");
            if (showFlag == 1)
            {
               univArr[4] = "Confirmed";
            }
            else
            {
               univArr[4] = "Unconfirmed";
            }
            i++;
            universities.add(univArr);
         }
         univData.setAttribList(universities);
         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
      }
      catch (SQLException e)
      {
         logger.error("DB error while fetching list of universities", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "DB error while fetching universities");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching list of universities", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching universities");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing result set while fetching university list", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
      return RuleDecisionKey.ON_SUCCESS;
   }
}
