package com.intelli.ezed.db;

import java.util.HashMap;
import java.util.Map;

import org.sabre.exceptions.InputProcessorException;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.LinkedProcessorManager;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.GoalModificationData;
import com.intelli.ezed.data.Response;

public class NewChapterInCoursePathHandler extends SingletonRule
{

   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String goalAdditionProcessor;

   public NewChapterInCoursePathHandler(String ruleName, String goalAdditionProcessor)
   {
      super(ruleName);
      this.goalAdditionProcessor = goalAdditionProcessor;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      String courseId = messageData.getParameter("courseid");
      if (courseId == null || courseId.trim().compareTo("") == 0 || courseId.trim().compareTo("null") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Course id is invalid " + courseId);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid course id");
         return RuleDecisionKey.ON_FAILURE;
      }

      String chapterId = messageData.getParameter("chapterid");
      if (chapterId == null || chapterId.trim().compareTo("") == 0 || chapterId.trim().compareTo("null") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Chapter/test id is invalid " + courseId);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid chapter id");
         return RuleDecisionKey.ON_FAILURE;
      }

      String flag = messageData.getParameter("flag");
      String testId = null;
      if (flag != null)
      {
         testId = chapterId;
      }

      GoalModificationData data = new GoalModificationData();
      if (testId == null)
      {
         data.setChapterId(chapterId);
         data.setTestId(null);
      }
      else
      {
         data.setChapterId(null);
         data.setTestId(testId);
      }
      data.getCourseIdList().add(courseId);

      Map<Object, Object> mapData = new HashMap<Object, Object>();
      mapData.put(EzedKeyConstants.GOAL_MODIFICATION_DATA, data);
      try
      {
         LinkedProcessorManager.addTask(goalAdditionProcessor, mapData);
      }
      catch (InputProcessorException e)
      {
         logger.error("Error while starting goal addition processor", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error occurred. Pls try again later");
         return RuleDecisionKey.ON_FAILURE;
      }
      response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
      return RuleDecisionKey.ON_SUCCESS;
   }
}
