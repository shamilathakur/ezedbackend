package com.intelli.ezed.rules;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.UniversityData;

public class AddOtherUniversityRequestParser extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
   private String dbConnName;

   public AddOtherUniversityRequestParser(String ruleName, String dbconnName)
   {
      super(ruleName);
      this.dbConnName = dbconnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      UniversityData univData = new UniversityData();
      
     /* String country = messageData.getParameter("country");
      if (country == null || country.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Country name in request in null or blank");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid country");
         return RuleDecisionKey.ON_FAILURE;
      }
      
      univData.setCountryName(country);
      
      String state = messageData.getParameter("state");
      if (state == null || state.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("State name in request in null or blank");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid state");
         return RuleDecisionKey.ON_FAILURE;
      }
      univData.setStateName(state);*/
      
      String city = messageData.getParameter("city");
      if (city == null || city.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("City name in request in null or blank");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid city");
         return RuleDecisionKey.ON_FAILURE;
      }
      univData.setCityName(city);
      
      String univName = messageData.getParameter("univName");
      if (univName == null || univName.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("University name in request in null or blank");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid university name");
         return RuleDecisionKey.ON_FAILURE;
      }
      univData.setUnivName(univName);
      
      String phone = messageData.getParameter("phone");
      if (phone == null || phone.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("University phone number in request in null or blank");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid university phone numbers");
         return RuleDecisionKey.ON_FAILURE;
      }
      univData.setPhoneNo(phone);
      
      String website = messageData.getParameter("website");
      if (website == null || website.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("University website name in request in null or blank");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid university website");
         return RuleDecisionKey.ON_FAILURE;
      }
      univData.setWebsite(website);
      
      String createDateTime = sdf.format(new Date());
      univData.setCreatedOn(createDateTime);
      
      String createdBy = messageData.getParameter("email");
      univData.setCreatedBy(createdBy);
      
      
      univData.setShowFlag(EzedKeyConstants.UNIVERSITY_ACTIVE);
      System.out.println("Showflag() in Request Parser:"+EzedKeyConstants.UNIVERSITY_ACTIVE);
      univData.setUserFlag(EzedKeyConstants.UNIVERSITY_ADDED_BY_USER);
      System.out.println("ShowUserFlag() in Request Parser:"+EzedKeyConstants.UNIVERSITY_ADDED_BY_USER);
      flowContext.putIntoContext(EzedKeyConstants.UNIVERSITY_DATA, univData);
      return RuleDecisionKey.ON_SUCCESS;
   }
}
