package com.intelli.ezed.bulkupload.rules;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import org.sabre.exceptions.InputProcessorException;
import org.sabre.io.transport.http.data.HttpTransactionData;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.LinkedProcessorManager;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;

public class BulkUploadInitiator extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.BULK_UPLOAD_LOG);
   private String univUploadPath;
   private String contentUploadPath;
   private AtomicInteger contentCount = new AtomicInteger(0);
   private Map<String, String> contentDirIdMap = new HashMap<String, String>();
   private Map<String, String> quizDirIdMap = new HashMap<String, String>();
   private AtomicInteger quizCount = new AtomicInteger(0);

   public BulkUploadInitiator(String ruleName)
   {
      super(ruleName);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      HttpTransactionData transData = (HttpTransactionData) flowContext.getFromContext(HttpTransactionData.READ_REQ_DATA);
      if (transData == null || transData.getParaMap() == null || transData.getParaMap().size() == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Improper bulk upload request");
         }
         respondFailure(flowContext);
         return RuleDecisionKey.ON_FAILURE;
      }

      String univUploadPath = transData.getParaMap().get("univuploadpath");
      if (univUploadPath == null || univUploadPath.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Improper univ upload path in request");
         }
         respondFailure(flowContext);
         return RuleDecisionKey.ON_FAILURE;
      }

      String contentUploadPath = transData.getParaMap().get("contentuploadpath");
      if (contentUploadPath == null || contentUploadPath.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Improper content upload path in request");
         }
         respondFailure(flowContext);
         return RuleDecisionKey.ON_FAILURE;
      }

      this.univUploadPath = univUploadPath;
      this.contentUploadPath = contentUploadPath;

      processContents();

      respondSuccess(flowContext);
      return RuleDecisionKey.ON_SUCCESS;
   }

   private void respondFailure(FlowContext flowContext)
   {
      HttpTransactionData transData = new HttpTransactionData();
      transData.setContent("1".getBytes());
      transData.setRequestType(HttpTransactionData.POST);
      flowContext.putIntoContext(HttpTransactionData.WRITE_RESP_DATA, transData);
   }

   private void respondSuccess(FlowContext flowContext)
   {
      HttpTransactionData transData = new HttpTransactionData();
      transData.setContent("0".getBytes());
      transData.setRequestType(HttpTransactionData.POST);
      flowContext.putIntoContext(HttpTransactionData.WRITE_RESP_DATA, transData);
   }

   public void decrementContentCount()
   {
      int contentCount = this.contentCount.decrementAndGet();
      if (contentCount == 0)
      {
         this.processQuiz();
      }
   }

   public void decrementQuizCount()
   {
      int contentCount = this.quizCount.decrementAndGet();
      if (logger.isDebugEnabled())
      {
         logger.debug("Quiz count decremented. Current count : " + contentCount);
      }
      if (contentCount == 0)
      {
         this.processUniversity();
      }
   }

   public String putContentInMap(String dirName, String contentId)
   {
      return this.contentDirIdMap.put(dirName, contentId);
   }

   public String putQuizInMap(String dirName, String quizId)
   {
      quizCount.incrementAndGet();
      return this.quizDirIdMap.put(dirName, quizId);
   }

   public void processContents()
   {
      if (logger.isDebugEnabled())
      {
         logger.debug("Processing of contents started");
      }
      File file = new File(contentUploadPath);
      if (file.isDirectory())
      {
         File[] contentDirs = file.listFiles();
         Map<Object, Object> processorMap = null;
         this.contentCount.addAndGet(contentDirs.length);
         for (File singleDir : contentDirs)
         {
            processorMap = new HashMap<Object, Object>();
            processorMap.put(EzedKeyConstants.BULK_UPLOAD_CONTENTFILE, singleDir);
            processorMap.put(EzedKeyConstants.BULK_UPLOAD_STARTER, this);
            try
            {
               LinkedProcessorManager.addTask("BulkUploadContentHandler", processorMap);
            }
            catch (InputProcessorException e)
            {
               logger.error("Could not add task to processor", e);
               this.contentCount.decrementAndGet();
               //               respondFailure(flowContext);
               //               return RuleDecisionKey.ON_FAILURE;
            }
         }
      }
      else
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("absolute path of content is not a directory");
         }
         //         respondFailure(flowContext);
         //         return RuleDecisionKey.ON_FAILURE;
      }
   }

   public void processQuiz()
   {
      if (logger.isDebugEnabled())
      {
         logger.debug("Processing of quizzes started. Looking for a total of " + quizCount + " quizzes");
      }
      Set<Entry<String, String>> entrySet = quizDirIdMap.entrySet();
      Iterator<Entry<String, String>> iterator = entrySet.iterator();
      Entry<String, String> entry = null;
      Map<Object, Object> processorMap = null;
      while (iterator.hasNext())
      {
         entry = iterator.next();
         processorMap = new HashMap<Object, Object>();
         processorMap.put(EzedKeyConstants.BULK_UPLOAD_CONTENTFILE, entry.getKey());
         processorMap.put(EzedKeyConstants.BULK_UPLOAD_CONTENTID, entry.getValue());
         processorMap.put(EzedKeyConstants.BULK_UPLOAD_STARTER, this);
         processorMap.put(EzedKeyConstants.BULK_UPLOAD_CONTENTMAP, contentDirIdMap);
         try
         {
            LinkedProcessorManager.addTask("BulkUploadQuizHandler", processorMap);
         }
         catch (InputProcessorException e)
         {
            logger.error("Could not add task to processor", e);
            //               respondFailure(flowContext);
            //               return RuleDecisionKey.ON_FAILURE;
         }
      }
   }

   public void processUniversity()
   {
      if (logger.isDebugEnabled())
      {
         logger.debug("Processing of universities started");
      }
      File file = new File(univUploadPath);
      if (file.isDirectory())
      {
         File[] univDirs = file.listFiles();
         Map<Object, Object> processorMap = null;
         for (File singleDir : univDirs)
         {
            processorMap = new HashMap<Object, Object>();
            processorMap.put(EzedKeyConstants.BULK_UPLOAD_UNIVFILE, singleDir);
            processorMap.put(EzedKeyConstants.BULK_UPLOAD_CONTENTMAP, contentDirIdMap);
            try
            {
               LinkedProcessorManager.addTask("BulkUploadUnivHandler", processorMap);
            }
            catch (InputProcessorException e)
            {
               logger.error("Could not add task to processor", e);
               //               respondFailure(flowContext);
               //               return RuleDecisionKey.ON_FAILURE;
            }
         }
      }
      else
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("absolute path is not a directory");
         }
         //         respondFailure(flowContext);
         //         return RuleDecisionKey.ON_FAILURE;
      }
   }
}
