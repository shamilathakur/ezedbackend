package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.analytics.data.GenericChartData;
import com.intelli.ezed.data.CoursePercentagePointData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class CoursePointsPercentageFetcher extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public CoursePointsPercentageFetcher(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      String userId = (String) flowContext.getFromContext(EzedKeyConstants.USER_ID);
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      Connection conn = null;
      PreparedStatement ps = null;
      ResultSet rs = null;
      CoursePercentagePointData singleData = null;
      ArrayList<CoursePercentagePointData> datas = new ArrayList<CoursePercentagePointData>(10);
      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchCoursePointsPercentage");
         ps.setString(1, userId);
         rs = ps.executeQuery();
         while (rs.next())
         {
            singleData = new CoursePercentagePointData();
            singleData.setCourseId(rs.getString("courseid"));
            singleData.setCourseName(rs.getString("coursename"));
            singleData.setPoints(Integer.toString(rs.getInt("points")));
            singleData.setCompletionPercentage(Integer.toString(rs.getInt("percentcomplete")));
            singleData.setCourseImage(rs.getString("courseimage"));
            datas.add(singleData);
         }
         JSONArray array = new JSONArray();
         for (int i = 0; i < datas.size(); i++)
         {
            datas.get(i).prepareResponse();
            array.put(datas.get(i));
         }
         GenericChartData responseData = new GenericChartData();
         responseData.getText().add("courses");
         responseData.getValues().add(array);
         response.setResponseObject(responseData);
         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching course percentage and point details", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching course details");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching course percentage and point details", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching course details");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing resultset while fetching course points and completion percentage");
            }
            DbConnectionManager.releaseConnection(dbConnName, conn);
         }
      }
      return RuleDecisionKey.ON_SUCCESS;
   }

}
