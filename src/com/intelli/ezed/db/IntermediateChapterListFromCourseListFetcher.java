package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Iterator;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class IntermediateChapterListFromCourseListFetcher extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public IntermediateChapterListFromCourseListFetcher(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      //      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      String[] courseIdList = (String[]) flowContext.getFromContext(EzedKeyConstants.COURSE_ID_LIST);
      Connection conn = null;
      PreparedStatement ps = null;
      ResultSet rs = null;
      String courseId = null;
      HashSet<String> chapterList = new HashSet<String>();
      for (int j = 0; j < courseIdList.length; j++)
      {
         courseId = courseIdList[j];
         try
         {
            conn = DbConnectionManager.getConnectionByName(dbConnName);
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchCourseDetail");
            ps.setString(1, courseId);
            rs = ps.executeQuery();
            if (rs.next())
            {
               String defChapter = rs.getString("defchapter");
               if (defChapter != null && defChapter.compareTo("") != 0)
               {
                  chapterList.add(defChapter);
               }

               String lmrChapter = rs.getString("lmr");
               if (lmrChapter != null && lmrChapter.compareTo("") != 0)
               {
                  chapterList.add(lmrChapter);
               }

               String univQuestionPaper = rs.getString("questionpaperid");
               if (univQuestionPaper != null && univQuestionPaper.compareTo("") != 0)
               {
                  chapterList.add(univQuestionPaper);
               }

               String univQuestionSolution = rs.getString("solvedpaperid");
               if (univQuestionSolution != null && univQuestionSolution.compareTo("") != 0)
               {
                  chapterList.add(univQuestionSolution);
               }
            }
            else
            {
               if (logger.isDebugEnabled())
               {
                  logger.debug("Chapter details could not be found for courseid " + courseId);
               }
               response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Course details could not be found");
               return RuleDecisionKey.ON_FAILURE;
            }
         }
         catch (SQLException e)
         {
            logger.error("Error while fetching course chapter details", e);
            response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching course chapters");
            return RuleDecisionKey.ON_FAILURE;
         }
         catch (Exception e)
         {
            logger.error("Error while fetching course chapter details", e);
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching course chapters");
            return RuleDecisionKey.ON_FAILURE;
         }
         finally
         {
            if (rs != null)
            {
               try
               {
                  rs.close();
               }
               catch (SQLException e)
               {
               }
            }
            DbConnectionManager.releaseConnection(dbConnName, conn);
         }

         try
         {
            conn = DbConnectionManager.getConnectionByName(dbConnName);
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchCourseChapterWithDetail");
            ps.setString(1, courseId);
            rs = ps.executeQuery();
            while (rs.next())
            {

               chapterList.add(rs.getString("chapterid"));

            }
         }
         catch (SQLException e)
         {
            logger.error("Error while fetching course chapter details", e);
            response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching course chapters");
            return RuleDecisionKey.ON_FAILURE;
         }
         catch (Exception e)
         {
            logger.error("Error while fetching course chapter details", e);
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching course chapters");
            return RuleDecisionKey.ON_FAILURE;
         }
         finally
         {
            if (rs != null)
            {
               try
               {
                  rs.close();
               }
               catch (SQLException e)
               {
               }
            }
            DbConnectionManager.releaseConnection(dbConnName, conn);
         }
      }
      String[] chapterArr = new String[chapterList.size()];
      Iterator<String> chapterIte = chapterList.iterator();
      int i = 0;
      while (chapterIte.hasNext())
      {
         chapterArr[i] = chapterIte.next();
         i++;
      }
      flowContext.putIntoContext(EzedKeyConstants.CHAPTER_ID_LIST, chapterArr);
      return RuleDecisionKey.ON_SUCCESS;
   }
}
