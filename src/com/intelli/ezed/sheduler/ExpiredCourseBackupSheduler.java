package com.intelli.ezed.sheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.atomic.AtomicBoolean;

import org.sabre.db.DbConnectionManager;
import org.sabre.utils.threads.TimedTask;
import org.sabre.workflow.context.FlowContext;
import org.sabre.workflow.processor.InputProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedLoggerName;

public class ExpiredCourseBackupSheduler extends InputProcessor
{
   Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private Resetter resetter;
   private static AtomicBoolean shedulerRunning = new AtomicBoolean();

   public ExpiredCourseBackupSheduler(String startAtTime, String pollingTime, String dbConnName)
   {
      resetter = new Resetter(startAtTime, Long.parseLong(pollingTime), dbConnName);
      logger.debug("Expired course sheduler started.");
      shedulerRunning.set(true);
   }

   class Resetter extends TimedTask
   {
      SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
      private String dbConnName = null;
      public Resetter(String startAtTime, long pollingTime, String dbConnName)
      {
         super(startAtTime, pollingTime);
         this.dbConnName = dbConnName;
         this.setStartAtTime(startAtTime);
         this.setExecAtStartup(true);
         this.setOneTimeTask(false);
      }

      @Override
      public void doTimedTask()
      {
         if (shedulerRunning.get())
         {
            shedulerRunning.set(true);
            removeExpiredCourse();
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Sheduler already running. Skipping current shedule.");
            }
         }
      }
      private void removeExpiredCourse()
      {

         String date = sdf.format(new Date());
         Connection conn = null;
         PreparedStatement ps = null;

         try
         {
            conn = DbConnectionManager.getConnectionByName(dbConnName);
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "BackupCourseTable");
            ps.setString(1, date);
            ps.setString(2, date);

            int backupedCourses = ps.executeUpdate();

            if (logger.isDebugEnabled())
            {
               logger.debug("Rows selected to backup : " + backupedCourses + " coursed expired");
            }

            if (backupedCourses != 0)
            {
               ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "DeleteExpiredCourse");
               ps.setString(1, date);
               int deletedRows = ps.executeUpdate();

               if (logger.isDebugEnabled())
               {
                  logger.debug("Rows deleted : " + deletedRows + " coursed deleted");
               }

               ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "BackupUserPointsTable");
               ps.setString(1, date + "000000");
               ps.setString(2, date + "000000");
               int rows = ps.executeUpdate();

               if (logger.isDebugEnabled())
               {
                  logger.debug(rows + " rows inserted in userpointsbackup table while backing up expired user points");
               }

               if (rows != 0)
               {
                  ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "DeleteExpiredUserPoints");
                  ps.setString(1, date + "000000");
                  rows = ps.executeUpdate();

                  if (logger.isDebugEnabled())
                  {
                     logger.debug(rows + " rows deleted from userpoints table while deleting expired user points");
                  }
               }
            }
         }
         catch (SQLException e)
         {
            logger.error("Database Exception occoured while trying to backup expired courses ", e);
         }
         catch (Exception e)
         {
            logger.error("Exception occoured while trying to backup expired courses ", e);
         }
         finally
         {
            DbConnectionManager.releaseConnection(dbConnName, conn);
         }

         shedulerRunning.set(false);

      }
   }

   @Override
   public void initialize(FlowContext flowContext)
   {
   }

   @Override
   public void start()
   {
      resetter.startTask(true);
   }
}
