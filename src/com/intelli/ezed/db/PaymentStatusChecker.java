package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.utils.StringSplitter;
import com.intelli.ezed.data.CartData;
import com.intelli.ezed.data.CartDetails;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.PaymentData;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.sheduler.PurchasedCartUpdater;
import com.intelli.ezed.utils.PaymentLockManager;

public class PaymentStatusChecker extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private Logger cartLog = LoggerFactory.getLogger(EzedLoggerName.CART_LOG);

   public PaymentStatusChecker(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      PaymentData paydata = (PaymentData) flowContext.getFromContext(EzedKeyConstants.PAYMENT_DATA);
      if (paydata == null)
      {
         CartDetails cartObject = (CartDetails) flowContext.getFromContext(EzedKeyConstants.CART_DETAILS);
         paydata = new PaymentData();
         paydata.setAppId(cartObject.getAppId());
         paydata.setPaymentId(cartObject.getPaymentId());
      }
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      String userId = (String) flowContext.getFromContext(EzedKeyConstants.USER_ID);

      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "SelectCartDetails");
         ps.setString(1, paydata.getAppId());
         ps.setString(2, paydata.getPaymentId());
         rs = ps.executeQuery();
         if (rs.next())
         {
            String status = rs.getString("cartstatus");
            if (status.compareTo(PaymentData.CART_ADDED_TO_USER) == 0)
            {
               if (logger.isDebugEnabled())
               {
                  logger.debug("Cart is already paid for and added to user");
               }
               return RuleDecisionKey.ON_PARTIAL;
            }
            else if (status.compareTo(PaymentData.CART_PAYMENT_FAILED_TIMEOUT) == 0)
            {
               if (logger.isDebugEnabled())
               {
                  logger.debug("Cart payment had failed for user");
               }
               return RuleDecisionKey.ON_PARTIAL;
            }
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            CartDetails cartDetails = new CartDetails();
            String cartString = rs.getString("cart");
            cartDetails.setAppId(paydata.getAppId());
            cartDetails.setPaymentId(paydata.getPaymentId());
            CartData[] cartData = parseCartString(cartString, calendar);
            if (cartData == null)
            {
               if (logger.isDebugEnabled())
               {
                  logger.debug("Cart could not be processed for payment id " + paydata.getAppId() + paydata.getPaymentId());
               }
               PurchasedCartUpdater.logFailedCartProcessing(cartLog, userId, cartString, EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Cart String from DB not proper");
            }
            cartDetails.setCartDatas(cartData);
            flowContext.putIntoContext(EzedKeyConstants.CART_DETAILS, cartDetails);
            flowContext.putIntoContext(EzedKeyConstants.CART_STRING, cartString);
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Payment details could not be found for payment id " + paydata.getAppId() + paydata.getPaymentId());
            }
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Payment details could not be fetched");
            return RuleDecisionKey.ON_FAILURE;
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while checking cart status for payment id " + paydata.getAppId() + paydata.getPaymentId());
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while processing cart");
         PaymentLockManager.removeFromList(paydata.getAppId() + paydata.getPaymentId());
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while checking cart status for payment id " + paydata.getAppId() + paydata.getPaymentId());
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while processing cart");
         PaymentLockManager.removeFromList(paydata.getAppId() + paydata.getPaymentId());
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing result set while checking cart status");
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
      return RuleDecisionKey.ON_SUCCESS;
   }

   private CartData[] parseCartString(String cartString, Calendar todayCal)
   {
      SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
      Date courseStartDate = null;
      Date courseEndDate = null;
      if (cartString == null || cartString.trim().compareTo("") == 0)
      {
         return null;
      }
      String[] oneCartDetails = StringSplitter.splitString(cartString, ";");
      CartData[] cartData = new CartData[oneCartDetails.length];
      String[] cartConstructor = null;
      for (int i = 0; i < oneCartDetails.length; i++)
      {
         cartConstructor = StringSplitter.splitString(oneCartDetails[i], ",");
         if (cartConstructor == null || cartConstructor.length != 4)
         {
            return null;
         }
         try
         {
            courseStartDate = sdf.parse(cartConstructor[2]);
            Calendar startCal = Calendar.getInstance();
            startCal.setTime(courseStartDate);
            if (startCal.before(todayCal))
            {
               courseStartDate = todayCal.getTime();
            }

            courseEndDate = sdf.parse(cartConstructor[3]);
         }
         catch (ParseException e)
         {
            logger.error("Error while parsing start/end date from cart string", e);
            return null;
         }
         cartData[i] = new CartData(cartConstructor[0], cartConstructor[1], courseStartDate, courseEndDate);
      }
      return cartData;
   }
}
