package com.intelli.ezed.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.slf4j.Logger;

public class MasterDeleteUpdateHelper
{
   private static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

   public static void insertDeletedCountry(String dbConnName, Logger logger, String countryname, String userid) throws SQLException, Exception
   {
      //insert into deletedcountrylist (countryname,userid,deletetime) values (?,?,?);
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertDeleteCountry");
         ps.setString(1, countryname);
         ps.setString(2, userid);
         ps.setString(3, sdf.format(new Date()));
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in deleted country table while deleting country " + countryname);
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while inserting country in deleted country log " + countryname, e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while inserting country in deleted country log " + countryname, e);
         throw e;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   public static void insertDeletedStateForCountry(String dbConnName, Logger logger, String countryname, String userid) throws SQLException, Exception
   {
      //insert into deletedstatelist (countryname,statename,userid,deletetime,deleteaction) (select countryname,statename,?,?,? from statelist where countryname=?);
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertDeleteStateForCountry");
         ps.setString(1, userid);
         ps.setString(2, sdf.format(new Date()));
         ps.setString(3, "DELETE_COUNTRY");
         ps.setString(4, countryname);
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in deleted state table while deleting country " + countryname);
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while inserting states in deleted state log for country " + countryname, e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while inserting states in deleted state log for country " + countryname, e);
         throw e;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   public static void insertDeletedState(String dbConnName, Logger logger, String countryname, String statename, String userid) throws SQLException, Exception
   {
      //insert into deletedstatelist (countryname,statename,userid,deletetime,deleteaction) values (?,?,?,?,?);
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertDeleteState");
         ps.setString(1, countryname);
         ps.setString(2, statename);
         ps.setString(3, userid);
         ps.setString(4, sdf.format(new Date()));
         ps.setString(5, "DELETE_STATE");
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in deleted state table while deleting state " + statename);
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while inserting states in deleted state log for state " + statename, e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while inserting states in deleted state log for state " + statename, e);
         throw e;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   public static void insertDeletedCityForCountry(String dbConnName, Logger logger, String countryname, String userid) throws SQLException, Exception
   {
      //insert into deletedcitylist (countryname,statename,cityname,cityid,userid,deletetime,deleteaction) (select countryname,statename,cityname,cityid ?,?,? from citylist where countryname=?);
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertDeleteCityForCountry");
         ps.setString(1, userid);
         ps.setString(2, sdf.format(new Date()));
         ps.setString(3, "DELETE_COUNTRY");
         ps.setString(4, countryname);
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in deleted city table while deleting country " + countryname);
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while inserting cities in deleted city log for country " + countryname, e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while inserting cities in deleted city log for country " + countryname, e);
         throw e;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   public static void insertDeletedCityForState(String dbConnName, Logger logger, String countryname, String statename, String userid) throws SQLException, Exception
   {
      //insert into deletedcitylist (countryname,statename,cityname,cityid,userid,deletetime,deleteaction) (select countryname,statename,cityname,cityid ?,?,? from citylist where countryname=? and statename=?);
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertDeleteCityForState");
         ps.setString(1, userid);
         ps.setString(2, sdf.format(new Date()));
         ps.setString(3, "DELETE_STATE");
         ps.setString(4, countryname);
         ps.setString(5, statename);
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in deleted city table while deleting state " + statename);
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while inserting cities in deleted city log for state " + statename, e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while inserting cities in deleted city log for state " + statename, e);
         throw e;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   public static void insertDeletedCity(String dbConnName, Logger logger, String countryname, String statename, String cityname, String userid) throws SQLException, Exception
   {
      //insert into deletedcitylist (countryname,statename,cityname,cityid,userid,deletetime,deleteaction) (select countryname,statename,cityname,cityid,?,?,? from citylist where countryname=? and statename=? and cityname=?);
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertDeleteCity");
         ps.setString(1, userid);
         ps.setString(2, sdf.format(new Date()));
         ps.setString(3, "DELETE_CITY");
         ps.setString(4, countryname);
         ps.setString(5, statename);
         ps.setString(6, cityname);
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in deleted city table while deleting city " + cityname);
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while inserting cities in deleted city log for city " + cityname, e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while inserting cities in deleted city log for city " + cityname, e);
         throw e;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   public static void insertUpdatedUsersForCountry(String dbConnName, Logger logger, String countryname, String userid) throws SQLException, Exception
   {
      //insert into userupdatedeletesnapshot (userid,cityid,univname,collegename,adminuserid,deleteupdatetime,updatedeleteaction) (select userid,cityid,univname,collegename,?,?,? from users where cityid in(select cityid from citylist where countryname=?));
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertUpdateUserForCountry");
         ps.setString(1, userid);
         ps.setString(2, sdf.format(new Date()));
         ps.setString(3, "DELETE_COUNTRY");
         ps.setString(4, countryname);
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in updated users table while deleting country " + countryname);
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while inserting users in updated users log for country " + countryname, e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while inserting users in updated users log for country " + countryname, e);
         throw e;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   public static void insertUpdatedUsersForState(String dbConnName, Logger logger, String countryname, String statename, String userid) throws SQLException, Exception
   {
      //insert into userupdatedeletesnapshot (userid,cityid,univname,collegename,adminuserid,deleteupdatetime,updatedeleteaction) (select userid,cityid,univname,collegename,?,?,? from users where cityid in(select cityid from citylist where countryname=? and statename=?));
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertUpdateUserForState");
         ps.setString(1, userid);
         ps.setString(2, sdf.format(new Date()));
         ps.setString(3, "DELETE_STATE");
         ps.setString(4, countryname);
         ps.setString(5, statename);
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in updated users table while deleting state " + statename);
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while inserting users in updated users log for state " + statename, e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while inserting users in updated users log for state " + statename, e);
         throw e;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   public static void insertUpdatedUsersForCity(String dbConnName, Logger logger, String countryname, String statename, String cityname, String userid) throws SQLException, Exception
   {
      //insert into userupdatedeletesnapshot (userid,cityid,univname,collegename,adminuserid,deleteupdatetime,updatedeleteaction) (select userid,cityid,univname,collegename,?,?,? from users where cityid in(select cityid from citylist where countryname=? and statename=? and cityname=?));
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertUpdateUserForCity");
         ps.setString(1, userid);
         ps.setString(2, sdf.format(new Date()));
         ps.setString(3, "DELETE_CITY");
         ps.setString(4, countryname);
         ps.setString(5, statename);
         ps.setString(6, cityname);
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in updated users table while deleting city " + cityname);
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while inserting users in updated users log for city " + cityname, e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while inserting users in updated users log for city " + cityname, e);
         throw e;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   public static void insertUpdatedUniversityForCountry(String dbConnName, Logger logger, String countryname, String userid) throws SQLException, Exception
   {
      //insert into userupdatedeletesnapshot (userid,cityid,univname,collegename,adminuserid,deleteupdatetime,updatedeleteaction) (select userid,cityid,univname,collegename,?,?,? from users where cityid in(select cityid from citylist where countryname=?));
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertUpdateUniversityForCountry");
         ps.setString(1, userid);
         ps.setString(2, sdf.format(new Date()));
         ps.setString(3, "DELETE_COUNTRY");
         ps.setString(4, countryname);
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in updated university table while deleting country " + countryname);
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while inserting univ in updated university log for country " + countryname, e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while inserting univ in updated university log for country " + countryname, e);
         throw e;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   public static void insertUpdatedUniversityForState(String dbConnName, Logger logger, String countryname, String statename, String userid) throws SQLException, Exception
   {
      //insert into userupdatedeletesnapshot (userid,cityid,univname,collegename,adminuserid,deleteupdatetime,updatedeleteaction) (select userid,cityid,univname,collegename,?,?,? from users where cityid in(select cityid from citylist where countryname=? and statename=?));
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertUpdateUniversityForState");
         ps.setString(1, userid);
         ps.setString(2, sdf.format(new Date()));
         ps.setString(3, "DELETE_STATE");
         ps.setString(4, countryname);
         ps.setString(5, statename);
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in updated university table while deleting state " + statename);
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while inserting univ in updated university log for state " + statename, e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while inserting univ in updated university log for state " + statename, e);
         throw e;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   public static void insertUpdatedUniversityForCity(String dbConnName, Logger logger, String countryname, String statename, String cityname, String userid) throws SQLException, Exception
   {
      //insert into userupdatedeletesnapshot (userid,cityid,univname,collegename,adminuserid,deleteupdatetime,updatedeleteaction) (select userid,cityid,univname,collegename,?,?,? from users where cityid in(select cityid from citylist where countryname=? and statename=? and cityname=?));
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertUpdateUniversityForCity");
         ps.setString(1, userid);
         ps.setString(2, sdf.format(new Date()));
         ps.setString(3, "DELETE_CITY");
         ps.setString(4, countryname);
         ps.setString(5, statename);
         ps.setString(6, cityname);
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in updated university table while deleting city " + cityname);
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while inserting univ in updated university log for city " + cityname, e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while inserting univ in updated university log for city " + cityname, e);
         throw e;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   public static void insertDeletedCollegeForUniversity(String dbConnName, Logger logger, String univname, String userid) throws SQLException, Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertDeletedCollegeForUniv");
         ps.setString(1, userid);
         ps.setString(2, sdf.format(new Date()));
         ps.setString(3, "DELETE_UNIVERSITY");
         ps.setString(4, univname);
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in deleted college table while deleting university " + univname);
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while inserting colleges in deleted college log for university " + univname, e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while inserting colleges in deleted college log for university " + univname, e);
         throw e;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   public static void insertDeletedCollege(String dbConnName, Logger logger, String univname, String collegename, String userid) throws SQLException, Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertDeletedCollege");
         ps.setString(1, userid);
         ps.setString(2, sdf.format(new Date()));
         ps.setString(3, "DELETE_COLLEGE");
         ps.setString(4, univname);
         ps.setString(5, collegename);
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in deleted college table while deleting college " + collegename);
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while inserting colleges in deleted college log for college " + collegename, e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while inserting colleges in deleted college log for college " + collegename, e);
         throw e;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   public static void insertDeletedUniversity(String dbConnName, Logger logger, String univname, String userid) throws SQLException, Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertDeletedUniversity");
         ps.setString(1, userid);
         ps.setString(2, sdf.format(new Date()));
         ps.setString(3, "DELETE_UNIVERSITY");
         ps.setString(4, univname);
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in deleted university table while deleting university " + univname);
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while inserting univ in deleted university log for university " + univname, e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while inserting univ in deleted university log for university " + univname, e);
         throw e;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   public static void insertUpdatedUsersForUniversity(String dbConnName, Logger logger, String univname, String userid) throws SQLException, Exception
   {
      //insert into userupdatedeletesnapshot (userid,cityid,univname,collegename,adminuserid,deleteupdatetime,updatedeleteaction) (select userid,cityid,univname,collegename,?,?,? from users where univname=?);
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertUpdateUserForUniversity");
         ps.setString(1, userid);
         ps.setString(2, sdf.format(new Date()));
         ps.setString(3, "DELETE_UNIVERSITY");
         ps.setString(4, univname);
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in updated users table while deleting university " + univname);
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while inserting users in updated users log for university " + univname, e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while inserting users in updated users log for university " + univname, e);
         throw e;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   public static void insertUpdatedUsersForCollege(String dbConnName, Logger logger, String univname, String collegename, String userid) throws SQLException, Exception
   {
      //insert into userupdatedeletesnapshot (userid,cityid,univname,collegename,adminuserid,deleteupdatetime,updatedeleteaction) (select userid,cityid,univname,collegename,?,?,? from users where univname=?);
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertUpdateUserForCollege");
         ps.setString(1, userid);
         ps.setString(2, sdf.format(new Date()));
         ps.setString(3, "DELETE_COLLEGE");
         ps.setString(4, univname);
         ps.setString(5, collegename);
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in updated users table while deleting college " + collegename);
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while inserting users in updated users log for college " + collegename, e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while inserting users in updated users log for college " + collegename, e);
         throw e;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
   
   public static void insertDeletedOfferingForUniversity(String dbConnName, Logger logger, String univname, String userid) throws SQLException, Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertDeletedUnivOffering");
         ps.setString(1, userid);
         ps.setString(2, sdf.format(new Date()));
         ps.setString(3, "DELETE_UNIVERSITY");
         ps.setString(4, univname);
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in deleted offering table while deleting university " + univname);
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while inserting offerings in deleted offering log for university " + univname, e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while inserting offerings in deleted offering log for university " + univname, e);
         throw e;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
   
   public static void insertDeletedCourseOfferingForUniversity(String dbConnName, Logger logger, String univname, String userid) throws SQLException, Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertDeletedUnivCourseOffering");
         ps.setString(1, userid);
         ps.setString(2, sdf.format(new Date()));
         ps.setString(3, "DELETE_UNIVERSITY");
         ps.setString(4, univname);
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in deleted course offering table while deleting university " + univname);
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while inserting course offerings in deleted course offering log for university " + univname, e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while inserting course offerings in deleted course offering log for university " + univname, e);
         throw e;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
}