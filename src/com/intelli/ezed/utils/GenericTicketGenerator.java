/*
 * Last changed by           : $Author: indraneeld $
 * Last check in             : $Date: 2012/06/26 06:46:53 $
 * CVS version number        : $Revision: 1.1 $
 * CVS archive file location : $Archive$
 *
 * Copyright Notice
 * ================
 * Copyright (c) 2006 - 2007 SABRE Development Group
 * Copying or reproduction without prior written approval is prohibited.
 * SABRE Group PROPRIETARY/CONFIDENTIAL. Use is subject to approval from the SABRE group.
 * 
 */
package com.intelli.ezed.utils;

import java.util.concurrent.atomic.AtomicLong;

import org.sabre.utils.HexUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MbcLoggerNames;


public class GenericTicketGenerator
{
   private Logger logger = LoggerFactory.getLogger(MbcLoggerNames.SERVICE_LOG);
   private long rolloverVal;
   private int nodeId;
   private AtomicLong longSequence = null;
   private boolean shortValueRequired = false;

   public GenericTicketGenerator(long rolloverVal, int nodeId, long initValue)
   {
      setRolloverVal(rolloverVal);
      setNodeId(nodeId);
      longSequence = new AtomicLong(initValue);
   }

   public long getRolloverVal()
   {
      return rolloverVal;
   }

   public void setRolloverVal(long rolloverVal)
   {
      this.rolloverVal = rolloverVal;
   }


   /* (non-Javadoc)
    * @see com.eigroup.mdsa.resource.framework.TicketGenerator#getNextEdgeTicket()
    */
   public String getNextEdgeTicket()
   {
      long value = 0;
      synchronized (longSequence)
      {
         value = longSequence.getAndIncrement();
         if (logger.isInfoEnabled())
         {
            logger.info("Generated key :: " + value);
         }
         if (value == getRolloverVal())
         {
            longSequence.set(1);
         }

      }

      return getNodeId() + Long.toString(value);
   }

   public synchronized String getNextEdgeTicketFor(GenericTicketKey ticketKey)
   {
      if (ticketKey == null)
      {
         return getNextEdgeTicket();
      }

      String ticket = Integer.toString(ticketKey.getProtoId()) + getNextEdgeTicket();

      if (ticketKey.isShortValueRequired())
      {
         return new String(HexUtil.getShortenedValue(ticket, ticketKey.getPadLength()));
      }
      else
      {
         if (ticketKey.getPaddingFormatter() != null)
         {
            return ticketKey.getPaddingFormatter().format(Long.parseLong(ticket));
         }
         else
         {
            return ticket;
         }
      }
   }


   public int getNodeId()
   {
      return nodeId;
   }


   public void setNodeId(int nodeId)
   {
      this.nodeId = nodeId;
   }


   public boolean isShortValueRequired()
   {
      return shortValueRequired;
   }


   public void setShortValueRequired(boolean shortValueRequired)
   {
      this.shortValueRequired = shortValueRequired;
   }


}
