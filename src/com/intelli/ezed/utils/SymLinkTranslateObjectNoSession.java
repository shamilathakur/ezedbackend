package com.intelli.ezed.utils;

import java.util.HashMap;
import java.util.Map;

public class SymLinkTranslateObjectNoSession
{
   //Since this is for symlinks without session, the below map contains a 
   //simple map with contentid against symlink object. No user id or
   //session id comes to the picture here.
   private Map<String, SymLinkObject> symLinkMap = new HashMap<String, SymLinkObject>();

   public SymLinkObject getExistingSymLinkPresent(String contentId)
   {
      if (contentId == null)
      {
         return null;
      }

      SymLinkObject contentSymLink = symLinkMap.get(contentId);
      return contentSymLink;
   }

   public SymLinkObject putSymLink(String contentId, String origLink, String newLink, String fileName) throws Exception
   {
      if (contentId == null || origLink == null || newLink == null || fileName == null)
      {
         return null;
      }

      SymLinkObject contentSymLink = new SymLinkObject();
      contentSymLink.setContentId(contentId);
      contentSymLink.setContentLink(origLink);
      contentSymLink.setTranslatedContentLink(newLink);
      contentSymLink.setSessionId(null);
      contentSymLink.setFileName(fileName);
      symLinkMap.put(contentId, contentSymLink);
      return contentSymLink;
   }

   public SymLinkObject removeSymlinkForSession(String contentId)
   {
      if (contentId == null)
      {
         return null;
      }

      return symLinkMap.remove(contentId);
   }
}
