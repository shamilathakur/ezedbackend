package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.json.JSONArray;
import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.analytics.data.GenericChartData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.SingleChallenge;

public class UserChallengeTestsFetcher extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

   public UserChallengeTestsFetcher(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      String userId = (String) flowContext.getFromContext(EzedKeyConstants.USER_ID);

      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      //select * from challengetest where challengeeuserid=? and challengestatus=0
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchChallengeTests");
         ps.setString(1, userId);
         rs = ps.executeQuery();
         ArrayList<SingleChallenge> datas = new ArrayList<SingleChallenge>();
         SingleChallenge data = null;
         while (rs.next())
         {
            data = new SingleChallenge();
            data.setChallengeId(rs.getString("challengeid"));
            data.setTestId(rs.getString("testid"));
            data.setChallengedBy(rs.getString("userid"));
            data.setChallengeDate(sdf.parse(rs.getString("challengedate")));
            data.setCourseId(rs.getString("courseid"));
            data.setTestName(rs.getString("testname"));
            datas.add(data);
         }
         JSONArray jsonArray = new JSONArray();
         GenericChartData genericChartData = new GenericChartData();
         genericChartData.getText().add("challenges");
         if (datas.size() == 0)
         {
            genericChartData.getValues().add(jsonArray);
         }
         else
         {
            for (SingleChallenge singleChallenge : datas)
            {
               singleChallenge.prepareResponse();
               jsonArray.put(singleChallenge);
            }
            genericChartData.getValues().add(jsonArray);
         }
         response.setResponseObject(genericChartData);
         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
         return RuleDecisionKey.ON_SUCCESS;
      }
      catch (SQLException e)
      {
         logger.error("DB Error while fetching challenges for user " + userId, e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching user challenges");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching challenges for user " + userId, e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching user challenges");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing result set while fetching user challenge tests", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
}
