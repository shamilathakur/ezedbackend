package com.intelli.ezed.rules;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;

import com.intelli.ezed.data.EzedKeyConstants;

public class UserBypasser extends SingletonRule
{

   private String userId;
   public UserBypasser(String ruleName, String userId)
   {
      super(ruleName);
      this.userId = userId;
      // TODO Auto-generated constructor stub
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      flowContext.putIntoContext(EzedKeyConstants.USER_ID, userId);
      return RuleDecisionKey.ON_SUCCESS;
   }

}
