package com.intelli.ezed.bulkupload.data;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.sabre.db.DbConnectionManager;
import org.slf4j.Logger;

import com.a.helper.utils.StringSplitter;
import com.intelli.ezed.data.FIBData;
import com.intelli.ezed.data.MCQData;
import com.intelli.ezed.data.MTFData;
import com.intelli.ezed.data.QuestionContent;
import com.intelli.ezed.data.TestQuestion;
import com.intelli.ezed.data.TestQuestionData;
import com.intelli.ezed.utils.EzedHelper;
import com.intelli.ezed.utils.PrepareContentIdBackend;

public class ContentDetails
{
   public static final int CONTENT_NOTE = 1;
   public static final int CONTENT_VIDEO = 3;
   public static final int CONTENT_AUDIO = 2;
   public static final int CONTENT_QUIZ = 4;
   public static final int CONTENT_PDF = 1;
   public static final int CONTENT_TEST = 7;
   public static final int CONTENT_QBANK = 10;
   public static final int CONTENT_IMAGE = 11;
   public static final int CONTENT_YOUTUBE = 16;
   public static final int CONTENT_TEST_1 = 7;
   public static final int CONTENT_IMAGE_1 = 8;
   public static final Integer[] CONTENT_ONLY_COURSE_CONTENTS = {CONTENT_NOTE, CONTENT_VIDEO, CONTENT_AUDIO, CONTENT_PDF, CONTENT_IMAGE, CONTENT_YOUTUBE};
   public static final Integer[] TEST_ONLY_COURSE_CONTENTS = {CONTENT_QUIZ, CONTENT_TEST, CONTENT_TEST_1};
   private static Map<String, Integer> contentTypeMap = new HashMap<String, Integer>();
   private static Map<Integer, String> contentStringMap = new HashMap<Integer, String>();
   static
   {
      contentTypeMap.put("NOTE", CONTENT_NOTE);
      contentTypeMap.put("VIDEO", CONTENT_VIDEO);
      contentTypeMap.put("AUDIO", CONTENT_AUDIO);
      contentTypeMap.put("QUIZ", CONTENT_QUIZ);
      contentTypeMap.put("PDF", CONTENT_PDF);
      contentTypeMap.put("QBANK", CONTENT_QBANK);
      contentTypeMap.put("IMAGE", CONTENT_IMAGE);

      contentStringMap.put(CONTENT_NOTE, "NOTE");
      contentStringMap.put(CONTENT_VIDEO, "VIDEO");
      contentStringMap.put(CONTENT_AUDIO, "AUDIO");
      contentStringMap.put(CONTENT_QUIZ, "QUIZ");
      contentStringMap.put(CONTENT_PDF, "PDF");
      contentStringMap.put(CONTENT_QBANK, "QBANK");
      contentStringMap.put(CONTENT_IMAGE, "IMAGE");
      contentStringMap.put(CONTENT_TEST, "TEST");
      contentStringMap.put(CONTENT_YOUTUBE, "YOUTUBE");
      contentStringMap.put(CONTENT_TEST_1, "TEST");
      contentStringMap.put(CONTENT_IMAGE_1, "IMAGE");
   }
   private static PrepareContentIdBackend prepareContentId = new PrepareContentIdBackend("CON2", "ezeddb");
   private String contentName;
   private String contentId;
   private String contentLink;
   private String desc;
   private int contentType;
   private String contentTypeString;
   private int hours;
   private int qFlag = 0;

   public static boolean isContentOnlyCourse(int contentType)
   {
      for (int i = 0; i < CONTENT_ONLY_COURSE_CONTENTS.length; i++)
      {
         if (CONTENT_ONLY_COURSE_CONTENTS[i] == contentType)
         {
            return true;
         }
      }
      return false;
   }

   public static boolean isTestOnlyCourse(int contentType)
   {
      for (int i = 0; i < TEST_ONLY_COURSE_CONTENTS.length; i++)
      {
         if (TEST_ONLY_COURSE_CONTENTS[i] == contentType)
         {
            return true;
         }
      }
      return false;
   }

   public void setContentTypeFromName(String contentName)
   {
      if (contentName.endsWith(".html") || contentName.endsWith(".HTML") || contentName.endsWith(".htm") || contentName.endsWith(".HTM"))
      {
         setContentType(CONTENT_NOTE);
      }
      else
      {
         setContentType(CONTENT_IMAGE);
      }
   }
   public String getContentName()
   {
      return contentName;
   }
   public static Map<Integer, String> getContentStringMap()
   {
      return contentStringMap;
   }
   public static void setContentStringMap(Map<Integer, String> contentStringMap)
   {
      ContentDetails.contentStringMap = contentStringMap;
   }
   public void setContentName(String contentName)
   {
      this.contentName = EzedHelper.handleBulkUploadStringUpperCase(contentName, 50);
   }
   public String getContentId()
   {
      return contentId;
   }
   public void setContentId(String contentId)
   {
      this.contentId = contentId;
   }
   public void setNewContentId()
   {
      setContentId(fetchNewContentId());
   }
   public String getContentLink()
   {
      return contentLink;
   }
   public void setContentLink(String contentLink)
   {
      this.contentLink = contentLink;
   }
   public String getDesc()
   {
      return desc;
   }
   public void setDesc(String desc)
   {
      this.desc = EzedHelper.handleBulkUploadString(desc, 200);
   }
   public int getContentType()
   {
      return contentType;
   }
   public void setContentType(int contentType)
   {
      this.contentType = contentType;
      this.contentTypeString = contentStringMap.get(this.contentType);
   }
   public String getContentTypeString()
   {
      return contentTypeString;
   }
   public void setContentTypeString(String contentTypeString)
   {
      if (contentTypeString == null)
      {
         this.contentTypeString = contentTypeString;
      }
      else
      {
         this.contentTypeString = contentTypeString.toUpperCase();
         this.contentType = contentTypeMap.get(this.contentTypeString);
      }
   }
   public int getHours()
   {
      return hours;
   }
   public void setHours(int hours)
   {
      this.hours = hours;
   }

   public static String fetchNewContentId()
   {
      return prepareContentId.getNextId();
   }

   public void persistContent(String dbConnName, Logger logger) throws Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertContent");
         ps.setString(1, getContentId());
         ps.setString(2, getContentName());
         ps.setString(3, getDesc());
         ps.setInt(4, getContentType());
         ps.setInt(5, getHours());
         ps.setString(6, getContentLink());
         ps.setInt(7, getqFlag());

         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in db while inserting content");
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while inserting content in db", e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while inserting content in db", e);
         throw e;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   public void printDetails(Logger logger)
   {
      if (logger.isTraceEnabled())
      {
         logger.trace("contentname " + getContentName());
         logger.trace("contentid " + getContentId());
         logger.trace("desc " + getDesc());
         logger.trace("content type " + getContentType());
         logger.trace("content type string " + getContentTypeString());
         logger.trace("hours " + getHours());
      }
   }

   public void writeToFile(String rootDir, String contentFileName) throws IOException
   {
      File file = new File(rootDir + File.separator + getContentId());
      if (!file.exists())
      {
         file.mkdir();
      }
      File contentFile = new File(file.getAbsolutePath() + File.separator + contentFileName);
      if (!contentFile.exists())
      {
         contentFile.createNewFile();
      }
      FileOutputStream fos = null;
      try
      {
         fos = new FileOutputStream(contentFile);
         StringBuffer sb = new StringBuffer();
         sb.append("<contentdetails>\n");
         sb.append("<name>");
         sb.append(getContentName());
         sb.append("</name>\n");
         sb.append("<id>");
         sb.append(getContentId());
         sb.append("</id>\n");
         sb.append("<desc>");
         sb.append(getDesc());
         sb.append("</desc>\n");
         sb.append("<type>");
         sb.append(getContentTypeString());
         sb.append("</type>\n");
         sb.append("<hours>");
         sb.append(getHours());
         sb.append("</hours>\n</contentdetails>");
         fos.write(sb.toString().getBytes());
      }
      finally
      {
         if (fos != null)
         {
            try
            {
               fos.close();
            }
            catch (IOException e)
            {

            }
         }
      }
   }
   public ArrayList<TestQuestion> fetchQuiz(String dbConnName, Logger logger) throws SQLException, Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      ArrayList<TestQuestion> testQuestion = new ArrayList<TestQuestion>();
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchQuizQuestions");
         ps.setString(1, getContentId());
         rs = ps.executeQuery();
         QuestionContent content = null;
         while (rs.next())
         {
            int questionType = rs.getInt("questiontype");
            if (questionType == TestQuestionData.MCQ_QUESTION_TYPE)
            {
               MCQData data = new MCQData();
               content = new QuestionContent();
               content.setType(rs.getInt("questioncontenttype"));
               if (content.getType() == QuestionContent.STRING_TYPE)
               {
                  content.setContentString(rs.getString("questionstring"));
               }
               else
               {
                  content.setContentLoc(rs.getString("questioncontent"));
               }
               data.setQuestion(content);

               for (int i = 1; i < 7; i++)
               {
                  content = new QuestionContent();
                  content.setType(rs.getInt("ans" + i + "type"));
                  if (content.getType() == QuestionContent.STRING_TYPE)
                  {
                     content.setContentString(rs.getString("ans" + i + "string"));
                     data.addAnswers(content);
                  }
                  else if (content.getType() == QuestionContent.CONTENT_TYPE)
                  {
                     content.setContentLoc(rs.getString("ans" + i + "content"));
                     data.addAnswers(content);
                  }
               }
               data.setExplanation(new QuestionContent());
               data.getExplanation().setType(rs.getInt("explanationtype"));
               data.getExplanation().setContentString(rs.getString("explanation"));
               data.getExplanation().setContentLoc(rs.getString("explanationcontent"));
               data.setCorrectAnswer(rs.getString("correctans"));
               testQuestion.add(data);
            }
            else if (questionType == TestQuestionData.MTF_QUESTION_TYPE)
            {
               MTFData data = new MTFData();
               content = new QuestionContent();
               content.setType(rs.getInt("questioncontenttype"));
               String questionString = rs.getString("questionstring");
               String[] questions = StringSplitter.splitString(questionString, ";");
               if (content.getType() == QuestionContent.STRING_TYPE)
               {
                  content.setContentString(questions[0]);
               }
               else
               {
                  content.setContentLoc(questions[0]);
               }
               data.setQuestion(content);
               int type = Integer.parseInt(questions[1]);
               for (int i = 2; i < questions.length; i++)
               {
                  content = new QuestionContent();
                  content.setType(type);
                  if (type == QuestionContent.STRING_TYPE)
                  {
                     content.setContentString(questions[i]);
                  }
                  else
                  {
                     content.setContentLoc(questions[i]);
                  }
                  data.addQuestions(content);
               }

               for (int i = 1; i < 7; i++)
               {
                  content = new QuestionContent();
                  content.setType(rs.getInt("ans" + i + "type"));
                  if (content.getType() == QuestionContent.STRING_TYPE)
                  {
                     content.setContentString(rs.getString("ans" + i + "string"));
                     data.addAnswers(content);
                  }
                  else if (content.getType() == QuestionContent.CONTENT_TYPE)
                  {
                     content.setContentLoc(rs.getString("ans" + i + "content"));
                     data.addAnswers(content);
                  }
               }
               data.setExplanation(new QuestionContent());
               data.getExplanation().setType(rs.getInt("explanationtype"));
               data.getExplanation().setContentString(rs.getString("explanation"));
               data.getExplanation().setContentLoc(rs.getString("explanationcontent"));
               data.setCorrectAnswer(rs.getString("correctans"));
               testQuestion.add(data);
            }
            else
            {
               FIBData data = new FIBData();
               content = new QuestionContent();
               content.setType(rs.getInt("questioncontenttype"));
               if (content.getType() == QuestionContent.STRING_TYPE)
               {
                  content.setContentString(rs.getString("questionstring"));
               }
               else
               {
                  content.setContentLoc(rs.getString("questioncontent"));
               }
               data.setQuestion(content);

               for (int i = 1; i < 7; i++)
               {
                  content = new QuestionContent();
                  content.setType(rs.getInt("ans" + i + "type"));
                  if (content.getType() == QuestionContent.STRING_TYPE)
                  {
                     content.setContentString(rs.getString("ans" + i + "string"));
                     data.addAnswers(content);
                  }
                  else if (content.getType() == QuestionContent.CONTENT_TYPE)
                  {
                     content.setContentLoc(rs.getString("ans" + i + "content"));
                     data.addAnswers(content);
                  }
               }
               data.setExplanation(new QuestionContent());
               data.getExplanation().setType(rs.getInt("explanationtype"));
               data.getExplanation().setContentString(rs.getString("explanation"));
               data.getExplanation().setContentLoc(rs.getString("explanationcontent"));
               testQuestion.add(data);
            }
         }
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
      return testQuestion;
   }
   public void writeQuiz(String rootContentDir, String quizDetailsFileName, ArrayList<TestQuestion> testQuestions) throws IOException
   {
      File file = new File(rootContentDir + File.separator + getContentId() + File.separator + quizDetailsFileName);
      if (!file.exists())
      {
         file.createNewFile();
      }
      FileOutputStream fos = null;
      try
      {
         fos = new FileOutputStream(file);
         fos.write("<quizquestions>\n".getBytes());
         for (int i = 0; i < testQuestions.size(); i++)
         {
            testQuestions.get(i).writeToFile(fos);
         }
         fos.write("</quizquestions>".getBytes());
      }
      finally
      {
         fos.close();
      }
   }

   public int getqFlag()
   {
      return qFlag;
   }

   public void setqFlag(int qFlag)
   {
      this.qFlag = qFlag;
   }

}
