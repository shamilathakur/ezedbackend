package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class AdminRegistrationCountGetter extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.ANALYTIC_BACKUP_LOG);
   private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

   public AdminRegistrationCountGetter(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      String adminUserid = messageData.getParameter("adminid");

      Connection connection = DbConnectionManager.getConnectionByName(dbConnName);
      ResultSet resultSet = null;

      try
      {
         //select addedby,count(*) from users group by addedby;
         //select count(*) from users where addedby = ?;
         PreparedStatement preparedStatement = DbConnectionManager.getPreparedStatement(dbConnName, connection, "GetRegCount");
         preparedStatement.setString(1, adminUserid);
         resultSet = preparedStatement.executeQuery();
         
         while(resultSet.next())
         {
            
         }
         
      }
      catch (SQLException e)
      {
         logger.error("Database exception. ", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Internal Error. Please try again later.");
         return RuleDecisionKey.ON_SUCCESS;
      }
      catch (Exception e)
      {
         logger.error("Exception occoured. ", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Internal Error. Please try again later.");
         return RuleDecisionKey.ON_SUCCESS;
      }
      finally
      {
         try
         {
            resultSet.close();
         }
         catch (Exception e)
         {
            logger.error("Error while closing the resultset.", e);
         }
         DbConnectionManager.releaseConnection(dbConnName, connection);
      }
      
      return RuleDecisionKey.ON_SUCCESS;
   }
}
