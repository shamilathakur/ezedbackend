package com.intelli.ezed.db;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.ManageUserCourseFromBackendBean;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.utils.EzedHelper;

public class AlreadyOwnedCourseChecker extends SingletonRule {

	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
	private String dbConnName;

	public AlreadyOwnedCourseChecker(String ruleName, String dbConnName) {
		super(ruleName);
		this.dbConnName = dbConnName;
	}

	@Override
	public RuleDecisionKey executeRule(FlowContext flowContext) {
		Response response = (Response) flowContext
				.getFromContext(EzedKeyConstants.EZED_RESPONSE);
		ManageUserCourseFromBackendBean data = (ManageUserCourseFromBackendBean) flowContext
				.getFromContext(EzedKeyConstants.ADD_COURSE_TO_USER_FROM_ADMIN);

		boolean ownedStatus = EzedHelper.checkIfCourseAlreadyPurchased(
				dbConnName, logger, data.getUserId(), data.getCourseId());
		if (ownedStatus) {
			System.out.println("The course "
					+ EzedHelper.fetchCourseName(dbConnName,
							data.getCourseId(), logger)
					+ " is already owned by " + data.getUserId());
			if (logger.isDebugEnabled()) {
				logger.info("The course "
						+ EzedHelper.fetchCourseName(dbConnName,
								data.getCourseId(), logger)
						+ " is already owned by the user : " + data.getUserId());
			}
			response.setParameters(EzedTransactionCodes.COURSE_ALREADY_OWNED,
					"The course is already owned by the user!");
			return RuleDecisionKey.ON_FAILURE;
		}
		flowContext.putIntoContext(
				EzedKeyConstants.PAYMENT_ID_FOR_TRANSACTION_TYPE,
				EzedKeyConstants.PAYMENT_ID_FOR_TRANSACTION_TYPE_ONLINE_PAYMENT);
		return RuleDecisionKey.ON_SUCCESS;
	}

}
