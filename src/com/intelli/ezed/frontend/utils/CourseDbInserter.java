package com.intelli.ezed.frontend.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedLoggerName;

public class CourseDbInserter
{
   //   private static PrepareCourseId prepareCourseId;
   //   private static PrepareOfferingsId prepareOfferingsId;

   private static PrepareCourseIdBackend prepareCourseId;
   private static PrepareOfferingIdBackend prepareOfferingsId;

   //use your own logger name at this position
   private String loggerName = EzedLoggerName.SERVICE_LOG;
   private Logger logger = LoggerFactory.getLogger(loggerName);

   static
   {
      prepareCourseId = new PrepareCourseIdBackend("COU1", "");
      prepareOfferingsId = new PrepareOfferingIdBackend("OFF1", "");
   }

   private String getNextCourseId()
   {
      String courseId = prepareCourseId.getNextId();
      return courseId;
   }

   private String getNextOfferingId()
   {
      String courseId = prepareOfferingsId.getNextId();
      return courseId;
   }

   private String courseId;
   private String courseName;
   private String shortDesc;
   private String introDesc;
   private String introVideo;
   private String defChapter;
   private String lmrChapter;
   private String contentOnlyAmt;
   private String testOnlyAmt;
   private String fullCourseAmt;
   private int longDescType;
   private String longDescString;
   private String longDescContent;
   private String questionPaperId;
   private String solvedPaperId;
   private String lmr;
   private String endDate;

   //for university offerings details
   private String univName;
   private String degree;
   private String stream;
   private String year;
   private String semester;
   private String univOfferingId;


   public String getUnivName()
   {
      return univName;
   }

   public void setUnivName(String univName)
   {
      this.univName = univName;
   }

   public String getDegree()
   {
      return degree;
   }

   public void setDegree(String degree)
   {
      this.degree = degree;
   }

   public String getStream()
   {
      return stream;
   }

   public void setStream(String stream)
   {
      this.stream = stream;
   }

   public String getYear()
   {
      return year;
   }

   public void setYear(String year)
   {
      this.year = year;
   }

   public String getSemester()
   {
      return semester;
   }

   public void setSemester(String semester)
   {
      this.semester = semester;
   }

   public String getUnivOfferingId()
   {
      return univOfferingId;
   }

   public void setUnivOfferingId(String univOfferingId)
   {
      this.univOfferingId = univOfferingId;
   }

   public String getCourseId()
   {
      return courseId;
   }
   public void setCourseId(String courseId)
   {
      this.courseId = courseId;
   }
   public String getCourseName()
   {
      return courseName;
   }
   public void setCourseName(String courseName)
   {
      this.courseName = courseName;
   }
   public String getShortDesc()
   {
      return shortDesc;
   }
   public void setShortDesc(String shortDesc)
   {
      this.shortDesc = shortDesc;
   }
   public String getIntroDesc()
   {
      return introDesc;
   }
   public void setIntroDesc(String introDesc)
   {
      this.introDesc = introDesc;
   }
   public String getIntroVideo()
   {
      return introVideo;
   }
   public void setIntroVideo(String introVideo)
   {
      this.introVideo = introVideo;
   }
   public String getDefChapter()
   {
      return defChapter;
   }
   public void setDefChapter(String defChapter)
   {
      this.defChapter = defChapter;
   }
   public String getLmrChapter()
   {
      return lmrChapter;
   }
   public void setLmrChapter(String lmrChapter)
   {
      this.lmrChapter = lmrChapter;
   }
   public String getContentOnlyAmt()
   {
      return contentOnlyAmt;
   }
   public void setContentOnlyAmt(String contentOnlyAmt)
   {
      this.contentOnlyAmt = contentOnlyAmt;
   }
   public String getTestOnlyAmt()
   {
      return testOnlyAmt;
   }
   public void setTestOnlyAmt(String testOnlyAmt)
   {
      this.testOnlyAmt = testOnlyAmt;
   }
   public String getFullCourseAmt()
   {
      return fullCourseAmt;
   }
   public void setFullCourseAmt(String fullCourseAmt)
   {
      this.fullCourseAmt = fullCourseAmt;
   }
   public int getLongDescType()
   {
      return longDescType;
   }
   public void setLongDescType(int longDescType)
   {
      this.longDescType = longDescType;
   }
   public String getLongDescString()
   {
      return longDescString;
   }
   public void setLongDescString(String longDescString)
   {
      this.longDescString = longDescString;
   }
   public String getLongDescContent()
   {
      return longDescContent;
   }
   public void setLongDescContent(String longDescContent)
   {
      this.longDescContent = longDescContent;
   }
   public String getQuestionPaperId()
   {
      return questionPaperId;
   }
   public void setQuestionPaperId(String questionPaperId)
   {
      this.questionPaperId = questionPaperId;
   }
   public String getSolvedPaperId()
   {
      return solvedPaperId;
   }
   public void setSolvedPaperId(String solvedPaperId)
   {
      this.solvedPaperId = solvedPaperId;
   }
   public String getLmr()
   {
      return lmr;
   }
   public void setLmr(String lmr)
   {
      this.lmr = lmr;
   }
   public String getEndDate()
   {
      return endDate;
   }
   public void setEndDate(String endDate)
   {
      this.endDate = endDate;
   }

   public int getFromDatabase()
   {
      //change how you get your connection here. you can use your own database connection pool or whatever.
      Connection connection = getDbConnection();
      if (connection == null)
      {
         return EzedFrontEndTransactionCodes.NO_CONNECTION;
      }

      //change how you get your prepared statement here. you can put the connection in some configuration files for any other setup.
      PreparedStatement preparedStatement = getFetchStatement(connection);
      if (preparedStatement == null)
      {
         return EzedFrontEndTransactionCodes.NO_STATEMENT;
      }

      ResultSet resultSet = null;

      try
      {

         /*
          * courseid,coursename,
          * shortdesc,introdesc,introvideo,defchapter,lmrchapter,contentonlyamt,testonlyamt,fullcourseamt,
          * longdesctype,longdescstring,longdesccontent,questionpaperid,solvedpaperid,lmr,enddate
          */
         preparedStatement.setString(1, getCourseId());
         resultSet = preparedStatement.executeQuery();

         if (resultSet.next())
         {
            setCourseId(resultSet.getString("courseid"));
            setCourseName(resultSet.getString("coursename"));
            setShortDesc(resultSet.getString("shortdesc"));
            setIntroDesc(resultSet.getString("introdesc"));
            setIntroVideo(resultSet.getString("introvideo"));
            setDefChapter(resultSet.getString("defchapter"));
            setLmrChapter(resultSet.getString("lmrchapter"));
            setContentOnlyAmt(resultSet.getString("contentonlyamt"));
            setTestOnlyAmt(resultSet.getString("testonlyamt"));
            setFullCourseAmt(resultSet.getString("fullcourseamt"));
            setLongDescType(resultSet.getInt("longdesctype"));
            setLongDescString(resultSet.getString("longdescstring"));
            setLongDescContent(resultSet.getString("longdesccontent"));
            setQuestionPaperId(resultSet.getString("questionpaperid"));
            setSolvedPaperId(resultSet.getString("solvedpaperid"));
            setLmr(resultSet.getString("lmr"));
            setEndDate(resultSet.getString("enddate"));
         }
      }
      catch (SQLException e)
      {
         System.out.println(e);
         logger.error("Database Exception occoured while fetching course from database.", e);
         return EzedFrontEndTransactionCodes.DB_EXCEPTION_OCCURRED;
      }
      catch (Exception e)
      {
         logger.error("Exception occoured while fetching course from database.", e);
         return EzedFrontEndTransactionCodes.INTERNAL_ERROR;
      }
      finally
      {
         try
         {
            resultSet.close();
         }
         catch (Exception e)
         {
            logger.error("Exception occoured while closing result set.", e);
         }
      }


      return EzedFrontEndTransactionCodes.SUCCESS;
   }

   public int insertIntoDatabase()
   {
      //change how you get your connection here. you can use your own database connection pool or whatever.
      Connection connection = getDbConnection();
      if (connection == null)
      {
         return EzedFrontEndTransactionCodes.NO_CONNECTION;
      }

      //change how you get your prepared statement here. you can put the connection in some configuration files for any other setup.
      PreparedStatement preparedStatement = getStatement(connection);
      if (preparedStatement == null)
      {
         return EzedFrontEndTransactionCodes.NO_STATEMENT;
      }
      String courseIdForNewCourse = getNextCourseId();
      setCourseId(courseIdForNewCourse);

      try
      {
         /*
          * courseid,coursename,
          * shortdesc,introdesc,introvideo,defchapter,lmrchapter,contentonlyamt,testonlyamt,fullcourseamt,
          * longdesctype,longdescstring,longdesccontent,questionpaperid,solvedpaperid,lmr,enddate
          */

         preparedStatement.setString(1, courseIdForNewCourse);
         preparedStatement.setString(2, getCourseName());
         preparedStatement.setString(3, getShortDesc());
         preparedStatement.setString(4, getIntroDesc());
         preparedStatement.setString(5, getIntroVideo());
         preparedStatement.setString(6, getDefChapter());
         preparedStatement.setString(7, getLmrChapter());
         preparedStatement.setString(8, getContentOnlyAmt());
         preparedStatement.setString(9, getTestOnlyAmt());
         preparedStatement.setString(10, getFullCourseAmt());
         preparedStatement.setInt(11, getLongDescType());
         preparedStatement.setString(12, getLongDescString());
         preparedStatement.setString(13, getLongDescContent());
         preparedStatement.setString(14, getEndDate());
         //         preparedStatement.setString(14, getQuestionPaperId());
         //         preparedStatement.setString(15, getSolvedPaperId());
         //         preparedStatement.setString(16, getLmr());


         int i = preparedStatement.executeUpdate();

         if (i == 1)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Course added in database.");
            }
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("no data inserted in database status : " + i);
            }
            return EzedFrontEndTransactionCodes.NO_ROWS_INSERTED;
         }
      }
      catch (SQLException e)
      {
         System.out.println(e);

         if (e.getMessage().contains("course_fkey2"))//introdec
         {
            logger.error("Intro description is not present in content table.", e);
            return EzedFrontEndTransactionCodes.INVALID_OPRATION;
         }
         if (e.getMessage().contains("course_fkey3"))//introvedio
         {
            logger.error("introvedio is not present in content table.", e);
            return EzedFrontEndTransactionCodes.INVALID_OPRATION;
         }
         if (e.getMessage().contains("course_fkey4"))//defChapter
         {
            logger.error("defChapter is not present in chapter table.", e);
            return EzedFrontEndTransactionCodes.INVALID_OPRATION;
         }
         if (e.getMessage().contains("course_fkey5"))//questionpaperid
         {
            logger.error("questionpaperid is not present in content table.", e);
            return EzedFrontEndTransactionCodes.INVALID_OPRATION;
         }
         if (e.getMessage().contains("course_fkey6"))//solvedpaperid
         {
            logger.error("solvedpaperid is not present in content table.", e);
            return EzedFrontEndTransactionCodes.INVALID_OPRATION;
         }
         if (e.getMessage().contains("course_fkey7"))//lmr
         {
            logger.error("LMR is not present in chapter table.", e);
            return EzedFrontEndTransactionCodes.INVALID_OPRATION;
         }
         logger.error("Database Exception occoured while inserting content in database.", e);
         return EzedFrontEndTransactionCodes.DB_EXCEPTION_OCCURRED;
      }
      catch (Exception e)
      {
         logger.error("Exception occoured while inserting content in database.", e);
         return EzedFrontEndTransactionCodes.INTERNAL_ERROR;
      }

      if (logger.isDebugEnabled())
      {
         logger.debug("Added this course into database with Course id :" + courseIdForNewCourse);
      }

      //Check for offering in the database
      preparedStatement = getOfferingsStatement(connection);
      if (preparedStatement == null)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("No check offering statement. Course added in database with course id : " + courseIdForNewCourse);
         }
         return EzedFrontEndTransactionCodes.NO_STATEMENT;
      }


      ResultSet resultSet = null;
      boolean insertInOfferings = false;


      //univname=?,degree=?,stream=?,year=?,semester=?
      try
      {
         preparedStatement.setString(1, getUnivName());
         preparedStatement.setString(2, getDegree());
         preparedStatement.setString(3, getStream());
         //preparedStatement.setString(4, getYear());
         preparedStatement.setString(4, getSemester());
         resultSet = preparedStatement.executeQuery();

         String offeringId = null;
         if (resultSet.next())
         {
            offeringId = resultSet.getString("univofferingid");
         }

         if (offeringId == null || offeringId.equals("") || offeringId.equals(" "))
         {
            setUnivOfferingId(getNextOfferingId());
            insertInOfferings = true;
         }
         else
         {
            setUnivOfferingId(offeringId);
         }

      }
      catch (SQLException e)
      {
         System.out.println(e);
         logger.error("Database exception occoured checking for offering id.", e);
         return EzedFrontEndTransactionCodes.INTERNAL_ERROR;
      }
      catch (Exception e)
      {
         System.out.println(e);
         logger.error("Exception occoured checking for offering id.", e);
         return EzedFrontEndTransactionCodes.INTERNAL_ERROR;
      }
      finally
      {
         try
         {
            resultSet.close();
         }
         catch (Exception e)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Exception occoured while closing resultset.", e);
            }
         }
      }

      if (insertInOfferings == true)
      {
         preparedStatement = getInsertInOffringStatement(connection);
         if (preparedStatement == null)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Error while getting insert offering statement.");
               return EzedFrontEndTransactionCodes.NO_STATEMENT;
            }
         }

         //universityofferings,univname,degree,stream,year,semester
         try
         {
            preparedStatement.setString(1, getUnivOfferingId());
            preparedStatement.setString(2, getUnivName());
            preparedStatement.setString(3, getDegree());
            preparedStatement.setString(4, getStream());
            //preparedStatement.setString(5, getYear());
            preparedStatement.setString(6, getSemester());
            int i = preparedStatement.executeUpdate();

            if (i == 1)
            {
               if (logger.isDebugEnabled())
               {
                  logger.debug("Offering added in database.");
               }
            }
            else
            {
               if (logger.isDebugEnabled())
               {
                  logger.debug("no data inserted in database status : " + i);
               }
               return EzedFrontEndTransactionCodes.NO_ROWS_INSERTED;
            }
         }
         catch (SQLException e)
         {
            System.out.println(e);
            logger.error("Database exception occoured inserting for offering id.", e);
            return EzedFrontEndTransactionCodes.INTERNAL_ERROR;
         }
         catch (Exception e)
         {
            System.out.println(e);
            logger.error("Exception occoured inserting for offering id.", e);
            return EzedFrontEndTransactionCodes.INTERNAL_ERROR;
         }
      }

      preparedStatement = getInsertIntoUniversityCourseStatement(connection);

      try
      {
         preparedStatement.setString(1, getUnivOfferingId());
         preparedStatement.setString(2, getCourseId());
         int i = preparedStatement.executeUpdate();

         if (i == 1)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("University-Course added in database." + getUnivOfferingId() + " " + getCourseId() + " " + getCourseName());
            }
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("no data inserted in database status : " + i);
            }
            return EzedFrontEndTransactionCodes.NO_ROWS_INSERTED;
         }
      }
      catch (SQLException e)
      {
         System.out.println(e);
         logger.error("Database exception occoured inserting for UniversityCourse.", e);
         return EzedFrontEndTransactionCodes.INTERNAL_ERROR;
      }
      catch (Exception e)
      {
         System.out.println(e);
         logger.error("Exception occoured inserting for UniversityCourse", e);
         return EzedFrontEndTransactionCodes.INTERNAL_ERROR;
      }

      return EzedFrontEndTransactionCodes.SUCCESS;
   }

   public int persisteIntoDatabase()
   {
      //change how you get your connection here. you can use your own database connection pool or whatever.
      Connection connection = getDbConnection();
      if (connection == null)
      {
         return EzedFrontEndTransactionCodes.NO_CONNECTION;
      }

      //change how you get your prepared statement here. you can put the connection in some configuration files for any other setup.
      PreparedStatement preparedStatement = getUpdateStatement(connection);
      if (preparedStatement == null)
      {
         return EzedFrontEndTransactionCodes.NO_STATEMENT;
      }

      try
      {
         /*
          * courseid,coursename,
          * shortdesc,introdesc,introvideo,defchapter,lmrchapter,contentonlyamt,testonlyamt,fullcourseamt,
          * longdesctype,longdescstring,longdesccontent,questionpaperid,solvedpaperid,lmr,enddate
          */

         preparedStatement.setString(1, getCourseName());
         preparedStatement.setString(2, getShortDesc());
         preparedStatement.setString(3, getIntroDesc());
         preparedStatement.setString(4, getIntroVideo());
         preparedStatement.setString(5, getDefChapter());
         preparedStatement.setString(6, getLmrChapter());
         preparedStatement.setString(7, getContentOnlyAmt());
         preparedStatement.setString(8, getTestOnlyAmt());
         preparedStatement.setString(9, getFullCourseAmt());
         preparedStatement.setInt(10, getLongDescType());
         preparedStatement.setString(11, getLongDescString());
         preparedStatement.setString(12, getLongDescContent());
         preparedStatement.setString(13, getQuestionPaperId());
         preparedStatement.setString(14, getSolvedPaperId());
         preparedStatement.setString(15, getLmr());
         preparedStatement.setString(16, getEndDate());
         preparedStatement.setString(17, getNextCourseId());

         int i = preparedStatement.executeUpdate();

         if (i == 1)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Course added in database.");
            }
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("no data inserted in database status : " + i);
            }
            return EzedFrontEndTransactionCodes.NO_ROWS_INSERTED;
         }
      }
      catch (SQLException e)
      {
         System.out.println(e);
         if (e.getMessage().contains("course_fkey2"))//introdec
         {
            logger.error("Intro description is not present in content table.", e);
            return EzedFrontEndTransactionCodes.INVALID_OPRATION;
         }
         if (e.getMessage().contains("course_fkey3"))//introvedio
         {
            logger.error("introvedio is not present in content table.", e);
            return EzedFrontEndTransactionCodes.INVALID_OPRATION;
         }
         if (e.getMessage().contains("course_fkey4"))//defChapter
         {
            logger.error("defChapter is not present in chapter table.", e);
            return EzedFrontEndTransactionCodes.INVALID_OPRATION;
         }
         if (e.getMessage().contains("course_fkey5"))//questionpaperid
         {
            logger.error("questionpaperid is not present in content table.", e);
            return EzedFrontEndTransactionCodes.INVALID_OPRATION;
         }
         if (e.getMessage().contains("course_fkey6"))//solvedpaperid
         {
            logger.error("solvedpaperid is not present in content table.", e);
            return EzedFrontEndTransactionCodes.INVALID_OPRATION;
         }
         if (e.getMessage().contains("course_fkey7"))//lmr
         {
            logger.error("LMR is not present in chapter table.", e);
            return EzedFrontEndTransactionCodes.INVALID_OPRATION;
         }
         logger.error("Database Exception occoured while inserting content in database.", e);
         return EzedFrontEndTransactionCodes.DB_EXCEPTION_OCCURRED;
      }
      catch (Exception e)
      {
         logger.error("Exception occoured while inserting content in database.", e);
         return EzedFrontEndTransactionCodes.INTERNAL_ERROR;
      }
      return EzedFrontEndTransactionCodes.SUCCESS;
   }

   private Connection getDbConnection()
   {
      try
      {
         String url = "jdbc:postgresql://localhost:5432/ezed2";
         Class.forName("org.postgresql.Driver");
         return DriverManager.getConnection(url, "postgres", "postgres");
      }
      catch (Exception e)
      {
         return null;
      }
   }

   private PreparedStatement getStatement(Connection conn)
   {
      try
      {
         //return (conn.prepareStatement("insert into courselist (courseid,coursename,shortdesc,introdesc,introvideo,defchapter,lmrchapter,contentonlyamt,testonlyamt,fullcourseamt,longdesctype,longdescstring,longdesccontent,questionpaperid,solvedpaperid,lmr,enddate) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"));
         return (conn.prepareStatement("insert into courselist (courseid,coursename,shortdesc,introdesc,introvideo,defchapter,lmrchapter,contentonlyamt,testonlyamt,fullcourseamt,longdesctype,longdescstring,longdesccontent,enddate) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)"));
      }
      catch (Exception e)
      {
         return null;
      }
   }

   private PreparedStatement getOfferingsStatement(Connection conn)
   {
      try
      {
         //return (conn.prepareStatement("select univofferingid from universityofferings where univname=? and degree=? and stream=? and year=? and semester=? "));
         return (conn.prepareStatement("select univofferingid from universityofferings where univname=? and degree=? and stream=? and semester=? "));
      }
      catch (Exception e)
      {
         return null;
      }
   }

   private PreparedStatement getUpdateStatement(Connection conn)
   {
      try
      {
         return (conn.prepareStatement("update courselist set coursename=?,shortdesc=?,introdesc=?,introvideo=?,defchapter=?,lmrchapter=?,contentonlyamt=?,testonlyamt=?,fullcourseamt=?,longdesctype=?,longdescstring=?,longdesccontent=?,questionpaperid=?,solvedpaperid=?,lmr=?,enddate=? where courseid =? "));
      }
      catch (Exception e)
      {
         return null;
      }
   }

   private PreparedStatement getFetchStatement(Connection conn)
   {
      try
      {
         return (conn.prepareStatement("select * from courselist where courseid = ?"));
      }
      catch (Exception e)
      {
         return null;
      }
   }

   private PreparedStatement getInsertInOffringStatement(Connection conn)
   {
      try
      {
         //return (conn.prepareStatement("insert into universityofferings (univofferingid,univname,degree,stream,year,semester) values (?,?,?,?,?,?)"));
         return (conn.prepareStatement("insert into universityofferings (univofferingid,univname,degree,stream,semester) values (?,?,?,?,?)"));
      }
      catch (Exception e)
      {
         return null;
      }
   }

   private PreparedStatement getInsertIntoUniversityCourseStatement(Connection conn)
   {
      try
      {
         return (conn.prepareStatement("insert into universitycourse (univofferingid,courseid) values (?,?)"));
      }
      catch (Exception e)
      {
         return null;
      }
   }

   public static void main(String[] args)
   {
      /*
       * courseid,coursename,
       * shortdesc,introdesc,introvideo,defchapter,lmrchapter,contentonlyamt,testonlyamt,fullcourseamt,
       * longdesctype,longdescstring,longdesccontent,questionpaperid,solvedpaperid,lmr,enddate
       */

      CourseDbInserter courseDbInserter = new CourseDbInserter();
      courseDbInserter.setCourseName("JAVA2");
      courseDbInserter.setShortDesc("shortTwo");
      courseDbInserter.setIntroDesc("CON20000000136499398");
      courseDbInserter.setIntroVideo("CON20000000136499396");
      courseDbInserter.setDefChapter("CHA20000000136499396");
      courseDbInserter.setLmr("CHA20000000136499396");
      courseDbInserter.setContentOnlyAmt("100");
      courseDbInserter.setTestOnlyAmt("200");
      courseDbInserter.setFullCourseAmt("160");
      courseDbInserter.setLongDescContent("1");
      courseDbInserter.setLongDescString("Long Desc");
      courseDbInserter.setLongDescContent(null);
      courseDbInserter.setQuestionPaperId("CHA20000000136499396");
      courseDbInserter.setSolvedPaperId("CHA20000000136499396");
      courseDbInserter.setLmr("CHA20000000136499396");
      courseDbInserter.setEndDate("08091988");

      //universityofferings,univname,degree,stream,year,semester

      courseDbInserter.setUnivName("PUNE UNIVERSITY");
      courseDbInserter.setDegree("MBS");
      courseDbInserter.setStream("TIMEPASS");
      courseDbInserter.setYear("LAST");
      courseDbInserter.setSemester("THIRD");


      courseDbInserter.insertIntoDatabase();

      courseDbInserter.setCourseId("COU10000000136516935");
      courseDbInserter.getFromDatabase();
      courseDbInserter.setCourseName("CHANGE");
      courseDbInserter.persisteIntoDatabase();

   }

}
