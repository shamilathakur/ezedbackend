package com.intelli.ezed.analytics.data;

import org.json.JSONArray;

import com.intelli.ezed.data.ResponseObject;

public class TestResposeTwo extends ResponseObject
{

   private GenericChartData title = new GenericChartData();
   private GenericChartData xAxis = new GenericChartData();
   private GenericChartData yAxis = new GenericChartData();
   private GenericChartData type = new GenericChartData();

   private String chartType;
   private String yAxisDisplayText;
   private String xAxisDisplayText;

   private String yAxisDisplayLable;
   private String xAxisDisplayLable;

   private String graphTitle;

   private String[] cats;


   public String getGraphTitle()
   {
      return graphTitle;
   }

   public void setGraphTitle(String graphTitle)
   {
      this.graphTitle = graphTitle;
   }

   public String getyAxisDisplayText()
   {
      return yAxisDisplayText;
   }

   public String getChartType()
   {
      return chartType;
   }

   public void setChartType(String chartType)
   {
      this.chartType = chartType;
   }

   public void setyAxisDisplayText(String yAxisDisplayText)
   {
      this.yAxisDisplayText = yAxisDisplayText;
   }
   public String getxAxisDisplayText()
   {
      return xAxisDisplayText;
   }
   public void setxAxisDisplayText(String xAxisDisplayText)
   {
      this.xAxisDisplayText = xAxisDisplayText;
   }
   public String getyAxisDisplayLable()
   {
      return yAxisDisplayLable;
   }
   public void setyAxisDisplayLable(String yAxisDisplayLable)
   {
      this.yAxisDisplayLable = yAxisDisplayLable;
   }
   public String getxAxisDisplayLable()
   {
      return xAxisDisplayLable;
   }
   public void setxAxisDisplayLable(String xAxisDisplayLable)
   {
      this.xAxisDisplayLable = xAxisDisplayLable;
   }
   public String[] getCats()
   {
      return cats;
   }
   public void setCats(String[] cats)
   {
      this.cats = cats;
   }

   public void prepareChart() throws Exception
   {
      type.getText().add("type");
      type.getValues().add(chartType);
      type.prepareResponse();

      title.getText().add("text");
      title.getValues().add(graphTitle);
      title.prepareResponse();

      xAxis.getText().add("categories");
      xAxis.getValues().add(getCats());
      xAxis.getText().add("title");
      GenericChartData xAxisTitleData = new GenericChartData();
      xAxisTitleData.getText().add("text");
      xAxisTitleData.getValues().add(xAxisDisplayText);
      xAxisTitleData.prepareResponse();
      xAxis.getValues().add(xAxisTitleData);
      xAxis.prepareResponse();

      yAxis.getText().add("min");
      yAxis.getValues().add(0);
      yAxis.getText().add("title");
      GenericChartData yAxisText = new GenericChartData();
      yAxisText.getText().add("text");
      yAxisText.getValues().add(yAxisDisplayText);
      //      yAxisText.getText().add("align");
      //      yAxisText.getValues().add("high");
      yAxis.getValues().add(yAxisText);
      yAxis.getText().add("labels");
      GenericChartData yAxisLabels = new GenericChartData();
      yAxisLabels.getText().add("overflow");
      yAxisLabels.getValues().add("justify");
      yAxis.getValues().add(yAxisLabels);

      yAxisLabels.prepareResponse();
      yAxisText.prepareResponse();
      yAxis.prepareResponse();


   }

   private TestResponseOne[] testResponseOnes;

   public TestResponseOne[] getTestResponseOnes()
   {
      return testResponseOnes;
   }
   public void setTestResponseOnes(TestResponseOne[] testResponseOnes)
   {
      this.testResponseOnes = testResponseOnes;
   }

   @Override
   public void prepareResponse() throws Exception
   {
      JSONArray array = new JSONArray();
      for (int i = 0; i < getTestResponseOnes().length; i++)
      {
         getTestResponseOnes()[i].prepareResponse();
         array.put(getTestResponseOnes()[i]);
      }

      prepareChart();

      this.put("chart", type);
      this.put("title", title);
      this.put("xAxis", xAxis);
      this.put("yAxis", yAxis);
      this.put("series", array);

      //this.put("values", array);


   }
}
