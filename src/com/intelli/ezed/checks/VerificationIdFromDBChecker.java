package com.intelli.ezed.checks;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class VerificationIdFromDBChecker extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public VerificationIdFromDBChecker(String ruleName)
   {
      super(ruleName);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      String verifIdRequest = messageData.getParameter("verifid");
      String verifIdDB = messageData.getParameter("verifdb");
      String emailid = messageData.getParameter("email");

      if (logger.isDebugEnabled())
      {
         logger.debug("Verif ID in request " + verifIdRequest + " and in DB " + verifIdDB);
      }

      if (verifIdRequest == null || verifIdDB == null || verifIdRequest.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Verification ID in request or in DB is null or blank. Check failed");
         }
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Verification failed");
         return RuleDecisionKey.ON_FAILURE;
      }
      if (verifIdDB.compareToIgnoreCase(verifIdRequest) == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Verification ID's match for email id " + emailid);
         }
         response.setParameters(EzedTransactionCodes.SUCCESS, "User verification successful");
         return RuleDecisionKey.ON_SUCCESS;
      }
      else
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Verification ID's do not match for email id " + emailid);
         }
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Verification failed");
         return RuleDecisionKey.ON_FAILURE;
      }
   }
}
