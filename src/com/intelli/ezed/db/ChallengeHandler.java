package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.ChallengeTestData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.QuizRequestResponse;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.TestDetails;
import com.intelli.ezed.data.UserProfile;
import com.intelli.ezed.utils.PrepareChallengeIdBackend;

public class ChallengeHandler extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private static PrepareChallengeIdBackend challengeIdGetter = new PrepareChallengeIdBackend("CHL2", "ezeddb");

   public static final int CHALLENGEE_NOT_REGISTERED = 0;
   public static final int CHALLENGEE_REGISTERED = 1;
   public static final int CHALLENGEE_PURCHASED_COURSE = 2;

   public ChallengeHandler(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      TestDetails testDetails = (TestDetails) flowContext.getFromContext(TestDetails.TEST_DETAILS);
      UserProfile userProfile = (UserProfile) flowContext.getFromContext(EzedKeyConstants.USER_DETAILS);
      String testSessionId = (String) flowContext.getFromContext(EzedKeyConstants.TEST_SESSION_ID);
      String challengeUserId = (String) flowContext.getFromContext(EzedKeyConstants.CHALLENGE_USER_ID);
      String inTime = (String) flowContext.getFromContext(EzedKeyConstants.REQUEST_IN_TIME);

      if (challengeUserId == null)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Test is being taken without challenging anyone");
         }
         return RuleDecisionKey.ON_SUCCESS;
      }

      String challengeId = challengeIdGetter.getNextId();
      flowContext.putIntoContext(EzedKeyConstants.CHALLENGE_TEST_ID, challengeId);

      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;

      //insert into challengetest (userid,courseid,testid,challengeeuserid,challengestatus,challengedate,testname,testsessionid,challengeid) values (?,?,?,?,?,?,?,?,?);
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertTestChallenge");
         ps.setString(1, userProfile.getUserid());
         ps.setString(2, testDetails.getCourseId());
         ps.setString(3, testDetails.getTestid());
         ps.setString(4, challengeUserId);
         ps.setInt(5, 0);
         ps.setString(6, inTime);
         ps.setString(7, testDetails.getTestName());
         ps.setString(8, testSessionId);
         ps.setString(9, challengeId);
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in challenge db for challenge id " + challengeId);
         }
      }
      catch (SQLException e)
      {
         logger.error("DB Error while inserting challege in db for challenge id " + challengeId, e);
      }
      catch (Exception e)
      {
         logger.error("Error while inserting challege in db for challenge id " + challengeId, e);
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      ResultSet rs = null;
      boolean isUserRegistered = false;
      try
      {
         //select userid from users where userid=?
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchUserPoints");
         ps.setString(1, challengeUserId);
         rs = ps.executeQuery();
         if (rs.next())
         {
            isUserRegistered = true;
         }
      }
      catch (Exception e)
      {
         logger.error("Error while checking if challengee user id is registered " + challengeId, e);
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing resultset while checking if challenged userid is registered", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      boolean isCoursePurchased = false;
      if (isUserRegistered)
      {
         //select * from usercourse where userid=? and courseid=? and coursetype in (1,2);
         try
         {
            conn = DbConnectionManager.getConnectionByName(dbConnName);
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "CheckIfTestPurchased");
            ps.setString(1, challengeUserId);
            ps.setString(2, testDetails.getCourseId());
            rs = ps.executeQuery();
            if (rs.next())
            {
               isCoursePurchased = true;
            }
         }
         catch (Exception e)
         {
            logger.error("Error while checking if challengee user id has purchased course for challenge id " + challengeId, e);
         }
         finally
         {
            if (rs != null)
            {
               try
               {
                  rs.close();
               }
               catch (SQLException e)
               {
                  logger.error("Error while closing resultset while checking if challenged userid has purchased course", e);
               }
            }
            DbConnectionManager.releaseConnection(dbConnName, conn);
         }
      }

      int challengeUserStatus = CHALLENGEE_NOT_REGISTERED;
      if (!isUserRegistered)
      {
         challengeUserStatus = CHALLENGEE_NOT_REGISTERED;
      }
      else
      {
         if (!isCoursePurchased)
         {
            challengeUserStatus = CHALLENGEE_REGISTERED;
         }
         else
         {
            challengeUserStatus = CHALLENGEE_PURCHASED_COURSE;
         }
      }

      ChallengeTestData challengeTestData = new ChallengeTestData();
      challengeTestData.setChallengeId(challengeId);
      challengeTestData.setChallengeUserId(challengeUserId);
      challengeTestData.setUserStatus(challengeUserStatus);

      QuizRequestResponse quizRequestResponse = (QuizRequestResponse) response.getResponseObject();
      quizRequestResponse.setChallengeTestData(challengeTestData);
      return RuleDecisionKey.ON_SUCCESS;
   }
}
