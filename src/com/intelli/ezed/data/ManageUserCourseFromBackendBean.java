package com.intelli.ezed.data;

/**
 * This Bean class has been added initially for adding the course from the
 * backend .. Later on same bean class is being used while deleting the user
 * course ..
 * */
public class ManageUserCourseFromBackendBean {
	private String userId;
	private String courseId;
	private String courseType;
	private String startDate;
	private String endDate;
	private int paidAmount;
	private String addedBy;
	private String sessionId;
	private String paymentId;
	private int totalCourses; // count of courses sent in the courseId String
								// separated by ',' ..

	/**
	 * Added by kunal on 22/04/2015 .. Needed these fields for the service
	 * DeleteCourseFromBackend
	 */
	private int points;
	private int goalsachieved;
	private int goalsfailed;
	private int percentcomplete;

	/**
	 * Added by kunal on 22/04/2015 .. Needed these fields for the service
	 * DeleteCourseFromBackend
	 */
	public int getGoalsachieved() {
		return goalsachieved;
	}

	public void setGoalsachieved(int goalsachieved) {
		this.goalsachieved = goalsachieved;
	}

	public int getGoalsfailed() {
		return goalsfailed;
	}

	public void setGoalsfailed(int goalsfailed) {
		this.goalsfailed = goalsfailed;
	}

	public int getPercentcomplete() {
		return percentcomplete;
	}

	public void setPercentcomplete(int percentcomplete) {
		this.percentcomplete = percentcomplete;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	public String getCourseType() {
		return courseType;
	}

	public void setCourseType(String courseType) {
		this.courseType = courseType;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public int getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(int paidAmount) {
		this.paidAmount = paidAmount;
	}

	public String getAddedBy() {
		return addedBy;
	}

	public void setAddedBy(String addedBy) {
		this.addedBy = addedBy;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	public int getTotalCourses() {
		return totalCourses;
	}

	public void setTotalCourses(int totalCourses) {
		this.totalCourses = totalCourses;
	}

}
