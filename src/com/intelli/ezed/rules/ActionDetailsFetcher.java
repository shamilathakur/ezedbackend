package com.intelli.ezed.rules;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.ActionData;
import com.intelli.ezed.data.EventData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.MasterActionRecords;

public class ActionDetailsFetcher extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.STREAM_LOG);

   public ActionDetailsFetcher(String ruleName)
   {
      super(ruleName);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      EventData eventData = (EventData) flowContext.getFromContext(EzedKeyConstants.EVENT_OBJECT);
      ActionData actionData = MasterActionRecords.fetchRecord(eventData.getActionId());
      if (actionData == null)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Could not identify the action present in event");
         }
         return RuleDecisionKey.ON_FAILURE;
      }
      flowContext.putIntoContext(EzedKeyConstants.ACTION_DATA, actionData);

      return RuleDecisionKey.ON_SUCCESS;
   }

}
