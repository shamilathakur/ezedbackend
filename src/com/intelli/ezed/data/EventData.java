package com.intelli.ezed.data;

public class EventData
{
   private String actionId;
   private String courseId;
   private String chapterId;
   private String contentId;
   private String streamString;
   private boolean isGoalRelated = false;

   public String getActionId()
   {
      return actionId;
   }
   public void setActionId(String actionId)
   {
      this.actionId = actionId;
   }
   public String getCourseId()
   {
      return courseId;
   }
   public void setCourseId(String courseId)
   {
      this.courseId = courseId;
   }
   public String getChapterId()
   {
      return chapterId;
   }
   public void setChapterId(String chapterId)
   {
      this.chapterId = chapterId;
   }
   public String getContentId()
   {
      return contentId;
   }
   public void setContentId(String contentId)
   {
      this.contentId = contentId;
   }
   public String getStreamString()
   {
      return streamString;
   }
   public void setStreamString(String streamString)
   {
      this.streamString = streamString;
   }
   public boolean isGoalRelated()
   {
      return isGoalRelated;
   }
   public void setGoalRelated(boolean isGoalRelated)
   {
      this.isGoalRelated = isGoalRelated;
   }
}
