package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.DataFiller;
import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.FrontendFetchData;
import com.intelli.ezed.data.Response;

public class RankedUserFetcher extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public RankedUserFetcher(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      String university = messageData.getParameter("university");
      if (university == null || university.trim().compareTo("") == 0 || university.trim().compareToIgnoreCase("default") == 0)
      {
         university = null;
      }

      String college = messageData.getParameter("college");
      if (college == null || college.trim().compareTo("") == 0 || college.trim().compareToIgnoreCase("default") == 0)
      {
         college = null;
      }
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      String[] userArr = null;
      FrontendFetchData userData = new FrontendFetchData();
      response.setResponseObject(userData);
      response.setStatusCodeSend(false);
      userData.setAttribName("aaData");
      ArrayList<String[]> users = new ArrayList<String[]>();
      try
      {
         if (university == null && college == null)
         {
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchRankedUserList");
         }
         else if (university != null && college == null)
         {
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchRankedUserUnivList");
            ps.setString(1, university);
         }
         else
         {
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchRankedUserCollegeList");
            ps.setString(1, university);
            ps.setString(2, college);
         }
         rs = ps.executeQuery();
         int i = 1;
         String firstName = null;
         String lastName = null;
         String univName = null;
         String collegeName = null;
         while (rs.next())
         {
            userArr = new String[6];
            userArr[0] = DataFiller.getLeftZeroPadding(Integer.toString(i), 2);
            userArr[1] = rs.getString(1);
            firstName = rs.getString(2);
            lastName = rs.getString(3);
            if (firstName == null || lastName == null)
            {
               userArr[2] = "";
            }
            else
            {
               userArr[2] = firstName + " " + lastName;
            }
            univName = rs.getString(4);
            userArr[3] = univName == null ? "" : univName;
            collegeName = rs.getString(5);
            userArr[4] = collegeName == null ? "" : collegeName;
            userArr[5] = Integer.toString(rs.getInt(6));
            i++;
            users.add(userArr);
         }
         userData.setAttribList(users);
         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
      }
      catch (SQLException e)
      {
         logger.error("DB error while fetching list of users", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "DB error while fetching users");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching list of users", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching users");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing result set while fetching user list", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      return RuleDecisionKey.ON_SUCCESS;
   }
}
