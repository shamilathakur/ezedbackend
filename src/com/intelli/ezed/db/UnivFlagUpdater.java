package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class UnivFlagUpdater extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public UnivFlagUpdater(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      //      response.setStringOnlySend(true);

      String univstatus = messageData.getParameter("status");
      if (univstatus == null || univstatus.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("New university status is invalid " + univstatus);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Status to be updated not found");
         return RuleDecisionKey.ON_FAILURE;
      }

      int univStatusInt = 0;
      if (univstatus.trim().compareToIgnoreCase("confirmed") == 0)
      {
         univStatusInt = 1;
      }

      String univname = messageData.getParameter("university");
      if (univname == null || univname.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("University name in request is not proper " + univname);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "University name is invalid");
         return RuleDecisionKey.ON_FAILURE;
      }

      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdateUniversityStatus");
         ps.setInt(1, univStatusInt);
         ps.setString(2, univname);
         int rows = ps.executeUpdate();
         if (rows > 0)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug(rows + " rows updated in db while updating college status for id " + univname);
            }
            response.setParameters(EzedTransactionCodes.SUCCESS, univstatus);
            return RuleDecisionKey.ON_SUCCESS;
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("university status cannot be updated in request for university id " + univname);
            }
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "university flag could not be updated");
            return RuleDecisionKey.ON_FAILURE;
         }
      }
      catch (SQLException e)
      {
         logger.error("DB error while updating university status for id " + univname, e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "university flag could not be updated");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while updating university status for id " + univname, e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "university flag could not be updated");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
}