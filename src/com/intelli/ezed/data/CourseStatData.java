package com.intelli.ezed.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.slf4j.Logger;

import com.intelli.ezed.bulkupload.data.ContentDetails;
import com.intelli.ezed.utils.EndDateCalculator;
import com.intelli.ezed.utils.EzedHelper;

public class CourseStatData extends ResponseObject
{
   private static SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");
   private String courseId;
   private int chapterCount;
   private int videoCount;
   private int testCount;
   private int questionCount;
   private String endDate;
   private String courseName;
   private String suggestionString;
   private String shortDesc;
   private String courseImage;
   private int publishFlag;

   public String getShortDesc()
   {
      return shortDesc;
   }

   public void setShortDesc(String shortDesc)
   {
      this.shortDesc = shortDesc;
   }

   public String getSuggestionString()
   {
      return suggestionString;
   }

   public void setSuggestionString(String suggestionString)
   {
      this.suggestionString = suggestionString;
   }

   public String getCourseName()
   {
      return courseName;
   }

   public void setCourseName(String courseName)
   {
      this.courseName = courseName;
   }

   public String getCourseId()
   {
      return courseId;
   }

   public void setCourseId(String courseId)
   {
      this.courseId = courseId;
   }

   public int getChapterCount()
   {
      return chapterCount;
   }

   public void setChapterCount(int chapterCount)
   {
      this.chapterCount = chapterCount;
   }

   public int getVideoCount()
   {
      return videoCount;
   }

   public void setVideoCount(int videoCount)
   {
      this.videoCount = videoCount;
   }

   public int getTestCount()
   {
      return testCount;
   }

   public void setTestCount(int testCount)
   {
      this.testCount = testCount;
   }

   public int getQuestionCount()
   {
      return questionCount;
   }

   public void setQuestionCount(int questionCount)
   {
      this.questionCount = questionCount;
   }

   public String getEndDate()
   {
      return endDate;
   }

   public void setEndDate(String endDate)
   {
      this.endDate = endDate;
   }

   public void fetchEndDateCourseName(String dbConnName, Logger logger, boolean isCourseNameFetch) throws SQLException, Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchCourseDetail");
         ps.setString(1, getCourseId());
         rs = ps.executeQuery();
         if (rs.next())
         {
            String endMonths = rs.getString("enddate");
            Date endDate = EndDateCalculator.findEndDate(endMonths, new Date());
            Calendar cal = Calendar.getInstance();
            cal.setTime(endDate);
            int day = cal.get(Calendar.DAY_OF_MONTH);
            String suffix = EzedHelper.getDayOfMonthSuffix(day);
            String dateString = sdf.format(cal.getTime());
            dateString = dateString.substring(0, dateString.indexOf(' ')) + suffix + " " + dateString.substring(dateString.indexOf(' '), dateString.length());
            this.setEndDate(dateString);
            this.setCourseName(rs.getString("coursename"));
            this.setCourseImage(rs.getString("courseimage"));
            if (isCourseNameFetch)
            {
               this.setShortDesc(rs.getString("shortdesc"));
            }
            this.setPublishFlag(rs.getInt("publishflag"));
         }
         else
         {
            this.setEndDate(null);
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching end date from db for course id " + courseId, e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching end date from db for course id " + courseId, e);
         throw e;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing result set while fetching end date", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   public void fetchQuestionCount(String dbConnName, Logger logger) throws SQLException, Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchQuestionCount");
         ps.setString(1, getCourseId());
         rs = ps.executeQuery();
         if (rs.next())
         {
            this.setQuestionCount(rs.getInt(1));
         }
         else
         {
            this.setQuestionCount(0);
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching question count from db for course id " + courseId, e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching question count from db for course id " + courseId, e);
         throw e;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing result set while fetching question count", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   public void fetchTestCount(String dbConnName, Logger logger) throws SQLException, Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchTestCount");
         ps.setString(1, getCourseId());
         rs = ps.executeQuery();
         if (rs.next())
         {
            this.setTestCount(rs.getInt(1));
         }
         else
         {
            this.setTestCount(0);
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching test count from db for course id " + courseId, e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching test count from db for course id " + courseId, e);
         throw e;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing result set while fetching video count", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   public void fetchVideoCount(String dbConnName, Logger logger) throws SQLException, Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchVideoCount");
         ps.setString(1, getCourseId());
         ps.setInt(2, ContentDetails.CONTENT_VIDEO);
         ps.setString(3, "1");
         rs = ps.executeQuery();
         if (rs.next())
         {
            this.setVideoCount(rs.getInt(1));
         }
         else
         {
            this.setVideoCount(0);
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching video count from db for course id " + courseId, e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching video count from db for course id " + courseId, e);
         throw e;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing result set while fetching video count", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   public void fetchChapterCount(String dbConnName, Logger logger) throws SQLException, Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchChapterCount");
         ps.setString(1, getCourseId());
         rs = ps.executeQuery();
         if (rs.next())
         {
            this.setChapterCount(rs.getInt(1));
         }
         else
         {
            this.setChapterCount(0);
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching chapter count from db for course id " + courseId, e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching chapter count from db for course id " + courseId, e);
         throw e;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing result set while fetching chapter count", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   @Override
   public void prepareResponse() throws Exception
   {
      this.put("courseid", getCourseId());
      this.put("chaptercount", getChapterCount());
      this.put("videocount", getVideoCount());
      this.put("testcount", getTestCount());
      this.put("questioncount", getQuestionCount());
      this.put("enddate", getEndDate());
      this.put("coursename", getCourseName());
      this.put("suggestionstring", getSuggestionString());
      this.put("shortDescription", getShortDesc());
      this.put("courseimage", getCourseImage());
   }

   public static void main(String[] args) throws Exception
   {
      Calendar cal = Calendar.getInstance();
      cal.setTime(new Date());
      int day = cal.get(Calendar.DAY_OF_MONTH);
      String suffix = EzedHelper.getDayOfMonthSuffix(day);
      String dateString = sdf.format(new Date());
      dateString = dateString.substring(0, dateString.indexOf(' ')) + suffix + " " + dateString.substring(dateString.indexOf(' '), dateString.length());
      System.out.println(dateString);
   }

   public String getCourseImage()
   {
      return courseImage;
   }

   public void setCourseImage(String courseImage)
   {
      this.courseImage = courseImage;
   }

   public int getPublishFlag()
   {
      return publishFlag;
   }

   public void setPublishFlag(int publishFlag)
   {
      this.publishFlag = publishFlag;
   }
}
