package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.TestDetails;
import com.intelli.ezed.data.UserProfile;

public class GroupTestAttemptInserter extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;
   private static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

   public GroupTestAttemptInserter(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      UserProfile userProfile = (UserProfile) flowContext.getFromContext(EzedKeyConstants.USER_DETAILS);
      TestDetails testDetails = (TestDetails) flowContext.getFromContext(TestDetails.TEST_DETAILS);

      if (!testDetails.getCourseId().startsWith("GRO") && !testDetails.getCourseId().startsWith("gro"))
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Test is not a group test. Not inserting it in DB");
         }
         return RuleDecisionKey.ON_SUCCESS;
      }

      String testSessionId = (String) flowContext.getFromContext(EzedKeyConstants.TEST_SESSION_ID);

      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertGroupTestAttempt");
         ps.setString(1, userProfile.getUserid());
         ps.setString(2, testDetails.getCourseId());
         ps.setString(3, testDetails.getTestid());
         ps.setString(4, testSessionId);
         ps.setString(5, sdf.format(new Date()));
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in db while inserting group test attempt");
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while inserting group test attempt", e);
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while inserting group test attempt", e);
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
      return RuleDecisionKey.ON_SUCCESS;
   }
}
