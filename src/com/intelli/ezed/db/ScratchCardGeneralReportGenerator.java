package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.FrontendFetchData;
import com.intelli.ezed.data.Response;

public class ScratchCardGeneralReportGenerator extends SingletonRule {

	private String dbConnName;
	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
	SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyyMMddHHmmss");
	SimpleDateFormat sourceFormat1 = new SimpleDateFormat("yyyyMM");

	public ScratchCardGeneralReportGenerator(String ruleName, String dbConnName) {
		super(ruleName);
		this.dbConnName = dbConnName;
	}

	@SuppressWarnings("deprecation")
	@Override
	public RuleDecisionKey executeRule(FlowContext flowContext) {
		Response response = (Response) flowContext
				.getFromContext(EzedKeyConstants.EZED_RESPONSE);
		Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
		MessageData messageData = (MessageData) flowContext
				.getFromContext(MessageData.class.getName());
		PreparedStatement ps = null;
		ResultSet rs = null;

		String dynaQuery = "SELECT sc.srno,sc.scratchcardid,sc.coursetype,sc.usedstatus,sc.price,to_date(substring(sc.validtill,0,9), 'yyyymmdd') as validtill,"
				+ "to_char(to_date(substring(sc.generatedate,0,9), 'yyyymmdd'),'dd/mm/yyyy') as generatedate,"
				+ "sc.generatedby,sc.assignedstatus,pscm.activestatus,coalesce(pscm.partnerid,'-') as partnerid,coalesce(p.partnername,'-') as partnername, "
				+ "course.coursename from scratchcardlist sc left join partnerscratchcardmapping pscm on "
				+ "sc.scratchcardid=pscm.scratchcardid left join partnerlist p on pscm.partnerid=p.partnerid left join "
				+ "courselist course on sc.courseid=course.courseid";

		String finalQuery = dynaQuery + "";
		StringBuffer sb = new StringBuffer("");

		String startdate = messageData.getParameter("startdate");
		String enddate = messageData.getParameter("enddate");
		if ((startdate != null && startdate.compareTo("null") != 0 && startdate
				.compareTo("") != 0) && enddate.compareTo("null") == 0) {
			enddate = sourceFormat.format(new Date());
		}
		String coursetype = messageData.getParameter("coursetype");
		String courseid = messageData.getParameter("courseid");
		String createdBy = messageData.getParameter("createdby");
		String partnerid = messageData.getParameter("partnerid");
		int cardstatus = Integer.parseInt(messageData
				.getParameter("cardstatus"));
		int statustype = Integer.parseInt(messageData
				.getParameter("statustype"));

		String qryDate = " sc.generatedate between '" + startdate
				+ "' and '" + enddate + "'";
		String qryCourseId = " sc.courseid='" + courseid + "'";
		String qryAdmin = " sc.generatedby='" + createdBy + "'";
		String qryCourseType = " sc.coursetype='" + coursetype + "'";
		String qryActiveCard = " pscm.activestatus='" + cardstatus + "'";
		String qryAssignedCard = " sc.assignedstatus='" + cardstatus + "'";
		String qryUsedCard = " sc.usedstatus='" + cardstatus + "'";
		String qryExpiredCard = " sc.validtill <'"
				+ sourceFormat1.format(new Date()) + "'";
		String qryExpiredCard1 = " sc.validtill >'"
				+ sourceFormat1.format(new Date()) + "'";
		String qryPartnerId = " p.partnerid= '" + partnerid + "'";

		if (statustype != 0) {
			if (statustype == 1) {
				if (!(sb.length() > 0)) {
					sb.append(" where");
					sb.append(qryActiveCard);
				} else {
					sb.append(" and");
					sb.append(qryActiveCard);
				}
			} else if (statustype == 2) {
				if (!(sb.length() > 0)) {
					sb.append(" where");
					sb.append(qryAssignedCard);
				} else {
					sb.append(" and");
					sb.append(qryAssignedCard);
				}
			} else if (statustype == 3) {
				if (!(sb.length() > 0)) {
					sb.append(" where");
					sb.append(qryUsedCard);
				} else {
					sb.append(" and");
					sb.append(qryUsedCard);
				}
			} else if (statustype == 4) {
				if (cardstatus == 0) {
					if (!(sb.length() > 0)) {
						sb.append(" where");
						sb.append(qryExpiredCard1);
					} else {
						sb.append(" and");
						sb.append(qryExpiredCard1);
					}

				} else if (cardstatus == 1) {
					if (!(sb.length() > 0)) {
						sb.append(" where");
						sb.append(qryExpiredCard);
					} else {
						sb.append(" and");
						sb.append(qryExpiredCard);
					}

				}
			}
		}

		if (startdate != null && startdate.compareTo("null") != 0
				&& startdate.compareTo("") != 0) {
			if (!(sb.length() > 0)) {
				sb.append(" where");
				sb.append(qryDate);
			} else {
				sb.append(" and");
				sb.append(qryDate);
			}
		}
		if (createdBy.compareTo("null") != 0 && createdBy.compareTo("") != 0
				&& createdBy != null) {
			if (!(sb.length() > 0)) {
				sb.append(" where");
				sb.append(qryAdmin);
			} else {
				sb.append(" and");
				sb.append(qryAdmin);
			}
		}
		if (coursetype.compareTo("null") != 0 && coursetype.compareTo("") != 0
				&& coursetype != null) {
			if (!(sb.length() > 0)) {
				sb.append(" where");
				sb.append(qryCourseType);
			} else {
				sb.append(" and");
				sb.append(qryCourseType);
			}
		}
		if (courseid.compareTo("null") != 0 && courseid.compareTo("") != 0
				&& courseid != null) {
			if (!(sb.length() > 0)) {
				sb.append(" where");
				sb.append(qryCourseId);
			} else {
				sb.append(" and");
				sb.append(qryCourseId);
			}
		}
		if (partnerid.compareTo("null") != 0 && partnerid.compareTo("") != 0
				&& partnerid != null) {
			if (!(sb.length() > 0)) {
				sb.append(" where");
				sb.append(qryPartnerId);
			} else {
				sb.append(" and");
				sb.append(qryPartnerId);
			}
		}

		finalQuery = finalQuery + sb.toString();

		System.out.println("Final Query : " + finalQuery);

		String[] reportData = null;
		FrontendFetchData ScratchCardReport = new FrontendFetchData();
		response.setResponseObject(ScratchCardReport);
		response.setStatusCodeSend(false);
		ScratchCardReport.setAttribName("aaData");
		ArrayList<String[]> reportList = new ArrayList<String[]>();

		try {
			ps = conn.prepareStatement(finalQuery);
			rs = ps.executeQuery();
			while (rs.next()) {
				reportData = new String[13];
				reportData[0] = rs.getInt("srno") + "";
				reportData[1] = rs.getString("scratchcardid");
				reportData[2] = rs.getString("coursename");

				int type = rs.getInt("coursetype");
				if (type == 1) {
					reportData[3] = "Content Only";
				} else if (type == 2) {
					reportData[3] = "Test Only";
				} else if (type == 3) {
					reportData[3] = "Full Course";
				}
				reportData[4] = rs.getInt("price") + "";
				reportData[5] = rs.getString("generatedate");
				reportData[6] = rs.getString("generatedby");
				reportData[7] = rs.getString("partnername");
				reportData[8] = rs.getString("partnerid");
				if (rs.getInt("activestatus") == 1) {
					reportData[9] = "Active";
				} else {
					reportData[9] = "Inactive";
				}
				if (rs.getInt("assignedstatus") == 1) {
					reportData[10] = "Assigned";
				} else {
					reportData[10] = "Unassigned";
				}
				if (rs.getInt("usedstatus") == 1) {
					reportData[11] = "Used";
				} else {
					reportData[11] = "Unused";
				}
				Date validTill = rs.getDate("validtill");
				Date newValidTill = validTill;
				newValidTill.setMonth(validTill.getMonth() + 1);
				newValidTill.setDate(validTill.getDate() - 1);
				Date now = new Date();
				if (newValidTill.before(now)) {
					reportData[12] = "Expired";
				} else {
					reportData[12] = "Valid";
				}
				reportList.add(reportData);
			}
			ScratchCardReport.setAttribList(reportList);
			response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
		} catch (SQLException e) {
			logger.error("Error while fetching Utilized SC details from db : ",
					e);
			response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED,
					"Error while fetching Utilized SC details");
			return RuleDecisionKey.ON_FAILURE;
		} catch (Exception e) {
			logger.error(
					"Internal Error while fetching Utilized SC details : ", e);
			response.setParameters(EzedTransactionCodes.INTERNAL_ERROR,
					"Error while fetching Utilized SC details");
			return RuleDecisionKey.ON_FAILURE;
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					logger.error(
							"Error while closing result set while fetching Utilized SC list",
							e);
				}
			}
			DbConnectionManager.releaseConnection(dbConnName, conn);
		}

		return RuleDecisionKey.ON_SUCCESS;
	}

}
