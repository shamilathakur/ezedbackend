package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.a.helper.utils.StringSplitter;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class BannerUpdater extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public BannerUpdater(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      response.setStringOnlySend(true);

      String newBannerValue = messageData.getParameter("value");
      if (newBannerValue == null || newBannerValue.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("New banner value is invalid " + newBannerValue);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "");
         return RuleDecisionKey.ON_FAILURE;
      }
      int colNo = 0;
      try
      {
         colNo = Integer.parseInt(messageData.getParameter("columnid"));
      }
      catch (Exception e)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Column no is invalid in request", e);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "");
         return RuleDecisionKey.ON_FAILURE;
      }

      String origRowData = messageData.getParameter("rowdata");
      if (origRowData == null || origRowData.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Orig row data is invalid " + origRowData);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "");
         return RuleDecisionKey.ON_FAILURE;
      }

      String[] origRowStrings = StringSplitter.splitString(origRowData, "~~");
      if (origRowStrings.length < 6)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Orig row data is invalid since its length is not proper " + origRowData);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "");
         return RuleDecisionKey.ON_FAILURE;
      }

      String bannerId = origRowStrings[5];
      if (bannerId == null || bannerId.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Banner id in request is not proper " + bannerId);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "");
         return RuleDecisionKey.ON_FAILURE;
      }

      origRowStrings[colNo] = newBannerValue;

      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdateBanner");
         ps.setString(1, origRowStrings[1]);
         ps.setString(2, origRowStrings[2]);
         ps.setString(3, bannerId);
         int rows = ps.executeUpdate();
         if (rows > 0)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug(rows + " rows updated in db while updating banner details for id " + bannerId);
            }
            response.setParameters(EzedTransactionCodes.SUCCESS, newBannerValue);
            return RuleDecisionKey.ON_SUCCESS;
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Banner cannot be updated in request for test id " + bannerId);
            }
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "");
            return RuleDecisionKey.ON_FAILURE;
         }
      }
      catch (SQLException e)
      {
         logger.error("DB error while updating banner value for id " + bannerId, e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while updating banner value for id " + bannerId, e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
}
