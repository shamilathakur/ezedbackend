package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.BannerData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.FrontendFetchData;
import com.intelli.ezed.data.Response;

public class BannerListFetcher extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public BannerListFetcher(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      String[] bannerArr = null;
      FrontendFetchData bannerData = new FrontendFetchData();
      response.setResponseObject(bannerData);
      response.setStatusCodeSend(false);
      bannerData.setAttribName("aaData");
      ArrayList<String[]> banners = new ArrayList<String[]>();
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchBannerDetails");
         rs = ps.executeQuery();
         int i = 1;
         int bannerType = 0;
         int bannerStatus = 0;
         while (rs.next())
         {
            bannerArr = new String[6];
            bannerArr[0] = Integer.toString(i);
            bannerArr[1] = rs.getString("bannertitle");
            bannerArr[2] = rs.getString("bannerdesc");

            bannerType = rs.getInt("bannertype");

            if (bannerType == BannerData.BANNER_IMAGE_TYPE)
            {
               bannerArr[3] = "IMAGE";
            }
            else if (bannerType == BannerData.BANNER_VIDEO_TYPE)
            {
               bannerArr[3] = "VIDEO";
            }
            else if (bannerType == BannerData.BANNER_YOUTUBE_TYPE)
            {
               bannerArr[3] = "YOUTUBE";
            }
            else
            {
               bannerArr[3] = "UNKNOWN";
            }

            bannerStatus = rs.getInt("bannerstatus");
            if (bannerStatus == 0)
            {
               bannerArr[4] = BannerData.PUBLISHED_BANNER;
            }
            else
            {
               bannerArr[4] = BannerData.NOT_PUBLISHED_BANNER;
            }
            bannerArr[5] = rs.getString("bannerid");
            i++;
            banners.add(bannerArr);
         }
         bannerData.setAttribList(banners);
         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
      }
      catch (SQLException e)
      {
         logger.error("DB error while fetching list of banners", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "DB error while fetching banners");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching list of banners", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching banners");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing result set while fetching banners list", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
      return RuleDecisionKey.ON_SUCCESS;
   }
}
