package com.intelli.ezed.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;

import org.sabre.db.DbConnectionManager;
import org.slf4j.Logger;

public class TestQuestionData
{
   public static final int MCQ_QUESTION_TYPE = 1;
   public static final int MTF_QUESTION_TYPE = 2;
   public static final int FIB_QUESTION_TYPE = 3;

   private String courseid;
   private String chapterid;
   private TestQuestion testQuestion;

   public TestQuestion getTestQuestion()
   {
      return testQuestion;
   }
   public void setTestQuestion(TestQuestion testQuestion)
   {
      this.testQuestion = testQuestion;
   }
   public String getCourseid()
   {
      return courseid;
   }
   public void setCourseid(String courseid)
   {
      this.courseid = courseid;
   }
   public String getChapterid()
   {
      return chapterid;
   }
   public void setChapterid(String chapterid)
   {
      this.chapterid = chapterid;
   }

   public void persistData(ArrayList<TestQuestion> randomQuestions, String dbConnName, Logger logger)
   {
      Connection conn = null;
      PreparedStatement ps = null;
      for (int i = 0; i < randomQuestions.size(); i++)
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         try
         {
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertRandomQuestion");
            ps.setString(1, getCourseid());
            ps.setString(2, getChapterid());
            ps.setString(3, getTestQuestion().getQuestionid());
            ps.setInt(4, (i + 1));
            ps.setInt(5, getTestQuestion().getDifficultyLevel());
            randomQuestions.get(i).persistQuestion(ps, 5);
            int rows = ps.executeUpdate();
            if (logger.isDebugEnabled())
            {
               logger.debug(rows + " rows inserted in db while inserting for question id " + getTestQuestion().getQuestionid() + " amd question no " + (i + 1));
            }
         }
         catch (Exception e)
         {
            logger.error("Error while persisting Random question for question id " + getTestQuestion().getQuestionid() + " and question no " + (i + 1), e);
         }
         finally
         {
            DbConnectionManager.releaseConnection(dbConnName, conn);
         }
      }
   }
}
