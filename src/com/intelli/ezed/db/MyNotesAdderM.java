package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.frontend.utils.PrepareNoteId;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.utils.EzedHelper;


public class MyNotesAdderM extends SingletonRule
{

	private String dbConnName;
	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
	PrepareNoteId prepareNoteId = null;

	public MyNotesAdderM(String ruleName,  String dbConnName) {
		super(ruleName);
		 this.dbConnName = dbConnName;
	}

	@Override
	public RuleDecisionKey executeRule(FlowContext flowContext) {
		 MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
	     Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
	     
	     String courseid = messageData.getParameter("courseid");
	     if (courseid == null || courseid.trim().compareTo("") == 0)
	      {
	         if (logger.isDebugEnabled())
	         {
	            logger.debug("Course id is invalid " + courseid);
	         }
	         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid  course id");
	         return RuleDecisionKey.ON_FAILURE;
	      }
	     String userid = messageData.getParameter("email");
	     if (userid == null || userid.trim().compareTo("") == 0)
	      {
	         if (logger.isDebugEnabled())
	         {
	            logger.debug("user id is invalid " + userid);
	         }
	         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid  user id");
	         return RuleDecisionKey.ON_FAILURE;
	      }
	     
	     String contentid = messageData.getParameter("contentid");
	     if (contentid == null || contentid.trim().compareTo("") == 0)
	      {
	         if (logger.isDebugEnabled())
	         {
	            logger.debug("content id is invalid " + contentid);
	         }
	         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid  content id");
	         return RuleDecisionKey.ON_FAILURE;
	      }
	     String chapterid = messageData.getParameter("chapterid");
	     if (chapterid == null || chapterid.trim().compareTo("") == 0)
	      {
	         if (logger.isDebugEnabled())
	         {
	            logger.debug("chaoter id is chapter " + chapterid);
	         }
	         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid  chapter id");
	         return RuleDecisionKey.ON_FAILURE;
	      }
	     String note = messageData.getParameter("note");
	     if (note == null || note.trim().compareTo("") == 0)
	      {
	         if (logger.isDebugEnabled())
	         {
	            logger.debug("note id is invalid " + note);
	         }
	         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid  note ");
	         return RuleDecisionKey.ON_FAILURE;
	      }
	     String imagepath = messageData.getParameter("imagepath");
		 String noteId = null;

	     prepareNoteId = PrepareNoteId.getPrepareNoteId();
		
		if (prepareNoteId != null) {
			
			noteId = prepareNoteId.getNoteId();
			if (noteId == null || noteId.trim().compareTo("") == 0) {
				logger.error("Generated NoteId is Null !");
				 response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Error while generating Note Id ");
		         return RuleDecisionKey.ON_FAILURE;
		      }
		} else {
			logger.error("Generated NoteId is Null !");
			 response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Error while generating Note Id ");
	         return RuleDecisionKey.ON_FAILURE;
		}

	     Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
	     boolean isCoursePurchased = EzedHelper.checkIfCourseAlreadyPurchased(dbConnName, logger, userid, courseid);
		   
		    if(!isCoursePurchased){
		    	 response.setParameters(EzedTransactionCodes.INVALID_COURSE_ID, "Course Not Purchased By User");
		    	 return RuleDecisionKey.ON_FAILURE;
		    }
	     PreparedStatement ps = null;
	     try {
		     ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertMyNotes");
	         ps.setString(1, userid);
	         ps.setString(2, courseid);
	         SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
	         ps.setString(3, dateFormat.format(new Date()));
	         ps.setString(4, note);
	         ps.setString(5,chapterid);
	         ps.setString(6, noteId);
	         ps.setString(7, contentid);
	         ps.setString(8, imagepath);
	         int rows = ps.executeUpdate();
	         if (rows > 0)
	         {
	            if (logger.isDebugEnabled())
	            {
	               logger.debug(rows + " rows inserted in db while adding notes to chapter " + chapterid + ". Added  for content " + contentid);
	            }
	            response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
	            return RuleDecisionKey.ON_SUCCESS;
	         }
	         else
	         {
	            if (logger.isDebugEnabled())
	            {
	               logger.debug("No rows were updated");
	            }
	            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "New notes could not be added");
	            return RuleDecisionKey.ON_FAILURE;
	         }
         
		} catch (SQLException e)
	      {
	         logger.error("Error while insering notes for user " + userid, e);
	         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while insering notes for user" + e.getMessage());
	         return RuleDecisionKey.ON_FAILURE;
	      }
	      catch (Exception e)
	      {
	    	  logger.error("Error while insering notes for user " + userid, e);
	         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while insering notes for user" + e.getMessage());
	         return RuleDecisionKey.ON_FAILURE;
	      }
	      finally
	      {
	         DbConnectionManager.releaseConnection(dbConnName, conn);
	      }
	}

}
