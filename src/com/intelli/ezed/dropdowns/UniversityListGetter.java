package com.intelli.ezed.dropdowns;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import bsh.org.objectweb.asm.Constants;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.dropdowns.data.DropDownListData;
import com.intelli.ezed.dropdowns.data.SingleDropDownData;
import com.intelli.ezed.utils.Globals;

public class UniversityListGetter extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;

   public UniversityListGetter(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      DropDownListData dropDownListData = new DropDownListData();
      dropDownListData.setName("UniversityIdMapList");
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "GetUniversityList");
         ps.setInt(1,Globals.UNIVERSITY_CONFIRMED);
         rs = ps.executeQuery();
         SingleDropDownData data = null;
         while (rs.next())
         {
            data = new SingleDropDownData();
            data.setParam(rs.getString("univname"));
            data.setValue(rs.getString("univname"));
            dropDownListData.getDropDownDatas().add(data);
         }
         response.setStatusCodeSend(false);
         response.setResponseObject(dropDownListData);
         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
         return RuleDecisionKey.ON_SUCCESS;
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching universities for universities drop downs", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while populating university list drop down");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching universities for universities drop downs", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while populating universities list drop down");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing result set while populating drop down for universities");
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
}
