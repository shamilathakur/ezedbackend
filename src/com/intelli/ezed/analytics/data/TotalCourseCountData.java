package com.intelli.ezed.analytics.data;

import com.intelli.ezed.data.ResponseObject;

public class TotalCourseCountData extends ResponseObject
{
   private int totalNotes = 0;
   private int totalAudios = 0;
   private int totalVideos = 0;
   private int totalTests = 0;
   private int totalTestsAttempted = 0;
   private int uniqueTestsAttempted = 0;
   private int totalQuizzes = 0;

   public TotalCourseCountData(int totalNotes, int totalAudios, int totalVideos, int totalTests, int totalTestAttempts,
                               int totalUniqueTestAttempts, int totalQuizzes)
   {
      this.totalNotes = totalNotes;
      this.totalAudios = totalAudios;
      this.totalVideos = totalVideos;
      this.totalTests = totalTests;
      this.totalTestsAttempted = totalTestAttempts;
      this.uniqueTestsAttempted = totalUniqueTestAttempts;
      this.totalQuizzes = totalQuizzes;
   }

   @Override
   public void prepareResponse() throws Exception
   {
      float[] floats = new float[7];
      floats[0] = (float) this.totalNotes;
      floats[1] = (float) this.totalVideos;
      floats[2] = (float) this.totalAudios;
      floats[3] = (float) this.totalTests;
      floats[4] = (float) this.totalTestsAttempted;
      floats[5] = (float) this.uniqueTestsAttempted;
      floats[6] = (float) this.totalQuizzes;

      this.put("name", "Remaining Course contents");
      this.put("data", floats);
   }

}
