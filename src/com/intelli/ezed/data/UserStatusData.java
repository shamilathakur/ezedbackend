package com.intelli.ezed.data;

public class UserStatusData extends ResponseObject
{

   private String userid;
   private String roll;

   public String getUserid()
   {
      return userid;
   }
   public void setUserid(String userid)
   {
      this.userid = userid;
   }
   public String getRoll()
   {
      return roll;
   }
   public void setRoll(String roll)
   {
      this.roll = roll;
   }

   @Override
   public void prepareResponse() throws Exception
   {
      this.put("userid", getUserid());
      this.put("status", getRoll());
   }
}
