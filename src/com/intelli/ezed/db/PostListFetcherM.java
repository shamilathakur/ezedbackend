package com.intelli.ezed.db;

/*
 * Class used for fetching post list in the user profile. Used for mobile implementation.
 */

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import org.json.JSONArray;
import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.analytics.data.GenericChartData;
import com.intelli.ezed.data.PostData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;


public class PostListFetcherM extends SingletonRule{

	private String dbConnName;
	
    private Logger log = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
	
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss");
    private SimpleDateFormat sdfOut = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");

    
	public PostListFetcherM(String ruleName, String dbConnName)
	   {
	      super(ruleName);
	      this.dbConnName = dbConnName;
	   }

	   @Override
	   public RuleDecisionKey executeRule(FlowContext flowContext)
	   {
		  Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
	      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
		
		  ResultSet resultSet=null;
		  Connection connection=null;
		  connection = DbConnectionManager.getConnectionByName(dbConnName);
		  PreparedStatement preparedStatement=null;
		  PostData singlePostData = null;
	      ArrayList<PostData> posts = new ArrayList<PostData>();
	      try{
		        String threadId = messageData.getParameter("threadId");
		        if (threadId == null || threadId.trim().compareTo("") == 0 || threadId.trim().compareToIgnoreCase("default") == 0)
		        {
		        	if (log.isDebugEnabled()) {
						log.debug("thread id received in request is invalid");
					}
					response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER,"Invalid thread id");
					return RuleDecisionKey.ON_FAILURE;
		        }
		        
		        preparedStatement= DbConnectionManager.getPreparedStatement(dbConnName, connection, "FetchPostListForThreadId");
		     	preparedStatement.setString(1, threadId);
		     	
		        resultSet = preparedStatement.executeQuery();
				
				if (resultSet != null) {
					while (resultSet.next()) 
					{
						singlePostData= new PostData();
						singlePostData.setPostId(resultSet.getString(1));
						singlePostData.setUserId(resultSet.getString(2));
						singlePostData.setParentPost(resultSet.getString(3));
						singlePostData.setPostData(resultSet.getString(4));
						singlePostData.setImagePath(resultSet.getString(5));
						singlePostData.setAbuseCount(resultSet.getInt(6));
						singlePostData.setLikeCount(resultSet.getInt(7));
						singlePostData.setCreateTime(sdfOut.format(sdf.parse(resultSet.getString(8))));
						singlePostData.setAdminFlag(resultSet.getInt(9));
						singlePostData.setFirstname(resultSet.getString(10));
						singlePostData.setLastname(resultSet.getString(11));
						singlePostData.setStatus(resultSet.getString(12));
						singlePostData.setProfleImage(resultSet.getString(13));
																
						posts.add(singlePostData);
					}// end of while
					
				}// end of if(resultSet!=null)

		         GenericChartData data = new GenericChartData();
		         JSONArray array = new JSONArray();
		         for (int i = 0; i < posts.size(); i++)
		         {
		        	 posts.get(i).prepareResponse();
		            array.put(posts.get(i));
		         }
		         data.getText().add("posts");
		         data.getValues().add(array);
		         response.setResponseObject(data);
		         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
		         return RuleDecisionKey.ON_SUCCESS;
				
		    }//end of try
		    catch (SQLException e)
		      {
		         log.error("DB error while fetching list of posts", e);
		         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "DB error while fetching posts");
		         return RuleDecisionKey.ON_FAILURE;
		      }
		      
		      catch(Exception e){
    	         log.error("Error while fetching list of threads", e);
    	         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching posts");
    	         return RuleDecisionKey.ON_FAILURE;
		    }
		    finally
		      {
		         if (resultSet != null)
		         {
		            try
		            {
		            	resultSet.close();
		            }
		            catch (SQLException e)
		            {
		               log.error("Error while closing result set while fetching post list", e);
		            }
		         }
		         DbConnectionManager.releaseConnection(dbConnName, connection);
		      }
	      
	   }
}
