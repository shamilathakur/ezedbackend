package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.a.helper.utils.StringSplitter;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.utils.EzedHelper;

public class UniversityUpdater extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public UniversityUpdater(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      response.setStringOnlySend(true);

      String newUniversityValue = messageData.getParameter("value");
      if (newUniversityValue == null || newUniversityValue.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("New university value is invalid " + newUniversityValue);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "");
         return RuleDecisionKey.ON_FAILURE;
      }
      int colNo = 0;
      try
      {
         colNo = Integer.parseInt(messageData.getParameter("columnid"));
      }
      catch (Exception e)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Column no is invalid in request", e);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "");
         return RuleDecisionKey.ON_FAILURE;
      }

      String origRowData = messageData.getParameter("rowdata");
      if (origRowData == null || origRowData.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Orig row data is invalid " + origRowData);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "");
         return RuleDecisionKey.ON_FAILURE;
      }

      String[] origRowStrings = StringSplitter.splitString(origRowData, "~~");
      if (origRowStrings.length < 7)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Orig row data is invalid since its length is not proper " + origRowData);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "");
         return RuleDecisionKey.ON_FAILURE;
      }

      String univId = origRowStrings[1];
      if (univId == null || univId.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("University id in request is not proper " + univId);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "");
         return RuleDecisionKey.ON_FAILURE;
      }

      if (colNo == 3)
      {
         if (EzedHelper.getTenDigMobNo(newUniversityValue.trim()) == null)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Ph no to be updated for university is not valid " + newUniversityValue);
            }
            response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "");
            return RuleDecisionKey.ON_FAILURE;
         }
         else
         {
            if (newUniversityValue.startsWith(" ") && newUniversityValue.trim().length() == 12)
            {
               newUniversityValue = newUniversityValue.trim() + "+";
            }
         }
      }

      origRowStrings[colNo] = newUniversityValue;

      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdateUniversity");
         ps.setString(1, origRowStrings[1]);
         ps.setString(2, origRowStrings[2]);
         ps.setString(3, origRowStrings[3]);
         ps.setString(4, univId);
         int rows = ps.executeUpdate();
         if (rows > 0)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug(rows + " rows updated in db while updating university name for id " + univId);
            }
            response.setParameters(EzedTransactionCodes.SUCCESS, newUniversityValue);
            return RuleDecisionKey.ON_SUCCESS;
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("University cannot be updated in request for university id " + univId);
            }
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "");
            return RuleDecisionKey.ON_FAILURE;
         }
      }
      catch (SQLException e)
      {
         logger.error("DB error while updating university name for id " + univId, e);
         if (e.getMessage().toUpperCase().contains("WEBSITE"))
         {
            response.setParameters(EzedTransactionCodes.WEBSITE_NAME_REPEATED, "");
         }
         else if (e.getMessage().toUpperCase().contains("PHONE"))
         {
            response.setParameters(EzedTransactionCodes.PH_NO_REPEATED, "");
         }
         else
         {
            response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "");
         }
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while updating university name for id " + univId, e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
}
