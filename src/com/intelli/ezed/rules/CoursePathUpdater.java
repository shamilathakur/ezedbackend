package com.intelli.ezed.rules;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.CoursePathData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class CoursePathUpdater extends SingletonRule
{

   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;
   private String processorName;
   private String deleteProcessorName;

   public CoursePathUpdater(String ruleName, String dbConnName, String processorName, String deleteProcessorName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
      this.processorName = processorName;
      this.deleteProcessorName = deleteProcessorName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      CoursePathData data = (CoursePathData) flowContext.getFromContext(EzedKeyConstants.COURSE_PATH_DATA);
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      //AdminProfile adminProfile = (AdminProfile)flowContext.getFromContext(EzedKeyConstants.ADMIN_PROFILE);

      String courseId = data.getCourseId();

      try
      {
         data.fetchExistingLearningPathDetails(dbConnName, logger);
      }
      catch (SQLException e1)
      {
         logger.error("Exception occurred while fetching old course path from the database.", e1);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while processing change course path");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e1)
      {
         logger.error("Exception occurred while fetching old course path from the database.", e1);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while processing change course path");
         return RuleDecisionKey.ON_FAILURE;
      }

      Connection connection = DbConnectionManager.getConnectionByName(dbConnName);

      try
      {
         //delete from coursepath where courseid = ?;
         PreparedStatement deleteLerningPath = DbConnectionManager.getPreparedStatement(dbConnName, connection, "DeleteCoursePath");
         deleteLerningPath.setString(1, courseId);
         int i = deleteLerningPath.executeUpdate();
         if (i == 0)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("No course path found for course id = " + courseId);
            }
         }
      }
      catch (Exception e)
      {
         //e.printStackTrace();
         logger.error("Exception occoured while deleting old course path from the database.", e);
      }

      try
      {
         int j = 1;
         for (int i = 0; i < data.getChapterIdList().size(); i++)
         {
            String chapterId = data.getChapterIdList().get(i);

            //Insert into coursepath (courseid,chapterid,sequenceno,testid) values(?,?,?,?)
            PreparedStatement addCoursePath = DbConnectionManager.getPreparedStatement(dbConnName, connection, "InsertCoursePath");
            addCoursePath.setString(1, courseId);

            if (data.getChapterTypeFlagList().get(i).compareTo(CoursePathData.TYPE_TEST) == 0)
            {
               addCoursePath.setString(2, null);
               addCoursePath.setFloat(3, j++);
               addCoursePath.setString(4, chapterId);
            }
            else
            {
               addCoursePath.setString(2, chapterId);
               addCoursePath.setInt(3, j++);
               addCoursePath.setString(4, null);
            }
            int k = addCoursePath.executeUpdate();

            if (k != 1)
            {
               if (logger.isInfoEnabled())
               {
                  logger.info("Some problem while adding coursepath path to the database : " + courseId + " content id : " + chapterId);
               }
            }
            else
            {
               if (logger.isInfoEnabled())
               {
                  logger.info("Added chapter/test : " + chapterId + " to the course path of chapter : " + courseId + " at : " + j);
               }
            }
         }
         data.startGoalModificationHandler(processorName);
         data.startGoalDeletionHandlerHandler(deleteProcessorName);
      }
      catch (SQLException e)
      {
         logger.error("Exception occoured : ", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Database exception occoured while updating the course path.");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Exception occoured : ", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal Error occoured while updating the course path.");
         return RuleDecisionKey.ON_FAILURE;
      }


      response.setParameters(EzedTransactionCodes.SUCCESS, "Course Path updated successfully for chapterid : " + courseId);

      return RuleDecisionKey.ON_SUCCESS;
   }

}
