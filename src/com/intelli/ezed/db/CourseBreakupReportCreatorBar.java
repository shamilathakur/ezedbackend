package com.intelli.ezed.db;

import java.sql.SQLException;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.analytics.data.CourseBreakupReportDataBar;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class CourseBreakupReportCreatorBar extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public CourseBreakupReportCreatorBar(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      String courseId = messageData.getParameter("courseid");
      if (courseId == null || courseId.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Course id in request is invalid " + courseId);
         }
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Invalid course id in request");
         return RuleDecisionKey.ON_FAILURE;
      }

      String chapterIdString = messageData.getParameter("chapterid");
      if (chapterIdString == null || chapterIdString.trim().compareTo("") == 0)
      {
         chapterIdString = null;
      }

      CourseBreakupReportDataBar data = new CourseBreakupReportDataBar();
      data.setCourseId(courseId);
      data.setChapterId(chapterIdString);

      try
      {
         data.fetchReport(dbConnName, logger);
         data.prepareChart();
         response.setResponseObject(data);
         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
         response.setStatusCodeSend(false);
         return RuleDecisionKey.ON_SUCCESS;
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching course breakup report", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching report");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching course breakup report", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching report");
         return RuleDecisionKey.ON_FAILURE;
      }
   }
}
