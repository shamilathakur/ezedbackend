package com.intelli.ezed.bulkupload.rules;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.bulkupload.data.ContentDetails;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.TestQuestion;

public class ContentReaderDumper extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.BULK_UPLOAD_LOG);
   private String dbConnName;
   private String rootContentDir;
   private String contentDetailsFileName;
   private String quizDetailsFileName;

   public ContentReaderDumper(String ruleName, String dbConnName, String rootContentDir, String contentDetailsFileName,
                              String quizDetailsFileName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
      this.rootContentDir = rootContentDir;
      this.contentDetailsFileName = contentDetailsFileName;
      this.quizDetailsFileName = quizDetailsFileName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;

      File file = new File(rootContentDir);
      if (!file.exists())
      {
         file.mkdir();
      }
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchContentDetails");
         rs = ps.executeQuery();
         ArrayList<ContentDetails> datas = new ArrayList<ContentDetails>();
         ContentDetails data = null;
         while (rs.next())
         {
            data = new ContentDetails();
            data.setContentId(rs.getString("contentid"));
            data.setContentName(rs.getString("contentname"));
            data.setContentType(rs.getInt("contenttype"));
            data.setDesc(rs.getString("description"));
            data.setHours(rs.getInt("hours"));
            data.writeToFile(rootContentDir, contentDetailsFileName);
            if (data.getContentType() == ContentDetails.CONTENT_QUIZ)
            {
               ArrayList<TestQuestion> testQuestions = data.fetchQuiz(dbConnName, logger);
               if (testQuestions.size() > 0)
               {
                  data.writeQuiz(rootContentDir, quizDetailsFileName, testQuestions);
               }
            }
            datas.add(data);
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching content details from db", e);
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (IOException e)
      {
         logger.error("Error while fetching content details from db", e);
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching content details from db", e);
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
      return RuleDecisionKey.ON_SUCCESS;
   }

}
