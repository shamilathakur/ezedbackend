package com.intelli.ezed.db;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.CourseListData;
import com.intelli.ezed.data.CourseListForPaging;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.FrontendFetchData;
import com.intelli.ezed.data.Response;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.FrontendFetchData;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.utils.EzedHelper;

public class SearchStringPurchasedCoursesFectherM extends SingletonRule{

	private String dbConnName;
	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
	
	public SearchStringPurchasedCoursesFectherM(String ruleName, String dbConnName) {
		super(ruleName);
		this.dbConnName = dbConnName;
	}
	
	@Override
	public RuleDecisionKey executeRule(FlowContext flowContext) {

		Response response = (Response) flowContext
				.getFromContext(EzedKeyConstants.EZED_RESPONSE);
		MessageData messageData = (MessageData) flowContext
				.getFromContext(MessageData.class.getName());
		

		String pageno = messageData.getParameter("pageno");
	     if(pageno == null || pageno.trim().compareTo("")==0){
	    	 if(logger.isDebugEnabled()){
	    		 logger.debug("page no in request is invalid");
	    	 }
	    	 response.setParameters(EzedTransactionCodes.PROCESSING_REQUEST, "Invalid Page No");
	    	 return RuleDecisionKey.ON_FAILURE;
	     }
	     
	     String noOfRecordsOnAPage = messageData.getParameter("noofrecords");
	     if(noOfRecordsOnAPage == null || noOfRecordsOnAPage.trim().compareTo("")==0){
	    	 if(logger.isDebugEnabled()){
	    		 logger.debug("page no in request is invalid");
	    	 }
	    	 response.setParameters(EzedTransactionCodes.PROCESSING_REQUEST, "Invalid No Of Records");
	    	 return RuleDecisionKey.ON_FAILURE;
	     }
	     int limit = Integer.parseInt(noOfRecordsOnAPage);
	     int pagenumber = Integer.parseInt(pageno);
	     int offset = (Integer.parseInt(pageno) -1) * limit;
	     String requesttype = messageData.getParameter("requesttype") ;
	     String userid = messageData.getParameter("email");
		
		final String query = 
				
				"SELECT "
				+ "distinct csl.courseid,csl.chaptercount,"
				+ "csl.videocount,csl.questioncount,cl.coursename,cl.courseimage"
				+ " from courselist cl,coursestatlist csl,  coursepath cp "
				+ "  where cl.publishflag =1  "
				+ "and cl.courseid = csl.courseid  "
				 +" and cl.courseid = cp.courseid "
				+"  and ( cl.coursename ~* '"+messageData.getParameter("searchString")+"' OR cp.chapterid in "
				+"( select chapterid from chapterlist where chaptername  ~* '"+messageData.getParameter("searchString")+"'))";
		
		
		final String queryForWebBrowser = 
				"SELECT "
						+ " distinct csl.courseid,csl.chaptercount, cl.shortdesc, cl.longdesctype, cl.longdescstring, cl.longdesccontent, "
						+ "csl.videocount,csl.questioncount,cl.coursename,cl.courseimage"
						+ " from courselist cl,coursestatlist csl,  coursepath cp "
						+ "  where cl.publishflag =1  "
						+ "and cl.courseid = csl.courseid  "
						 +" and cl.courseid = cp.courseid "
						+"  and ( cl.coursename ~* '"+messageData.getParameter("searchString")+"' OR cp.chapterid in "
						+"( select chapterid from chapterlist where chaptername  ~* '"+messageData.getParameter("searchString")+"'))";
				
		final String queryForAdmin = 
				"SELECT "
						+ " distinct csl.courseid,csl.chaptercount, cl.shortdesc, cl.longdesctype, cl.longdescstring, cl.longdesccontent, "
						+ "csl.videocount,csl.questioncount,cl.coursename,cl.courseimage"
						+ " from courselist cl,coursestatlist csl,  coursepath cp "
						+ "and cl.courseid = csl.courseid  "
						 +" and cl.courseid = cp.courseid "
						+"  and ( cl.coursename ~* '"+messageData.getParameter("searchString")+"' OR cp.chapterid in "
						+"( select chapterid from chapterlist where chaptername  ~* '"+messageData.getParameter("searchString")+"'))";
				
		
		StringBuilder sb = new StringBuilder("");
		sb.append(" order by courseid asc offset " + Integer.toString(offset) + " limit " + Integer.toString(limit));

		System.out.println("String buffer : " + sb.toString());
		String finalQuery = null;
		if(requesttype != null && requesttype.equalsIgnoreCase("web")){
			finalQuery = queryForWebBrowser;
		}
		else if(requesttype !=null && requesttype.equalsIgnoreCase("admin")){
			finalQuery = queryForAdmin;
		}
		else{
			finalQuery = query;
		}
		if (userid != null){
			finalQuery = finalQuery + " and cl.courseid NOT IN ( select courseid from usercourse where userid = '" + userid + "') ";
		}
		if (!sb.equals("")) {
			finalQuery = finalQuery + sb.toString();
		}
		System.out.println("finalquery "+ finalQuery);
		Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
		PreparedStatement ps = null;
		ResultSet rs = null;

		 ArrayList<CourseListData> courseList = new ArrayList<CourseListData>();
	     CourseListForPaging courseListForPaging = new CourseListForPaging();
		try {
			ps = conn.prepareStatement(finalQuery);
			System.out.println("Final Query : " + ps);
			rs = ps.executeQuery();
			while (rs.next()) {
				 CourseListData  courseListData = new CourseListData();
	         		courseListData.setCourseid(rs.getString("courseid"));
	         		courseListData.setChaptercount(rs.getInt("chaptercount"));
	         		courseListData.setQuestioncount(rs.getInt("questioncount"));
	         		courseListData.setVideocount(rs.getInt("videoCount"));
	         		courseListData.setCoursename(rs.getString("coursename"));
	         		courseListData.setCourseimage(rs.getString("courseimage"));	 
	        		if(requesttype != null && requesttype.equalsIgnoreCase("web")){
	         			courseListData.setShortdesc(rs.getString("shortdesc"));
	         			courseListData.setLongdesctype(rs.getString("longdesctype"));
	         			courseListData.setLongdescstring(rs.getString("longdescstring"));
	         			courseListData.setLongdesccontent(rs.getString("longdesccontent"));
	         		}
	         		courseList.add(courseListData);
	         		
			}
			 courseListForPaging.setCourseListData(courseList);
			 courseListForPaging.setLimit(limit);
			 courseListForPaging.setPageno(pagenumber);
			 courseListForPaging.setOffset(offset);
		} catch (SQLException e) {
			logger.error("Error while fetching purchased course details for given search string from db : ", e);
			response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED,
					"Error while fetching purchased course for given search string" + e.getMessage());
			return RuleDecisionKey.ON_FAILURE;
		} catch (Exception e) {
			logger.error("Internal Error while fetching purchased course details : ", e);
			response.setParameters(EzedTransactionCodes.INTERNAL_ERROR,
					"Error while fetching purchased course details for given search string" + e.getMessage());
			return RuleDecisionKey.ON_FAILURE;
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					logger.error(
							"Error while closing result set while fetching purchased course list for given search string",
							e);
				}
			}
			DbConnectionManager.releaseConnection(dbConnName, conn);
		}
		 response.setResponseObject(courseListForPaging);
	     response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
		 return RuleDecisionKey.ON_SUCCESS;	}

}
