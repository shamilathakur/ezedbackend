package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.ActionData;
import com.intelli.ezed.data.EventData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.MasterActionRecords;

public class UserEventProcessor extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public UserEventProcessor(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      EventData eventData = (EventData) flowContext.getFromContext(EzedKeyConstants.EVENT_OBJECT);
      //      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      String userid = (String) flowContext.getFromContext(EzedKeyConstants.USER_ID);
      ActionData actionData = MasterActionRecords.fetchRecord(eventData.getActionId());
      boolean isUserPointsToBeAdded = false;
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      if (actionData.getActionType() == ActionData.ONE_TIME_ACTION)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Action is a one time action. First checking count from db");
         }
         if (actionData.getPoints() != 0)
         {
            try
            {
               ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchUserEventCount");
               ps.setString(1, userid);
               ps.setString(2, eventData.getActionId());
               rs = ps.executeQuery();
               if (rs.next())
               {
                  int count = rs.getInt(1);
                  if (count == 1)
                  {
                     isUserPointsToBeAdded = true;
                  }
               }
               else
               {
                  if (logger.isDebugEnabled())
                  {
                     logger.debug("No entry found in db for userid " + userid + " and action id " + eventData.getActionId());
                  }
               }
            }
            catch (SQLException e)
            {
               logger.error("Error while fetching count from event db for userid " + userid + " and action id " + eventData.getActionId(), e);
            }
            catch (Exception e)
            {
               logger.error("Error while fetching count from event db for userid " + userid + " and action id " + eventData.getActionId(), e);
            }
            finally
            {
               if (rs != null)
               {
                  try
                  {
                     rs.close();
                  }
                  catch (SQLException e)
                  {
                  }
               }
               DbConnectionManager.releaseConnection(dbConnName, conn);
            }
         }
      }
      else
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Action is a repeatable action. Directly adding points");
         }
         isUserPointsToBeAdded = true;
      }

      if (isUserPointsToBeAdded)
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = null;
         try
         {
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdateUserPoints");
            ps.setInt(1, actionData.getPoints());
            ps.setString(2, userid);
            int rows = ps.executeUpdate();
            if (logger.isDebugEnabled())
            {
               logger.debug(rows + " rows updated in db while incrementing points for userid " + userid);
            }
         }
         catch (SQLException e)
         {
            logger.error("Error while incrementing points for userid " + userid, e);
         }
         catch (Exception e)
         {
            logger.error("Error while incrementing points for userid " + userid, e);
         }
         finally
         {
            DbConnectionManager.releaseConnection(dbConnName, conn);
         }
      }

      return RuleDecisionKey.ON_SUCCESS;
   }
}
