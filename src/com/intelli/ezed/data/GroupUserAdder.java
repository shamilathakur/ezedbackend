package com.intelli.ezed.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.regex.Matcher;

import org.sabre.db.DbConnectionManager;
import org.slf4j.Logger;

import com.a.helper.utils.StringSplitter;
import com.intelli.ezed.utils.Globals;

public class GroupUserAdder
{
   private String groupId;
   private String[] users;
   private String userString;

   public String getGroupId()
   {
      return groupId;
   }

   public void setGroupId(String groupId)
   {
      this.groupId = groupId;
   }

   public String[] getUsers()
   {
      return users;
   }

   public void setUsers(String[] users)
   {
      this.users = users;
   }

   public String getUserString()
   {
      return userString;
   }

   public void setUserString(String userString) throws Exception
   {
      this.userString = userString;
      String[] userArr = StringSplitter.splitString(userString, ",");
      Matcher matcher = null;
      for (int i = 0; i < userArr.length; i++)
      {
         matcher = Globals.emailPattern.matcher(userArr[i]);
         if (!matcher.matches())
         {
            throw new Exception("User " + userArr[i] + " has invalid user id");
         }
      }
      this.setUsers(userArr);
   }

   public void persistGroupUsers(String dbConnName, Logger logger) throws SQLException, Exception
   {
      Connection conn = null;
      PreparedStatement ps = null;
      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         for (String singleUser : getUsers())
         {
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertGroupUsers");
            ps.setString(1, getGroupId());
            ps.setString(2, singleUser);
            int rows = ps.executeUpdate();
            if (logger.isDebugEnabled())
            {
               logger.debug(rows + " rows inserted in db while inserting " + groupId + singleUser);
            }
         }
      }
      catch (SQLException e)
      {
         if (e.getMessage().toUpperCase().contains("UNIQUE"))
         {
            logger.error("Unique error while inserting group id " + getGroupId(), e);
         }
         else
         {
            logger.error("DB error while inserting for group id " + getGroupId(), e);
            throw e;
         }
      }
      catch (Exception e)
      {
         logger.error("Error while inserting for group id " + getGroupId(), e);
         throw e;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   public void deleteGroupUsers(String dbConnName, Logger logger) throws SQLException, Exception
   {
      Connection conn = null;
      PreparedStatement ps = null;
      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         for (String singleUser : getUsers())
         {
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "DeleteGroupUsers");
            ps.setString(1, getGroupId());
            ps.setString(2, singleUser);
            int rows = ps.executeUpdate();
            if (logger.isDebugEnabled())
            {
               logger.debug(rows + " rows inserted in db while deleting " + groupId + singleUser);
            }
         }
      }
      catch (SQLException e)
      {
         logger.error("DB error while deleting for group id " + getGroupId(), e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while deleting for group id " + getGroupId(), e);
         throw e;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   public void deleteGroup(String dbConnName, Logger logger) throws SQLException, Exception
   {
      Connection conn = null;
      PreparedStatement ps = null;
      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "DeleteGroup");
         ps.setString(1, getGroupId());
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in db while deleting entire group " + groupId);
         }
      }
      catch (SQLException e)
      {
         logger.error("DB error while deleting entire group for group id " + getGroupId(), e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while deleting entire group for group id " + getGroupId(), e);
         throw e;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }


}
