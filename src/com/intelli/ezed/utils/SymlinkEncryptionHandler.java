package com.intelli.ezed.utils;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.axis.encoding.Base64;
import org.sabre.utils.HexUtil;

import com.a.helper.data.DataFiller;
import com.a.helper.session.SessionIdGenerator;


public class SymlinkEncryptionHandler
{

   private static Cipher internalEncCipher = null;
   private static Cipher internalDecCipher = null;

   private static String key = "NDRCNDQ3MzVCM0E3QkZGOA==";
   public static void initializeCipher() throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, NumberFormatException, IllegalBlockSizeException, BadPaddingException
   {
      IvParameterSpec ivInt = new IvParameterSpec(new byte[8]);
      byte[] zpkBytesInt = HexUtil.hex2data(new String(Base64.decode(key)));
      SecretKey zpkKeySpecInt = new SecretKeySpec(zpkBytesInt, "DES");

      internalEncCipher = Cipher.getInstance("DES/CBC/NoPadding");
      internalDecCipher = Cipher.getInstance("DES/CBC/NoPadding");

      internalEncCipher.init(Cipher.ENCRYPT_MODE, zpkKeySpecInt, ivInt);
      internalDecCipher.init(Cipher.DECRYPT_MODE, zpkKeySpecInt, ivInt);
   }

   public static String getDecryptedKey(String key) throws NumberFormatException, IllegalBlockSizeException, BadPaddingException
   {
      byte[] cipherText = internalDecCipher.doFinal(HexUtil.hex2data(key));
      return HexUtil.data2hex(cipherText);
   }

   public static String getEncryptedKey(String key) throws NumberFormatException, IllegalBlockSizeException, BadPaddingException
   {
      byte[] cipherText = internalEncCipher.doFinal(HexUtil.hex2data(key));
      return HexUtil.data2hex(cipherText);
   }
   
   public static void main(String[] args)
   {
      try
      {
         SymlinkEncryptionHandler.initializeCipher();
         //         System.out.println(SymlinkEncryptionHandler.getEncryptedKey("0000000000000001"));
         //         System.out.println(SymlinkEncryptionHandler.getEncryptedKey("0000000000000001"));
         //         System.out.println(SymlinkEncryptionHandler.getDecryptedKey("32af317e1b20df96"));
         //         System.out.println(SymlinkEncryptionHandler.getDecryptedKey("de56f1655fac7987"));
         //         System.out.println(SymlinkEncryptionHandler.getDecryptedKey("a11ae2e05a202d65"));
         //         System.out.println(SymlinkEncryptionHandler.getDecryptedKey("cfaa3cb931ba2b62"));
         //         System.out.println(SymlinkEncryptionHandler.getDecryptedKey("536a4ea116efc177"));
         //         System.out.println(DataFiller.getLeftZeroPadding(SessionIdGenerator.getSessionId("abc"), 2));
         //         System.out.println(DataFiller.getLeftZeroPadding(SessionIdGenerator.getSessionId("abc"), 2));
         //         System.out.println(DataFiller.getLeftZeroPadding(SessionIdGenerator.getSessionId("abc"), 2));
         //         System.out.println(DataFiller.getLeftZeroPadding(SessionIdGenerator.getSessionId("abc"), 2));
         System.out.println(SymlinkEncryptionHandler.getDecryptedKey("ceb4e2cc6e517a09"));
         System.out.println(SymlinkEncryptionHandler.getDecryptedKey("40e44773acb8b58a"));
         System.out.println(SymlinkEncryptionHandler.getDecryptedKey("2f7632e5d5eed002"));
      }
      catch (InvalidKeyException e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
      catch (NumberFormatException e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
      catch (NoSuchAlgorithmException e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
      catch (NoSuchPaddingException e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
      catch (InvalidAlgorithmParameterException e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
      catch (IllegalBlockSizeException e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
      catch (BadPaddingException e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
   }
}
