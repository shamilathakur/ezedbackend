package com.intelli.ezed.db;

import java.sql.SQLException;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.GoalModificationData;

public class GoalDeletionHandler extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String actionId;

   public GoalDeletionHandler(String ruleName, String dbConnName, String actionId)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
      this.actionId = actionId;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      GoalModificationData data = (GoalModificationData) flowContext.getFromContext(EzedKeyConstants.GOAL_MODIFICATION_DATA);
      if (data.getContentIdList().size() == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("No contents to be added in goal modification data");
         }
         return RuleDecisionKey.ON_FAILURE;
      }
      try
      {
         data.handleGoalDeletions(dbConnName, logger, actionId);
      }
      catch (SQLException e)
      {
         logger.error("Error while deleting goals from calendar", e);
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while deleting goals from calendar", e);
         return RuleDecisionKey.ON_FAILURE;
      }

      return RuleDecisionKey.ON_SUCCESS;
   }

}
