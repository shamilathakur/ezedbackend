	package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.ContentDataForChapter;
import com.intelli.ezed.data.ContentDataForChapterList;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class ContentDetailsFromChapterFetcherM extends SingletonRule{
	
	private String dbConnName;
	   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);


	public ContentDetailsFromChapterFetcherM(String ruleName, String dbConnName) {
		super(ruleName);
		this.dbConnName = dbConnName;
	}

	@Override
	public RuleDecisionKey executeRule(FlowContext flowContext) {
		 Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
	     MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
		
	     String chapterId = messageData.getParameter("chapterid");
	     if(chapterId == null || chapterId.trim().compareTo("")==0){
	    	 if(logger.isDebugEnabled()){
	    		 logger.debug("chapter id in request is invalid");
	    	 }
	    	 response.setParameters(EzedTransactionCodes.INVALID_CHAPTER_ID, "Invalid Chapter Id");
	    	 return RuleDecisionKey.ON_FAILURE;
	     }
	     Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
	     ArrayList<ContentDataForChapter> contentForChapterList = new ArrayList<ContentDataForChapter>();
	     ContentDataForChapterList contentChapterListObject = new ContentDataForChapterList();
	     PreparedStatement ps = null;
	     ResultSet rs = null;
	     try{
		     conn = DbConnectionManager.getConnectionByName(dbConnName);
	         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchContentDetailsFromChapterId");
	         ps.setString(1, chapterId);
	         rs = ps.executeQuery();
	         while(rs.next())
	         	{
	        	 ContentDataForChapter  contentforChapter = new ContentDataForChapter();
	         		contentforChapter.setContentId(rs.getString("contentid"));
	         		contentforChapter.setContentName(rs.getString("contentName"));
	         		contentforChapter.setContentLink(rs.getString("link"));
	         		contentforChapter.setContentType(rs.getInt("contenttype"));
	         		contentforChapter.setSequenceno(rs.getInt("sequenceno"));
	         		contentforChapter.setContentshowflag(rs.getString("contentshowflag"));
	         		
	         		contentForChapterList.add(contentforChapter);
	         		
	         	}
	         contentChapterListObject.setChapterId(chapterId);
	         contentChapterListObject.setContentDataForChapter(contentForChapterList);
	     }
         catch (SQLException e)
         {
            logger.error("Error while fetching test details", e);
            response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "PL SQL Error while fetching ContentDetailsFromChapterId : " + e.getMessage());
            return RuleDecisionKey.ON_FAILURE;
         }
         catch (Exception e)
         {
            logger.error("Error while fetching test details", e);
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching ContentDetailsFromChapterId : " + e.getMessage());
            return RuleDecisionKey.ON_FAILURE;
         }
         finally
         {
            if (rs != null)
            {
               try
               {
                  rs.close();
               }
               catch (SQLException e)
               {
               }
            }
            DbConnectionManager.releaseConnection(dbConnName, conn);
         }
		 response.setResponseObject(contentChapterListObject);
	     response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
		 return RuleDecisionKey.ON_SUCCESS;
		
}

}
