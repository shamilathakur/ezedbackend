package com.intelli.ezed.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.a.helper.utils.StringSplitter;

public class EndDateCalculator
{
   public static Map<Integer, String> monthMap = new HashMap<Integer, String>();
   static
   {
      monthMap.put(1, "JANUARY");
      monthMap.put(2, "FEBRUARY");
      monthMap.put(3, "MARCH");
      monthMap.put(4, "APRIL");
      monthMap.put(5, "MAY");
      monthMap.put(6, "JUNE");
      monthMap.put(7, "JULY");
      monthMap.put(8, "AUGUST");
      monthMap.put(9, "SEPTEMBER");
      monthMap.put(10, "OCTOBER");
      monthMap.put(11, "NOVEMBER");
      monthMap.put(12, "DECEMBER");
   }
   public static Date findEndDate(String endMonthsCommaSeparated, Date startDate)
   {
      return findEndDate(StringSplitter.splitString(endMonthsCommaSeparated, ","), startDate);
   }
   public static Date findEndDate(String[] endMonths, Date startDate)
   {
      Calendar startCal = Calendar.getInstance();
      startCal.setTime(startDate);
      int startMonth = startCal.get(Calendar.MONTH);

      int[] endMonthNo = new int[endMonths.length];
      int mindiff = 13;
      int correctIndex = 0;
      boolean isNextYear = false;
      boolean ifFound = false;
      int diff = 0;
      for (int i = 0; i < endMonthNo.length; i++)
      {
         try
         {
            endMonthNo[i] = Integer.parseInt(endMonths[i]) - 1;
         }
         catch (Exception e)
         {
            return startDate;
         }

         //if in case course ends in same month as the start date and we want the end
         //date of the course to be the end date of same month, put in less than and
         //equal to sign in the check below instead of only less than sign
         if (startMonth < endMonthNo[i])
         {
            diff = endMonthNo[i] - startMonth;
            if (diff < mindiff)
            {
               mindiff = diff;
               correctIndex = i;
               ifFound = true;
            }
         }
      }
      if (!ifFound)
      {
         isNextYear = true;
         correctIndex = findMinValueIndex(endMonthNo);
      }

      Calendar endCal = Calendar.getInstance();
      endCal.set(Calendar.MONTH, endMonthNo[correctIndex]);
      if (isNextYear)
      {
         endCal.set(Calendar.YEAR, startCal.get(Calendar.YEAR) + 1);
      }
      else
      {
         endCal.set(Calendar.YEAR, startCal.get(Calendar.YEAR));
      }
      endCal.set(Calendar.DATE, endCal.getActualMaximum(Calendar.DATE));
      endCal.set(Calendar.HOUR_OF_DAY, 0);
      endCal.set(Calendar.MINUTE, 0);
      endCal.set(Calendar.SECOND, 0);
      return endCal.getTime();
   }

   public static Date[] getEndDatesFromEndMonths(String endMonthsCommaSeparated, Date startDate)
   {
      return getEndDatesFromEndMonths(StringSplitter.splitString(endMonthsCommaSeparated, ","), startDate);
   }

   public static Date[] getEndDatesFromEndMonths(String[] endMonths, Date startDate)
   {
      Calendar startCal = Calendar.getInstance();
      startCal.setTime(startDate);

      int[] endMonthNo = new int[endMonths.length];
      Date[] endMonthDates = new Date[endMonths.length];
      Calendar endCal = Calendar.getInstance();
      for (int i = 0; i < endMonthNo.length; i++)
      {
         endCal = Calendar.getInstance();
         endMonthNo[i] = Integer.parseInt(endMonths[i]) - 1;
         endCal.set(Calendar.MONTH, endMonthNo[i]);
         endCal.set(Calendar.YEAR, startCal.get(Calendar.YEAR));
         endCal.set(Calendar.DATE, endCal.getActualMaximum(Calendar.DATE));
         endCal.set(Calendar.HOUR_OF_DAY, 0);
         endCal.set(Calendar.MINUTE, 0);
         endCal.set(Calendar.SECOND, 0);
         endMonthDates[i] = endCal.getTime();
      }
      return endMonthDates;
   }

   private static int findMinValueIndex(int[] arr)
   {
      int minVal = arr[0];
      int minIndex = 0;
      for (int i = 0; i < arr.length; i++)
      {
         if (arr[i] < minVal)
         {
            minVal = arr[i];
            minIndex = i;
         }
      }
      return minIndex;
   }

   public static void main(String[] args) throws ParseException
   {
      Date startDate = new SimpleDateFormat("yyyyMMddHHmmss").parse("20131206000000");
      System.out.println(findEndDate(new String[]{"5", "12"}, startDate));
   }
}
