package com.intelli.ezed.rules;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.a.helper.utils.StringSplitter;
import com.intelli.ezed.analytics.data.TestResponseOne;
import com.intelli.ezed.analytics.data.TestResposeTwo;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class DiffWiseQuestionReportCreatorDonut extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;

   public DiffWiseQuestionReportCreatorDonut(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = new Response();
      flowContext.putIntoContext(EzedKeyConstants.EZED_RESPONSE, response);

      String courseId = messageData.getParameter("courseid");

      if (courseId == null || courseId.equals("") || courseId.equals(" "))
      {
         logger.error("no courseid provided.");
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Please check the course id.");
         return RuleDecisionKey.ON_FAILURE;
      }


      Vector<TestResponseOne> testResponseOnes = new Vector<TestResponseOne>();


      Vector<Float> questionCounts = new Vector<Float>();
      
      Connection connection = DbConnectionManager.getConnectionByName(dbConnName);
      ResultSet rs = null;
      try
      {
         //select count(*),difficultylevel,chapterid from questionbank where chapterid in (select chapterid from coursepath where courseid = 'COU20000000136532823' and chapterid is not null) GROUP by difficultylevel,chapterid order by difficultylevel;
         PreparedStatement preparedStatement = DbConnectionManager.getPreparedStatement(dbConnName, connection, "SelectQuestionCountChapterWise");
         preparedStatement.setString(1, courseId);
         rs = preparedStatement.executeQuery();

         while (rs.next())
         {
            int count = rs.getInt("count");
            int diffLevel = Integer.parseInt(rs.getString("difficultylevel"));
            String chapterId = rs.getString("chapterid");

            questionCounts.add((float) count);
            
         }

      }
      catch (SQLException e)
      {
         logger.error("Database Exception occoured while getting question count.", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Please check the course id and try again.");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Exception occoured while fetching question counts", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal Error please try again later.");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         try
         {
            rs.close();
         }
         catch (Exception e)
         {
            logger.error("Exception while closing the result set.", e);
         }
         DbConnectionManager.releaseConnection(dbConnName, connection);
      }

      TestResponseOne one = new TestResponseOne();
      float[] f1 = new float[10];
//      for (int k = 0; k < f1.length; k++)
//      {
//         f1[k] = questionCounts[k];
//      }
//      one.setValues(f1);
//      one.setName(course);

      testResponseOnes.add(one);


      TestResponseOne[] testResponseOnesArray = new TestResponseOne[testResponseOnes.size()];
      for (int i = 0; i < testResponseOnes.size(); i++)
      {
         testResponseOnesArray[i] = testResponseOnes.get(i);
      }

      TestResposeTwo testResposeTwo = new TestResposeTwo();
      testResposeTwo.setTestResponseOnes(testResponseOnesArray);

      String chartType = "bar";
      String yAxisDisplayText = "Count";
      String xAxisDisplayText = "Level";
      String yAxisDisplayLable = "QuestionCount";
      String xAxisDisplayLable = "DifficultyLevels";
      String graphTitle = "Question Count";
      String[] cats = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};

      testResposeTwo.setCats(cats);
      testResposeTwo.setxAxisDisplayLable(xAxisDisplayLable);
      testResposeTwo.setxAxisDisplayText(xAxisDisplayText);
      testResposeTwo.setyAxisDisplayLable(yAxisDisplayLable);
      testResposeTwo.setyAxisDisplayText(yAxisDisplayText);
      testResposeTwo.setChartType(chartType);
      testResposeTwo.setGraphTitle(graphTitle);

      response.setResponseObject(testResposeTwo);
      response.setStatusCodeSend(false);

      return RuleDecisionKey.ON_SUCCESS;
   }
}
