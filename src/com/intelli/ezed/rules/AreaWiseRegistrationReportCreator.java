package com.intelli.ezed.rules;

import java.sql.SQLException;
import java.util.Vector;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.a.helper.utils.StringSplitter;
import com.intelli.ezed.analytics.data.AreaWiseRegReportData;
import com.intelli.ezed.analytics.data.TestResponseOne;
import com.intelli.ezed.analytics.data.TestResposeTwo;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class AreaWiseRegistrationReportCreator extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;

   public AreaWiseRegistrationReportCreator(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = new Response();
      flowContext.putIntoContext(EzedKeyConstants.EZED_RESPONSE, response);

      String city = messageData.getParameter("city");

      boolean fetchAll = false;

      if (city == null || city.equals("") || city.equals(" "))
      {
         fetchAll = true;
      }

      if (fetchAll)
      {
         AreaWiseRegReportData adminRegReportData = new AreaWiseRegReportData();
         try
         {
            adminRegReportData.calculateUserRegCounts(dbConnName, logger);
         }
         catch (SQLException e)
         {
            logger.error("Database Exception occoured while fetching user flags", e);
            response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Please check the userid and try again.");
            return RuleDecisionKey.ON_FAILURE;
         }
         catch (Exception e)
         {
            logger.error("Exception occoured while fetching user flags", e);
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal Error please try again later.");
            return RuleDecisionKey.ON_FAILURE;
         }

         TestResponseOne one = new TestResponseOne();
         float[] f1 = new float[5];
         f1[0] = adminRegReportData.getTotalUsersRegistered();
         f1[1] = adminRegReportData.getYesterdayUsersRegistered();
         f1[2] = adminRegReportData.getLastWeekUsersRegistered();
         f1[3] = adminRegReportData.getLastMonthUsersRegistered();
         f1[4] = adminRegReportData.getLastYearUsersRegistered();
         one.setValues(f1);
         one.setName("Total Registration Count");

         TestResposeTwo testResposeTwo = new TestResposeTwo();
         TestResponseOne[] testResponseOnes = new TestResponseOne[1];
         testResponseOnes[0] = one;
         testResposeTwo.setTestResponseOnes(testResponseOnes);

         String chartType = "bar";
         String yAxisDisplayText = "No. Of Users";
         String xAxisDisplayText = "Time Period";
         String yAxisDisplayLable = "Lable";
         String xAxisDisplayLable = "Lable";
         String[] cats = {"Total", "Yesterday", "Last Week", "Last Month", "Last Year"};

         testResposeTwo.setGraphTitle("City Wise Registration Report");
         testResposeTwo.setCats(cats);
         testResposeTwo.setxAxisDisplayLable(xAxisDisplayLable);
         testResposeTwo.setxAxisDisplayText(xAxisDisplayText);
         testResposeTwo.setyAxisDisplayLable(yAxisDisplayLable);
         testResposeTwo.setyAxisDisplayText(yAxisDisplayText);
         testResposeTwo.setChartType(chartType);

         response.setResponseObject(testResposeTwo);
         response.setStatusCodeSend(false);
      }
      else
      {
         if (city.trim().compareToIgnoreCase("notset") == 0)
         {
            city = "CIT1000001";
         }
         String[] userids = StringSplitter.splitString(city, ",");

         Vector<TestResponseOne> testResponseOnes = new Vector<TestResponseOne>();

         String cityName = null;
         for (String users : userids)
         {
            AreaWiseRegReportData adminRegReportData = new AreaWiseRegReportData();
            try
            {
               adminRegReportData.calculateUserRegCounts(dbConnName, logger, users);
               cityName = adminRegReportData.fetchCityName(dbConnName, logger, users);
            }
            catch (SQLException e)
            {
               logger.error("Database Exception occoured while fetching user flags", e);
               response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Please check the userid and try again.");
               return RuleDecisionKey.ON_FAILURE;
            }
            catch (Exception e)
            {
               logger.error("Exception occoured while fetching user flags", e);
               response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal Error please try again later.");
               return RuleDecisionKey.ON_FAILURE;
            }

            TestResponseOne one = new TestResponseOne();
            float[] f1 = new float[5];
            f1[0] = adminRegReportData.getTotalUsersRegistered();
            f1[1] = adminRegReportData.getYesterdayUsersRegistered();
            f1[2] = adminRegReportData.getLastWeekUsersRegistered();
            f1[3] = adminRegReportData.getLastMonthUsersRegistered();
            f1[4] = adminRegReportData.getLastYearUsersRegistered();
            one.setValues(f1);
            one.setName(cityName);

            testResponseOnes.add(one);

         }

         TestResponseOne[] testResponseOnesArray = new TestResponseOne[testResponseOnes.size()];
         for (int i = 0; i < testResponseOnes.size(); i++)
         {
            testResponseOnesArray[i] = testResponseOnes.get(i);
         }

         TestResposeTwo testResposeTwo = new TestResposeTwo();
         testResposeTwo.setTestResponseOnes(testResponseOnesArray);

         String chartType = "bar";
         String yAxisDisplayText = "No. Of Users";
         String xAxisDisplayText = "Time Period";
         String yAxisDisplayLable = "Lable";
         String xAxisDisplayLable = "Lable";
         String[] cats = {"Total", "Yesterday", "Last Week", "Last Month", "Last Year"};

         testResposeTwo.setGraphTitle("City Wise Registration Report");
         testResposeTwo.setCats(cats);
         testResposeTwo.setxAxisDisplayLable(xAxisDisplayLable);
         testResposeTwo.setxAxisDisplayText(xAxisDisplayText);
         testResposeTwo.setyAxisDisplayLable(yAxisDisplayLable);
         testResposeTwo.setyAxisDisplayText(yAxisDisplayText);
         testResposeTwo.setChartType(chartType);

         response.setResponseObject(testResposeTwo);
         response.setStatusCodeSend(false);

      }
      return RuleDecisionKey.ON_SUCCESS;
   }

}
