package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class VerificationIdFromDBGetter extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public VerificationIdFromDBGetter(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      String emailIdFromRequest = messageData.getParameter("email");

      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;

      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "GetVerificationId");
         ps.setString(1, emailIdFromRequest);

         rs = ps.executeQuery();
         if (rs.next())
         {
            String verificationIdFromDb = rs.getString(1);
            if (verificationIdFromDb == null || verificationIdFromDb.trim().compareTo("") == 0)
            {
               if (logger.isDebugEnabled())
               {
                  logger.debug("Verification id from db is null or blank");
               }
               response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Verification failed");
               return RuleDecisionKey.ON_FAILURE;
            }
            messageData.putParameter("verifdb", verificationIdFromDb);
            if (logger.isDebugEnabled())
            {
               logger.debug("Verification id from db : " + verificationIdFromDb);
            }
            return RuleDecisionKey.ON_SUCCESS;
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("No entry found in db against email id " + emailIdFromRequest);
            }
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Verification failed");
            return RuleDecisionKey.ON_FAILURE;
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching verification code from DB for email id " + emailIdFromRequest, e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Verification failed");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching verification code from DB for email id " + emailIdFromRequest, e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Verification failed");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing resultset", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
}
