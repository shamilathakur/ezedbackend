package com.intelli.ezed.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.sabre.db.DbConnectionManager;
import org.sabre.exceptions.InputProcessorException;
import org.sabre.workflow.LinkedProcessorManager;
import org.slf4j.Logger;

public class CoursePathData
{
   public static final String TYPE_TEST = "0";
   public static final String TYPE_CHAPTER = "1";
   private String courseId;
   private ArrayList<String> chapterIdList = new ArrayList<String>();
   private ArrayList<String> chapterTypeFlagList = new ArrayList<String>();
   private ArrayList<String> origChapterList = new ArrayList<String>();
   private ArrayList<String> origChapterTypeFlagList = new ArrayList<String>();
   public String getCourseId()
   {
      return courseId;
   }
   public void setCourseId(String courseId)
   {
      this.courseId = courseId;
   }
   public ArrayList<String> getChapterIdList()
   {
      return chapterIdList;
   }
   public void setChapterIdList(ArrayList<String> chapterIdList)
   {
      this.chapterIdList = chapterIdList;
   }

   public void setChapterIdList(String[] chapterIdList)
   {
      for (int i = 0; i < chapterIdList.length; i++)
      {
         this.getChapterIdList().add(chapterIdList[i]);
      }
   }
   public ArrayList<String> getChapterTypeFlagList()
   {
      return chapterTypeFlagList;
   }
   public void setChapterTypeFlagList(ArrayList<String> contentShowFlagList)
   {
      this.chapterTypeFlagList = contentShowFlagList;
   }
   public void setChapterTypeFlagList(String[] chapterTypeFlagList)
   {
      for (int i = 0; i < chapterTypeFlagList.length; i++)
      {
         if (chapterTypeFlagList[i].trim().compareTo(TYPE_CHAPTER) == 0)
         {
            this.getChapterTypeFlagList().add(TYPE_CHAPTER);
         }
         else
         {
            this.getChapterTypeFlagList().add(TYPE_TEST);
         }
      }
   }
   public ArrayList<String> getOrigChapterList()
   {
      return origChapterList;
   }
   public void setOrigChapterList(ArrayList<String> origChapterList)
   {
      this.origChapterList = origChapterList;
   }
   public ArrayList<String> getOrigChapterTypeFlagList()
   {
      return origChapterTypeFlagList;
   }
   public void setOrigChapterTypeFlagList(ArrayList<String> origChapterTypeFlagList)
   {
      this.origChapterTypeFlagList = origChapterTypeFlagList;
   }

   public void fetchExistingLearningPathDetails(String dbConnName, Logger logger) throws SQLException, Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchCoursePathForUpdate");
         ps.setString(1, getCourseId());
         rs = ps.executeQuery();
         String chapterId = null;
         String testId = null;
         while (rs.next())
         {
            chapterId = rs.getString("chapterid");
            testId = rs.getString("testid");
            if (chapterId != null)
            {
               this.getOrigChapterList().add(chapterId);
               this.getOrigChapterTypeFlagList().add(TYPE_CHAPTER);
            }
            else
            {
               this.getOrigChapterList().add(testId);
               this.getOrigChapterTypeFlagList().add(TYPE_TEST);
            }
         }
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {

            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   private ArrayList<GoalModificationData> calculateExtraChapters()
   {
      ArrayList<GoalModificationData> datas = new ArrayList<GoalModificationData>();
      GoalModificationData data = null;
      String currentChapter = null;
      String currentChapterTypeFlag = null;
      boolean chapterMatches = false;
      for (int i = 0; i < getChapterIdList().size(); i++)
      {
         currentChapter = getChapterIdList().get(i);
         currentChapterTypeFlag = getChapterTypeFlagList().get(i);
         chapterMatches = false;
         for (int j = 0; j < getOrigChapterList().size(); j++)
         {
            if (currentChapter.compareTo(getOrigChapterList().get(j)) == 0)
            {
               chapterMatches = true;
               break;
            }
         }
         if (!chapterMatches)
         {
            if (currentChapterTypeFlag.compareTo(TYPE_CHAPTER) == 0)
            {
               data = new GoalModificationData();
               data.setChapterId(currentChapter);
               data.getCourseIdList().add(getCourseId());
               datas.add(data);
            }
            else
            {
               data = new GoalModificationData();
               data.setChapterId(null);
               data.setTestId(currentChapter);
               data.getCourseIdList().add(getCourseId());
               datas.add(data);
            }
         }
      }
      return datas;
   }

   private ArrayList<GoalModificationData> calculateChaptersToBeDeleted()
   {
      ArrayList<GoalModificationData> datas = new ArrayList<GoalModificationData>();
      GoalModificationData data = null;
      String currentChapter = null;
      String currentChapterTypeFlag = null;
      boolean chapterMatches = false;
      for (int i = 0; i < getOrigChapterList().size(); i++)
      {
         currentChapter = getOrigChapterList().get(i);
         currentChapterTypeFlag = getOrigChapterTypeFlagList().get(i);
         chapterMatches = false;
         for (int j = 0; j < getChapterIdList().size(); j++)
         {
            if (currentChapter.compareTo(getChapterIdList().get(j)) == 0)
            {
               chapterMatches = true;
               break;
            }
         }
         if (!chapterMatches)
         {
            if (currentChapterTypeFlag.compareTo(TYPE_CHAPTER) == 0)
            {
               data = new GoalModificationData();
               data.setChapterId(currentChapter);
               data.getCourseIdList().add(getCourseId());
               datas.add(data);
            }
            else
            {
               data = new GoalModificationData();
               data.setChapterId(null);
               data.setTestId(currentChapter);
               data.getCourseIdList().add(getCourseId());
               datas.add(data);
            }
         }
      }
      return datas;
   }

   public void startGoalModificationHandler(String processorName) throws InputProcessorException
   {
      ArrayList<GoalModificationData> datas = calculateExtraChapters();
      Map<Object, Object> data = null;
      for (int i = 0; i < datas.size(); i++)
      {
         data = new HashMap<Object, Object>();
         data.put(EzedKeyConstants.GOAL_MODIFICATION_DATA, datas.get(i));
         LinkedProcessorManager.addTask(processorName, data);
      }
   }

   public void startGoalDeletionHandlerHandler(String processorName) throws InputProcessorException
   {
      ArrayList<GoalModificationData> datas = calculateChaptersToBeDeleted();
      Map<Object, Object> data = null;
      for (int i = 0; i < datas.size(); i++)
      {
         data = new HashMap<Object, Object>();
         data.put(EzedKeyConstants.GOAL_MODIFICATION_DATA, datas.get(i));
         LinkedProcessorManager.addTask(processorName, data);
      }
   }
}
