package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.axis.encoding.Base64;
import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.utils.StringSplitter;
import com.intelli.ezed.bulkupload.data.ContentDetails;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.QuestionContent;
import com.intelli.ezed.data.QuizRequestResponse;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.SymLinkObject;
import com.intelli.ezed.data.TestQuestionData;
import com.intelli.ezed.data.TestRequestResponse;
import com.intelli.ezed.utils.SymLinkManager;

public class QuizFetcher extends SingletonRule
{

   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;
   private String newLinkLocationNotes;
   private String newLinkLocationVideos;
   private String notesStartPoint;
   private String vidStartPoint;
   private boolean betaOverride = false;

   public QuizFetcher(String ruleName, String dbConnName, String newLinkLoc, String newLinkVideos,
                      String notesStartPoint, String vidStartPoint, String betaOverride)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
      this.newLinkLocationNotes = newLinkLoc;
      this.newLinkLocationVideos = newLinkVideos;
      this.notesStartPoint = notesStartPoint;
      this.vidStartPoint = vidStartPoint;
      this.betaOverride = Boolean.parseBoolean(betaOverride);
   }

   public QuizFetcher(String ruleName, String dbConnName, String newLinkLoc, String newLinkVideos,
                      String notesStartPoint, String vidStartPoint)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
      this.newLinkLocationNotes = newLinkLoc;
      this.newLinkLocationVideos = newLinkVideos;
      this.notesStartPoint = notesStartPoint;
      this.vidStartPoint = vidStartPoint;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      QuizRequestResponse quizResponse = (QuizRequestResponse) response.getResponseObject();

      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      SymLinkManager symLinkManager = (SymLinkManager) flowContext.getFromContext(EzedKeyConstants.SYMLINK_MANAGER);
      String userId = (String) flowContext.getFromContext(EzedKeyConstants.USER_ID);
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchQuizQuestions");
         ps.setString(1, quizResponse.getQuizId());
         rs = ps.executeQuery();
         ArrayList<TestRequestResponse> questionList = new ArrayList<TestRequestResponse>();
         TestRequestResponse singleQuestion = null;
         String explanationContent = null;
         while (rs.next())
         {
            StringBuffer correctAnsForFillInTheBlanks = new StringBuffer();
            singleQuestion = new TestRequestResponse();
            singleQuestion.setQuestiontype(rs.getInt("questionType"));
            singleQuestion.setQuestionNumberOfRandomQuestion(rs.getInt("questionnumber"));
            singleQuestion.setExplanation(new QuestionContent());
            singleQuestion.getExplanation().setType(rs.getInt("explanationtype"));
            singleQuestion.getExplanation().setContentString(rs.getString("explanation"));
            explanationContent = rs.getString("explanationcontent");
            if (explanationContent != null)
            {
               singleQuestion.getExplanation().setContentLoc(fetchSymLinkForContent(explanationContent, dbConnName, symLinkManager, userId));
            }
            int questionType = rs.getInt("questionType");

            //select question 
            QuestionContent question = new QuestionContent();
            int i = rs.getInt("questioncontenttype");
            question.setType(i); //type 0 = no data , 1 = string, 2 = content
            if (i == QuestionContent.CONTENT_ABSENT)
            {
               //set No content
            }
            else if (i == QuestionContent.STRING_TYPE)
            {
               question.setContentString(rs.getString("questionstring"));
            }
            else if (i == QuestionContent.CONTENT_TYPE)
            {
               question.setContentString(rs.getString("questioncontent"));
            }
            //Setting the question into the response object
            singleQuestion.setQuestion(question);

            //Creating a array list to put all the answers inside it
            ArrayList<QuestionContent> answers = new ArrayList<QuestionContent>();

            if (questionType == TestQuestionData.MTF_QUESTION_TYPE)
            {
               //Match state with capital;1;Delhi;West Bengal;Tamil Nadu;Maharashtra
               String mtfQuestionString = question.getContentString();
               String[] tokens = StringSplitter.splitString(mtfQuestionString, ";");

               //Setting new question string to the question part
               if (question.getType() == QuestionContent.CONTENT_TYPE)
               {
                  question.setContentString(fetchSymLinkForContent(tokens[0], dbConnName, symLinkManager, userId));
               }
               else
               {
                  question.setContentString(tokens[0]);
               }

               String mtfContentType = tokens[1];

               for (int k = 1; k <= 6; k++)
               {
                  QuestionContent ans = new QuestionContent();
                  i = rs.getInt("ans" + k + "type");
                  ans.setType(Integer.parseInt(mtfContentType));
                  if (i == QuestionContent.CONTENT_ABSENT)
                  {
                     //set No content
                  }
                  else
                  {
                     if (i == QuestionContent.STRING_TYPE)
                     {
                        String t1 = rs.getString("ans" + k + "string");
                        //                      ans.setContentString((mtfContentType + "*" + tokens[k + 1] + "*" + i + "*" + t1));
                        ans.setContentString((tokens[k + 1] + "~" + i + "~" + Base64.encode(t1.getBytes("UTF-8"))));
                     }
                     else if (i == QuestionContent.CONTENT_TYPE)
                     {
                        String t1 = rs.getString("ans" + k + "content");
                        t1 = fetchSymLinkForContent(t1, dbConnName, symLinkManager, userId);
                        //                      ans.setContentString((mtfContentType + "*" + tokens[k + 1] + "*" + "*" + t1));
                        ans.setContentString((tokens[k + 1] + "~" + i + "~" + t1));
                     }
                     //adding answers to array list
                     answers.add(ans);
                  }
               }

            }
            else
            {
               if (i == QuestionContent.CONTENT_TYPE)
               {
                  question.setContentString(fetchSymLinkForContent(question.getContentString(), dbConnName, symLinkManager, userId));
               }
               for (int k = 1; k <= 6; k++)
               {
                  QuestionContent ans = new QuestionContent();
                  i = rs.getInt("ans" + k + "type");
                  ans.setType(i);
                  if (i == QuestionContent.CONTENT_ABSENT)
                  {
                     //set No content
                  }
                  else
                  {
                     if (i == QuestionContent.STRING_TYPE)
                     {
                        String temp = rs.getString("ans" + k + "string");
                        ans.setContentString(temp);
                        if (questionType == TestQuestionData.FIB_QUESTION_TYPE)
                        {
                           correctAnsForFillInTheBlanks.append(temp).append(",");
                        }
                     }
                     else if (i == QuestionContent.CONTENT_TYPE)
                     {
                        ans.setContentString(rs.getString("ans" + k + "content"));
                        ans.setContentString(fetchSymLinkForContent(ans.getContentString(), dbConnName, symLinkManager, userId));
                     }
                     //adding answers to array list
                     answers.add(ans);
                  }
               }
            }

            //Adding Arraylist to Response
            singleQuestion.setAnswers(answers);
            singleQuestion.setCorrectAnswer(rs.getString("correctans"));
            questionList.add(singleQuestion);
         }
         if (questionList.size() == 0)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("No questions found against the quiz id " + quizResponse.getQuizId());
            }
            response.setParameters(EzedTransactionCodes.INVALID_TEST_ID, "No questions found for quiz");
            return RuleDecisionKey.ON_FAILURE;
         }
         quizResponse.setQuizQuestions(questionList);
         response.setParameters(EzedTransactionCodes.SUCCESS, "Quiz fetched successfully");
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching questions against the quiz id " + quizResponse.getQuizId(), e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Database error while fetching quiz questions");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching questions against the quiz id " + quizResponse.getQuizId(), e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching quiz questions");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing resultset while fetching quiz", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
      return RuleDecisionKey.ON_SUCCESS;
   }

   public String fetchSymLinkForContent(String contentId, String dbConnName, SymLinkManager symLinkManager, String userId) throws Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchContentDetailsForId");
      ResultSet rs = null;
      String newLink = null;
      try
      {
         ps.setString(1, contentId);
         rs = ps.executeQuery();
         if (rs.next())
         {
            String contentlink = rs.getString("link");
            int actualContentType = rs.getInt("contenttype");
            if (betaOverride)
            {
               return contentlink;
            }
            String linkLocation = null;
            boolean isContent = false;
            if (actualContentType == ContentDetails.CONTENT_NOTE || actualContentType == ContentDetails.CONTENT_PDF || actualContentType == ContentDetails.CONTENT_IMAGE || actualContentType == ContentDetails.CONTENT_IMAGE_1)
            {
               linkLocation = newLinkLocationNotes;
               isContent = true;
            }
            else
            {
               linkLocation = newLinkLocationVideos;
            }

            //session id is userid
            newLink = symLinkManager.getSymLink(userId, contentId, contentlink, logger, linkLocation);
            if (isContent)
            {
               int index = newLink.indexOf(notesStartPoint);
               newLink = newLink.substring(index + 1);
            }
            else
            {
               int index = newLink.indexOf(vidStartPoint);
               newLink = newLink.substring(index + 1);
               String fileType = newLink.substring(newLink.lastIndexOf('.') + 1);
               if (fileType.compareToIgnoreCase("mov") == 0)
               {
                  fileType = "mp4";
               }
               newLink = fileType + ":" + newLink;
            }
            SymLinkObject linkObj = new SymLinkObject();
            linkObj.setLink(newLink);
         }
         return newLink;
      }
      catch (Exception e)
      {
         throw e;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

}
