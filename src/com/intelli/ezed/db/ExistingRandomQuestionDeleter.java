package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.TestQuestionData;

public class ExistingRandomQuestionDeleter extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.RANDOM_QUESTION_LOG);
   private String dbConnName;

   public ExistingRandomQuestionDeleter(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      TestQuestionData testData = (TestQuestionData) flowContext.getFromContext(EzedKeyConstants.QUESTION_OBJECT);
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      try
      {
         if (testData.getTestQuestion() == null)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Test question was not properly read from db");
            }
            return RuleDecisionKey.ON_FAILURE;
         }
         PreparedStatement ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "DeleteRandomQuestions");
         ps.setString(1, testData.getTestQuestion().getQuestionid());
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows deleted from database after deleting random questions for question id " + testData.getTestQuestion().getQuestionid());
         }
      }
      catch (Exception e)
      {
         logger.error("Error while updating flag after deleting random questions for question id " + testData.getTestQuestion().getQuestionid(), e);
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
      return RuleDecisionKey.ON_SUCCESS;
   }

}
