package com.intelli.ezed.checks;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.db.EzedRequestData;

public class StartTestRequestInterpreter extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public StartTestRequestInterpreter(String ruleName)
   {
      super(ruleName);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      String userid = messageData.getParameter("email");
      String testId = messageData.getParameter("testid");
      String challengeUserId = messageData.getParameter("challengeuserid");
      String challengeId = messageData.getParameter("challengeid");


      if (userid == null || userid.equals("") || userid.equals(" "))
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Email address not provided.");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Please enter correct email id.");
         return RuleDecisionKey.ON_FAILURE;
      }

      if (testId == null || testId.equals("") || testId.equals(" "))
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("testId not provided.");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Please enter valid testId.");
         return RuleDecisionKey.ON_FAILURE;
      }

      if (challengeUserId == null || challengeUserId.trim().compareTo("") == 0)
      {
         challengeUserId = null;
      }

      if (challengeUserId != null)
      {
         flowContext.putIntoContext(EzedKeyConstants.CHALLENGE_USER_ID, challengeUserId);
      }
      
      if (challengeId == null || challengeId.trim().compareTo("") == 0)
      {
         challengeId = null;
      }

      if (challengeId != null)
      {
         flowContext.putIntoContext(EzedKeyConstants.CHALLENGE_ID, challengeId);
      }

      //Creating this new Data object so that we can keep incoming request data in this object and compare it with the fetched userprofile data
      EzedRequestData userProfile = new EzedRequestData();
      userProfile.setUserid(userid);
      userProfile.setTestId(testId);
      flowContext.putIntoContext(EzedRequestData.EZED_REQUEST_DATA, userProfile);
      //Setting userid so that we can fetch user profile later
      flowContext.putIntoContext(EzedKeyConstants.USER_ID, userid);

      return RuleDecisionKey.ON_SUCCESS;
   }
}
