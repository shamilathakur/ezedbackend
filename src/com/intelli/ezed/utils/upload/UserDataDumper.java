package com.intelli.ezed.utils.upload;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import com.ezed.utils.dbutil.DataFiller;

public class UserDataDumper
{

   public static void main(String[] args)
   {
      SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
      SimpleDateFormat sdfOut = new SimpleDateFormat("HH:mm:ss dd-MM-yyyy");
      Connection conn = getDbConnectionPostgres();
      PreparedStatement ps = getSelectPostStatement(conn);

      try
      {
         ResultSet rs = ps.executeQuery();
         int chkflag = 0;
         String chkString = null;
         String dateString = null;
         while (rs.next())
         {
            try
            {
               dateString = sdfOut.format(sdf.parse(rs.getString("createtime")));
            }
            catch (Exception e)
            {
               dateString = "";
            }
            chkflag = rs.getInt("chkflag");
            if (chkflag == 0)
            {
               chkString = "INACTIVE";
            }
            else if (chkflag == 2)
            {
               chkString = "ACTIVE";
            }
            else if (chkflag == 6)
            {
               chkString = "VALIDATED";
            }
            else
            {
               chkString = "UNKNOWN";
            }

            System.out.println(rs.getString("userid") + "," + rs.getString("firstname") + "," + rs.getString("lastname") + "," + chkString + "," + dateString);
         }
      }
      catch (SQLException e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
   }

   private static Connection getDbConnectionPostgres()
   {
      try
      {
         String url = "jdbc:postgresql://localhost:15432/ezed";
         Class.forName("org.postgresql.Driver");
         return DriverManager.getConnection(url, "postgres", "postgres");
      }
      catch (Exception e)
      {
         e.printStackTrace();
         System.out.println("Error initialising Postgres connection");
         System.exit(1);
         return null;
      }
   }

   private static PreparedStatement getSelectPostStatement(Connection conn)
   {
      try
      {
         return (conn.prepareStatement("select userid,firstname,lastname,chkflag,createtime from users"));
      }
      catch (Exception e)
      {
         System.out.println("Error initializing postgres statment");
         System.exit(1);
         return null;
      }
   }
}
