package com.intelli.ezed.data;

import java.util.ArrayList;

import org.json.JSONArray;

public class BannerDataList extends ResponseObject
{
   private ArrayList<BannerData> bannerData;
   
   public ArrayList<BannerData> getBannerData()
   {
      return bannerData;
   }
   public void setBannerData(ArrayList<BannerData> bannerData)
   {
      this.bannerData = bannerData;
   }

   @Override
   public void prepareResponse() throws Exception
   {
      JSONArray array = new JSONArray();
      
      for(BannerData data : bannerData)
      {
         data.prepareResponse();
         array.put(data);
      }
      
      this.put("banners", array);
   }
}
