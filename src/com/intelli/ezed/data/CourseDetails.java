package com.intelli.ezed.data;

import java.util.List;

public class CourseDetails implements ResponseData
{
   public static final int CONTENT_COURSE_TYPE = 1;
   public static final int CONTENT_TEST_TYPE = 2;
   public static final int CONTENT_BOTH_TYPE = 3;

   private String courseId;
   private List<String> coursePath;
   int i = 0;

   public List<String> getCoursePath()
   {
      return coursePath;
   }

   public void setCoursePath(List<String> coursePath)
   {
      this.coursePath = coursePath;
   }

   public String getNextChapterId()
   {
      ++i;
      return coursePath.get(i);
   }

   public String getPriviousChapterId()
   {
      --i;
      return coursePath.get(i);
   }

   public String getChapterId(int i)
   {
      return coursePath.get(i);
   }

   public String getCourseId()
   {
      return courseId;
   }

   public void setCourseId(String courseId)
   {
      this.courseId = courseId;
   }

   @Override
   public void prepareResponse()
   {
      // TODO Auto-generated method stub
      //      return null;
   }
}
