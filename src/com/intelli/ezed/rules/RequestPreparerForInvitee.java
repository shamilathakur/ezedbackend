package com.intelli.ezed.rules;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.UserProfile;

public class RequestPreparerForInvitee extends SingletonRule
{

   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public RequestPreparerForInvitee(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      UserProfile userProfile = (UserProfile) flowContext.getFromContext(EzedKeyConstants.USER_DETAILS);
      if (userProfile.getInvitedBy() != null)
      {
         String userId = userProfile.getInvitedBy();
         userProfile = new UserProfile();
         userProfile.setUserid(userId);
         try
         {
            userProfile.populateUserProfile(dbConnName, logger);
            flowContext.putIntoContext(EzedKeyConstants.USER_DETAILS, userProfile);
         }
         catch (Exception e)
         {
            logger.error("Error while populating user profile for invitee " + userId, e);
         }
         return RuleDecisionKey.ON_SUCCESS;
      }
      return RuleDecisionKey.ON_FAILURE;
   }
}
