package com.intelli.ezed.checks;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;

import com.intelli.ezed.data.CourseCalendarData;
import com.intelli.ezed.data.EzedKeyConstants;

public class CalendarChangesValidator extends SingletonRule
{
   public CalendarChangesValidator(String ruleName)
   {
      super(ruleName);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      CourseCalendarData courseCalendarData = (CourseCalendarData) flowContext.getFromContext(EzedKeyConstants.COURSE_CALENDAR);
      CourseCalendarData existingCalendar = (CourseCalendarData) flowContext.getFromContext(EzedKeyConstants.EXISTING_COURSE_CALENDAR);

      int dayBufferBeforeEnd = (Integer) flowContext.getFromContext(EzedKeyConstants.BUFFER_END_DATE);
      courseCalendarData.checkGoalValidityForUpdate(existingCalendar, dayBufferBeforeEnd);
      return RuleDecisionKey.ON_SUCCESS;
   }
}
