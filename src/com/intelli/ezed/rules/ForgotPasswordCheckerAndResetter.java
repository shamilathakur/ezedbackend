package com.intelli.ezed.rules;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.UserProfile;
import com.intelli.ezed.utils.AccountStatusHelper;

public class ForgotPasswordCheckerAndResetter extends SingletonRule
{

   public ForgotPasswordCheckerAndResetter(String ruleName)
   {
      super(ruleName);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      UserProfile userProfile = (UserProfile) flowContext.getFromContext(EzedKeyConstants.USER_DETAILS);
      int chkFlag = userProfile.getChkflag();

      if (AccountStatusHelper.isPasswordForgotten(chkFlag))
      {
         chkFlag = AccountStatusHelper.resetForgotPassword(chkFlag);
         userProfile.setChkflag(chkFlag);
         return RuleDecisionKey.ON_FAILURE;
      }
      return RuleDecisionKey.ON_SUCCESS;
   }
}
