package com.intelli.ezed.data;

public class AdminLoginResponse extends ResponseObject
{

   private String sessionId;
   private AdminProfile adminProfile;

   public String getSessionId()
   {
      return sessionId;
   }

   public void setSessionId(String sessionId)
   {
      this.sessionId = sessionId;
   }

   public AdminProfile getUserProfile()
   {
      return adminProfile;
   }

   public void setUserProfile(AdminProfile userProfile)
   {
      this.adminProfile = userProfile;
   }

   @Override
   public void prepareResponse() throws Exception
   {
      this.put("sessionid", getSessionId());
      getUserProfile().prepareResponse();
      this.put("profile", getUserProfile());
   }
}
