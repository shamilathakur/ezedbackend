package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedLoggerName;

public class VerificationIdFromDBRemover extends SingletonRule
{

   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public VerificationIdFromDBRemover(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
     
      String userid = messageData.getParameter("email");

      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      try
      {
         PreparedStatement ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "DeleteFromUserVerif");
         ps.setString(1, userid);
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows deleted from verif database after verifying email id " + userid);
         }
      }
      catch (SQLException e)
      {
         logger.error("Could not delete from verif db for email id " + userid);
      }
      catch (Exception e)
      {
         logger.error("Could not delete from verif db for email id " + userid);
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
      return RuleDecisionKey.ON_SUCCESS;
   }

}
