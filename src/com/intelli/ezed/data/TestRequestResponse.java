package com.intelli.ezed.data;

import java.util.ArrayList;

import org.apache.axis.encoding.Base64;

import com.a.helper.utils.StringSplitter;
import com.intelli.ezed.utils.Globals;


public class TestRequestResponse extends ResponseObject
{

   private QuestionContent question;
   private ArrayList<QuestionContent> answers = new ArrayList<QuestionContent>();
   private String correctAnswer;
   private String questionId;
   private int questionNumberOfRandomQuestion;
   private int difficultyLevel;

   private int currentQuestionNumber;

   private QuestionContent explanation;

   private String userAns;

   private int questiontype;


   public int getQuestionNumberOfRandomQuestion()
   {
      return questionNumberOfRandomQuestion;
   }

   public void setQuestionNumberOfRandomQuestion(int questionNumberOfRandomQuestion)
   {
      this.questionNumberOfRandomQuestion = questionNumberOfRandomQuestion;
   }

   public int getCurrentQuestionNumber()
   {
      return currentQuestionNumber;
   }

   public void setCurrentQuestionNumber(int currentQuestionNumber)
   {
      this.currentQuestionNumber = currentQuestionNumber;
   }

   public String getQuestionId()
   {
      return questionId;
   }

   public void setQuestionId(String questionId)
   {
      this.questionId = questionId;
   }

   public String getUserAns()
   {
      return userAns;
   }

   public void setUserAns(String userAns)
   {
      this.userAns = userAns;
   }

   public QuestionContent getQuestion()
   {
      return question;
   }

   public void setQuestion(QuestionContent question)
   {
      this.question = question;
   }

   public ArrayList<QuestionContent> getAnswers()
   {
      return answers;
   }

   public void setAnswers(ArrayList<QuestionContent> answers)
   {
      this.answers = answers;
   }

   public String getCorrectAnswer()
   {
      return correctAnswer;
   }

   public void setCorrectAnswer(String correctAnswer)
   {
      this.correctAnswer = correctAnswer;
   }

   public int getQuestiontype()
   {
      return questiontype;
   }

   public void setQuestiontype(int questiontype)
   {
      this.questiontype = questiontype;
   }

   @Override
   public void prepareResponse() throws Exception
   {
      this.put("questionType", getQuestiontype());
      this.put("questionContentType", question.getType());
      this.put("questionid", getQuestionId());
      String contentString = question.getContentString();
      if (question.getType() == QuestionContent.STRING_TYPE)
      {
         contentString = Base64.encode(contentString.getBytes("UTF-8"));
      }
      this.put("question", contentString);
      this.put("questionNumber", getCurrentQuestionNumber());

      this.put("userans", getUserAns());

      StringBuffer sb = new StringBuffer();

      for (int i = 0; i < getAnswers().size(); i++)
      {
         QuestionContent temp = getAnswers().get(i);
         int tpye = temp.getType();
         if (tpye == QuestionContent.CONTENT_TYPE)
         {
            this.put("option" + (i + 1) + "ContentType", temp.getType());
            this.put("option" + (i + 1) + "Content", temp.getContentString());
         }
         if (tpye == QuestionContent.STRING_TYPE)
         {
            if (!(getQuestiontype() == TestQuestionData.FIB_QUESTION_TYPE))
            {
               this.put("option" + (i + 1) + "ContentType", temp.getType());
               this.put("option" + (i + 1) + "Content", Base64.encode(temp.getContentString().getBytes("UTF-8")));
            }
            else
            {
               this.put("option" + (i + 1) + "ContentType", QuestionContent.CONTENT_ABSENT);
               this.put("option" + (i + 1) + "Content", "");
               sb.append(temp.getContentString()).append(Globals.FIB_SEPARATOR);
            }
         }
         if (tpye == QuestionContent.CONTENT_ABSENT)
         {
            //continue;
            this.put("option" + (i + 1) + "ContentType", temp.getType());
            this.put("option" + (i + 1) + "Content", "");
         }
      }
      this.put("correctAnswerContentType", QuestionContent.STRING_TYPE);
      if (getExplanation() != null)
      {
         //         this.put("explaination", Base64.encode(getExplanation().getBytes("UTF-8")));
         this.put("explainationContentType", getExplanation().getType());
         if (getExplanation().getType() == QuestionContent.STRING_TYPE)
         {
            this.put("explaination", Base64.encode(getExplanation().getContentString().getBytes("UTF-8")));
         }
         else
         {
            this.put("explaination", getExplanation().getContentLoc());
         }
      }

      if (getQuestiontype() == TestQuestionData.FIB_QUESTION_TYPE)
      {
         for (int k = 1; k <= Globals.FIB_SEPARATOR.length(); k++)
         {
            sb.deleteCharAt(sb.length() - 1);
         }
         this.put("correctAnswerContent", sb.toString());
         setCorrectAnswer(sb.toString());
         this.put("multipleflag", 0);
      }
      else if (getQuestiontype() == TestQuestionData.MCQ_QUESTION_TYPE && getCorrectAnswer().contains(","))
      {
         this.put("correctAnswerContent", getCorrectAnswer());
         this.put("multipleflag", 1);
         String[] correctAnswers = StringSplitter.splitString(getCorrectAnswer(), ",");
         boolean putCorrectAnsStringFlag = true;
         StringBuffer correctAnsPtags = new StringBuffer();
         for (int i = 0; i < correctAnswers.length; i++)
         {
            if (correctAnswers[i].compareToIgnoreCase("1") == 0)
            {
               if (getAnswers().get(0).getType() == QuestionContent.STRING_TYPE)
               {
                  correctAnsPtags.append("<p>").append(getAnswers().get(0).getContentString()).append("</p>");
               }
               else
               {
                  putCorrectAnsStringFlag = false;
                  break;
               }
            }
            else if (correctAnswers[i].compareToIgnoreCase("2") == 0)
            {
               if (getAnswers().get(1).getType() == QuestionContent.STRING_TYPE)
               {
                  correctAnsPtags.append("<p>").append(getAnswers().get(1).getContentString()).append("</p>");
               }
               else
               {
                  putCorrectAnsStringFlag = false;
                  break;
               }
            }
            else if (correctAnswers[i].compareToIgnoreCase("3") == 0)
            {
               if (getAnswers().get(2).getType() == QuestionContent.STRING_TYPE)
               {
                  correctAnsPtags.append("<p>").append(getAnswers().get(2).getContentString()).append("</p>");
               }
               else
               {
                  putCorrectAnsStringFlag = false;
                  break;
               }
            }
            else if (correctAnswers[i].compareToIgnoreCase("4") == 0)
            {
               if (getAnswers().get(3).getType() == QuestionContent.STRING_TYPE)
               {
                  correctAnsPtags.append("<p>").append(getAnswers().get(3).getContentString()).append("</p>");
               }
               else
               {
                  putCorrectAnsStringFlag = false;
                  break;
               }
            }
            else if (correctAnswers[i].compareToIgnoreCase("5") == 0)
            {
               if (getAnswers().get(4).getType() == QuestionContent.STRING_TYPE)
               {
                  correctAnsPtags.append("<p>").append(getAnswers().get(4).getContentString()).append("</p>");
               }
               else
               {
                  putCorrectAnsStringFlag = false;
                  break;
               }
            }
            else if (correctAnswers[i].compareToIgnoreCase("6") == 0)
            {
               if (getAnswers().get(5).getType() == QuestionContent.STRING_TYPE)
               {
                  correctAnsPtags.append("<p>").append(getAnswers().get(5).getContentString()).append("</p>");
               }
               else
               {
                  putCorrectAnsStringFlag = false;
                  break;
               }
            }
         }
         if (putCorrectAnsStringFlag)
         {
            this.put("correctanswerstring", Base64.encode(correctAnsPtags.toString().getBytes("UTF-8")));
         }
      }
      else
      {
         if (getQuestiontype() == TestQuestionData.MCQ_QUESTION_TYPE)
         {
            boolean putCorrectAnsStringFlag = true;
            StringBuffer correctAnsPtags = new StringBuffer();
            if (getCorrectAnswer().compareToIgnoreCase("1") == 0)
            {
               if (getAnswers().get(0).getType() == QuestionContent.STRING_TYPE)
               {
                  correctAnsPtags.append("<p>").append(getAnswers().get(0).getContentString()).append("</p>");
               }
               else
               {
                  putCorrectAnsStringFlag = false;
               }
            }
            else if (getCorrectAnswer().compareToIgnoreCase("2") == 0)
            {
               if (getAnswers().get(1).getType() == QuestionContent.STRING_TYPE)
               {
                  correctAnsPtags.append("<p>").append(getAnswers().get(1).getContentString()).append("</p>");
               }
               else
               {
                  putCorrectAnsStringFlag = false;
               }
            }
            else if (getCorrectAnswer().compareToIgnoreCase("3") == 0)
            {
               if (getAnswers().get(2).getType() == QuestionContent.STRING_TYPE)
               {
                  correctAnsPtags.append("<p>").append(getAnswers().get(2).getContentString()).append("</p>");
               }
               else
               {
                  putCorrectAnsStringFlag = false;
               }
            }
            else if (getCorrectAnswer().compareToIgnoreCase("4") == 0)
            {
               if (getAnswers().get(3).getType() == QuestionContent.STRING_TYPE)
               {
                  correctAnsPtags.append("<p>").append(getAnswers().get(3).getContentString()).append("</p>");
               }
               else
               {
                  putCorrectAnsStringFlag = false;
               }
            }
            else if (getCorrectAnswer().compareToIgnoreCase("5") == 0)
            {
               if (getAnswers().get(4).getType() == QuestionContent.STRING_TYPE)
               {
                  correctAnsPtags.append("<p>").append(getAnswers().get(4).getContentString()).append("</p>");
               }
               else
               {
                  putCorrectAnsStringFlag = false;
               }
            }
            else if (getCorrectAnswer().compareToIgnoreCase("6") == 0)
            {
               if (getAnswers().get(5).getType() == QuestionContent.STRING_TYPE)
               {
                  correctAnsPtags.append("<p>").append(getAnswers().get(5).getContentString()).append("</p>");
               }
               else
               {
                  putCorrectAnsStringFlag = false;
               }
            }
            if (putCorrectAnsStringFlag)
            {
               this.put("correctanswerstring", Base64.encode(correctAnsPtags.toString().getBytes("UTF-8")));
            }
         }
         this.put("correctAnswerContent", getCorrectAnswer());
         this.put("multipleflag", 0);
      }
   }

   public QuestionContent getExplanation()
   {
      return explanation;
   }

   public void setExplanation(QuestionContent explanation)
   {
      this.explanation = explanation;
   }

   public int getDifficultyLevel()
   {
      return difficultyLevel;
   }

   public void setDifficultyLevel(int difficultyLevel)
   {
      this.difficultyLevel = difficultyLevel;
   }
}
