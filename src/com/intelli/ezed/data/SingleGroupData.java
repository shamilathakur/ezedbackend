package com.intelli.ezed.data;

public class SingleGroupData extends ResponseObject
{
   private String groupId;
   private String courseId;
   private String groupName;

   @Override
   public void prepareResponse() throws Exception
   {
      this.put("groupid", getGroupId());
      this.put("courseid", getCourseId());
      this.put("groupname", getGroupName());
   }

   public String getGroupId()
   {
      return groupId;
   }

   public void setGroupId(String groupId)
   {
      this.groupId = groupId;
   }

   public String getCourseId()
   {
      return courseId;
   }

   public void setCourseId(String courseId)
   {
      this.courseId = courseId;
   }

   public String getGroupName()
   {
      return groupName;
   }

   public void setGroupName(String groupName)
   {
      this.groupName = groupName;
   }

}
