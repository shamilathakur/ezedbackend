package com.intelli.ezed.data;

import java.util.List;

public class ChapterDetails implements ResponseData
{
   //Basic Chapter Details
   private String chapterid;
   private String chaptername;
   private String introimage;
   private String introvideo;
   private String description;
   private String weightage;

   /*
    * It will be a list of contentIds ordered according to the learning path.
    */
   private List<String> learningPath;

   public String getChapterid()
   {
      return chapterid;
   }

   public void setChapterid(String chapterid)
   {
      this.chapterid = chapterid;
   }

   public String getChaptername()
   {
      return chaptername;
   }

   public void setChaptername(String chaptername)
   {
      this.chaptername = chaptername;
   }

   public String getIntroimage()
   {
      return introimage;
   }

   public void setIntroimage(String introimage)
   {
      this.introimage = introimage;
   }

   public String getIntrovideo()
   {
      return introvideo;
   }

   public void setIntrovideo(String introvideo)
   {
      this.introvideo = introvideo;
   }

   public String getDescription()
   {
      return description;
   }

   public void setDescription(String description)
   {
      this.description = description;
   }

   public String getWeightage()
   {
      return weightage;
   }

   public void setWeightage(String weightage)
   {
      this.weightage = weightage;
   }

   public List<String> getLearningPath()
   {
      return learningPath;
   }

   public void setLearningPath(List<String> learningPath)
   {
      this.learningPath = learningPath;
   }

   @Override
   public void prepareResponse()
   {
      //      return null;
   }

}
