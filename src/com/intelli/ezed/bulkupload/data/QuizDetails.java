package com.intelli.ezed.bulkupload.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

import org.sabre.db.DbConnectionManager;
import org.slf4j.Logger;

import com.intelli.ezed.data.TestQuestion;

public class QuizDetails
{
   private ArrayList<TestQuestion> testQuestions = new ArrayList<TestQuestion>();

   private String quizId;

   public String getQuizId()
   {
      return quizId;
   }

   public void setQuizId(String quizId)
   {
      this.quizId = quizId;
   }

   public void addQuestion(TestQuestion testQuestion)
   {
      this.testQuestions.add(testQuestion);
   }

   public void replaceContentIds(Map<String, String> contentDirIdMap) throws Exception
   {
      TestQuestion question = null;
      for (int i = 0; i < testQuestions.size(); i++)
      {
         question = testQuestions.get(i);
         question.replaceContentIds(contentDirIdMap);
      }
   }

   public void persistQuiz(String dbConnName, Logger logger) throws Exception
   {
      Connection conn = null;
      PreparedStatement ps = null;
      for (int i = 0; i < testQuestions.size(); i++)
      {
         try
         {
            conn = DbConnectionManager.getConnectionByName(dbConnName);
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "PersistQuiz");
            ps.setString(1, getQuizId());
            ps.setInt(2, (i + 1));
            ps.setInt(3, testQuestions.get(i).getExplanation().getType());
            ps.setString(4, testQuestions.get(i).getExplanation().getContentString());
            ps.setString(5, testQuestions.get(i).getExplanation().getContentLoc());
            testQuestions.get(i).persistForBulkUpload(ps, 5);
            int rows = ps.executeUpdate();
            if (logger.isDebugEnabled())
            {
               logger.debug(rows + " rows inserted in db while inserting quiz question");
            }
         }
         catch (SQLException e)
         {
            logger.error("Error while inserting quiz question in db", e);
            throw e;
         }
         catch (Exception e)
         {
            logger.error("Error while inserting quiz question in db", e);
            throw e;
         }
         finally
         {
            DbConnectionManager.releaseConnection(dbConnName, conn);
         }
      }
   }
}
