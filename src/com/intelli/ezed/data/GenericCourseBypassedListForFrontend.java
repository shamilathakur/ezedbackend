package com.intelli.ezed.data;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

import org.json.JSONArray;
import org.slf4j.Logger;

public class GenericCourseBypassedListForFrontend extends ResponseObject
{
   private Map<String, CourseBypassedStatData> courseIdList = new HashMap<String, CourseBypassedStatData>();
   private int exceptionCount;

   public void addCourse(String courseId, String suggestion)
   {
      if (getCourseIdList().get(courseId.intern()) == null)
      {
         CourseBypassedStatData data = new CourseBypassedStatData();
         data.setCourseId(courseId);
         data.setSuggestionString(suggestion);
         this.getCourseIdList().put(courseId.intern(), data);
      }
   }
   public GenericCourseBypassedListForFrontend(int exceptionCount)
   {
      this.exceptionCount = exceptionCount;
   }

   public Map<String, CourseBypassedStatData> getCourseIdList()
   {
      return courseIdList;
   }

   public void setCourseIdList(Map<String, CourseBypassedStatData> courseIdList)
   {
      this.courseIdList = courseIdList;
   }

   public void fetchCourseDetails(String dbConnName, Logger logger) throws SQLException, Exception
   {
      CourseBypassedStatData data = null;
      int count = 0;
      Iterator<String> courseIds = getCourseIdList().keySet().iterator();
      while (courseIds.hasNext())
      {
         data = getCourseIdList().get(courseIds.next());
         //         try
         //         {
         //            data.fetchChapterCount(dbConnName, logger);
         //         }
         //         catch (Exception e)
         //         {
         //            count++;
         //            if (count < exceptionCount)
         //            {
         //               data.setChapterCount(0);
         //            }
         //            else
         //            {
         //               throw e;
         //            }
         //         }
         //
         //         try
         //         {
         //            data.fetchVideoCount(dbConnName, logger);
         //         }
         //         catch (Exception e)
         //         {
         //            count++;
         //            if (count < exceptionCount)
         //            {
         //               data.setVideoCount(0);
         //            }
         //            else
         //            {
         //               throw e;
         //            }
         //         }
         //
         //         try
         //         {
         //            data.fetchTestCount(dbConnName, logger);
         //         }
         //         catch (Exception e)
         //         {
         //            count++;
         //            if (count < exceptionCount)
         //            {
         //               data.setTestCount(0);
         //            }
         //            else
         //            {
         //               throw e;
         //            }
         //         }
         //
         //         try
         //         {
         //            data.fetchQuestionCount(dbConnName, logger);
         //         }
         //         catch (Exception e)
         //         {
         //            count++;
         //            if (count < exceptionCount)
         //            {
         //               data.setQuestionCount(0);
         //            }
         //            else
         //            {
         //               throw e;
         //            }
         //         }

         try
         {
            data.fetchCounts(dbConnName, logger);
         }
         catch (Exception e)
         {
            count++;
            if (count < exceptionCount)
            {
               data.setQuestionCount(0);
            }
            else
            {
               throw e;
            }
         }

         try
         {
            data.fetchEndDateCourseName(dbConnName, logger, true);
         }
         catch (Exception e)
         {
            throw e;
         }
      }
   }

   @Override
   public void prepareResponse() throws Exception
   {
      JSONArray jsonArray = new JSONArray();
      Iterator<String> courseIds = getCourseIdList().keySet().iterator();
      CourseBypassedStatData data = null;
      while (courseIds.hasNext())
      {
         data = getCourseIdList().get(courseIds.next());
         if (data.getPublishFlag() != 0)
         {
            data.prepareResponse();
            jsonArray.put(data);
         }
      }
      this.put("courses", jsonArray);
   }

   public void removeAlreadyPurchasedCourse(Vector<String> courseList, Vector<String> courseTypeList)
   {
      Iterator<String> courseIds = getCourseIdList().keySet().iterator();
      String courseId = null;
      while (courseIds.hasNext())
      {
         courseId = courseIds.next();
         for (int j = 0; j < courseList.size(); j++)
         {
            //Below check to be removed later. Removing this now so that any course purchased
            //earlier will not be shown in suggested courses irrespective of course type
            //if (courseId.compareTo(courseList.get(j)) == 0 && courseTypeList.get(j).compareTo(CartData.ALL_COURSE) == 0)
            if (courseId.compareTo(courseList.get(j)) == 0)
            {
               courseIds.remove();
               continue;
            }
         }
      }
   }
}
