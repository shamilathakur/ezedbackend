package com.intelli.ezed.utils;

import java.io.IOException;
import java.io.PrintWriter;

public class SymlinkCreator
{
   private Process process;
   private PrintWriter out;

   public SymlinkCreator(String path) throws IOException
   {
      process = Runtime.getRuntime().exec(path);
      out = new PrintWriter(process.getOutputStream());
   }

   public void link(String oldpath, String newpath)
   {
      out.println(oldpath);
      out.println(newpath);
      out.flush();
   }

   public void terminate() throws InterruptedException
   {
      out.close();
      process.waitFor();
      process.destroy();
   }

   public static void main(String[] args)
   {
      SymlinkCreator linkCreator = null;
      try
      {
         linkCreator = new SymlinkCreator("lib/symlinkcreator");
         linkCreator.link("conf", "newconf");
      }
      catch (IOException e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
      finally
      {
         try
         {
            linkCreator.terminate();
         }
         catch (InterruptedException e)
         {
            // TODO Auto-generated catch block
            e.printStackTrace();
         }
      }
   }
}