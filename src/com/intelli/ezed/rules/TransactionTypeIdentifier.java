package com.intelli.ezed.rules;

import org.sabre.io.transport.http.data.HttpTransactionData;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.CloneableRule;
import org.sabre.workflow.context.FlowContext;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedTransactionCodes;

public class TransactionTypeIdentifier extends CloneableRule
{
   public TransactionTypeIdentifier(String ruleName)
   {
      super(ruleName);
   }

   @Override
   protected Object clone() throws CloneNotSupportedException
   {
      return new TransactionTypeIdentifier(getRuleName());
   }

   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      String keyWord = (String) flowContext.getFromContext(EzedKeyConstants.KEYWORD);
      HttpTransactionData transactionData = new HttpTransactionData();
      flowContext.putIntoContext(HttpTransactionData.WRITE_RESP_DATA,transactionData);

      String decisionKey = "if-" + keyWord;
      RuleDecisionKey ruleDecisionKey = RuleDecisionKey.getRuleDecisionKey(decisionKey);

      if (ruleDecisionKey == RuleDecisionKey.INVALID_KEY)
      {
         transactionData.setContent(("statuscode=" + EzedTransactionCodes.INVALID_MESSAGE_KEYWORD + "&statusmessage=Cannot process request.").getBytes());
         return RuleDecisionKey.ON_FAILURE;
      }
      else
      {
         flowContext.putIntoContext(EzedKeyConstants.REQUEST_TYPE, keyWord.toUpperCase());
         return ruleDecisionKey;
      }
   }

}
