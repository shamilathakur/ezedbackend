package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.analytics.data.GenericChartData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.SingleGroupTest;
import com.intelli.ezed.data.SingleTestDataForGroup;

public class UserGroupTestsFetcher extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

   public UserGroupTestsFetcher(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      String userId = (String) flowContext.getFromContext(EzedKeyConstants.USER_ID);

      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      Map<String, SingleGroupTest> groupMap = new HashMap<String, SingleGroupTest>();
      try
      {
         //select t.*,g.groupname from testlist t, grouplist g where t.courseid = g.groupid and t.courseid in (select groupid from groupusers where userid=?) and to_date(t.deadline,'YYYYMMDDHH24MISS')<now()
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchGroupTests");
         ps.setString(1, userId);
         rs = ps.executeQuery();
         ArrayList<SingleGroupTest> datas = new ArrayList<SingleGroupTest>();
         SingleGroupTest data = null;
         SingleTestDataForGroup testData = null;
         String groupId = null;
         while (rs.next())
         {
            groupId = rs.getString("groupid");
            data = groupMap.get(groupId);
            if (data == null)
            {
               data = new SingleGroupTest();
               data.setGroupId(groupId);
               data.setGroupName(rs.getString("groupname"));
               groupMap.put(data.getGroupId(), data);
               datas.add(data);
            }
            testData = new SingleTestDataForGroup();
            testData.setTestId(rs.getString("testid"));
            testData.setTestName(rs.getString("testname"));
            testData.setDeadLine(sdf.parse(rs.getString("deadLine")));
            data.getTests().add(testData);
         }
         JSONArray jsonArray = new JSONArray();
         GenericChartData genericChartData = new GenericChartData();
         genericChartData.getText().add("grouptests");
         if (datas.size() == 0)
         {
            genericChartData.getValues().add(jsonArray);
         }
         else
         {
            for (SingleGroupTest singleGroupTest : datas)
            {
               singleGroupTest.prepareResponse();
               jsonArray.put(singleGroupTest);
            }
            genericChartData.getValues().add(jsonArray);
         }
         response.setResponseObject(genericChartData);
         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
         return RuleDecisionKey.ON_SUCCESS;
      }
      catch (SQLException e)
      {
         logger.error("DB Error while fetching group tests for user " + userId, e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching user group tests");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching group tests for user " + userId, e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching user group tests");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing result set while fetching user group tests", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
}
