package com.intelli.ezed.bulkupload.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

import org.sabre.db.DbConnectionManager;
import org.slf4j.Logger;

import com.intelli.ezed.data.TestQuestion;
import com.intelli.ezed.utils.PrepareQuestionNoBackend;

public class QuestionBankDetails
{
   private ArrayList<TestQuestion> testQuestions = new ArrayList<TestQuestion>();
   private static PrepareQuestionNoBackend prepareQuestionId = new PrepareQuestionNoBackend("QUE2", "ezeddb");

   private String courseId;
   private String chapterId;

   public String getCourseId()
   {
      return courseId;
   }

   public void setCourseId(String courseId)
   {
      this.courseId = courseId;
   }

   public String getChapterId()
   {
      return chapterId;
   }

   public void setChapterId(String chapterId)
   {
      this.chapterId = chapterId;
   }

   public void addQuestion(TestQuestion testQuestion)
   {
      this.testQuestions.add(testQuestion);
   }

   private String fetchNewQuestionId()
   {
      return prepareQuestionId.getNextId();
   }

   public void persistQuestionBank(String dbConnName, Logger logger) throws Exception
   {
      Connection conn = null;
      PreparedStatement ps = null;
      for (int i = 0; i < testQuestions.size(); i++)
      {
         try
         {
            conn = DbConnectionManager.getConnectionByName(dbConnName);
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "PersistQuestionBank");
            ps.setString(1, getCourseId());
            ps.setString(2, getChapterId());
            ps.setString(3, fetchNewQuestionId());
            ps.setInt(4, testQuestions.get(i).getDifficultyLevel());
            ps.setInt(5, testQuestions.get(i).getExplanation().getType());
            ps.setString(6, testQuestions.get(i).getExplanation().getContentString());
            ps.setString(7, testQuestions.get(i).getExplanation().getContentLoc());
            testQuestions.get(i).persistForBulkUpload(ps, 7);
            int rows = ps.executeUpdate();
            if (logger.isDebugEnabled())
            {
               logger.debug(rows + " rows inserted in question bank db while inserting question");
            }
         }
         catch (SQLException e)
         {
            logger.error("Error while inserting question bank in db", e);
            throw e;
         }
         catch (Exception e)
         {
            logger.error("Error while inserting question bank in db", e);
            throw e;
         }
         finally
         {
            DbConnectionManager.releaseConnection(dbConnName, conn);
         }
      }
   }
}
