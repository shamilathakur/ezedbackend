package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.GenericCourseListForFrontend;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.UserProfile;

public class SuggestedCourseListFetcherM extends SingletonRule
{
	   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
	   private String dbConnName;
	   private int failureExceptionCount;

	   public SuggestedCourseListFetcherM(String ruleName, String dbConnName, String failureExceptionCount)
	   {
	      super(ruleName);
	      this.dbConnName = dbConnName;
	      this.failureExceptionCount = Integer.parseInt(failureExceptionCount);
	   }

	   @Override
	   public RuleDecisionKey executeRule(FlowContext flowContext)
	   {
	      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
	      UserProfile userProfile = (UserProfile) flowContext.getFromContext(EzedKeyConstants.USER_DETAILS);
	      GenericCourseListForFrontend genericCourseListForFrontend = new GenericCourseListForFrontend(failureExceptionCount);
	      MessageData messageData = (MessageData) flowContext
					.getFromContext(MessageData.class.getName());
	      response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
	      response.setResponseObject(genericCourseListForFrontend);
	      String pageno = messageData.getParameter("pageno");
		     if(pageno == null || pageno.trim().compareTo("")==0){
		    	 if(logger.isDebugEnabled()){
		    		 logger.debug("page no in request is invalid");
		    	 }
		    	 response.setParameters(EzedTransactionCodes.PROCESSING_REQUEST, "Invalid Page No");
		    	 return RuleDecisionKey.ON_FAILURE;
		     }
		     
		     String noOfRecordsOnAPage = messageData.getParameter("noofrecords");
		     if(noOfRecordsOnAPage == null || noOfRecordsOnAPage.trim().compareTo("")==0){
		    	 if(logger.isDebugEnabled()){
		    		 logger.debug("page no in request is invalid");
		    	 }
		    	 response.setParameters(EzedTransactionCodes.PROCESSING_REQUEST, "Invalid No Of Records");
		    	 return RuleDecisionKey.ON_FAILURE;
		     }
		     int limit = Integer.parseInt(noOfRecordsOnAPage);
		     int pagenumber = Integer.parseInt(pageno);
		     int offset = (Integer.parseInt(pageno) -1) * limit;
		     Connection conn = null;
	      PreparedStatement ps = null;
	      ResultSet rs = null;
	      if (userProfile.getUnivName() != null)
	      {
	         try
	         {
	            conn = DbConnectionManager.getConnectionByName(dbConnName);
	            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchUnivCoursesWithPaging");
	            ps.setString(1, userProfile.getUnivName());
	            ps.setInt(2, offset);
	            ps.setInt(3, limit);
	            rs = ps.executeQuery();
	            while (rs.next())
	            {
	               genericCourseListForFrontend.addCourse(rs.getString("courseid"), "Course belongs to your university");
	            }
	         
	         }
	         catch (SQLException e)
	         {
	            logger.error("Error while fetching suggested course list via univ " + userProfile.getUnivName(), e);
	         }
	         catch (Exception e)
	         {
	            logger.error("Error while fetching suggested course list via univ " + userProfile.getUnivName(), e);
	         }
	         finally
	         {
	            if (rs != null)
	            {
	               try
	               {
	                  rs.close();
	               }
	               catch (SQLException e)
	               {
	                  logger.error("Error while closing result set while fetching suggested courses based on user univ " + userProfile.getUnivName(), e);
	               }
	            }
	            DbConnectionManager.releaseConnection(dbConnName, conn);
	         }
	      }

	      if (userProfile.getCourseList().size() != 0)
	      {
	         for (String purchasedCourseId : userProfile.getCourseList())
	         {
	            try
	            {
	               conn = DbConnectionManager.getConnectionByName(dbConnName);
	               ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchSuggestBasedOnPurchaseWithPaging");
	               ps.setString(1, purchasedCourseId);
	               ps.setInt(2, offset);
		           ps.setInt(3, limit);
		           rs = ps.executeQuery();
	               while (rs.next())
	               {
	                      genericCourseListForFrontend.addCourse(rs.getString("courseid"), "You have purchased a course in the same offering");
	               }
	             
		        
	            }
	            catch (SQLException e)
	            {
	               logger.error("Error while fetching suggested course list via purchased course " + purchasedCourseId, e);
	            }
	            catch (Exception e)
	            {
	               logger.error("Error while fetching suggested course list via purchased course " + purchasedCourseId, e);
	            }
	            finally
	            {
	               if (rs != null)
	               {
	                  try
	                  {
	                     rs.close();
	                  }
	                  catch (SQLException e)
	                  {
	                     logger.error("Error while closing result set while fetching suggested courses based on pruchased course " + purchasedCourseId, e);
	                  }
	               }
	               DbConnectionManager.releaseConnection(dbConnName, conn);
	            }
	         }
	      }

	      if (genericCourseListForFrontend.getCourseIdList().size() == 0 && response.getResponseCode() == EzedTransactionCodes.SUCCESS)
	      {
	         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "No suggested courses could be found");
	         return RuleDecisionKey.ON_FAILURE;
	      }
	      else if (genericCourseListForFrontend.getCourseIdList().size() != 0 && response.getResponseCode() == EzedTransactionCodes.SUCCESS)
	      {
	         genericCourseListForFrontend.removeAlreadyPurchasedCourse(userProfile.getCourseList(), userProfile.getCourseTypeList());
	         try
	         {
	            genericCourseListForFrontend.fetchCourseDetails(dbConnName, logger);
	            genericCourseListForFrontend.append("pageno", Integer.toString(pagenumber));
	            genericCourseListForFrontend.append("offset", Integer.toString(offset));
	            genericCourseListForFrontend.append("limit", Integer.toString(limit));
	         }
	         catch (SQLException e)
	         {
	            logger.error("Error while fetching course details of suggested course", e);
	            response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching suggested courses");
	            return RuleDecisionKey.ON_FAILURE;
	         }
	         catch (Exception e)
	         {
	            logger.error("Error while fetching course details of suggested course", e);
	            response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching suggested courses");
	            return RuleDecisionKey.ON_FAILURE;
	         }
	         return RuleDecisionKey.ON_SUCCESS;
	      }
	      else
	      {
	         return RuleDecisionKey.ON_FAILURE;
	      }
	   }
}
