package com.intelli.ezed.db;

import java.sql.SQLException;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.GroupUserAdder;
import com.intelli.ezed.data.Response;

public class GroupDeleteHandler extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;

   public GroupDeleteHandler(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      String groupId = messageData.getParameter("groupid");
      if (groupId == null || groupId.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Group id is invalid");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid group id");
         return RuleDecisionKey.ON_FAILURE;
      }

      GroupUserAdder groupUsers = new GroupUserAdder();
      groupUsers.setGroupId(groupId);

      try
      {
         groupUsers.deleteGroup(dbConnName, logger);
         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
         return RuleDecisionKey.ON_SUCCESS;
      }
      catch (SQLException e)
      {
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while adding users to group");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while adding users to group");
         return RuleDecisionKey.ON_FAILURE;
      }
   }
}
