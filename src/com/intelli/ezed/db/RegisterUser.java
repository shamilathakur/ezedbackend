package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.utils.RandomNumberGenerator;
import com.intelli.ezed.data.ActionData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.MasterActionRecords;
import com.intelli.ezed.data.RegistrationResponse;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.UserProfile;
import com.intelli.ezed.utils.AccountStatusHelper;
import com.intelli.ezed.utils.PasswordHelper;

public class RegisterUser extends SingletonRule
{

   Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;
   private static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
   private String actionId;

   public RegisterUser(String ruleName, String dbConnName, String actionId)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
      this.actionId = actionId;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      UserProfile userProfile = (UserProfile) flowContext.getFromContext(EzedKeyConstants.USER_DETAILS);//extracts details of USER_DETAILS populated from RegistrationDataChecker
      Connection connection = DbConnectionManager.getConnectionByName(dbConnName);
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      ActionData actionData = MasterActionRecords.fetchRecord(actionId); //What does fetch record fetch?
      String userid = userProfile.getUserid().toLowerCase();
      int regType = userProfile.getRegLoginType();
      String password;

      if (regType == 1)
      {
         password = " ";
      }
      else
      {
         password = userProfile.getPassword();
      }

      try
      {
         password = PasswordHelper.getEncryptedData(password);
      }
      catch (IllegalBlockSizeException e2)
      {
         logger.error("Error while encrypting password", e2);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while processing registration");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (BadPaddingException e2)
      {
         logger.error("Error while encrypting password", e2);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while processing registration");
         return RuleDecisionKey.ON_FAILURE;
      }

      try
      {
         //insert into users (userid,password,chkflag) values (?,?,?)
         PreparedStatement insertInUsers = DbConnectionManager.getPreparedStatement(dbConnName, connection, "InsertInUsers");
         insertInUsers.setString(1, userid);
         insertInUsers.setString(2, password);
         if (regType == 1)
         {
            insertInUsers.setInt(3, 2); //Inserting chkflag 2 as user is getting registered vi some other site like facebook, gmail etc.
            insertInUsers.setString(4, "Facebook");
         }
         else
         {
            insertInUsers.setInt(3, 0); //Inserting chkflag 0 for first time.
            insertInUsers.setString(4, "EZED");
         }
         insertInUsers.setString(5, userProfile.getFirstName());
         insertInUsers.setString(6, userProfile.getLastName());
         insertInUsers.setString(7, userProfile.getGender());
         insertInUsers.setString(8, userProfile.getDateOfBirth());
         insertInUsers.setString(9, sdf.format(new Date()));
         insertInUsers.setString(10, userProfile.getFbId());
         insertInUsers.setString(11, userProfile.getProfileImage());
         insertInUsers.setInt(12, actionData.getPoints());

         int i = insertInUsers.executeUpdate();

         if (i == 0)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("No rows inserted into table. RegisterUSer.java");
            }
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal Error. Please try again.");
            return RuleDecisionKey.ON_FAILURE;
         }
      }
      catch (SQLException e)
      {
         logger.error("Database Exception.", e);

         if (e.getMessage().contains("users_pkey1"))
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Userid already present in database.");
               try
               {
                  String firstName = userProfile.getFirstName();
                  String lastName = userProfile.getLastName();
                  String gender = userProfile.getGender();
                  String dob = userProfile.getDateOfBirth();
                  userProfile.populateUserProfile(dbConnName, logger);
                  if (AccountStatusHelper.isRegistered(userProfile.getChkflag()))
                  {
                     if (userProfile.getRegLoginType() == 1)
                     {
                        response.setParameters(EzedTransactionCodes.USERID_ALREADY_PRESENT, "User already registered");
                        return RuleDecisionKey.ON_FAILURE;
                     }
                     response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "User already registered");
                     return RuleDecisionKey.ON_FAILURE;
                  }
                  else
                  {
                     //go to success case. Send verif key again.
                     //before that, update his password.
                     Connection conn1 = DbConnectionManager.getConnectionByName(dbConnName);
                     try
                     {
                        PreparedStatement ps1 = DbConnectionManager.getPreparedStatement(dbConnName, conn1, "UpdateReregisterUsers");
                        ps1.setString(1, password);
                        ps1.setString(2, firstName);
                        ps1.setString(3, lastName);
                        ps1.setString(4, gender);
                        ps1.setString(5, dob);
                        if (regType == 1)
                        {
                           ps1.setInt(6, 2); //Inserting chkflag 2 as user is getting registered vi some other site like facebook, gmail etc.
                           ps1.setString(7, "Facebook");
                        }
                        else
                        {
                           ps1.setInt(6, 0); //Inserting chkflag 0 for first time.
                           ps1.setString(7, "EZED");
                        }
                        ps1.setString(8, userid);
                        int rows = ps1.executeUpdate();
                        if (logger.isDebugEnabled())
                        {
                           logger.debug(rows + " rows updated in db while updating user for fresh registration before verif " + userid);
                        }
                     }
                     catch (Exception e2)
                     {
                        logger.error("Error while updating user details while re-registering user " + userid, e);
                        response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while re-registering user");
                        return RuleDecisionKey.ON_FAILURE;
                     }
                     finally
                     {
                        DbConnectionManager.releaseConnection(dbConnName, conn1);
                     }
                  }
               }
               catch (Exception e1)
               {
                  response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "User already registered");
                  return RuleDecisionKey.ON_FAILURE;
               }
            }
         }
         else
         {
            response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Database exception occourred. Please try again.");
            return RuleDecisionKey.ON_FAILURE;
         }
      }
      catch (Exception e)
      {
         logger.error("Exception.", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal Error. Please try again.");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, connection);
      }

      //inserting in user stream
      Connection conn = null;
      PreparedStatement ps = null;
      String userRegStream = userProfile.getName() + " registered on EzEd";
      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertUserStreamAll");
         ps.setString(1, userRegStream);
         ps.setString(2, userProfile.getUserid());
         ps.setString(3, sdf.format(new Date()));
         ps.setString(4, null);
         ps.setString(5, null);
         ps.setString(6, null);
         ps.setString(7, actionId);
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in db while inserting stream for user " + userProfile.getUserid());
         }
      }
      catch (Exception e)
      {
         logger.error("Error inserting stream entry for " + userProfile.getUserid(), e);
         logger.error("INSERT FAILED " + userRegStream);
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      //Generating a Hex Key for verification
      String registrationVerificationKey = RandomNumberGenerator.generateRandomHex(15);
      RegistrationResponse regResponse = new RegistrationResponse();
      String requestInTime = (String) flowContext.getFromContext(EzedKeyConstants.REQUEST_IN_TIME);

      if (regType == 0)
      {
         connection = DbConnectionManager.getConnectionByName(dbConnName);
         try
         {
            //insert into userverif (userid,verifid,intime) values (?,?,?)
            PreparedStatement insertIntoUserverif = DbConnectionManager.getPreparedStatement(dbConnName, connection, "InsertInUserverif");
            insertIntoUserverif.setString(1, userid);
            insertIntoUserverif.setString(2, registrationVerificationKey);
            insertIntoUserverif.setString(3, requestInTime);
            int i = insertIntoUserverif.executeUpdate();

            if (i == 0)
            {
               if (logger.isDebugEnabled())
               {
                  logger.debug("No rows inserted into table. RegisterUSer.java");
               }
               response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal Error. Please try again.");
               return RuleDecisionKey.ON_FAILURE;
            }

         }
         catch (SQLException e)
         {
            logger.error("Database Exception.", e);
            if (e.getMessage().contains("userverif_uniq"))
            {
               logger.debug("Need to replace the verification key");
               try
               {
                  //update userverif set verifid = ?, intime = ? where userid = ?
                  PreparedStatement updateIntoUserverif = DbConnectionManager.getPreparedStatement(dbConnName, connection, "UpdateInUserverif");
                  updateIntoUserverif.setString(1, registrationVerificationKey);
                  updateIntoUserverif.setString(2, requestInTime);
                  updateIntoUserverif.setString(3, userid);
                  int j = updateIntoUserverif.executeUpdate();
                  if (j == 0)
                  {
                     if (logger.isDebugEnabled())
                     {
                        logger.debug("No rows inserted into table while updating userveirf table. RegisterUSer.java");
                     }
                     response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal Error. Please try again.");
                     return RuleDecisionKey.ON_FAILURE;
                  }
               }
               catch (Exception e1)
               {
                  logger.error("error", e1);
                  response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Database exception occourred. Please try again.");
                  return RuleDecisionKey.ON_FAILURE;
               }
            }
            else
            {
               response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Database exception occourred. Please try again.");
               return RuleDecisionKey.ON_FAILURE;
            }
         }
         catch (Exception e)
         {
            logger.error("Exception.", e);
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal Error. Please try again.");
            return RuleDecisionKey.ON_FAILURE;
         }
         finally
         {
            DbConnectionManager.releaseConnection(dbConnName, connection);
         }
      }

      regResponse.setRegistrationVerificaitonKey(registrationVerificationKey);

      if (logger.isDebugEnabled())
      {
         logger.debug("User with email id : " + userid + " is added into database successfully");
      }
      response.setParameters(EzedTransactionCodes.SUCCESS, "Userid : " + userid + " registered Successfully.");
      response.setResponseObject(regResponse);
      if (regType == 1)
      {
         return RuleDecisionKey.ON_PARTIAL;
      }
      return RuleDecisionKey.ON_SUCCESS;
   }
}
