package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class PostAbuseHandler extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

   public PostAbuseHandler(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      String userId = (String) flowContext.getFromContext(EzedKeyConstants.USER_ID);

      String postId = messageData.getParameter("postid");
      if (postId == null || postId.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Post id in request is invalid " + postId);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid post id");
         return RuleDecisionKey.ON_FAILURE;
      }
      postId = postId.trim();

      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertPostAbuse");
         ps.setString(1, userId);
         ps.setString(2, sdf.format(new Date()));
         ps.setString(3, postId);
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in db while inserting forum post abuse for post id " + postId);
         }
         if (rows > 0)
         {
            response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
         }
         else
         {
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Abuse not recorded");
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while inserting post abuse in db for postid " + postId, e);
         if (e.getMessage().toUpperCase().contains("UNIQUE") || e.getMessage().toUpperCase().contains("PRIMARY"))
         {
            response.setParameters(EzedTransactionCodes.POST_EVENT_ALREADY_RECORDED, "Post abuse already reported");
            return RuleDecisionKey.ON_FAILURE;
         }
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Abuse not recorded");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while inserting post abuse in db for postid " + postId, e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Abuse not recorded");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "IncrementPostAbuseCount");
         ps.setString(1, postId);
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows updated in db while incrementing abuse count for postid " + postId);
         }
         return RuleDecisionKey.ON_SUCCESS;
      }
      catch (Exception e)
      {
         logger.error("Error while incrementing abuse count in db for postid " + postId, e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Abuse count not recorded");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
}
