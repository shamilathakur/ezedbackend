package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.CourseData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.utils.EzedHelper;

/*
 * This class fetches course path for Demo Chapter For a Course. 	This class is used to fetch content for courses when not purchased.
 */
public class DemoCoursePathFetcherM  extends SingletonRule{

	private String dbConnName;
	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
	
	/*
	 * 
	 */
	public DemoCoursePathFetcherM(String ruleName, String dbConnName) {
		super(ruleName);
		this.dbConnName = dbConnName;
	}

	@Override
	public RuleDecisionKey executeRule(FlowContext flowContext) {
		
		 Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
	     MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
	      
	     String courseId = messageData.getParameter("courseid");
	     if(courseId == null || courseId.trim().compareTo("")==0){
	    	 if(logger.isDebugEnabled()){
	    		 logger.debug("courseid in request is invalid");
	    	 }
	    	 response.setParameters(EzedTransactionCodes.INVALID_COURSE_ID, "Invalid Course Id");
	    	 return RuleDecisionKey.ON_FAILURE;
	     }
	     
	     String userid = messageData.getParameter("email");
	     if(userid == null || userid.trim().compareTo("")==0){
	    	 if(logger.isDebugEnabled()){
	    		 logger.debug("userid in request is invalid");
	    	 }
	    	 response.setParameters(EzedTransactionCodes.INVALID_USERID, "Invalid User Id");
	    	 return RuleDecisionKey.ON_FAILURE;
	     }
	     Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
	     
	    boolean isCoursePurchased = EzedHelper.checkIfCourseAlreadyPurchased(dbConnName, logger, userid, courseId);
	   
	    if(isCoursePurchased){
	    	 response.setParameters(EzedTransactionCodes.COURSE_ALREADY_OWNED, "Course Is Purchased");
	    	 return RuleDecisionKey.ON_FAILURE;
	    }
	     PreparedStatement ps = null;
	     ResultSet rs = null;
	     
	     try{
	    	 
	    	 ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchDefLMRQuestionSolutionFromCourseList");
	    	 ps.setString(1, courseId);
	    	 rs = ps.executeQuery();
	    	 
	    	 if(rs.next()){
	    		 CourseData nonPurchasedCourseData = new CourseData();
	    		 nonPurchasedCourseData.setCourseid(rs.getString("courseid"));
	    		 nonPurchasedCourseData.setCoursename(rs.getString("coursename"));
	    		 nonPurchasedCourseData.setShortdesc(rs.getString("shortdesc"));
	    		 nonPurchasedCourseData.setIntrovideo(rs.getString("introvideo"));
	    		 nonPurchasedCourseData.setDefchapter(rs.getString("defchapter"));
	    		 nonPurchasedCourseData.setContentonlyamt(rs.getString("contentonlyamt"));
	    		 nonPurchasedCourseData.setTestonlyamt(rs.getString("testonlyamt"));
	    		 nonPurchasedCourseData.setFullcourseamt(rs.getString("fullcourseamt"));
	    		 nonPurchasedCourseData.setQuestionpaperid(rs.getString("questionpaperid"));
	    		 nonPurchasedCourseData.setDefchaptername(rs.getString("defaultchaptername"));
	    		 nonPurchasedCourseData.setQuestionpapername("questionpapername");
	    		 response.setResponseObject(nonPurchasedCourseData);
	    		 response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
	    		 return RuleDecisionKey.ON_SUCCESS;
	    		 
	    	 }
	    	 else
	         {
	            if (logger.isDebugEnabled())
	            {
	               logger.debug("No record found in DB for chapterid " + courseId);
	            }
	            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Course not found");
	            return RuleDecisionKey.ON_FAILURE;
	         }
	     }catch(SQLException e){
	    	 logger.error("Error while fetching course details from db for course id " + courseId +" " + e.getMessage());
	         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching course details");
	         return RuleDecisionKey.ON_FAILURE;
	     }
	     catch(Exception e){
	    	 logger.error("Error while fetching chapter details from db for chapter id " + courseId + " " + e.getMessage());
	         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching chapter details");
	         return RuleDecisionKey.ON_FAILURE;
	     }
	     finally
	      {
	         if (rs != null)
	         {
	            try
	            {
	               rs.close();
	            }
	            catch (SQLException e)
	            {
	               logger.error("Error while closing result set while fetching chapter detail", e);
	            }
	         }
	         DbConnectionManager.releaseConnection(dbConnName, conn);
	      }
	    
		
	}
	

}
