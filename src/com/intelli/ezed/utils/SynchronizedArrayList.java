package com.intelli.ezed.utils;

import java.util.ArrayList;
import java.util.HashSet;

public class SynchronizedArrayList
{
   private ArrayList<String> activeSymLinks = new ArrayList<String>();
   private HashSet<String> sessionList = new HashSet<String>();

   private void add(String object, String sessionId)
   {
      synchronized (sessionList)
      {
         while (sessionList.contains(sessionId.intern()))
         {
            try
            {
               sessionList.wait();
            }
            catch (InterruptedException e)
            {
               //put logger here
            }
         }
      }
      sessionList.add(sessionId.intern());
      activeSymLinks.add(object);
      sessionList.remove(sessionId.intern());
      sessionList.notify();
   }
}
