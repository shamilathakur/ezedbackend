package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.AdminProfile;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.utils.MasterDeleteUpdateHelper;

public class StateDeleter extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public StateDeleter(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      AdminProfile adminProfile = (AdminProfile) flowContext.getFromContext(EzedKeyConstants.ADMIN_PROFILE);
      String userId = adminProfile.getUserid();

      String country = messageData.getParameter("country");
      if (country == null || country.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Country name is invalid " + country);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Country name is invalid");
         return RuleDecisionKey.ON_FAILURE;
      }

      String state = messageData.getParameter("state");
      if (state == null || state.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("State name is invalid " + state);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "State name is invalid");
         return RuleDecisionKey.ON_FAILURE;
      }

      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         MasterDeleteUpdateHelper.insertDeletedState(dbConnName, logger, country, state, userId);
         MasterDeleteUpdateHelper.insertDeletedCityForState(dbConnName, logger, country, state, userId);
         MasterDeleteUpdateHelper.insertUpdatedUsersForState(dbConnName, logger, country, state, userId);
         MasterDeleteUpdateHelper.insertUpdatedUniversityForState(dbConnName, logger, country, state, userId);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "DeleteState");
         ps.setString(1, country);
         ps.setString(2, state);
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows deleted from db while deleting state " + state);
         }
         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
         return RuleDecisionKey.ON_SUCCESS;
      }
      catch (SQLException e)
      {
         logger.error("Error while deleting state from db " + state, e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "DB error while deleting state");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while deleting state from db " + state, e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while deleting state");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
}
