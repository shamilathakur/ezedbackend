package com.intelli.ezed.db;

import java.sql.SQLException;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.a.helper.utils.StringSplitter;
import com.intelli.ezed.analytics.data.SingleTestAttemptSummaryData;
import com.intelli.ezed.analytics.data.SingleTestSummaryData;
import com.intelli.ezed.analytics.data.TestSummaryReportData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class TestAttemptReportFetcher extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;

   public TestAttemptReportFetcher(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      String userId = (String) flowContext.getFromContext(EzedKeyConstants.USER_ID);

      //      String courseId = messageData.getParameter("courseid");
      //      if (courseId == null || courseId.trim().compareTo("") == 0)
      //      {
      //         if (logger.isDebugEnabled())
      //         {
      //            logger.debug("course id is invalid " + courseId);
      //         }
      //         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid course id");
      //         return RuleDecisionKey.ON_FAILURE;
      //      }
      //
      //      String chapterId = messageData.getParameter("chapterid");
      //      if (chapterId == null || chapterId.trim().compareTo("") == 0 || chapterId.trim().compareToIgnoreCase("null") == 0)
      //      {
      //         if (logger.isDebugEnabled())
      //         {
      //            logger.debug("chatper id received was " + chapterId + ". Setting chapter id as null");
      //         }
      //         chapterId = null;
      //      }
      String testId = messageData.getParameter("testid");
      if (testId == null || testId.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("invalid test id in request " + testId);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid test id");
         return RuleDecisionKey.ON_FAILURE;
      }
      String[] testIdList = StringSplitter.splitString(testId.trim(), ",");

      TestSummaryReportData data = new TestSummaryReportData();
      //      data.setCourseId(courseId);
      //      data.setChapterId(chapterId);
      data.setUserId(userId);
      SingleTestSummaryData singleTestSummaryData = null;
      SingleTestAttemptSummaryData singleTestAttemptSummaryData = null;
      for (String singleTestId : testIdList)
      {
         singleTestSummaryData = new SingleTestSummaryData();
         singleTestSummaryData.setTestId(singleTestId);
         data.getTestIdList().add(singleTestSummaryData);

         singleTestAttemptSummaryData = new SingleTestAttemptSummaryData();
         singleTestAttemptSummaryData.setTestId(singleTestId);
         data.getTestIdAttemptList().add(singleTestAttemptSummaryData);
      }
      response.setResponseObject(data);
      response.setStatusCodeSend(false);
      //      response.setOnlyArraySend(true);

      try
      {
         data.populateTestSummary(dbConnName, logger);
         data.prepareChart();
         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
         return RuleDecisionKey.ON_SUCCESS;
      }
      catch (SQLException e)
      {
         logger.error("DB error while fetching summary of tests", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching test summary");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching summary of tests", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching test summary");
         return RuleDecisionKey.ON_FAILURE;
      }
   }
}
