package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.ScratchCardData;
import com.intelli.ezed.data.ScratchCardDetailResponse;

public class SingleScratchCardsFetcher extends SingletonRule {

	private String dbConnName;
	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
	SimpleDateFormat format = new SimpleDateFormat("yyyyMM");

	public SingleScratchCardsFetcher(String ruleName, String dbConnName) {
		super(ruleName);
		this.dbConnName = dbConnName;
	}

	@Override
	public RuleDecisionKey executeRule(FlowContext flowContext) {

		Response response = (Response) flowContext
				.getFromContext(EzedKeyConstants.EZED_RESPONSE);
		MessageData messageData = (MessageData) flowContext
				.getFromContext(MessageData.class.getName());
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		String serialno = messageData.getParameter("srno");
		if (serialno == null || serialno.trim().compareTo("") == 0
				|| serialno.trim().compareToIgnoreCase("null") == 0) {
			if (logger.isDebugEnabled()) {
				logger.debug("serialno is Invalid." + serialno);
			}
			response.setParameters(
					EzedTransactionCodes.INVALID_MESSAGE_PARAMETER,
					"serialno is invalid" + serialno);
			return RuleDecisionKey.ON_FAILURE;
		}
		int srno = Integer.parseInt(serialno);

		conn = DbConnectionManager.getConnectionByName(dbConnName);
		ScratchCardData scratchCardData = null;
		try {
			ps = DbConnectionManager.getPreparedStatement(dbConnName, conn,
					"FetchScratchCardBySrNo");
			ps.setInt(1, srno);
			rs = ps.executeQuery();
			if (rs.next()) {
				scratchCardData = new ScratchCardData();
				scratchCardData.setSrno(rs.getInt("srno"));
				scratchCardData.setScratchCardId(rs.getString("scratchcardid"));
				scratchCardData.setCourseId(rs.getString("courseid"));
				scratchCardData.setCourseType(rs.getInt("coursetype"));
				scratchCardData.setPrice(rs.getInt("price"));
				scratchCardData.setValidTill(rs.getString("validtill")
						.substring(0, 6));
				scratchCardData.setAssignedStatus(rs.getInt("assignedstatus"));
				scratchCardData.setUsedStatus(rs.getInt("usedstatus"));
				if (scratchCardData.getAssignedStatus() == 1) {
					scratchCardData.setAssignedTo(rs.getString("partnerid"));
				}
				int flag = scratchCardData.fetchUnivName(dbConnName, logger);
				if (flag == 1) {
					logger.error("Error while fetching UnivName for courseid : "
							+ scratchCardData.getCourseId() + " from db : ");
				}
			}
		} catch (SQLException e) {
			logger.error(
					"Error while fetching Scratch Card details for Srno : "
							+ srno + " from db : ", e);
			response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED,
					"Error while fetching Scratch Card details for srno : "
							+ srno);
			return RuleDecisionKey.ON_FAILURE;
		} catch (Exception e) {
			logger.error(
					"Unknown Error while fetching Scratch Card details for Srno : "
							+ srno, e);
			response.setParameters(EzedTransactionCodes.INTERNAL_ERROR,
					"Error while fetching Scratch Card for srno : " + srno);
			return RuleDecisionKey.ON_FAILURE;
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					logger.error(
							"Error while closing result set while fetching Scratch Card details for Srno : "
									+ srno, e);
				}
			}
			DbConnectionManager.releaseConnection(dbConnName, conn);
		}

		ScratchCardDetailResponse scratchCardDetailResponse = new ScratchCardDetailResponse();
		scratchCardDetailResponse.setScratchCardData(scratchCardData);
		response.setResponseObject(scratchCardDetailResponse);
		response.setParameters(EzedTransactionCodes.SUCCESS,
				"Scratch Card Details Fetched Successfully.");

		return RuleDecisionKey.ON_SUCCESS;
	}

}
