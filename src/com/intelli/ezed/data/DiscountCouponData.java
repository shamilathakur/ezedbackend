package com.intelli.ezed.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;

import com.intelli.ezed.analytics.data.GenericChartData;
import com.intelli.ezed.utils.RandomNumberGenerator;

public class DiscountCouponData extends ResponseObject
{
   public static final int DISCOUNT_VALID = 1;
   public static final int DISCOUNT_INVALID = 0;
   public static final String VALUE_DISCOUNT = "VD";
   public static final String PERCENT_DISCOUNT = "PD";
   public static final String MAX_VALUE_DISCOUNT = "MD";
   public static final SimpleDateFormat sdfDb = new SimpleDateFormat("yyyyMMddHHmmss");

   private String createAdmin;
   private Date createTime;
   private Date startTime;
   private Date endTime;
   private String couponCode;
   private int valueDiscount;
   private float percentDiscount;
   private int maxValueDiscount;
   private int maxCount;
   private String assignedAdmin;
   private int validFlag;
   private int retrialCount;
   private int totalCouponLength;

   public DiscountCouponData(int retrialCount, int totalCouponLength)
   {
      this.retrialCount = retrialCount;
      this.totalCouponLength = totalCouponLength;
   }

   public int getTotalCouponLength()
   {
      return totalCouponLength;
   }

   public void setTotalCouponLength(int totalCouponLength)
   {
      this.totalCouponLength = totalCouponLength;
   }

   public int getRetrialCount()
   {
      return retrialCount;
   }

   public void setRetrialCount(int retrialCount)
   {
      this.retrialCount = retrialCount;
   }

   public String getCreateAdmin()
   {
      return createAdmin;
   }
   public void setCreateAdmin(String createAdmin)
   {
      this.createAdmin = createAdmin;
   }
   public Date getCreateTime()
   {
      return createTime;
   }
   public void setCreateTime(Date createTime)
   {
      this.createTime = createTime;
   }
   public Date getStartTime()
   {
      return startTime;
   }
   public void setStartTime(Date startTime)
   {
      this.startTime = startTime;
   }
   public Date getEndTime()
   {
      return endTime;
   }
   public void setEndTime(Date endTime)
   {
      this.endTime = endTime;
   }
   public String getCouponCode()
   {
      return couponCode;
   }
   public void setCouponCode(String couponCode)
   {
      this.couponCode = couponCode;
   }
   public int getValueDiscount()
   {
      return valueDiscount;
   }
   public void setValueDiscount(int valueDiscount)
   {
      this.valueDiscount = valueDiscount;
   }
   public float getPercentDiscount()
   {
      return percentDiscount;
   }
   public void setPercentDiscount(float percentDiscount)
   {
      this.percentDiscount = percentDiscount;
   }
   public int getMaxValueDiscount()
   {
      return maxValueDiscount;
   }
   public void setMaxValueDiscount(int maxValueDiscount)
   {
      this.maxValueDiscount = maxValueDiscount;
   }
   public int getMaxCount()
   {
      return maxCount;
   }
   public void setMaxCount(int maxCount)
   {
      this.maxCount = maxCount;
   }
   public String getAssignedAdmin()
   {
      return assignedAdmin;
   }
   public void setAssignedAdmin(String assignedAdmin)
   {
      this.assignedAdmin = assignedAdmin;
   }
   public int getValidFlag()
   {
      return validFlag;
   }
   public void setValidFlag(int validFlag)
   {
      this.validFlag = validFlag;
   }

   public void handleCreateCoupon(String dbConnName, Logger logger, int count) throws SQLException, Exception
   {
      setCouponCode(RandomNumberGenerator.generateRandomAlphaNum(getTotalCouponLength()));
      this.persistDiscountCoupon(dbConnName, logger, count);
   }

   //insert into couponlist(couponcode, valuediscount, percentagediscount, maxvaluediscount, starttime, endtime, count, maxcount, assignedadmin, createadmin, createtime, validflag) values (?,?,?,?,?,?,?,?,?,?,?,?)
   private void persistDiscountCoupon(String dbConnName, Logger logger, int count) throws SQLException, Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertDiscountCoupon");
         ps.setString(1, getCouponCode());
         ps.setInt(2, getValueDiscount());
         ps.setString(3, Float.toString(getPercentDiscount()));
         ps.setInt(4, getMaxValueDiscount());
         ps.setString(5, sdfDb.format(getStartTime()));
         ps.setString(6, sdfDb.format(getEndTime()));
         ps.setInt(7, getMaxCount());
         ps.setInt(8, getMaxCount());
         ps.setString(9, getAssignedAdmin());
         ps.setString(10, getCreateAdmin());
         ps.setString(11, sdfDb.format(getCreateTime()));
         ps.setInt(12, getValidFlag());
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in discount db while inserting discount for " + getCouponCode());
         }
      }
      catch (SQLException e)
      {
         if (e.getMessage().toUpperCase().contains("primary"))
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Discount coupon already exists for coupon code " + getCouponCode());
            }
            if (count >= getRetrialCount())
            {
               throw e;
            }
            else
            {
               if (logger.isDebugEnabled())
               {
                  logger.debug("Retrying to generate coupon code and inserting in db");
               }
               this.handleCreateCoupon(dbConnName, logger, count + 1);
            }
         }
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   public boolean isCouponValid(Response response)
   {
      Calendar todayCal = Calendar.getInstance();
      todayCal.setTime(new Date());

      Calendar startDateCal = Calendar.getInstance();
      startDateCal.setTime(getStartTime());

      if (startDateCal.after(todayCal))
      {
         response.setParameters(EzedTransactionCodes.DISCOUNT_NOT_YET_STARTED, "Discount has not yet started");
         return false;
      }

      Calendar endDateCal = Calendar.getInstance();
      endDateCal.setTime(getEndTime());

      if (endDateCal.before(todayCal))
      {
         response.setParameters(EzedTransactionCodes.DISCOUNT_EXPIRED, "Discount has already expired");
         return false;
      }

      if (getValidFlag() != DiscountCouponData.DISCOUNT_VALID)
      {
         response.setParameters(EzedTransactionCodes.DISCOUNT_INVALID, "Discount is not valid");
         return false;
      }
      return true;
   }

   public void applyDiscount(float totalAmountInt, PaymentIdData data, FlowContext flowContext)
   {
      float totalAmount = (float) totalAmountInt;
      if (getValueDiscount() != 0)
      {
         float amountAfterValueDisc = totalAmount - (float) getValueDiscount();
         if (amountAfterValueDisc < 1f)
         {
            data.setNewAmount(totalAmountInt);
            data.setDiscountString("Discount could not be applied on the total amount");
            flowContext.putIntoContext(EzedKeyConstants.DISCOUNT_TYPE, VALUE_DISCOUNT);
            flowContext.putIntoContext(EzedKeyConstants.DISCOUNT_AMOUNT, 0);
            return;
         }
         data.setNewAmount(amountAfterValueDisc);
         data.setDiscountString("A discount of Rs. " + getValueDiscount() + " was provided");
         flowContext.putIntoContext(EzedKeyConstants.DISCOUNT_TYPE, VALUE_DISCOUNT);
         flowContext.putIntoContext(EzedKeyConstants.DISCOUNT_AMOUNT, getValueDiscount());
         return;
      }
      if (getPercentDiscount() != 0.0f)
      {
         float amountAfterPercentageDisc = (totalAmount * getPercentDiscount() / 100f);
         if ((int) amountAfterPercentageDisc > getMaxValueDiscount())
         {
            amountAfterPercentageDisc = totalAmount - (float) getMaxValueDiscount();
            if (amountAfterPercentageDisc < 1f)
            {
               data.setNewAmount(totalAmountInt);
               data.setDiscountString("Discount could not be applied on the total amount");
               flowContext.putIntoContext(EzedKeyConstants.DISCOUNT_TYPE, MAX_VALUE_DISCOUNT);
               flowContext.putIntoContext(EzedKeyConstants.DISCOUNT_AMOUNT, 0);
               return;
            }
            data.setNewAmount(amountAfterPercentageDisc);
            data.setDiscountString("Discount capped at " + getMaxValueDiscount() + " was provided");
            flowContext.putIntoContext(EzedKeyConstants.DISCOUNT_TYPE, MAX_VALUE_DISCOUNT);
            flowContext.putIntoContext(EzedKeyConstants.DISCOUNT_AMOUNT, getMaxValueDiscount());
         }
         else
         {
            flowContext.putIntoContext(EzedKeyConstants.DISCOUNT_AMOUNT, (int) amountAfterPercentageDisc);
            amountAfterPercentageDisc = (int) totalAmount - (int) amountAfterPercentageDisc;
            if (amountAfterPercentageDisc < 1f)
            {
               data.setNewAmount(totalAmountInt);
               data.setDiscountString("Discount could not be applied on the total amount");
               flowContext.putIntoContext(EzedKeyConstants.DISCOUNT_TYPE, PERCENT_DISCOUNT);
               flowContext.putIntoContext(EzedKeyConstants.DISCOUNT_AMOUNT, 0);
               return;
            }
            data.setNewAmount(amountAfterPercentageDisc);
            data.setDiscountString("Percentage discount of " + getPercentDiscount() + " was provided");
            flowContext.putIntoContext(EzedKeyConstants.DISCOUNT_TYPE, PERCENT_DISCOUNT);
         }
         return;
      }
   }

   public GenericChartData applyDiscountInitial(int totalAmountInt) throws Exception
   {
      GenericChartData data = new GenericChartData();
      float totalAmount = (float) totalAmountInt;
      if (getValueDiscount() != 0)
      {
         float amountAfterValueDisc = totalAmount - (float) getValueDiscount();
         if (amountAfterValueDisc < 1f)
         {
            throw new Exception("Amount after subtracting value discount cannot be less than 1 Rs. Cannot apply discount");
         }
         data.getText().add("newamount");
         data.getValues().add((int) amountAfterValueDisc);
         data.getText().add("discountstring");
         data.getValues().add("A discount of Rs. " + getValueDiscount() + " was provided");
         return data;
      }
      if (getPercentDiscount() != 0.0f)
      {
         float amountAfterPercentageDisc = (totalAmount * getPercentDiscount() / 100f);
         if ((int) amountAfterPercentageDisc > getMaxValueDiscount())
         {
            amountAfterPercentageDisc = totalAmount - (float) getMaxValueDiscount();
            if (amountAfterPercentageDisc < 1f)
            {
               throw new Exception("Amount after subtracting max value discount cannot be less than 1 Rs. Cannot apply discount");
            }
            data.getText().add("newamount");
            data.getValues().add((int) amountAfterPercentageDisc);
            data.getText().add("discountstring");
            data.getValues().add("Discount capped at " + getMaxValueDiscount() + " was provided");
         }
         else
         {
            amountAfterPercentageDisc = (int) totalAmount - (int) amountAfterPercentageDisc;
            if (amountAfterPercentageDisc < 1f)
            {
               throw new Exception("Amount after subtracting percentage discount cannot be less than 1 Rs. Cannot apply discount");
            }
            data.getText().add("newamount");
            data.getValues().add((int) amountAfterPercentageDisc);
            data.getText().add("discountstring");
            data.getValues().add("Percentage discount of " + getPercentDiscount() + " was provided");
         }
      }
      return data;
   }

   //   public void applyDiscountTest(int totalAmountInt, PaymentIdData data)
   //   {
   //      setValueDiscount(0);
   //      setPercentDiscount(0f);
   //      setMaxValueDiscount(50);
   //      float totalAmount = (float) totalAmountInt;
   //      if (getValueDiscount() != 0)
   //      {
   //         float amountAfterValueDisc = totalAmount - (float) getValueDiscount();
   //         data.setNewAmount((int) amountAfterValueDisc);
   //         data.setDiscountString("A discount of " + getValueDiscount() + " was provided");
   //         return;
   //      }
   //      if (getPercentDiscount() != 0.0f)
   //      {
   //         float amountAfterPercentageDisc = (totalAmount * getPercentDiscount() / 100f);
   //         if ((int) amountAfterPercentageDisc > getMaxValueDiscount())
   //         {
   //            amountAfterPercentageDisc = totalAmount - (float) getMaxValueDiscount();
   //            data.setNewAmount((int) amountAfterPercentageDisc);
   //            data.setDiscountString("Discount capped at " + getMaxValueDiscount() + " was provided");
   //         }
   //         else
   //         {
   //            amountAfterPercentageDisc = totalAmount - amountAfterPercentageDisc;
   //            data.setNewAmount((int) amountAfterPercentageDisc);
   //            data.setDiscountString("Percentage discount of " + getPercentDiscount() + " was provided");
   //         }
   //         return;
   //      }
   //   }
   //
   //   public static void main(String[] args)
   //   {
   //      DiscountCouponData data = new DiscountCouponData(0, 0);
   //      PaymentIdData dataPay = new PaymentIdData();
   //      data.applyDiscountTest(1000, dataPay);
   //      System.out.println(dataPay.getNewAmount() + "  " + dataPay.getDiscountString());
   //   }

   @Override
   public void prepareResponse() throws Exception
   {
      this.put("couponcode", getCouponCode());
   }
}
