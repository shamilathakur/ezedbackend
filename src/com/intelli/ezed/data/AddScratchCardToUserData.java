package com.intelli.ezed.data;

public class AddScratchCardToUserData {
	private String scratchcardid;
	private int activestatus;
	private int usedstatus;
	private String courseid;
	private int coursetype;
	private int price;
	private String userid;
	private String sessionid;
	private String paymentId;
	private String appId;
	private String courseenddate;
	private String coursestartdate;

	public String getCoursestartdate() {
		return coursestartdate;
	}

	public void setCoursestartdate(String coursestartdate) {
		this.coursestartdate = coursestartdate;
	}

	public String getCourseenddate() {
		return courseenddate;
	}

	public void setCourseenddate(String courseenddate) {
		this.courseenddate = courseenddate;
	}

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getSessionid() {
		return sessionid;
	}

	public void setSessionid(String sessionid) {
		this.sessionid = sessionid;
	}

	public String getScratchcardid() {
		return scratchcardid;
	}

	public void setScratchcardid(String scratchcardid) {
		this.scratchcardid = scratchcardid;
	}

	public int getActivestatus() {
		return activestatus;
	}

	public void setActivestatus(int activestatus) {
		this.activestatus = activestatus;
	}

	public int getUsedstatus() {
		return usedstatus;
	}

	public void setUsedstatus(int usedstatus) {
		this.usedstatus = usedstatus;
	}

	public String getCourseid() {
		return courseid;
	}

	public void setCourseid(String courseid) {
		this.courseid = courseid;
	}

	public int getCoursetype() {
		return coursetype;
	}

	public void setCoursetype(int coursetype) {
		this.coursetype = coursetype;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

}
