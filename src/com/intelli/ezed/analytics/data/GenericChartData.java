package com.intelli.ezed.analytics.data;

import java.util.ArrayList;

import com.intelli.ezed.data.ResponseObject;

public class GenericChartData extends ResponseObject
{
   private ArrayList<String> text = new ArrayList<String>();
   private ArrayList<Object> values = new ArrayList<Object>();

   public ArrayList<String> getText()
   {
      return text;
   }

   public void setText(ArrayList<String> text)
   {
      this.text = text;
   }

   public ArrayList<Object> getValues()
   {
      return values;
   }

   public void setValues(ArrayList<Object> values)
   {
      this.values = values;
   }

   @Override
   public void prepareResponse() throws Exception
   {
      for (int i = 0; i < getValues().size(); i++)
      {
         this.put(getText().get(i), getValues().get(i));
      }
   }

}
