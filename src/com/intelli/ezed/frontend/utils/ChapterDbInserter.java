package com.intelli.ezed.frontend.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedLoggerName;

public class ChapterDbInserter
{
   private static PrepareChapterId prepareChapterId;

   //use your own logger name at this position
   private String loggerName = EzedLoggerName.SERVICE_LOG;
   private Logger logger = LoggerFactory.getLogger(loggerName);

   static
   {
      prepareChapterId = new PrepareChapterId("CHA1","");
   }

   private String getNextChapterId()
   {
      String chapterId = prepareChapterId.getNextId();
      setChapterId(chapterId);
      return chapterId;
   }

   private String chapterId;
   private String chaptername;
   private String description;

   public String getChapterId()
   {
      return chapterId;
   }
   public void setChapterId(String chapterId)
   {
      this.chapterId = chapterId;
   }
   public String getChaptername()
   {
      return chaptername;
   }
   public void setChaptername(String chaptername)
   {
      this.chaptername = chaptername;
   }
   public String getDescription()
   {
      return description;
   }
   public void setDescription(String description)
   {
      this.description = description;
   }

   public int fetchFromDatabase()
   {
      //change how you get your connection here. you can use your own database connection pool or whatever.
      Connection connection = getDbConnection();
      if (connection == null)
      {
         return EzedFrontEndTransactionCodes.NO_CONNECTION;
      }

      //change how you get your prepared statement here. you can put the connection in some configuration files for any other setup.
      PreparedStatement preparedStatement = getFetchStatement(connection);
      if (preparedStatement == null)
      {
         return EzedFrontEndTransactionCodes.NO_STATEMENT;
      }

      ResultSet resultSet = null;

      try
      {
         //chapterid,chaptername,introimage,introvideo,description,weightage
         preparedStatement.setString(1, getChapterId());
         resultSet = preparedStatement.executeQuery();

         setChaptername(resultSet.getString("chaptername"));
         setDescription(resultSet.getString("description"));

      }
      catch (SQLException e)
      {
         logger.error("Database Exception occoured while inserting content in database.", e);
         return EzedFrontEndTransactionCodes.DB_EXCEPTION_OCCURRED;
      }
      catch (Exception e)
      {
         logger.error("Exception occoured while inserting content in database.", e);
         return EzedFrontEndTransactionCodes.INTERNAL_ERROR;
      }
      finally
      {
         try
         {
            resultSet.close();
         }
         catch (Exception e)
         {
            logger.error("Exception occoured while closing result set.", e);
         }
      }
      return EzedFrontEndTransactionCodes.SUCCESS;
   }

   public int persisteIntoDatabase()
   {
      //change how you get your connection here. you can use your own database connection pool or whatever.
      Connection connection = getDbConnection();
      if (connection == null)
      {
         return EzedFrontEndTransactionCodes.NO_CONNECTION;
      }

      //change how you get your prepared statement here. you can put the connection in some configuration files for any other setup.
      PreparedStatement preparedStatement = getUpdateStatement(connection);
      if (preparedStatement == null)
      {
         return EzedFrontEndTransactionCodes.NO_STATEMENT;
      }

      try
      {
         preparedStatement.setString(1, getChaptername());
         preparedStatement.setString(4, getDescription());
         preparedStatement.setString(6, getChapterId());
         boolean i = preparedStatement.execute();

         if (i == true)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Chapter added in database.");
            }
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("no data inserted in database status : " + i);
            }
            return EzedFrontEndTransactionCodes.NO_ROWS_INSERTED;
         }
      }
      catch (SQLException e)
      {
         logger.error("Database Exception occoured while inserting content in database.", e);
         return EzedFrontEndTransactionCodes.DB_EXCEPTION_OCCURRED;
      }
      catch (Exception e)
      {
         logger.error("Exception occoured while inserting content in database.", e);
         return EzedFrontEndTransactionCodes.INTERNAL_ERROR;
      }

      return EzedFrontEndTransactionCodes.SUCCESS;
   }


   public int insertIntoDatabase()
   {
      //change how you get your connection here. you can use your own database connection pool or whatever.
      Connection connection = getDbConnection();
      if (connection == null)
      {
         return EzedFrontEndTransactionCodes.NO_CONNECTION;
      }

      //change how you get your prepared statement here. you can put the connection in some configuration files for any other setup.
      PreparedStatement preparedStatement = getStatement(connection);
      if (preparedStatement == null)
      {
         return EzedFrontEndTransactionCodes.NO_STATEMENT;
      }

      try
      {
         preparedStatement.setString(1, getNextChapterId());
         preparedStatement.setString(2, getChaptername());
         preparedStatement.setString(3, getDescription());
         int i = preparedStatement.executeUpdate();

         if (i == 1)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Chapter added in database.");
            }
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("no data inserted in database status : " + i);
            }
            return EzedFrontEndTransactionCodes.NO_ROWS_INSERTED;
         }
      }
      catch (SQLException e)
      {
         System.out.println(e);
         if (e.getMessage().contains("foreign key constraint"))
         {
            if (logger.isInfoEnabled())
            {
               logger.info("Either intro vedio or into image is not present as content in the system. Please add the content in content table first.", e);
            }
            return EzedFrontEndTransactionCodes.INVALID_OPRATION;
         }
         logger.error("Database Exception occoured while inserting content in database.", e);
         return EzedFrontEndTransactionCodes.DB_EXCEPTION_OCCURRED;
      }
      catch (Exception e)
      {
         System.out.println(e);
         logger.error("Exception occoured while inserting content in database.", e);
         return EzedFrontEndTransactionCodes.INTERNAL_ERROR;
      }
      return EzedFrontEndTransactionCodes.SUCCESS;
   }

   private Connection getDbConnection()
   {
      try
      {
         String url = "jdbc:postgresql://localhost:5432/ezed";
         Class.forName("org.postgresql.Driver");
         return DriverManager.getConnection(url, "postgres", "postgres");
      }
      catch (Exception e)
      {
         return null;
      }
   }

   private PreparedStatement getStatement(Connection conn)
   {
      try
      {
         return (conn.prepareStatement("insert into chapterlist (chapterid,chaptername,description) values (?,?,?)"));
      }
      catch (Exception e)
      {
         return null;
      }
   }

   private PreparedStatement getFetchStatement(Connection conn)
   {
      try
      {
         return (conn.prepareStatement("select * chapterlist where chapterid = ?"));
      }
      catch (Exception e)
      {
         return null;
      }
   }

   private PreparedStatement getUpdateStatement(Connection conn)
   {
      try
      {
         return (conn.prepareStatement("update chapterlist chaptername = ?,introimage= ?,introvideo =?,description=?,weightage = ? where chapterid = ?"));
      }
      catch (Exception e)
      {
         return null;
      }
   }
   
   public static void main(String[] args)
   {
      ChapterDbInserter chapterDbInserter = new ChapterDbInserter();
      chapterDbInserter.setChapterId(chapterDbInserter.getNextChapterId());
      chapterDbInserter.setChaptername("TESTING THIS");
      chapterDbInserter.setDescription("STRING DESCRIPTION");
      chapterDbInserter.insertIntoDatabase();
   }

}
