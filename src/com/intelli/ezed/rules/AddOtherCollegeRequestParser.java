package com.intelli.ezed.rules;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.CollegeData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class AddOtherCollegeRequestParser extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
   private String dbConnName;
   
   public AddOtherCollegeRequestParser(String ruleName, String dbconnName)
   {
      super(ruleName);
      this.dbConnName = dbconnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      
      CollegeData collegeData = new CollegeData();
      
      String collegeName = messageData.getParameter("college");
      if (collegeName == null || collegeName.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("College name in request in null or blank");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid college name");
         return RuleDecisionKey.ON_FAILURE;
      }
      collegeData.setCollege(collegeName);
      
      String univName = messageData.getParameter("univName");
      if (univName == null || univName.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("University name in request in null or blank");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid university name");
         return RuleDecisionKey.ON_FAILURE;
      }
      collegeData.setUnivName(univName);
      
      String phone = messageData.getParameter("phone");
      if (phone == null || phone.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("University phone number in request in null or blank");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid university phone numbers");
         return RuleDecisionKey.ON_FAILURE;
      }
      collegeData.setPhoneNo(phone);
      
      String website = messageData.getParameter("website");
      if (website == null || website.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("University website name in request in null or blank");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid university website");
         return RuleDecisionKey.ON_FAILURE;
      }
      collegeData.setWebsite(website);
      
      String createDateTime = sdf.format(new Date());
      collegeData.setCreatedOn(createDateTime);
      
      String createdBy = messageData.getParameter("email");
      collegeData.setCreatedBy(createdBy);
      
      collegeData.setShowFlag(EzedKeyConstants.COLLEGE_ACTIVE);
      collegeData.setUserFlag(EzedKeyConstants.COLLEGE_ADDED_BY_USER);
      
      flowContext.putIntoContext(EzedKeyConstants.COLLEGE_DATA, collegeData);
      return RuleDecisionKey.ON_SUCCESS;
   }
}
