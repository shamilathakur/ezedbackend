package com.intelli.ezed.utils;

import java.util.ArrayList;
import java.util.Random;

public class Randomizer
{
   private ArrayList<String> permutations = new ArrayList<String>();
   private Random random;

   public Randomizer(int number)
   {
      StringBuffer sb = new StringBuffer();
      for (int i = 1; i <= number; i++)
      {
         sb.append(i);
      }
      permute("", sb.toString());
      random = new Random(permutations.size());
   }

   public void permute(String beginningString, String endingString)
   {
      if (endingString.length() <= 1)
      {
         permutations.add(beginningString + endingString);
      }
      else
      {
         for (int i = 0; i < endingString.length(); i++)
         {
            try
            {
               String newString = endingString.substring(0, i) + endingString.substring(i + 1);

               permute(beginningString + endingString.charAt(i), newString);
            }
            catch (StringIndexOutOfBoundsException exception)
            {
               exception.printStackTrace();
            }
         }
      }
   }

   public String getRandom()
   {
      return permutations.get(random.nextInt());
   }

   public ArrayList<String> getSpacedPermutations(int no)
   {
      ArrayList<String> permutedList = new ArrayList<String>();
      if (permutations.size() < no)
      {
         for (int i = 0; i < permutations.size(); i++)
         {
            permutedList.add(permutations.get(i));
         }
      }
      else
      {
         Random rand = new Random();
         ArrayList<Integer> alreadyEncountered = new ArrayList<Integer>();
         for (int i = 0, j = 0; i < no; i++, j = rand.nextInt(permutations.size()))
         {
            if (alreadyEncountered.contains(j))
            {
               i--;
               continue;
            }
            permutedList.add(permutations.get(j));
            alreadyEncountered.add(j);
         }
      }
      return permutedList;
   }
   public static void main(String[] args)
   {
      Randomizer randomizer = new Randomizer(4);
      System.out.println(randomizer.getSpacedPermutations(5));
   }
}