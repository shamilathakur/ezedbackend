package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;

public class AnalyticMarkBackupTaker extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.ANALYTIC_BACKUP_LOG);
   private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
   private static Map<Integer, String> statusMap = new HashMap<Integer, String>();
   static
   {
      statusMap.put(0, "SUCCESS");
      statusMap.put(1, "ERROR INSERTING");
      statusMap.put(2, "NOT TRIED");
   }

   public AnalyticMarkBackupTaker(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      String prepStmtNameDeleteLastUsers = (String) flowContext.getFromContext(EzedKeyConstants.LAST_BACKUP_DELETE_PREP_STMT_USERS);
      String prepStmtNameDeleteLastToLastUsers = (String) flowContext.getFromContext(EzedKeyConstants.LAST_TO_LAST_BACKUP_DELETE_PREP_STMT_USERS);
      String prepStmtNameDeleteLastUserCourse = (String) flowContext.getFromContext(EzedKeyConstants.LAST_BACKUP_DELETE_PREP_STMT_USER_COURSE);
      String prepStmtNameDeleteLastToLastUserCourse = (String) flowContext.getFromContext(EzedKeyConstants.LAST_TO_LAST_BACKUP_DELETE_PREP_STMT_USER_COURSE);

      String prepStmtNameLastToLastUsers = (String) flowContext.getFromContext(EzedKeyConstants.LAST_TO_LAST_BACKUP_PREP_STMT_USERS);
      String prepStmtNameLastUsers = (String) flowContext.getFromContext(EzedKeyConstants.LAST_BACKUP_PREP_STMT_USERS);
      String prepStmtNameLastToLastUserCourse = (String) flowContext.getFromContext(EzedKeyConstants.LAST_TO_LAST_BACKUP_PREP_STMT_USER_COURSE);
      String prepStmtNameLastUserCourse = (String) flowContext.getFromContext(EzedKeyConstants.LAST_BACKUP_PREP_STMT_USER_COURSE);
      Date updateDate = (Date) flowContext.getFromContext(EzedKeyConstants.TODAY_DATE);
      String analyticDateUpdateStmt = (String) flowContext.getFromContext(EzedKeyConstants.ANALYTIC_DATE_UPDATE_PREPSTMT);

      int lastToLastUserInsertionStatus = 2;
      int lastUserInsertionStatus = 2;
      int lastToLastUserCourseInsertionStatus = 2;
      int lastUserCourseInsertionStatus = 2;
      int analyticDateUpdationStatus = 2;

      int lastToLastUserDeletionStatus = 2;
      int lastUserDeletionStatus = 2;
      int lastToLastUserCourseDeletionStatus = 2;
      int lastUserCourseDeletionStatus = 2;

      Connection conn = null;
      PreparedStatement ps = null;
      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, prepStmtNameDeleteLastToLastUsers);
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows deleted from last to last backup table of users for prepstmt name " + prepStmtNameDeleteLastToLastUsers);
         }
         lastToLastUserDeletionStatus = 0;
      }
      catch (Exception e)
      {
         logger.error("Error while deleting backup records from last to last backup table of users for prepstmt name " + prepStmtNameDeleteLastToLastUsers, e);
         lastToLastUserDeletionStatus = 1;
         logBackupStatus(prepStmtNameDeleteLastToLastUsers, lastToLastUserDeletionStatus, prepStmtNameLastToLastUsers, lastToLastUserInsertionStatus, prepStmtNameDeleteLastUsers, lastUserDeletionStatus, prepStmtNameLastUsers, lastUserInsertionStatus, prepStmtNameDeleteLastToLastUserCourse, lastToLastUserCourseDeletionStatus, prepStmtNameLastToLastUserCourse, lastToLastUserCourseInsertionStatus, prepStmtNameDeleteLastUserCourse, lastUserCourseDeletionStatus, prepStmtNameLastUserCourse, lastUserCourseInsertionStatus, analyticDateUpdateStmt, analyticDateUpdationStatus);
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, prepStmtNameLastToLastUsers);
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in last to last backup table of users for prepstmt name " + prepStmtNameLastToLastUsers);
         }
         lastToLastUserInsertionStatus = 0;
      }
      catch (Exception e)
      {
         logger.error("Error while inserting backup records in last to last backup table of users for prepstmt name " + prepStmtNameLastToLastUsers, e);
         lastToLastUserInsertionStatus = 1;
         logBackupStatus(prepStmtNameDeleteLastToLastUsers, lastToLastUserDeletionStatus, prepStmtNameLastToLastUsers, lastToLastUserInsertionStatus, prepStmtNameDeleteLastUsers, lastUserDeletionStatus, prepStmtNameLastUsers, lastUserInsertionStatus, prepStmtNameDeleteLastToLastUserCourse, lastToLastUserCourseDeletionStatus, prepStmtNameLastToLastUserCourse, lastToLastUserCourseInsertionStatus, prepStmtNameDeleteLastUserCourse, lastUserCourseDeletionStatus, prepStmtNameLastUserCourse, lastUserCourseInsertionStatus, analyticDateUpdateStmt, analyticDateUpdationStatus);
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, prepStmtNameDeleteLastUsers);
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows deleted from last backup table of users for prepstmt name " + prepStmtNameDeleteLastUsers);
         }
         lastUserDeletionStatus = 0;
      }
      catch (Exception e)
      {
         logger.error("Error while deleting backup records from last backup table of users for prepstmt name " + prepStmtNameDeleteLastUsers, e);
         lastUserDeletionStatus = 1;
         logBackupStatus(prepStmtNameDeleteLastToLastUsers, lastToLastUserDeletionStatus, prepStmtNameLastToLastUsers, lastToLastUserInsertionStatus, prepStmtNameDeleteLastUsers, lastUserDeletionStatus, prepStmtNameLastUsers, lastUserInsertionStatus, prepStmtNameDeleteLastToLastUserCourse, lastToLastUserCourseDeletionStatus, prepStmtNameLastToLastUserCourse, lastToLastUserCourseInsertionStatus, prepStmtNameDeleteLastUserCourse, lastUserCourseDeletionStatus, prepStmtNameLastUserCourse, lastUserCourseInsertionStatus, analyticDateUpdateStmt, analyticDateUpdationStatus);
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, prepStmtNameLastUsers);
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in last backup table of users for prepstmt name " + prepStmtNameLastUsers);
         }
         lastUserInsertionStatus = 0;
      }
      catch (Exception e)
      {
         logger.error("Error while inserting backup records in last backup table of users for prepstmt name " + prepStmtNameLastUsers, e);
         lastUserInsertionStatus = 1;
         logBackupStatus(prepStmtNameDeleteLastToLastUsers, lastToLastUserDeletionStatus, prepStmtNameLastToLastUsers, lastToLastUserInsertionStatus, prepStmtNameDeleteLastUsers, lastUserDeletionStatus, prepStmtNameLastUsers, lastUserInsertionStatus, prepStmtNameDeleteLastToLastUserCourse, lastToLastUserCourseDeletionStatus, prepStmtNameLastToLastUserCourse, lastToLastUserCourseInsertionStatus, prepStmtNameDeleteLastUserCourse, lastUserCourseDeletionStatus, prepStmtNameLastUserCourse, lastUserCourseInsertionStatus, analyticDateUpdateStmt, analyticDateUpdationStatus);
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, prepStmtNameDeleteLastToLastUserCourse);
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows deleted from last to last backup table of user course for prepstmt name " + prepStmtNameDeleteLastToLastUserCourse);
         }
         lastToLastUserCourseDeletionStatus = 0;
      }
      catch (Exception e)
      {
         logger.error("Error while deleting backup records from last to last backup table of user course for prepstmt name " + prepStmtNameDeleteLastToLastUserCourse, e);
         lastToLastUserCourseDeletionStatus = 1;
         logBackupStatus(prepStmtNameDeleteLastToLastUsers, lastToLastUserDeletionStatus, prepStmtNameLastToLastUsers, lastToLastUserInsertionStatus, prepStmtNameDeleteLastUsers, lastUserDeletionStatus, prepStmtNameLastUsers, lastUserInsertionStatus, prepStmtNameDeleteLastToLastUserCourse, lastToLastUserCourseDeletionStatus, prepStmtNameLastToLastUserCourse, lastToLastUserCourseInsertionStatus, prepStmtNameDeleteLastUserCourse, lastUserCourseDeletionStatus, prepStmtNameLastUserCourse, lastUserCourseInsertionStatus, analyticDateUpdateStmt, analyticDateUpdationStatus);
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, prepStmtNameLastToLastUserCourse);
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in last to last backup table of user course for prepstmt name " + prepStmtNameLastToLastUserCourse);
         }
         lastToLastUserCourseInsertionStatus = 0;
      }
      catch (Exception e)
      {
         logger.error("Error while inserting backup records in last to last backup table of user course for prepstmt name " + prepStmtNameLastToLastUserCourse, e);
         lastToLastUserCourseInsertionStatus = 1;
         logBackupStatus(prepStmtNameDeleteLastToLastUsers, lastToLastUserDeletionStatus, prepStmtNameLastToLastUsers, lastToLastUserInsertionStatus, prepStmtNameDeleteLastUsers, lastUserDeletionStatus, prepStmtNameLastUsers, lastUserInsertionStatus, prepStmtNameDeleteLastToLastUserCourse, lastToLastUserCourseDeletionStatus, prepStmtNameLastToLastUserCourse, lastToLastUserCourseInsertionStatus, prepStmtNameDeleteLastUserCourse, lastUserCourseDeletionStatus, prepStmtNameLastUserCourse, lastUserCourseInsertionStatus, analyticDateUpdateStmt, analyticDateUpdationStatus);
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, prepStmtNameDeleteLastUserCourse);
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows deleted from last backup table of user course for prepstmt name " + prepStmtNameDeleteLastUserCourse);
         }
         lastUserCourseDeletionStatus = 0;
      }
      catch (Exception e)
      {
         logger.error("Error while deleting backup records in last backup table of user course for prepstmt name " + prepStmtNameDeleteLastUserCourse, e);
         lastUserCourseDeletionStatus = 1;
         logBackupStatus(prepStmtNameDeleteLastToLastUsers, lastToLastUserDeletionStatus, prepStmtNameLastToLastUsers, lastToLastUserInsertionStatus, prepStmtNameDeleteLastUsers, lastUserDeletionStatus, prepStmtNameLastUsers, lastUserInsertionStatus, prepStmtNameDeleteLastToLastUserCourse, lastToLastUserCourseDeletionStatus, prepStmtNameLastToLastUserCourse, lastToLastUserCourseInsertionStatus, prepStmtNameDeleteLastUserCourse, lastUserCourseDeletionStatus, prepStmtNameLastUserCourse, lastUserCourseInsertionStatus, analyticDateUpdateStmt, analyticDateUpdationStatus);
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, prepStmtNameLastUserCourse);
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in last backup table of user course for prepstmt name " + prepStmtNameLastUserCourse);
         }
         lastUserCourseInsertionStatus = 0;
      }
      catch (Exception e)
      {
         logger.error("Error while inserting backup records in last backup table of user course for prepstmt name " + prepStmtNameLastUserCourse, e);
         lastUserCourseInsertionStatus = 1;
         logBackupStatus(prepStmtNameDeleteLastToLastUsers, lastToLastUserDeletionStatus, prepStmtNameLastToLastUsers, lastToLastUserInsertionStatus, prepStmtNameDeleteLastUsers, lastUserDeletionStatus, prepStmtNameLastUsers, lastUserInsertionStatus, prepStmtNameDeleteLastToLastUserCourse, lastToLastUserCourseDeletionStatus, prepStmtNameLastToLastUserCourse, lastToLastUserCourseInsertionStatus, prepStmtNameDeleteLastUserCourse, lastUserCourseDeletionStatus, prepStmtNameLastUserCourse, lastUserCourseInsertionStatus, analyticDateUpdateStmt, analyticDateUpdationStatus);
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      conn = DbConnectionManager.getConnectionByName(dbConnName);
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, analyticDateUpdateStmt);
         ps.setString(1, sdf.format(updateDate));
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows updated in backup dates table for prepstmt name " + analyticDateUpdateStmt);
         }
         analyticDateUpdationStatus = 0;
      }
      catch (Exception e)
      {
         logger.error("Error while updating date in backup dates table for prepstmt name " + analyticDateUpdateStmt, e);
         analyticDateUpdationStatus = 1;
         logBackupStatus(prepStmtNameDeleteLastToLastUsers, lastToLastUserDeletionStatus, prepStmtNameLastToLastUsers, lastToLastUserInsertionStatus, prepStmtNameDeleteLastUsers, lastUserDeletionStatus, prepStmtNameLastUsers, lastUserInsertionStatus, prepStmtNameDeleteLastToLastUserCourse, lastToLastUserCourseDeletionStatus, prepStmtNameLastToLastUserCourse, lastToLastUserCourseInsertionStatus, prepStmtNameDeleteLastUserCourse, lastUserCourseDeletionStatus, prepStmtNameLastUserCourse, lastUserCourseInsertionStatus, analyticDateUpdateStmt, analyticDateUpdationStatus);
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
      logBackupStatus(prepStmtNameDeleteLastToLastUsers, lastToLastUserDeletionStatus, prepStmtNameLastToLastUsers, lastToLastUserInsertionStatus, prepStmtNameDeleteLastUsers, lastUserDeletionStatus, prepStmtNameLastUsers, lastUserInsertionStatus, prepStmtNameDeleteLastToLastUserCourse, lastToLastUserCourseDeletionStatus, prepStmtNameLastToLastUserCourse, lastToLastUserCourseInsertionStatus, prepStmtNameDeleteLastUserCourse, lastUserCourseDeletionStatus, prepStmtNameLastUserCourse, lastUserCourseInsertionStatus, analyticDateUpdateStmt, analyticDateUpdationStatus);
      return RuleDecisionKey.ON_SUCCESS;
   }

   private void logBackupStatus(String prepStmtName1, int status1, String prepStmtName2, int status2, String prepStmtName3, int status3, String prepStmtName4, int status4, String prepStmtName5, int status5, String prepStmtName6, int status6, String prepStmtName7, int status7, String prepStmtName8, int status8, String prepStmtName9, int status9)
   {
      StringBuffer sb = new StringBuffer();
      sb.append(prepStmtName1).append(" : ").append(statusMap.get(status1)).append("\n");
      sb.append(prepStmtName2).append(" : ").append(statusMap.get(status2)).append("\n");
      sb.append(prepStmtName3).append(" : ").append(statusMap.get(status3)).append("\n");
      sb.append(prepStmtName4).append(" : ").append(statusMap.get(status4)).append("\n");
      sb.append(prepStmtName5).append(" : ").append(statusMap.get(status5)).append("\n");
      sb.append(prepStmtName6).append(" : ").append(statusMap.get(status6)).append("\n");
      sb.append(prepStmtName7).append(" : ").append(statusMap.get(status7)).append("\n");
      sb.append(prepStmtName8).append(" : ").append(statusMap.get(status8)).append("\n");
      sb.append(prepStmtName9).append(" : ").append(statusMap.get(status9)).append("\n");
      if (logger.isInfoEnabled())
      {
         logger.info("Backup log : \n" + sb.toString());
      }

   }
}
