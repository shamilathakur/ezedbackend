package com.intelli.ezed.checks;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.QuizRequestResponse;
import com.intelli.ezed.data.Response;

public class FetchQuizRequestChecker extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public FetchQuizRequestChecker(String ruleName)
   {
      super(ruleName);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      String quizId = messageData.getParameter("quizid");
      if (quizId == null || quizId.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Quiz id not provided or is not valid : " + quizId);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Quiz id absent or invalid");
         return RuleDecisionKey.ON_FAILURE;
      }
      QuizRequestResponse quizRequestResponse = new QuizRequestResponse();
      quizRequestResponse.setQuizId(quizId);
      response.setResponseObject(quizRequestResponse);
      if (logger.isDebugEnabled())
      {
         logger.debug("Fetch quiz request parsed successfully for quiz id " + quizId);
      }
      return RuleDecisionKey.ON_SUCCESS;
   }

}
