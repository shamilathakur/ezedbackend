package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.utils.StringSplitter;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.FrontendFetchData;
import com.intelli.ezed.data.QuestionContent;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.TestQuestionData;

public class QuestionListFromCourseFetcher extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public QuestionListFromCourseFetcher(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      String[] courseId = (String[]) flowContext.getFromContext(EzedKeyConstants.COURSE_ID_LIST);
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      String[] questionArr = null;
      FrontendFetchData questionData = new FrontendFetchData();
      response.setResponseObject(questionData);
      response.setStatusCodeSend(false);
      questionData.setAttribName("aaData");
      ArrayList<String[]> questions = new ArrayList<String[]>();
      int i = 1;
      int questionType = 0;
      int questionContentType = 0;

      for (int j = 0; j < courseId.length; j++)
      {
         try
         {
            conn = DbConnectionManager.getConnectionByName(dbConnName);
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchChapterLevelQuestionForCourse");
            ps.setString(1, courseId[j]);
            rs = ps.executeQuery();
            while (rs.next())
            {
               questionArr = new String[6];
               questionArr[0] = Integer.toString(i);
               questionArr[1] = rs.getString("questionid");
               questionType = rs.getInt("questiontype");
               if (questionType == TestQuestionData.MCQ_QUESTION_TYPE)
               {
                  questionArr[2] = "MCQ";
               }
               else if (questionType == TestQuestionData.MTF_QUESTION_TYPE)
               {
                  questionArr[2] = "MTF";
               }
               else
               {
                  questionArr[2] = "FIB";
               }
               questionArr[3] = Integer.toString(rs.getInt("difficultylevel"));
               questionContentType = rs.getInt("questioncontenttype");
               if (questionContentType == QuestionContent.STRING_TYPE)
               {
                  questionArr[4] = "String";
                  if (questionType != TestQuestionData.MTF_QUESTION_TYPE)
                  {
                     questionArr[5] = rs.getString("questionstring");
                  }
                  else
                  {
                     questionArr[5] = StringSplitter.splitString(rs.getString("questionstring"), ",")[0];
                  }
               }
               else if (questionContentType == QuestionContent.CONTENT_TYPE)
               {
                  questionArr[4] = "Content";
                  questionArr[5] = "N.A.";
               }
               else
               {
                  questionArr[4] = "N.A";
                  questionArr[5] = "N.A";
               }
               questions.add(questionArr);
               i++;
            }
         }
         catch (SQLException e)
         {
            logger.error("Error while fetching chapter question details for course", e);
            response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching chapter questions for course");
            return RuleDecisionKey.ON_FAILURE;
         }
         catch (Exception e)
         {
            logger.error("Error while fetching chapter question details for course", e);
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching chapter questions for course");
            return RuleDecisionKey.ON_FAILURE;
         }
         finally
         {
            if (rs != null)
            {
               try
               {
                  rs.close();
               }
               catch (SQLException e)
               {
               }
            }
            DbConnectionManager.releaseConnection(dbConnName, conn);
         }

         try
         {
            conn = DbConnectionManager.getConnectionByName(dbConnName);
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchCourseLevelQuestion");
            ps.setString(1, courseId[j]);
            rs = ps.executeQuery();
            while (rs.next())
            {
               questionArr = new String[6];
               questionArr[0] = Integer.toString(i);
               questionArr[1] = rs.getString("questionid");
               questionType = rs.getInt("questiontype");
               if (questionType == TestQuestionData.MCQ_QUESTION_TYPE)
               {
                  questionArr[2] = "MCQ";
               }
               else if (questionType == TestQuestionData.MTF_QUESTION_TYPE)
               {
                  questionArr[2] = "MTF";
               }
               else
               {
                  questionArr[2] = "FIB";
               }
               questionArr[3] = Integer.toString(rs.getInt("difficultylevel"));
               questionContentType = rs.getInt("questioncontenttype");
               if (questionContentType == QuestionContent.STRING_TYPE)
               {
                  questionArr[4] = "String";
                  if (questionType != TestQuestionData.MTF_QUESTION_TYPE)
                  {
                     questionArr[5] = rs.getString("questionstring");
                  }
                  else
                  {
                     questionArr[5] = StringSplitter.splitString(rs.getString("questionstring"), ",")[0];
                  }
               }
               else if (questionContentType == QuestionContent.CONTENT_TYPE)
               {
                  questionArr[4] = "Content";
                  questionArr[5] = "N.A.";
               }
               else
               {
                  questionArr[4] = "N.A";
                  questionArr[5] = "N.A";
               }
               questions.add(questionArr);
               i++;
            }
         }
         catch (SQLException e)
         {
            logger.error("Error while fetching question details for course only", e);
            response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching questions for course only");
            return RuleDecisionKey.ON_FAILURE;
         }
         catch (Exception e)
         {
            logger.error("Error while fetching question details for course only", e);
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching questions for course only");
            return RuleDecisionKey.ON_FAILURE;
         }
         finally
         {
            if (rs != null)
            {
               try
               {
                  rs.close();
               }
               catch (SQLException e)
               {
               }
            }
            DbConnectionManager.releaseConnection(dbConnName, conn);
         }
      }
      questionData.setAttribList(questions);
      response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
      return RuleDecisionKey.ON_SUCCESS;
   }
}
