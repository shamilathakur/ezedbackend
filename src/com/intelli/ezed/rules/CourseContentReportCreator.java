package com.intelli.ezed.rules;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.analytics.data.DonutData;
import com.intelli.ezed.analytics.data.DonutDrillDownData;
import com.intelli.ezed.analytics.data.DonutOuterData;
import com.intelli.ezed.analytics.data.GenericChartData;
import com.intelli.ezed.bulkupload.data.ContentDetails;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.utils.EzedHelper;

public class CourseContentReportCreator extends SingletonRule
{

   private static Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;

   public CourseContentReportCreator(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      String courseId = messageData.getParameter("courseid");
      Connection connection = DbConnectionManager.getConnectionByName(dbConnName);

      ResultSet resultSet = null;

      Vector<String> chapterList = new Vector<String>();
      Vector<Float> chapterContentCount = new Vector<Float>();

      float tallyContentCount = 0;

      try
      {
         //select count(contentid), chapterid from learningpath 
         //where chapterid in (select chapterid from coursepath where courseid = ? and chapterid is not null) GROUP BY chapterid
         PreparedStatement getChapterwiseContentCount = DbConnectionManager.getPreparedStatement(dbConnName, connection, "GetChapterwiseContentCount");
         getChapterwiseContentCount.setString(1, courseId);

         resultSet = getChapterwiseContentCount.executeQuery();

         while (resultSet.next())
         {
            float count = resultSet.getFloat("count");
            tallyContentCount += count;
            String chapterId = resultSet.getString("chapterid");

            chapterList.add(chapterId);
            chapterContentCount.add(count);
         }

         if (chapterList.size() == 0)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("No content/chapter found for course : " + courseId);
            }
            response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Given course does not content any chapter.");
            return RuleDecisionKey.ON_FAILURE;
         }

         if (logger.isDebugEnabled())
         {
            logger.debug("Total content count for chapters : " + tallyContentCount + " for course id : " + courseId);
         }

      }
      catch (SQLException e)
      {
         logger.error("Database exception while trying to get chapter list for courseid : " + courseId + " ", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Database Exception. Please try again later.");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Exception while trying to get chapter list for courseid : " + courseId + " ", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal error. Please try again later.");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         try
         {
            resultSet.close();
         }
         catch (Exception e)
         {
            logger.debug("Exception while closing result set in CourseContentReportCreator.", e);
         }
         DbConnectionManager.releaseConnection(dbConnName, connection);
      }

      //This array will contain data to display the internal Pi chart for the donut
      float[] chapterContentCountArray = new float[chapterContentCount.size()];
      for (int i = 0; i < chapterContentCount.size(); i++)
      {
         chapterContentCountArray[i] = chapterContentCount.get(i);
      }
      EzedHelper.calculatePercentage(chapterContentCountArray, 0);


      DonutOuterData[] outDonutOuterDatas = new DonutOuterData[chapterList.size()];

      resultSet = null;
      connection = DbConnectionManager.getConnectionByName(dbConnName);
      try
      {
         for (int k = 0; k < chapterList.size(); k++)
         {
            Vector<Float> chaperWiseContentDistribustionCount = new Vector<Float>();
            Vector<String> chapterWizeContentType = new Vector<String>();
            
            String chapterIdFromList = chapterList.get(k);
            DonutDrillDownData drillDownData = new DonutDrillDownData();
            drillDownData.setName(EzedHelper.fetchChapterNameForChapterId(dbConnName, chapterIdFromList, logger));

            //select count(c.contenttype),c.contenttype,o.courseid,l.chapterid from contentlist c, learningpath l, coursepath o
            //where l.chapterid = ? 
            //and c.contentid = l.contentid
            //and o.chapterid = l.chapterid
            //GROUP BY c.contenttype, o.courseid, l.chapterid

            PreparedStatement getChapterwiseContentDistribustion = DbConnectionManager.getPreparedStatement(dbConnName, connection, "GetChapterWiseContentDistribustion");
            getChapterwiseContentDistribustion.setString(1, chapterIdFromList);
            getChapterwiseContentDistribustion.setString(2, courseId);
            resultSet = getChapterwiseContentDistribustion.executeQuery();

            while (resultSet.next())
            {
               float count = resultSet.getFloat("count");
               int contentType = resultSet.getInt("contenttype");

               //logger.debug("content type :" + contentType);
               chaperWiseContentDistribustionCount.add(count);
               chapterWizeContentType.add(ContentDetails.getContentStringMap().get(contentType));
            }

            String[] categories = new String[chapterWizeContentType.size()];
            float[] data = new float[chapterWizeContentType.size()];

            for (int i = 0; i < chapterWizeContentType.size(); i++)
            {
               categories[i] = chapterWizeContentType.get(i);
               data[i] = chaperWiseContentDistribustionCount.get(i);
            }

            //Calculate % depending on total distribution
            EzedHelper.calculatePercentage(data, tallyContentCount);

            drillDownData.setCategories(categories);
            drillDownData.setData(data);

            DonutOuterData outerData = new DonutOuterData();
            outerData.setDrilldown(drillDownData);

            outerData.setY(chapterContentCountArray[k]);

            outDonutOuterDatas[k] = outerData;
         }

      }
      catch (SQLException e)
      {
         logger.error("Database exception while trying to get chapter list for courseid : " + courseId + " ", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Database Exception. Please try again later.");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Exception while trying to get chapter list for courseid : " + courseId + " ", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal error. Please try again later.");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         try
         {
            resultSet.close();
         }
         catch (Exception e)
         {
            logger.debug("Exception while closing result set in CourseContentReportCreator.", e);
         }
         DbConnectionManager.releaseConnection(dbConnName, connection);
      }

      DonutData donutData = new DonutData();
      donutData.setName(EzedHelper.fetchCourseName(dbConnName, courseId, logger));
      donutData.setData(outDonutOuterDatas);
      String[] outerDataCategories = new String[chapterList.size()];
      for (int j = 0; j < chapterList.size(); j++)
      {
         outerDataCategories[j] = EzedHelper.fetchChapterNameForChapterId(dbConnName, chapterList.get(j), logger);
      }
      donutData.setCategoris(outerDataCategories);

      response.setResponseObject(donutData);

      donutData.getType().getText().add("type");
      donutData.getType().getValues().add("pie");

      donutData.getTitle().getText().add("text");
      donutData.getTitle().getValues().add("Chapter wise content share.");

      donutData.getxAxis().getText().add("categories");


      donutData.getyAxis().getText().add("title");
      GenericChartData yAxisText = new GenericChartData();
      yAxisText.getText().add("text");
      yAxisText.getValues().add("Total content share");
      donutData.getyAxis().getValues().add(yAxisText);
      try
      {
         yAxisText.prepareResponse();
      }
      catch (Exception e)
      {
         logger.error("Exception Occoured while prepare response", e);
      }
      donutData.getyAxis().getText().add("Total count");

      response.setParameters(EzedTransactionCodes.SUCCESS, "Donut data sent.");

      response.setStatusCodeSend(false);

      return RuleDecisionKey.ON_SUCCESS;
   }
}
