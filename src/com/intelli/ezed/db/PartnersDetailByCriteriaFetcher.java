package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.AdminLoginResponse;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.PartnerData;
import com.intelli.ezed.data.PartnerDetailResponse;
import com.intelli.ezed.data.Response;

public class PartnersDetailByCriteriaFetcher extends SingletonRule {

	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
	private String dbConnName;
	SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyyMMddHHmmss");
	SimpleDateFormat formatTo = new SimpleDateFormat("yyyy-MM-dd");

	public PartnersDetailByCriteriaFetcher(String ruleName, String dbConnName) {
		super(ruleName);
		this.dbConnName = dbConnName;
	}

	@Override
	public RuleDecisionKey executeRule(FlowContext flowContext) {
		Response response = (Response) flowContext
				.getFromContext(EzedKeyConstants.EZED_RESPONSE);

		MessageData messageData = (MessageData) flowContext
				.getFromContext(MessageData.class.getName());

		String partnerId = messageData.getParameter("partnerid");
		if (partnerId == null || partnerId.trim().compareTo("") == 0
				|| partnerId.trim().compareToIgnoreCase("null") == 0) {
			if (logger.isDebugEnabled()) {
				logger.debug("Partner Id Invalid." + partnerId);
			}
			response.setParameters(
					EzedTransactionCodes.INVALID_MESSAGE_PARAMETER,
					"Partnerid is invalid");
			return RuleDecisionKey.ON_FAILURE;
		}

		Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
		PreparedStatement ps = null;
		ResultSet rs = null;
		PartnerData partnerData = null;
		try {
			ps = DbConnectionManager.getPreparedStatement(dbConnName, conn,
					"FetchPartnerById");
			ps.setString(1, partnerId);
			rs = ps.executeQuery();
			while (rs.next()) {
				partnerData = new PartnerData();
				partnerData.setSrno(rs.getInt("srno"));
				partnerData.setPartnerId(rs.getString("partnerid"));
				partnerData.setContactPerson(rs.getString("contactperson"));
				partnerData.setPartnerName(rs.getString("partnername"));
				partnerData.setAddress(rs.getString("address"));
				partnerData.setState(rs.getString("state"));
				partnerData.setCity(rs.getString("city"));
				partnerData.setCountry(rs.getString("country"));
				partnerData.setContactNumber(rs.getString("contactnumber"));
				partnerData.setDateOfJoining(formatTo.format(sourceFormat
						.parse(rs.getString("dateofjoining"))));
				partnerData.setEmailaddress(rs.getString("emailaddress"));
				partnerData.setStatus(rs.getInt("status"));
			}
		} catch (SQLException e) {
			logger.error(
					"Error while fetching partner details for partner id : "
							+ partnerId + " from db : ", e);
			response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED,
					"Error while fetching partner details for partner id : "
							+ partnerId);
			return RuleDecisionKey.ON_FAILURE;
		} catch (Exception e) {
			logger.error(
					"Unknown Error while fetching partner details for partner id : "
							+ partnerId, e);
			response.setParameters(EzedTransactionCodes.INTERNAL_ERROR,
					"Error while fetching partner details for partner id : "
							+ partnerId);
			return RuleDecisionKey.ON_FAILURE;
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					logger.error(
							"Error while closing result set while fetching Partner details for partner id : "
									+ partnerId, e);
				}
			}
			DbConnectionManager.releaseConnection(dbConnName, conn);
		}

		PartnerDetailResponse partnerDetailResponse = new PartnerDetailResponse();
		partnerDetailResponse.setPartnerData(partnerData);
		response.setResponseObject(partnerDetailResponse);
		response.setParameters(EzedTransactionCodes.SUCCESS,
				"Partner Details Fetched Successfully.");
		return RuleDecisionKey.ON_SUCCESS;
	}

}
