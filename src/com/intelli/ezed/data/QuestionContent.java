package com.intelli.ezed.data;

import com.intelli.ezed.utils.EzedHelper;

public class QuestionContent
{
   public static final int CONTENT_ABSENT = 0;
   public static final int STRING_TYPE = 1;
   public static final int CONTENT_TYPE = 2;

   private int type;
   private String contentString;
   private String contentLoc;

   public QuestionContent()
   {

   }

   public QuestionContent(int type)
   {
      this.setType(type);
   }

   public int getType()
   {
      return type;
   }
   public void setType(int type)
   {
      this.type = type;
   }
   public String getContentString()
   {
      return contentString;
   }
   public void setContentString(String contentString)
   {
      this.contentString = EzedHelper.handleBulkUploadString(contentString, 200);
      //      try
      //      {
      //         this.contentString = new String(this.contentString.getBytes(), "UTF-8");
      //      }
      //      catch (UnsupportedEncodingException e)
      //      {
      //         // TODO Auto-generated catch block
      //         e.printStackTrace();
      //      }
   }
   public String getContentLoc()
   {
      return contentLoc;
   }
   public void setContentLoc(String contentLoc)
   {
      this.contentLoc = contentLoc;
   }
}
