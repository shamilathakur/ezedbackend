package com.intelli.ezed.rules;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.AdminStatusData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class AdminRoleUpdater extends SingletonRule
{

   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;

   public AdminRoleUpdater(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      String adminUserid = messageData.getParameter("userid");
      String changeRole = messageData.getParameter("changerole");
      if (changeRole == null || changeRole.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("New roles is invalid " + changeRole);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid new roles");
      }

      AdminStatusData data = new AdminStatusData();
      data.setUserid(adminUserid);
      data.setRoll(changeRole);
      flowContext.putIntoContext(EzedKeyConstants.ADMIN_STATUS_DATA, data);
      Connection connection = DbConnectionManager.getConnectionByName(dbConnName);

      try
      {
         //delete * from adminrepo where userid = ?;
         PreparedStatement deleteLerningPath = DbConnectionManager.getPreparedStatement(dbConnName, connection, "DeleteAdminRoll");
         deleteLerningPath.setString(1, adminUserid);
         int i = deleteLerningPath.executeUpdate();
         if (i == 0)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("No admin role found for user id = " + adminUserid);
            }
         }
      }
      catch (Exception e)
      {
         logger.error("Exception occoured while deleting old role from the database.", e);
      }

      String[] str = changeRole.split(",");

      try
      {
         int i = 1;
         for (String rollId : str)
         {
            //Insert into adminrepo (userid,roleid) values(?,?)
            PreparedStatement addUserRolls = DbConnectionManager.getPreparedStatement(dbConnName, connection, "InsertAdminRoles");
            addUserRolls.setString(1, adminUserid);
            addUserRolls.setInt(2, Integer.parseInt(rollId));
            int k = addUserRolls.executeUpdate();

            if (k != 1)
            {
               if (logger.isInfoEnabled())
               {
                  logger.info("Some problem while admin role to the database : " + adminUserid + " role id : " + rollId);
               }
            }
            else
            {
               if (logger.isInfoEnabled())
               {
                  logger.info("Added role : " + rollId + " to the role of admin : " + adminUserid + " at : " + i);
               }
            }
            i++;
         }
      }
      catch (SQLException e)
      {
         logger.error("Exception occoured : ", e);
         if (e.getMessage().contains("foreign key constraint"))
         {
            logger.error("Invalid role id is used.");
            response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Please check the role id.");
            return RuleDecisionKey.ON_FAILURE;
         }
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Database exception occoured while updating user roles.");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Exception occoured : ", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal Error occoured while updating user role.");
         return RuleDecisionKey.ON_FAILURE;
      }
      //      response.setParameters(EzedTransactionCodes.SUCCESS, "Admin roles updated successfully for admin : " + adminUserid);
      return RuleDecisionKey.ON_SUCCESS;
   }

}
