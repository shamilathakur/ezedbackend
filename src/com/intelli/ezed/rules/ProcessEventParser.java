package com.intelli.ezed.rules;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EventData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.MasterActionRecords;
import com.intelli.ezed.data.Response;

public class ProcessEventParser extends SingletonRule
{

   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public ProcessEventParser(String ruleName)
   {
      super(ruleName);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      String actionId = messageData.getParameter("actionid");
      if (actionId == null || actionId.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Action id received in event process request is improper");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid action id");
         return RuleDecisionKey.ON_FAILURE;
      }

      if (!MasterActionRecords.isRecordPresent(actionId))
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Action id received in event process request is invalid" + actionId);
         }
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Invalid action id");
         return RuleDecisionKey.ON_FAILURE;
      }

      String courseId = messageData.getParameter("courseid");
      if (courseId == null || courseId.trim().compareTo("") == 0)
      {
         courseId = null;
      }

      String chapterId = messageData.getParameter("chapterid");
      if (chapterId == null || chapterId.trim().compareTo("") == 0)
      {
         chapterId = null;
      }

      String contentId = messageData.getParameter("contentid");
      if (contentId == null || contentId.trim().compareTo("") == 0)
      {
         contentId = null;
      }

      EventData eventData = new EventData();
      eventData.setActionId(actionId);
      eventData.setCourseId(courseId);
      eventData.setChapterId(chapterId);
      eventData.setContentId(contentId);
      eventData.setGoalRelated(true);
      flowContext.putIntoContext(EzedKeyConstants.EVENT_OBJECT, eventData);
      if (logger.isDebugEnabled())
      {
         logger.debug("Event parsed successfully");
      }
      return RuleDecisionKey.ON_SUCCESS;
   }

}
