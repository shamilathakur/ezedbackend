package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.bulkupload.data.ContentDetails;
import com.intelli.ezed.data.CourseIntroData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.QuestionContent;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.utils.EndDateCalculator;
import com.intelli.ezed.utils.EzedHelper;
import com.intelli.ezed.utils.SymLinkManager;

public class CourseIntroDetailsFetcher extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");
   private String newLinkLocationNotes;
   private String newLinkLocationVideos;
   private String notesStartPoint;
   private String vidStartPoint;

   public CourseIntroDetailsFetcher(String ruleName, String dbConnName, String newLinkLoc, String newLinkVideos,
                                    String notesStartPoint, String vidStartPoint)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
      this.newLinkLocationNotes = newLinkLoc;
      this.newLinkLocationVideos = newLinkVideos;
      this.notesStartPoint = notesStartPoint;
      this.vidStartPoint = vidStartPoint;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());

      String courseId = messageData.getParameter("courseid");
      if (courseId == null || courseId.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Course id in request in invalid");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid course id");
         return RuleDecisionKey.ON_FAILURE;
      }
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      CourseIntroData introData = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchCourseChapterDetails");
         ps.setString(1, courseId);
         rs = ps.executeQuery();
         boolean executedOnce = false;
         while (rs.next())
         {
            if (!executedOnce)
            {
               introData = new CourseIntroData();
               introData.setCourseId(courseId);
               introData.setCourseName(rs.getString("coursename"));
               introData.setIntroVidId(rs.getString("introvideo"));
               introData.setShortDesc(rs.getString("shortdesc"));
               introData.setLongDescType(rs.getInt("longdesctype"));
               introData.setLongDescString(rs.getString("longdescstring"));
               introData.setLongDescContentId(rs.getString("longdesccontent"));
               introData.setIntroImage(rs.getString("introimage"));
               String endMonths = rs.getString("enddate");
               Date endDate = EndDateCalculator.findEndDate(endMonths, new Date());
               Calendar cal = Calendar.getInstance();
               cal.setTime(endDate);
               int day = cal.get(Calendar.DAY_OF_MONTH);
               String suffix = EzedHelper.getDayOfMonthSuffix(day);
               String dateString = sdf.format(cal.getTime());
               dateString = dateString.substring(0, dateString.indexOf(' ')) + suffix + " " + dateString.substring(dateString.indexOf(' '), dateString.length());
               introData.setEndDate(dateString);
               executedOnce = true;
            }
            introData.getChapterList().add(rs.getString("chaptername"));
         }
         if (introData == null)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("No course found for course id " + courseId);
            }
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Course details not found");
            return RuleDecisionKey.ON_FAILURE;
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while course details from db for course id " + courseId, e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching course details");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching course details from db for course id " + courseId, e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching course details");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing result set while fetching course details", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchChapterCount");
         ps.setString(1, courseId);
         rs = ps.executeQuery();
         if (rs.next())
         {
            introData.setChapterCount(rs.getInt(1));
         }
         else
         {
            introData.setChapterCount(0);
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching chapter count from db for course id " + courseId, e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching course details");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching chapter count from db for course id " + courseId, e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching course details");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing result set while fetching chapter count", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchVideoCount");
         ps.setString(1, courseId);
         ps.setInt(2, ContentDetails.CONTENT_VIDEO);
         ps.setString(3, "1");
         rs = ps.executeQuery();
         if (rs.next())
         {
            introData.setVideoCount(rs.getInt(1));
         }
         else
         {
            introData.setVideoCount(0);
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching video count from db for course id " + courseId, e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching course details");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching video count from db for course id " + courseId, e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching course details");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing result set while fetching video count", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchTestCount");
         ps.setString(1, courseId);
         rs = ps.executeQuery();
         if (rs.next())
         {
            introData.setTestCount(rs.getInt(1));
         }
         else
         {
            introData.setTestCount(0);
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching test count from db for course id " + courseId, e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching course details");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching test count from db for course id " + courseId, e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching course details");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing result set while fetching video count", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchQuestionCount");
         ps.setString(1, courseId);
         rs = ps.executeQuery();
         if (rs.next())
         {
            introData.setQuestionCount(rs.getInt(1));
         }
         else
         {
            introData.setQuestionCount(0);
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching question count from db for course id " + courseId, e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching course details");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching question count from db for course id " + courseId, e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching course details");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing result set while fetching question count", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      SymLinkManager symLinkManager = (SymLinkManager) flowContext.getFromContext(EzedKeyConstants.SYMLINK_MANAGER);
      try
      {
         introData.fetchIntroVidDetails(dbConnName, logger);
         String newLink = null;
         int index = 0;
         if (introData.getIntroVidLink() != null && introData.getIntroVidType() == ContentDetails.CONTENT_VIDEO)
         {
            newLink = symLinkManager.getSymLinkNoSession(introData.getIntroVidId(), introData.getIntroVidLink(), logger, newLinkLocationVideos);
            index = newLink.indexOf(vidStartPoint);
            newLink = newLink.substring(index + 1);
            String fileType = newLink.substring(newLink.lastIndexOf('.') + 1);
            if (fileType.compareToIgnoreCase("mov") == 0)
            {
               fileType = "mp4";
            }
            newLink = fileType + ":" + newLink;
            introData.setIntroVidLink(newLink);
         }
         if (introData.getLongDescType() == QuestionContent.CONTENT_TYPE)
         {
            introData.fetchLongDescDetails(dbConnName, logger);
            if (introData.getLongDescLink() != null)
            {
               newLink = symLinkManager.getSymLinkNoSession(introData.getLongDescContentId(), introData.getLongDescLink(), logger, newLinkLocationNotes);
               index = newLink.indexOf(notesStartPoint);
               newLink = newLink.substring(index + 1);
               introData.setLongDescLink(newLink);
            }
         }
         response.setResponseObject(introData);
         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
         return RuleDecisionKey.ON_SUCCESS;
      }
      catch (SQLException e)
      {
         logger.error("Error while course details from db for course id " + courseId, e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching course details");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while course details from db for course id " + courseId, e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching course details");
         return RuleDecisionKey.ON_FAILURE;
      }
   }
}
