package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.ActionData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.MasterActionRecords;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.UserProfile;

public class PostLikeHandler extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
   private String actionId;

   public PostLikeHandler(String ruleName, String dbConnName, String actionId)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
      this.actionId = actionId;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      String userId = (String) flowContext.getFromContext(EzedKeyConstants.USER_ID);
      UserProfile userProfile = (UserProfile) flowContext.getFromContext(EzedKeyConstants.USER_DETAILS);

      String postId = messageData.getParameter("postid");
      if (postId == null || postId.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Post id in request is invalid " + postId);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid post id");
         return RuleDecisionKey.ON_FAILURE;
      }
      postId = postId.trim();

      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertPostLike");
         ps.setString(1, userId);
         ps.setString(2, sdf.format(new Date()));
         ps.setString(3, postId);
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in db while inserting forum post like for post id " + postId);
         }
         if (rows > 0)
         {
            response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
         }
         else
         {
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Like not recorded");
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while inserting post like in db for postid " + postId, e);
         if (e.getMessage().toUpperCase().contains("UNIQUE") || e.getMessage().toUpperCase().contains("PRIMARY"))
         {
            response.setParameters(EzedTransactionCodes.POST_EVENT_ALREADY_RECORDED, "Post like already reported");
            return RuleDecisionKey.ON_FAILURE;
         }
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Like not recorded");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while inserting post like in db for postid " + postId, e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Like not recorded");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      ResultSet rs = null;
      int likeCount = 0;
      int pointsGiven = 0;
      String threadName = null;
      String courseId = null;
      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchPostDetails");
         ps.setString(1, postId);
         rs = ps.executeQuery();
         if (rs.next())
         {
            likeCount = rs.getInt("likecount");
            pointsGiven = rs.getInt("pointsgiven");
            threadName = rs.getString("threadtitle");
            courseId = rs.getString("courseid");
         }
      }
      catch (Exception e)
      {
         logger.error("Error while fetching like count and whether points given", e);
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing resultset while fetching like count and points given flag", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "IncrementPostLikeCount");
         ps.setString(1, postId);
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows updated in db while incrementing like count for postid " + postId);
         }
      }
      catch (Exception e)
      {
         logger.error("Error while incrementing like count in db for postid " + postId, e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Like count not recorded");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      if (likeCount >= 19 && pointsGiven != 0)
      {
         ActionData actionData = MasterActionRecords.fetchRecord(actionId);
         //points valid for giving
         StringBuffer sb = new StringBuffer();
         sb.append(userProfile.getName());
         sb.append("'s post in thread ");
         sb.append(threadName);
         sb.append(" was liked by 20 or more users.");
         String fbLikeStream = sb.toString();

         try
         {
            conn = DbConnectionManager.getConnectionByName(dbConnName);
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdateCoursePoints");
            ps.setInt(1, actionData.getPoints());
            ps.setString(2, userProfile.getUserid());
            ps.setString(3, courseId);
            int rows = ps.executeUpdate();
            if (logger.isDebugEnabled())
            {
               logger.debug(rows + " rows updated in db while incrementing points for userid " + userProfile.getUserid() + " and course " + courseId);
            }
         }
         catch (SQLException e)
         {
            logger.error("Error while incrementing points for userid " + userProfile.getUserid() + " and course " + courseId, e);
         }
         catch (Exception e)
         {
            logger.error("Error while incrementing points for userid " + userProfile.getUserid() + " and course " + courseId, e);
         }
         finally
         {
            DbConnectionManager.releaseConnection(dbConnName, conn);
         }


         try
         {
            conn = DbConnectionManager.getConnectionByName(dbConnName);
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdateUserPoints");
            ps.setInt(1, actionData.getPoints());
            ps.setString(2, userProfile.getUserid());
            int rows = ps.executeUpdate();
            if (logger.isDebugEnabled())
            {
               logger.debug(rows + " rows updated in db while incrementing points for userid " + userProfile.getUserid());
            }
         }
         catch (SQLException e)
         {
            logger.error("Error while incrementing points for userid " + userProfile.getUserid(), e);
         }
         catch (Exception e)
         {
            logger.error("Error while incrementing points for userid " + userProfile.getUserid(), e);
         }
         finally
         {
            DbConnectionManager.releaseConnection(dbConnName, conn);
         }

         try
         {
            conn = DbConnectionManager.getConnectionByName(dbConnName);
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertUserStreamAll");
            ps.setString(1, fbLikeStream);
            ps.setString(2, userProfile.getUserid());
            ps.setString(3, sdf.format(new Date()));
            ps.setString(4, courseId);
            ps.setString(5, null);
            ps.setString(6, null);
            ps.setString(7, actionId);
            int rows = ps.executeUpdate();
            if (logger.isDebugEnabled())
            {
               logger.debug(rows + " rows inserted in db while inserting stream for user " + userProfile.getUserid());
            }
         }
         catch (Exception e)
         {
            logger.error("Error inserting stream entry for " + userProfile.getUserid(), e);
            logger.error("INSERT FAILED " + fbLikeStream);
         }
         finally
         {
            DbConnectionManager.releaseConnection(dbConnName, conn);
         }
      }

      return RuleDecisionKey.ON_SUCCESS;
   }
}
