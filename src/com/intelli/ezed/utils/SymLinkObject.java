package com.intelli.ezed.utils;


public class SymLinkObject
{
   private String contentId;
   private String contentLink;
   private String fileName;
   private String translatedContentLink;
   private String sessionId;

   public String getFileName()
   {
      return fileName;
   }
   public void setFileName(String fileName)
   {
      this.fileName = fileName;
   }
   public String getSessionId()
   {
      return sessionId;
   }
   public void setSessionId(String sessionId)
   {
      this.sessionId = sessionId;
   }
   public String getContentId()
   {
      return contentId;
   }
   public void setContentId(String contentId)
   {
      this.contentId = contentId;
   }
   public String getContentLink()
   {
      return contentLink;
   }
   public void setContentLink(String contentLink) throws Exception
   {
      //      File file = new File(contentLink);
      //      this.setContentLink(file.getParentFile().getAbsolutePath());
      //      this.setFileName(file.getName());
      this.contentLink = contentLink;
   }
   public String getTranslatedContentLink()
   {
      return translatedContentLink;
   }
   public void setTranslatedContentLink(String translatedContentLink)
   {
      this.translatedContentLink = translatedContentLink;
   }
   //   public String getTranslatedFile()
   //   {
   //      return getTranslatedContentLink() + "/" + getFileName();
   //   }
}
