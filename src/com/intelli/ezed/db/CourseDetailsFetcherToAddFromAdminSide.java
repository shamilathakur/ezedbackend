package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.ManageUserCourseFromBackendBean;
import com.intelli.ezed.data.AddScratchCardToUserData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.ScratchCardErrorLogData;
import com.intelli.ezed.utils.EndDateCalculator;
import com.intelli.ezed.utils.ScratchCardHelper;

public class CourseDetailsFetcherToAddFromAdminSide extends SingletonRule {

	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
	private String dbConnName;
	
	private final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
	private final SimpleDateFormat sdf1 = new SimpleDateFormat("MM/dd/yyyy");

	public CourseDetailsFetcherToAddFromAdminSide(String ruleName,
			String dbConnName) {
		super(ruleName);
		this.dbConnName = dbConnName;
	}

	@Override
	public RuleDecisionKey executeRule(FlowContext flowContext) {

		int dayBufferBeforeEnd = (Integer) flowContext
				.getFromContext(EzedKeyConstants.BUFFER_END_DATE);
		Response response = (Response) flowContext
				.getFromContext(EzedKeyConstants.EZED_RESPONSE);
		ManageUserCourseFromBackendBean data = (ManageUserCourseFromBackendBean) flowContext
				.getFromContext(EzedKeyConstants.ADD_COURSE_TO_USER_FROM_ADMIN);
		System.out.println("In Class : " + this.getClass().getName());
		System.out.print("USERID : " + data.getUserId() + "  Sessionid : "
				+ data.getSessionId());

		Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
		PreparedStatement ps1 = null;
		ResultSet rs = null;
		Calendar calendar = Calendar.getInstance();
		Date startDate = new Date();
		data.setStartDate(sdf.format(startDate));

		try {
			ps1 = DbConnectionManager.getPreparedStatement(dbConnName, conn,
					"FetchCourseDetailsByCourseId");
			ps1.setString(1, data.getCourseId());
			rs = ps1.executeQuery();
			if (rs.next()) {
				String commaSeparatedEndMonths = rs.getString("enddate");
				Date courseEndDate = EndDateCalculator.findEndDate(
						commaSeparatedEndMonths, startDate);
				calendar.setTime(courseEndDate);
				calendar.add(Calendar.DATE, -dayBufferBeforeEnd);
				Date endDate = calendar.getTime();
				data.setEndDate(sdf.format(endDate));
			}
		} catch (SQLException e) {
			System.out.println("SQL Error In Class : "
					+ this.getClass().getName());
			if (logger.isDebugEnabled()) {
				logger.error(
						"SQL Error occured while fetching course details for courseId : "
								+ data.getCourseId(), e);
			}
			response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED,
					"SQL error while fetching course details.");
			return RuleDecisionKey.ON_FAILURE;
		} catch (Exception e) {
			if (logger.isDebugEnabled()) {
				logger.error(
						"Internal Error occured while fetching course details for courseId : "
								+ data.getCourseId(), e);
			}
			response.setParameters(EzedTransactionCodes.INTERNAL_ERROR,
					"Internal error while fetching course details.");
			return RuleDecisionKey.ON_FAILURE;
		} finally {
			DbConnectionManager.releaseConnection(dbConnName, conn);
		}
		flowContext.putIntoContext(
				EzedKeyConstants.ADD_COURSE_TO_USER_FROM_ADMIN, data);
		return RuleDecisionKey.ON_SUCCESS;

	}

}
