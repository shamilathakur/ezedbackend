package com.intelli.ezed.data;

import java.text.SimpleDateFormat;

public class UpcomingGoalData extends ResponseObject
{
   private static SimpleDateFormat sdfDb = new SimpleDateFormat("yyyyMMddHHmmss");
   private static SimpleDateFormat sdfOut = new SimpleDateFormat("dd-MM-yyyy");
   private String courseId;
   private String chapterId;
   private String contentId;
   private String courseName;
   private String chapterName;
   private String contentName;
   private String endDate;

   @Override
   public void prepareResponse() throws Exception
   {
      this.put("courseid", getCourseId());
      this.put("coursename", getCourseName());
      this.put("chapterid", getChapterId());
      this.put("chaptername", getChapterName());
      this.put("contentname", getContentName());
      this.put("contentid", getContentId());
      this.put("date", sdfOut.format(sdfDb.parse(getEndDate())));
   }

   public String getCourseName()
   {
      return courseName;
   }

   public void setCourseName(String courseName)
   {
      this.courseName = courseName;
   }

   public String getChapterName()
   {
      return chapterName;
   }

   public void setChapterName(String chapterName)
   {
      this.chapterName = chapterName;
   }

   public String getContentName()
   {
      return contentName;
   }

   public void setContentName(String contentName)
   {
      this.contentName = contentName;
   }

   public String getEndDate()
   {
      return endDate;
   }

   public void setEndDate(String endDate)
   {
      this.endDate = endDate;
   }

   public String getCourseId()
   {
      return courseId;
   }

   public void setCourseId(String courseId)
   {
      this.courseId = courseId;
   }

   public String getChapterId()
   {
      return chapterId;
   }

   public void setChapterId(String chapterId)
   {
      this.chapterId = chapterId;
   }

   public String getContentId()
   {
      return contentId;
   }

   public void setContentId(String contentId)
   {
      this.contentId = contentId;
   }

}
