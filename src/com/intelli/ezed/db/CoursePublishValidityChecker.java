package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class CoursePublishValidityChecker extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public CoursePublishValidityChecker(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      String courseId = messageData.getParameter("courseid");
      if (courseId == null || courseId.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Course id not received in request " + courseId);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Course id is invalid");
         return RuleDecisionKey.ON_FAILURE;
      }

      Connection conn = null;
      PreparedStatement ps = null;
      ResultSet rs = null;

      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "CheckCoursePublishValidity");
         ps.setString(1, courseId);
         ps.setString(2, courseId);
         rs = ps.executeQuery();
         int count = 0;
         while (rs.next())
         {
            if (rs.getInt("count") == 0)
            {
               if (logger.isDebugEnabled())
               {
                  logger.debug("Count for row num " + (count + 1) + " 0. Course cannot be published");
               }
               response.setParameters(EzedTransactionCodes.COURSE_CANNOT_BE_PUBLISHED, "Course cannot be published. Please put a default chapter in the course with at least 1 content and add a course chapter to the course with at least 1 content");
               return RuleDecisionKey.ON_FAILURE;
            }
            count++;
         }
         if (count < 2)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("No of rows in count should be 2. It was " + (count + 1));
            }
            response.setParameters(EzedTransactionCodes.COURSE_CANNOT_BE_PUBLISHED, "Course cannot be published. Please put a default chapter in the course with at least 1 content and add a course chapter to the course with at least 1 content");
            return RuleDecisionKey.ON_FAILURE;
         }
      }
      catch (Exception e)
      {
         logger.error("Error while checking for course publish validity", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while checking for course publish validity");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
      response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
      return RuleDecisionKey.ON_SUCCESS;
   }
}
