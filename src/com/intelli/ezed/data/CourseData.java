package com.intelli.ezed.data;

import java.text.SimpleDateFormat;
import java.util.Date;

public class CourseData extends ResponseObject
{
   private static SimpleDateFormat sdf = new SimpleDateFormat("dd~MM~yyyy");

   private String courseid;
   private String coursename;
   private String shortdesc;
   private String introvideo;
   private String defchapter;
   private String defchaptername;
   private String lmrchapter;
   private String contentonlyamt;
   private String testonlyamt;
   private String fullcourseamt;
   private String longdesctype;
   private String longdescstring;
   private String longdesccontent;
   private String questionpaperid;
   private String solvedpaperid;
   private String lmr;
   private Date[] enddate;
   private String courseimage;
   private String lmrname;
   private String questionpapername;
   private String solvedpapername;
  // private String 

   public String getCourseid()
   {
      return courseid;
   }

   public void setCourseid(String courseid)
   {
      this.courseid = courseid;
   }

   public String getCoursename()
   {
      return coursename;
   }

   public void setCoursename(String coursename)
   {
      this.coursename = coursename;
   }

   public String getShortdesc()
   {
      return shortdesc;
   }

   public void setShortdesc(String shortdesc)
   {
      this.shortdesc = shortdesc;
   }

   public String getIntrovideo()
   {
      return introvideo;
   }

   public void setIntrovideo(String introvideo)
   {
      this.introvideo = introvideo;
   }

   public String getDefchapter()
   {
      return defchapter;
   }

   public void setDefchapter(String defchapter)
   {
      this.defchapter = defchapter;
   }

   public String getLmrchapter()
   {
      return lmrchapter;
   }

   public void setLmrchapter(String lmrchapter)
   {
      this.lmrchapter = lmrchapter;
   }

   public String getContentonlyamt()
   {
      return contentonlyamt;
   }

   public void setContentonlyamt(String contentonlyamt)
   {
      this.contentonlyamt = contentonlyamt;
   }

   public String getTestonlyamt()
   {
      return testonlyamt;
   }

   public void setTestonlyamt(String testonlyamt)
   {
      this.testonlyamt = testonlyamt;
   }

   public String getFullcourseamt()
   {
      return fullcourseamt;
   }

   public void setFullcourseamt(String fullcourseamt)
   {
      this.fullcourseamt = fullcourseamt;
   }

   public String getLongdesctype()
   {
      return longdesctype;
   }

   public void setLongdesctype(String longdesctype)
   {
      this.longdesctype = longdesctype;
   }

   public String getLongdescstring()
   {
      return longdescstring;
   }

   public void setLongdescstring(String longdescstring)
   {
      this.longdescstring = longdescstring;
   }

   public String getLongdesccontent()
   {
      return longdesccontent;
   }

   public void setLongdesccontent(String longdesccontent)
   {
      this.longdesccontent = longdesccontent;
   }

   public String getQuestionpaperid()
   {
      return questionpaperid;
   }

   public void setQuestionpaperid(String questionpaperid)
   {
      this.questionpaperid = questionpaperid;
   }

   public String getSolvedpaperid()
   {
      return solvedpaperid;
   }

   public void setSolvedpaperid(String solvedpaperid)
   {
      this.solvedpaperid = solvedpaperid;
   }

   public String getLmr()
   {
      return lmr;
   }

   public void setLmr(String lmr)
   {
      this.lmr = lmr;
   }

   public Date[] getEnddate()
   {
      return enddate;
   }

   public void setEnddate(Date[] enddate)
   {
      this.enddate = enddate;
   }

   @Override
   public void prepareResponse() throws Exception
   {
      this.put("courseid", getCourseid());
      this.put("coursename", getCoursename());
      this.put("shortdesc", getShortdesc());
      this.put("introvid", getIntrovideo());
      this.put("longdesctype", getLongdesctype());
      this.put("longdescstring", getLongdescstring());
      this.put("longdesccontent", getLongdesccontent());
      this.put("contentonlyamt", getContentonlyamt());
      this.put("testonlyamount", getTestonlyamt());
      this.put("fullcourseamt", getFullcourseamt());
      this.put("defchapter", getDefchapter());
      this.put("lmrchapter", getLmrchapter());
      this.put("universityqpaperchapter", getQuestionpaperid());
      this.put("universitysolutionchapter", getSolvedpaperid());
      this.put("defchaptername", getDefchaptername());
      this.put("courseimage", getCourseImage());
      this.put("questionpapername", getQuestionpapername());
      this.put("lmrname", getLmrname());
      this.put("solvedpapername", getSolvedpapername());

      String[] endDatesString=null;
      if(getEnddate() !=null){
    	  endDatesString= new String[getEnddate().length];
     
      for (int i = 0; i < getEnddate().length; i++)
      {
         endDatesString[i] = sdf.format(getEnddate()[i]);
      }
      this.put("enddate", endDatesString);
      }
      this.put("courseimage", getCourseImage());
   }

   public String getCourseImage()
   {
      return courseimage;
   }

   public void setCourseImage(String courseImage)
   {
      this.courseimage = courseImage;
   }

/**
 * @return the defchaptername
 */
public String getDefchaptername() {
	return defchaptername;
}

/**
 * @param defchaptername the defchaptername to set
 */
public void setDefchaptername(String defchaptername) {
	this.defchaptername = defchaptername;
}

/**
 * @return the questionpapername
 */
public String getQuestionpapername() {
	return questionpapername;
}

/**
 * @param questionpapername the questionpapername to set
 */
public void setQuestionpapername(String questionpapername) {
	this.questionpapername = questionpapername;
}

/**
 * @return the lmrname
 */
public String getLmrname() {
	return lmrname;
}

/**
 * @param lmrname the lmrname to set
 */
public void setLmrname(String lmrname) {
	this.lmrname = lmrname;
}

/**
 * @return the solvedpapername
 */
public String getSolvedpapername() {
	return solvedpapername;
}

/**
 * @param solvedpapername the solvedpapername to set
 */
public void setSolvedpapername(String solvedpapername) {
	this.solvedpapername = solvedpapername;
}
}
