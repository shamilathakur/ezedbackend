package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.ActionData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.MasterActionRecords;
import com.intelli.ezed.data.UserProfile;

public class InviteFriendPointsGiver extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String actionId;
   private static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

   public InviteFriendPointsGiver(String ruleName, String dbConnName, String actionId)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
      this.actionId = actionId;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      UserProfile userProfile = (UserProfile) flowContext.getFromContext(EzedKeyConstants.USER_DETAILS);
      String friendId = (String) flowContext.getFromContext(EzedKeyConstants.INVITED_FRIEND);

      ActionData actionData = MasterActionRecords.fetchRecord(actionId);

      StringBuffer sb = new StringBuffer();
      sb.append(userProfile.getName());
      sb.append(" invited ");
      sb.append(friendId);
      sb.append(" to join EzEd");
      String inviteFriendStream = sb.toString();

      Connection conn = null;
      PreparedStatement ps = null;

      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdateUserPoints");
         ps.setInt(1, actionData.getPoints());
         ps.setString(2, userProfile.getUserid());
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows updated in db while incrementing points for userid " + userProfile.getUserid());
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while incrementing points for userid " + userProfile.getUserid(), e);
      }
      catch (Exception e)
      {
         logger.error("Error while incrementing points for userid " + userProfile.getUserid(), e);
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertUserStreamAll");
         ps.setString(1, inviteFriendStream);
         ps.setString(2, userProfile.getUserid());
         ps.setString(3, sdf.format(new Date()));
         ps.setString(4, null);
         ps.setString(5, null);
         ps.setString(6, null);
         ps.setString(7, actionId);
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in db while inserting stream for user " + userProfile.getUserid());
         }
      }
      catch (Exception e)
      {
         logger.error("Error inserting stream entry for " + userProfile.getUserid(), e);
         logger.error("INSERT FAILED " + inviteFriendStream);
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      return RuleDecisionKey.ON_SUCCESS;
   }

}
