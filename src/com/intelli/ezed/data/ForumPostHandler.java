package com.intelli.ezed.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.sabre.db.DbConnectionManager;
import org.slf4j.Logger;

public class ForumPostHandler extends ResponseObject
{
   private ArrayList<ForumPostData> origPosts = new ArrayList<ForumPostData>();
   private ArrayList<ForumPostData> finalPosts = new ArrayList<ForumPostData>();
   Map<String, ForumPostData> treeUnitMap = new HashMap<String, ForumPostData>();
   ArrayList<ForumPostData> parentNodes = new ArrayList<ForumPostData>();

   public void addForumPostData(ForumPostData forumData)
   {
      origPosts.add(forumData);
      treeUnitMap.put(forumData.getPostId().intern(), forumData);
   }

   public void handleUserPostLike(String postId)
   {
      ForumPostData data = treeUnitMap.get(postId);
      if (data != null)
      {
         data.setLiked(true);
      }
   }

   public void handleUserPostAbuse(String postId)
   {
      ForumPostData data = treeUnitMap.get(postId);
      if (data != null)
      {
         data.setAbused(true);
      }
   }

   public void handleProfileImages(String dbConnName, Logger logger, String defaultProfilePic)
   {
      Connection conn = null;
      PreparedStatement ps = null;
      ResultSet rs = null;
      String profImage = null;
      for (ForumPostData treeUnit : origPosts)
      {
         try
         {
            if (treeUnit.isAdminFlag())
            {
               treeUnit.setProfileImage(defaultProfilePic);
               continue;
            }
            conn = DbConnectionManager.getConnectionByName(dbConnName);
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchUserProfilePic");
            ps.setString(1, treeUnit.getUserId());
            rs = ps.executeQuery();
            if (rs.next())
            {
               profImage = rs.getString(1);
               if (profImage == null || profImage.trim().compareTo("") == 0)
               {
                  treeUnit.setProfileImage(defaultProfilePic);
               }
               else
               {
                  treeUnit.setProfileImage(profImage);
               }
            }
            else
            {
               treeUnit.setProfileImage(defaultProfilePic);
            }
         }
         catch (Exception e)
         {
            if (logger.isTraceEnabled())
            {
               logger.trace("Error while fetching profile image for user " + treeUnit.getUserId(), e);
            }
            treeUnit.setProfileImage(defaultProfilePic);
         }
         finally
         {
            if (rs != null)
            {
               try
               {
                  rs.close();
               }
               catch (Exception e)
               {
                  if (logger.isTraceEnabled())
                  {
                     logger.trace("Error while closing resultset while fetching profile pic of user");
                  }
               }
            }
            DbConnectionManager.releaseConnection(dbConnName, conn);
         }
      }
   }

   public void createTree()
   {
      for (ForumPostData treeUnit : origPosts)
      {
         addTreeToNode(treeUnit);
      }
   }

   private void addTreeToNode(ForumPostData treeUnit)
   {
      if (treeUnit.getParentPostId() != null)
      {
         ForumPostData parentUnit = treeUnitMap.get(treeUnit.getParentPostId().intern());
         if (parentUnit != null)
         {
            parentUnit.getChildNodes().add(treeUnit);
         }
      }
      else
      {
         parentNodes.add(treeUnit);
      }
   }
   public void calculateIndents()
   {
      for (ForumPostData parentUnits : parentNodes)
      {
         parentUnits.calculateIndents(1);
      }
   }

   public void print()
   {
      for (ForumPostData parentUnits : parentNodes)
      {
         parentUnits.print();
      }
   }

   public void finalizeTree()
   {
      for (ForumPostData parentUnits : parentNodes)
      {
         parentUnits.finalizeTree(finalPosts);
      }
   }

   @Override
   public void prepareResponse() throws Exception
   {
      ForumPostData[] posts = new ForumPostData[finalPosts.size()];
      for (int i = 0; i < finalPosts.size(); i++)
      {
         posts[i] = finalPosts.get(i);
         finalPosts.get(i).prepareResponse();
      }
      this.put("posts", posts);
   }
}
