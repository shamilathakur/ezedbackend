package com.intelli.ezed.rules;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.PaymentData;
import com.intelli.ezed.data.Response;

public class PaymentUpdateRequestParser extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public PaymentUpdateRequestParser(String ruleName)
   {
      super(ruleName);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      String paymentId = messageData.getParameter("paymentid");
      if (paymentId == null || paymentId.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Payment id received in update request is invalid");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid payment id");
         return RuleDecisionKey.ON_FAILURE;
      }

      String appId = null;

      if (paymentId.length() >= 12)
      {
         appId = paymentId.substring(0, 2);
         paymentId = paymentId.substring(2);
      }
      else
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Payment id received in update request is not of sufficient length");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid payment id");
         return RuleDecisionKey.ON_FAILURE;
      }

      String paymentStatus = messageData.getParameter("status");
      if (paymentStatus == null || paymentStatus.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Invalid payment status");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Payment status is invalid");
         return RuleDecisionKey.ON_FAILURE;
      }

      int paymentStatusInt = 2;
      try
      {
         paymentStatusInt = Integer.parseInt(paymentStatus.trim());
      }
      catch (Exception e)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Invalid payment status");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Payment status is invalid");
         return RuleDecisionKey.ON_FAILURE;
      }

      PaymentData payData = new PaymentData();
      payData.setAppId(appId);
      payData.setPaymentId(paymentId);
      payData.setPaymentStatus(paymentStatusInt);

      String merchantRef = messageData.getParameter("merchantref");
      if (merchantRef == null)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Merchant reference is null. Setting default value");
         }
         merchantRef = "";
      }
      payData.setMerchantRef(merchantRef);

      flowContext.putIntoContext(EzedKeyConstants.PAYMENT_DATA, payData);
      return RuleDecisionKey.ON_SUCCESS;
   }
}
