package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.FrontendFetchData;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.utils.EzedHelper;

/**
 * @author Kunal Description : This class is used for fetching all the used
 *         coupons details. It queries the payData table for the condition
 *         couponcode not null.. Service : 3017
 */
public class UsedDiscountCouponFetcher extends SingletonRule {

	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
	private String db_name;
	SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyyMMddHHmmss");
	SimpleDateFormat formatTo = new SimpleDateFormat("yyyy-MM-dd");

	public UsedDiscountCouponFetcher(String ruleName, String db_name) {
		super(ruleName);
		this.db_name = db_name;
	}

	@Override
	public RuleDecisionKey executeRule(FlowContext flowContext) {

		Response response = (Response) flowContext
				.getFromContext(EzedKeyConstants.EZED_RESPONSE);

		MessageData messageData = (MessageData) flowContext
				.getFromContext(MessageData.class.getName());

		String Query = "select userid,cart,paymentid,couponcode,discountamount, to_char(to_date(substring(intime,0,9), 'yyyymmdd'),'dd-mm-yy') as enrolldate from paydata where couponcode notnull";
		String finalQuery = Query;
		System.out.println("Query : " + Query);

		String start = messageData.getParameter("startdate");
		String end = messageData.getParameter("enddate");
		if ((start != null && start.compareTo("null") != 0 && start
				.compareTo("") != 0)
				&& (end.compareTo("null") == 0 || end == null || end.trim()
						.compareTo("") != 0)) {
			end = sourceFormat.format(new Date());
		}

		String status = messageData.getParameter("paymentstatus");
		if (status != null & status.trim().compareTo("") != 0
				&& status.compareTo("null") != 0) {
			int paymentstatus = Integer.parseInt(status);
			if (paymentstatus == 1) {
				finalQuery = finalQuery + " and paymentstatus='1'";
			} else if (paymentstatus == 0) {
				finalQuery = finalQuery
						+ " and (paymentstatus!='1' or paymentstatus is null)";
			}
		}

		Connection conn = DbConnectionManager.getConnectionByName(db_name);
		PreparedStatement ps = null;
		ResultSet rs = null;
		String[] discData = null;
		FrontendFetchData couponData = new FrontendFetchData();
		response.setResponseObject(couponData);
		response.setStatusCodeSend(false);
		couponData.setAttribName("aaData");
		ArrayList<String[]> couponList = new ArrayList<String[]>();

		if ((start != null && start.compareTo("null") != 0 && start
				.compareTo("") != 0)) {
			finalQuery = finalQuery + " and intime between '" + start
					+ "' and '" + end + "'";
		}

		String courseDetails[] = new String[3];
		try {

			conn = DbConnectionManager.getConnectionByName(db_name);
			ps = conn.prepareStatement(finalQuery);
			System.out.println("Final Query : " + ps);
			rs = ps.executeQuery();
			int count = 0;
			while (rs.next()) {
				String cartdata[] = rs.getString("cart").split(";");

				count++;
				discData = new String[10];
				discData[0] = Integer.toString(count);
				discData[1] = rs.getString("userid");
				discData[2] = rs.getString("couponcode");
				String courseid = "", coursetype = "", startdate = "", enddate = "";
				for (int i = 0; i < cartdata.length; i++) {

					courseDetails = cartdata[i].split(",");
					courseid = courseid
							+ EzedHelper.fetchCourseName(db_name,
									courseDetails[0], logger);
					int type = Integer.parseInt(courseDetails[1]);
					if (type == 1) {
						coursetype = coursetype + "Content Only";
					} else if (type == 2) {
						coursetype = coursetype + "Test Only";
					} else if (type == 3) {
						coursetype = coursetype + "Full Course";
					}
					courseDetails[2] = courseDetails[2] + "000000";
					startdate = startdate
							+ formatTo.format(sourceFormat
									.parse(courseDetails[2]));
					courseDetails[3] = courseDetails[3] + "000000";
					enddate = enddate
							+ formatTo.format(sourceFormat
									.parse(courseDetails[3]));
					if (i != cartdata.length - 1) {
						courseid = courseid + " <br><br> ";
						coursetype = coursetype + " <br><br><br><br><br> ";
						startdate = startdate + " <br><br><br><br><br> ";
						enddate = enddate + " <br><br><br><br><br> ";
					}
				}
				discData[3] = courseid;
				discData[4] = coursetype;
				discData[5] = startdate;
				discData[6] = enddate;
				discData[7] = rs.getInt("discountamount") + "";
				discData[8] = rs.getString("enrolldate");
				discData[9] = rs.getString("paymentid");
				couponList.add(discData);

			}
			couponData.setAttribList(couponList);
			response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
		} catch (SQLException e) {
			logger.error(
					"Error while fetching discount details for all coupons", e);
			response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED,
					"Error while fetching discount details");
			return RuleDecisionKey.ON_FAILURE;
		} catch (Exception e) {
			logger.error(
					"Error while fetching discount details for all coupons", e);
			response.setParameters(EzedTransactionCodes.INTERNAL_ERROR,
					"Error while fetching discount details");
			return RuleDecisionKey.ON_FAILURE;
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					logger.error(
							"Error while closing result set while fetching Coupon list",
							e);
				}
			}
			DbConnectionManager.releaseConnection(db_name, conn);
		}
		return RuleDecisionKey.ON_SUCCESS;
	}
}
