package com.intelli.ezed.db;

import java.sql.SQLException;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.GroupTestData;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.utils.EzedHelper;

public class GroupTestAddHandler extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;

   public GroupTestAddHandler(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      String groupid = messageData.getParameter("groupid");
      if (groupid == null || groupid.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("group id is invalid" + groupid);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid group id");
         return RuleDecisionKey.ON_FAILURE;
      }

      String noOfQuestionsString = messageData.getParameter("noofquestions");
      if (noOfQuestionsString == null || noOfQuestionsString.trim().compareTo("") == 0 || !EzedHelper.isNumeric(noOfQuestionsString.trim()))
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("no of questions is invalid " + noOfQuestionsString);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid no of questions");
         return RuleDecisionKey.ON_FAILURE;
      }
      int noOfQuestions = Integer.parseInt(noOfQuestionsString);

      if (noOfQuestions == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("no of questions cannot be 0");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid no of questions");
      }

      String hoursString = messageData.getParameter("hours");
      if (hoursString == null || hoursString.trim().compareTo("") == 0 || !EzedHelper.isNumeric(hoursString.trim()))
      {
         hoursString = "0";
      }
      int hours = Integer.parseInt(hoursString);

      String minustesString = messageData.getParameter("minutes");
      if (minustesString == null || minustesString.trim().compareTo("") == 0 || !EzedHelper.isNumeric(minustesString.trim()))
      {
         minustesString = "0";
      }
      int minutes = Integer.parseInt(minustesString);

      String secondsString = messageData.getParameter("seconds");
      if (secondsString == null || secondsString.trim().compareTo("") == 0 || !EzedHelper.isNumeric(secondsString.trim()))
      {
         secondsString = "0";
      }
      int seconds = Integer.parseInt(secondsString);

      if (hours == 0 && minutes == 0 && seconds == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Invalid test time");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid test time");
         return RuleDecisionKey.ON_FAILURE;
      }

      String questionBankId = messageData.getParameter("questionbankid");
      if (questionBankId == null || questionBankId.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Invalid question bank id " + questionBankId);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid question bank id");
         return RuleDecisionKey.ON_FAILURE;
      }

      String testName = messageData.getParameter("testname");
      if (testName == null || testName.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Test name is invalid " + testName);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid test name");
         return RuleDecisionKey.ON_FAILURE;
      }


      GroupTestData data = new GroupTestData();
      data.setGroupId(groupid);
      data.setQuestionBankId(questionBankId);
      data.setTestName(testName);
      data.setTestTime(hours, minutes, seconds);
      data.setNoOfQuestions(noOfQuestions);
      try
      {
         data.setNewTestId();
         data.persistTest(dbConnName, logger);
         data.persistTestToUsers(dbConnName, logger);
         response.setParameters(EzedTransactionCodes.SUCCESS, "Test added to group users successfully");
         return RuleDecisionKey.ON_SUCCESS;
      }
      catch (SQLException e)
      {
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while adding test to group");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while adding test to group");
         return RuleDecisionKey.ON_FAILURE;
      }
   }
}
