package com.intelli.ezed.rules;

import java.util.HashMap;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.sabre.io.transport.http.data.HttpTransactionData;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class EzedResponseConstructor extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public EzedResponseConstructor(String ruleName)
   {
      super(ruleName);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      HttpTransactionData transData = new HttpTransactionData();
      transData.setRequestType(HttpTransactionData.POST);
      transData.setHeaderMap(new HashMap<String, List<String>>());
      transData.addHeader("content-type", "application/json");
      String responseMessage = "";
      if (response != null)
      {
         try
         {
            responseMessage = response.getResponse();
         }
         catch (Exception e)
         {
            logger.error("Error while constructing response. Setting default response code and message", e);
            try
            {
               responseMessage = constructDefaultResponse(EzedTransactionCodes.INTERNAL_ERROR, "Error while constructing response");
            }
            catch (JSONException e1)
            {
               logger.error("Error while constructing default response also", e1);
               responseMessage = "null";
            }
         }
      }
      else
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Response object is null");
         }
         try
         {
            responseMessage = constructDefaultResponse(EzedTransactionCodes.INTERNAL_ERROR, "Internal Error");
         }
         catch (JSONException e)
         {
            logger.error("Error while constructing default response also", e);
            responseMessage = "null";
         }
      }

      transData.setContent(responseMessage.getBytes());
      flowContext.putIntoContext(HttpTransactionData.WRITE_RESP_DATA, transData);

      if (logger.isDebugEnabled())
      {
         logger.debug("Response to be sent back " + transData);
      }
      return RuleDecisionKey.ON_SUCCESS;
   }

   private String constructDefaultResponse(int statusCode, String statusMessage) throws JSONException
   {
      JSONObject jsonObject = new JSONObject();
      jsonObject.put("statuscode", statusCode);
      jsonObject.put("statusmessage", statusMessage);
      return jsonObject.toString(2);
   }

}
