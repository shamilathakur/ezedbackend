package com.intelli.ezed.db;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.UserProfile;
import com.intelli.ezed.utils.AccountStatusHelper;

public class RegistrationPrechecker extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public RegistrationPrechecker(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      String email = messageData.getParameter("email");
      if (email == null || email.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Email id invalid " + email);
         }
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Invalid email id in registration precheck request");
         return RuleDecisionKey.ON_FAILURE;
      }

      UserProfile userProfile = new UserProfile();
      userProfile.setUserid(email);
      try
      {
         userProfile.populateUserProfile(dbConnName, logger);
         if (AccountStatusHelper.isRegistered(userProfile.getChkflag()))
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("User is already registered " + email);
            }
            response.setParameters(EzedTransactionCodes.USERID_ALREADY_PRESENT, "User id already registered");
            return RuleDecisionKey.ON_FAILURE;
         }
         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while checking if user is registered", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while checking if user is registered");
         return RuleDecisionKey.ON_FAILURE;
      }
   }
}
