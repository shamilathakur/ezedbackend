package com.intelli.ezed.checks;

import java.text.SimpleDateFormat;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.ManageUserCourseFromBackendBean;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class AddCourseToUserFromBackendRequestParser extends SingletonRule {
	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
	private String value;

	public AddCourseToUserFromBackendRequestParser(String ruleName) {
		super(ruleName);
	}

	@Override
	public RuleDecisionKey executeRule(FlowContext flowContext) {
		MessageData messageData = (MessageData) flowContext
				.getFromContext(MessageData.class.getName());
		Response response = (Response) flowContext
				.getFromContext(EzedKeyConstants.EZED_RESPONSE);
		ManageUserCourseFromBackendBean data = new ManageUserCourseFromBackendBean();

		value = messageData.getParameter("userid");
		if (value == null || value.trim().compareTo("") == 0
				|| value.trim().compareToIgnoreCase("null") == 0) {
			if (logger.isDebugEnabled()) {
				logger.debug("User id is invalid");
			}
			response.setParameters(
					EzedTransactionCodes.INVALID_MESSAGE_PARAMETER,
					"User id is invalid");
			return RuleDecisionKey.ON_FAILURE;
		}
		flowContext.putIntoContext(EzedKeyConstants.USER_ID , value);
		data.setUserId(value);

		value = messageData.getParameter("courseid");
		if (value == null || value.trim().compareTo("") == 0
				|| value.trim().compareToIgnoreCase("null") == 0) {
			if (logger.isDebugEnabled()) {
				logger.debug("Course Id is invalid");
			}
			response.setParameters(
					EzedTransactionCodes.INVALID_MESSAGE_PARAMETER,
					"Course ID is invalid");
			return RuleDecisionKey.ON_FAILURE;
		}
		data.setCourseId(value);

		value = messageData.getParameter("coursetype");
		if (value == null || value.trim().compareTo("") == 0
				|| value.trim().compareToIgnoreCase("null") == 0) {
			if (logger.isDebugEnabled()) {
				logger.debug("Course type is invalid");
			}
			response.setParameters(
					EzedTransactionCodes.INVALID_MESSAGE_PARAMETER,
					"Course type is invalid");
			return RuleDecisionKey.ON_FAILURE;
		}
		data.setCourseType(value);

		value = messageData.getParameter("paidamount");
		if (value == null || value.trim().compareTo("") == 0
				|| value.trim().compareToIgnoreCase("null") == 0) {
			if (logger.isDebugEnabled()) {
				logger.debug("Paid amount is invalid");
			}
			response.setParameters(
					EzedTransactionCodes.INVALID_MESSAGE_PARAMETER,
					"Paid amount is invalid");
			return RuleDecisionKey.ON_FAILURE;
		}
		data.setPaidAmount(Integer.parseInt(value));

		value = messageData.getParameter("totalcourses");
		if (value == null || value.trim().compareTo("") == 0
				|| value.trim().compareToIgnoreCase("null") == 0) {
			if (logger.isDebugEnabled()) {
				logger.debug("Total courses is invalid");
			}
			response.setParameters(
					EzedTransactionCodes.INVALID_MESSAGE_PARAMETER,
					" is invalid");
			return RuleDecisionKey.ON_FAILURE;
		}
		data.setTotalCourses(Integer.parseInt(value));

		data.setSessionId(messageData.getParameter("sessionid"));
		data.setAddedBy(messageData.getParameter("email"));

		flowContext.putIntoContext(
				EzedKeyConstants.ADD_COURSE_TO_USER_FROM_ADMIN, data);

		return RuleDecisionKey.ON_SUCCESS;
	}
}
