package com.intelli.ezed.checks;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.a.helper.utils.StringSplitter;
import com.intelli.ezed.data.CoursePathData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class CoursePathModifictionRequestChecker extends SingletonRule
{

   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   public CoursePathModifictionRequestChecker(String ruleName)
   {
      super(ruleName);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      //email,sessionid,chapterid,lerningpath (~) separated

      String userid = messageData.getParameter("email");
      String courseId = messageData.getParameter("courseid");
      String coursePath = messageData.getParameter("coursepath");
      //      String showFlags = messageData.getParameter("typeflags");

      if (userid == null || userid.equals("") || userid.equals(" "))
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Email address not provided.");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Please enter correct email id.");
         return RuleDecisionKey.ON_FAILURE;
      }
      if (courseId == null || courseId.equals("") || courseId.equals(" "))
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("course id provided is not correct. ");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Please enter course-id correctly.");
         return RuleDecisionKey.ON_FAILURE;
      }

      if (coursePath == null || coursePath.equals("") || coursePath.equals(" "))
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Course path provided is not correct.");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Please enter course path correctly.");
         return RuleDecisionKey.ON_FAILURE;
      }
      //      if (showFlags == null || showFlags.trim().compareTo("") == 0)
      //      {
      //         if (logger.isDebugEnabled())
      //         {
      //            logger.debug("Chapter type flags provided were incorrect");
      //         }
      //         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid chapter type flags");
      //         return RuleDecisionKey.ON_FAILURE;
      //      }

      String[] chapterIds = null;
      String[] typeFlagsArr = null;
      try
      {
         String[] oneChapter = StringSplitter.splitString(coursePath, "~");
         chapterIds = new String[oneChapter.length];
         typeFlagsArr = new String[oneChapter.length];
         String[] chapterTypeSeparated = null;
         for (int i = 0; i < oneChapter.length; i++)
         {
            chapterTypeSeparated = StringSplitter.splitString(oneChapter[i], ":");
            chapterIds[i] = chapterTypeSeparated[0];
            typeFlagsArr[i] = chapterTypeSeparated[1];
         }
      }
      catch (Exception e)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Error parsing course path", e);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Chapters and their flags improper");
         return RuleDecisionKey.ON_FAILURE;
      }
      //      String[] chapterIds = StringSplitter.splitString(coursePath, "~");
      //      String[] typeFlagsArr = StringSplitter.splitString(showFlags, "~");

      if (chapterIds.length != typeFlagsArr.length)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Length of type flags doesn't match that of content ids in learning path");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Contents and their flags improper");
         return RuleDecisionKey.ON_FAILURE;
      }

      if (logger.isDebugEnabled())
      {
         logger.debug("Change course path request recived form userid : " + userid + " for courseid : " + courseId);
      }

      CoursePathData data = new CoursePathData();
      data.setCourseId(courseId);
      data.setChapterIdList(chapterIds);
      data.setChapterTypeFlagList(typeFlagsArr);
      flowContext.putIntoContext(EzedKeyConstants.COURSE_PATH_DATA, data);
      return RuleDecisionKey.ON_SUCCESS;
   }
}
