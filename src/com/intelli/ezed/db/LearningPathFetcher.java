package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.LearningPathResponse;
import com.intelli.ezed.data.Response;

public class LearningPathFetcher extends SingletonRule
{

   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;
   private String preparedStatementName;

   public LearningPathFetcher(String ruleName, String dbConnName, String preparedStatementName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
      this.preparedStatementName = preparedStatementName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      EzedRequestData requestData = (EzedRequestData) flowContext.getFromContext(EzedRequestData.EZED_REQUEST_DATA);
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      String chapterid = requestData.getChapterId();

      Connection connection = DbConnectionManager.getConnectionByName(dbConnName);
      ResultSet rs = null;
      Vector<String> lp = new Vector<String>();
      try
      {
         //select contentid from learningpath where chapterid = ? order by sequenceno
         PreparedStatement preparedStatement = DbConnectionManager.getPreparedStatement(dbConnName, connection, preparedStatementName);
         preparedStatement.setString(1, chapterid);
         rs = preparedStatement.executeQuery();

         if(rs != null && rs.next())
         {
            lp.add(rs.getString("contentid"));
            while (rs.next())
            {
               lp.add(rs.getString("contentid"));
            }
         }
         else
         {
            if(logger.isDebugEnabled())
            {
               logger.debug("No Lerning path found for the given chapterid : " + chapterid);
            }
            response.setParameters(EzedTransactionCodes.INVALID_CHAPTER_ID, "Please check the chapter.");
            return RuleDecisionKey.ON_FAILURE;
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while persisting user profile in DB", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Failed to persist user profile");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while persist user profile from DB", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Failed to persist user profile");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         try
         {
            rs.close();
         }
         catch (SQLException e)
         {
            logger.error("Error closing resultset in class LerningPathFetcher", e);
         }
         DbConnectionManager.releaseConnection(dbConnName, connection);
      }
      
      if(logger.isDebugEnabled())
      {
         logger.debug("Setting learning Path for chapter : " + chapterid);
         logger.debug("Learning path is :  " + lp);
      } 
      LearningPathResponse learningPathResponse = new LearningPathResponse();
      learningPathResponse.setLearningPath(lp);

      response.setParameters(EzedTransactionCodes.SUCCESS, "Learning path is set.");
      response.setResponseObject(learningPathResponse);
      return RuleDecisionKey.ON_SUCCESS;
   }

}
