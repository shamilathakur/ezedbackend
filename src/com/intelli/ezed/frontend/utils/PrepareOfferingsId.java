package com.intelli.ezed.frontend.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicLong;

public class PrepareOfferingsId
{
   //keep calling getnextid() to get new note ID's. Note ID's will be
   //in the format NOT1, NOT2, NOT3, and so on...
   private AtomicLong atomicLong = null;

   public PrepareOfferingsId()
   {
      long initialValue = getStartUpTransactionId();
      if (initialValue == 0)
      {
         initialValue = (System.currentTimeMillis() / 10000);
      }
      atomicLong = new AtomicLong(initialValue);
   }

   private long getStartUpTransactionId()
   {
      //change how you get your connection here.
      Connection connection = getDbConnection();
      if (connection == null)
      {
         return 0;
      }

      //change how you get your prepared statement here.
      PreparedStatement preparedStatement = getStatement(connection);
      if (preparedStatement == null)
      {
         return 0;
      }
      try
      {
         ResultSet resultSet = preparedStatement.executeQuery();
         if (resultSet.next())
         {
            String str = resultSet.getString(1);
            String numericPart = str.substring(4);
            long value = Long.parseLong(numericPart);
            resultSet.close();
            resultSet = null;

            if (value == 0)
            {
               value = 1;
               return value;
            }

            return value + 1;
         }
         return 1;
      }
      catch (Exception e)
      {
         return 0;
      }
      finally
      {
         //if there is any code to release the prepared statements
         //or connections back to pool, write them here.
         try
         {
            preparedStatement.close();
         }
         catch (SQLException e)
         {
         }
         try
         {
            connection.close();
         }
         catch (SQLException e)
         {
         }
      }
   }


   public String getNextId()
   {
      return "COU" + Long.toString(this.atomicLong.incrementAndGet());
   }

   private Connection getDbConnection()
   {
      try
      {
         String url = "jdbc:postgresql://localhost:5432/ezed";
         Class.forName("org.postgresql.Driver");
         return DriverManager.getConnection(url, "postgres", "postgres");
      }
      catch (Exception e)
      {
         return null;
      }
   }

   private static PreparedStatement getStatement(Connection conn)
   {
      try
      {
         return (conn.prepareStatement("select max(univofferingid) from universityofferings"));
      }
      catch (Exception e)
      {
         return null;
      }
   }
}
