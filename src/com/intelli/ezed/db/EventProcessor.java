package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.utils.StringSplitter;
import com.intelli.ezed.data.EventData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class EventProcessor extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;
   private ArrayList<String> courseActionList = new ArrayList<String>(10);

   public EventProcessor(String ruleName, String dbConnName, String courseActionList)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
      String[] courseList = StringSplitter.splitString(courseActionList, ",");
      for (int i = 0; i < courseList.length; i++)
      {
         this.courseActionList.add(courseList[i]);
      }
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      EventData eventData = (EventData) flowContext.getFromContext(EzedKeyConstants.EVENT_OBJECT);
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      String userid = (String) flowContext.getFromContext(EzedKeyConstants.USER_ID);

      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         if (courseActionList.contains(eventData.getActionId()))
         {
            if (eventData.isGoalRelated())
            {
               if (eventData.getChapterId() != null)
               {
                  ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdateEventCount");
                  ps.setString(1, userid);
                  ps.setString(2, eventData.getActionId());
                  ps.setString(3, eventData.getCourseId());
                  ps.setString(4, eventData.getChapterId());
                  ps.setString(5, eventData.getContentId());
               }
               else
               {
                  ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdateEventCountCourseLevelTest");
                  ps.setString(1, userid);
                  ps.setString(2, eventData.getActionId());
                  ps.setString(3, eventData.getCourseId());
                  ps.setString(4, eventData.getContentId());
               }
               int rows = ps.executeUpdate();
               if (logger.isDebugEnabled())
               {
                  logger.debug(rows + " rows updated in db while incrementing count of event for userid " + userid + " and action id " + eventData.getActionId() + " and course id " + eventData.getCourseId());
               }
            }
            response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
            return RuleDecisionKey.IF_COURSEACTION;
         }
         else
         {
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdateEventCountUserEvent");
            ps.setString(1, userid);
            ps.setString(2, eventData.getActionId());
            int rows = ps.executeUpdate();
            if (logger.isDebugEnabled())
            {
               logger.debug(rows + " rows updated in db while incrementing count of event for userid " + userid + " and action id " + eventData.getActionId());
            }
            response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
            return RuleDecisionKey.IF_USERACTION;
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while updating count in event database", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while updating points");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while updating count in event database", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while updating points");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

}
