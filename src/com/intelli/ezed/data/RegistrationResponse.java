package com.intelli.ezed.data;


public class RegistrationResponse extends ResponseObject
{
   private String registrationVerificaitonKey;
   private String defaultPassword = "0000";
   private String firstname = null;

   public String getDefaultPassword()
   {
      return defaultPassword;
   }

   public void setDefaultPassword(String defaultPassword)
   {
      this.defaultPassword = defaultPassword;
   }

   public String getRegistrationVerificaitonKey()
   {
      return registrationVerificaitonKey;
   }

   public void setRegistrationVerificaitonKey(String registrationVerificaitonKey)
   {
      this.registrationVerificaitonKey = registrationVerificaitonKey;
   }

   @Override
   public void prepareResponse() throws Exception
   {
      this.put("verifkey", getRegistrationVerificaitonKey());
      if (getDefaultPassword().compareTo("0000") != 0)
      {
         this.put("defaultPass", getDefaultPassword());
      }
      this.put("firstname", getFirstname());
   }

   public String getFirstname()
   {
      return firstname;
   }

   public void setFirstname(String firstname)
   {
      this.firstname = firstname;
   }

}
