package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.AdminProfile;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class CountryAddDeleter extends SingletonRule
{
   private static final int ADD_COUNTRY = 1;
   private static final int MOD_COUNTRY = 2;
   private static final int DEL_COUNTRY = 3;

   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;
   private int actionType;

   public CountryAddDeleter(String ruleName, String dbConnName, String actionType)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
      this.actionType = Integer.parseInt(actionType);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      AdminProfile adminProfile = (AdminProfile) flowContext.getFromContext(EzedKeyConstants.ADMIN_PROFILE);

      String countryName = messageData.getParameter("country");
      if (countryName == null || countryName.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Country name is invalid");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid country name");
         return RuleDecisionKey.ON_FAILURE;
      }

      boolean status = false;
      if (actionType == DEL_COUNTRY)
      {
         status = deleteCountry(dbConnName, countryName, response);
      }
      else if (actionType == ADD_COUNTRY)
      {
         status = addCountry(dbConnName, countryName, response);
      }

      if (status == true)
      {
         return RuleDecisionKey.ON_SUCCESS;
      }
      else
      {
         return RuleDecisionKey.ON_FAILURE;
      }
   }
   private boolean addCountry(String dbConnName2, String countryName, Response response)
   {

      Connection connection = DbConnectionManager.getConnectionByName(dbConnName2);

      try
      {
         //insert into countrylist values (?)
         PreparedStatement addCountry = DbConnectionManager.getPreparedStatement(dbConnName2, connection, "AddCountryToDatabase");
         addCountry.setString(1, countryName);
         addCountry.execute();
      }
      catch (SQLException e)
      {
         //e.printStackTrace();
         if (e.getMessage().contains("unique constraint"))
         {
            if (logger.isInfoEnabled())
            {
               logger.info("country : " + countryName + " already present in database.", e);
            }
            response.setParameters(EzedTransactionCodes.DUPLICATE_COUNTRY, "country : " + countryName + " already exists.");
            return false;
         }
         else
         {
            logger.error("Database exception occoured.", e);
            response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Database exception occoured while adding country " + countryName);
            return false;
         }
      }
      catch (Exception e)
      {
         logger.error("exception occoured.", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal Error while adding the country.");
         return false;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName2, connection);
      }
      response.setParameters(EzedTransactionCodes.SUCCESS, "Country : " + countryName + " added to database.");
      return true;
   }

   private boolean deleteCountry(String dbConnName2, String countryName, Response response)
   {
      Connection connection = DbConnectionManager.getConnectionByName(dbConnName2);

      try
      {
         //delete from countrylist where countryname = ?
         PreparedStatement deleteCountry = DbConnectionManager.getPreparedStatement(dbConnName2, connection, "DeleteCountry");
         deleteCountry.setString(1, countryName);
         int i = deleteCountry.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(i + " rows deleted while deleting country : " + countryName + " from database. ");
         }
      }
      catch (SQLException e)
      {
         //e.printStackTrace();
         if (e.getMessage().contains("foreign key"))
         {
            if (logger.isInfoEnabled())
            {
               logger.info("Can not delete county : " + countryName + " from database it is refereced from some other table.", e);
            }
            response.setParameters(EzedTransactionCodes.INVALID_OPRATION, "Country is being used in application can not delete.");
         }
         else
         {
            logger.error("Database exception occoured.", e);
            response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Database exception occoured while deleting the country.");
         }

         return false;
      }
      catch (Exception e)
      {
         logger.error("exception occoured.", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal Error while deleting the country.");
         return false;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName2, connection);
      }


      response.setParameters(EzedTransactionCodes.SUCCESS, "Country : " + countryName + " deleted from database.");
      return true;
   }
}
