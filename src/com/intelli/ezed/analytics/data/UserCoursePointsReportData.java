package com.intelli.ezed.analytics.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.slf4j.Logger;

import com.intelli.ezed.data.ResponseObject;

public class UserCoursePointsReportData extends ResponseObject
{
   private String userId;
   private String courseId;
   private String courseName;
   private int currentPoints;
   private int dayBeforePoints;
   private int dayToDayBeforePoints;
   private int weekBeforePoints;
   private int weekToWeekBeforePoints;
   private int monthBeforePoints;
   private int monthToMonthBeforePoints;
   private int sixMonthsBeforePoints;
   private int yearBeforePoints;

   private int pointsEarnedToday;
   private int pointsEarnedYesterday;
   private int pointsEarnedThisWeek;
   private int pointsEarnedLastWeek;
   private int pointsEarnedThisMonth;
   private int pointsEarnedLastMonth;
   private int pointsEarnedThisHalfYear;
   private int pointsEarnedLastHalfYear;

   public String getCourseName()
   {
      return courseName;
   }
   public void setCourseName(String courseName)
   {
      this.courseName = courseName;
   }
   public String getCourseId()
   {
      return courseId;
   }
   public void setCourseId(String courseId)
   {
      this.courseId = courseId;
   }
   public int getPointsEarnedToday()
   {
      return pointsEarnedToday;
   }
   public void setPointsEarnedToday(int pointsEarnedToday)
   {
      this.pointsEarnedToday = pointsEarnedToday;
   }
   public int getPointsEarnedYesterday()
   {
      return pointsEarnedYesterday;
   }
   public void setPointsEarnedYesterday(int pointsEarnedYesterday)
   {
      this.pointsEarnedYesterday = pointsEarnedYesterday;
   }
   public int getPointsEarnedThisWeek()
   {
      return pointsEarnedThisWeek;
   }
   public void setPointsEarnedThisWeek(int pointsEarnedThisWeek)
   {
      this.pointsEarnedThisWeek = pointsEarnedThisWeek;
   }
   public int getPointsEarnedLastWeek()
   {
      return pointsEarnedLastWeek;
   }
   public void setPointsEarnedLastWeek(int pointsEarnedLastWeek)
   {
      this.pointsEarnedLastWeek = pointsEarnedLastWeek;
   }
   public int getPointsEarnedThisMonth()
   {
      return pointsEarnedThisMonth;
   }
   public void setPointsEarnedThisMonth(int pointsEarnedThisMonth)
   {
      this.pointsEarnedThisMonth = pointsEarnedThisMonth;
   }
   public int getPointsEarnedLastMonth()
   {
      return pointsEarnedLastMonth;
   }
   public void setPointsEarnedLastMonth(int pointsEarnedLastMonth)
   {
      this.pointsEarnedLastMonth = pointsEarnedLastMonth;
   }
   public int getPointsEarnedThisHalfYear()
   {
      return pointsEarnedThisHalfYear;
   }
   public void setPointsEarnedThisHalfYear(int pointsEarnedThisHalfYear)
   {
      this.pointsEarnedThisHalfYear = pointsEarnedThisHalfYear;
   }
   public int getPointsEarnedLastHalfYear()
   {
      return pointsEarnedLastHalfYear;
   }
   public void setPointsEarnedLastHalfYear(int pointsEarnedLastHalfYear)
   {
      this.pointsEarnedLastHalfYear = pointsEarnedLastHalfYear;
   }
   public String getUserId()
   {
      return userId;
   }
   public void setUserId(String userId)
   {
      this.userId = userId;
   }
   public int getCurrentPoints()
   {
      return currentPoints;
   }
   public void setCurrentPoints(int currentPoints)
   {
      this.currentPoints = currentPoints;
   }
   public int getDayBeforePoints()
   {
      return dayBeforePoints;
   }
   public void setDayBeforePoints(int dayBeforePoints)
   {
      this.dayBeforePoints = dayBeforePoints;
   }
   public int getDayToDayBeforePoints()
   {
      return dayToDayBeforePoints;
   }
   public void setDayToDayBeforePoints(int dayToDayBeforePoints)
   {
      this.dayToDayBeforePoints = dayToDayBeforePoints;
   }
   public int getWeekBeforePoints()
   {
      return weekBeforePoints;
   }
   public void setWeekBeforePoints(int weekBeforePoints)
   {
      this.weekBeforePoints = weekBeforePoints;
   }
   public int getWeekToWeekBeforePoints()
   {
      return weekToWeekBeforePoints;
   }
   public void setWeekToWeekBeforePoints(int weekToWeekBeforePoints)
   {
      this.weekToWeekBeforePoints = weekToWeekBeforePoints;
   }
   public int getMonthBeforePoints()
   {
      return monthBeforePoints;
   }
   public void setMonthBeforePoints(int monthBeforePoints)
   {
      this.monthBeforePoints = monthBeforePoints;
   }
   public int getMonthToMonthBeforePoints()
   {
      return monthToMonthBeforePoints;
   }
   public void setMonthToMonthBeforePoints(int monthToMonthBeforePoints)
   {
      this.monthToMonthBeforePoints = monthToMonthBeforePoints;
   }
   public int getSixMonthsBeforePoints()
   {
      return sixMonthsBeforePoints;
   }
   public void setSixMonthsBeforePoints(int sixMonthsBeforePoints)
   {
      this.sixMonthsBeforePoints = sixMonthsBeforePoints;
   }
   public int getYearBeforePoints()
   {
      return yearBeforePoints;
   }
   public void setYearBeforePoints(int yearBeforePoints)
   {
      this.yearBeforePoints = yearBeforePoints;
   }

   public void calculateCourseHistoryPoints(String dbConnName, Logger logger) throws SQLException, Exception
   {
      this.fetchAllHistoryPointsForUserCourse(dbConnName, logger);
      this.setPointsEarnedToday(getCurrentPoints() - getDayBeforePoints());
      this.setPointsEarnedYesterday(getDayBeforePoints() - getDayToDayBeforePoints());
      this.setPointsEarnedThisWeek(getCurrentPoints() - getWeekBeforePoints());
      this.setPointsEarnedLastWeek(getWeekBeforePoints() - getWeekToWeekBeforePoints());
      this.setPointsEarnedThisMonth(getCurrentPoints() - getMonthBeforePoints());
      this.setPointsEarnedLastMonth(getMonthBeforePoints() - getMonthToMonthBeforePoints());
      this.setPointsEarnedThisHalfYear(getCurrentPoints() - getSixMonthsBeforePoints());
      this.setPointsEarnedLastHalfYear(getSixMonthsBeforePoints() - getYearBeforePoints());
   }

   private void fetchAllHistoryPointsForUserCourse(String dbConnName, Logger logger) throws SQLException, Exception
   {
      setDayBeforePoints(fetchPointsFromUserCourseTables(dbConnName, "FetchYesterdayCoursePoints", logger));
      setDayToDayBeforePoints(fetchPointsFromUserCourseTables(dbConnName, "FetchDayBeforeYesterdayCoursePoints", logger));
      setWeekBeforePoints(fetchPointsFromUserCourseTables(dbConnName, "FetchLastWeekCoursePoints", logger));
      setWeekToWeekBeforePoints(fetchPointsFromUserCourseTables(dbConnName, "FetchLastFortnightCoursePoints", logger));
      setMonthBeforePoints(fetchPointsFromUserCourseTables(dbConnName, "FetchLastMonthCoursePoints", logger));
      setMonthToMonthBeforePoints(fetchPointsFromUserCourseTables(dbConnName, "FetchLastTwoMonthCoursePoints", logger));
      setSixMonthsBeforePoints(fetchPointsFromUserCourseTables(dbConnName, "FetchLastSixMonthCoursePoints", logger));
      setYearBeforePoints(fetchPointsFromUserCourseTables(dbConnName, "FetchLastYearCoursePoints", logger));
   }

   private int fetchPointsFromUserCourseTables(String dbConnName, String prepStmtName, Logger logger) throws SQLException, Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, prepStmtName);
         ps.setString(1, getUserId());
         ps.setString(2, getCourseId());
         rs = ps.executeQuery();
         if (rs.next())
         {
            int points = rs.getInt(1);
            return points;
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("No entry found for user in history table for prep stmt name " + prepStmtName);
            }
            return 0;
         }

      }
      catch (SQLException e)
      {
         logger.error("Error while fetching points from one of users history tables for prep stmt name " + prepStmtName, e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching points from one of users history tables for prep stmt name " + prepStmtName, e);
         throw e;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
               logger.error("Error while closing result set while fetching points from one of users table");
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   public void fetchAllCourseHistoryPointsForUser(String dbConnName, Logger logger) throws SQLException, Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchCourseDetailsForUserCourse");
         ps.setString(1, getUserId());
         ps.setString(2, getCourseId());
         ps.setString(3, getCourseId());
         rs = ps.executeQuery();
         while (rs.next())
         {
            this.setCurrentPoints(rs.getInt("points"));
            this.setCourseName(rs.getString("coursename"));
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching user courses for userid " + getUserId(), e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching user courses for userid " + getUserId(), e);
         throw e;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
               logger.error("Error while closing result set while fetching courses of user");
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   @Override
   public void prepareResponse() throws Exception
   {
      this.put("courseid", getCourseId());
      this.put("coursename", getCourseName());
      this.put("pointsearnedtoday", getPointsEarnedToday());
      this.put("pointsearnedyesterday", getPointsEarnedYesterday());
      this.put("pointsearnedthisweek", getPointsEarnedThisWeek());
      this.put("pointsearnedlastweek", getPointsEarnedLastWeek());
      this.put("pointsearnedthismonth", getPointsEarnedThisMonth());
      this.put("pointsearnedlastmonth", getPointsEarnedLastMonth());
      this.put("pointsearnedthishalfyear", getPointsEarnedThisHalfYear());
      this.put("pointsearnedlasthalfyear", getPointsEarnedLastHalfYear());
   }
}
