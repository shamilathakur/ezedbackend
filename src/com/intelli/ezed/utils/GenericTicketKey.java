/*
 * Last changed by           : $Author: indraneeld $
 * Last check in             : $Date: 2012/06/26 06:46:53 $
 * CVS version number        : $Revision: 1.1 $
 * CVS archive file location : $Archive$
 *
 * Copyright Notice
 * ================
 * Copyright (c) 2006 - 2007 SABRE Development Group
 * Copying or reproduction without prior written approval is prohibited.
 * SABRE Group PROPRIETARY/CONFIDENTIAL. Use is subject to approval from the SABRE group.
 * 
 */
package com.intelli.ezed.utils;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class GenericTicketKey
{
   private final int protoId;
   private final boolean shortValueRequired;
   private final int padLength;
   private NumberFormat paddingFormatter;


   public GenericTicketKey(int protoId, boolean shortValueRequired, int padLength)
   {
      this.protoId = protoId;
      this.shortValueRequired = shortValueRequired;
      this.padLength = padLength;
      if (!shortValueRequired && padLength > 0)
      {
         char[] formatChars = new char[padLength];
         for (int i = 0; i < padLength; i++)
         {
            formatChars[i] = '0';
         }
         paddingFormatter = new DecimalFormat(new String(formatChars));
      }
   }

   public NumberFormat getPaddingFormatter()
   {
      return paddingFormatter;
   }

   public int getPadLength()
   {
      return padLength;
   }

   public boolean isShortValueRequired()
   {
      return shortValueRequired;
   }

   public int getProtoId()
   {
      return protoId;
   }

}
