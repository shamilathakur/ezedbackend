/*package com.intelli.ezed.checks;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EnrollmentReportRequestData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;

public class EnrollmentReqDataParser extends SingletonRule {
	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

	private String dbConnName;

	public EnrollmentReqDataParser(String ruleName, String dbConnName) {
		super(ruleName);
		this.dbConnName = dbConnName;
	}

	@Override
	public RuleDecisionKey executeRule(FlowContext flowContext) {

		flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
		DbConnectionManager.getConnectionByName(dbConnName);
		EnrollmentReportRequestData data = new EnrollmentReportRequestData();

		MessageData messageData = (MessageData) flowContext
				.getFromContext(MessageData.class.getName());

		String date;
		String enddate;
		String month;
		String endmonth;

		String univname;
		String college;
		String cityid;

		date = messageData.getParameter("date");
		enddate = messageData.getParameter("enddate");
		month = messageData.getParameter("month");
		endmonth = messageData.getParameter("endmonth");

		univname = messageData.getParameter("univname");
		college = messageData.getParameter("college");
		cityid = messageData.getParameter("cityid");

		try {
			if (date != "" && date != null) {
				date = date.substring(6, 10) + date.substring(0, 2)
						+ date.substring(3, 5) + "000000";
				data.setStartdate(date);
			}
			if (enddate.trim() != "" && enddate != null) {
				enddate = enddate.substring(6, 10) + enddate.substring(0, 2)
						+ enddate.substring(3, 5) + "000000";
				data.setEnddate(enddate);
			}

			if (month.trim() != "" && month != null) {
				month = month.substring(6, 10) + month.substring(0, 2);
				data.setMonth(month);
			}
			if (endmonth.trim() != "" && endmonth != null) {
				endmonth = endmonth.substring(6, 10) + endmonth.substring(0, 2);
				data.setEndmonth(endmonth);
			}

			if (univname != null && univname.trim() != "") {
				data.setUniversity(univname);
			}

			if (college != null && college.trim() != "") {
				data.setCollege(college);
			}

			if (cityid != null && cityid.trim() != "") {
				data.setCity(cityid);
			}
		} catch (Exception e) {
			logger.error("Exception occured while parsing the params from the request : "
					+ e);
			return RuleDecisionKey.ON_FAILURE;
		}

		flowContext.putIntoContext(EzedKeyConstants.ENROLLMENT_DATA, data);
		System.out.println("Data : date - " + data.getStartdate()
				+ " enddate - " + data.getEnddate() + " month - "
				+ data.getMonth() + " endmonth - " + data.getEndmonth()
				+ " univname - " + data.getUniversity() + " college - "
				+ data.getCollege() + "cityid - " + data.getCity());
		return RuleDecisionKey.ON_SUCCESS;
	}
}*/