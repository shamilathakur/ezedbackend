package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.a.helper.utils.StringSplitter;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class CountryUpdater extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public CountryUpdater(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      response.setStringOnlySend(true);

      String newCountryValue = messageData.getParameter("value");
      if (newCountryValue == null || newCountryValue.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("New country value is invalid " + newCountryValue);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "");
         return RuleDecisionKey.ON_FAILURE;
      }
      String origRowData = messageData.getParameter("rowdata");
      if (origRowData == null || origRowData.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Orig row data is invalid " + origRowData);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "");
         return RuleDecisionKey.ON_FAILURE;
      }

      String[] origRowStrings = StringSplitter.splitString(origRowData, "~~");
      if (origRowStrings.length < 2)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Orig row data is invalid since its length is not proper " + origRowData);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "");
         return RuleDecisionKey.ON_FAILURE;
      }

      String countryId = origRowStrings[1];
      if (countryId == null || countryId.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("State id in request is not proper " + countryId);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "");
         return RuleDecisionKey.ON_FAILURE;
      }

      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdateCountry");
         ps.setString(1, newCountryValue);
         ps.setString(2, countryId);
         int rows = ps.executeUpdate();
         if (rows > 0)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug(rows + " rows updated in db while updating country name for id " + countryId);
            }
            response.setParameters(EzedTransactionCodes.SUCCESS, newCountryValue);
            return RuleDecisionKey.ON_SUCCESS;
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Country cannot be updated in request for country id " + countryId);
            }
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "");
            return RuleDecisionKey.ON_FAILURE;
         }
      }
      catch (SQLException e)
      {
         logger.error("DB error while updating country name for id " + countryId, e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while updating country name for id " + countryId, e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
}
