package com.intelli.ezed.db;

import java.sql.SQLException;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.a.helper.utils.StringSplitter;
import com.intelli.ezed.analytics.data.UserCoursePointsReportData;
import com.intelli.ezed.analytics.data.UserPointsReportDataWithCourse;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class CourseSpecificPointsReportFetcher extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public CourseSpecificPointsReportFetcher(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      String userId = (String) flowContext.getFromContext(EzedKeyConstants.USER_ID);
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      String courseIdList = messageData.getParameter("courseid");
      if (courseIdList == null || courseIdList.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Course id is invalid " + courseIdList);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid course ids");
         return RuleDecisionKey.ON_FAILURE;
      }

      String type = messageData.getParameter("type");
      if (type == null || type.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Type of report required is invalid " + type + ". Setting default type as 1");
         }
         type = "1";
         return RuleDecisionKey.ON_FAILURE;
      }

      UserPointsReportDataWithCourse reportData = new UserPointsReportDataWithCourse();
      reportData.setUserId(userId);
      reportData.setReportType(type);

      String[] courseIds = StringSplitter.splitString(courseIdList.trim(), ",");
      UserCoursePointsReportData userCoursePointsReportData = null;
      for (int i = 0; i < courseIds.length; i++)
      {
         userCoursePointsReportData = new UserCoursePointsReportData();
         userCoursePointsReportData.setCourseId(courseIds[i]);
         userCoursePointsReportData.setUserId(userId);
         reportData.getUserCoursePoints().add(userCoursePointsReportData);
      }

      response.setResponseObject(reportData);
      try
      {
         reportData.calculateHistoryPoints(dbConnName, logger);
         reportData.prepareChart();
         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
         response.setStatusCodeSend(false);
         return RuleDecisionKey.ON_SUCCESS;
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching user points", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching points report");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching user points", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching points report");
         return RuleDecisionKey.ON_FAILURE;
      }
   }
}