package com.intelli.ezed.analytics.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.slf4j.Logger;

import com.intelli.ezed.data.ResponseObject;
import com.intelli.ezed.utils.AccountStatusHelper;

public class RegVsEnrollReportData extends ResponseObject
{
   private String userId;
   private String courseId;
   private String courseName;

   private int totalUsersRegistered = 0;
   private int yesterdayUsersRegistered = 0;
   private int lastWeekUsersRegistered = 0;
   private int lastMonthUsersRegistered = 0;
   private int lastYearUsersRegistered = 0;

   private int totalUsersEnrolled = 0;
   private int yesterdayUsersEnrolled = 0;
   private int lastWeekUsersEnrolled = 0;
   private int lastMonthUsersEnrolled = 0;
   private int lastYearUsersEnrolled = 0;


   public String getUserId()
   {
      return userId;
   }

   public int getTotalUsersRegistered()
   {
      return totalUsersRegistered;
   }
   public void setTotalUsersRegistered(int totalUsersRegistered)
   {
      this.totalUsersRegistered = totalUsersRegistered;
   }


   public int getYesterdayUsersRegistered()
   {
      return yesterdayUsersRegistered;
   }


   public void setYesterdayUsersRegistered(int yesterdayUsersRegistered)
   {
      this.yesterdayUsersRegistered = yesterdayUsersRegistered;
   }


   public int getLastWeekUsersRegistered()
   {
      return lastWeekUsersRegistered;
   }


   public void setLastWeekUsersRegistered(int lastWeekUsersRegistered)
   {
      this.lastWeekUsersRegistered = lastWeekUsersRegistered;
   }


   public int getLastMonthUsersRegistered()
   {
      return lastMonthUsersRegistered;
   }


   public void setLastMonthUsersRegistered(int lastMonthUsersRegistered)
   {
      this.lastMonthUsersRegistered = lastMonthUsersRegistered;
   }


   public int getLastYearUsersRegistered()
   {
      return lastYearUsersRegistered;
   }


   public void setLastYearUsersRegistered(int lastYearUsersRegistered)
   {
      this.lastYearUsersRegistered = lastYearUsersRegistered;
   }


   public int getTotalUsersEnrolled()
   {
      return totalUsersEnrolled;
   }


   public void setTotalUsersEnrolled(int totalUsersEnrolled)
   {
      this.totalUsersEnrolled = totalUsersEnrolled;
   }


   public int getYesterdayUsersEnrolled()
   {
      return yesterdayUsersEnrolled;
   }


   public void setYesterdayUsersEnrolled(int yesterdayUsersEnrolled)
   {
      this.yesterdayUsersEnrolled = yesterdayUsersEnrolled;
   }


   public int getLastWeekUsersEnrolled()
   {
      return lastWeekUsersEnrolled;
   }


   public void setLastWeekUsersEnrolled(int lastWeekUsersEnrolled)
   {
      this.lastWeekUsersEnrolled = lastWeekUsersEnrolled;
   }


   public int getLastMonthUsersEnrolled()
   {
      return lastMonthUsersEnrolled;
   }


   public void setLastMonthUsersEnrolled(int lastMonthUsersEnrolled)
   {
      this.lastMonthUsersEnrolled = lastMonthUsersEnrolled;
   }


   public int getLastYearUsersEnrolled()
   {
      return lastYearUsersEnrolled;
   }


   public void setLastYearUsersEnrolled(int lastYearUsersEnrolled)
   {
      this.lastYearUsersEnrolled = lastYearUsersEnrolled;
   }


   public void setUserId(String userId)
   {
      this.userId = userId;
   }

   public String getCourseId()
   {
      return courseId;
   }

   public void setCourseId(String courseId)
   {
      this.courseId = courseId;
   }

   public String getCourseName()
   {
      return courseName;
   }

   public void setCourseName(String courseName)
   {
      this.courseName = courseName;
   }


   public void calculateUserRegCounts(String dbConnName, Logger logger) throws SQLException, Exception
   {
      this.fetchAllHistoryPointsForUserCourse(dbConnName, logger);
   }

   private void fetchAllHistoryPointsForUserCourse(String dbConnName, Logger logger) throws SQLException, Exception
   {
      fetchChkflagFromUserTable(dbConnName, "GetChkflagFromUsers", logger);
      fetchChkflagFromYesterdayUserTable(dbConnName, "GetChkflagFromYesterdayUsers", logger);
      fetchChkflagFromLastWeekUserTable(dbConnName, "GetChkflagFromLastWeekUsers", logger);
      fetchChkflagFromLastMonthUserTable(dbConnName, "GetChkflagFromLastMonthUsers", logger);
      fetchChkflagFromLastYearUserTable(dbConnName, "GetChkflagFromlastYearUsers", logger);
   }

   private int fetchChkflagFromUserTable(String dbConnName, String prepStmtName, Logger logger) throws SQLException, Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, prepStmtName);
         rs = ps.executeQuery();
         while (rs.next())
         {
            int chkFlag = rs.getInt("chkflag");
            if (AccountStatusHelper.isRegistered(chkFlag))
            {
               setTotalUsersRegistered(getTotalUsersRegistered() + 1);
            }
            if (AccountStatusHelper.isValidate(chkFlag))
            {
               setTotalUsersEnrolled(getTotalUsersEnrolled() + 1);
            }
         }
         //         else
         //         {
         //            if (logger.isDebugEnabled())
         //            {
         //               logger.debug("No entry found for user in history table for prep stmt name " + prepStmtName);
         //            }
         //            return 0;
         //         }

      }
      catch (SQLException e)
      {
         logger.error("Error while fetching points from one of users history tables for prep stmt name " + prepStmtName, e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching points from one of users history tables for prep stmt name " + prepStmtName, e);
         throw e;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
               logger.error("Error while closing result set while fetching points from one of users table");
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      return 1;
   }

   private int fetchChkflagFromYesterdayUserTable(String dbConnName, String prepStmtName, Logger logger) throws SQLException, Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, prepStmtName);
         rs = ps.executeQuery();
         while (rs.next())
         {
            int chkFlag = rs.getInt("chkflag");
            if (AccountStatusHelper.isRegistered(chkFlag))
            {
               setYesterdayUsersRegistered(getYesterdayUsersRegistered() + 1);
            }
            if (AccountStatusHelper.isValidate(chkFlag))
            {
               setYesterdayUsersEnrolled(getYesterdayUsersEnrolled() + 1);
            }
         }
         //         else
         //         {
         //            if (logger.isDebugEnabled())
         //            {
         //               logger.debug("No entry found for user in history table for prep stmt name " + prepStmtName);
         //            }
         //            return 0;
         //         }

      }
      catch (SQLException e)
      {
         logger.error("Error while fetching points from one of users history tables for prep stmt name " + prepStmtName, e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching points from one of users history tables for prep stmt name " + prepStmtName, e);
         throw e;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
               logger.error("Error while closing result set while fetching points from one of users table");
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      return 1;
   }

   private int fetchChkflagFromLastWeekUserTable(String dbConnName, String prepStmtName, Logger logger) throws SQLException, Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, prepStmtName);
         rs = ps.executeQuery();
         while (rs.next())
         {
            int chkFlag = rs.getInt("chkflag");
            if (AccountStatusHelper.isRegistered(chkFlag))
            {
               setLastWeekUsersRegistered(getLastWeekUsersRegistered() + 1);
            }
            if (AccountStatusHelper.isValidate(chkFlag))
            {
               setLastWeekUsersEnrolled(getLastWeekUsersEnrolled() + 1);
            }
         }
         //         else
         //         {
         //            if (logger.isDebugEnabled())
         //            {
         //               logger.debug("No entry found for user in history table for prep stmt name " + prepStmtName);
         //            }
         //            return 0;
         //         }

      }
      catch (SQLException e)
      {
         logger.error("Error while fetching points from one of users history tables for prep stmt name " + prepStmtName, e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching points from one of users history tables for prep stmt name " + prepStmtName, e);
         throw e;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
               logger.error("Error while closing result set while fetching points from one of users table");
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      return 1;
   }

   private int fetchChkflagFromLastMonthUserTable(String dbConnName, String prepStmtName, Logger logger) throws SQLException, Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, prepStmtName);
         rs = ps.executeQuery();
         while (rs.next())
         {
            int chkFlag = rs.getInt("chkflag");
            if (AccountStatusHelper.isRegistered(chkFlag))
            {
               setLastMonthUsersRegistered(getLastMonthUsersRegistered() + 1);
            }
            if (AccountStatusHelper.isValidate(chkFlag))
            {
               setLastMonthUsersEnrolled(getLastMonthUsersEnrolled() + 1);
            }
         }
         //         else
         //         {
         //            if (logger.isDebugEnabled())
         //            {
         //               logger.debug("No entry found for user in history table for prep stmt name " + prepStmtName);
         //            }
         //            return 0;
         //         }

      }
      catch (SQLException e)
      {
         logger.error("Error while fetching points from one of users history tables for prep stmt name " + prepStmtName, e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching points from one of users history tables for prep stmt name " + prepStmtName, e);
         throw e;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
               logger.error("Error while closing result set while fetching points from one of users table");
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      return 1;
   }

   private int fetchChkflagFromLastYearUserTable(String dbConnName, String prepStmtName, Logger logger) throws SQLException, Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, prepStmtName);
         rs = ps.executeQuery();
         while (rs.next())
         {
            int chkFlag = rs.getInt("chkflag");
            if (AccountStatusHelper.isRegistered(chkFlag))
            {
               setLastYearUsersRegistered(getLastYearUsersRegistered() + 1);
            }
            if (AccountStatusHelper.isValidate(chkFlag))
            {
               setLastYearUsersEnrolled(getLastYearUsersEnrolled() + 1);
            }
         }
         //         else
         //         {
         //            if (logger.isDebugEnabled())
         //            {
         //               logger.debug("No entry found for user in history table for prep stmt name " + prepStmtName);
         //            }
         //            return 0;
         //         }

      }
      catch (SQLException e)
      {
         logger.error("Error while fetching points from one of users history tables for prep stmt name " + prepStmtName, e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching points from one of users history tables for prep stmt name " + prepStmtName, e);
         throw e;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
               logger.error("Error while closing result set while fetching points from one of users table");
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      return 1;
   }

   @Override
   public void prepareResponse() throws Exception
   {
   }
}
