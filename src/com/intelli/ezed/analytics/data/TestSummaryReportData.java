package com.intelli.ezed.analytics.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.sabre.db.DbConnectionManager;
import org.slf4j.Logger;

import com.intelli.ezed.data.ResponseObject;

public class TestSummaryReportData extends ResponseObject
{

   //   private String courseId;
   //   private String chapterId;
   private String userId;
   private ArrayList<SingleTestSummaryData> testIdList = new ArrayList<SingleTestSummaryData>();
   private ArrayList<SingleTestAttemptSummaryData> testIdAttemptList = new ArrayList<SingleTestAttemptSummaryData>();
   GenericChartData title = new GenericChartData();
   GenericChartData xAxis = new GenericChartData();
   GenericChartData yAxis = new GenericChartData();
   private GenericChartData type = new GenericChartData();

   public void prepareChart() throws Exception
   {
      type.getText().add("type");
      type.getValues().add("line");
      type.getText().add("marginRight");
      type.getValues().add(130);
      type.getText().add("marginBottom");
      type.getValues().add(25);
      type.prepareResponse();

      title.getText().add("text");
      title.getValues().add("Test Attempt summary report");
      title.getText().add("x");
      title.getValues().add(-20);
      title.prepareResponse();

      xAxis.getText().add("categories");
      String[] cats = new String[getMaxAttempts()];
      for (int i = 0; i < cats.length; i++)
      {
         cats[i] = Integer.toString(i + 1);
      }
      xAxis.getValues().add(cats);
      xAxis.getText().add("title");
      GenericChartData xAxisTitle = new GenericChartData();
      xAxisTitle.getText().add("text");
      xAxisTitle.getValues().add("Attempt no");
      xAxis.getValues().add(xAxisTitle);
      xAxisTitle.prepareResponse();
      xAxis.prepareResponse();

      yAxis.getText().add("title");
      GenericChartData yAxisTitle = new GenericChartData();
      yAxisTitle.getText().add("text");
      yAxisTitle.getValues().add("Percentage of correct/attempted answers");
      yAxis.getValues().add(yAxisTitle);
      yAxisTitle.prepareResponse();
      yAxis.prepareResponse();
   }

   private int getMaxAttempts()
   {
      int maxVal = 0;
      for (int i = 0; i < testIdList.size(); i++)
      {
         if (maxVal < testIdList.get(i).getTestSummaries().size())
         {
            maxVal = testIdList.get(i).getTestSummaries().size();
         }
      }
      if (maxVal > 20)
      {
         return 20;
      }
      return maxVal;
   }


   public ArrayList<SingleTestAttemptSummaryData> getTestIdAttemptList()
   {
      return testIdAttemptList;
   }

   public void setTestIdAttemptList(ArrayList<SingleTestAttemptSummaryData> testIdAttemptList)
   {
      this.testIdAttemptList = testIdAttemptList;
   }

   public ArrayList<SingleTestSummaryData> getTestIdList()
   {
      return testIdList;
   }

   public void setTestIdList(ArrayList<SingleTestSummaryData> testIdList)
   {
      this.testIdList = testIdList;
   }

   public String getUserId()
   {
      return userId;
   }

   public void setUserId(String userId)
   {
      this.userId = userId;
   }

   //   public String getCourseId()
   //   {
   //      return courseId;
   //   }
   //
   //   public void setCourseId(String courseId)
   //   {
   //      this.courseId = courseId;
   //   }
   //
   //   public String getChapterId()
   //   {
   //      return chapterId;
   //   }
   //
   //   public void setChapterId(String chapterId)
   //   {
   //      this.chapterId = chapterId;
   //   }

   public void populateTestSummary(String dbConnName, Logger logger) throws SQLException, Exception
   {
      //      this.fetchTest(dbConnName, logger);
      SingleTestSummaryData data = null;
      SingleTestAttemptSummaryData attemptData = null;
      for (int i = 0; i < getTestIdList().size(); i++)
      {
         data = getTestIdList().get(i);
         attemptData = getTestIdAttemptList().get(i);
         try
         {
            fetchTestDetails(data, attemptData, dbConnName, logger);
            fetchTestSummary(data, attemptData, dbConnName, logger);
         }
         catch (Exception e)
         {
            logger.error("Error while fetching test summary for an individual test attempt");
         }
      }
   }

   private void fetchTestDetails(SingleTestSummaryData testData, SingleTestAttemptSummaryData attemptData, String dbConnName, Logger logger) throws SQLException, Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchTestDetails");
         ps.setString(1, testData.getTestId());
         rs = ps.executeQuery();
         if (rs.next())
         {
            testData.setTestName(rs.getString("testname"));
            attemptData.setTestName(rs.getString("testname"));
         }
      }
      catch (SQLException e)
      {
         logger.error("DB error while fetching test details for test id", e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching test details for test id ", e);
         throw e;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
               logger.error("Error while closing resultset while fetching individual test summary for test id " + testData.getTestId());
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   private void fetchTestSummary(SingleTestSummaryData testData, SingleTestAttemptSummaryData attemptData, String dbConnName, Logger logger) throws SQLException, Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchTestSummary");
         ps.setString(1, testData.getTestId());
         ps.setString(2, getUserId());
         rs = ps.executeQuery();
         int isTestCompleted = 0;
         SingleSummaryData summaryData = null;
         while (rs.next())
         {
            summaryData = new SingleSummaryData();
            summaryData.setNoOfQuestions(rs.getInt("numberofquestions"));
            summaryData.setNoOfQuestionsAttempted(rs.getInt("attempted"));
            summaryData.setNoOfCorrectAnswers(rs.getInt("correct"));
            isTestCompleted = rs.getInt("testcompleted");
            if (isTestCompleted == 0)
            {
               summaryData.setTestCompleted(false);
            }
            else
            {
               summaryData.setTestCompleted(true);
            }
            testData.getTestSummaries().add(summaryData);
            attemptData.getTestSummaries().add(summaryData);
         }
      }
      catch (SQLException e)
      {
         logger.error("DB error while fetching test summary for test id", e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching test summary for test id ", e);
         throw e;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
               logger.error("Error while closing resultset while fetching individual test summary for test id " + testData.getTestId());
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   //   private void fetchTest(String dbConnName, Logger logger) throws SQLException, Exception
   //   {
   //      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
   //      PreparedStatement ps = null;
   //      ResultSet rs = null;
   //      try
   //      {
   //         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "");
   //         ps.setString(1, getCourseId());
   //         ps.setString(2, getChapterId());
   //         rs = ps.executeQuery();
   //         String testId = null;
   //         while (rs.next())
   //         {
   //            testId = rs.getString(1);
   //            if (testId != null && testId.trim().compareTo("") != 0)
   //            {
   //               SingleTestSummaryData data = new SingleTestSummaryData();
   //               data.setTestId(rs.getString(1));
   //               testIdList.add(data);
   //            }
   //         }
   //         if (testIdList.size() == 0)
   //         {
   //            if (logger.isDebugEnabled())
   //            {
   //               logger.debug("No tests found against the course id " + getCourseId() + " and chapter id " + getChapterId());
   //            }
   //            throw new Exception("No tests found in the current course/chapter");
   //         }
   //      }
   //      catch (SQLException e)
   //      {
   //         logger.error("DB error while fetching list of tests against course and chapter");
   //         throw e;
   //      }
   //      catch (Exception e)
   //      {
   //         logger.error("Error while fetching list of tests against course and chapter", e);
   //         throw e;
   //      }
   //      finally
   //      {
   //         if (rs != null)
   //         {
   //            rs.close();
   //         }
   //         DbConnectionManager.releaseConnection(dbConnName, conn);
   //      }
   //   }

   @Override
   public void prepareResponse() throws Exception
   {
      this.put("title", title);
      this.put("xAxis", xAxis);
      this.put("yAxis", yAxis);
      this.put("chart", type);
      JSONArray jsonArray = new JSONArray();
      //      SingleTestSummaryData datas[] = new SingleTestSummaryData[testIdList.size()];
      for (int i = 0; i < testIdList.size(); i++)
      {
         testIdList.get(i).prepareResponse();
         jsonArray.put(testIdList.get(i));

         testIdAttemptList.get(i).prepareResponse();
         jsonArray.put(testIdAttemptList.get(i));
         //         datas[i] = testIdList.get(i);
      }
      this.put("series", jsonArray);
   }

}
