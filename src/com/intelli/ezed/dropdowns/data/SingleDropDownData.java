package com.intelli.ezed.dropdowns.data;

import com.intelli.ezed.data.ResponseObject;

public class SingleDropDownData extends ResponseObject
{
   private String param;
   private String value;
   private Float valueInt;

   public String getParam()
   {
      return param;
   }

   public void setParam(String param)
   {
      this.param = param;
   }

   public String getValue()
   {
      return value;
   }

   public void setValue(String value)
   {
      this.value = value;
   }

   @Override
   public void prepareResponse() throws Exception
   {
      this.put("param", getParam());
      if (getValue() != null)
      {
         this.put("value", getValue());
      }
      else
      {
         this.put("value", Float.toString((float) Math.round(getValueInt() * 100) / 100));
      }
   }

   public Float getValueInt()
   {
      return valueInt;
   }

   public void setValueInt(Float valueInt)
   {
      this.valueInt = valueInt;
   }

}
