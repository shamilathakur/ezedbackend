package com.intelli.ezed.frontend.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedLoggerName;

public class UniversityDatabaseInserter
{
   //use your own logger name at this position
   private String loggerName = EzedLoggerName.SERVICE_LOG;
   private Logger logger = LoggerFactory.getLogger(loggerName);

   private String website;
   private String univname;
   private String phonenumber;
   private String cityid;
   
   public String getWebsite()
   {
      return website;
   }
   public void setWebsite(String website)
   {
      this.website = website;
   }
   public String getUnivname()
   {
      return univname;
   }
   public void setUnivname(String univname)
   {
      this.univname = univname;
   }
   public String getPhonenumber()
   {
      return phonenumber;
   }
   public void setPhonenumber(String phonenumber)
   {
      this.phonenumber = phonenumber;
   }
   public String getCityid()
   {
      return cityid;
   }
   public void setCityid(String cityid)
   {
      this.cityid = cityid;
   }

   public int fetchFromDatabase()
   {
      //change how you get your connection here. you can use your own database connection pool or whatever.
      Connection connection = getDbConnection();
      if (connection == null)
      {
         return EzedFrontEndTransactionCodes.NO_CONNECTION;
      }

      //change how you get your prepared statement here. you can put the connection in some configuration files for any other setup.
      PreparedStatement preparedStatement = getFetchStatement(connection);
      if (preparedStatement == null)
      {
         return EzedFrontEndTransactionCodes.NO_STATEMENT;
      }

      ResultSet resultSet = null;

      try
      {
         preparedStatement.setString(1, getUnivname());
         resultSet = preparedStatement.executeQuery();

         if (resultSet != null && resultSet.next())
         {
            setCityid(resultSet.getString("cityid"));
            setPhonenumber(resultSet.getString("phonenumber"));
            setWebsite(resultSet.getString("website"));
         }
         else
         {
            if (logger.isInfoEnabled())
            {
               logger.info("No data available for university Name : " + getUnivname());
            }
            return EzedFrontEndTransactionCodes.INVALID_OPRATION;
         }

      }
      catch (SQLException e)
      {
         logger.error("Database exception occoured while fetching university details from database. ", e);
         return EzedFrontEndTransactionCodes.DB_EXCEPTION_OCCURRED;
      }
      catch (Exception e)
      {
         logger.error("Exception occoured while getting university details from database. ", e);
         return EzedFrontEndTransactionCodes.INTERNAL_ERROR;
      }
      finally
      {
         try
         {
            resultSet.close();
         }
         catch (SQLException e)
         {
            logger.error("Exception occoured while closing the resultset", e);
         }
      }
      return EzedFrontEndTransactionCodes.SUCCESS;
   }

   public int insertIntoDatabase()
   {
      //change how you get your connection here. you can use your own database connection pool or whatever.
      Connection connection = getDbConnection();
      if (connection == null)
      {
         return EzedFrontEndTransactionCodes.NO_CONNECTION;
      }

      //change how you get your prepared statement here. you can put the connection in some configuration files for any other setup.
      PreparedStatement preparedStatement = getInsertStatement(connection);
      if (preparedStatement == null)
      {
         return EzedFrontEndTransactionCodes.NO_STATEMENT;
      }

      //website,univname,phonenumber,cityid

      try
      {
         preparedStatement.setString(1, getWebsite());
         preparedStatement.setString(2, getUnivname().toUpperCase());
         preparedStatement.setString(3, getPhonenumber());
         preparedStatement.setString(4, getCityid());

         boolean i = preparedStatement.execute();

         if (i == true)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Course added in database.");
            }
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("no data inserted in database status : " + i);
            }
            return EzedFrontEndTransactionCodes.NO_ROWS_INSERTED;
         }

      }
      catch (SQLException e)
      {
         if (e.getMessage().contains("foreign key constraint"))
         {
            if (logger.isInfoEnabled())
            {
               logger.info("City id provided is not present in the system.");
            }
            return EzedFrontEndTransactionCodes.INVALID_MESSAGE_PARAMETER;
         }

         logger.error("Database exception occoured while adding city name into the database.", e);
         return EzedFrontEndTransactionCodes.DB_EXCEPTION_OCCURRED;

      }
      catch (Exception e)
      {
         logger.error("Exception occourd while inserting univeristy details in database.", e);
         return EzedFrontEndTransactionCodes.INTERNAL_ERROR;
      }


      return EzedFrontEndTransactionCodes.SUCCESS;
   }

   public int persisteIntoDatabase()
   {
    //change how you get your connection here. you can use your own database connection pool or whatever.
      Connection connection = getDbConnection();
      if (connection == null)
      {
         return EzedFrontEndTransactionCodes.NO_CONNECTION;
      }

      //change how you get your prepared statement here. you can put the connection in some configuration files for any other setup.
      PreparedStatement preparedStatement = getUpdateStatement(connection);
      if (preparedStatement == null)
      {
         return EzedFrontEndTransactionCodes.NO_STATEMENT;
      }

      //website,univname,phonenumber,cityid

      try
      {
         preparedStatement.setString(1, getWebsite());
         preparedStatement.setString(2, getPhonenumber());
         preparedStatement.setString(3, getCityid());
         preparedStatement.setString(4, getUnivname());

         boolean i = preparedStatement.execute();

         if (i == true)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Course added in database.");
            }
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("no data inserted in database status : " + i);
            }
            return EzedFrontEndTransactionCodes.NO_ROWS_INSERTED;
         }

      }
      catch (SQLException e)
      {
         if (e.getMessage().contains("foreign key constraint"))
         {
            if (logger.isInfoEnabled())
            {
               logger.info("City id provided is not present in the system.");
            }
            return EzedFrontEndTransactionCodes.INVALID_MESSAGE_PARAMETER;
         }

         logger.error("Database exception occoured while adding city name into the database.", e);
         return EzedFrontEndTransactionCodes.DB_EXCEPTION_OCCURRED;

      }
      catch (Exception e)
      {
         logger.error("Exception occourd while inserting univeristy details in database.", e);
         return EzedFrontEndTransactionCodes.INTERNAL_ERROR;
      }
      return EzedFrontEndTransactionCodes.SUCCESS;
   }
   
   private Connection getDbConnection()
   {
      try
      {
         String url = "jdbc:postgresql://localhost:5432/ezed";
         Class.forName("org.postgresql.Driver");
         return DriverManager.getConnection(url, "postgres", "postgres");
      }
      catch (Exception e)
      {
         return null;
      }
   }

   private PreparedStatement getInsertStatement(Connection conn)
   {
      try
      {
         return (conn.prepareStatement("insert into universitylist (website,univname,phonenumber,cityid) values (?,?,?,?)"));
      }
      catch (Exception e)
      {
         return null;
      }
   }

   private PreparedStatement getUpdateStatement(Connection conn)
   {
      try
      {
         return (conn.prepareStatement("update universitylist set website = ?,phonenumber = ?,cityid=? where univname = ?"));
      }
      catch (Exception e)
      {
         return null;
      }
   }

   private PreparedStatement getFetchStatement(Connection conn)
   {
      try
      {
         return (conn.prepareStatement("select * from universitylist where univname = ?"));
      }
      catch (Exception e)
      {
         return null;
      }
   }


}
