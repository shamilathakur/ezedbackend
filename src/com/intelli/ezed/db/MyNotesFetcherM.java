package com.intelli.ezed.db;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.MyNotes;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.MyNotesList;
import com.intelli.ezed.data.Response;
public class MyNotesFetcherM extends SingletonRule{

	private String dbConnName;
	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
	
	public MyNotesFetcherM(String ruleName, String dbConnName) {
		super(ruleName);
		this.dbConnName = dbConnName;
	}
	
	@Override
	
	public RuleDecisionKey executeRule(FlowContext flowContext) {

		Response response = (Response) flowContext
				.getFromContext(EzedKeyConstants.EZED_RESPONSE);
		MessageData messageData = (MessageData) flowContext
				.getFromContext(MessageData.class.getName());
		
		
		String courseid = messageData.getParameter("courseid");
	     if(courseid == null || courseid.trim().compareTo("")==0){
	    	 if(logger.isDebugEnabled()){
	    		 logger.debug("course id in request is invalid");
	    	 }
	    	 response.setParameters(EzedTransactionCodes.PROCESSING_REQUEST, "Invalid Course Id");
	    	 return RuleDecisionKey.ON_FAILURE;
	     }
	     
	     String chapterid = messageData.getParameter("chapterid");
	     if(chapterid == null || chapterid.trim().compareTo("")==0){
	    	 if(logger.isDebugEnabled()){
	    		 logger.debug("Chapter id in request is invalid");
	    	 }
	    	 response.setParameters(EzedTransactionCodes.PROCESSING_REQUEST, "Invalid Chapter Id");
	    	 return RuleDecisionKey.ON_FAILURE;
	     }
	     
	     String userid = messageData.getParameter("email");
	     if(userid == null || userid.trim().compareTo("")==0){
	    	 if(logger.isDebugEnabled()){
	    		 logger.debug("User id in request is invalid");
	    	 }
	    	 response.setParameters(EzedTransactionCodes.PROCESSING_REQUEST, "Invalid User Id");
	    	 return RuleDecisionKey.ON_FAILURE;
	     }
	     
	    
	     
	     final String query = 
					
					"SELECT "
					+ "notes.createtime,notes.note,content.contentname,"+
							"notes.imagepath,notes.noteid from mynotes notes, "+
									 "contentlist content where notes.contentid=content.contentid "+
							"AND notes.courseid='"+courseid+"' AND notes.chapterid='"+chapterid+"' AND notes.userid='"+userid+ "'";
	     StringBuilder sb = new StringBuilder("");
		 System.out.println("String buffer : " + sb.toString());
		 String contentid = messageData.getParameter("contentid");
		 if (contentid != null && contentid.trim().compareTo("") != 0 && contentid.compareTo("null") != 0) {
				String qryContentId = " and notes.contentid = '" + contentid + "' ";
				sb.append(qryContentId);
			}
	     sb.append(" order by createtime") ;
	     
	     String finalQuery = null;
	     
	     if (!sb.equals("")) {
				finalQuery = query + sb.toString();
			}
	     System.out.println("finalquery "+ finalQuery);
			Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
			PreparedStatement ps = null;
			ResultSet rs = null;

			 ArrayList<MyNotes> myNotesList = new ArrayList<MyNotes>();
			 MyNotesList myNotesListObj = new MyNotesList();
			try {
					ps = conn.prepareStatement(finalQuery);
					System.out.println("Final Query : " + ps);
					rs = ps.executeQuery();
					while (rs.next()) {
						MyNotes myNote = new MyNotes();
						myNote.setCreateTime(rs.getString("createtime"));
						myNote.setNote(rs.getString("note"));
						myNote.setContentName(rs.getString("contentname"));
						myNote.setImagePath(rs.getString("imagepath"));
						myNote.setNoteId(rs.getString("noteid"));
						myNotesList.add(myNote);
					}
					myNotesListObj.setNoteslist(myNotesList);
				 }
				 catch (SQLException e)
		         {
		            logger.error("Error while fetching CoursesWithPaging", e);
		            response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "PL SQL Error while fetching CoursesWithPaging : " + e.getMessage());
		            return RuleDecisionKey.ON_FAILURE;
		         }
		         catch (Exception e)
		         {
		            logger.error("Error while fetching test details", e);
		            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching CoursesWithPaging : " + e.getMessage());
		            return RuleDecisionKey.ON_FAILURE;
		         }
		         finally
		         {
		            if (rs != null)
		            {
		               try
		               {
		                  rs.close();
		               }
		               catch (SQLException e)
		               {
		               }
		            }
		            DbConnectionManager.releaseConnection(dbConnName, conn);
		         }
				 response.setResponseObject(myNotesListObj);
			     response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
				 return RuleDecisionKey.ON_SUCCESS;
			
				}
	 
		

}
