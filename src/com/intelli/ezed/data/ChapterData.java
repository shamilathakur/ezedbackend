package com.intelli.ezed.data;

public class ChapterData extends ResponseObject
{
   private String chapterId;
   private String chapterName;
   private String desc;

   public String getChapterId()
   {
      return chapterId;
   }

   public void setChapterId(String chapterId)
   {
      this.chapterId = chapterId;
   }

   public String getChapterName()
   {
      return chapterName;
   }

   public void setChapterName(String chapterName)
   {
      this.chapterName = chapterName;
   }

   public String getDesc()
   {
      return desc;
   }

   public void setDesc(String desc)
   {
      this.desc = desc;
   }

   @Override
   public void prepareResponse() throws Exception
   {
      this.put("chapterid", getChapterId());
      this.put("chaptername", getChapterName());
      this.put("description", getDesc());
   }

}
