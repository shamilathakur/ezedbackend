package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.CartData;
import com.intelli.ezed.data.CartDetails;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.UserProfile;
import com.intelli.ezed.sheduler.PurchasedCartUpdater;
import com.intelli.ezed.utils.PaymentLockManager;

public class CartToUserAdder extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private Logger cartLogger = LoggerFactory.getLogger(EzedLoggerName.CART_LOG);
   private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

   public CartToUserAdder(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      UserProfile userProfile = (UserProfile) flowContext.getFromContext(EzedKeyConstants.USER_DETAILS);
      String userId = userProfile.getUserid();
      CartDetails cartObject = (CartDetails) flowContext.getFromContext(EzedKeyConstants.CART_DETAILS);
      CartData[] cartData = cartObject.getCartDatas();
      String cartDetails = (String) flowContext.getFromContext(EzedKeyConstants.CART_STRING);
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      response.setParameters(EzedTransactionCodes.SUCCESS, "");
      StringBuffer sb = new StringBuffer();

      Connection conn = null;
      PreparedStatement ps = null;
      PreparedStatement ps1 = null;
     
      for (int i = 0; i < cartData.length; i++)
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         try
         {
            if (cartData[i].isUpdateUserCourse() && cartData[i].isUpdateStartDate())
            {
               ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdateUserCourseType");
               ps.setString(1, cartData[i].getCourseType());
               ps.setString(2, sdf.format(cartData[i].getStartDate()));
               ps.setString(3, userId);
               ps.setString(4, cartData[i].getCourseId());
               ps.setString(5, sdf.format(cartData[i].getEndDate()));
              // ps.setString(6, cartObject.getPaymentId());
                            int rows = ps.executeUpdate();
               if (logger.isDebugEnabled())
               {
                  logger.debug(rows + " rows updated in database while updateing course type for cart for userid " + userId);
               }
               if (rows == 1)
               {
                  sb.append("|").append(cartData[i].getCourseId()).append(",").append(EzedTransactionCodes.SUCCESS);
                  cartData[i].setStatusCode(EzedTransactionCodes.SUCCESS);
               }
               else
               {
                  response.setResponseCode(EzedTransactionCodes.INTERNAL_ERROR);
                  sb.append("|").append(cartData[i].getCourseId()).append(",").append(EzedTransactionCodes.INTERNAL_ERROR);
                  cartData[i].setStatusCode(EzedTransactionCodes.INTERNAL_ERROR);
               }
            }
            else if (cartData[i].isUpdateUserCourse() && !cartData[i].isUpdateStartDate())
            {
               ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdateUserCourseTypeNoStartDate");
               ps.setString(1, cartData[i].getCourseType());
               ps.setString(2, userId);
               ps.setString(3, cartData[i].getCourseId());
               ps.setString(4, sdf.format(cartData[i].getEndDate()));
               int rows = ps.executeUpdate();
               if (logger.isDebugEnabled())
               {
                  logger.debug(rows + " rows updated in database while updateing course type for cart for userid " + userId);
               }
               if (rows == 1)
               {
                  sb.append("|").append(cartData[i].getCourseId()).append(",").append(EzedTransactionCodes.SUCCESS);
                  cartData[i].setStatusCode(EzedTransactionCodes.SUCCESS);
               }
               else
               {
                  response.setResponseCode(EzedTransactionCodes.INTERNAL_ERROR);
                  sb.append("|").append(cartData[i].getCourseId()).append(",").append(EzedTransactionCodes.INTERNAL_ERROR);
                  cartData[i].setStatusCode(EzedTransactionCodes.INTERNAL_ERROR);
               }
            }
            else
            {
               ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertUserCourse");
               ps.setString(1, userId);
               ps.setString(2, cartData[i].getCourseId());
               ps.setString(3, cartData[i].getCourseType());
               ps.setString(4, sdf.format(cartData[i].getStartDate()));
               ps.setString(5, sdf.format(cartData[i].getEndDate()));
               int rows = ps.executeUpdate();
               if (logger.isDebugEnabled())
               {
                  logger.debug(rows + " rows inserted in database while inserting cart for userid " + userId);
               }
               if (rows == 1)
               {
                  sb.append("|").append(cartData[i].getCourseId()).append(",").append(EzedTransactionCodes.SUCCESS);
                  cartData[i].setStatusCode(EzedTransactionCodes.SUCCESS);
               }
               else
               {
                  response.setResponseCode(EzedTransactionCodes.INTERNAL_ERROR);
                  sb.append("|").append(cartData[i].getCourseId()).append(",").append(EzedTransactionCodes.INTERNAL_ERROR);
                  cartData[i].setStatusCode(EzedTransactionCodes.INTERNAL_ERROR);
               }
            }
            //added by shamila
            if(cartData[i].getStatusCode() == EzedTransactionCodes.SUCCESS){
            	//userid,courseid,coursetype,createtime,startdate,enddate,paymentid,appid
	            ps1 = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertUserCoursePaymentMapping");
	            ps1.setString(1, userId);
	            ps1.setString(2, cartData[i].getCourseId());
	            ps1.setString(3, cartData[i].getCourseType());
	            ps1.setString(4, sdf.format(new Date()));
	            ps1.setString(5, sdf.format(cartData[i].getStartDate()));
	            ps1.setString(6, sdf.format(cartData[i].getEndDate()));
	            ps1.setString(7, cartObject.getPaymentId());
	            ps1.setString(8, cartObject.getAppId());
	            int rows = ps1.executeUpdate();
	               if (logger.isDebugEnabled())
	               {
	                  logger.debug(rows + " rows inserted in database while inserting cart for userid " + userId);
	               }
	               if (rows == 1)
	               {
	                  sb.append("|").append(cartData[i].getCourseId()).append(",").append(EzedTransactionCodes.SUCCESS);
	                  cartData[i].setStatusCode(EzedTransactionCodes.SUCCESS);
	               }
	               else
	               {
	                  response.setResponseCode(EzedTransactionCodes.INTERNAL_ERROR);
	                  sb.append("|").append(cartData[i].getCourseId()).append(",").append(EzedTransactionCodes.INTERNAL_ERROR);
	                  cartData[i].setStatusCode(EzedTransactionCodes.INTERNAL_ERROR);
	               }
            }
         }
         catch (SQLException e)
         {
            logger.error("Error while inserting in user course details", e);
            response.setResponseCode(EzedTransactionCodes.DB_EXCEPTION_OCCURRED);
            sb.append("|").append(cartData[i].getCourseId()).append(",").append(EzedTransactionCodes.DB_EXCEPTION_OCCURRED);
            cartData[i].setStatusCode(EzedTransactionCodes.DB_EXCEPTION_OCCURRED);
            PaymentLockManager.removeFromList(cartObject.getAppId() + cartObject.getPaymentId());
            return RuleDecisionKey.ON_FAILURE;
         }
         catch (Exception e)
         {
            logger.error("Error while inserting in user course details", e);
            response.setResponseCode(EzedTransactionCodes.INTERNAL_ERROR);
            sb.append("|").append(cartData[i].getCourseId()).append(",").append(EzedTransactionCodes.INTERNAL_ERROR);
            cartData[i].setStatusCode(EzedTransactionCodes.INTERNAL_ERROR);
            PaymentLockManager.removeFromList(cartObject.getAppId() + cartObject.getPaymentId());
            return RuleDecisionKey.ON_FAILURE;
         }
         finally
         {
            DbConnectionManager.releaseConnection(dbConnName, conn);
            response.setResponseMessage(sb.toString());
            PurchasedCartUpdater.logFailedCartProcessing(cartLogger, userId, cartDetails, response.getResponseCode(), response.getResponseMessage());
         }
      }
      return RuleDecisionKey.ON_SUCCESS;
   }
}
