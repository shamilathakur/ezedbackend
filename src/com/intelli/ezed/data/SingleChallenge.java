package com.intelli.ezed.data;

import java.text.SimpleDateFormat;
import java.util.Date;

public class SingleChallenge extends ResponseObject
{
   private static final SimpleDateFormat sdf = new SimpleDateFormat("ddMMMMyyyy");
   private String challengedBy;
   private String testId;
   private String courseId;
   private Date challengeDate;
   private String testName;
   private String challengeId;

   public String getChallengedBy()
   {
      return challengedBy;
   }

   public void setChallengedBy(String challengedBy)
   {
      this.challengedBy = challengedBy;
   }

   public String getTestId()
   {
      return testId;
   }

   public void setTestId(String testId)
   {
      this.testId = testId;
   }

   public String getCourseId()
   {
      return courseId;
   }

   public void setCourseId(String courseId)
   {
      this.courseId = courseId;
   }

   public Date getChallengeDate()
   {
      return challengeDate;
   }

   public void setChallengeDate(Date challengeDate)
   {
      this.challengeDate = challengeDate;
   }

   public String getTestName()
   {
      return testName;
   }

   public void setTestName(String testName)
   {
      this.testName = testName;
   }

   public String getChallengeId()
   {
      return challengeId;
   }

   public void setChallengeId(String challengeId)
   {
      this.challengeId = challengeId;
   }

   @Override
   public void prepareResponse() throws Exception
   {
      this.put("challengeid", getChallengeId());
      this.put("testid", getTestId());
      this.put("testname", getTestName());
      this.put("challengedby", getChallengedBy());
      this.put("challengedate", sdf.format(getChallengeDate()));
      this.put("courseid", getCourseId());
   }
}
