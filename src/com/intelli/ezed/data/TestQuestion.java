package com.intelli.ezed.data;

import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Map;

import com.intelli.ezed.utils.Randomizer;

public abstract class TestQuestion
{
   private int difficultyLevel;
   private String questionid;
   private QuestionContent explanation;
   public abstract void persistQuestion(PreparedStatement ps, int startPoint) throws Exception;
   public abstract ArrayList<TestQuestion> getPermutedQuestions(int noOfRandomQuestions, Randomizer randomizer);
   public abstract int getNoOfAnswers();
   public abstract void persistForBulkUpload(PreparedStatement ps, int startPoint) throws Exception;
   public abstract void replaceContentIds(Map<String, String> contentDirIdMap) throws Exception;
   public void setDifficultyLevel(String difficultyLevel)
   {
      try
      {
         setDifficultyLevel(Integer.parseInt(difficultyLevel));
      }
      catch (Exception e)
      {
         setDifficultyLevel(1);
      }
   }
   public void setDifficultyLevel(int difficultyLevel)
   {
      this.difficultyLevel = difficultyLevel;
   }
   public int getDifficultyLevel()
   {
      return this.difficultyLevel;
   }
   public String getQuestionid()
   {
      return questionid;
   }
   public void setQuestionid(String questionid)
   {
      this.questionid = questionid;
   }
   public QuestionContent getExplanation()
   {
      return explanation;
   }
   public void setExplanation(QuestionContent explanation)
   {
      this.explanation = explanation;
   }
   public abstract void writeToFile(FileOutputStream fos) throws IOException;
}
