package com.intelli.ezed.db;

import java.sql.SQLException;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.AdminProfile;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.NewGroupData;
import com.intelli.ezed.data.Response;

public class CreateGroupRequestHandler extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;

   public CreateGroupRequestHandler(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      AdminProfile profile = (AdminProfile) flowContext.getFromContext(EzedKeyConstants.ADMIN_PROFILE);

      String groupName = messageData.getParameter("groupname");
      if (groupName == null || groupName.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("group name is invalid");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid group name");
         return RuleDecisionKey.ON_FAILURE;
      }

      String courseId = messageData.getParameter("courseid");
      if (courseId == null || courseId.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("course id of group is invalid " + courseId);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid Course");
         return RuleDecisionKey.ON_FAILURE;
      }

      String groupAdminId = messageData.getParameter("groupadmin");
      if (groupAdminId == null || groupAdminId.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Group admin id of group is invalid " + groupAdminId);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid Group admin");
         return RuleDecisionKey.ON_FAILURE;
      }

      String users = messageData.getParameter("users");
      if (users == null || users.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("No user to be added to group");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Users to be added are invalid");
         return RuleDecisionKey.ON_FAILURE;
      }
      NewGroupData groupData = new NewGroupData();
      groupData.setNewGroupId();
      groupData.setAdminId(groupAdminId);
      groupData.setGroupName(groupName);
      groupData.setCourseId(courseId);
      groupData.setMicAdminId(profile.getUserid());
      try
      {
         groupData.setUserString(users);
      }
      catch (Exception e)
      {
         logger.error("Error while handling user CSV", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, e.getMessage());
         return RuleDecisionKey.ON_FAILURE;
      }
      response.setResponseObject(groupData);
      try
      {
         groupData.persistGroup(dbConnName, logger);
         groupData.persistGroupUsers(dbConnName, logger);
         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
         return RuleDecisionKey.ON_SUCCESS;
      }
      catch (SQLException e)
      {
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while inserting group");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while inserting group");
         return RuleDecisionKey.ON_FAILURE;
      }
   }
}
