package com.intelli.ezed.data;

import java.util.ArrayList;

/*
 * This class is used for pagination of course list 
 */
public class CourseListForPaging  extends ResponseObject{

	private ArrayList<CourseListData> courseListData ;
	private int offset;
	private int limit;
    private int pageno;
	@Override
	public void prepareResponse() throws Exception {

		CourseListData[] courseListData = new CourseListData[getCourseListData().size()];
		if(courseListData != null ){
			for(int i=0;i<courseListData.length; i++)
			{
				courseListData[i] = getCourseListData().get(i);
			}
		}
		this.put("contentDataForChapter", courseListData);
		this.put("offset", getOffset());
		this.put("limit", getLimit());
		this.put("pageno", getPageno());
	}

	/**
	 * @return the courseListData
	 */
	public ArrayList<CourseListData> getCourseListData() {
		return courseListData;
	}

	/**
	 * @param courseListData the courseListData to set
	 */
	public void setCourseListData(ArrayList<CourseListData> courseListData) {
		this.courseListData = courseListData;
	}

	/**
	 * @return the offset
	 */
	public int getOffset() {
		return offset;
	}

	/**
	 * @param offset the offset to set
	 */
	public void setOffset(int offset) {
		this.offset = offset;
	}

	/**
	 * @return the limit
	 */
	public int getLimit() {
		return limit;
	}

	/**
	 * @param limit the limit to set
	 */
	public void setLimit(int limit) {
		this.limit = limit;
	}

	/**
	 * @return the pageno
	 */
	public int getPageno() {
		return pageno;
	}

	/**
	 * @param pageno the pageno to set
	 */
	public void setPageno(int pageno) {
		this.pageno = pageno;
	}

}
