package com.intelli.ezed.rules;

import java.util.HashMap;

import org.sabre.io.transport.http.data.HttpTransactionData;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;

public class EmailRequestConstructor extends SingletonRule
{

   public EmailRequestConstructor(String ruleName)
   {
      super(ruleName);
      // TODO Auto-generated constructor stub
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      HttpTransactionData transData = new HttpTransactionData();
      transData.setRequestType(HttpTransactionData.POST);
      transData.setParaMap(new HashMap<String, String>());
      transData.addParam("emailid", "d.indraneeldey@gmail.com");
      transData.addParam("message", "test email from EzEd");
      transData.addParam("emailtype", "EXPIREDGOALS");

      flowContext.putIntoContext(HttpTransactionData.WRITE_REQ_DATA, transData);
      return RuleDecisionKey.ON_SUCCESS;
   }

}
