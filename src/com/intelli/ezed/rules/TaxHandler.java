package com.intelli.ezed.rules;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.PaymentIdData;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.dropdowns.data.DropDownListData;
import com.intelli.ezed.dropdowns.data.SingleDropDownData;

public class TaxHandler extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private float cGST;
   private float sGST;

   public TaxHandler(String ruleName, String cGST, String sGST)
   {
      super(ruleName);
      this.cGST = Float.parseFloat(cGST);
      this.sGST = Float.parseFloat(sGST);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Integer totalAmt = (Integer) flowContext.getFromContext(EzedKeyConstants.TOTAL_CART_AMOUNT);
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      PaymentIdData payData = (PaymentIdData) response.getResponseObject();
      Float newAmt = payData.getNewAmount();

      DropDownListData data = new DropDownListData();
      data.setName("values");
      SingleDropDownData singleData = null;

      float amount = totalAmt;
      if (newAmt != 0 && newAmt < totalAmt)
      {
         amount = newAmt;
         singleData = new SingleDropDownData();
         singleData.setParam("Discount coupon application");
         singleData.setValueInt((float) newAmt - totalAmt);
         flowContext.putIntoContext(EzedKeyConstants.DISCOUNT_DB_AMT, singleData.getValueInt());
         data.getDropDownDatas().add(singleData);
      }

      float amountFloat = (float) amount;
      float cGSTAmt = amountFloat * 0.01f * cGST;
      float sGSTAmt = amountFloat * 0.01f * sGST;
     

      float cGSTInt = (float) Math.round(cGSTAmt * 100) / 100;
      float sGSTInt = (float) Math.round(sGSTAmt * 100) / 100;

      singleData = new SingleDropDownData();
      singleData.setParam("CGST @" + cGST + "%");
      singleData.setValueInt(cGSTInt);
      flowContext.putIntoContext(EzedKeyConstants.CGST_DB_AMT, singleData.getValueInt());
      data.getDropDownDatas().add(singleData);

      singleData = new SingleDropDownData();
      singleData.setParam("SGST @" + sGST + "%");
      singleData.setParam("SGST @" + sGST + "%");
      singleData.setValueInt(sGSTInt);
      flowContext.putIntoContext(EzedKeyConstants.SGST_DB_AMT, singleData.getValueInt());
      data.getDropDownDatas().add(singleData);

   

      float totalAmountPostTax = amount + cGSTInt + sGSTInt;
     // float totalAmountPostTax = amount + serviceTaxInt ;
      if (logger.isDebugEnabled())
      {
         logger.debug("Total amount : " + amount + " service tax amount " + cGSTInt + " educess " + sGSTInt );
      }
      payData.setNewAmount((float) Math.round(totalAmountPostTax * 100) / 100);
      payData.setList(data);
      return RuleDecisionKey.ON_SUCCESS;
   }

   public static void main(String[] args)
   {
      System.out.println((float) Math.round(23.33333f * 100) / 100);
   }
}
