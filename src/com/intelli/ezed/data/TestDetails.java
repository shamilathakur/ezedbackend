package com.intelli.ezed.data;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TestDetails
{

   public static final String TEST_DETAILS = "TestDetails";

   private String testid;
   private String userid;
   private String testTimeLimit;
   private String testStartTime;
   private String testEndTime;
   private int numberOfQuestions;
   private String chapterId;
   private String courseId;
   private String userAns;

   private int difficultyLevel;
   private int currentQuestionNumber = 0;
   private String lastQuestionCorrectAns;
   private String testName;

   private String challengeId;

   private int correctAnsGiven;

   private TestRequestResponse sentQuestionDetails;

   public boolean isTimeEnd()
   {
      SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
      try
      {
         Date d = df.parse(getTestEndTime());
         Long endTime = d.getTime();
         Long currentTime = System.currentTimeMillis();
         if (endTime < currentTime)
         {
            return true;
         }
      }
      catch (ParseException e)
      {
         return true;
      }
      return false;
   }
   
   public Long getTestDuration()
   {
      SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
      try
      {
         Date startDate = df.parse(getTestStartTime());
         Long startMillis = startDate.getTime();
         Long currentMillis = System.currentTimeMillis();
         return currentMillis - startMillis;
      }
      catch (ParseException e)
      {
         return 0l;
      }
   }

   public String getTestName()
   {
      return testName;
   }

   public void setTestName(String testName)
   {
      this.testName = testName;
   }

   public int getCorrectAnsGiven()
   {
      return correctAnsGiven;
   }

   public void setCorrectAnsGiven(int correctAnsGiven)
   {
      this.correctAnsGiven = correctAnsGiven;
   }

   public String getUserAns()
   {
      return userAns;
   }

   public String getChallengeId()
   {
      return challengeId;
   }

   public void setChallengeId(String challengeId)
   {
      this.challengeId = challengeId;
   }

   public void setUserAns(String userAns)
   {
      if (userAns != null)
      {
         this.userAns = userAns.replaceAll(" ", "");
      }
   }

   public int getNextQuestionNumber()
   {
      int i = getCurrentQuestionNumber() + 1;
      if (getNumberOfQuestions() < i)
      {
         return 0;
      }
      setCurrentQuestionNumber(i);
      return i;
   }

   public void decreaseDifficultyLevel()
   {
      setDifficultyLevel(getDifficultyLevel() - 1);
   }

   public void increaseDifficultyLevel()
   {
      setDifficultyLevel(getDifficultyLevel() + 1);
   }

   public TestRequestResponse getSentQuestionDetails()
   {
      return sentQuestionDetails;
   }
   public void setSentQuestionDetails(TestRequestResponse sentQuestionDetails)
   {
      this.sentQuestionDetails = sentQuestionDetails;
   }
   public String getLastQuestionCorrectAns()
   {
      return lastQuestionCorrectAns;
   }
   public void setLastQuestionCorrectAns(String lastQuestionCorrectAns)
   {
      if (lastQuestionCorrectAns != null)
      {
         this.lastQuestionCorrectAns = lastQuestionCorrectAns.replaceAll(" ", "");
      }
   }
   public int getCurrentQuestionNumber()
   {
      return currentQuestionNumber;
   }
   public void setCurrentQuestionNumber(int currentQuestionNumber)
   {
      this.currentQuestionNumber = currentQuestionNumber;
   }
   public int getDifficultyLevel()
   {
      return difficultyLevel;
   }
   public void setDifficultyLevel(int difficultyLevel)
   {
      this.difficultyLevel = difficultyLevel;
   }
   public String getChapterId()
   {
      return chapterId;
   }
   public void setChapterId(String chapterId)
   {
      this.chapterId = chapterId;
   }
   public String getCourseId()
   {
      return courseId;
   }
   public void setCourseId(String courseId)
   {
      this.courseId = courseId;
   }
   public String getTestid()
   {
      return testid;
   }
   public void setTestid(String testId)
   {
      this.testid = testId;
   }
   public String getUserid()
   {
      return userid;
   }
   public void setUserid(String userid)
   {
      this.userid = userid;
   }
   public String getTestTimeLimit()
   {
      return testTimeLimit;
   }
   public void setTestTimeLimit(String testTimeLimit)
   {
      this.testTimeLimit = testTimeLimit;
   }
   public String getTestStartTime()
   {
      return testStartTime;
   }
   public void setTestStartTime(String testStartTime)
   {
      this.testStartTime = testStartTime;
   }
   public String getTestEndTime()
   {
      return testEndTime;
   }
   public void setTestEndTime(String testEndTime)
   {
      this.testEndTime = testEndTime;
   }
   public int getNumberOfQuestions()
   {
      return numberOfQuestions;
   }
   public void setNumberOfQuestions(int numberOfQuestions)
   {
      this.numberOfQuestions = numberOfQuestions;
   }
}
