package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.a.helper.utils.StringSplitter;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.utils.EzedHelper;

public class TestUpdater extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private SimpleDateFormat sdfView = new SimpleDateFormat("HH:mm:ss");
   private SimpleDateFormat sdfDb = new SimpleDateFormat("HHmmss");

   public TestUpdater(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      response.setStringOnlySend(true);

      String newTestValue = messageData.getParameter("value");
      if (newTestValue == null || newTestValue.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("New test value is invalid " + newTestValue);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "");
         return RuleDecisionKey.ON_FAILURE;
      }
      int colNo = 0;
      try
      {
         colNo = Integer.parseInt(messageData.getParameter("columnid"));
      }
      catch (Exception e)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Column no is invalid in request", e);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "");
         return RuleDecisionKey.ON_FAILURE;
      }

      String origRowData = messageData.getParameter("rowdata");
      if (origRowData == null || origRowData.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Orig row data is invalid " + origRowData);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "");
         return RuleDecisionKey.ON_FAILURE;
      }

      String[] origRowStrings = StringSplitter.splitString(origRowData, "~~");
      if (origRowStrings.length < 8)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Orig row data is invalid since its length is not proper " + origRowData);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "");
         return RuleDecisionKey.ON_FAILURE;
      }

      String testId = origRowStrings[7];
      if (testId == null || testId.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Test id in request is not proper " + testId);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "");
         return RuleDecisionKey.ON_FAILURE;
      }

      if (colNo == 2)
      {
         if (!EzedHelper.isNumeric(newTestValue))
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("No of questions to be updated is not proper " + newTestValue);
            }
            response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "");
            return RuleDecisionKey.ON_FAILURE;
         }
         origRowStrings[colNo] = newTestValue;
      }
      else if (colNo == 3)
      {
         String newTestDate = null;
         try
         {
            newTestDate = sdfDb.format(sdfView.parse(newTestValue));
         }
         catch (Exception e)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Test time to be updated is not proper " + newTestValue, e);
            }
            response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "");
            return RuleDecisionKey.ON_FAILURE;
         }
         origRowStrings[colNo] = newTestDate;
      }
      else
      {
         origRowStrings[colNo] = newTestValue;
      }

      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdateTest");
         ps.setString(1, origRowStrings[1]);
         ps.setInt(2, Integer.parseInt(origRowStrings[2]));
         ps.setString(3, origRowStrings[3]);
         ps.setString(4, testId);
         int rows = ps.executeUpdate();
         if (rows > 0)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug(rows + " rows updated in db while updating test name for id " + testId);
            }
            response.setParameters(EzedTransactionCodes.SUCCESS, newTestValue);
            return RuleDecisionKey.ON_SUCCESS;
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Test cannot be updated in request for test id " + testId);
            }
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "");
            return RuleDecisionKey.ON_FAILURE;
         }
      }
      catch (SQLException e)
      {
         logger.error("DB error while updating test value for id " + testId, e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while updating test value for id " + testId, e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
}
