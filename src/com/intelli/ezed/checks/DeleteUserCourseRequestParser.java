package com.intelli.ezed.checks;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.ManageUserCourseFromBackendBean;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class DeleteUserCourseRequestParser extends SingletonRule {

	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

	public DeleteUserCourseRequestParser(String ruleName) {
		super(ruleName);
	}

	@Override
	public RuleDecisionKey executeRule(FlowContext flowContext) {
		MessageData messageData = (MessageData) flowContext
				.getFromContext(MessageData.class.getName());
		Response response = (Response) flowContext
				.getFromContext(EzedKeyConstants.EZED_RESPONSE);
		ManageUserCourseFromBackendBean data = new ManageUserCourseFromBackendBean();
		// Bean originally added for adding course from the backend ..
		// Just being reused while deleting the course ..

		String userid = messageData.getParameter("userid");
		if (userid == null || userid.trim().compareTo("") == 0
				|| userid.trim().compareToIgnoreCase("null") == 0) {
			if (logger.isDebugEnabled()) {
				logger.debug("User id is invalid");
			}
			response.setParameters(
					EzedTransactionCodes.INVALID_MESSAGE_PARAMETER,
					"User id is invalid");
			return RuleDecisionKey.ON_FAILURE;
		}
		flowContext.putIntoContext(EzedKeyConstants.USER_ID, userid);
		data.setUserId(userid);

		String courseid = messageData.getParameter("courseid");
		if (courseid == null || courseid.trim().compareTo("") == 0
				|| courseid.trim().compareToIgnoreCase("null") == 0) {
			if (logger.isDebugEnabled()) {
				logger.debug("Course Id is invalid");
			}
			response.setParameters(
					EzedTransactionCodes.INVALID_MESSAGE_PARAMETER,
					"Course ID is invalid");
			return RuleDecisionKey.ON_FAILURE;
		}
		data.setCourseId(courseid);

		String coursetype = messageData.getParameter("coursetype");
		if (coursetype == null || coursetype.trim().compareTo("") == 0
				|| coursetype.trim().compareToIgnoreCase("null") == 0) {
			if (logger.isDebugEnabled()) {
				logger.debug("Course type is invalid");
			}
			response.setParameters(
					EzedTransactionCodes.INVALID_MESSAGE_PARAMETER,
					"Course type is invalid");
			return RuleDecisionKey.ON_FAILURE;
		}
		data.setCourseType(coursetype);

		data.setAddedBy(messageData.getParameter("email"));

		flowContext.putIntoContext(
				EzedKeyConstants.ADD_COURSE_TO_USER_FROM_ADMIN, data);
		// same bean being reused .. Originally written for adding course from
		// the backend..

		return RuleDecisionKey.ON_SUCCESS;
	}

}
