package com.intelli.ezed.data;

import java.util.ArrayList;

import org.json.JSONArray;

public class SingleGroupTest extends ResponseObject
{
   private String groupId;
   private String groupName;
   private ArrayList<SingleTestDataForGroup> tests = new ArrayList<SingleTestDataForGroup>();

   public String getGroupId()
   {
      return groupId;
   }

   public void setGroupId(String groupId)
   {
      this.groupId = groupId;
   }

   @Override
   public void prepareResponse() throws Exception
   {
      this.put("groupid", getGroupId());
      this.put("groupname", getGroupName());
      JSONArray jsonArray = new JSONArray();
      for (int i = 0; i < tests.size(); i++)
      {
         tests.get(i).prepareResponse();
         jsonArray.put(tests.get(i));
      }
      this.put("tests", jsonArray);
   }

   public String getGroupName()
   {
      return groupName;
   }

   public void setGroupName(String groupName)
   {
      this.groupName = groupName;
   }

   public ArrayList<SingleTestDataForGroup> getTests()
   {
      return tests;
   }

   public void setTests(ArrayList<SingleTestDataForGroup> tests)
   {
      this.tests = tests;
   }
}
