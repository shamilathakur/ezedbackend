package com.intelli.ezed.sheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

import org.sabre.db.DbConnectionManager;
import org.sabre.utils.threads.TimedTask;
import org.sabre.workflow.context.FlowContext;
import org.sabre.workflow.processor.InputProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.utils.StringSplitter;
import com.intelli.ezed.data.CartData;
import com.intelli.ezed.data.CartDetails;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.PaymentData;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.utils.PaymentLockManager;

public class PurchasedCartUpdater extends InputProcessor
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private Logger cartLog = LoggerFactory.getLogger(EzedLoggerName.CART_LOG);
   private Resetter resetter;
   private FlowContext baseContext;
   private AtomicInteger atomicInteger;
   private Long throttleSleepTime;
   private int throttleParallelRecords;
   private Calendar calendar = Calendar.getInstance();

   public PurchasedCartUpdater(String startAtTime, String pollingTime, String dbConnName, String throttleSleepTime,
                               String throttleParallelRecords)
   {
      atomicInteger = new AtomicInteger(1);
      resetter = new Resetter(startAtTime, Long.parseLong(pollingTime), dbConnName);
      this.throttleSleepTime = Long.parseLong(throttleSleepTime);
      this.throttleParallelRecords = Integer.parseInt(throttleParallelRecords);
   }

   class Resetter extends TimedTask
   {
      private String dbConnName = null;
      public Resetter(String startAtTime, long pollingTime, String dbConnName)
      {
         super(startAtTime, pollingTime);
         this.dbConnName = dbConnName;
         this.setStartAtTime(startAtTime);
         this.setExecAtStartup(true);
         this.setOneTimeTask(false);
      }

      @Override
      public void doTimedTask()
      {
         //Put this in a new thread later so that timed task immediately goes ahead and
         //further runs of timed task don't get delayed
         RemoveInvalidKeys();
      }

      private void RemoveInvalidKeys()
      {
         //select userid,cart from paydata where cartstatus = '1';
         logger.info("Updating paid cart details for user");
         Connection connection = DbConnectionManager.getConnectionByName(dbConnName);
         ResultSet rs = null;
         calendar.setTime(new Date(System.currentTimeMillis()));
         try
         {
            PreparedStatement selectStmt = DbConnectionManager.getPreparedStatement(dbConnName, connection, "SelectPaidCarts");
            selectStmt.setString(1, PaymentData.CART_PAID);
            rs = selectStmt.executeQuery();
            CartDetails cartDetails = new CartDetails();
            String userId = null;
            String cartString = null;
            CartData[] cartData = null;
            FlowContext currentContext = null;
            int count = 0;
            while (rs.next())
            {
               cartDetails = new CartDetails();
               userId = rs.getString(1);
               cartString = rs.getString(2);
               cartDetails.setAppId(rs.getString(3));
               cartDetails.setPaymentId(rs.getString(4));
               cartData = parseCartString(cartString, calendar);
               if (cartData == null)
               {
                  if (logger.isDebugEnabled())
                  {
                     logger.debug("Cart could not be processed for userid " + userId);
                  }
                  logFailedCartProcessing(cartLog, userId, cartString, EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Cart String from DB not proper");
               }
               cartDetails.setCartDatas(cartData);
               currentContext = baseContext.createSubContext(getSubContextName());
               currentContext.putIntoContext(EzedKeyConstants.USER_ID, userId);
               currentContext.putIntoContext(EzedKeyConstants.CART_DETAILS, cartDetails);
               currentContext.putIntoContext(EzedKeyConstants.CART_STRING, cartString);
               currentContext.putIntoContext(EzedKeyConstants.EZED_RESPONSE, new Response());
               PaymentLockManager.addToList(cartDetails.getAppId() + cartDetails.getPaymentId());
               process(currentContext);
               try
               {
                  count++;
                  if (count == throttleParallelRecords)
                  {
                     count = 0;
                     Thread.sleep(throttleSleepTime);
                  }
               }
               catch (Exception e1)
               {
                  logger.error("Exception while sleeping thread", e1);
                  continue;
               }
            }
         }
         catch (Exception e)
         {
            logger.error("Exception occured while fetching paid cart details ", e);
         }
         finally
         {
            DbConnectionManager.releaseConnection(dbConnName, connection);
            if (rs != null)
            {
               try
               {
                  rs.close();
               }
               catch (SQLException e)
               {
                  logger.error("Error while closing resultset", e);
               }
            }
         }
      }
   }

   @Override
   public void initialize(FlowContext flowContext)
   {
      this.baseContext = flowContext;
   }

   @Override
   public void start()
   {
      resetter.startTask(true);
   }

   private CartData[] parseCartString(String cartString, Calendar todayCal)
   {
      SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
      Date courseStartDate = null;
      Date courseEndDate = null;
      if (cartString == null || cartString.trim().compareTo("") == 0)
      {
         return null;
      }
      String[] oneCartDetails = StringSplitter.splitString(cartString, ";");
      CartData[] cartData = new CartData[oneCartDetails.length];
      String[] cartConstructor = null;
      for (int i = 0; i < oneCartDetails.length; i++)
      {
         cartConstructor = StringSplitter.splitString(oneCartDetails[i], ",");
         if (cartConstructor == null || cartConstructor.length != 4)
         {
            return null;
         }
         try
         {
            courseStartDate = sdf.parse(cartConstructor[2]);
            Calendar startCal = Calendar.getInstance();
            startCal.setTime(courseStartDate);
            if (startCal.before(todayCal))
            {
               courseStartDate = todayCal.getTime();
            }

            courseEndDate = sdf.parse(cartConstructor[3]);
         }
         catch (ParseException e)
         {
            logger.error("Error while parsing start/end date from cart string", e);
            return null;
         }
         cartData[i] = new CartData(cartConstructor[0], cartConstructor[1], courseStartDate, courseEndDate);
      }
      return cartData;
   }

   public static void logFailedCartProcessing(Logger logger, String userId, String cartString, String responseCode, String responseString)
   {
      if (logger.isDebugEnabled())
      {
         logger.debug(userId + "|" + cartString + "|" + responseCode + "|" + responseString);
      }
   }

   public static void logFailedCartProcessing(Logger logger, String userId, String cartString, int responseCode, String responseString)
   {
      logFailedCartProcessing(logger, userId, cartString, Integer.toString(responseCode), responseString);
   }

   public String getSubContextName()
   {
      return "PurchasedCartUpdater" + atomicInteger.getAndIncrement();
   }
}
