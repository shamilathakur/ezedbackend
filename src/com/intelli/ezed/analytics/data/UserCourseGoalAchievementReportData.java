package com.intelli.ezed.analytics.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.slf4j.Logger;

import com.intelli.ezed.data.ResponseObject;

public class UserCourseGoalAchievementReportData extends ResponseObject
{
   private String userId;
   private String courseId;
   private String courseName;

   private int currentGoalAchieved;
   private int dayBeforeGoalAchieved;
   private int dayToDayBeforeGoalAchieved;
   private int weekBeforeGoalAchieved;
   private int weekToWeekBeforeGoalAchieved;
   private int monthBeforeGoalAchieved;
   private int monthToMonthBeforeGoalAchieved;
   private int sixMonthsBeforeGoalAchieved;
   private int yearBeforeGoalAchieved;

   private int goalsAchievedToday;
   private int goalsAchievedYesterday;
   private int goalsAchievedThisWeek;
   private int goalsAchievedLastWeek;
   private int goalsAchievedThisMonth;
   private int goalsAchievedLastMonth;
   private int goalsAchievedThisHalfYear;
   private int goalsAchievedLastHalfYear;

   private int currentGoalFailed;
   private int dayBeforeGoalFailed;
   private int dayToDayBeforeGoalFailed;
   private int weekBeforeGoalFailed;
   private int weekToWeekBeforeGoalFailed;
   private int monthBeforeGoalFailed;
   private int monthToMonthBeforeGoalFailed;
   private int sixMonthsBeforeGoalFailed;
   private int yearBeforeGoalFailed;

   private int goalsFailedToday;
   private int goalsFailedYesterday;
   private int goalsFailedThisWeek;
   private int goalsFailedLastWeek;
   private int goalsFailedThisMonth;
   private int goalsFailedLastMonth;
   private int goalsFailedThisHalfYear;
   private int goalsFailedLastHalfYear;

   public void calculateCourseHistoryPoints(String dbConnName, Logger logger) throws SQLException, Exception
   {
      this.fetchCurrentCoursePointsForUser(dbConnName, logger);
      this.fetchAllHistoryPointsForUserCourse(dbConnName, logger);

      this.setGoalsAchievedToday(getCurrentGoalAchieved() - getDayBeforeGoalAchieved());
      this.setGoalsAchievedYesterday(getDayBeforeGoalAchieved() - getDayToDayBeforeGoalAchieved());
      this.setGoalsAchievedThisWeek(getCurrentGoalAchieved() - getWeekBeforeGoalAchieved());
      this.setGoalsAchievedLastWeek(getWeekBeforeGoalAchieved() - getWeekToWeekBeforeGoalAchieved());
      this.setGoalsAchievedThisMonth(getCurrentGoalAchieved() - getMonthBeforeGoalAchieved());
      this.setGoalsAchievedLastMonth(getMonthBeforeGoalAchieved() - getMonthToMonthBeforeGoalAchieved());
      this.setGoalsAchievedThisHalfYear(getCurrentGoalAchieved() - getSixMonthsBeforeGoalAchieved());
      this.setGoalsAchievedLastHalfYear(getSixMonthsBeforeGoalAchieved() - getYearBeforeGoalAchieved());

      this.setGoalsFailedToday(getCurrentGoalFailed() - getDayBeforeGoalFailed());
      this.setGoalsFailedYesterday(getDayBeforeGoalFailed() - getDayToDayBeforeGoalFailed());
      this.setGoalsFailedThisWeek(getCurrentGoalFailed() - getWeekBeforeGoalFailed());
      this.setGoalsFailedLastWeek(getWeekBeforeGoalFailed() - getWeekToWeekBeforeGoalFailed());
      this.setGoalsFailedThisMonth(getCurrentGoalFailed() - getMonthBeforeGoalFailed());
      this.setGoalsFailedLastMonth(getMonthBeforeGoalFailed() - getMonthToMonthBeforeGoalFailed());
      this.setGoalsFailedThisHalfYear(getCurrentGoalFailed() - getSixMonthsBeforeGoalFailed());
      this.setGoalsFailedLastHalfYear(getSixMonthsBeforeGoalFailed() - getYearBeforeGoalFailed());
   }

   private void fetchCurrentCoursePointsForUser(String dbConnName, Logger logger) throws SQLException, Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchCourseDetailsForCourseGoal");
         ps.setString(1, getUserId());
         ps.setString(2, getCourseId());
         ps.setString(3, getCourseId());
         rs = ps.executeQuery();
         while (rs.next())
         {
            this.setCourseName(rs.getString("coursename"));
            this.setCurrentGoalAchieved(rs.getInt("goalsachieved"));
            this.setCurrentGoalFailed(rs.getInt("goalsfailed"));
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching user courses for userid " + getUserId(), e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching user courses for userid " + getUserId(), e);
         throw e;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
               logger.error("Error while closing result set while fetching courses of user");
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   private void fetchAllHistoryPointsForUserCourse(String dbConnName, Logger logger) throws SQLException, Exception
   {
      setDayBeforeGoal(fetchGoalCountFromUserCourse(dbConnName, "FetchYesterdayCourseGoalsAchieved", logger));
      setDayToDayBeforeGoal(fetchGoalCountFromUserCourse(dbConnName, "FetchDayBeforeYesterdayCourseGoalsAchieved", logger));
      setWeekBeforeGoal(fetchGoalCountFromUserCourse(dbConnName, "FetchLastWeekCourseGoalsAchieved", logger));
      setWeekToWeekBeforeGoal(fetchGoalCountFromUserCourse(dbConnName, "FetchLastFortnightCourseGoalsAchieved", logger));
      setMonthBeforeGoal(fetchGoalCountFromUserCourse(dbConnName, "FetchLastMonthCourseGoalsAchieved", logger));
      setMonthToMonthBeforeGoal(fetchGoalCountFromUserCourse(dbConnName, "FetchLastTwoMonthCourseGoalsAchieved", logger));
      setSixMonthsBeforeGoal(fetchGoalCountFromUserCourse(dbConnName, "FetchLastSixMonthCourseGoalsAchieved", logger));
      setYearBeforeGoal(fetchGoalCountFromUserCourse(dbConnName, "FetchLastYearCourseGoalsAchieved", logger));
   }

   private int[] fetchGoalCountFromUserCourse(String dbConnName, String prepStmtName, Logger logger) throws SQLException, Exception
   {
      int[] values = new int[2];
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, prepStmtName);
         ps.setString(1, getUserId());
         ps.setString(2, getCourseId());
         rs = ps.executeQuery();
         if (rs.next())
         {
            values[0] = rs.getInt(1);
            values[1] = rs.getInt(2);
            return values;
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("No entry found for user in history table for prep stmt name " + prepStmtName);
            }
            values[0] = 0;
            values[1] = 0;
            return values;
         }

      }
      catch (SQLException e)
      {
         logger.error("Error while fetching goal count from one of users history tables for prep stmt name " + prepStmtName, e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching goal count from one of users history tables for prep stmt name " + prepStmtName, e);
         throw e;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
               logger.error("Error while closing result set while fetching points from one of users table");
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   @Override
   public void prepareResponse() throws Exception
   {
      this.put("courseid", getCourseId());
      this.put("coursename", getCourseName());

      this.put("goalsachievedtoday", getGoalsAchievedToday());
      this.put("goalsachievedyesterday", getGoalsAchievedYesterday());
      this.put("goalsachievedthisweek", getGoalsAchievedThisWeek());
      this.put("goalsachievedlastweek", getGoalsAchievedLastWeek());
      this.put("goalsachievedthismonth", getGoalsAchievedThisMonth());
      this.put("goalsachievedlastmonth", getGoalsAchievedLastMonth());
      this.put("goalsachievedthishalfyear", getGoalsAchievedThisHalfYear());
      this.put("goalsachievedlasthalfyear", getGoalsAchievedLastHalfYear());

      this.put("goalsfailedtoday", getGoalsFailedToday());
      this.put("goalsfailedyesterday", getGoalsFailedYesterday());
      this.put("goalsfailedthisweek", getGoalsFailedThisWeek());
      this.put("goalsfailedlastweek", getGoalsFailedLastWeek());
      this.put("goalsfailedthismonth", getGoalsFailedThisMonth());
      this.put("goalsfailedlastmonth", getGoalsFailedLastMonth());
      this.put("goalsfailedthishalfyear", getGoalsFailedThisHalfYear());
      this.put("goalsfailedlasthalfyear", getGoalsFailedLastHalfYear());
   }

   public String getUserId()
   {
      return userId;
   }

   public void setUserId(String userId)
   {
      this.userId = userId;
   }

   public String getCourseId()
   {
      return courseId;
   }

   public void setCourseId(String courseId)
   {
      this.courseId = courseId;
   }

   public String getCourseName()
   {
      return courseName;
   }

   public void setCourseName(String courseName)
   {
      this.courseName = courseName;
   }

   public int getCurrentGoalAchieved()
   {
      return currentGoalAchieved;
   }

   public void setCurrentGoalAchieved(int currentGoalAchieved)
   {
      this.currentGoalAchieved = currentGoalAchieved;
   }

   public int getDayBeforeGoalAchieved()
   {
      return dayBeforeGoalAchieved;
   }

   public void setDayBeforeGoalAchieved(int dayBeforeGoalAchieved)
   {
      this.dayBeforeGoalAchieved = dayBeforeGoalAchieved;
   }

   public int getDayToDayBeforeGoalAchieved()
   {
      return dayToDayBeforeGoalAchieved;
   }

   public void setDayToDayBeforeGoalAchieved(int dayToDayBeforeGoalAchieved)
   {
      this.dayToDayBeforeGoalAchieved = dayToDayBeforeGoalAchieved;
   }

   public int getWeekBeforeGoalAchieved()
   {
      return weekBeforeGoalAchieved;
   }

   public void setWeekBeforeGoalAchieved(int weekBeforeGoalAchieved)
   {
      this.weekBeforeGoalAchieved = weekBeforeGoalAchieved;
   }

   public int getWeekToWeekBeforeGoalAchieved()
   {
      return weekToWeekBeforeGoalAchieved;
   }

   public void setWeekToWeekBeforeGoalAchieved(int weekToWeekBeforeGoalAchieved)
   {
      this.weekToWeekBeforeGoalAchieved = weekToWeekBeforeGoalAchieved;
   }

   public int getMonthBeforeGoalAchieved()
   {
      return monthBeforeGoalAchieved;
   }

   public void setMonthBeforeGoalAchieved(int monthBeforeGoalAchieved)
   {
      this.monthBeforeGoalAchieved = monthBeforeGoalAchieved;
   }

   public int getMonthToMonthBeforeGoalAchieved()
   {
      return monthToMonthBeforeGoalAchieved;
   }

   public void setMonthToMonthBeforeGoalAchieved(int monthToMonthBeforeGoalAchieved)
   {
      this.monthToMonthBeforeGoalAchieved = monthToMonthBeforeGoalAchieved;
   }

   public int getSixMonthsBeforeGoalAchieved()
   {
      return sixMonthsBeforeGoalAchieved;
   }

   public void setSixMonthsBeforeGoalAchieved(int sixMonthsBeforeGoalAchieved)
   {
      this.sixMonthsBeforeGoalAchieved = sixMonthsBeforeGoalAchieved;
   }

   public int getYearBeforeGoalAchieved()
   {
      return yearBeforeGoalAchieved;
   }

   public void setYearBeforeGoalAchieved(int yearBeforeGoalAchieved)
   {
      this.yearBeforeGoalAchieved = yearBeforeGoalAchieved;
   }

   public int getGoalsAchievedToday()
   {
      return goalsAchievedToday;
   }

   public void setGoalsAchievedToday(int goalsAchievedToday)
   {
      this.goalsAchievedToday = goalsAchievedToday;
   }

   public int getGoalsAchievedYesterday()
   {
      return goalsAchievedYesterday;
   }

   public void setGoalsAchievedYesterday(int goalsAchievedYesterday)
   {
      this.goalsAchievedYesterday = goalsAchievedYesterday;
   }

   public int getGoalsAchievedThisWeek()
   {
      return goalsAchievedThisWeek;
   }

   public void setGoalsAchievedThisWeek(int goalsAchievedThisWeek)
   {
      this.goalsAchievedThisWeek = goalsAchievedThisWeek;
   }

   public int getGoalsAchievedLastWeek()
   {
      return goalsAchievedLastWeek;
   }

   public void setGoalsAchievedLastWeek(int goalsAchievedLastWeek)
   {
      this.goalsAchievedLastWeek = goalsAchievedLastWeek;
   }

   public int getGoalsAchievedThisMonth()
   {
      return goalsAchievedThisMonth;
   }

   public void setGoalsAchievedThisMonth(int goalsAchievedThisMonth)
   {
      this.goalsAchievedThisMonth = goalsAchievedThisMonth;
   }

   public int getGoalsAchievedLastMonth()
   {
      return goalsAchievedLastMonth;
   }

   public void setGoalsAchievedLastMonth(int goalsAchievedLastMonth)
   {
      this.goalsAchievedLastMonth = goalsAchievedLastMonth;
   }

   public int getGoalsAchievedThisHalfYear()
   {
      return goalsAchievedThisHalfYear;
   }

   public void setGoalsAchievedThisHalfYear(int goalsAchievedThisHalfYear)
   {
      this.goalsAchievedThisHalfYear = goalsAchievedThisHalfYear;
   }

   public int getGoalsAchievedLastHalfYear()
   {
      return goalsAchievedLastHalfYear;
   }

   public void setGoalsAchievedLastHalfYear(int goalsAchievedLastHalfYear)
   {
      this.goalsAchievedLastHalfYear = goalsAchievedLastHalfYear;
   }

   public int getCurrentGoalFailed()
   {
      return currentGoalFailed;
   }

   public void setCurrentGoalFailed(int currentGoalFailed)
   {
      this.currentGoalFailed = currentGoalFailed;
   }

   public int getDayBeforeGoalFailed()
   {
      return dayBeforeGoalFailed;
   }

   public void setDayBeforeGoalFailed(int dayBeforeGoalFailed)
   {
      this.dayBeforeGoalFailed = dayBeforeGoalFailed;
   }

   public int getDayToDayBeforeGoalFailed()
   {
      return dayToDayBeforeGoalFailed;
   }

   public void setDayToDayBeforeGoalFailed(int dayToDayBeforeGoalFailed)
   {
      this.dayToDayBeforeGoalFailed = dayToDayBeforeGoalFailed;
   }

   public int getWeekBeforeGoalFailed()
   {
      return weekBeforeGoalFailed;
   }

   public void setWeekBeforeGoalFailed(int weekBeforeGoalFailed)
   {
      this.weekBeforeGoalFailed = weekBeforeGoalFailed;
   }

   public int getWeekToWeekBeforeGoalFailed()
   {
      return weekToWeekBeforeGoalFailed;
   }

   public void setWeekToWeekBeforeGoalFailed(int weekToWeekBeforeGoalFailed)
   {
      this.weekToWeekBeforeGoalFailed = weekToWeekBeforeGoalFailed;
   }

   public int getMonthBeforeGoalFailed()
   {
      return monthBeforeGoalFailed;
   }

   public void setMonthBeforeGoalFailed(int monthBeforeGoalFailed)
   {
      this.monthBeforeGoalFailed = monthBeforeGoalFailed;
   }

   public int getMonthToMonthBeforeGoalFailed()
   {
      return monthToMonthBeforeGoalFailed;
   }

   public void setMonthToMonthBeforeGoalFailed(int monthToMonthBeforeGoalFailed)
   {
      this.monthToMonthBeforeGoalFailed = monthToMonthBeforeGoalFailed;
   }

   public int getSixMonthsBeforeGoalFailed()
   {
      return sixMonthsBeforeGoalFailed;
   }

   public void setSixMonthsBeforeGoalFailed(int sixMonthsBeforeGoalFailed)
   {
      this.sixMonthsBeforeGoalFailed = sixMonthsBeforeGoalFailed;
   }

   public int getYearBeforeGoalFailed()
   {
      return yearBeforeGoalFailed;
   }

   public void setYearBeforeGoalFailed(int yearBeforeGoalFailed)
   {
      this.yearBeforeGoalFailed = yearBeforeGoalFailed;
   }

   public int getGoalsFailedToday()
   {
      return goalsFailedToday;
   }

   public void setGoalsFailedToday(int goalsFailedToday)
   {
      this.goalsFailedToday = goalsFailedToday;
   }

   public int getGoalsFailedYesterday()
   {
      return goalsFailedYesterday;
   }

   public void setGoalsFailedYesterday(int goalsFailedYesterday)
   {
      this.goalsFailedYesterday = goalsFailedYesterday;
   }

   public int getGoalsFailedThisWeek()
   {
      return goalsFailedThisWeek;
   }

   public void setGoalsFailedThisWeek(int goalsFailedThisWeek)
   {
      this.goalsFailedThisWeek = goalsFailedThisWeek;
   }

   public int getGoalsFailedLastWeek()
   {
      return goalsFailedLastWeek;
   }

   public void setGoalsFailedLastWeek(int goalsFailedLastWeek)
   {
      this.goalsFailedLastWeek = goalsFailedLastWeek;
   }

   public int getGoalsFailedThisMonth()
   {
      return goalsFailedThisMonth;
   }

   public void setGoalsFailedThisMonth(int goalsFailedThisMonth)
   {
      this.goalsFailedThisMonth = goalsFailedThisMonth;
   }

   public int getGoalsFailedLastMonth()
   {
      return goalsFailedLastMonth;
   }

   public void setGoalsFailedLastMonth(int goalsFailedLastMonth)
   {
      this.goalsFailedLastMonth = goalsFailedLastMonth;
   }

   public int getGoalsFailedThisHalfYear()
   {
      return goalsFailedThisHalfYear;
   }

   public void setGoalsFailedThisHalfYear(int goalsFailedThisHalfYear)
   {
      this.goalsFailedThisHalfYear = goalsFailedThisHalfYear;
   }

   public int getGoalsFailedLastHalfYear()
   {
      return goalsFailedLastHalfYear;
   }

   public void setGoalsFailedLastHalfYear(int goalsFailedLastHalfYear)
   {
      this.goalsFailedLastHalfYear = goalsFailedLastHalfYear;
   }

   private void setDayBeforeGoal(int[] values)
   {
      this.setDayBeforeGoalAchieved(values[0]);
      this.setDayBeforeGoalFailed(values[1]);
   }

   private void setDayToDayBeforeGoal(int[] values)
   {
      this.setDayToDayBeforeGoalAchieved(values[0]);
      this.setDayToDayBeforeGoalFailed(values[1]);
   }

   private void setWeekBeforeGoal(int[] values)
   {
      this.setWeekBeforeGoalAchieved(values[0]);
      this.setWeekBeforeGoalFailed(values[1]);
   }

   private void setWeekToWeekBeforeGoal(int[] values)
   {
      this.setWeekToWeekBeforeGoalAchieved(values[0]);
      this.setWeekToWeekBeforeGoalFailed(values[1]);
   }

   private void setMonthBeforeGoal(int[] values)
   {
      this.setMonthBeforeGoalAchieved(values[0]);
      this.setMonthBeforeGoalFailed(values[1]);
   }

   private void setMonthToMonthBeforeGoal(int[] values)
   {
      this.setMonthToMonthBeforeGoalAchieved(values[0]);
      this.setMonthToMonthBeforeGoalFailed(values[1]);
   }

   private void setSixMonthsBeforeGoal(int[] values)
   {
      this.setSixMonthsBeforeGoalAchieved(values[0]);
      this.setSixMonthsBeforeGoalFailed(values[1]);
   }

   private void setYearBeforeGoal(int[] values)
   {
      this.setYearBeforeGoalAchieved(values[0]);
      this.setYearBeforeGoalFailed(values[1]);
   }
}
