package com.intelli.ezed.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.slf4j.Logger;

import com.intelli.ezed.data.ScratchCardData;
import com.intelli.ezed.data.ScratchCardErrorLogData;

public class ScratchCardHelper {

	static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

	public static void callInternalError(ScratchCardErrorLogData data,
			String dbConnName, Logger logger) {
		ScratchCardErrorLogData errordata = data;
		errordata.setErrorcode(302);
		errordata.setErrordate(sdf.format(new Date()));
		errordata.setErrordesc("Internal Error Occured");
		callErrorEntry(errordata, dbConnName, logger);
	}

	public static void callDbError(ScratchCardErrorLogData data,
			String dbConnName, Logger logger) {
		ScratchCardErrorLogData errordata = data;
		errordata.setErrorcode(301);
		errordata.setErrordate(sdf.format(new Date()));
		errordata.setErrordesc("Database error");
		callErrorEntry(errordata, dbConnName, logger);
	}

	public static void callInvalidScratchCard(ScratchCardErrorLogData data,
			String dbConnName, Logger logger) {
		ScratchCardErrorLogData errordata = data;
		errordata.setErrorcode(252);
		errordata.setErrordate(sdf.format(new Date()));
		errordata.setErrordesc("Scratchcardid Invalid");
		callErrorEntry(errordata, dbConnName, logger);
	}

	public static void callAlreadyOwnCourse(ScratchCardErrorLogData data,
			String dbConnName, Logger logger) {

		ScratchCardErrorLogData errordata = data;
		errordata.setErrorcode(255);
		errordata.setErrordate(sdf.format(new Date()));
		errordata.setErrordesc("Course already owned");
		callErrorEntry(errordata, dbConnName, logger);
	}

	public static void callUsedScratchCard(ScratchCardErrorLogData data,
			String dbConnName, Logger logger) {
		ScratchCardErrorLogData errordata = data;
		errordata.setErrorcode(253);
		errordata.setErrordate(sdf.format(new Date()));
		errordata.setErrordesc("Scratch card already used");
		callErrorEntry(errordata, dbConnName, logger);
	}

	public static void callExpiredScratchCard(ScratchCardErrorLogData data,
			String dbConnName, Logger logger) {
		ScratchCardErrorLogData errordata = data;
		errordata.setErrorcode(256);
		errordata.setErrordate(sdf.format(new Date()));
		errordata.setErrordesc("Scratch Card Expired");
		callErrorEntry(errordata, dbConnName, logger);
	}

	public static void callInactivatedScratchCard(ScratchCardErrorLogData data,
			String dbConnName, Logger logger) {
		ScratchCardErrorLogData errordata = data;
		errordata.setErrorcode(254);
		errordata.setErrordate(sdf.format(new Date()));
		errordata.setErrordesc("Scratch card Inactivated");
		callErrorEntry(errordata, dbConnName, logger);
	}

	public static void callErrorEntry(ScratchCardErrorLogData data,
			String dbConnName, Logger logger) {
		ScratchCardErrorLogData errordata = data;
		Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
		PreparedStatement ps = null;
		int rs;
		try {
			ps = DbConnectionManager.getPreparedStatement(dbConnName, conn,
					"ScratchCardErrorLogEntry");
			ps.setString(1, errordata.getScratchcardid());
			ps.setString(2, errordata.getUserid());
			ps.setString(3, errordata.getPartnerid());
			ps.setString(4, errordata.getCourseid());
			ps.setString(5, errordata.getErrordesc());
			ps.setString(6, errordata.getErrordate());
			ps.setInt(7, errordata.getErrorcode());
			ps.setString(8, errordata.getClassname());
			System.out.println("Query : " + ps);
			rs = ps.executeUpdate();
			if (rs > 0) {
				if (logger.isDebugEnabled()) {
					logger.info("Error has been recorded successfully for userid : "
							+ errordata.getUserid()
							+ " and for the scratchcardid: "
							+ errordata.getScratchcardid());
				}
			} else {
				if (logger.isDebugEnabled()) {
					logger.info("Something went wrong while recording the Error for userid : "
							+ errordata.getUserid()
							+ " and for the scratchcardid: "
							+ errordata.getScratchcardid());
				}
			}
		} catch (SQLException e) {
			logger.error(
					"SQL error occured while adding 'ScratchCardErrorRecord' for userid : "
							+ errordata.getUserid()
							+ " and for the scratchcardid: "
							+ errordata.getScratchcardid(), e);
		} catch (Exception e) {
			logger.error(
					"Internal error occured while adding 'ScratchCardErrorRecord' for userid : "
							+ errordata.getUserid()
							+ " and for the scratchcardid: "
							+ errordata.getScratchcardid(), e);
		} finally {
			DbConnectionManager.releaseConnection(dbConnName, conn);
		}
	}

}
