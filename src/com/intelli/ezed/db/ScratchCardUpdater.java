package com.intelli.ezed.db;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.ScratchCardData;

public class ScratchCardUpdater extends SingletonRule {

	private String dbConnName;
	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

	public ScratchCardUpdater(String ruleName, String dbConnName) {
		super(ruleName);
		this.dbConnName = dbConnName;
	}

	@Override
	public RuleDecisionKey executeRule(FlowContext flowContext) {
		Response response = (Response) flowContext
				.getFromContext(EzedKeyConstants.EZED_RESPONSE);

		ScratchCardData data = (ScratchCardData) flowContext
				.getFromContext((EzedKeyConstants.SCRATCH_CARD_OBJECT));

		int flag = (Integer) flowContext.getFromContext("FLAG");

		data.updateScratchCard(dbConnName, logger);
		if (flag == 1) {
			// being assigned for the first time ..
			data.persistMappingDetails(dbConnName, logger);
		} else if (flag == 2) {
			// being reassigned or may be just getting updated ..
			if (data.getAssignedStatus() == 1) {
				data.updateAssignedPartnerDetails(dbConnName, logger);
			}
		} else if (flag == 3) {
			// being unassigned ...
			data.unAssignPartner(dbConnName, logger);
		}

		response.setParameters(EzedTransactionCodes.SUCCESS,
				"SC updated in the database successful.");
		return RuleDecisionKey.ON_SUCCESS;
	}
}
