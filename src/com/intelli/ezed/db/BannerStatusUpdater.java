package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class BannerStatusUpdater extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public BannerStatusUpdater(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      response.setStringOnlySend(true);

      String bannerStatus = messageData.getParameter("status");
      if (bannerStatus == null || bannerStatus.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("New banner status is invalid " + bannerStatus);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "");
         return RuleDecisionKey.ON_FAILURE;
      }

      int bannerStatusInt = 1;
      if (bannerStatus.trim().compareToIgnoreCase("published") == 0)
      {
         bannerStatusInt = 0;
      }

      String bannerId = messageData.getParameter("bannerid");
      if (bannerId == null || bannerId.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Banner id in request is not proper " + bannerId);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "");
         return RuleDecisionKey.ON_FAILURE;
      }


      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdateBannerStatus");
         ps.setInt(1, bannerStatusInt);
         ps.setString(2, bannerId);
         int rows = ps.executeUpdate();
         if (rows > 0)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug(rows + " rows updated in db while updating banner status for id " + bannerId);
            }
            response.setParameters(EzedTransactionCodes.SUCCESS, bannerStatus);
            return RuleDecisionKey.ON_SUCCESS;
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Banner status cannot be updated in request for banner id " + bannerId);
            }
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "");
            return RuleDecisionKey.ON_FAILURE;
         }
      }
      catch (SQLException e)
      {
         logger.error("DB error while updating banner status for id " + bannerId, e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while updating banner status for id " + bannerId, e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
}
