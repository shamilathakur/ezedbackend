package com.intelli.ezed.data;

import java.util.ArrayList;
public class MyNotesList extends ResponseObject{

	
	private ArrayList<MyNotes> noteslist ;
	@Override
	public void prepareResponse() throws Exception {
		
		MyNotes[] myNotesList = new MyNotes[getNoteslist().size()];
		if(myNotesList != null ){
			for(int i=0;i<myNotesList.length; i++)
			{
				myNotesList[i] = getNoteslist().get(i);
			}
		}
		this.put("noteslist", myNotesList);
		
	}
	/**
	 * @return the notesList
	 */
	public ArrayList<MyNotes> getNoteslist() {
		return noteslist;
	}
	/**
	 * @param notesList the notesList to set
	 */
	public void setNoteslist(ArrayList<MyNotes> noteslist) {
		this.noteslist = noteslist;
	}


}
