package com.intelli.ezed.sheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;

import org.sabre.db.DbConnectionManager;
import org.sabre.utils.threads.TimedTask;
import org.sabre.workflow.context.FlowContext;
import org.sabre.workflow.processor.InputProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedLoggerName;

public class ResetRegistrationKey extends InputProcessor
{
   Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private Resetter resetter;

   public ResetRegistrationKey(String startAtTime, String pollingTime, String dbConnName)
   {
      resetter = new Resetter(startAtTime, Long.parseLong(pollingTime), dbConnName);
   }

   class Resetter extends TimedTask
   {
      private String dbConnName = null;
      public Resetter(String startAtTime, long pollingTime, String dbConnName)
      {
         super(startAtTime, pollingTime);
         this.dbConnName = dbConnName;
         this.setStartAtTime(startAtTime);
         this.setExecAtStartup(true);
         this.setOneTimeTask(false);
      }

      @Override
      public void doTimedTask()
      {
         RemoveInvalidKeys();
      }

      private void RemoveInvalidKeys()
      {
         //select * from userverif where now() - to_date(intime,'YYYYMMDDHH24MISS') > Interval '1 minutes';
         logger.info("Removing Invalid RegistrationKeys");
         Connection connection = DbConnectionManager.getConnectionByName(dbConnName);
         try
         {
            PreparedStatement selectStmt = DbConnectionManager.getPreparedStatement(dbConnName, connection, "DeleteExpiredFromUserVerif");
            int rows = selectStmt.executeUpdate();
            if (logger.isDebugEnabled())
            {
               logger.debug(rows + " rows deleted from database while deleting expired entries from userverif");
            }
         }
         catch (Exception e)
         {
            logger.error("Exception occured while deleting expired rows from userverif::", e);
         }
         finally
         {
            DbConnectionManager.releaseConnection(dbConnName, connection);
         }
      }
   }

   @Override
   public void initialize(FlowContext flowContext)
   {
   }

   @Override
   public void start()
   {
      resetter.startTask(true);
   }
}
