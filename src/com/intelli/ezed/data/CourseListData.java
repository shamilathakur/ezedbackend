package com.intelli.ezed.data;

/*
 * This class is used to create the list for returning paging data for the course list
 */
public class CourseListData {

	private String courseid;
	private int chaptercount;
	private int videocount;
	private int questioncount;
	private String coursename;
	private String courseimage;
	private String shortdesc;
	private String longdesctype;
	private String longdescstring;
	private String longdesccontent;
	/**
	 * @return the courseid
	 */
	public String getCourseid() {
		return courseid;
	}
	/**
	 * @param courseid the courseid to set
	 */
	public void setCourseid(String courseid) {
		this.courseid = courseid;
	}
	/**
	 * @return the chaptercount
	 */
	public int getChaptercount() {
		return chaptercount;
	}
	/**
	 * @param chaptercount the chaptercount to set
	 */
	public void setChaptercount(int chaptercount) {
		this.chaptercount = chaptercount;
	}
	public int getVideocount() {
		return videocount;
	}
	public void setVideocount(int videocount) {
		this.videocount = videocount;
	}
	public int getQuestioncount() {
		return questioncount;
	}
	public void setQuestioncount(int questioncount) {
		this.questioncount = questioncount;
	}
	public String getCoursename() {
		return coursename;
	}
	public void setCoursename(String coursename) {
		this.coursename = coursename;
	}
	public String getCourseimage() {
		return courseimage;
	}
	public void setCourseimage(String courseimage) {
		this.courseimage = courseimage;
	}
	public String getShortdesc() {
		return shortdesc;
	}
	public void setShortdesc(String shortdesc) {
		this.shortdesc = shortdesc;
	}
	public String getLongdesctype() {
		return longdesctype;
	}
	public void setLongdesctype(String longdesctype) {
		this.longdesctype = longdesctype;
	}
	public String getLongdescstring() {
		return longdescstring;
	}
	public void setLongdescstring(String longdescstring) {
		this.longdescstring = longdescstring;
	}
	public String getLongdesccontent() {
		return longdesccontent;
	}
	public void setLongdesccontent(String longdesccontent) {
		this.longdesccontent = longdesccontent;
	}
	
	
}
