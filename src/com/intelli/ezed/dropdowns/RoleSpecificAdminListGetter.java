package com.intelli.ezed.dropdowns;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.a.helper.utils.StringSplitter;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.dropdowns.data.DropDownListData;
import com.intelli.ezed.dropdowns.data.SingleDropDownData;

public class RoleSpecificAdminListGetter extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;

   public RoleSpecificAdminListGetter(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      String roles = messageData.getParameter("roleslist");
      if (roles == null || roles.trim().compareTo("") == 0 || roles.trim().compareToIgnoreCase("null") == 0 || roles.trim().compareToIgnoreCase("default") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("No roles received in request " + roles);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid roles");
         return RuleDecisionKey.ON_FAILURE;
      }

      String roleList[] = StringSplitter.splitString(roles, ",");

      Connection conn = null;
      PreparedStatement ps = null;
      ResultSet rs = null;
      DropDownListData dropDownListData = new DropDownListData();
      dropDownListData.setName("adminMapList");

      for (int i = 0; i < roleList.length; i++)
      {
         try
         {
            conn = DbConnectionManager.getConnectionByName(dbConnName);
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchRoleSpecificAdminList");
            ps.setInt(1, Integer.parseInt(roleList[i]));
            rs = ps.executeQuery();
            SingleDropDownData data = null;
            String name = null;
            while (rs.next())
            {
               data = new SingleDropDownData();
               data.setParam(rs.getString("userid"));
               name = rs.getString("name");
               if (name != null)
               {
                  data.setValue(name);
               }
               else
               {
                  data.setValue(data.getParam());
               }
               dropDownListData.getDropDownDatas().add(data);
            }
         }
         catch (SQLException e)
         {
            logger.error("Error while fetching courses for course id drop downs", e);
            response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while populating course list drop down");
            return RuleDecisionKey.ON_FAILURE;
         }
         catch (Exception e)
         {
            logger.error("Error while fetching courses for course id drop downs", e);
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while populating course list drop down");
            return RuleDecisionKey.ON_FAILURE;
         }
         finally
         {
            if (rs != null)
            {
               try
               {
                  rs.close();
               }
               catch (SQLException e)
               {
                  logger.error("Error while closing result set while populating drop down for courses");
               }
            }
            DbConnectionManager.releaseConnection(dbConnName, conn);
         }
      }

      response.setStatusCodeSend(false);
      response.setResponseObject(dropDownListData);
      response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
      return RuleDecisionKey.ON_SUCCESS;
   }

}
