package com.intelli.ezed.data;

import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.a.helper.utils.StringSplitter;
import com.intelli.ezed.utils.Randomizer;

public class MCQData extends TestQuestion
{
   public static Map<String, String> answerMap = new HashMap<String, String>();
   static
   {
      answerMap.put("a", "1");
      answerMap.put("b", "2");
      answerMap.put("c", "3");
      answerMap.put("d", "4");
      answerMap.put("e", "5");
      answerMap.put("f", "6");

      answerMap.put("A", "1");
      answerMap.put("B", "2");
      answerMap.put("C", "3");
      answerMap.put("D", "4");
      answerMap.put("E", "5");
      answerMap.put("F", "6");

      answerMap.put("1", "1");
      answerMap.put("2", "2");
      answerMap.put("3", "3");
      answerMap.put("4", "4");
      answerMap.put("5", "5");
      answerMap.put("6", "6");
   }
   private QuestionContent question;
   private ArrayList<QuestionContent> answers = new ArrayList<QuestionContent>();
   private String[] correctAnswer;

   public QuestionContent getQuestion()
   {
      return question;
   }

   public void setQuestion(QuestionContent question)
   {
      this.question = question;
   }

   public ArrayList<QuestionContent> getAnswers()
   {
      return answers;
   }

   public void setAnswers(ArrayList<QuestionContent> answers)
   {
      this.answers = answers;
   }

   public void addAnswers(QuestionContent answer)
   {
      this.getAnswers().add(answer);
   }

   public String[] getCorrectAnswer()
   {
      return correctAnswer;
   }

   public int setCorrectAnswer(String correctAnswer) throws Exception
   {
      try
      {
         String[] correctAnsArr = StringSplitter.splitString(correctAnswer, ",");
         this.correctAnswer = new String[correctAnsArr.length];
         int correctAnsInt = 0;
         for (int i = 0; i < correctAnsArr.length; i++)
         {
            this.correctAnswer[i] = answerMap.get(correctAnsArr[i]);
            correctAnsInt = Integer.parseInt(this.correctAnswer[i]);
            if (correctAnsInt > answers.size())
            {
               return 1;
            }
         }
         return 0;
      }
      catch (Exception e)
      {
         return 1;
      }
   }

   public String getCorrectAnswerString()
   {
      StringBuffer sb = new StringBuffer();
      for (int i = 0; i < getCorrectAnswer().length; i++)
      {
         sb.append(getCorrectAnswer()[i]).append(",");
      }
      if (sb.length() > 0)
      {
         sb.deleteCharAt(sb.length() - 1);
      }
      return sb.toString();
   }

   public ArrayList<TestQuestion> getPermutedQuestions(int noOfRandomQuestions, Randomizer randomizer)
   {
      ArrayList<String> questionCombo = randomizer.getSpacedPermutations(noOfRandomQuestions);
      String oneCombo;
      int index;
      MCQData nextData;
      ArrayList<TestQuestion> permutedQuestions = new ArrayList<TestQuestion>();
      StringBuffer permutedCorrectAns = new StringBuffer();
      for (int i = 0; i < questionCombo.size(); i++)
      {
         oneCombo = questionCombo.get(i);
         nextData = new MCQData();
         permutedCorrectAns = new StringBuffer();
         for (int j = 0; j < oneCombo.length(); j++)
         {
            nextData.setQuestion(this.getQuestion());
            index = Integer.parseInt(Character.toString(oneCombo.charAt(j)));
            nextData.getAnswers().add(this.getAnswers().get(index - 1));
            if (getCorrectAnswerString().contains(Integer.toString(index)))
            {
               permutedCorrectAns.append(Integer.toString(j + 1)).append(",");
            }
         }
         if (permutedCorrectAns.length() > 0)
         {
            permutedCorrectAns.deleteCharAt(permutedCorrectAns.length() - 1);
         }
         try
         {
            nextData.setCorrectAnswer(permutedCorrectAns.toString());
         }
         catch (Exception e)
         {

         }
         nextData.setExplanation(this.getExplanation());
         permutedQuestions.add(nextData);
      }
      Collections.shuffle(permutedQuestions);

      return permutedQuestions;
   }

   @Override
   public String toString()
   {
      StringBuffer sb = new StringBuffer();
      if (getQuestion().getType() == QuestionContent.STRING_TYPE)
      {
         sb.append("Question : ").append(getQuestion().getContentString()).append("\n");
      }
      else
      {
         sb.append("Question : ").append(getQuestion().getContentLoc()).append("\n");
      }

      QuestionContent questContent = null;
      for (int i = 0; i < getAnswers().size(); i++)
      {
         sb.append("Answer").append(i + 1).append(" : ");
         questContent = getAnswers().get(i);
         if (questContent.getType() == QuestionContent.STRING_TYPE)
         {
            sb.append(questContent.getContentString()).append("\n");
         }
         else
         {
            sb.append(questContent.getContentLoc()).append("\n");
         }
      }

      sb.append("Correct Answer : " + getCorrectAnswerString());
      return sb.toString();
   }

   public static void main(String[] args)
   {
      MCQData mcqData = new MCQData();
      QuestionContent question = new QuestionContent();
      question.setType(QuestionContent.STRING_TYPE);
      question.setContentString("What is capital of India");
      mcqData.setQuestion(question);

      QuestionContent answer = new QuestionContent();
      answer.setType(QuestionContent.STRING_TYPE);
      answer.setContentString("Delhi");
      mcqData.getAnswers().add(answer);

      answer = new QuestionContent();
      answer.setType(QuestionContent.STRING_TYPE);
      answer.setContentString("Mumbai");
      mcqData.getAnswers().add(answer);

      answer = new QuestionContent();
      answer.setType(QuestionContent.STRING_TYPE);
      answer.setContentString("Chennai");
      mcqData.getAnswers().add(answer);

      answer = new QuestionContent();
      answer.setType(QuestionContent.STRING_TYPE);
      answer.setContentString("Bangalore");
      mcqData.getAnswers().add(answer);

      // answer = new QuestionContent();
      // answer.setType(QuestionContent.STRING_TYPE);
      // answer.setContentString("Kolkata");
      // mcqData.getAnswers().add(answer);

      try
      {
         mcqData.setCorrectAnswer("1,2,3");
      }
      catch (Exception e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }

      System.out.println("Original question : \n");
      System.out.println(mcqData.toString());
      ArrayList<TestQuestion> randomPermutedQuestions = mcqData.getPermutedQuestions(8, new Randomizer(4));
      for (int i = 0; i < randomPermutedQuestions.size(); i++)
      {
         System.out.println("\n\nQuestion " + (i + 1) + " \n");
         System.out.println(randomPermutedQuestions.get(i).toString());
      }
   }

   @Override
   public void persistQuestion(PreparedStatement ps, int startPoint) throws Exception
   {
      ps.setInt(startPoint + 1, getQuestion().getType());
      if (getQuestion().getType() == QuestionContent.STRING_TYPE)
      {
         ps.setString(startPoint + 2, getQuestion().getContentString());
         ps.setString(startPoint + 3, null);
      }
      else
      {
         ps.setString(startPoint + 2, null);
         ps.setString(startPoint + 3, getQuestion().getContentLoc());
      }

      int j = 0;
      int i = 0;
      for (; i < getAnswers().size(); i++, j += 3)
      {
         ps.setInt(startPoint + 4 + j, getAnswers().get(i).getType());
         if (getAnswers().get(i).getType() == QuestionContent.STRING_TYPE)
         {
            ps.setString(startPoint + 5 + j, getAnswers().get(i).getContentString());
            ps.setString(startPoint + 6 + j, null);
         }
         else
         {
            ps.setString(startPoint + 5 + j, null);
            ps.setString(startPoint + 6 + j, getAnswers().get(i).getContentLoc());
         }
      }
      for (; i < 6; i++, j += 3)
      {
         ps.setInt(startPoint + 4 + j, 0);
         ps.setString(startPoint + j + 5, null);
         ps.setString(startPoint + j + 6, null);
      }
      ps.setString(startPoint + j + 4, getCorrectAnswerString());
      ps.setInt(startPoint + j + 5, TestQuestionData.MCQ_QUESTION_TYPE);
      ps.setInt(startPoint + j + 6, getExplanation().getType());
      ps.setString(startPoint + j + 7, getExplanation().getContentString());
      ps.setString(startPoint + j + 8, getExplanation().getContentLoc());
   }

   @Override
   public int getNoOfAnswers()
   {
      return getAnswers().size();
   }

   @Override
   public void persistForBulkUpload(PreparedStatement ps, int startPoint) throws Exception
   {
      ps.setInt(startPoint + 1, getQuestion().getType());
      if (getQuestion().getType() == QuestionContent.STRING_TYPE)
      {
         ps.setString(startPoint + 2, getQuestion().getContentString());
         ps.setString(startPoint + 3, null);
      }
      else
      {
         ps.setString(startPoint + 2, null);
         ps.setString(startPoint + 3, getQuestion().getContentLoc());
      }

      int j = 0;
      int i = 0;
      for (; i < getAnswers().size(); i++, j += 3)
      {
         ps.setInt(startPoint + 4 + j, getAnswers().get(i).getType());
         if (getAnswers().get(i).getType() == QuestionContent.STRING_TYPE)
         {
            ps.setString(startPoint + 5 + j, getAnswers().get(i).getContentString());
            ps.setString(startPoint + 6 + j, null);
         }
         else
         {
            ps.setString(startPoint + 5 + j, null);
            ps.setString(startPoint + 6 + j, getAnswers().get(i).getContentLoc());
         }
      }
      for (; i < 6; i++, j += 3)
      {
         ps.setInt(startPoint + 4 + j, 0);
         ps.setString(startPoint + j + 5, null);
         ps.setString(startPoint + j + 6, null);
      }
      ps.setString(startPoint + j + 4, getCorrectAnswerString());
      ps.setInt(startPoint + j + 5, TestQuestionData.MCQ_QUESTION_TYPE);
   }

   @Override
   public void writeToFile(FileOutputStream fos) throws IOException
   {
      StringBuffer sb = new StringBuffer();
      sb.append("<question type=\"MCQ\">\n");
      if (getQuestionid() != null)
      {
         sb.append("<id>");
         sb.append(getQuestionid());
         sb.append("</id>\n");
      }
      sb.append("<questionstatement>\n");
      sb.append("<type>");
      sb.append(getQuestion().getType());
      sb.append("</type>\n");
      if (getQuestion().getType() == QuestionContent.STRING_TYPE)
      {
         sb.append("<string>");
         sb.append(getQuestion().getContentString());
         sb.append("</string>\n");
      }
      else
      {
         sb.append("<content>");
         sb.append(getQuestion().getContentLoc());
         sb.append("</content>\n");
      }
      sb.append("</questionstatement>\n");
      for (int i = 0; i < getAnswers().size(); i++)
      {
         sb.append("<answer" + (i + 1) + "statement>");
         sb.append("<type>");
         sb.append(getAnswers().get(i).getType());
         sb.append("</type>\n");
         if (getAnswers().get(i).getType() == QuestionContent.STRING_TYPE)
         {
            sb.append("<string>");
            sb.append(getAnswers().get(i).getContentString());
            sb.append("</string>\n");
         }
         else
         {
            sb.append("<content>");
            sb.append(getAnswers().get(i).getContentLoc());
            sb.append("</content>\n");
         }
         sb.append("</answer" + (i + 1) + "statement>\n");
      }
      if (getDifficultyLevel() != 0)
      {
         sb.append("<difficultylevel>");
         sb.append(getDifficultyLevel());
         sb.append("</difficultylevel>\n");
      }
      sb.append("<correctanswer>");
      sb.append(getCorrectAnswerString());
      sb.append("</correctanswer>\n");
      sb.append("<explanation>\n");
      sb.append("<type>");
      sb.append(getExplanation().getType());
      sb.append("</type>\n");
      if (getExplanation().getType() == QuestionContent.STRING_TYPE)
      {
         sb.append("<string>");
         sb.append(getExplanation().getContentString());
         sb.append("</string>\n");
      }
      else
      {
         sb.append("<content>");
         sb.append(getExplanation().getContentLoc());
         sb.append("</content>\n");
      }
      sb.append("</explanation>\n");
      sb.append("</question>");
      fos.write(sb.toString().getBytes());
   }

   @Override
   public void replaceContentIds(Map<String, String> contentDirIdMap) throws Exception
   {
      QuestionContent questionContent = getQuestion();
      if (questionContent.getType() == QuestionContent.CONTENT_TYPE)
      {
         String questionContentTemp = contentDirIdMap.get(questionContent.getContentLoc());
         if (questionContentTemp == null)
         {
            throw new Exception("Question content could not be found");
         }
         questionContent.setContentLoc(questionContentTemp);
      }
      String answerContentTemp = null;
      for (int i = 0; i < getAnswers().size(); i++)
      {
         questionContent = getAnswers().get(i);
         if (questionContent.getType() == QuestionContent.CONTENT_TYPE)
         {
            answerContentTemp = contentDirIdMap.get(questionContent.getContentLoc());
            if (answerContentTemp == null)
            {
               throw new Exception("Answer content could not be found");
            }
            questionContent.setContentLoc(answerContentTemp);
         }
      }
      questionContent = getExplanation();
      if (questionContent.getType() == QuestionContent.CONTENT_TYPE)
      {
         questionContent.setContentLoc(contentDirIdMap.get(questionContent.getContentLoc()));
      }
   }
}