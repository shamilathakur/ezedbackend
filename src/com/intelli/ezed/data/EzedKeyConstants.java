package com.intelli.ezed.data;

public class EzedKeyConstants {

	// Keys related to test
	public static final String TEST_SESSION_ID = "TestSessionId";
	public static final String ADMIN_SESSION_ID = "AdminSessionId";
	public static final String TEST_DETAILS = "TestDetails";
	public static final String CITY_ID_GENERATOR = "CityIdGenerator";

	public static final String KEYWORD = "Keyword";
	public static final String REQUEST_TYPE = "RequestType";
	public static final String EZED_RESPONSE = "ResponseObject";
	public static final String USER_ID = "UserId";
	public static final String USER_DETAILS = "UserDetails";
	public static final String REQUEST_IN_TIME = "RequestInTime";
	public static final String SESSION_ID = "SessionId";
	public static final String CART_DETAILS = "CartDetails";
	public static final String PAYMENT_DATA = "PaymentData";

	public static final String RESPONSE_SEPARATOR = ":";
	public static final String SESSION_MANAGER = "SessionManager";
	public static final String TEST_SESSION_MANAGER = "TestSessionManager";
	public static final String ADMIN_SESSION_MANAGER = "AdminSessionManager";
	public static final String CART_STRING = "CartString";
	public static final String QUESTION_OBJECT = "QuestionObject";
	public static final String RANDOM_QUESTION_LIST = "RandomQuestionList";
	public static final String EVENT_OBJECT = "EventObject";
	public static final String UPDATED_USER_PROFILE = "UpdatedUserProfile";
	public static final String USER_VERIF_COMPLETED = "UserVerifCompleted";
	public static final String ACTION_DATA = "ActionData";
	public static final String STREAM_ENTRY_STRING = "StreamEntryString";
	public static final String INVITED_FRIEND = "InvitedFriend";
	public static final String COURSE_CALENDAR = "CourseCalendar";
	public static final String EXISTING_COURSE_CALENDAR = "ExistingCourseCalendar";
	public static final String EXPIRED_GOAL = "ExpiredGoal";
	public static final String SYMLINK_MANAGER = "SymLinkManager";
	public static final String COURSE_ID = "Courseid";
	public static final String CHALLENGE_USER_ID = "ChallengeUserId";
	public static final String CHALLENGE_TEST_ID = "ChallengeTestId";
	public static final String CHALLENGE_ID = "ChallengeId";
	public static final String DISCOUNT_COUPON = "DiscountCoupon";
	public static final String BUFFER_END_DATE = "EndDateBuffer";
	public static final String LEARNING_PATH_DATA = "LearningPatData";
	public static final String COURSE_PATH_DATA = "CoursePathData";
	public static final String COURSE_ID_LIST = "CourseIdList";
	public static final String CHAPTER_ID_LIST = "ChapterIdList";

	// Keys realted to admin login
	public static final String ADMIN_PROFILE = "AdminProfile";

	// keys related to bulk upload
	public static final String BULK_UPLOAD_UNIVFILE = "BulkUploadUnivFile";
	public static final String BULK_UPLOAD_UNIVOBJECT = "BulkUploadUnivObj";
	public static final String BULK_UPLOAD_COURSEOBJECT = "BulkUploadCourseObj";
	public static final String BULK_UPLOAD_CHAPTERFILE = "BulkUploadChapterFile";
	public static final String BULK_UPLOAD_CONTENTFILE = "BulkUploadContentFile";
	public static final String BULK_UPLOAD_CONTENTOBJECT = "BulkUploadContentObj";
	public static final String BULK_UPLOAD_STARTER = "BulkUploadStarter";
	public static final String BULK_UPLOAD_CONTENTMAP = "BulkUploadContentMap";
	public static final String BULK_UPLOAD_CHAPTEROBJECT = "BulkUploadChapterObject";
	public static final String BULK_UPLOAD_QBANK_DIR_FILE = "BulkUploadQbankDirFile";
	public static final String BULK_UPLOAD_CONTENTID = "BulkUploadContentId";

	// keys related to analytics specifically
	public static final String LAST_BACKUP_PREP_STMT_USERS = "LastBackupPreparedStatementUsers";
	public static final String LAST_TO_LAST_BACKUP_PREP_STMT_USERS = "LastToLastBackupPreparedStatementUsers";
	public static final String LAST_BACKUP_PREP_STMT_USER_COURSE = "LastBackupPreparedStatementUserCourse";
	public static final String LAST_TO_LAST_BACKUP_PREP_STMT_USER_COURSE = "LastToLastBackupPreparedStatementUserCourse";
	public static final String TODAY_DATE = "TodayDate";
	public static final String ANALYTIC_DATE_UPDATE_PREPSTMT = "AnalyticDateUpdatePreparedStatement";
	public static final String LAST_BACKUP_DELETE_PREP_STMT_USERS = "LastBackupDeletePrepStmtUsers";
	public static final String LAST_TO_LAST_BACKUP_DELETE_PREP_STMT_USERS = "LastToLastBackupDeletePrepStmtUsers";
	public static final String LAST_BACKUP_DELETE_PREP_STMT_USER_COURSE = "LastBackupDeletePrepStmtUserCourse";
	public static final String LAST_TO_LAST_BACKUP_DELETE_PREP_STMT_USER_COURSE = "LastToLastBackupDeletePrepStmtUserCourse";
	public static final String TOTAL_CART_AMOUNT = "TotalCartAmount";
	public static final String DISCOUNT_TYPE = "DiscountType";
	public static final String DISCOUNT_AMOUNT = "DiscountAmount";
	public static final String GOAL_MODIFICATION_DATA = "GoalModificationData";
	public static final String CORRECT_ANSWER_TRACKER = "CorrectAnswerTracker";
	public static final String ADMIN_USER_ID = "AdminUserId";
	public static final String ADMIN_ROLE = "AdminRole";
	public static final String ADMIN_STATUS_DATA = "AdminStatusData";
	public static final String USER_FOCUS_REPORT_DATA = "UserFocusReportData";
	public static final String DIFF_WISE_POINTS = "DiffWisePoints";
	public static final String MEGA_TEST_POINTS = "MegaTestPoints";
	public static final String DISCOUNT_DB_AMT = "DiscountDBAmt";
	public static final String CGST_DB_AMT = "CGSTDBAmt";
	public static final String SGST_DB_AMT = "SGSTDBAmt";
	public static final String HIGHER_EDU_CESS_DB_AMT = "HigherEduCessDBAmt";
	public static final String QUESTION_ID = "QuestionId";
	public static final String CART_PAYMENT_DATA = "CartPaymentData";
	public static final String MAX_TEST_POINTS = "MaxTestPoints";

	// Added by kunal on 13/01/2015
	public static final String ENROLLMENT_DATA = "EnrollmentDataObject";

	// keys related to scratch card management added on 25-01-2015
	public static final String PARTNER_DATA_OBJECT = "PartnerData";
	public static final int PARTNER_STATUS_ACTIVE = 1;
	public static final int PARTNER_STATUS_INACTIVE = 0;
	public static final String PARTNER_ID_GENERATOR = "PARTNER0000000";
	public static final String SCRATCH_CARD_OBJECT = "ScratchCardData";
	public static final String ADD_COURSE_USING_SCRATCHCARD_DETAILS = "UserScratchCardDetailsObject";
	public static final String SCRATCH_CARD_ERROR_LOG_DB = "ScratchCardErrorDataObject";

	// added on 15/04/2015 for the scheduler MailerForNewRegistration
	public static final String DAY_NUMBER = "noOfDays";

	// added on 18/04/2015 for add course to user from backend
	public static final String ADD_COURSE_TO_USER_FROM_ADMIN = "ManageUserCourseFromBackendBean";
	// added on 18/06/2015 for managing paymentid generation
	
		public static final String PAYMENT_ID_FOR_TRANSACTION_TYPE = "transactiontype";
		public static final int PAYMENT_ID_FOR_TRANSACTION_TYPE_ONLINE_PAYMENT = 0 ;
		public static final int PAYMENT_ID_FOR_TRANSACTION_TYPE_SCRATCHCARD = 1 ;
      //keys related to new university and new college
	    //added by Saroj on 11th July, 2015
	    public static final String UNIVERSITY_DATA = "UniversityData";
	    public static final String COLLEGE_DATA = "CollegeData";
	    public static final int UNIVERSITY_ACTIVE=1;
	    public static final int UNIVERSITY_INACTIVE=0;
	    public static final int COLLEGE_ACTIVE=1;
	    public static final int COLLEGE_INACTIVE=0;
	    public static final int UNIVERSITY_ADDED_BY_USER =1;
	    public static final int UNIVERSITY_ADDED_BY_ADMIN =0;
	    public static final int COLLEGE_ADDED_BY_USER =1;
	    public static final int COLLEGE_ADDED_BY_ADMIN =0;

}
