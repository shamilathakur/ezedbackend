package com.intelli.ezed.data;

public class GenericResponseObject extends ResponseObject
{
   private Object value;

   @Override
   public void prepareResponse() throws Exception
   {
      this.put("value", getValue());
   }

   public Object getValue()
   {
      return value;
   }

   public void setValue(Object value)
   {
      this.value = value;
   }

}
