package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.FrontendFetchData;
import com.intelli.ezed.data.Response;

public class CollegeListFetcher extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public CollegeListFetcher(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      String[] collegeArr = null;
      FrontendFetchData collegeData = new FrontendFetchData();
      response.setResponseObject(collegeData);
      response.setStatusCodeSend(false);
      collegeData.setAttribName("aaData");
      ArrayList<String[]> colleges = new ArrayList<String[]>();
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchCollegeList");
         rs = ps.executeQuery();
         int i = 1;
         while (rs.next())
         {
            collegeArr = new String[5];
            collegeArr[0] = Integer.toString(i);
            collegeArr[1] = rs.getString(1);
            collegeArr[2] = rs.getString(2);
            collegeArr[3] = rs.getString(3);
            collegeArr[4] = rs.getString(4);
            i++;
            colleges.add(collegeArr);
         }
         collegeData.setAttribList(colleges);
         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
      }
      catch (SQLException e)
      {
         logger.error("DB error while fetching list of colleges", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "DB error while fetching colleges");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching list of colleges", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching colleges");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing result set while fetching college list", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
      return RuleDecisionKey.ON_SUCCESS;
   }
}
