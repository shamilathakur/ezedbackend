package com.intelli.ezed.rules;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.a.helper.utils.StringSplitter;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.UserProfile;
import com.intelli.ezed.utils.AccountStatusHelper;

public class UserStatusManager extends SingletonRule
{

   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;

   private String REG = "REG";
   private String NOTREG = "NOTREG";
   private String AUTH = "AUTH"; //it is actually verified status
   private String UNAUTH = "UNAUTH"; 
   private String BLOCK = "BLOCK";
   private String UNBLOCK = "UNBLOCK";

   public UserStatusManager(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      //http://localhost:10181/ezed/?svc=6002&email=a@b.com&sessionid=000000123321&useremail=chetan@b.com&changeto=UNAUTH

      String userid = messageData.getParameter("useremail");
      String changeToList = messageData.getParameter("changeto");

      UserProfile userProfile = new UserProfile();
      userProfile.setUserid(userid);
      try
      {
         userProfile.populateUserProfile(dbConnName, logger);
      }
      catch (Exception e)
      {
         logger.error("Exception occoured while fetching user profile.", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Please check the userid.");
         return RuleDecisionKey.ON_FAILURE;
         //e.printStackTrace();
      }

      int chkflag = userProfile.getChkflag();
      String[] temp = StringSplitter.splitString(changeToList, ",");


      if (logger.isDebugEnabled())
      {
         logger.debug("Chkflag before : " + chkflag);
      }
      for (String changeTo : temp)
      {
         if (changeTo.compareTo(AUTH) == 0)
         {
            chkflag = AccountStatusHelper.setValidate(chkflag);
         }
         else if (changeTo.compareTo(UNAUTH) == 0)
         {
            chkflag = AccountStatusHelper.resetValidate(chkflag);
         }
         else if (changeTo.compareTo(BLOCK) == 0)
         {
            chkflag = AccountStatusHelper.setLock(chkflag);
         }
         else if (changeTo.compareTo(UNBLOCK) == 0)
         {
            chkflag = AccountStatusHelper.resetLock(chkflag);
         }
         else if (changeTo.compareTo(REG) == 0)
         {
            chkflag = AccountStatusHelper.setRegistered(chkflag);
         }
         else if (changeTo.compareTo(NOTREG) == 0)
         {
            chkflag = AccountStatusHelper.resetRegistered(chkflag);
         }
         else
         {
            logger.error("Invalid action. : " + changeTo);
            response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid status change Requested.");
            return RuleDecisionKey.ON_FAILURE;
         }
      }

      userProfile.setChkflag(chkflag);
      if (logger.isDebugEnabled())
      {
         logger.debug("Chkflag after : " + chkflag);
      }


      try
      {
         userProfile.commitUserProfile(dbConnName, logger);
      }
      catch (Exception e)
      {
         logger.error("Exception occoured while commiting user profile.", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal Error please try again.");
         return RuleDecisionKey.ON_FAILURE;
      }

      response.setParameters(EzedTransactionCodes.SUCCESS, "User updated successfully.");
      return RuleDecisionKey.ON_SUCCESS;
   }
}
