package com.intelli.ezed.analytics.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.sabre.db.DbConnectionManager;
import org.slf4j.Logger;

import com.intelli.ezed.data.ResponseObject;


public class PercentageCompletionReportData extends ResponseObject
{
   private String userId;
   private String cityId;
   private String univName;
   private String collegeName;

   private ArrayList<CoursePercentageData> courseData = new ArrayList<CoursePercentageData>();

   private GenericChartData title = new GenericChartData();
   private GenericChartData xAxis = new GenericChartData();
   private GenericChartData yAxis = new GenericChartData();
   private GenericChartData type = new GenericChartData();

   public void prepareChart() throws Exception
   {
      type.getText().add("type");
      type.getValues().add("bar");
      type.prepareResponse();

      title.getText().add("text");
      title.getValues().add("Percentage Completion report Per Course");
      title.prepareResponse();

      xAxis.getText().add("categories");
      String[] cats = {"Your Completion", "Average Completion across your City", "Average completion across your University", "Average completion across your College"};
      xAxis.getValues().add(cats);
      xAxis.getText().add("title");
      GenericChartData xAxisTitleData = new GenericChartData();
      xAxisTitleData.getText().add("text");
      xAxisTitleData.getValues().add("Domains");
      xAxisTitleData.prepareResponse();
      xAxis.getValues().add(xAxisTitleData);
      xAxis.prepareResponse();

      yAxis.getText().add("min");
      yAxis.getValues().add(0);
      yAxis.getText().add("title");
      GenericChartData yAxisText = new GenericChartData();
      yAxisText.getText().add("text");
      yAxisText.getValues().add("Percentage Completion");
      yAxisText.getText().add("align");
      yAxisText.getValues().add("high");
      yAxis.getValues().add(yAxisText);
      yAxis.getText().add("labels");
      GenericChartData yAxisLabels = new GenericChartData();
      yAxisLabels.getText().add("overflow");
      yAxisLabels.getValues().add("justify");
      yAxis.getValues().add(yAxisLabels);

      yAxisLabels.prepareResponse();
      yAxisText.prepareResponse();
      yAxis.prepareResponse();
   }

   public ArrayList<CoursePercentageData> getCourseData()
   {
      return courseData;
   }

   public void setCourseData(ArrayList<CoursePercentageData> courseData)
   {
      this.courseData = courseData;
   }

   public String getUserId()
   {
      return userId;
   }

   public void setUserId(String userId)
   {
      this.userId = userId;
   }

   public String getCityId()
   {
      return cityId;
   }

   public void setCityId(String cityId)
   {
      if (cityId.compareTo("CIT1000001") == 0)
      {
         this.cityId = null;
      }
      else
      {
         this.cityId = cityId;
      }
   }

   public String getUnivName()
   {
      return univName;
   }

   public void setUnivName(String univName)
   {
      this.univName = univName;
      if (univName == null || univName.trim().compareTo("") == 0)
      {
         this.univName = null;
      }
   }

   public String getCollegeName()
   {
      return collegeName;
   }

   public void setCollegeName(String collegeName)
   {
      this.collegeName = collegeName;
      if (collegeName == null || collegeName.trim().compareTo("") == 0)
      {
         this.collegeName = null;
      }
   }

   public void populateReport(String dbConnName, Logger logger) throws SQLException, Exception
   {
      this.populateUserDetails(dbConnName, logger);
      for (int i = 0; i < this.courseData.size(); i++)
      {
         this.courseData.get(i).fetchAverageValues(getCityId(), getUnivName(), getCollegeName(), dbConnName, logger);
      }
   }

   //select u.userid,cityid,univname,collegename,courseid,percentcomplete from users u, usercourse uc where u.userid = 'altaf@ymail.com' and uc.userid='altaf@ymail.com';
   private void populateUserDetails(String dbConnName, Logger logger) throws SQLException, Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchUserArea");
         ps.setString(1, getUserId());
         rs = ps.executeQuery();
         while (rs.next())
         {
            setCityId(rs.getString("cityid"));
            setUnivName(rs.getString("univname"));
            setCollegeName(rs.getString("collegename"));
         }
      }
      catch (SQLException e)
      {
         throw e;
      }
      catch (Exception e)
      {
         throw e;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
   @Override
   public void prepareResponse() throws Exception
   {
      //      this.put("userid", getUserId());
      //      CoursePercentageData[] datas = new CoursePercentageData[this.courseData.size()];
      //      for (int i = 0; i < this.courseData.size(); i++)
      //      {
      //         datas[i] = this.courseData.get(i);
      //         datas[i].prepareResponse();
      //      }
      //      this.put("courses", datas);

      JSONArray jsonArray = new JSONArray();
      for (int i = 0; i < this.courseData.size(); i++)
      {
         this.courseData.get(i).prepareResponse();
         jsonArray.put(this.courseData.get(i));
      }
      this.put("chart", type);
      this.put("title", title);
      this.put("xAxis", xAxis);
      this.put("yAxis", yAxis);
      this.put("series", jsonArray);
   }

}
