package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.CartData;
import com.intelli.ezed.data.CartDetails;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.PaymentData;
import com.intelli.ezed.data.PaymentIdData;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.UserProfile;

public class BackendAddPaymentDetailsPersister extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;
   private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
   private String merchantRef;

   public BackendAddPaymentDetailsPersister(String ruleName, String dbConnName)
   {
      this(ruleName, dbConnName, "ADMIN ADD");
   }

   public BackendAddPaymentDetailsPersister(String ruleName, String dbConnName, String merchantRef)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
      this.merchantRef = merchantRef;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      CartDetails cartDetails = (CartDetails) flowContext.getFromContext(EzedKeyConstants.CART_DETAILS);
      CartData[] cart = cartDetails.getCartDatas();
      PaymentIdData payData = (PaymentIdData) flowContext.getFromContext(EzedKeyConstants.CART_PAYMENT_DATA);
      Connection con = DbConnectionManager.getConnectionByName(dbConnName);
      String sessionId = (String) flowContext.getFromContext(EzedKeyConstants.ADMIN_SESSION_ID);
      UserProfile userProfile = (UserProfile) flowContext.getFromContext(EzedKeyConstants.USER_DETAILS);
      String inTime = (String) flowContext.getFromContext(EzedKeyConstants.REQUEST_IN_TIME);
      response.setResponseObject(null);
      try
      {
         PreparedStatement ps = DbConnectionManager.getPreparedStatement(dbConnName, con, "PersistCompletedPaymentDetails");
         ps.setString(1, payData.getAppId());
         ps.setString(2, payData.getPaymentId());
         ps.setString(3, sessionId);
         ps.setString(4, userProfile.getUserid());
         ps.setString(5, inTime);
         ps.setString(6, PaymentData.CART_ADDED_TO_USER);
         StringBuffer sb = new StringBuffer();
         for (int i = 0; i < cart.length; i++)
         {
            sb.append(cart[i].getCourseId());
            sb.append(",");
            sb.append(cart[i].getCourseType());
            sb.append(",");
            sb.append(sdf.format(cart[i].getStartDate()));
            sb.append(",");
            sb.append(sdf.format(cart[i].getEndDate()));
            sb.append(";");
         }
         sb.deleteCharAt(sb.length() - 1);
         ps.setString(7, sb.toString());
         ps.setInt(8, 0);
         ps.setInt(9, 0);
         ps.setInt(10, 0);
         ps.setString(11, null);
         ps.setString(12, null);
         ps.setInt(13, 0);
         ps.setString(14, merchantRef);
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in database while persisting payment id" + payData.getPaymentId() + " in session id " + sessionId);
         }
         response.setResponseObject(payData);
         response.setParameters(EzedTransactionCodes.SUCCESS, "Course added successfully to user");
      }
      catch (SQLException e)
      {
         logger.error("Error while persisting payment id in database for session " + sessionId, e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while persisting session");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while persisting payment id in database for session " + sessionId, e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while persisting session");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, con);
      }
      return RuleDecisionKey.ON_SUCCESS;
   }
}
