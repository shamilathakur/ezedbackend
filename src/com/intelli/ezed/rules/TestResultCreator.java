package com.intelli.ezed.rules;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.utils.StringSplitter;
import com.intelli.ezed.analytics.data.TestResultPieChartData;
import com.intelli.ezed.bulkupload.data.ContentDetails;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.QuestionContent;
import com.intelli.ezed.data.QuizRequestResponse;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.SymLinkObject;
import com.intelli.ezed.data.TestDetails;
import com.intelli.ezed.data.TestQuestionData;
import com.intelli.ezed.data.TestRequestResponse;
import com.intelli.ezed.data.TestResultResponse;
import com.intelli.ezed.data.TestResultResponseWithPieChart;
import com.intelli.ezed.data.UserProfile;
import com.intelli.ezed.utils.Globals;
import com.intelli.ezed.utils.SymLinkManager;

public class TestResultCreator extends SingletonRule
{

   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String newLinkLocationNotes;
   private String newLinkLocationVideos;
   private String notesStartPoint;
   private String vidStartPoint;
   private boolean betaOverride = false;

   public TestResultCreator(String ruleName, String dbConnName, String newLinkLoc, String newLinkVideos,
                            String notesStartPoint, String vidStartPoint, String betaOverride)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
      this.newLinkLocationNotes = newLinkLoc;
      this.newLinkLocationVideos = newLinkVideos;
      this.notesStartPoint = notesStartPoint;
      this.vidStartPoint = vidStartPoint;
      this.betaOverride = Boolean.parseBoolean(betaOverride);
   }

   public TestResultCreator(String ruleName, String dbConnName, String newLinkLoc, String newLinkVideos,
                            String notesStartPoint, String vidStartPoint)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
      this.newLinkLocationNotes = newLinkLoc;
      this.newLinkLocationVideos = newLinkVideos;
      this.notesStartPoint = notesStartPoint;
      this.vidStartPoint = vidStartPoint;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      UserProfile userProfile = (UserProfile) flowContext.getFromContext(EzedKeyConstants.USER_DETAILS);
      TestDetails testDetails = (TestDetails) flowContext.getFromContext(TestDetails.TEST_DETAILS);
      String testSessionId = (String) flowContext.getFromContext(EzedKeyConstants.TEST_SESSION_ID);

      Connection connection = DbConnectionManager.getConnectionByName(dbConnName);
      ResultSet resultSet = null;
      QuizRequestResponse quizRequestResponse;
      ArrayList<TestRequestResponse> quizQuestions = new ArrayList<TestRequestResponse>();
      TestResultPieChartData pieChartData = new TestResultPieChartData();

      SymLinkManager symLinkManager = (SymLinkManager) flowContext.getFromContext(EzedKeyConstants.SYMLINK_MANAGER);
      String userId = (String) flowContext.getFromContext(EzedKeyConstants.USER_ID);
      Integer diffWisePoints = 0;
      Integer maxPoints = 0;
      try
      {
         //select * from ongoingtest where testsessionid = ?
         PreparedStatement statement = DbConnectionManager.getPreparedStatement(dbConnName, connection, "SelectSummaryTestQuestions");
         statement.setString(1, testSessionId);
         resultSet = statement.executeQuery();

         String explanationContent = null;
         String userAns = null;
         String correctAns = null;
         while (resultSet.next())
         {
            TestRequestResponse testResponse = new TestRequestResponse();

            int questionSerialNumber = resultSet.getInt("qserialnumber");
            testResponse.setCurrentQuestionNumber(questionSerialNumber);

            testResponse.setExplanation(new QuestionContent());
            testResponse.getExplanation().setType(resultSet.getInt("explanationtype"));
            testResponse.getExplanation().setContentString(resultSet.getString("explanation"));
            explanationContent = resultSet.getString("explanationcontent");
            if (explanationContent != null)
            {
               testResponse.getExplanation().setContentLoc(fetchSymLinkForContent(explanationContent, dbConnName, symLinkManager, userId));
            }

            testResponse.setUserAns(resultSet.getString("receivedcorrectansstring"));

            int questionType = resultSet.getInt("questiontype");
            testResponse.setQuestiontype(questionType);
            testResponse.setQuestionId(resultSet.getString("questionid"));
            testResponse.setQuestionNumberOfRandomQuestion(resultSet.getInt("questionnumber"));

            //have to create correct answer string for FIB because no data in correctAns column in table.
            StringBuffer correctAnsForFillInTheBlanks = new StringBuffer();

            //select question 
            QuestionContent question = new QuestionContent();
            int i = resultSet.getInt("questioncontenttype");
            question.setType(i); //type 0 = no data , 1 = string, 2 = content
            if (i == QuestionContent.CONTENT_ABSENT)
            {
               //set No content
            }
            else if (i == QuestionContent.STRING_TYPE)
            {
               question.setContentString(resultSet.getString("questionstring"));
            }
            else if (i == QuestionContent.CONTENT_TYPE)
            {
               question.setContentString(resultSet.getString("questioncontent"));
            }
            //Setting the question into the response object
            testResponse.setQuestion(question);
            testResponse.setDifficultyLevel(resultSet.getInt("difficultylevel"));

            //Creating a array list to put all the answers inside it
            ArrayList<QuestionContent> answers = new ArrayList<QuestionContent>();

            if (questionType == TestQuestionData.MTF_QUESTION_TYPE)
            {
               //Match state with capital;1;Delhi;West Bengal;Tamil Nadu;Maharashtra
               String mtfQuestionString = question.getContentString();
               String[] tokens = StringSplitter.splitString(mtfQuestionString, ";");

               //Setting new question string to the question part
               if (question.getType() == QuestionContent.CONTENT_TYPE)
               {
                  question.setContentString(fetchSymLinkForContent(tokens[0], dbConnName, symLinkManager, userId));
               }
               else
               {
                  question.setContentString(tokens[0]);
               }

               String mtfContentType = tokens[1];

               for (int k = 1; k <= 6; k++)
               {
                  QuestionContent ans = new QuestionContent();
                  i = resultSet.getInt("ans" + k + "type");
                  ans.setType(Integer.parseInt(mtfContentType));
                  if (i == QuestionContent.CONTENT_ABSENT)
                  {
                     //set No content
                  }
                  else
                  {
                     if (i == QuestionContent.STRING_TYPE)
                     {
                        String t1 = resultSet.getString("ans" + k + "string");
                        //                        ans.setContentString((mtfContentType + "*" + tokens[k + 1] + "*" + i + "*" + t1));
                        ans.setContentString((tokens[k + 1] + "~" + i + "~" + t1));
                     }
                     else if (i == QuestionContent.CONTENT_TYPE)
                     {
                        String t1 = resultSet.getString("ans" + k + "content");
                        t1 = fetchSymLinkForContent(t1, dbConnName, symLinkManager, userId);
                        //                        ans.setContentString((mtfContentType + "*" + tokens[k + 1] + "*" + "*" + t1));
                        ans.setContentString((tokens[k + 1] + "~" + i + "~" + t1));
                     }
                     //adding answers to array list
                     answers.add(ans);
                  }
               }
            }
            else
            {
               if (i == QuestionContent.CONTENT_TYPE)
               {
                  question.setContentString(fetchSymLinkForContent(question.getContentString(), dbConnName, symLinkManager, userId));
               }
               for (int k = 1; k <= 6; k++)
               {
                  QuestionContent ans = new QuestionContent();
                  i = resultSet.getInt("ans" + k + "type");
                  ans.setType(i);
                  if (i == QuestionContent.CONTENT_ABSENT)
                  {
                     //set No content
                  }
                  else
                  {
                     if (i == QuestionContent.STRING_TYPE)
                     {
                        String temp = resultSet.getString("ans" + k + "string");
                        ans.setContentString(temp);
                        if (questionType == TestQuestionData.FIB_QUESTION_TYPE)
                        {
                           correctAnsForFillInTheBlanks.append(temp).append(",");
                        }
                     }
                     else if (i == QuestionContent.CONTENT_TYPE)
                     {
                        ans.setContentString(resultSet.getString("ans" + k + "content"));
                        ans.setContentString(fetchSymLinkForContent(ans.getContentString(), dbConnName, symLinkManager, userId));
                     }
                     //adding answers to array list
                     answers.add(ans);
                  }
               }
            }


            //Adding Arraylist to Response
            testResponse.setAnswers(answers);

            StringBuffer sb = new StringBuffer();
            logger.debug("FIB ONE : " + correctAnsForFillInTheBlanks.toString());
            if ((testResponse.getQuestiontype() == TestQuestionData.FIB_QUESTION_TYPE))
            {
               for (int u = 0; u < testResponse.getAnswers().size(); u++)
               {
                  QuestionContent temp = testResponse.getAnswers().get(u);
                  sb.append(temp.getContentString()).append(Globals.FIB_SEPARATOR);
               }
               if (sb.length() > 1)
               {
                  for (int k = 1; k <= Globals.FIB_SEPARATOR.length(); k++)
                  {
                     sb.deleteCharAt(sb.length() - 1);
                  }
               }
               logger.debug("Correct ans for FIB : " + sb.toString());
               testResponse.setCorrectAnswer(sb.toString());
               testDetails.setLastQuestionCorrectAns(testResponse.getCorrectAnswer());
            }
            else
            {
               testResponse.setCorrectAnswer(resultSet.getString("correctans"));
               testDetails.setLastQuestionCorrectAns(testResponse.getCorrectAnswer());
            }
            if (testResponse.getUserAns() != null)
            {
               userAns = testResponse.getUserAns().replaceAll(" ", "");
            }
            if (testResponse.getCorrectAnswer() != null)
            {
               correctAns = testResponse.getCorrectAnswer().replaceAll(" ", "");
            }

            if (userAns.compareToIgnoreCase(correctAns) == 0)
            {
               pieChartData.getCorrectAnswers().add(true);
               diffWisePoints = diffWisePoints + testResponse.getDifficultyLevel();
            }
            else
            {
               pieChartData.getCorrectAnswers().add(false);
            }
            maxPoints += testResponse.getDifficultyLevel();

            quizQuestions.add(testResponse);
         }
      }
      catch (SQLException e)
      {
         logger.debug("Database exception while trying to get records from database. : ", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Database exception occored. Please try again.");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.debug("Exception while trying to get records from database. : ", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal Error. Please try again.");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         try
         {
            resultSet.close();
         }
         catch (Exception e)
         {
            logger.error("Exception occoure when trying to close the result set.", e);
         }
         DbConnectionManager.releaseConnection(dbConnName, connection);
      }

      quizRequestResponse = new QuizRequestResponse();
      quizRequestResponse.setQuizQuestions(quizQuestions);

      TestResultResponse testResultResponse = new TestResultResponse();
      //      TestResultResponse testResultResponse = (TestResultResponse) response.getResponseObject();
      testResultResponse.setRequestResponse(quizRequestResponse);
      response.setResponseObject(testResultResponse);
      quizRequestResponse.setQuizId("jadfsjhgdfsgdsfa");
      TestResultResponseWithPieChart finalResult = new TestResultResponseWithPieChart();
      finalResult.setTestResultResponse(testResultResponse);
      finalResult.setTestResultPieChartData(pieChartData);
      response.setResponseObject(finalResult);

      //mega points calculation
      Boolean megaPointsValid = false;
      try
      {
         SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
         int correctAnsCount = Integer.parseInt(testResultResponse.getFinalScore());
         int totalQuestions = testResultResponse.getRequestResponse().getQuizQuestions().size();
         Date endTime = sdf.parse(testDetails.getTestEndTime());
         Date startTime = sdf.parse(testDetails.getTestStartTime());
         long maxDifference = endTime.getTime() - startTime.getTime();
         long actualDifference = new Date().getTime() - startTime.getTime();
         float percent = maxDifference / actualDifference;
         if (percent > 2f)
         {
            float percentCorrect = correctAnsCount / totalQuestions;
            if (percentCorrect >= 0.7f)
            {
               megaPointsValid = true;
            }
         }
      }
      catch (Exception e)
      {
         logger.error("Error while calculating if mega points is valid");
         megaPointsValid = false;
      }
      flowContext.putIntoContext(EzedKeyConstants.DIFF_WISE_POINTS, diffWisePoints);
      flowContext.putIntoContext(EzedKeyConstants.MEGA_TEST_POINTS, megaPointsValid);
      flowContext.putIntoContext(EzedKeyConstants.MAX_TEST_POINTS, maxPoints);
      return RuleDecisionKey.ON_SUCCESS;
   }

   public String fetchSymLinkForContent(String contentId, String dbConnName, SymLinkManager symLinkManager, String userId) throws Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchContentDetailsForId");
      ResultSet rs = null;
      String newLink = null;
      try
      {
         ps.setString(1, contentId);
         rs = ps.executeQuery();
         if (rs.next())
         {
            String contentlink = rs.getString("link");
            int actualContentType = rs.getInt("contenttype");
            if (betaOverride)
            {
               return contentlink;
            }
            String linkLocation = null;
            boolean isContent = false;
            if (actualContentType == ContentDetails.CONTENT_NOTE || actualContentType == ContentDetails.CONTENT_PDF || actualContentType == ContentDetails.CONTENT_IMAGE || actualContentType == ContentDetails.CONTENT_IMAGE_1)
            {
               linkLocation = newLinkLocationNotes;
               isContent = true;
            }
            else
            {
               linkLocation = newLinkLocationVideos;
            }

            //session id is userid
            newLink = symLinkManager.getSymLink(userId, contentId, contentlink, logger, linkLocation);
            if (isContent)
            {
               int index = newLink.indexOf(notesStartPoint);
               newLink = newLink.substring(index + 1);
            }
            else
            {
               int index = newLink.indexOf(vidStartPoint);
               newLink = newLink.substring(index + 1);
               String fileType = newLink.substring(newLink.lastIndexOf('.') + 1);
               if (fileType.compareToIgnoreCase("mov") == 0)
               {
                  fileType = "mp4";
               }
               newLink = fileType + ":" + newLink;
            }
            SymLinkObject linkObj = new SymLinkObject();
            linkObj.setLink(newLink);
         }
         return newLink;
      }
      catch (Exception e)
      {
         throw e;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
}
