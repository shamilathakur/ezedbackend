package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.analytics.data.GenericChartData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.TestDetails;
import com.intelli.ezed.data.UserProfile;
import com.intelli.ezed.utils.EzedHelper;

public class TestDetailsCreator extends SingletonRule
{

   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;
   private static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

   public TestDetailsCreator(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      UserProfile userProfile = (UserProfile) flowContext.getFromContext(EzedKeyConstants.USER_DETAILS);
      EzedRequestData requestData = (EzedRequestData) flowContext.getFromContext(EzedRequestData.EZED_REQUEST_DATA);
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      //      String startAtTime = (String) flowContext.getFromContext(EzedKeyConstants.REQUEST_IN_TIME);
      String startAtTime = sdf.format(new Date());
      String challengeId = (String) flowContext.getFromContext(EzedKeyConstants.CHALLENGE_ID);

      TestDetails testDetails = new TestDetails();
      flowContext.putIntoContext(TestDetails.TEST_DETAILS, testDetails);

      String testId = requestData.getTestId();
      String userid = userProfile.getUserid();
      testDetails.setUserid(userid);
      testDetails.setTestid(testId);
      testDetails.setTestStartTime(startAtTime);
      testDetails.setChallengeId(challengeId);

      ResultSet resultSet = null;
      Connection connection = DbConnectionManager.getConnectionByName(dbConnName);

      try
      {
         //select * from testlist where testid = ?
         PreparedStatement getTestDetails = DbConnectionManager.getPreparedStatement(dbConnName, connection, "SelectTestDetails");
         getTestDetails.setString(1, testId);
         resultSet = getTestDetails.executeQuery();
         if (resultSet != null && resultSet.next())
         {
            testDetails.setChapterId(resultSet.getString("chapterid"));
            testDetails.setCourseId(resultSet.getString("courseid"));
            testDetails.setTestTimeLimit(resultSet.getString("testtime"));
            testDetails.setNumberOfQuestions(resultSet.getInt("numberofquestion"));
            testDetails.setTestName(resultSet.getString("testname"));
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Error while reading resultset for test id : " + testId);
            }
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal Error while reading resultset.");
            return RuleDecisionKey.ON_FAILURE;
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while updating payment status", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while processing payment");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while updating payment status", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while processing payment");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         try
         {
            resultSet.close();
         }
         catch (SQLException e)
         {
            logger.error("Error while closing resultset", e);
         }
         DbConnectionManager.releaseConnection(dbConnName, connection);
      }

      SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
      SimpleDateFormat dfDisplay = new SimpleDateFormat("HH:mm:ss dd-MM-yyyy");
      String testTimeLimit = testDetails.getTestTimeLimit(); //hh:mm
      try
      {
         if (testTimeLimit.length() < 4 || Integer.parseInt(testTimeLimit) == 0)
         {
            logger.debug("Test time is in invalid format " + testTimeLimit);
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Invalid format of test time");
            return RuleDecisionKey.ON_FAILURE;
         }
      }
      catch (Exception e)
      {
         logger.debug("Test time is in invalid format " + testTimeLimit);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Invalid format of test time");
         return RuleDecisionKey.ON_FAILURE;
      }
      Long testEndTime = 0l;
      try
      {
         Date d = df.parse(startAtTime);
         testEndTime = d.getTime() + EzedHelper.getTimeInMillisec(testTimeLimit);
         String endTime = df.format(new Date(testEndTime));
         testDetails.setTestEndTime(endTime);
         if (logger.isDebugEnabled())
         {
            logger.debug("Start at time in millis :" + d.getTime() + " Start at time :" + startAtTime + " test end time in millis :" + testEndTime + " test end time :" + endTime);
         }
      }
      catch (ParseException e)
      {
         logger.error("Error while parsing test time, check values in database.");
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal Error.");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while parsing test time, check values in database.");
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal Error.");
         return RuleDecisionKey.ON_FAILURE;
      }

      GenericChartData genericChartData = new GenericChartData();
      genericChartData.getText().add("text");
      genericChartData.getValues().add("Test contains : " + testDetails.getNumberOfQuestions() + " Questions. Test will end at " + dfDisplay.format(new Date(testEndTime)));

      response.setResponseObject(genericChartData);
      response.setParameters(EzedTransactionCodes.SUCCESS, "Test details added in response.");

      return RuleDecisionKey.ON_SUCCESS;
   }
   //   public static void main(String[] args) throws ParseException
   //   {
   //      SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
   //      String currentTimeStamp = df.format(new Date(System.currentTimeMillis()));
   //      System.out.println("Current time : " + currentTimeStamp);
   //
   //      Date d = df.parse(currentTimeStamp);
   //
   //      String testTimeLimit = "04:30";
   //      Long testStartAtTime = d.getTime();
   //      System.out.println("current time in millis : " + testStartAtTime);
   //      Long testEndTime = 0l;
   //      try
   //      {
   //         testEndTime = testStartAtTime + EzedHelper.getTimeInMillisec(testTimeLimit);
   //         System.out.println(EzedHelper.getTimeInMillisec(testTimeLimit));
   //      }
   //      catch (Exception e)
   //      {
   //         lo
   //      }
   //      System.out.println("End time : " + testEndTime);
   //
   //      String endTime = df.format(new Date(testEndTime));
   //      System.out.println("End time in format : " + endTime);
   //
   //      System.out.println("current time in millis " + System.currentTimeMillis());
   //   }
}
