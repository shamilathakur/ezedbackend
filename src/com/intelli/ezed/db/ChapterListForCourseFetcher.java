package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.FrontendFetchData;
import com.intelli.ezed.data.Response;

public class ChapterListForCourseFetcher extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public ChapterListForCourseFetcher(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      String courseId = messageData.getParameter("courseid");
      if (courseId == null || courseId.compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Course id is invalid " + courseId);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid course id");
         return RuleDecisionKey.ON_FAILURE;
      }
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      String[] chapterArr = null;
      FrontendFetchData chapterData = new FrontendFetchData();
      response.setResponseObject(chapterData);
      response.setStatusCodeSend(false);
      chapterData.setAttribName("aaData");
      ArrayList<String[]> chapters = new ArrayList<String[]>();
      int chapterCount = 1;

      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchCourseDetail");
         ps.setString(1, courseId);
         rs = ps.executeQuery();
         if (rs.next())
         {
            String defChapter = rs.getString("defchapter");
            if (defChapter != null && defChapter.compareTo("") != 0)
            {
               chapterArr = new String[5];
               chapterArr[0] = Integer.toString(chapterCount);
               chapterArr[1] = defChapter;
               chapterArr[2] = null;
               chapterArr[3] = "DEMO CHAPTER";
               chapterArr[4] = "ACTIVE";
               chapters.add(chapterArr);
               chapterCount++;
            }

            String lmrChapter = rs.getString("lmr");
            if (lmrChapter != null && lmrChapter.compareTo("") != 0)
            {
               chapterArr = new String[5];
               chapterArr[0] = Integer.toString(chapterCount);
               chapterArr[1] = lmrChapter;
               chapterArr[2] = null;
               chapterArr[3] = "LMR CHAPTER";
               chapterArr[4] = "ACTIVE";
               chapters.add(chapterArr);
               chapterCount++;
            }

            String univQuestionPaper = rs.getString("questionpaperid");
            if (univQuestionPaper != null && univQuestionPaper.compareTo("") != 0)
            {
               chapterArr = new String[5];
               chapterArr[0] = Integer.toString(chapterCount);
               chapterArr[1] = univQuestionPaper;
               chapterArr[2] = null;
               chapterArr[3] = "UNIVERSITY QUESTION PAPER";
               chapterArr[4] = "ACTIVE";
               chapters.add(chapterArr);
               chapterCount++;
            }

            String univQuestionSolution = rs.getString("solvedpaperid");
            if (univQuestionSolution != null && univQuestionSolution.compareTo("") != 0)
            {
               chapterArr = new String[5];
               chapterArr[0] = Integer.toString(chapterCount);
               chapterArr[1] = univQuestionSolution;
               chapterArr[2] = null;
               chapterArr[3] = "UNIVERSITY QUESTION SOLUTION";
               chapterArr[4] = "ACTIVE";
               chapters.add(chapterArr);
               chapterCount++;
            }
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Course details could not be found for courseid " + courseId);
            }
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Course details could not be found");
            return RuleDecisionKey.ON_FAILURE;
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching course chapter details", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching course chapters");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching course chapter details", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching course chapters");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      String chapterName = null;
      for (int i = 0; i < chapters.size(); i++)
      {
         chapterArr = chapters.get(i);
         try
         {
            conn = DbConnectionManager.getConnectionByName(dbConnName);
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchChapterDetail");
            ps.setString(1, chapterArr[1]);
            rs = ps.executeQuery();
            if (rs.next())
            {
               chapterName = rs.getString("chaptername");
               if (chapterName == null)
               {
                  chapterName = "";
               }
               chapterArr[2] = chapterName;
            }
         }
         catch (Exception e)
         {
            logger.error("Error while fetching chapter name for chapter " + chapterArr[1], e);
            response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching chapter details");
            return RuleDecisionKey.ON_FAILURE;
         }
         finally
         {
            if (rs != null)
            {
               try
               {
                  rs.close();
               }
               catch (SQLException e)
               {
               }
            }
            DbConnectionManager.releaseConnection(dbConnName, conn);
         }
      }


      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchCourseChapterWithDetail");
         ps.setString(1, courseId);
         rs = ps.executeQuery();
         int i = chapterCount;
         while (rs.next())
         {
            chapterArr = new String[5];
            chapterArr[0] = Integer.toString(i);
            chapterArr[1] = rs.getString("chapterid");
            chapterArr[2] = rs.getString("chaptername");
            chapterArr[3] = "COURSE CHAPTER";
            chapterArr[4] = "ACTIVE";
            chapters.add(chapterArr);
            i++;
         }
         chapterCount = i;
         chapterData.setAttribList(chapters);
         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching course chapter details", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching course chapters");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching course chapter details", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching course chapters");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
      return RuleDecisionKey.ON_SUCCESS;
   }
}
