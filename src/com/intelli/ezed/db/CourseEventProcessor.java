package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.bulkupload.data.ContentDetails;
import com.intelli.ezed.data.ActionData;
import com.intelli.ezed.data.EventData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.GoalData;
import com.intelli.ezed.data.MasterActionRecords;
import com.intelli.ezed.data.Response;

public class CourseEventProcessor extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public CourseEventProcessor(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      EventData eventData = (EventData) flowContext.getFromContext(EzedKeyConstants.EVENT_OBJECT);
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      String userid = (String) flowContext.getFromContext(EzedKeyConstants.USER_ID);
      ActionData actionData = MasterActionRecords.fetchRecord(eventData.getActionId());
      boolean isCourseToBeAdded = false;
      if (logger.isDebugEnabled())
      {
         logger.debug("Action is a one time action. First checking count from db");
      }
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      String goalDate = null;
      int goalAchieved = GoalData.GOAL_INCOMPLETE;

      boolean isVideo = false;
      if (actionData.getPoints() != 0)
      {
         try
         {
            if (eventData.getChapterId() != null)
            {
               ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchEventCount");
               ps.setString(1, userid);
               ps.setString(2, eventData.getActionId());
               ps.setString(3, eventData.getCourseId());
               ps.setString(4, eventData.getChapterId());
               ps.setString(5, eventData.getContentId());
            }
            else
            {
               ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchEventCountCourseLevelTest");
               ps.setString(1, userid);
               ps.setString(2, eventData.getActionId());
               ps.setString(3, eventData.getCourseId());
               ps.setString(4, eventData.getContentId());
            }
            rs = ps.executeQuery();
            if (rs.next())
            {
               int count = rs.getInt(1);
               goalDate = rs.getString(2);
               goalAchieved = rs.getInt(3);
               if (count == 1)
               {
                  isCourseToBeAdded = true;
               }
               int contentType = rs.getInt("contenttype");
               if (contentType == ContentDetails.CONTENT_VIDEO || contentType == ContentDetails.CONTENT_YOUTUBE)
               {
                  isVideo = true;
               }
            }
            else
            {
               if (logger.isDebugEnabled())
               {
                  logger.debug("No entry found in db for userid " + userid + " and action id " + eventData.getActionId());
               }
               response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Content could not be found");
               return RuleDecisionKey.ON_FAILURE;
            }
         }
         catch (SQLException e)
         {
            logger.error("Error while fetching count from event db for userid " + userid + " and action id " + eventData.getActionId(), e);
         }
         catch (Exception e)
         {
            logger.error("Error while fetching count from event db for userid " + userid + " and action id " + eventData.getActionId(), e);
         }
         finally
         {
            if (rs != null)
            {
               try
               {
                  rs.close();
               }
               catch (SQLException e)
               {
               }
            }
            DbConnectionManager.releaseConnection(dbConnName, conn);
         }
         if (actionData.getActionType() != ActionData.ONE_TIME_ACTION)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Action is a repeatable action. Directly adding points");
            }
            isCourseToBeAdded = true;
         }

         if (isCourseToBeAdded && isVideo)
         {
            conn = DbConnectionManager.getConnectionByName(dbConnName);
            ps = null;
            try
            {
               ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdateCoursePoints");
               ps.setInt(1, actionData.getPoints());
               ps.setString(2, userid);
               ps.setString(3, eventData.getCourseId());
               int rows = ps.executeUpdate();
               if (logger.isDebugEnabled())
               {
                  logger.debug(rows + " rows updated in db while incrementing points for userid " + userid + " and course " + eventData.getCourseId());
               }
            }
            catch (SQLException e)
            {
               logger.error("Error while incrementing points for userid " + userid + " and course " + eventData.getCourseId(), e);
            }
            catch (Exception e)
            {
               logger.error("Error while incrementing points for userid " + userid + " and course " + eventData.getCourseId(), e);
            }
            finally
            {
               DbConnectionManager.releaseConnection(dbConnName, conn);
            }

            conn = DbConnectionManager.getConnectionByName(dbConnName);
            ps = null;
            try
            {
               ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdateUserPoints");
               ps.setInt(1, actionData.getPoints());
               ps.setString(2, userid);
               int rows = ps.executeUpdate();
               if (logger.isDebugEnabled())
               {
                  logger.debug(rows + " rows updated in db while incrementing points for userid " + userid);
               }
            }
            catch (SQLException e)
            {
               logger.error("Error while incrementing points for userid " + userid, e);
            }
            catch (Exception e)
            {
               logger.error("Error while incrementing points for userid " + userid, e);
            }
            finally
            {
               DbConnectionManager.releaseConnection(dbConnName, conn);
            }
         }
      }

      //Code for checking if his goal is valid for achievement
      if (goalAchieved == GoalData.GOAL_COMPLETE)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Goal is already completed");
         }
         return RuleDecisionKey.ON_SUCCESS;
      }

      SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
      Calendar todayCal = Calendar.getInstance();
      todayCal.setTime(new Date(System.currentTimeMillis()));

      Calendar goalCal = Calendar.getInstance();
      try
      {
         goalCal.setTime(sdf.parse(goalDate));
         if (goalCal.after(todayCal))
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Goal is valid for achievement");
            }
            return RuleDecisionKey.IF_VALIDGOAL;
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Goal date has already passed");
            }
         }
      }
      catch (ParseException e)
      {
         logger.error("Goal date from db could not be parsed", e);
         return RuleDecisionKey.ON_SUCCESS;
      }
      catch (Exception e)
      {
         logger.error("Goal date from db could not be parsed", e);
         return RuleDecisionKey.ON_SUCCESS;
      }
      return RuleDecisionKey.ON_SUCCESS;
   }

}
