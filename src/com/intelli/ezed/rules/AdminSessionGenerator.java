package com.intelli.ezed.rules;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.session.SessionException;
import com.a.helper.session.SessionManager;
import com.a.helper.utils.StringLockerUtil;
import com.intelli.ezed.data.AdminLoginResponse;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.utils.Globals;

public class AdminSessionGenerator extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public AdminSessionGenerator(String ruleName)
   {
      super(ruleName);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      SessionManager sessionManager = (SessionManager) flowContext.getFromContext(EzedKeyConstants.ADMIN_SESSION_MANAGER);
      String sessionId = null;
      try
      {
         sessionId = sessionManager.getSessionId();
         if (logger.isDebugEnabled())
         {
            logger.debug("Test Session id generated " + sessionId);
         }
         AdminLoginResponse adminLoginResponse = (AdminLoginResponse) response.getResponseObject();
         adminLoginResponse.setSessionId(sessionId);
         flowContext.putIntoContext(EzedKeyConstants.ADMIN_SESSION_ID, sessionId);
      }
      catch (SessionException e)
      {
         logger.error("Session id could not be generated", e);
         response.setParameters(EzedTransactionCodes.SESSION_ERROR_OCCURRED, "Error while generating session");
         return RuleDecisionKey.ON_FAILURE;
      }
      try
      {
         StringLockerUtil.lockString(sessionId, Globals.ADMIN_SESSION_LOCK_SET);
      }
      catch (InterruptedException e)
      {
         logger.error("Admin session could not be locked " + sessionId, e);
         response.setParameters(EzedTransactionCodes.SESSION_ERROR_OCCURRED, "Error while generating session");
         return RuleDecisionKey.ON_FAILURE;
      }
      return RuleDecisionKey.ON_SUCCESS;
   }
}
