package com.intelli.ezed.db;

import java.sql.SQLException;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.GoalModificationData;

public class GoalChapterDeletionHandler extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String actionId;

   public GoalChapterDeletionHandler(String ruleName, String dbConnName, String actionId)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
      this.actionId = actionId;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      GoalModificationData data = (GoalModificationData) flowContext.getFromContext(EzedKeyConstants.GOAL_MODIFICATION_DATA);
      try
      {
         if (data.getChapterId() != null)
         {
            data.populateChapterDetails(dbConnName, logger);
            data.handleChapterGoalDeletions(dbConnName, logger, actionId);
         }
         else
         {
            data.populateTestDetails(dbConnName, logger);
            data.handleChapterGoalDeletions(dbConnName, logger, actionId);
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while deleting goals from calendar", e);
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while deleting goals from calendar", e);
         return RuleDecisionKey.ON_FAILURE;
      }

      return RuleDecisionKey.ON_SUCCESS;
   }

}
