//package com.intelli.ezed.frontend.utils;
//
//import java.sql.Connection;
//import java.sql.DriverManager;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//
//import org.apache.log4j.Logger;
//
//import com.ezed.utils.dbutil.ConnectionFactory;
//
//public class ContentDbInserter
//{
//   private static final int SUCCESS = 0;
//   private static final int NO_CONNECTION = 101;
//   private static final int NO_STATEMENT = 102;
//   private static final int NO_ROWS_INSERTED = 104;
//   private static final int NO_ROWS_INSERTED_LEARNINGPATH = 105;
//
//   private static PrepareContentId prepareContentId;
//
//   //use your own logger name at this position
//   //  private String loggerName = EzedLoggerName.SERVICE_LOG;
//   private Logger logger = Logger.getLogger(ContentDbInserter.class);
//
//   private String contentId;
//   private String contentname;
//   private String link;
//   private String description;
//   private int contenttype;
//   private String chapterId;
//   private String contentShowFlag;
//
//
//   static
//   {
//      prepareContentId = new PrepareContentId("CON2", "");
//   }
//
//
//   public String getNextContentid()
//   {
//      String contentId = prepareContentId.getNextId();
//      setContentId(contentId);
//      return contentId;
//   }
//
//
//   public String getContentId()
//   {
//      return contentId;
//   }
//
//
//   public void setContentId(String contentId)
//   {
//      this.contentId = contentId;
//   }
//
//
//   public String getContentname()
//   {
//      return contentname;
//   }
//
//   public void setContentname(String contentname)
//   {
//      this.contentname = contentname;
//   }
//   public String getLink()
//   {
//      return link;
//   }
//   public void setLink(String link)
//   {
//      this.link = link;
//   }
//   public String getDescription()
//   {
//      return description;
//   }
//   public void setDescription(String description)
//   {
//      this.description = description;
//   }
//   public int getContenttype()
//   {
//      return contenttype;
//   }
//   public void setContenttype(int contenttype)
//   {
//      this.contenttype = contenttype;
//   }
//
//   public int insertIntoDatabaseAndLearningPath()
//   {
//      //change how you get your connection here. you can use your own database connection pool or whatever.
//      Connection connection = getDbConnection();
//      if (connection == null)
//      {
//         return NO_CONNECTION;
//      }
//
//      //change how you get your prepared statement here. you can put the connection in some configuration files for any other setup.
//      PreparedStatement preparedStatement = getStatement(connection);
//      if (preparedStatement == null)
//      {
//         return NO_STATEMENT;
//      }
//
//      try
//      {
//         preparedStatement.setString(1, getContentId());
//         preparedStatement.setString(2, getContentname().toUpperCase());
//         preparedStatement.setString(3, getLink());
//         preparedStatement.setString(4, getDescription());
//         preparedStatement.setInt(5, getContenttype());
//         int i = preparedStatement.executeUpdate();
//
//         if (i == 1)
//         {
//            if (logger.isDebugEnabled())
//            {
//               logger.debug("Cantent added in database.");
//            }
//         }
//         else
//         {
//            if (logger.isDebugEnabled())
//            {
//               logger.debug("no data inserted in database status : " + i);
//            }
//            return NO_ROWS_INSERTED;
//         }
//      }
//      catch (SQLException e)
//      {
//         System.out.println(e);
//         logger.error("Database Exception occoured while inserting content in database.", e);
//         return EzedFrontEndTransactionCodes.DB_EXCEPTION_OCCURRED;
//      }
//      catch (Exception e)
//      {
//         logger.error("Exception occoured while inserting content in database.", e);
//         return EzedFrontEndTransactionCodes.INTERNAL_ERROR;
//      }
//
//      //change how you get your prepared statement here. you can put the connection in some configuration files for any other setup.
//      preparedStatement = insertLearningPathStatement(connection);
//      if (preparedStatement == null)
//      {
//         return NO_STATEMENT;
//      }
//
//      try
//      {
//         preparedStatement.setString(1, getChapterId());
//         preparedStatement.setString(2, getContentId());
//         preparedStatement.setString(3, getContentShowFlag());
//         preparedStatement.setString(4, getChapterId());
//         int i = preparedStatement.executeUpdate();
//
//         if (i == 1)
//         {
//            if (logger.isDebugEnabled())
//            {
//               logger.debug("Content added in database for learningpath.");
//            }
//         }
//         else
//         {
//            if (logger.isDebugEnabled())
//            {
//               logger.debug("no data inserted in database for learingpath status : " + i);
//            }
//            return NO_ROWS_INSERTED_LEARNINGPATH;
//         }
//      }
//      catch (SQLException e)
//      {
//         System.out.println(e);
//         logger.error("Database Exception occoured while inserting content in learningpath database.", e);
//         return EzedFrontEndTransactionCodes.DB_EXCEPTION_OCCURRED;
//      }
//      catch (Exception e)
//      {
//         logger.error("Exception occoured while inserting content in learningpath database.", e);
//         return EzedFrontEndTransactionCodes.INTERNAL_ERROR;
//      }
//      return SUCCESS;
//   }
//
//   public int insertIntoDatabase()
//   {
//      //change how you get your connection here. you can use your own database connection pool or whatever.
//      Connection connection = getDbConnection();
//      if (connection == null)
//      {
//         return NO_CONNECTION;
//      }
//
//      //change how you get your prepared statement here. you can put the connection in some configuration files for any other setup.
//      PreparedStatement preparedStatement = getStatement(connection);
//      if (preparedStatement == null)
//      {
//         return NO_STATEMENT;
//      }
//
//      try
//      {
//         preparedStatement.setString(1, getContentId());
//         preparedStatement.setString(2, getContentname().toUpperCase());
//         preparedStatement.setString(3, getLink());
//         preparedStatement.setString(4, getDescription());
//         preparedStatement.setInt(5, getContenttype());
//         int i = preparedStatement.executeUpdate();
//
//         if (i == 1)
//         {
//            if (logger.isDebugEnabled())
//            {
//               logger.debug("Cantent added in database.");
//            }
//         }
//         else
//         {
//            if (logger.isDebugEnabled())
//            {
//               logger.debug("no data inserted in database status : " + i);
//            }
//            return NO_ROWS_INSERTED;
//         }
//      }
//      catch (SQLException e)
//      {
//         System.out.println(e);
//         logger.error("Database Exception occoured while inserting content in database.", e);
//         return EzedFrontEndTransactionCodes.DB_EXCEPTION_OCCURRED;
//      }
//      catch (Exception e)
//      {
//         logger.error("Exception occoured while inserting content in database.", e);
//         return EzedFrontEndTransactionCodes.INTERNAL_ERROR;
//      }
//      return SUCCESS;
//   }
//
//   public static void main(String[] args)
//   {
//      ContentDbInserter contentDbInserter = new ContentDbInserter();
//
//      //      contentDbInserter.setContentId(contentDbInserter.getNextContentid());
//      //      contentDbInserter.setContentname("TestContent");
//      //      contentDbInserter.setContenttype(1);
//      //      contentDbInserter.setDescription("Testing party");
//      //      contentDbInserter.setLink("/something/somethingsomthing");
//      //
//      //      //contentDbInserter.insertIntoDatabase();
//
//      contentDbInserter.setContentId("CON20000000136518795");
//      //      contentDbInserter.fetchFromDatabase();
//      //
//      //      contentDbInserter.setDescription("change");
//      //      contentDbInserter.persistIntoDatabase();
//
//      contentDbInserter.deleteFromDatabase();
//
//   }
//
//
//   public int persistIntoDatabase()
//   {
//      //change how you get your connection here. you can use your own database connection pool or whatever.
//      Connection connection = getDbConnection();
//      if (connection == null)
//      {
//         return NO_CONNECTION;
//      }
//
//      //change how you get your prepared statement here. you can put the connection in some configuration files for any other setup.
//      PreparedStatement preparedStatement = getUpdateStatement(connection);
//      if (preparedStatement == null)
//      {
//         return NO_STATEMENT;
//      }
//
//      try
//      {
//         preparedStatement.setString(1, getContentname().toUpperCase());
//         preparedStatement.setString(2, getLink());
//         preparedStatement.setString(3, getDescription());
//         preparedStatement.setInt(4, getContenttype());
//         preparedStatement.setString(5, getNextContentid());
//         int i = preparedStatement.executeUpdate();
//
//         if (i == 1)
//         {
//            if (logger.isDebugEnabled())
//            {
//               logger.debug("Cantent added in database.");
//            }
//         }
//         else
//         {
//            if (logger.isDebugEnabled())
//            {
//               logger.debug("no data inserted in database status : " + i);
//            }
//            return NO_ROWS_INSERTED;
//         }
//      }
//      catch (SQLException e)
//      {
//         logger.error("Database Exception occoured while inserting content in database.", e);
//         return EzedFrontEndTransactionCodes.DB_EXCEPTION_OCCURRED;
//      }
//      catch (Exception e)
//      {
//         logger.error("Exception occoured while inserting content in database.", e);
//         return EzedFrontEndTransactionCodes.INTERNAL_ERROR;
//      }
//      return EzedFrontEndTransactionCodes.SUCCESS;
//   }
//
//   public int fetchFromDatabase()
//   {
//      //change how you get your connection here. you can use your own database connection pool or whatever.
//      Connection connection = getDbConnection();
//      if (connection == null)
//      {
//         return NO_CONNECTION;
//      }
//
//      //change how you get your prepared statement here. you can put the connection in some configuration files for any other setup.
//      PreparedStatement preparedStatement = getFetchStatement(connection);
//      if (preparedStatement == null)
//      {
//         return NO_STATEMENT;
//      }
//
//      ResultSet resultSet = null;
//
//      try
//      {
//         //contentid,contentname,link,description,contenttype
//         preparedStatement.setString(1, getContentId());
//         resultSet = preparedStatement.executeQuery();
//
//         if (resultSet.next())
//         {
//            setContentId(resultSet.getString("contentid"));
//            setContentname(resultSet.getString("contentname"));
//            setLink(resultSet.getString("link"));
//            setDescription(resultSet.getString("description"));
//            setContenttype(resultSet.getInt("contenttype"));
//         }
//
//      }
//      catch (SQLException e)
//      {
//         System.out.println(e);
//         logger.error("Database Exception occoured while fetching content in database.", e);
//         return EzedFrontEndTransactionCodes.DB_EXCEPTION_OCCURRED;
//      }
//      catch (Exception e)
//      {
//         System.out.println(e);
//         logger.error("Exception occoured while fetching content in database.", e);
//         return EzedFrontEndTransactionCodes.INTERNAL_ERROR;
//      }
//      finally
//      {
//         try
//         {
//            resultSet.close();
//         }
//         catch (Exception e)
//         {
//            logger.error("Exception occoured while closing result set.", e);
//         }
//      }
//
//
//      return EzedFrontEndTransactionCodes.SUCCESS;
//   }
//
//   private Connection getDbConnection()
//   {
//      try
//      {
//         String url = "jdbc:postgresql://localhost:5432/ezed2";
//         Class.forName("org.postgresql.Driver");
//         //   return DriverManager.getConnection(url, "postgres", "postgres");
//         return ConnectionFactory.getConnection();
//      }
//      catch (Exception e)
//      {
//         return null;
//      }
//   }
//
//   private PreparedStatement getStatement(Connection conn)
//   {
//      try
//      {
//         return (conn.prepareStatement("insert into contentlist (contentid,contentname,link,description,contenttype) values (?,?,?,?,?)"));
//      }
//      catch (Exception e)
//      {
//         return null;
//      }
//   }
//
//   private PreparedStatement getStatement(Connection conn)
//   {
//      try
//      {
//         return (conn.prepareStatement("insert into contentlist (contentid,contentname,link,description,contenttype) values (?,?,?,?,?)"));
//      }
//      catch (Exception e)
//      {
//         return null;
//      }
//   }
//
//   private PreparedStatement insertLearningPathStatement(Connection conn)
//   {
//      try
//      {
//         return (conn.prepareStatement("insert into learningpath(chapterid,contentid,contentshowflag,sequenceno) select ?,?,?,coalesce(max(sequenceno),0)+1 from learningpath where chapterid=?"));
//      }
//      catch (Exception e)
//      {
//         return null;
//      }
//   }
//
//   private PreparedStatement getUpdateStatement(Connection conn)
//   {
//      try
//      {
//         return (conn.prepareStatement("update contentlist set contentname = ?,link = ?,description = ?,contenttype = ? where contentid = ?"));
//      }
//      catch (Exception e)
//      {
//         return null;
//      }
//   }
//
//   private PreparedStatement getFetchStatement(Connection conn)
//   {
//      try
//      {
//         return (conn.prepareStatement("select * from contentlist where contentid = ?"));
//      }
//      catch (Exception e)
//      {
//         return null;
//      }
//   }
//
//   public int deleteFromDatabase()
//   {
//      //change how you get your connection here. you can use your own database connection pool or whatever.
//      Connection connection = getDbConnection();
//      if (connection == null)
//      {
//         return EzedFrontEndTransactionCodes.NO_CONNECTION;
//      }
//
//      //change how you get your prepared statement here. you can put the connection in some configuration files for any other setup.
//      PreparedStatement preparedStatement = getDropStatement(connection);
//      if (preparedStatement == null)
//      {
//         return EzedFrontEndTransactionCodes.NO_STATEMENT;
//      }
//
//      //website,univname,phonenumber,cityid
//
//      try
//      {
//         preparedStatement.setString(1, getContentId());
//
//         int i = preparedStatement.executeUpdate();
//         System.out.println("Delted response is :" + i);
//
//
//         if (i > 0)
//         {
//            if (logger.isDebugEnabled())
//            {
//               logger.debug("Content deleted from database.");
//            }
//         }
//         else
//         {
//            if (logger.isDebugEnabled())
//            {
//               logger.debug("no data deleted from database status : " + i);
//            }
//            return EzedFrontEndTransactionCodes.NO_ROWS_INSERTED;
//         }
//
//      }
//      catch (SQLException e)
//      {
//         System.out.println(e);
//         if (e.getMessage().contains("foreign key constraint"))
//         {
//            if (logger.isInfoEnabled())
//            {
//               logger.info("Content id provided is not present in the system.");
//            }
//            return EzedFrontEndTransactionCodes.INVALID_MESSAGE_PARAMETER;
//         }
//
//         logger.error("Database exception occoured while Deleting Content from database.", e);
//         return EzedFrontEndTransactionCodes.DB_EXCEPTION_OCCURRED;
//
//      }
//      catch (Exception e)
//      {
//         logger.error("Exception occourd while deleting Content details from database.", e);
//         return EzedFrontEndTransactionCodes.INTERNAL_ERROR;
//      }
//
//
//      return EzedFrontEndTransactionCodes.SUCCESS;
//   }
//
//   private PreparedStatement getDropStatement(Connection conn)
//   {
//      try
//      {
//         return (conn.prepareStatement("delete from contentlist where contentid=?"));
//      }
//      catch (Exception e)
//      {
//         return null;
//      }
//   }
//
//
//   public String getChapterId()
//   {
//      return chapterId;
//   }
//
//
//   public void setChapterId(String chapterId)
//   {
//      this.chapterId = chapterId;
//   }
//
//
//   public String getContentShowFlag()
//   {
//      return contentShowFlag;
//   }
//
//
//   public void setContentShowFlag(String contentShowFlag)
//   {
//      this.contentShowFlag = contentShowFlag;
//   }
//}
