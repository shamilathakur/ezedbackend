package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.ActionData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.MasterActionRecords;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.UserProfile;

public class TestChalengeeHandler extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String actionId;
   private static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

   public TestChalengeeHandler(String ruleName, String dbConnName, String actionId)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
      this.actionId = actionId;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      UserProfile profile = (UserProfile) flowContext.getFromContext(EzedKeyConstants.USER_DETAILS);
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      String challengeId = (String) flowContext.getFromContext(EzedKeyConstants.CHALLENGE_ID);
      if (challengeId == null)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Test is not a challenge acceptance. Not giving points");
         }
         return RuleDecisionKey.ON_FAILURE;
      }

      Connection conn = null;
      PreparedStatement ps = null;
      ResultSet rs = null;
      String chalengerUserId = null;
      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchChallengeDetailsForId");
         ps.setString(1, challengeId);
         rs = ps.executeQuery();
         if (rs.next())
         {
            chalengerUserId = rs.getString("userid");
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Challenge details not found against challenge id " + challengeId);
            }
            return RuleDecisionKey.ON_FAILURE;
         }
      }
      catch (Exception e)
      {
         logger.error("Error while fetching challenge details for id " + challengeId, e);
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
               logger.error("Error while closing resultset while fetching challenge details", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      UserProfile userProfile = new UserProfile();
      userProfile.setUserid(chalengerUserId);
      try
      {
         userProfile.populateUserProfile(dbConnName, logger);
      }
      catch (Exception e)
      {
         logger.error("Error while populating user profile for id " + chalengerUserId, e);
      }

      ActionData actionData = MasterActionRecords.fetchRecord(actionId);

      StringBuffer sb = new StringBuffer();
      sb.append(userProfile.getName());
      sb.append("'s challenge was accepted by ");
      sb.append(profile.getName());
      String challengeeStream = sb.toString();

      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdateUserPoints");
         ps.setInt(1, actionData.getPoints());
         ps.setString(2, userProfile.getUserid());
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows updated in db while incrementing points for userid " + userProfile.getUserid());
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while incrementing points for userid " + userProfile.getUserid(), e);
      }
      catch (Exception e)
      {
         logger.error("Error while incrementing points for userid " + userProfile.getUserid(), e);
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertUserStreamAll");
         ps.setString(1, challengeeStream);
         ps.setString(2, userProfile.getUserid());
         ps.setString(3, sdf.format(new Date()));
         ps.setString(4, null);
         ps.setString(5, null);
         ps.setString(6, null);
         ps.setString(7, actionId);
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in db while inserting stream for user " + userProfile.getUserid());
         }
      }
      catch (Exception e)
      {
         logger.error("Error inserting stream entry for " + userProfile.getUserid(), e);
         logger.error("INSERT FAILED " + challengeeStream);
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
      return RuleDecisionKey.ON_SUCCESS;
   }

}
