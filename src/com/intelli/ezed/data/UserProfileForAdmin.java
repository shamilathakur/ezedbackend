package com.intelli.ezed.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.slf4j.Logger;

import com.intelli.ezed.utils.EzedHelper;

public class UserProfileForAdmin extends ResponseObject
{
   private static final String POPULATE_PS_NAME = "GetUserDetails";

   private String userid;
   private String mobileNumber;
   private String firstName;
   private String middleName;
   private String lastName;
   private String gender;
   private String dateOfBirth;
   private String streetAdress;
   private String landmark;
   private String pincode;
   private String univName;
   private String collegeName;
   private String city;
   private String state;
   private String country;
   private String profileImage;
   private String invitedBy;
   private String stream;
   private String degree;

   private String loginWith;
   private String regWith;
   private int userPoints;
   private int chkflag;

   /* 
    * This integer type is used determine the login or registration type of a user.
    * it can take values as follows
    *  0 - registration vi Ezed i.e by entering the userid and password at ezed site.  
    *  1 - registration vi facebook or gmail or any other site.
    */
   private int regLoginType;

   /*
    * List containing courseIds of all the purchased courses.
    * List will be fetched from 'usercourse' table. 
    * It could be objects of CourseDetails or Front end can fetch them directly from
    * database. 
    */


   public int getUserPoints()
   {
      return userPoints;
   }

   public void setUserPoints(int userPoints)
   {
      this.userPoints = userPoints;
   }

   public String getProfileImage()
   {
      return profileImage;
   }

   public void setProfileImage(String profileImage)
   {
      this.profileImage = profileImage;
   }

   public String getCity()
   {
      return city;
   }

   public void setCity(String city)
   {
      this.city = city;
   }

   public String getState()
   {
      return state;
   }

   public void setState(String state)
   {
      this.state = state;
   }

   public String getCountry()
   {
      return country;
   }

   public void setCountry(String country)
   {
      this.country = country;
   }

   public String getMiddleName()
   {
      return middleName;
   }

   public void setMiddleName(String middleName)
   {
      this.middleName = middleName;
   }

   public String getLoginWith()
   {
      return loginWith;
   }

   public int getRegLoginType()
   {
      return regLoginType;
   }

   public void setRegLoginType(int regLoginType)
   {
      this.regLoginType = regLoginType;
   }

   public void setLoginWith(String loginWith)
   {
      this.loginWith = loginWith;
   }

   public int getChkflag()
   {
      return chkflag;
   }

   public void setChkflag(int chkflag)
   {
      this.chkflag = chkflag;
   }

   public String getUserid()
   {
      return userid;
   }
   public String getMobileNumber()
   {
      return mobileNumber;
   }
   public String getFirstName()
   {
      return firstName;
   }
   public String getLastName()
   {
      return lastName;
   }
   public String getGender()
   {
      return gender;
   }
   public String getDateOfBirth()
   {
      return dateOfBirth;
   }
   public String getStreetAdress()
   {
      return streetAdress;
   }
   public String getLandmark()
   {
      return landmark;
   }
   public String getPincode()
   {
      return pincode;
   }

   public String getUnivName()
   {
      return univName;
   }
   public String getCollegeName()
   {
      return collegeName;
   }

   public void setUserid(String userid)
   {
      this.userid = userid.toLowerCase();
   }

   public void setMobileNumber(String mobileNumber)
   {
      this.mobileNumber = mobileNumber;
   }

   public void setFirstName(String firstName)
   {
      this.firstName = firstName;
   }

   public void setLastName(String lastName)
   {
      this.lastName = lastName;
   }

   public void setGender(String gender)
   {
      this.gender = gender;
   }

   public void setDateOfBirth(String dateOfBirth)
   {
      this.dateOfBirth = dateOfBirth;
   }

   public void setStreetAdress(String streetAdress)
   {
      this.streetAdress = streetAdress;
   }

   public void setLandmark(String landmark)
   {
      this.landmark = landmark;
   }


   public String getRegWith()
   {
      return regWith;
   }

   public void setRegWith(String regWith)
   {
      this.regWith = regWith;
   }

   public void setPincode(String pincode)
   {
      this.pincode = pincode;
   }

   public void setUnivName(String univName)
   {
      this.univName = univName;
   }

   public void setCollegeName(String collegeName)
   {
      this.collegeName = collegeName;
   }

   @Override
   public void prepareResponse() throws Exception
   {
      this.put("userid", getUserid());
      this.put("firstname", getFirstName());
      this.put("lastname", getLastName());
      this.put("middlename", getMiddleName());
      this.put("dob", getDateOfBirth());
      this.put("mobilenumber", getMobileNumber());
      this.put("gender", getGender());
      this.put("streetaddress", getStreetAdress());
      this.put("landmark", getLandmark());
      this.put("pincode", getPincode());
      this.put("university", getUnivName());
      this.put("college", getCollegeName());
      this.put("profileimage", getProfileImage());
      this.put("city", getCity());
      this.put("state", getState());
      this.put("country", getCountry());
      this.put("stream", getStream());
      this.put("degree", getDegree());
      int totalPoints = 0;
      totalPoints += getUserPoints();
      this.put("points", totalPoints);
   }

   public int populateUserProfile(String dbConnName, Logger logger) throws Exception
   {
      if (getUserid() == null)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("userid is null. Details could not be populated");
         }
         return EzedTransactionCodes.INTERNAL_ERROR;
      }
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, POPULATE_PS_NAME);
         ps.setString(1, getUserid());
         rs = ps.executeQuery();
         if (rs != null && rs.next())
         {
            String value = rs.getString("mobilenumber");
            if (value != null && value.trim().compareTo("") != 0)
            {
               //               setMobileNumber(EzedHelper.getTenDigMobNo(value));
               setMobileNumber(value);
            }

            setChkflag(rs.getInt("chkflag"));
            setFirstName(rs.getString("firstname"));
            setLastName(rs.getString("lastname"));

            value = rs.getString("middlename");
            if (value == null || value.trim().compareTo("") == 0)
            {
               value = null;
            }
            setMiddleName(value);

            value = rs.getString("dob");
            if (value == null || value.trim().compareTo("") == 0)
            {
               value = null;
            }
            this.setDateOfBirth(value);

            value = rs.getString("gender");
            if (value == null || value.trim().compareTo("") == 0)
            {
               value = null;
            }
            this.setGender(value);

            value = rs.getString("streetaddress");
            if (value == null || value.trim().compareTo("") == 0)
            {
               value = null;
            }
            this.setStreetAdress(value);

            value = rs.getString("landmark");
            if (value == null || value.trim().compareTo("") == 0)
            {
               value = null;
            }
            this.setLandmark(value);

            value = rs.getString("cityname");
            if (value == null || value.trim().compareTo("") == 0)
            {
               value = null;
            }
            this.setCity(value);

            value = rs.getString("statename");
            if (value == null || value.trim().compareTo("") == 0)
            {
               value = null;
            }
            this.setState(value);

            value = rs.getString("countryname");
            if (value == null || value.trim().compareTo("") == 0)
            {
               value = null;
            }
            this.setCountry(value);

            value = rs.getString("pincode");
            if (value == null || value.trim().compareTo("") == 0)
            {
               value = null;
            }
            this.setPincode(value);

            value = rs.getString("univname");
            if (value == null || value.trim().compareTo("") == 0)
            {
               value = null;
            }
            this.setUnivName(value);

            value = rs.getString("collegename");
            if (value == null || value.trim().compareTo("") == 0)
            {
               value = null;
            }
            this.setCollegeName(value);

            value = rs.getString("profileimage");
            if (value == null || value.trim().compareTo("") == 0)
            {
               value = null;
            }
            this.setProfileImage(value);
            this.setUserPoints(rs.getInt("points"));
            this.setDegree(rs.getString("degree"));
            this.setStream(rs.getString("stream"));

            //return EzedTransactionCOdes.SUCCESS;
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("userid details not found in DB");
            }
            return EzedTransactionCodes.INTERNAL_ERROR;
         }
      }
      catch (SQLException e)
      {
         throw e;
      }
      catch (Exception e)
      {
         throw e;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
               logger.error("Error while closing resultset while populating user profile", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
      return EzedTransactionCodes.SUCCESS;
   }

   public String getInvitedBy()
   {
      return invitedBy;
   }

   public void setInvitedBy(String invitedBy)
   {
      this.invitedBy = invitedBy;
   }

   public String getStream()
   {
      return stream;
   }

   public void setStream(String stream)
   {
      this.stream = stream;
   }

   public String getDegree()
   {
      return degree;
   }

   public void setDegree(String degree)
   {
      this.degree = degree;
   }
}