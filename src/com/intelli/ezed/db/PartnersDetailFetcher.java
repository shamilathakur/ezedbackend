package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.FrontendFetchData;
import com.intelli.ezed.data.PartnerData;
import com.intelli.ezed.data.Response;

/**
 * @author Kunal Discription : This class is written for fetching all the
 *         details of the partner Can be modified later for any report to be
 *         generated related to the scratch card functionality ..
 */

public class PartnersDetailFetcher extends SingletonRule {

	private String dbConnName;
	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
	SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyyMMddHHmmss");
	SimpleDateFormat formatTo = new SimpleDateFormat("yyyy-MM-dd");
	private final String PARTNER_LIST = "SELECT partner.srno,partner.partnerid,"
			+ "partner.contactperson,partner.partnername,partner.contactnumber,"
			+ "partner.dateofjoining,partner.emailaddress,partner.status"
			+ " from partnerlist partner join citylist city on partner.city=city.cityid";

	public PartnersDetailFetcher(String ruleName, String dbConnName) {
		super(ruleName);
		this.dbConnName = dbConnName;
	}

	@Override
	public RuleDecisionKey executeRule(FlowContext flowContext) {

		Response response = (Response) flowContext
				.getFromContext(EzedKeyConstants.EZED_RESPONSE);
		MessageData messageData = (MessageData) flowContext
				.getFromContext(MessageData.class.getName());

		Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
		PreparedStatement ps = null;
		ResultSet rs = null;
		String finalQuery = PARTNER_LIST + " ";

		String enddate;
		String startdate;
		String createdBy;
		String cityid;
		String state;
		String country;
		StringBuilder sb = new StringBuilder("");

		createdBy = messageData.getParameter("createdby");
		enddate = messageData.getParameter("enddate");
		startdate = messageData.getParameter("startdate");
		if ((startdate != null && startdate.compareTo("null") != 0 && startdate
				.compareTo("") != 0) && enddate.compareTo("null") == 0) {
			enddate = sourceFormat.format(new Date());
		}
		cityid = messageData.getParameter("cityid");
		state = messageData.getParameter("state");
		country = messageData.getParameter("country");
		String qryDate = " partner.dateofjoining between '" + startdate
				+ "' and '" + enddate + "'";
		String qrycity = " city.cityid='" + cityid + "'";
		String qryAdmin = " partner.addedby='" + createdBy + "'";
		String qryState = " partner.state='" + state + "'";
		String qryCountry = " partner.country='" + country + "'";

		if (startdate != null && startdate.compareTo("null") != 0
				&& startdate.compareTo("") != 0) {
			if (!(sb.length() > 0)) {
				sb.append(" where");
				sb.append(qryDate);
			} else {
				sb.append(" and");
				sb.append(qryDate);
			}
		}

		if (createdBy.compareTo("null") != 0 && createdBy.compareTo("") != 0
				&& createdBy != null) {
			if (!(sb.length() > 0)) {
				sb.append(" where");
				sb.append(qryAdmin);
			} else {
				sb.append(" and");
				sb.append(qryAdmin);
			}
		}

		if (cityid.compareTo("null") != 0 && cityid.compareTo("") != 0
				&& cityid != null) {
			if (!(sb.length() > 0)) {
				sb.append(" where");
				sb.append(qrycity);
			} else {
				sb.append(" and");
				sb.append(qrycity);
			}
		}
		if (state.compareTo("null") != 0 && state.compareTo("") != 0
				&& state != null) {
			if (!(sb.length() > 0)) {
				sb.append(" where");
				sb.append(qryState);
			} else {
				sb.append(" and");
				sb.append(qryState);
			}
		}
		if (country.compareTo("null") != 0 && country.compareTo("") != 0
				&& country != null) {
			if (!(sb.length() > 0)) {
				sb.append(" where");
				sb.append(qryCountry);
			} else {
				sb.append(" and");
				sb.append(qryCountry);
			}
		}

		finalQuery = finalQuery + sb.toString();
		System.out.println("Final Qurey : " + finalQuery);

		String[] partnerData = null;
		FrontendFetchData partnerDetails = new FrontendFetchData();
		response.setResponseObject(partnerDetails);
		response.setStatusCodeSend(false);
		partnerDetails.setAttribName("aaData");
		ArrayList<String[]> partnerDataList = new ArrayList<String[]>();

		try {
			ps = conn.prepareStatement(finalQuery);
			rs = ps.executeQuery();
			while (rs.next()) {
				partnerData = new String[8];
				partnerData[0] = rs.getString("srno");
				partnerData[1] = rs.getString("partnerid");
				partnerData[2] = rs.getString("contactperson");
				partnerData[3] = rs.getString("partnername");
				partnerData[4] = rs.getLong("contactnumber") + "";
				partnerData[5] = formatTo.format(sourceFormat.parse(rs
						.getString("dateofjoining")));
				partnerData[6] = rs.getString("emailaddress");
				int status = rs.getInt("status");
				if (status == 0) {
					partnerData[7] = "Inactive";
				} else {
					partnerData[7] = "Active";
				}
				partnerDataList.add(partnerData);
			}
			partnerDetails.setAttribList(partnerDataList);
			response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
		} catch (SQLException e) {
			logger.error("Error while fetching partner details from db : ", e);
			response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED,
					"Error while fetching partner details");
			return RuleDecisionKey.ON_FAILURE;
		} catch (Exception e) {
			logger.error("Unknown Error while fetching partner details : ", e);
			response.setParameters(EzedTransactionCodes.INTERNAL_ERROR,
					"Error while fetching partner details");
			return RuleDecisionKey.ON_FAILURE;
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					logger.error(
							"Error while closing result set while fetching Partners list",
							e);
				}
			}
			DbConnectionManager.releaseConnection(dbConnName, conn);
		}

		return RuleDecisionKey.ON_SUCCESS;
	}
}