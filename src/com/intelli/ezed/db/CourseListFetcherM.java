package com.intelli.ezed.db;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.CourseListData;
import com.intelli.ezed.data.CourseListForPaging;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class CourseListFetcherM extends SingletonRule{

	private String dbConnName;
	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
	
	public CourseListFetcherM(String ruleName, String dbConnName) {
		super(ruleName);
		this.dbConnName = dbConnName;
	}
	
	@Override
	public RuleDecisionKey executeRule(FlowContext flowContext) {

		Response response = (Response) flowContext
				.getFromContext(EzedKeyConstants.EZED_RESPONSE);
		MessageData messageData = (MessageData) flowContext
				.getFromContext(MessageData.class.getName());
		
		String pageno = messageData.getParameter("pageno");
	     if(pageno == null || pageno.trim().compareTo("")==0){
	    	 if(logger.isDebugEnabled()){
	    		 logger.debug("page no in request is invalid");
	    	 }
	    	 response.setParameters(EzedTransactionCodes.PROCESSING_REQUEST, "Invalid Page No");
	    	 return RuleDecisionKey.ON_FAILURE;
	     }
	     
	     String noOfRecordsOnAPage = messageData.getParameter("noofrecords");
	     if(noOfRecordsOnAPage == null || noOfRecordsOnAPage.trim().compareTo("")==0){
	    	 if(logger.isDebugEnabled()){
	    		 logger.debug("page no in request is invalid");
	    	 }
	    	 response.setParameters(EzedTransactionCodes.PROCESSING_REQUEST, "Invalid No Of Records");
	    	 return RuleDecisionKey.ON_FAILURE;
	     }
	     int limit = Integer.parseInt(noOfRecordsOnAPage);
	     int pagenumber = Integer.parseInt(pageno);
	     int offset = (Integer.parseInt(pageno) -1) * limit;
	     String requesttype = messageData.getParameter("requesttype") ;
	     String userid = messageData.getParameter("email");
	     
	     
		final String query = 
				
				"SELECT "
				+ " csl.courseid,csl.chaptercount,"
				+ "csl.videocount,csl.questioncount,cl.coursename,cl.courseimage"
				+ " from courselist cl,coursestatlist csl, universitycourse uc,universityofferings uo"
				+ "  where cl.publishflag =1 "
				+ "and uo.univofferingid = uc.univofferingid "
				+ "and cl.courseid = csl.courseid and "
				+ "uc.courseid = csl.courseid ";
				
			
		final String queryForWebBrowser = 
				"SELECT "
						+ " csl.courseid,csl.chaptercount, cl.shortdesc, cl.longdesctype, cl.longdescstring, cl.longdesccontent, "
						+ "csl.videocount,csl.questioncount,cl.coursename,cl.courseimage"
						+ " from courselist cl,coursestatlist csl, universitycourse uc,universityofferings uo"
						+ "  where cl.publishflag =1 "
						+ "and uo.univofferingid = uc.univofferingid "
						+ "and cl.courseid = csl.courseid and "
						+ "uc.courseid = csl.courseid ";
		
		final String queryForAdmin = 
				"SELECT "
						+ " csl.courseid,csl.chaptercount, cl.shortdesc, cl.longdesctype, cl.longdescstring, cl.longdesccontent, "
						+ "csl.videocount,csl.questioncount,cl.coursename,cl.courseimage"
						+ " from courselist cl,coursestatlist csl, universitycourse uc,universityofferings uo"
						+ "  where "
						+ " uo.univofferingid = uc.univofferingid "
						+ "and cl.courseid = csl.courseid and "
						+ "uc.courseid = csl.courseid ";
		StringBuilder sb = new StringBuilder("");
		System.out.println("String buffer : " + sb.toString());
        
		
		/*
		 * Filters are: univName, degree, stream , year, semester
		 */
		
		String univName = messageData.getParameter("univName");
		if (univName != null && univName.trim().compareTo("") != 0 && univName.compareTo("null") != 0) {
			String qryCourseId = " and uo.univname = '" + univName + "'";
			sb.append(qryCourseId);
		}
		
			String degree = messageData.getParameter("degree");
			if (degree != null && degree.trim().compareTo("") != 0 && degree.compareTo("null") != 0) {
				String qryCourseId = " and uo.degree = '" + degree + "'";
				sb.append(qryCourseId);
			}
			
			String stream = messageData.getParameter("stream");
			if (stream != null && stream.trim().compareTo("") != 0 && stream.compareTo("null") != 0) {
				String qryCourseId = " and uo.stream = '" + stream + "'";
				sb.append(qryCourseId);
			} 
		
			String year = messageData.getParameter("year");
			if (year != null && year.trim().compareTo("") != 0 && year.compareTo("null") != 0) {
				String qryCourseId = " and uo.year = '" + year + "'";
				sb.append(qryCourseId);
			} 
		
			String semester = messageData.getParameter("semester");
			if (semester != null && semester.trim().compareTo("") != 0 && semester.compareTo("null") != 0) {
				String qryCourseId = " and uo.semester = '" + semester + "'";
				sb.append(qryCourseId);
			}

		
		sb.append(" order by courseid asc offset " + Integer.toString(offset) + " limit " + Integer.toString(limit));
		System.out.println("String buffer : " + sb.toString());
		String finalQuery = null;
		if(requesttype != null && requesttype.equalsIgnoreCase("web")){
			finalQuery = queryForWebBrowser;
		}
		else if(requesttype !=null && requesttype.equalsIgnoreCase("admin")){
			finalQuery = queryForAdmin;
		}
		else{
			finalQuery = query;
		}
		
		if (userid != null && (requesttype != null && !requesttype.equalsIgnoreCase("admin"))){
			finalQuery = finalQuery + " and cl.courseid NOT IN ( select courseid from usercourse where userid = '" + userid + "') ";
		}
		if (!sb.equals("")) {
			finalQuery = finalQuery + sb.toString();
		}
		System.out.println("finalquery "+ finalQuery);
		Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
		PreparedStatement ps = null;
		ResultSet rs = null;

		 ArrayList<CourseListData> courseList = new ArrayList<CourseListData>();
	     CourseListForPaging courseListForPaging = new CourseListForPaging();
		try {
				ps = conn.prepareStatement(finalQuery);
				System.out.println("Final Query : " + ps);
				rs = ps.executeQuery();
				while (rs.next()) {
					 CourseListData  courseListData = new CourseListData();
		         		courseListData.setCourseid(rs.getString("courseid"));
		         		courseListData.setChaptercount(rs.getInt("chaptercount"));
		         		courseListData.setQuestioncount(rs.getInt("questioncount"));
		         		courseListData.setVideocount(rs.getInt("videoCount"));
		         		courseListData.setCoursename(rs.getString("coursename"));
		         		courseListData.setCourseimage(rs.getString("courseimage"));	 
		        		if(requesttype != null &&  (requesttype.equalsIgnoreCase("web")|| requesttype.equalsIgnoreCase("admin"))){
		         			courseListData.setShortdesc(rs.getString("shortdesc"));
		         			courseListData.setLongdesctype(rs.getString("longdesctype"));
		         			courseListData.setLongdescstring(rs.getString("longdescstring"));
		         			courseListData.setLongdesccontent(rs.getString("longdesccontent"));
		         		}
		         		courseList.add(courseListData);
		         		
				}
				 courseListForPaging.setCourseListData(courseList);
				 courseListForPaging.setLimit(limit);
				 courseListForPaging.setPageno(pagenumber);
				 courseListForPaging.setOffset(offset);
		  }
		 catch (SQLException e)
         {
            logger.error("Error while fetching CoursesWithPaging", e);
            response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "PL SQL Error while fetching CoursesWithPaging : " + e.getMessage());
            return RuleDecisionKey.ON_FAILURE;
         }
         catch (Exception e)
         {
            logger.error("Error while fetching test details", e);
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching CoursesWithPaging : " + e.getMessage());
            return RuleDecisionKey.ON_FAILURE;
         }
         finally
         {
            if (rs != null)
            {
               try
               {
                  rs.close();
               }
               catch (SQLException e)
               {
               }
            }
            DbConnectionManager.releaseConnection(dbConnName, conn);
         }
		 response.setResponseObject(courseListForPaging);
	     response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
		 return RuleDecisionKey.ON_SUCCESS;
	}

	
}
