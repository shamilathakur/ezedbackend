package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.CartData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.utils.EndDateCalculator;

public class CartAmountsFetcher extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;

   public CartAmountsFetcher(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      int dayBufferBeforeEnd = (Integer) flowContext.getFromContext(EzedKeyConstants.BUFFER_END_DATE);
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      CartData[] cart = (CartData[]) flowContext.getFromContext(EzedKeyConstants.CART_DETAILS);
      Connection conn = null;
      PreparedStatement ps = null;
      ResultSet rs = null;
      Calendar calendar = Calendar.getInstance();
      for (int i = 0; i < cart.length; i++)
      {
         try
         {
        	 // sets the amount, the goals and the end date
            conn = DbConnectionManager.getConnectionByName(dbConnName);
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchCourseDetail");
            ps.setString(1, cart[i].getCourseId());
            rs = ps.executeQuery();
            if (rs.next())
            {
               if (cart[i].getCourseType().compareTo(CartData.ALL_COURSE) == 0)
               {
                  cart[i].setAmount(rs.getInt("fullcourseamt"));
               }
               else if (cart[i].getCourseType().compareTo(CartData.CONTENT_ONLY) == 0)
               {
                  cart[i].setAmount(rs.getInt("contentonlyamt"));
               }
               else
               {
                  cart[i].setAmount(rs.getInt("testonlyamt"));
               }
               String commaSeparatedEndMonths = rs.getString("enddate");
               Date courseEndDate = EndDateCalculator.findEndDate(commaSeparatedEndMonths, cart[i].getStartDate());
               calendar.setTime(courseEndDate);
               calendar.add(Calendar.DATE, -dayBufferBeforeEnd);
               cart[i].setEndDate(calendar.getTime());
               int publishFlag = rs.getInt("publishflag");
               if (publishFlag == 0)
               {
                  if (logger.isDebugEnabled())
                  {
                     logger.debug("Course with course id " + cart[i].getCourseId() + " is not published");
                  }
                  response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Course is not yet published");
                  return RuleDecisionKey.ON_FAILURE;
               }
            }
            else
            {
               if (logger.isDebugEnabled())
               {
                  logger.debug("Course details not found for course id " + cart[i].getCourseId());
               }
               response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Course details not found");
               return RuleDecisionKey.ON_FAILURE;
            }
         }
         catch (SQLException e)
         {
            logger.error("Error while fetching amount for course " + cart[i].getCourseId(), e);
            response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching course details");
            return RuleDecisionKey.ON_FAILURE;
         }
         catch (Exception e)
         {
            logger.error("Error while fetching amount for course " + cart[i].getCourseId(), e);
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching course details");
            return RuleDecisionKey.ON_FAILURE;
         }
         finally
         {
            DbConnectionManager.releaseConnection(dbConnName, conn);
         }
      }
      return RuleDecisionKey.ON_SUCCESS;
   }
}
