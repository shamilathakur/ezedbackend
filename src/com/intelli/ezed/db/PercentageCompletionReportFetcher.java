package com.intelli.ezed.db;

import java.sql.SQLException;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.a.helper.utils.StringSplitter;
import com.intelli.ezed.analytics.data.CoursePercentageData;
import com.intelli.ezed.analytics.data.PercentageCompletionReportData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class PercentageCompletionReportFetcher extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public PercentageCompletionReportFetcher(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      String userId = (String) flowContext.getFromContext(EzedKeyConstants.USER_ID);
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      String courseIdList = messageData.getParameter("courseid");
      if (courseIdList == null || courseIdList.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Course id is invalid " + courseIdList);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid course ids");
         return RuleDecisionKey.ON_FAILURE;
      }

      PercentageCompletionReportData reportData = new PercentageCompletionReportData();
      reportData.setUserId(userId);

      String[] courseIds = StringSplitter.splitString(courseIdList.trim(), ",");
      CoursePercentageData coursePercentageData = null;
      for (int i = 0; i < courseIds.length; i++)
      {
         coursePercentageData = new CoursePercentageData();
         coursePercentageData.setCourseId(courseIds[i]);
         coursePercentageData.setUserId(userId);
         reportData.getCourseData().add(coursePercentageData);
      }

      response.setResponseObject(reportData);
      try
      {
         reportData.populateReport(dbConnName, logger);
         reportData.prepareChart();
         response.setStatusCodeSend(false);
         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
         return RuleDecisionKey.ON_SUCCESS;
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching user average completion report", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching completion report");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching user average completion report", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching completion report");
         return RuleDecisionKey.ON_FAILURE;
      }
   }

}
