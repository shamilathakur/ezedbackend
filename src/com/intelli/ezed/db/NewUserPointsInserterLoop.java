package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.utils.StringSplitter;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;

public class NewUserPointsInserterLoop extends SingletonRule
{
   private String dbConnName;
   private String[] actions;
   private long sleepTime = 10l;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private Logger pointsLogger = LoggerFactory.getLogger(EzedLoggerName.POINTS_ERROR_LOG);

   public NewUserPointsInserterLoop(String ruleName, String dbConnName, String actionList)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
      this.actions = StringSplitter.splitString(actionList, ",");
   }

   public NewUserPointsInserterLoop(String ruleName, String dbConnName, String actionList, String sleepTime)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
      this.actions = StringSplitter.splitString(actionList, ",");
      this.sleepTime = Long.parseLong(sleepTime);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      String userid = (String) flowContext.getFromContext(EzedKeyConstants.USER_ID);
      String inTime = (String) flowContext.getFromContext(EzedKeyConstants.REQUEST_IN_TIME);

      for (int i = 0; i < actions.length; i++)
      {
         Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
         PreparedStatement ps = null;

         try
         {
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertNewUserPoints");
            ps.setString(1, userid);
            ps.setString(2, actions[i]);
            ps.setString(3, null);
            ps.setString(4, null);
            ps.setString(5, null);
            int rows = ps.executeUpdate();
            if (rows == 0)
            {
               if (logger.isDebugEnabled())
               {
                  logger.debug("Userid could not be registered for point entry making " + userid + " " + actions[i]);
               }
               pointsLogger.info(userid + " " + inTime + " " + actions[i]);
               continue;
            }
            else
            {
               if (logger.isDebugEnabled())
               {
                  logger.debug(rows + " rows inserted in db so that point entries can be made for user after verif" + userid + " " + actions[i]);
               }
            }
         }
         catch (Exception e)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Userid could not be registered for point entry making " + userid + " " + actions[i], e);
            }
            pointsLogger.info(userid + " " + inTime + " " + actions[i]);
            continue;
         }
         finally
         {
            DbConnectionManager.releaseConnection(dbConnName, conn);
            try
            {
               Thread.sleep(this.sleepTime);
            }
            catch (InterruptedException e)
            {
               continue;
            }
         }
      }
      return RuleDecisionKey.ON_SUCCESS;
   }
}
