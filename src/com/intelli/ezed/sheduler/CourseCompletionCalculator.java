package com.intelli.ezed.sheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.atomic.AtomicInteger;

import org.sabre.db.DbConnectionManager;
import org.sabre.utils.threads.TimedTask;
import org.sabre.workflow.context.FlowContext;
import org.sabre.workflow.processor.InputProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;

public class CourseCompletionCalculator extends InputProcessor
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private Resetter resetter;
   private FlowContext baseContext;
   private AtomicInteger atomicInteger;
   private Long throttleSleepTime;
   private int throttleParallelRecords;
   private SimpleDateFormat sdf1 = new SimpleDateFormat("HHmmss");

   public CourseCompletionCalculator(String startAtTime, String pollingTime, String dbConnName,
                                     String throttleSleepTime, String throttleParallelRecords)
   {
      atomicInteger = new AtomicInteger(1);
      Calendar calendar = Calendar.getInstance();
      calendar.add(Calendar.MINUTE, 1);
      //      resetter = new Resetter(startAtTime, Long.parseLong(pollingTime), dbConnName);
      resetter = new Resetter(sdf1.format(calendar.getTime()), Long.parseLong(pollingTime), dbConnName);
      this.throttleSleepTime = Long.parseLong(throttleSleepTime);
      this.throttleParallelRecords = Integer.parseInt(throttleParallelRecords);
   }

   class Resetter extends TimedTask
   {
      private String dbConnName = null;
      SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
      public Resetter(String startAtTime, long pollingTime, String dbConnName)
      {
         super(startAtTime, pollingTime);
         this.dbConnName = dbConnName;
         this.setStartAtTime(startAtTime);
         this.setExecAtStartup(true);
         this.setOneTimeTask(false);
      }

      @Override
      public void doTimedTask()
      {
         //Put this in a new thread later so that timed task immediately goes ahead and
         //further runs of timed task don't get delayed
         calculateCourseCompletionPercentages();
      }

      //select userid,courseid from goalchangedmarker;
      private void calculateCourseCompletionPercentages()
      {
         logger.info("Starting scheduler to calculate course completion percentages");
         Connection connection = DbConnectionManager.getConnectionByName(dbConnName);
         ResultSet rs = null;
         try
         {
            PreparedStatement selectStmt = DbConnectionManager.getPreparedStatement(dbConnName, connection, "SelectChangeMarkedCourses");

            rs = selectStmt.executeQuery();
            FlowContext currentContext = null;
            int count = 0;
            String userId = null;
            String courseId = null;
            while (rs.next())
            {
               userId = rs.getString(1);
               courseId = rs.getString(2);

               currentContext = baseContext.createSubContext(getSubContextName());
               currentContext.putIntoContext(EzedKeyConstants.USER_ID, userId);
               currentContext.putIntoContext(EzedKeyConstants.COURSE_ID, courseId);
               process(currentContext);
               try
               {
                  count++;
                  if (count == throttleParallelRecords)
                  {
                     count = 0;
                     Thread.sleep(throttleSleepTime);
                  }
               }
               catch (Exception e1)
               {
                  logger.error("Exception while sleeping thread", e1);
                  continue;
               }
            }
         }
         catch (Exception e)
         {
            logger.error("Exception occured while fetching changed marked courses ", e);
         }
         finally
         {
            DbConnectionManager.releaseConnection(dbConnName, connection);
            if (rs != null)
            {
               try
               {
                  rs.close();
               }
               catch (SQLException e)
               {
                  logger.error("Error while closing resultset", e);
               }
            }
         }

         //         try
         //         {
         //            connection = DbConnectionManager.getConnectionByName(dbConnName);
         //            PreparedStatement ps = DbConnectionManager.getPreparedStatement(dbConnName, connection, "DeleteMarkedCourses");
         //            int rows = ps.executeUpdate();
         //            if (logger.isDebugEnabled())
         //            {
         //               logger.debug(rows + " rows deleted from db while deleting change marked courses");
         //            }
         //         }
         //         catch (Exception e)
         //         {
         //            logger.error("Error while deleting courses marked for change");
         //         }
         //         finally
         //         {
         //            DbConnectionManager.releaseConnection(dbConnName, connection);
         //         }
      }
   }

   @Override
   public void initialize(FlowContext flowContext)
   {
      this.baseContext = flowContext;
   }

   @Override
   public void start()
   {
      resetter.startTask(true);
   }

   public String getSubContextName()
   {
      return "CourseCompPercentCalculator" + atomicInteger.getAndIncrement();
   }
}
