package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.ActionData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.MasterActionRecords;
import com.intelli.ezed.data.TestDetails;
import com.intelli.ezed.data.UserProfile;
import com.intelli.ezed.utils.EzedHelper;

public class TestEndPointsGiver extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String actionIdTestEnd;
   private static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
   private String actionIdDiffWise;
   private String actionIdMega;

   public TestEndPointsGiver(String ruleName, String dbConnName, String actionId, String actionIdDiffWise,
                             String actionIdMega)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
      this.actionIdTestEnd = actionId;
      this.actionIdDiffWise = actionIdDiffWise;
      this.actionIdMega = actionIdMega;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      UserProfile userProfile = (UserProfile) flowContext.getFromContext(EzedKeyConstants.USER_DETAILS);
      TestDetails testDetails = (TestDetails) flowContext.getFromContext(TestDetails.TEST_DETAILS);
      ActionData actionData = MasterActionRecords.fetchRecord(actionIdTestEnd);
      ActionData megaActionData = MasterActionRecords.fetchRecord(actionIdMega);
      Integer diffWisePoints = (Integer) flowContext.getFromContext(EzedKeyConstants.DIFF_WISE_POINTS);
      Boolean megaValid = (Boolean) flowContext.getFromContext(EzedKeyConstants.MEGA_TEST_POINTS);
      String testSessionId = (String) flowContext.getFromContext(EzedKeyConstants.TEST_SESSION_ID);

      String testName = EzedHelper.fetchTestNameForTestId(dbConnName, testDetails.getTestid(), logger);
      String courseName = EzedHelper.fetchCourseNameForCourseId(dbConnName, testDetails.getCourseId(), logger);

      StringBuffer sb = new StringBuffer();
      sb.append(userProfile.getName());
      sb.append("completed ");
      sb.append(testName);
      sb.append(" test in course ");
      sb.append(courseName);
      String testEndStream = sb.toString();

      sb = new StringBuffer();
      sb.append(userProfile.getName());
      sb.append("scored points as part of taking ");
      sb.append(EzedHelper.fetchTestNameForTestId(dbConnName, testDetails.getTestid(), logger));
      sb.append(" test in course ");
      sb.append(EzedHelper.fetchCourseNameForCourseId(dbConnName, testDetails.getCourseId(), logger));
      String testDiffWiseStream = sb.toString();

      sb = new StringBuffer();
      sb.append(userProfile.getName());
      sb.append("scored mega points for ");
      sb.append(EzedHelper.fetchTestNameForTestId(dbConnName, testDetails.getTestid(), logger));
      sb.append(" test in course ");
      sb.append(EzedHelper.fetchCourseNameForCourseId(dbConnName, testDetails.getCourseId(), logger));
      String testMegaStream = sb.toString();

      Connection conn = null;
      PreparedStatement ps = null;
      //      ResultSet rs = null;
      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdateCoursePoints");
         ps.setInt(1, actionData.getPoints());
         ps.setString(2, userProfile.getUserid());
         ps.setString(3, testDetails.getCourseId());
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows updated in db while incrementing points for userid " + userProfile.getUserid() + " and course " + testDetails.getCourseId());
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while incrementing points for userid " + userProfile.getUserid() + " and course " + testDetails.getCourseId(), e);
      }
      catch (Exception e)
      {
         logger.error("Error while incrementing points for userid " + userProfile.getUserid() + " and course " + testDetails.getCourseId(), e);
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      conn = DbConnectionManager.getConnectionByName(dbConnName);
      ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdateUserPoints");
         ps.setInt(1, actionData.getPoints());
         ps.setString(2, userProfile.getUserid());
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows updated in db while incrementing points for userid " + userProfile.getUserid());
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while incrementing points for userid " + userProfile.getUserid(), e);
      }
      catch (Exception e)
      {
         logger.error("Error while incrementing points for userid " + userProfile.getUserid(), e);
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      //diff wise points
      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdateCoursePoints");
         ps.setInt(1, diffWisePoints);
         ps.setString(2, userProfile.getUserid());
         ps.setString(3, testDetails.getCourseId());
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows updated in db while incrementing points for userid " + userProfile.getUserid() + " and course " + testDetails.getCourseId());
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while incrementing points for userid " + userProfile.getUserid() + " and course " + testDetails.getCourseId(), e);
      }
      catch (Exception e)
      {
         logger.error("Error while incrementing points for userid " + userProfile.getUserid() + " and course " + testDetails.getCourseId(), e);
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdateUserPoints");
         ps.setInt(1, diffWisePoints);
         ps.setString(2, userProfile.getUserid());
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows updated in db while incrementing points for userid " + userProfile.getUserid());
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while incrementing points for userid " + userProfile.getUserid(), e);
      }
      catch (Exception e)
      {
         logger.error("Error while incrementing points for userid " + userProfile.getUserid(), e);
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      //Mega points
      if (megaValid)
      {
         try
         {
            conn = DbConnectionManager.getConnectionByName(dbConnName);
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdateCoursePoints");
            ps.setInt(1, megaActionData.getPoints());
            ps.setString(2, userProfile.getUserid());
            ps.setString(3, testDetails.getCourseId());
            int rows = ps.executeUpdate();
            if (logger.isDebugEnabled())
            {
               logger.debug(rows + " rows updated in db while incrementing points for userid " + userProfile.getUserid() + " and course " + testDetails.getCourseId());
            }
         }
         catch (SQLException e)
         {
            logger.error("Error while incrementing points for userid " + userProfile.getUserid() + " and course " + testDetails.getCourseId(), e);
         }
         catch (Exception e)
         {
            logger.error("Error while incrementing points for userid " + userProfile.getUserid() + " and course " + testDetails.getCourseId(), e);
         }
         finally
         {
            DbConnectionManager.releaseConnection(dbConnName, conn);
         }

         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = null;
         try
         {
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdateUserPoints");
            ps.setInt(1, megaActionData.getPoints());
            ps.setString(2, userProfile.getUserid());
            int rows = ps.executeUpdate();
            if (logger.isDebugEnabled())
            {
               logger.debug(rows + " rows updated in db while incrementing points for userid " + userProfile.getUserid());
            }
         }
         catch (SQLException e)
         {
            logger.error("Error while incrementing points for userid " + userProfile.getUserid(), e);
         }
         catch (Exception e)
         {
            logger.error("Error while incrementing points for userid " + userProfile.getUserid(), e);
         }
         finally
         {
            DbConnectionManager.releaseConnection(dbConnName, conn);
         }
      }

      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertUserStreamAll");
         ps.setString(1, testEndStream);
         ps.setString(2, userProfile.getUserid());
         ps.setString(3, sdf.format(new Date()));
         ps.setString(4, testDetails.getCourseId());
         ps.setString(5, testDetails.getChapterId());
         ps.setString(6, testDetails.getTestid());
         ps.setString(7, actionIdTestEnd);
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in db while inserting stream for user " + userProfile.getUserid());
         }
      }
      catch (Exception e)
      {
         logger.error("Error inserting stream entry for " + userProfile.getUserid(), e);
         logger.error("INSERT FAILED " + testEndStream);
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      //diff wise stream
      try
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertUserStreamAll");
         ps.setString(1, testDiffWiseStream);
         ps.setString(2, userProfile.getUserid());
         ps.setString(3, sdf.format(new Date()));
         ps.setString(4, testDetails.getCourseId());
         ps.setString(5, testDetails.getChapterId());
         ps.setString(6, testDetails.getTestid());
         ps.setString(7, actionIdDiffWise);
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in db while inserting stream for user " + userProfile.getUserid());
         }
      }
      catch (Exception e)
      {
         logger.error("Error inserting stream entry for " + userProfile.getUserid(), e);
         logger.error("INSERT FAILED " + testDiffWiseStream);
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      //megastream
      if (megaValid)
      {
         try
         {
            conn = DbConnectionManager.getConnectionByName(dbConnName);
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertUserStreamAll");
            ps.setString(1, testMegaStream);
            ps.setString(2, userProfile.getUserid());
            ps.setString(3, sdf.format(new Date()));
            ps.setString(4, testDetails.getCourseId());
            ps.setString(5, testDetails.getChapterId());
            ps.setString(6, testDetails.getTestid());
            ps.setString(7, actionIdMega);
            int rows = ps.executeUpdate();
            if (logger.isDebugEnabled())
            {
               logger.debug(rows + " rows inserted in db while inserting stream for user " + userProfile.getUserid());
            }
         }
         catch (Exception e)
         {
            logger.error("Error inserting stream entry for " + userProfile.getUserid(), e);
            logger.error("INSERT FAILED " + testMegaStream);
         }
         finally
         {
            DbConnectionManager.releaseConnection(dbConnName, conn);
         }
      }

      try
      {
         int totalTestPoints = diffWisePoints + megaActionData.getPoints();

         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "");
         ps.setInt(1, totalTestPoints);
         ps.setString(2, testDetails.getTestid());
         ps.setString(3, testDetails.getUserid());
         ps.setString(4, testSessionId);
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted while inserting test points " + totalTestPoints + " for test session id " + testSessionId);
         }
      }
      catch (Exception e)
      {
         logger.error("Test points could not be updated for test session id " + testSessionId, e);
      }
      return RuleDecisionKey.ON_SUCCESS;
   }
}
