package com.intelli.ezed.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.sabre.db.DbConnectionManager;
import org.slf4j.Logger;

import com.intelli.ezed.bulkupload.data.ContentDetails;

public class GoalModificationData
{
   private String chapterId;
   private String testId;
   private ArrayList<String> courseIdList = new ArrayList<String>();
   private ArrayList<String> contentIdList = new ArrayList<String>();
   private ArrayList<String> contentshowFlag = new ArrayList<String>();
   private ArrayList<Integer> contentTypeList = new ArrayList<Integer>();
   private ArrayList<Integer> hoursList = new ArrayList<Integer>();
   private ArrayList<String> contentNameList = new ArrayList<String>();
   private ArrayList<SingleCourseGoalModificationData> datas = new ArrayList<SingleCourseGoalModificationData>();
   public String getChapterId()
   {
      return chapterId;
   }
   public void setChapterId(String chapterId)
   {
      this.chapterId = chapterId;
   }
   public ArrayList<String> getContentIdList()
   {
      return contentIdList;
   }
   public void setContentIdList(ArrayList<String> contentIdList)
   {
      this.contentIdList = contentIdList;
   }
   public ArrayList<String> getContentshowFlag()
   {
      return contentshowFlag;
   }
   public void setContentshowFlag(ArrayList<String> contentshowFlag)
   {
      this.contentshowFlag = contentshowFlag;
   }

   public ArrayList<String> getCourseIdList()
   {
      return courseIdList;
   }
   public void setCourseIdList(ArrayList<String> courseIdList)
   {
      this.courseIdList = courseIdList;
   }

   public void populateChapterDetails(String dbConnName, Logger logger) throws SQLException, Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchLearningPathForUpdate");
         ps.setString(1, getChapterId());
         rs = ps.executeQuery();
         while (rs.next())
         {
            this.getContentIdList().add(rs.getString("contentid"));
            this.getContentshowFlag().add(rs.getString("contentshowflag"));
         }
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
               logger.error("Error while closing result set while fetching chapter details");
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   public void populateTestDetails(String dbConnName, Logger logger) throws SQLException, Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "SelectTestDetails");
         ps.setString(1, getTestId());
         rs = ps.executeQuery();
         while (rs.next())
         {
            this.getContentIdList().add(rs.getString("testid"));
            this.getContentTypeList().add(ContentDetails.CONTENT_TEST);
            this.getContentNameList().add(rs.getString("testname"));
            String time = rs.getString("testtime");
            int hours = Integer.parseInt(time.substring(0, 2));
            int min = Integer.parseInt(time.substring(2, 4));
            min = min + (hours * 60);
            if (min == 0)
            {
               min = 1;
            }
            this.getHoursList().add(min);
            this.getContentshowFlag().add("1");
         }
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
               logger.error("Error while closing result set while fetching test details");
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   public void handleGoalDeletions(String dbConnName, Logger logger, String actionId) throws SQLException, Exception
   {
      fetchEnclosingCourseForChapter(dbConnName, logger);
      fetchValidUsersForCourse(dbConnName, logger, actionId);
      handleGoalCountDeletions(dbConnName, logger);
      deleteGoalsForEachUserCourse(dbConnName, logger);
   }

   public void handleChapterGoalDeletions(String dbConnName, Logger logger, String actionId) throws SQLException, Exception
   {
      fetchValidUsersForCourse(dbConnName, logger, actionId);
      handleGoalCountDeletions(dbConnName, logger);
      deleteGoalsForEachUserCourse(dbConnName, logger);
   }

   private void handleGoalCountDeletions(String dbConnName, Logger logger) throws SQLException, Exception
   {
      SingleCourseGoalModificationData data = null;
      Connection conn = null;
      PreparedStatement ps = null;
      int achievedGoalCount = 0;
      int failedGoalCount = 0;
      ResultSet rs = null;
      for (int i = 0; i < datas.size(); i++)
      {
         data = datas.get(i);
         for (int j = 0; j < getContentIdList().size(); j++)
         {
            conn = DbConnectionManager.getConnectionByName(dbConnName);
            try
            {
               if (getChapterId() != null)
               {
                  ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchGoalStatus");
                  ps.setString(1, data.getUserId());
                  ps.setString(2, data.getCourseId());
                  ps.setString(3, getChapterId());
                  ps.setString(4, getContentIdList().get(j));
               }
               else
               {
                  ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchGoalStatusCourseTest");
                  ps.setString(1, data.getUserId());
                  ps.setString(2, data.getCourseId());
                  ps.setString(3, getContentIdList().get(j));
               }
               rs = ps.executeQuery();
               if (rs.next())
               {
                  int goalAchieved = rs.getInt("goalachieved");
                  if (goalAchieved == GoalData.GOAL_COMPLETE)
                  {
                     achievedGoalCount++;
                  }
               }
            }
            finally
            {
               if (rs != null)
               {
                  try
                  {
                     rs.close();
                  }
                  catch (Exception e)
                  {
                     logger.error("Error while closing resultset", e);
                  }
               }
               DbConnectionManager.releaseConnection(dbConnName, conn);
            }

            conn = DbConnectionManager.getConnectionByName(dbConnName);
            try
            {
               if (getChapterId() != null)
               {
                  ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchFailedGoals");
                  ps.setString(1, data.getUserId());
                  ps.setString(2, data.getCourseId());
                  ps.setString(3, getChapterId());
                  ps.setString(4, getContentIdList().get(j));
               }
               else
               {
                  ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchFailedGoalsCourseTest");
                  ps.setString(1, data.getUserId());
                  ps.setString(2, data.getCourseId());
                  ps.setString(3, getContentIdList().get(j));
               }
               rs = ps.executeQuery();
               while (rs.next())
               {
                  failedGoalCount++;
               }
            }
            finally
            {
               if (rs != null)
               {
                  try
                  {
                     rs.close();
                  }
                  catch (Exception e)
                  {
                     logger.error("Error while closing resultset", e);
                  }
               }
               DbConnectionManager.releaseConnection(dbConnName, conn);
            }
         }

         conn = DbConnectionManager.getConnectionByName(dbConnName);
         try
         {
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "ReduceGoalCounts");
            ps.setInt(1, achievedGoalCount);
            ps.setInt(2, failedGoalCount);
            ps.setString(3, data.getUserId());
            ps.setString(4, data.getCourseId());
            int rows = ps.executeUpdate();
            if (logger.isDebugEnabled())
            {
               logger.debug(rows + " rows updated in db while reducing achieved goal count by " + achievedGoalCount + " and failed goal count by " + failedGoalCount);
            }
         }
         finally
         {
            DbConnectionManager.releaseConnection(dbConnName, conn);
         }
      }
   }
   private void deleteGoalsForEachUserCourse(String dbConnName, Logger logger) throws SQLException, Exception
   {
      SingleCourseGoalModificationData data = null;
      Connection conn = null;
      PreparedStatement ps = null;
      for (int i = 0; i < datas.size(); i++)
      {
         data = datas.get(i);
         for (int j = 0; j < getContentIdList().size(); j++)
         {
            conn = DbConnectionManager.getConnectionByName(dbConnName);
            try
            {
               if (getChapterId() != null)
               {
                  ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "DeleteSpecificGoal");
                  ps.setString(1, data.getUserId());
                  ps.setString(2, data.getCourseId());
                  ps.setString(3, getChapterId());
                  ps.setString(4, getContentIdList().get(j));
               }
               else
               {
                  ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "DeleteSpecificGoalCourseTest");
                  ps.setString(1, data.getUserId());
                  ps.setString(2, data.getCourseId());
                  ps.setString(3, getContentIdList().get(j));
               }
               int rows = ps.executeUpdate();
               if (logger.isDebugEnabled())
               {
                  logger.debug(rows + " rows deleted from goals while deleting goals");
               }
            }
            finally
            {
               DbConnectionManager.releaseConnection(dbConnName, conn);
            }

            conn = DbConnectionManager.getConnectionByName(dbConnName);
            try
            {
               if (getChapterId() != null)
               {
                  ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "DeleteFailedGoal");
                  ps.setString(1, data.getUserId());
                  ps.setString(2, data.getCourseId());
                  ps.setString(3, getChapterId());
                  ps.setString(4, getContentIdList().get(j));
               }
               else
               {
                  ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "DeleteFailedGoalCourseTest");
                  ps.setString(1, data.getUserId());
                  ps.setString(2, data.getCourseId());
                  ps.setString(3, getContentIdList().get(j));
               }
               int rows = ps.executeUpdate();
               if (logger.isDebugEnabled())
               {
                  logger.debug(rows + " rows deleted from goals while deleting failed goals");
               }
            }
            finally
            {
               DbConnectionManager.releaseConnection(dbConnName, conn);
            }
         }

         conn = DbConnectionManager.getConnectionByName(dbConnName);
         ps = null;
         try
         {
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "MarkGoalChanged");
            ps.setString(1, data.getUserId());
            ps.setString(2, data.getCourseId());
            int rows = ps.executeUpdate();
            if (logger.isDebugEnabled())
            {
               logger.debug(rows + " rows inserted in db while marking course view as changed for userid " + data.getUserId());
            }
         }
         catch (SQLException e)
         {
            if (logger.isTraceEnabled())
            {
               logger.trace("Cannot insert course in db while marking for change", e);
            }
            if (logger.isDebugEnabled())
            {
               logger.debug("Cannot insert course in db while marking course as changed for userid " + data.getUserId(), e);
            }
         }
         catch (Exception e)
         {
            if (logger.isTraceEnabled())
            {
               logger.trace("Cannot insert course in db while marking for change", e);
            }
            if (logger.isDebugEnabled())
            {
               logger.debug("Cannot insert course in db while marking course as changed for userid " + data.getUserId(), e);
            }
         }
         finally
         {
            DbConnectionManager.releaseConnection(dbConnName, conn);
         }
      }
   }
   public void handleGoalModifications(String dbConnName, Logger logger, float avgNoOfHours, String actionId) throws SQLException, Exception
   {
      fetchContentDetails(dbConnName, logger);
      fetchEnclosingCourseForChapter(dbConnName, logger);
      fetchValidUsersForCourse(dbConnName, logger, actionId);
      addGoalsForEachUserCourse();
      for (int i = 0; i < getDatas().size(); i++)
      {
         if (getDatas().get(i).getGoalDatas().size() > 0)
         {
            getDatas().get(i).handleNewGoals(dbConnName, logger, avgNoOfHours);
         }
      }
   }

   public void handleChapterGoalModifications(String dbConnName, Logger logger, float avgNoOfHours, String actionId) throws SQLException, Exception
   {
      fetchContentDetails(dbConnName, logger);
      fetchValidUsersForCourse(dbConnName, logger, actionId);
      addGoalsForEachUserCourse();
      for (int i = 0; i < getDatas().size(); i++)
      {
         if (getDatas().get(i).getGoalDatas().size() > 0)
         {
            getDatas().get(i).handleNewGoals(dbConnName, logger, avgNoOfHours);
         }
      }
   }

   public void handleTestGoalModifications(String dbConnName, Logger logger, float avgNoOfHours, String actionId) throws SQLException, Exception
   {
      fetchValidUsersForCourse(dbConnName, logger, actionId);
      addGoalsForEachUserCourse();
      for (int i = 0; i < getDatas().size(); i++)
      {
         if (getDatas().get(i).getGoalDatas().size() > 0)
         {
            getDatas().get(i).handleNewGoals(dbConnName, logger, avgNoOfHours);
         }
      }
   }

   private void fetchContentDetails(String dbConnName, Logger logger) throws SQLException, Exception
   {
      Connection conn = null;
      PreparedStatement ps = null;
      ResultSet rs = null;
      int hours = 0;
      for (int i = 0; i < getContentIdList().size(); i++)
      {
         try
         {
            conn = DbConnectionManager.getConnectionByName(dbConnName);
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchContentDetailsForId");
            ps.setString(1, getContentIdList().get(i));
            rs = ps.executeQuery();
            if (rs.next())
            {
               this.getContentTypeList().add(rs.getInt("contenttype"));
               hours = rs.getInt("hours");
               if (hours == 0)
               {
                  hours = 1;
               }
               this.getHoursList().add(hours);
               this.getContentNameList().add(rs.getString("contentname"));
            }
            else
            {
               throw new Exception("Content details not found for id " + getContentIdList().get(i));
            }
         }
         finally
         {
            if (rs != null)
            {
               try
               {
                  rs.close();
               }
               catch (Exception e)
               {
                  logger.error("Error while closing result set while fetching enclosing courses for chapter");
               }
            }
            DbConnectionManager.releaseConnection(dbConnName, conn);
         }
      }
   }

   private void fetchEnclosingCourseForChapter(String dbConnName, Logger logger) throws SQLException, Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchEnclosingCoursesForChapter");
         ps.setString(1, getChapterId());
         rs = ps.executeQuery();
         while (rs.next())
         {
            this.getCourseIdList().add(rs.getString("courseid"));
         }
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
               logger.error("Error while closing result set while fetching enclosing courses for chapter");
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   private void fetchValidUsersForCourse(String dbConnName, Logger logger, String actionId) throws SQLException, Exception
   {
      Connection conn = null;
      PreparedStatement ps = null;
      ResultSet rs = null;
      String courseId = null;
      SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
      SingleCourseGoalModificationData data = null;
      for (int i = 0; i < getCourseIdList().size(); i++)
      {
         courseId = getCourseIdList().get(i);
         try
         {
            conn = DbConnectionManager.getConnectionByName(dbConnName);
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchValidUsersForCourse");
            ps.setString(1, courseId);
            rs = ps.executeQuery();
            while (rs.next())
            {
               data = new SingleCourseGoalModificationData();
               data.setActionId(actionId);
               data.setCourseId(courseId);
               data.setCourseType(rs.getString("coursetype"));
               data.setUserId(rs.getString("userid"));
               data.setEndDate(sdf.parse(rs.getString("enddate")));
               data.setStartDate(sdf.parse(rs.getString("startdate")));
               this.datas.add(data);
            }
         }
         finally
         {
            if (rs != null)
            {
               try
               {
                  rs.close();
               }
               catch (Exception e)
               {
                  logger.error("Error while closing result set while fetching Users who have purchased a course");
               }
            }
            DbConnectionManager.releaseConnection(dbConnName, conn);
         }
      }
   }

   private void addGoalsForEachUserCourse()
   {
      SingleCourseGoalModificationData data = null;
      GoalData goalData = null;
      for (int i = 0; i < datas.size(); i++)
      {
         data = datas.get(i);
         if (data.getCourseType().compareTo(CartData.ALL_COURSE) == 0)
         {
            for (int j = 0; j < getContentIdList().size(); j++)
            {
               goalData = new GoalData();
               goalData.setChapterId(getChapterId());
               goalData.setContentId(getContentIdList().get(j));
               goalData.setContentShowFlag(getContentshowFlag().get(j));
               goalData.setCourseId(data.getCourseId());
               goalData.setHours(getHoursList().get(j));
               goalData.setContentname(getContentNameList().get(j));
               data.getGoalDatas().add(goalData);
            }
         }
         else if (data.getCourseType().compareTo(CartData.CONTENT_ONLY) == 0)
         {
            for (int j = 0; j < getContentIdList().size(); j++)
            {
               int contentType = getContentTypeList().get(j);
               if (ContentDetails.isContentOnlyCourse(contentType))
               {
                  goalData = new GoalData();
                  goalData.setChapterId(getChapterId());
                  goalData.setContentId(getContentIdList().get(j));
                  goalData.setContentShowFlag(getContentshowFlag().get(j));
                  goalData.setCourseId(data.getCourseId());
                  goalData.setHours(getHoursList().get(j));
                  goalData.setContentname(getContentNameList().get(j));
                  data.getGoalDatas().add(goalData);
               }
            }
         }
         else
         {
            for (int j = 0; j < getContentIdList().size(); j++)
            {
               int contentType = getContentTypeList().get(j);
               //if (contentType == ContentDetails.CONTENT_TEST || contentType == ContentDetails.CONTENT_QUIZ)
               if (ContentDetails.isTestOnlyCourse(contentType))
               {
                  goalData = new GoalData();
                  goalData.setChapterId(getChapterId());
                  goalData.setContentId(getContentIdList().get(j));
                  goalData.setContentShowFlag(getContentshowFlag().get(j));
                  goalData.setCourseId(data.getCourseId());
                  goalData.setHours(getHoursList().get(j));
                  goalData.setContentname(getContentNameList().get(j));
                  data.getGoalDatas().add(goalData);
               }
            }
         }
      }
   }

   public ArrayList<Integer> getContentTypeList()
   {
      return contentTypeList;
   }
   public void setContentTypeList(ArrayList<Integer> contentTypeList)
   {
      this.contentTypeList = contentTypeList;
   }
   public ArrayList<Integer> getHoursList()
   {
      return hoursList;
   }
   public void setHoursList(ArrayList<Integer> hoursList)
   {
      this.hoursList = hoursList;
   }
   public ArrayList<String> getContentNameList()
   {
      return contentNameList;
   }
   public void setContentNameList(ArrayList<String> contentNameList)
   {
      this.contentNameList = contentNameList;
   }
   public ArrayList<SingleCourseGoalModificationData> getDatas()
   {
      return datas;
   }
   public void setDatas(ArrayList<SingleCourseGoalModificationData> datas)
   {
      this.datas = datas;
   }
   public String getTestId()
   {
      return testId;
   }
   public void setTestId(String testId)
   {
      this.testId = testId;
   }
}
