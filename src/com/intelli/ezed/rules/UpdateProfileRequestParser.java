package com.intelli.ezed.rules;

import java.text.SimpleDateFormat;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.UserProfile;
import com.intelli.ezed.utils.EzedHelper;

public class UpdateProfileRequestParser extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   SimpleDateFormat birthDateFormat = new SimpleDateFormat("ddMMyyyy");

   public UpdateProfileRequestParser(String ruleName)
   {
      super(ruleName);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      UserProfile userProfile = new UserProfile();
      Boolean isVerifCompletedFlag = true;

      String value = messageData.getParameter("firstname");
      if (value == null || value.trim().compareTo("") == 0 || value.trim().compareToIgnoreCase("null") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("First name in update profile is invalid");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "First name is invalid");
         return RuleDecisionKey.ON_FAILURE;
      }
      userProfile.setFirstName(value);

      value = messageData.getParameter("middlename");
      userProfile.setMiddleName(value);

      value = messageData.getParameter("lastname");
      if (value == null || value.trim().compareTo("") == 0 || value.trim().compareToIgnoreCase("null") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Last name in update profile is invalid");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Last name is invalid");
         return RuleDecisionKey.ON_FAILURE;
      }
      userProfile.setLastName(value);

      value = messageData.getParameter("dateofbirth");
      if (value == null || value.trim().compareTo("") == 0 || value.trim().compareToIgnoreCase("null") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Date of birth in update profile is invalid");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Date of birth is invalid");
         return RuleDecisionKey.ON_FAILURE;
      }
      //      try
      //      {
      //         birthDateFormat.parse(value);
      //      }
      //      catch (ParseException e)
      //      {
      //         if (logger.isDebugEnabled())
      //         {
      //            logger.debug("Date of birth in update profile is invalid date", e);
      //         }
      //         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Date of birth is an invalid date");
      //         return RuleDecisionKey.ON_FAILURE;
      //      }
      userProfile.setDateOfBirth(value);

      value = messageData.getParameter("gender");
      if (value != null)
      {
         if (value.trim().compareTo("") == 0 || value.trim().compareToIgnoreCase("null") == 0)
         {
            value = null;
         }
         else if (value.trim().compareToIgnoreCase("m") != 0 && value.trim().compareToIgnoreCase("f") != 0)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Gender in update profile is invalid");
            }
            response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Gender is invalid");
            return RuleDecisionKey.ON_FAILURE;
         }
      }
      userProfile.setGender(value == null ? value : value.trim().toUpperCase());
      if (value == null)
      {
         value = null;
         isVerifCompletedFlag = false;
      }

      value = messageData.getParameter("university");
      if (value == null || value.trim().compareTo("") == 0 || value.trim().compareToIgnoreCase("null") == 0 || value.trim().compareToIgnoreCase("default") == 0)
      {
         value = null;
         isVerifCompletedFlag = false;
      }
      userProfile.setUnivName(value);

      value = messageData.getParameter("college");
      if (value == null || value.trim().compareTo("") == 0 || value.trim().compareToIgnoreCase("null") == 0 || value.trim().compareToIgnoreCase("default") == 0)
      {
         value = null;
         isVerifCompletedFlag = false;
      }
      userProfile.setCollegeName(value);

      value = messageData.getParameter("streetaddress");
      if (value == null || value.trim().compareTo("") == 0 || value.trim().compareToIgnoreCase("null") == 0)
      {
         value = null;
         //         isVerifCompletedFlag = false;
      }
      userProfile.setStreetAdress(value);

      value = messageData.getParameter("city");
      if (value == null || value.trim().compareTo("") == 0 || value.trim().compareToIgnoreCase("null") == 0 || value.trim().compareToIgnoreCase("default") == 0)
      {
         value = null;
         //         isVerifCompletedFlag = false;
      }
      userProfile.setCity(value);

      value = messageData.getParameter("country");
      if (value == null || value.trim().compareTo("") == 0 || value.trim().compareToIgnoreCase("null") == 0 || value.trim().compareToIgnoreCase("default") == 0)
      {
         value = null;
         //         isVerifCompletedFlag = false;
      }
      userProfile.setCountry(value);

      value = messageData.getParameter("state");
      if (value == null || value.trim().compareTo("") == 0 || value.trim().compareToIgnoreCase("null") == 0 || value.trim().compareToIgnoreCase("default") == 0)
      {
         value = null;
         //         isVerifCompletedFlag = false;
      }
      userProfile.setState(value);

      if (userProfile.getCountry() == null || userProfile.getState() == null || userProfile.getCity() == null)
      {
         userProfile.setCountry(null);
         userProfile.setState(null);
         userProfile.setCity(null);
      }

      value = messageData.getParameter("profileimage");
      if (value == null || value.trim().compareTo("") == 0 || value.trim().compareToIgnoreCase("null") == 0)
      {
         value = null;
         //         isVerifCompletedFlag = false;
      }
      userProfile.setProfileImage(value);

      value = messageData.getParameter("landmark");
      if (value == null || value.trim().compareTo("") == 0 || value.trim().compareToIgnoreCase("null") == 0)
      {
         value = null;
         //         isVerifCompletedFlag = false;
      }
      userProfile.setLandmark(value);

      value = messageData.getParameter("pincode");
      if (value != null)
      {
         if (value.trim().compareTo("") == 0 || value.trim().compareToIgnoreCase("null") == 0)
         {
            value = null;
         }
         else if (!EzedHelper.isNumeric(value.trim()))
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("pin code in update profile is invalid : " + value);
            }
            response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Pin code is invalid number");
            return RuleDecisionKey.ON_FAILURE;
         }
      }
      if (value == null || value.trim().compareTo("") == 0)
      {
         //         isVerifCompletedFlag = false;
      }
      userProfile.setPincode(value == null ? value : value.trim());

      value = messageData.getParameter("mobilenumber");
      if (value != null)
      {
         if (value.trim().compareTo("") == 0 || value.trim().compareToIgnoreCase("null") == 0)
         {
            value = null;
         }
         //         else if (EzedHelper.getTenDigMobNo(value) == null)
         //         {
         //            if (logger.isDebugEnabled())
         //            {
         //               logger.debug("mobile number in update profile is invalid");
         //            }
         //            response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "mobile is invalid number");
         //            return RuleDecisionKey.ON_FAILURE;
         //         }
      }
      if (value != null)
      {
         if (value.startsWith(" "))
         {
            value = "+" + value.trim();
         }
      }
      userProfile.setMobileNumber(value == null ? value : value.trim());
      if (value == null || value.trim().compareTo("") == 0)
      {
         //         isVerifCompletedFlag = false;
      }

      value = messageData.getParameter("stream");
      if (value == null || value.trim().compareTo("") == 0 || value.trim().compareTo("default") == 0 || value.trim().compareTo("null") == 0)
      {
         value = null;
      }
      userProfile.setStream(value);

      value = messageData.getParameter("degree");
      if (value == null || value.trim().compareTo("") == 0 || value.trim().compareTo("default") == 0 || value.trim().compareTo("null") == 0)
      {
         value = null;
      }
      userProfile.setDegree(value);
      flowContext.putIntoContext(EzedKeyConstants.UPDATED_USER_PROFILE, userProfile);
      flowContext.putIntoContext(EzedKeyConstants.USER_VERIF_COMPLETED, isVerifCompletedFlag);
      return RuleDecisionKey.ON_SUCCESS;
   }
}
