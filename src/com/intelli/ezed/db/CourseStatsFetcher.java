package com.intelli.ezed.db;

import java.sql.SQLException;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.CourseStatData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class CourseStatsFetcher extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public CourseStatsFetcher(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());

      String courseId = messageData.getParameter("courseid");
      if (courseId == null || courseId.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Course id in request in invalid");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid course id");
         return RuleDecisionKey.ON_FAILURE;
      }
      CourseStatData data = new CourseStatData();
      data.setCourseId(courseId);
      response.setResponseObject(data);

      try
      {
         data.fetchChapterCount(dbConnName, logger);
         data.fetchVideoCount(dbConnName, logger);
         data.fetchTestCount(dbConnName, logger);
         data.fetchQuestionCount(dbConnName, logger);
         data.fetchEndDateCourseName(dbConnName, logger, false);
         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
         return RuleDecisionKey.ON_SUCCESS;
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching course stat from db for course id " + courseId, e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching course details");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching course stat from db for course id " + courseId, e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching course details");
         return RuleDecisionKey.ON_FAILURE;
      }
   }
}
