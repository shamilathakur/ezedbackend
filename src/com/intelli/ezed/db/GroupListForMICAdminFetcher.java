package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.AdminProfile;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.FrontendFetchData;
import com.intelli.ezed.data.Response;

public class GroupListForMICAdminFetcher extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public GroupListForMICAdminFetcher(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      AdminProfile profile = (AdminProfile) flowContext.getFromContext(EzedKeyConstants.ADMIN_PROFILE);
      String adminUserId = profile.getUserid();
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      String[] groupArr = null;
      FrontendFetchData groupData = new FrontendFetchData();
      response.setResponseObject(groupData);
      response.setStatusCodeSend(false);
      groupData.setAttribName("aaData");
      ArrayList<String[]> groups = new ArrayList<String[]>();
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchGroupsForMIC");
         ps.setString(1, adminUserId);
         rs = ps.executeQuery();
         int i = 1;
         while (rs.next())
         {
            groupArr = new String[5];
            groupArr[0] = Integer.toString(i);
            groupArr[1] = rs.getString("groupname");
            groupArr[2] = rs.getString("coursename");
            groupArr[3] = rs.getString("name");
            groupArr[4] = rs.getString("groupid");
            groups.add(groupArr);
            i++;
         }
         groupData.setAttribList(groups);
         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
         return RuleDecisionKey.ON_SUCCESS;
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching list of groups", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching list of groups");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching list of groups", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching list of groups");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing resultset while fetching group list", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
}
