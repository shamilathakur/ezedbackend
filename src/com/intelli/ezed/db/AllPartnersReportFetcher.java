package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.FrontendFetchData;
import com.intelli.ezed.data.Response;

public class AllPartnersReportFetcher extends SingletonRule {

	private String dbConnName;
	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
	SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyyMMddHHmmss");
	SimpleDateFormat formatTo = new SimpleDateFormat("yyyy-MM-dd");

	public AllPartnersReportFetcher(String ruleName, String dbConnName) {
		super(ruleName);
		this.dbConnName = dbConnName;
	}

	@Override
	public RuleDecisionKey executeRule(FlowContext flowContext) {
		Response response = (Response) flowContext
				.getFromContext(EzedKeyConstants.EZED_RESPONSE);
		Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
		MessageData messageData = (MessageData) flowContext
				.getFromContext(MessageData.class.getName());
		PreparedStatement ps = null;
		ResultSet rs = null;

		String enddate;
		String startdate;
		String createdBy;
		String cityid;

		createdBy = messageData.getParameter("createdby");
		enddate = messageData.getParameter("enddate");
		startdate = messageData.getParameter("startdate");
		if ((startdate != null && startdate.compareTo("null") != 0 && startdate
				.compareTo("") != 0) && enddate.compareTo("null") == 0) {
			enddate = sourceFormat.format(new Date());
		}
		cityid = messageData.getParameter("cityid");
		String state = messageData.getParameter("state");
		String country = messageData.getParameter("country");

		String dynaQuery = "SELECT partner.partnerid,partner.contactperson,partner.partnername,partner.address,city.cityname,"
				+ "to_char(to_date(substring(partner.dateofjoining,0,9), 'yyyymmdd'),'yyyy-mm-dd') as dateofjoining,"
				+ "partner.addedby from partnerlist partner join citylist city on partner.city=city.cityid ";
		StringBuffer finalQuery = new StringBuffer(dynaQuery);

		String qryDate = " and partner.dateofjoining between '" + startdate
				+ "' and '" + enddate + "'";
		String qrycity = " and city.cityid='" + cityid + "'";
		String qryAdmin = " and partner.addedby='" + createdBy + "'";
		String qryState = " and partner.state='" + state + "'";
		String qryCountry = " and partner.country='" + country + "'";

		if (startdate != null && startdate.compareTo("null") != 0
				&& startdate.compareTo("") != 0) {
			finalQuery.append(qryDate);
		}

		if (createdBy.compareTo("null") != 0 && createdBy.compareTo("") != 0
				&& createdBy != null) {
			finalQuery.append(qryAdmin);
		}

		if (cityid.compareTo("null") != 0 && cityid.compareTo("") != 0
				&& cityid != null) {
			finalQuery.append(qrycity);
		}

		if (state.compareTo("null") != 0 && state.compareTo("") != 0
				&& state != null) {
			finalQuery.append(qryState);
		}

		if (country.compareTo("null") != 0 && country.compareTo("") != 0
				&& country != null) {
			finalQuery.append(qryCountry);
		}

		System.out.println("Final Qurey : " + finalQuery);

		String[] partnerData = null;
		FrontendFetchData partnerDetails = new FrontendFetchData();
		response.setResponseObject(partnerDetails);
		response.setStatusCodeSend(false);
		partnerDetails.setAttribName("aaData");
		ArrayList<String[]> partnerDataList = new ArrayList<String[]>();

		try {
			ps = conn.prepareStatement(finalQuery.toString());
			rs = ps.executeQuery();
			while (rs.next()) {
				partnerData = new String[7];
				partnerData[0] = rs.getString("partnerid");
				partnerData[1] = rs.getString("partnername");
				partnerData[2] = rs.getString("contactperson");
				partnerData[3] = rs.getString("address");
				partnerData[4] = rs.getString("cityname");
				partnerData[5] = rs.getString("dateofjoining");
				partnerData[6] = rs.getString("addedby");
				partnerDataList.add(partnerData);
			}
			partnerDetails.setAttribList(partnerDataList);
			response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
		} catch (SQLException e) {
			logger.error("Error while fetching partner details from db : ", e);
			response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED,
					"Error while fetching partner details");
			return RuleDecisionKey.ON_FAILURE;
		} catch (Exception e) {
			logger.error("Internal Error while fetching partner details : ", e);
			response.setParameters(EzedTransactionCodes.INTERNAL_ERROR,
					"Error while fetching partner details");
			return RuleDecisionKey.ON_FAILURE;
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					logger.error(
							"Error while closing result set while fetching Partners list",
							e);
				}
			}
			DbConnectionManager.releaseConnection(dbConnName, conn);
		}
		return RuleDecisionKey.ON_SUCCESS;
	}

}
