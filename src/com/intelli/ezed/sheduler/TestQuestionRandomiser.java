package com.intelli.ezed.sheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.atomic.AtomicInteger;

import org.sabre.db.DbConnectionManager;
import org.sabre.utils.threads.TimedTask;
import org.sabre.workflow.context.FlowContext;
import org.sabre.workflow.processor.InputProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.utils.StringSplitter;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.FIBData;
import com.intelli.ezed.data.MCQData;
import com.intelli.ezed.data.MTFData;
import com.intelli.ezed.data.QuestionContent;
import com.intelli.ezed.data.TestQuestionData;

public class TestQuestionRandomiser extends InputProcessor
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.RANDOM_QUESTION_LOG);
   private Resetter resetter;
   private FlowContext baseContext;
   private AtomicInteger atomicInteger;
   private Long throttleSleepTime;
   private int throttleParallelRecords;
   private SimpleDateFormat sdf = new SimpleDateFormat("HHmmss");

   public TestQuestionRandomiser(String startAtTime, String pollingTime, String dbConnName, String throttleSleepTime,
                                 String throttleParallelRecords)
   {
      atomicInteger = new AtomicInteger(1);
      Calendar calendar = Calendar.getInstance();
      calendar.add(Calendar.MINUTE, 1);
      resetter = new Resetter(sdf.format(calendar.getTime()), Long.parseLong(pollingTime), dbConnName);
      this.throttleSleepTime = Long.parseLong(throttleSleepTime);
      this.throttleParallelRecords = Integer.parseInt(throttleParallelRecords);
   }

   class Resetter extends TimedTask
   {
      private String dbConnName = null;
      public Resetter(String startAtTime, long pollingTime, String dbConnName)
      {
         super(startAtTime, pollingTime);
         this.dbConnName = dbConnName;
         this.setStartAtTime(startAtTime);
         this.setExecAtStartup(true);
         this.setOneTimeTask(false);
      }

      @Override
      public void doTimedTask()
      {
         //Put this in a new thread later so that timed task immediately goes ahead and
         //further runs of timed task don't get delayed
         SelectQuestionsToBeProcessed();
      }

      private void SelectQuestionsToBeProcessed()
      {
         //select questionid,courseid,chapterid,questiontype,questionstring,questioncontent,ans1type,ans1string,ans1content,ans2type,ans2string,ans2content,ans3type,ans3string,ans3content,ans4type,ans4string,ans4content,ans5type,ans5string,ans5content,ans6type,ans6string,ans6content,correctans,difficultylevel from questionbank where processed=0;
         logger.info("Test question randomiser started");
         Connection connection = DbConnectionManager.getConnectionByName(dbConnName);
         ResultSet rs = null;
         try
         {
            PreparedStatement selectStmt = DbConnectionManager.getPreparedStatement(dbConnName, connection, "SelectTestQuestions");
            selectStmt.setInt(1, 0);
            rs = selectStmt.executeQuery();
            FlowContext currentContext = null;
            int count = 0;
            while (rs.next())
            {
               TestQuestionData questData = new TestQuestionData();
               questData.setCourseid(rs.getString(2));
               questData.setChapterid(rs.getString(3));
               int questionType = rs.getInt(4);
               if (questionType == TestQuestionData.MCQ_QUESTION_TYPE)
               {
                  MCQData mcqData = new MCQData();
                  QuestionContent questionContent = new QuestionContent();
                  questionContent.setType(rs.getInt(5));
                  if (questionContent.getType() == QuestionContent.STRING_TYPE)
                  {
                     questionContent.setContentString(rs.getString(6));
                     questionContent.setContentLoc(null);
                  }
                  else
                  {
                     questionContent.setContentString(null);
                     questionContent.setContentLoc(rs.getString(7));
                  }
                  mcqData.setQuestion(questionContent);

                  for (int i = 8; i < 26; i += 3)
                  {
                     if (rs.getInt(i) == QuestionContent.CONTENT_ABSENT)
                     {
                        continue;
                     }
                     questionContent = new QuestionContent();
                     questionContent.setType(rs.getInt(i));
                     if (questionContent.getType() == QuestionContent.STRING_TYPE)
                     {
                        questionContent.setContentString(rs.getString(i + 1));
                        questionContent.setContentLoc(null);
                     }
                     else
                     {
                        questionContent.setContentString(null);
                        questionContent.setContentLoc(rs.getString(i + 2));
                     }
                     mcqData.getAnswers().add(questionContent);
                  }
                  mcqData.setCorrectAnswer(rs.getString(26));
                  QuestionContent explanation = new QuestionContent();
                  explanation.setType(rs.getInt(28));
                  explanation.setContentString(rs.getString(29));
                  explanation.setContentLoc(rs.getString(30));
                  mcqData.setExplanation(explanation);
                  questData.setTestQuestion(mcqData);
               }
               else if (questionType == TestQuestionData.MTF_QUESTION_TYPE)
               {
                  MTFData mtfData = new MTFData();
                  QuestionContent questionContent = new QuestionContent();
                  questionContent.setType(rs.getInt(5));
                  String mtfQuestion;
                  String[] mtfQuestions;
                  if (questionContent.getType() == QuestionContent.STRING_TYPE)
                  {
                     mtfQuestion = rs.getString(6);
                     mtfQuestions = StringSplitter.splitString(mtfQuestion, ";");
                     questionContent.setContentString(mtfQuestions[0]);
                     questionContent.setContentLoc(null);
                  }
                  else
                  {
                     mtfQuestion = rs.getString(7);
                     mtfQuestions = StringSplitter.splitString(mtfQuestion, ";");
                     questionContent.setContentString(null);
                     questionContent.setContentLoc(mtfQuestions[0]);
                  }
                  mtfData.setQuestion(questionContent);

                  int questionsTypes = Integer.parseInt(mtfQuestions[1]);
                  if (questionsTypes == QuestionContent.STRING_TYPE)
                  {
                     for (int i = 2; i < mtfQuestions.length; i++)
                     {
                        questionContent = new QuestionContent();
                        questionContent.setType(QuestionContent.STRING_TYPE);
                        questionContent.setContentString(mtfQuestions[i]);
                        questionContent.setContentLoc(null);
                        mtfData.getQuestions().add(questionContent);
                     }
                  }
                  else
                  {
                     for (int i = 2; i < mtfQuestions.length; i++)
                     {
                        questionContent = new QuestionContent();
                        questionContent.setType(QuestionContent.CONTENT_TYPE);
                        questionContent.setContentString(null);
                        questionContent.setContentLoc(mtfQuestions[i]);
                        mtfData.getQuestions().add(questionContent);
                     }
                  }

                  for (int i = 8; i < 25; i += 3)
                  {
                     if (rs.getInt(i) == QuestionContent.CONTENT_ABSENT)
                     {
                        continue;
                     }
                     questionContent = new QuestionContent();
                     questionContent.setType(rs.getInt(i));
                     if (questionContent.getType() == QuestionContent.STRING_TYPE)
                     {
                        questionContent.setContentString(rs.getString(i + 1));
                        questionContent.setContentLoc(null);
                     }
                     else
                     {
                        questionContent.setContentString(null);
                        questionContent.setContentLoc(rs.getString(i + 2));
                     }
                     mtfData.getAnswers().add(questionContent);
                  }
                  mtfData.setCorrectAnswer(rs.getString(26).toUpperCase());
                  QuestionContent explanation = new QuestionContent();
                  explanation.setType(rs.getInt(28));
                  explanation.setContentString(rs.getString(29));
                  explanation.setContentLoc(rs.getString(30));
                  mtfData.setExplanation(explanation);
                  questData.setTestQuestion(mtfData);
               }
               else if (questionType == TestQuestionData.FIB_QUESTION_TYPE)
               {
                  FIBData fibData = new FIBData();
                  QuestionContent questionContent = new QuestionContent();
                  questionContent.setType(rs.getInt(5));
                  if (questionContent.getType() == QuestionContent.STRING_TYPE)
                  {
                     questionContent.setContentString(rs.getString(6));
                     questionContent.setContentLoc(null);
                  }
                  else
                  {
                     questionContent.setContentString(null);
                     questionContent.setContentLoc(rs.getString(7));
                  }
                  fibData.setQuestion(questionContent);

                  for (int i = 8; i < 25; i += 3)
                  {
                     if (rs.getInt(i) == QuestionContent.CONTENT_ABSENT)
                     {
                        continue;
                     }
                     questionContent = new QuestionContent();
                     questionContent.setType(rs.getInt(i));
                     if (questionContent.getType() == QuestionContent.STRING_TYPE)
                     {
                        questionContent.setContentString(rs.getString(i + 1));
                        questionContent.setContentLoc(null);
                     }
                     else
                     {
                        questionContent.setContentString(null);
                        questionContent.setContentLoc(rs.getString(i + 2));
                     }
                     fibData.getAnswers().add(questionContent);
                  }
                  QuestionContent explanation = new QuestionContent();
                  explanation.setType(rs.getInt(28));
                  explanation.setContentString(rs.getString(29));
                  explanation.setContentLoc(rs.getString(30));
                  fibData.setExplanation(explanation);
                  questData.setTestQuestion(fibData);
               }
               else
               {
                  if (logger.isDebugEnabled())
                  {
                     logger.debug("Question type " + questionType + " not identified. Not processing record");
                  }
                  continue;
               }
               questData.getTestQuestion().setDifficultyLevel(rs.getInt(27));
               questData.getTestQuestion().setQuestionid(rs.getString(1));
               currentContext = baseContext.createSubContext(getSubContextName());
               currentContext.putIntoContext(EzedKeyConstants.QUESTION_OBJECT, questData);
               process(currentContext);
               try
               {
                  count++;
                  if (count == throttleParallelRecords)
                  {
                     count = 0;
                     Thread.sleep(throttleSleepTime);
                  }
               }
               catch (Exception e1)
               {
                  logger.error("Exception while sleeping thread", e1);
                  continue;
               }
            }
         }
         catch (Exception e)
         {
            logger.error("Exception occured while fetching paid cart details ", e);
         }
         finally
         {
            DbConnectionManager.releaseConnection(dbConnName, connection);
            if (rs != null)
            {
               try
               {
                  rs.close();
               }
               catch (SQLException e)
               {
                  logger.error("Error while closing resultset", e);
               }
            }
         }
      }
   }

   @Override
   public void initialize(FlowContext flowContext)
   {
      this.baseContext = flowContext;
   }

   @Override
   public void start()
   {
      resetter.startTask(true);
   }

   public String getSubContextName()
   {
      return "TestQuestionRandomizer" + atomicInteger.getAndIncrement();
   }
}
