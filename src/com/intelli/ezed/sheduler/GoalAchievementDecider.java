package com.intelli.ezed.sheduler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

import org.sabre.db.DbConnectionManager;
import org.sabre.utils.threads.TimedTask;
import org.sabre.workflow.context.FlowContext;
import org.sabre.workflow.processor.InputProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.GoalData;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.UserProfile;

public class GoalAchievementDecider extends InputProcessor
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private Resetter resetter;
   private FlowContext baseContext;
   private AtomicInteger atomicInteger;
   private Long throttleSleepTime;
   private int throttleParallelRecords;

   public GoalAchievementDecider(String startAtTime, String pollingTime, String dbConnName, String throttleSleepTime,
                                 String throttleParallelRecords)
   {
      atomicInteger = new AtomicInteger(1);
      resetter = new Resetter(startAtTime, Long.parseLong(pollingTime), dbConnName);
      this.throttleSleepTime = Long.parseLong(throttleSleepTime);
      this.throttleParallelRecords = Integer.parseInt(throttleParallelRecords);
   }

   class Resetter extends TimedTask
   {
      private String dbConnName = null;
      SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
      public Resetter(String startAtTime, long pollingTime, String dbConnName)
      {
         super(startAtTime, pollingTime);
         this.dbConnName = dbConnName;
         this.setStartAtTime(startAtTime);
         this.setExecAtStartup(true);
         this.setOneTimeTask(false);
      }

      @Override
      public void doTimedTask()
      {
         //Put this in a new thread later so that timed task immediately goes ahead and
         //further runs of timed task don't get delayed
         selectNonAcheivedGoals();
      }

      //select u.userid,courseid,chapterid,contentid,endtime,mobilenumber from userpoints up, users u where goalachieved=0 and to_date(?,'YYYYMMDDHH24MISS') > to_date(endtime,'YYYYMMDDHH24MISS') and u.userid=up.userid;
      private void selectNonAcheivedGoals()
      {
         logger.info("Starting scheduler to alert for non achieved goals");
         Connection connection = DbConnectionManager.getConnectionByName(dbConnName);
         ResultSet rs = null;
         try
         {
            PreparedStatement selectStmt = DbConnectionManager.getPreparedStatement(dbConnName, connection, "SelectUnachievedGoals");

            selectStmt.setString(1, sdf.format(new Date(System.currentTimeMillis())));

            rs = selectStmt.executeQuery();
            GoalData goalData = null;
            FlowContext currentContext = null;
            int count = 0;
            while (rs.next())
            {
               goalData = new GoalData();
               goalData.setCourseId(rs.getString("courseid"));
               goalData.setChapterId(rs.getString("chapterid"));
               goalData.setContentId(rs.getString("contentid"));
               goalData.setEndDate(sdf.parse(rs.getString("endtime")));
               goalData.setContentname(rs.getString("contentname"));
               goalData.setCoursename(rs.getString("coursename"));

               UserProfile userProfile = new UserProfile();
               userProfile.setUserid(rs.getString("userid"));
               userProfile.setMobileNumber(rs.getString("mobilenumber"));
               userProfile.setGoalsAlertStatus(rs.getString("goalsalertstatus"));

               currentContext = baseContext.createSubContext(getSubContextName());
               currentContext.putIntoContext(EzedKeyConstants.USER_DETAILS, userProfile);
               currentContext.putIntoContext(EzedKeyConstants.USER_ID, userProfile.getUserid());
               currentContext.putIntoContext(EzedKeyConstants.EXPIRED_GOAL, goalData);
               currentContext.putIntoContext(EzedKeyConstants.EZED_RESPONSE, new Response());
               process(currentContext);
               try
               {
                  count++;
                  if (count == throttleParallelRecords)
                  {
                     count = 0;
                     Thread.sleep(throttleSleepTime);
                  }
               }
               catch (Exception e1)
               {
                  logger.error("Exception while sleeping thread", e1);
                  continue;
               }
            }
         }
         catch (Exception e)
         {
            logger.error("Exception occured while fetching paid cart details ", e);
         }
         finally
         {
            DbConnectionManager.releaseConnection(dbConnName, connection);
            if (rs != null)
            {
               try
               {
                  rs.close();
               }
               catch (SQLException e)
               {
                  logger.error("Error while closing resultset", e);
               }
            }
         }
      }
   }

   @Override
   public void initialize(FlowContext flowContext)
   {
      this.baseContext = flowContext;
   }

   @Override
   public void start()
   {
      resetter.startTask(true);
   }

   public String getSubContextName()
   {
      return "GoalExpiryHandler" + atomicInteger.getAndIncrement();
   }
}
