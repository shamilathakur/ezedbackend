package com.intelli.ezed.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.atomic.AtomicInteger;

import org.sabre.db.DbConnectionManager;
import org.sabre.utils.threads.TimedTask;
import org.sabre.workflow.context.FlowContext;
import org.sabre.workflow.processor.InputProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.UserProfile;

public class NewRegistrationEmailSender extends InputProcessor {
	static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
	Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
	private Resetter resetter;
	private FlowContext baseContext;
	private AtomicInteger atomicInteger1;
	private Long throttleSleepTime;
	private int throttleParallelRecords;

	public NewRegistrationEmailSender(String startAtTime, String pollingTime,
			String dbConnName, String throttleSleepTime,
			String throttleParallelRecords) {
		atomicInteger1 = new AtomicInteger(1);
		resetter = new Resetter(startAtTime, Long.parseLong(pollingTime),
				dbConnName);
		this.throttleSleepTime = Long.parseLong(throttleSleepTime);
		this.throttleParallelRecords = Integer
				.parseInt(throttleParallelRecords);
	}

	class Resetter extends TimedTask {
		private String dbConnName = null;

		public Resetter(String startAtTime, long pollingTime, String dbConnName) {
			super(startAtTime, pollingTime);
			this.dbConnName = dbConnName;
			this.setStartAtTime(startAtTime);
			this.setExecAtStartup(false);
			this.setOneTimeTask(false);
		}

		@Override
		public void doTimedTask() {
			extractNamesToSendEmail();
		}

		private void extractNamesToSendEmail() {
			logger.info("Checking the new registraions .. ");
			Connection connection = DbConnectionManager
					.getConnectionByName(dbConnName);
			ResultSet rs = null;
			try {
				Calendar today = Calendar.getInstance();
				String todayNewDate = sdf.format(today.getTime()).substring(0,
						8)
						+ "000000";
				Calendar date2DaysAgoCalendar = Calendar.getInstance();
				date2DaysAgoCalendar.add(Calendar.DATE, -2);
				String date2DaysAgoString = sdf.format(
						date2DaysAgoCalendar.getTime()).substring(0, 8)
						+ "000000";
				PreparedStatement ps = connection
						.prepareStatement("SELECT userid,firstname,createtime from users WHERE createtime between '"
								+ date2DaysAgoString
								+ "' AND '"
								+ todayNewDate
								+ "' and chkflag != '0'");
				System.out.println("Query : " + ps);
				rs = ps.executeQuery();
				FlowContext currentContext = null;
				int count = 0;
				while (rs.next()) {
					String createtime = rs.getString("createtime").substring(0,
							8)
							+ "000000";
					int diffInDays = (int) ((sdf.parse(todayNewDate).getTime() - sdf
							.parse(createtime).getTime()) / (1000 * 60 * 60 * 24));
					if (diffInDays != 0) {
						UserProfile userProfile = new UserProfile();
						userProfile.setUserid(rs.getString("userid"));
						userProfile.setFirstName(rs.getString("firstname"));

						System.out.println("No Of days : " + diffInDays
								+ "	userid : " + userProfile.getUserid());
						currentContext = baseContext
								.createSubContext(getSubContextName());
						currentContext.putIntoContext(
								EzedKeyConstants.USER_DETAILS, userProfile);
						currentContext.putIntoContext(EzedKeyConstants.USER_ID,
								userProfile.getUserid());
						currentContext.putIntoContext(
								EzedKeyConstants.DAY_NUMBER, diffInDays);
						currentContext.putIntoContext(
								EzedKeyConstants.EZED_RESPONSE, new Response());
						process(currentContext);
						try {
							count++;
							if (count == throttleParallelRecords) {
								count = 0;
								Thread.sleep(throttleSleepTime);
							}
						} catch (Exception e1) {
							logger.error("Exception while sleeping thread", e1);
							continue;
						}
					}
				}
			} catch (Exception e) {
				logger.error(
						"Exception occured while fetching registered users in last 3 days ::",
						e);
			} finally {
				DbConnectionManager.releaseConnection(dbConnName, connection);
				if (rs != null) {
					try {
						rs.close();
					} catch (SQLException e) {
						logger.error("Error while closing resultset", e);
					}
				}
			}
		}
	}

	@Override
	public void initialize(FlowContext flowContext) {
		// TODO Auto-generated method stub
		this.baseContext = flowContext;
	}

	public String getSubContextName() {
		return "PromoEmailSender" + atomicInteger1.getAndIncrement();
	}

	@Override
	public void start() {
		resetter.startTask(true);

	}
}