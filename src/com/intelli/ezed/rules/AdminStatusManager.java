package com.intelli.ezed.rules;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.a.helper.utils.StringSplitter;
import com.intelli.ezed.data.AdminProfile;
import com.intelli.ezed.data.AdminStatusData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.utils.AccountStatusHelper;

public class AdminStatusManager extends SingletonRule
{

   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
   private String dbConnName;

   private String AUTH = "AUTH"; //it is actually verified status
   private String UNAUTH = "UNAUTH";
   private String BLOCK = "BLOCK";
   private String UNBLOCK = "UNBLOCK";

   public AdminStatusManager(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      AdminProfile adminProfile = (AdminProfile) flowContext.getFromContext(EzedKeyConstants.ADMIN_PROFILE);
      String loggedInUserId = adminProfile.getUserid();

      //email=admin@b.com&sessionid=0000000001&userid=admin@ezed.com&changestatus=UNBLOCK&changeroll=1,2,5&newname=satish2

      String adminUserid = messageData.getParameter("userid");
      if (adminUserid == null || adminUserid.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Admin user id is invalid " + adminUserid);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid admin user id for updation");
         return RuleDecisionKey.ON_FAILURE;
      }

      if (adminUserid.compareToIgnoreCase(loggedInUserId) == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Admin cannot change his own user status");
         }
         response.setParameters(EzedTransactionCodes.CANNOT_CHANGE_LOGGEDIN_USER_STATUS, "Admin cannot change own status");
         return RuleDecisionKey.ON_FAILURE;
      }

      String changeToStatus = messageData.getParameter("changestatus");
      if (changeToStatus == null || changeToStatus.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Change to status is invalid " + adminUserid);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid new status for updation");
         return RuleDecisionKey.ON_FAILURE;
      }
      String name = messageData.getParameter("newname");
      if (name == null || name.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("New admin name is invalid " + adminUserid);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid new admin name for updation");
         return RuleDecisionKey.ON_FAILURE;
      }

      AdminStatusData userStatusData = new AdminStatusData();
      userStatusData.setName(name);
      Connection connection = DbConnectionManager.getConnectionByName(dbConnName);
      ResultSet resultSet = null;

      try
      {
         //select * from adminusers where userid = ?
         PreparedStatement preparedStatement = DbConnectionManager.getPreparedStatement(dbConnName, connection, "GetAdminStatus");
         preparedStatement.setString(1, adminUserid);
         resultSet = preparedStatement.executeQuery();

         if (resultSet != null && resultSet.next())
         {
            userStatusData.setChkflag(resultSet.getInt("chkflag"));
         }
         else
         {
            logger.error("Error while getting admin profile.");
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "No admin profile found.");
            return RuleDecisionKey.ON_FAILURE;
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while getting admin profile.", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while getting admin profile.");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while getting admin profile.", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while getting admin profile.");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         try
         {
            resultSet.close();
         }
         catch (SQLException e)
         {
            logger.error("Error while closing resultset", e);
         }
         catch (Exception e)
         {
            logger.error("Error while closing resultset", e);
         }
         DbConnectionManager.releaseConnection(dbConnName, connection);
      }

      int chkflag = userStatusData.getChkflag();
      String[] temp = StringSplitter.splitString(changeToStatus, ",");


      if (logger.isDebugEnabled())
      {
         logger.debug("Chkflag before : " + chkflag);
      }
      for (String changeTo : temp)
      {
         if (changeTo.compareTo(AUTH) == 0)
         {
            chkflag = AccountStatusHelper.setValidate(chkflag);
         }
         else if (changeTo.compareTo(UNAUTH) == 0)
         {
            chkflag = AccountStatusHelper.resetValidate(chkflag);
         }
         else if (changeTo.compareTo(BLOCK) == 0)
         {
            chkflag = AccountStatusHelper.setLock(chkflag);
         }
         else if (changeTo.compareTo(UNBLOCK) == 0)
         {
            chkflag = AccountStatusHelper.resetLock(chkflag);
         }
         else
         {
            logger.error("Invalid action. : " + changeTo);
            response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid status change Requested.");
            return RuleDecisionKey.ON_FAILURE;
         }
      }

      userStatusData.setChkflag(chkflag);
      if (logger.isDebugEnabled())
      {
         logger.debug("Chkflag after : " + chkflag);
      }

      connection = DbConnectionManager.getConnectionByName(dbConnName);

      try
      {
         //update adminusers set chkflag = ?, name =? where userid = ?
         PreparedStatement updateAdminUser = DbConnectionManager.getPreparedStatement(dbConnName, connection, "UpdateAdminUser");
         updateAdminUser.setInt(1, chkflag);
         updateAdminUser.setString(2, name);
         updateAdminUser.setString(3, adminUserid);

         int i = updateAdminUser.executeUpdate();

         if (i != 1)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Error while updating user details. Number of rows updated : " + i);
            }
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while updating the user. Please check userid and try again.");
            return RuleDecisionKey.ON_FAILURE;
         }

      }
      catch (Exception e)
      {
         logger.error("Exception occoured while commiting admin profile.", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Internal Error please try again.");
         return RuleDecisionKey.ON_FAILURE;
      }

      response.setParameters(EzedTransactionCodes.PROCESSING_REQUEST, "Admin chkflag and name updated successfully.");
      return RuleDecisionKey.ON_SUCCESS;
   }
}
