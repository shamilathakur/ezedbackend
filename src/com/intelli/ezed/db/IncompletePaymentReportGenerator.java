package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.FrontendFetchData;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.utils.EzedHelper;

public class IncompletePaymentReportGenerator extends SingletonRule {

	private String dbConnName;
	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
	SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyyMMddHHmmss");

	public IncompletePaymentReportGenerator(String ruleName, String dbConnName) {
		super(ruleName);
		this.dbConnName = dbConnName;

	}

	@Override
	public RuleDecisionKey executeRule(FlowContext flowContext) {

		Response response = (Response) flowContext
				.getFromContext(EzedKeyConstants.EZED_RESPONSE);
		MessageData messageData = (MessageData) flowContext
				.getFromContext(MessageData.class.getName());

		String start = messageData.getParameter("start");
		String end = messageData.getParameter("end");
		if ((start != null && start.compareTo("null") != 0 && start.compareTo("") != 0 )
				&& end.compareTo("null") == 0) {
			end = sourceFormat.format(new Date());
		}

		String univname = messageData.getParameter("university");
		String college = messageData.getParameter("college");
		String cityid = messageData.getParameter("city");
		String courseid = messageData.getParameter("courseid");

		String paymentincomplete = "SELECT pd.paymentid,pd.userid,us.firstname,"
				+ "coalesce(us.mobilenumber,'NA') as mobilenumber,coalesce(pd.paymentstatus,'0') as paymentstatus,pd.cartstatus,"
				+ "to_char(to_date(substring(pd.intime,0,9), 'yyyymmdd'),'dd/mm/yyyy') as intime,pd.cart,pd.totalamount from paydata pd "
				+ "join users us on pd.userid=us.userid and (pd.paymentstatus!='1' or pd.paymentstatus is null)	";

		String qryDate = " AND pd.intime between '" + start + "' and '" + end
				+ "'";

		String qrycity = " AND us.cityid ='" + cityid + "'";

		String qryUniv = " AND us.univname ='" + univname + "'";

		String qryCollege = " AND us.collegename ='"
				+ college.replaceAll("'", "''") + "'";

		if (univname != null && univname.trim() != ""
				&& univname.compareTo("null") != 0) {
			paymentincomplete = paymentincomplete + qryUniv;
		}
		if (college != null && college.trim() != ""
				&& college.compareTo("null") != 0) {
			paymentincomplete = paymentincomplete + qryCollege;
		}
		if (cityid != null && cityid.trim() != ""
				&& cityid.compareTo("null") != 0) {
			paymentincomplete = paymentincomplete + qrycity;
		}
		if (start != null && start.trim() != "" && start.compareTo("null") != 0) {
			paymentincomplete = paymentincomplete + qryDate;
		}

		Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
		PreparedStatement ps = null;
		ResultSet rs = null;
		String coursename = null;

		if (courseid != null && courseid.trim() != ""
				&& courseid.compareTo("null") != 0) {
			coursename = EzedHelper.fetchCourseNameForCourseId(dbConnName,
					courseid, logger);

		}

		FrontendFetchData responseData = new FrontendFetchData();
		response.setResponseObject(responseData);
		response.setStatusCodeSend(false);
		responseData.setAttribName("aaData");
		ArrayList<String[]> recordList = new ArrayList<String[]>();

		try {
			ps = conn.prepareStatement(paymentincomplete);
			System.out.println("Query : " + ps);
			rs = ps.executeQuery();
			while (rs.next()) {
				if (courseid != null && courseid.trim() != ""
						&& courseid.compareTo("null") != 0) {
					String[] cartdata = rs.getString("cart").split(";");
					for (int i = 0; i < cartdata.length; i++) {

						if (cartdata[i].contains(courseid)) {

							String coursetype = cartdata[i].split(",")[1];

							String[] reportData = new String[10];
							reportData[0] = rs.getString("userid");
							reportData[1] = rs.getString("firstname");
							reportData[2] = rs.getString("mobilenumber");
							reportData[3] = coursename;
							int type = Integer.parseInt(coursetype);
							if (type == 1) {
								reportData[4] = "Content Only";
							} else if (type == 2) {
								reportData[4] = "Test Only";
							} else if (type == 3) {
								reportData[4] = "Full Course";
							}
							reportData[5] = rs.getString("intime");
							reportData[6] = rs.getString("paymentid");
							if (Integer.parseInt(rs.getString("paymentstatus")) == 2) {
								reportData[7] = "Payment Unsuccessful";
							} else {
								reportData[7] = "Not Attempted";
							}
							if (Integer.parseInt(rs.getString("cartstatus")) == 0) {
								reportData[8] = "Cart Added to User";
							} else if (Integer.parseInt(rs
									.getString("cartstatus")) == 2) {
								reportData[8] = "Payment Timeout !";
							}
							reportData[9] = rs.getInt("totalamount") + "";
							recordList.add(reportData);
						}
					}

				}

				else {
					String cart[] = rs.getString("cart").split(";");
					for (int i = 0; i < cart.length; i++) {
						String[] reportData = new String[10];
						reportData[0] = rs.getString("userid");
						reportData[1] = rs.getString("firstname");
						reportData[2] = rs.getString("mobilenumber");
						reportData[3] = EzedHelper.fetchCourseName(dbConnName,
								cart[i].split(",")[0], logger);
						int type = Integer.parseInt(cart[i].split(",")[1]);
						if (type == 1) {
							reportData[4] = "Content Only";
						} else if (type == 2) {
							reportData[4] = "Test Only";
						} else if (type == 3) {
							reportData[4] = "Full Course";
						}
						reportData[5] = rs.getString("intime");
						reportData[6] = rs.getString("paymentid");
						if (Integer.parseInt(rs.getString("paymentstatus")) == 2) {
							reportData[7] = "Payment Unsuccessful";
						} else {
							reportData[7] = "Not Attempted";
						}
						if (Integer.parseInt(rs.getString("cartstatus")) == 0) {
							reportData[8] = "Cart Added to User";
						} else if (Integer.parseInt(rs.getString("cartstatus")) == 2) {
							reportData[8] = "Payment Timeout !";
						}
						reportData[9] = rs.getInt("totalamount") + "";
						recordList.add(reportData);
					}
				}
			}
			responseData.setAttribList(recordList);
			response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
		} catch (SQLException e) {
			logger.error("Error while fetching Utilized SC details from db : ",
					e);
			response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED,
					"Error while fetching Utilized SC details");
			return RuleDecisionKey.ON_FAILURE;
		} catch (Exception e) {
			logger.error(
					"Internal Error while fetching Utilized SC details : ", e);
			response.setParameters(EzedTransactionCodes.INTERNAL_ERROR,
					"Error while fetching Utilized SC details");
			return RuleDecisionKey.ON_FAILURE;
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					logger.error(
							"Error while closing result set while fetching Utilized SC list",
							e);
				}
			}
			DbConnectionManager.releaseConnection(dbConnName, conn);
		}

		return RuleDecisionKey.ON_SUCCESS;
	}
}
