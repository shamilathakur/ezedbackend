package com.intelli.ezed.bulkupload.data;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import org.sabre.db.DbConnectionManager;
import org.slf4j.Logger;

import com.a.helper.utils.StringSplitter;
import com.intelli.ezed.data.FIBData;
import com.intelli.ezed.data.MCQData;
import com.intelli.ezed.data.MTFData;
import com.intelli.ezed.data.QuestionContent;
import com.intelli.ezed.data.TestQuestion;
import com.intelli.ezed.data.TestQuestionData;
import com.intelli.ezed.utils.EzedHelper;
import com.intelli.ezed.utils.PrepareCourseIdBackend;
import com.intelli.ezed.utils.PrepareOfferingIdBackend;

public class CourseDetails
{
   private static PrepareOfferingIdBackend prepareOfferingId = new PrepareOfferingIdBackend("OFF2", "ezeddb");
   private static PrepareCourseIdBackend prepareCourseId = new PrepareCourseIdBackend("COU2", "ezeddb");
   private String courseDirName;
   private String univname;
   private String degree;
   private String stream;
   private String year;
   private String semester;
   private String offeringId;
   private String courseName;
   private String courseId;
   private String shortDesc;
   private int longDescType;
   private String longDescString;
   private String longDescContent;
   private String introVid;
   private String defChapter;
   private String lmr;
   private String univQuestPaper;
   private String univSolution;
   private String endDate;
   private File courseFile;
   private String contentCost;
   private String testCost;
   private String totalCost;
   private AtomicInteger subsetChapterCount = new AtomicInteger(0);
   private AtomicInteger learningPathChapterCount = new AtomicInteger(0);
   private String[] chapterPath;
   private Map<String, String> chapterDirIdMap = new HashMap<String, String>();
   private AtomicBoolean isCoursePersisted = new AtomicBoolean(false);
   private Object courseLock = new Object();
   private AtomicBoolean isCoursePathPersisted = new AtomicBoolean(false);
   private Object learningPathLock = new Object();
   private ArrayList<TestDetails> testDetails = new ArrayList<TestDetails>();

   public void addTestDetails(TestDetails testDetails)
   {
      this.testDetails.add(testDetails);
   }

   public String getContentCost()
   {
      return contentCost;
   }

   public void setContentCost(String contentCost)
   {
      this.contentCost = EzedHelper.handleBulkUploadStringUpperCase(contentCost, 6);
   }

   public String getTestCost()
   {
      return testCost;
   }

   public void setTestCost(String testCost)
   {
      this.testCost = EzedHelper.handleBulkUploadStringUpperCase(testCost, 6);
   }

   public String getTotalCost()
   {
      return totalCost;
   }

   public void setTotalCost(String totalCost)
   {
      this.totalCost = EzedHelper.handleBulkUploadStringUpperCase(totalCost, 6);
   }

   public String[] getChapterPath()
   {
      return chapterPath;
   }

   public void setChapterPath(String commaSeparatedChapterPath)
   {
      this.chapterPath = StringSplitter.splitString(commaSeparatedChapterPath, ",");
      learningPathChapterCount.addAndGet(chapterPath.length);
   }

   public String getCommaSeparatedChapterPath()
   {
      StringBuffer sb = new StringBuffer();
      for (int i = 0; i < chapterPath.length; i++)
      {
         sb.append(chapterPath[i]).append(",");
      }
      sb.deleteCharAt(sb.length() - 1);
      return sb.toString();
   }

   public void putChapterDirIdMap(String chapterDirName, String chapterId, String dbConnName, Logger logger, Map<String, String> contentDirIdMap) throws Exception
   {
      chapterDirIdMap.put(chapterDirName, chapterId);
      for (String learningPathChapter : chapterPath)
      {
         if (learningPathChapter.compareToIgnoreCase(chapterDirName) == 0)
         {
            learningPathChapterCount.decrementAndGet();
         }
      }
      if (subsetChapterCount.get() != 0)
      {
         if (getDefChapter() != null)
         {
            if (getDefChapter().compareToIgnoreCase(chapterDirName) == 0)
            {
               subsetChapterCount.decrementAndGet();
            }
         }

         if (getLmr() != null)
         {
            if (getLmr().compareToIgnoreCase(chapterDirName) == 0)
            {
               subsetChapterCount.decrementAndGet();
            }
         }

         if (getUnivQuestPaper() != null)
         {
            if (getUnivQuestPaper().compareToIgnoreCase(chapterDirName) == 0)
            {
               subsetChapterCount.decrementAndGet();
            }
         }

         if (getUnivSolution() != null)
         {
            if (getUnivSolution().compareToIgnoreCase(chapterDirName) == 0)
            {
               subsetChapterCount.decrementAndGet();
            }
         }
      }

      if (subsetChapterCount.get() == 0)
      {
         synchronized (courseLock)
         {
            if (isCoursePersisted.get() == false)
            {
               persistCourse(dbConnName, logger, contentDirIdMap);
               persistCourseOffering(dbConnName, logger);
               persistEnclosedTests(dbConnName, logger);
               isCoursePersisted.set(true);
            }
         }
      }

      if (learningPathChapterCount.get() == 0)
      {
         synchronized (learningPathLock)
         {
            if (isCoursePersisted.get() == true && isCoursePathPersisted.get() == false)
            {
               int count = persistCoursePath(dbConnName, logger);
               persistTestPath(dbConnName, logger, count);
               isCoursePathPersisted.set(true);
            }
         }
      }
   }

   public String getUnivname()
   {
      return univname;
   }
   public void setUnivname(String univname)
   {
      this.univname = univname;
   }
   public String getDegree()
   {
      return degree;
   }
   public void setDegree(String degree)
   {
      this.degree = EzedHelper.handleBulkUploadStringUpperCase(degree, 50);
   }
   public String getStream()
   {
      return stream;
   }
   public void setStream(String stream)
   {
      this.stream = EzedHelper.handleBulkUploadStringUpperCase(stream, 50);
   }
   public String getYear()
   {
      return year;
   }
   public void setYear(String year)
   {
      this.year = EzedHelper.handleBulkUploadStringUpperCase(year, 10);;
   }
   public String getSemester()
   {
      return semester;
   }
   public void setSemester(String semester)
   {
      this.semester = EzedHelper.handleBulkUploadStringUpperCase(semester, 10);
   }
   public String getOfferingId()
   {
      return offeringId;
   }
   public void setOfferingId(String offeringId)
   {
      this.offeringId = offeringId;
   }
   public void setOfferingIdCalculated(String dbConnName, Logger logger) throws Exception
   {
      this.fetchOfferingId(dbConnName, logger);
   }
   public String getCourseName()
   {
      return courseName;
   }
   public void setCourseName(String courseName)
   {
      this.courseName = EzedHelper.handleBulkUploadStringUpperCase(courseName, 200);
   }
   public String getCourseId()
   {
      return courseId;
   }
   public void setCourseId(String courseId)
   {
      this.courseId = courseId;
   }
   public void setNewCourseId()
   {
      setCourseId(fetchNewCourseId());
   }
   public String getShortDesc()
   {
      return shortDesc;
   }
   public void setShortDesc(String shortDesc)
   {
      this.shortDesc = EzedHelper.handleBulkUploadString(shortDesc, 800);
   }
   public int getLongDescType()
   {
      return longDescType;
   }
   public void setLongDescType(int longDescType)
   {
      this.longDescType = longDescType;
   }
   public String getLongDescString()
   {
      return longDescString;
   }
   public void setLongDescString(String longDescString)
   {
      this.longDescString = EzedHelper.handleBulkUploadString(longDescString, 500);
   }
   public String getLongDescContent()
   {
      return longDescContent;
   }
   public void setLongDescContent(String longDescContent)
   {
      this.longDescContent = longDescContent;
   }
   public String getIntroVid()
   {
      return introVid;
   }
   public void setIntroVid(String introVid)
   {
      this.introVid = introVid;
   }
   public String getDefChapter()
   {
      return defChapter;
   }
   public void setDefChapter(String defChapter)
   {
      if (defChapter.trim().compareTo("") == 0)
      {
         defChapter = null;
      }
      this.defChapter = defChapter;
      if (defChapter != null)
      {
         subsetChapterCount.incrementAndGet();
      }
   }
   public String getLmr()
   {
      return lmr;
   }
   public void setLmr(String lmr)
   {
      if (lmr.trim().compareTo("") == 0)
      {
         lmr = null;
      }
      this.lmr = lmr;
      if (lmr != null)
      {
         subsetChapterCount.incrementAndGet();
      }
   }
   public String getUnivQuestPaper()
   {
      return univQuestPaper;
   }
   public void setUnivQuestPaper(String univQuestPaper)
   {
      if (univQuestPaper.trim().compareTo("") == 0)
      {
         univQuestPaper = null;
      }
      this.univQuestPaper = univQuestPaper;
      if (univQuestPaper != null)
      {
         subsetChapterCount.incrementAndGet();
      }
   }
   public String getUnivSolution()
   {
      return univSolution;
   }
   public void setUnivSolution(String univSolution)
   {
      if (univSolution.trim().compareTo("") == 0)
      {
         univSolution = null;
      }
      this.univSolution = univSolution;
      if (univSolution != null)
      {
         subsetChapterCount.incrementAndGet();
      }
   }
   public String getEndDate()
   {
      return endDate;
   }
   public void setEndDate(String endDate)
   {
      this.endDate = endDate;
   }

   private void fetchOfferingId(String dbConnName, Logger logger) throws Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      String offeringId = null;

      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchOfferingId");
         ps.setString(1, getUnivname());
         ps.setString(2, getDegree());
         ps.setString(3, getStream());
         ps.setString(4, getYear());
         ps.setString(5, getSemester());
         rs = ps.executeQuery();
         if (rs.next())
         {
            offeringId = rs.getString(1);
            if (offeringId == null || offeringId.trim().compareTo("") == 0)
            {
               offeringId = null;
            }
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching offering id from db", e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching offering id from db", e);
         throw e;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing resultset while fetching offering id from db", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }

      if (offeringId == null)
      {
         offeringId = fetchNewOfferingId();
         setOfferingId(offeringId);
         persistOffering(dbConnName, logger);
      }
      else
      {
         setOfferingId(offeringId);
      }
   }

   private String fetchNewOfferingId()
   {
      return prepareOfferingId.getNextId();
   }

   private String fetchNewCourseId()
   {
      return prepareCourseId.getNextId();
   }

   private void persistOffering(String dbConnName, Logger logger) throws Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertOffering");
         ps.setString(1, getUnivname());
         ps.setString(2, getDegree());
         ps.setString(3, getStream());
         ps.setString(4, getYear());
         ps.setString(5, getSemester());
         ps.setString(6, getOfferingId());
         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in db while inserting offering");
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while inserting offering in db", e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while inserting offering in db", e);
         throw e;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   private void persistEnclosedTests(String dbConnName, Logger logger) throws Exception
   {
      for (int i = 0; i < testDetails.size(); i++)
      {
         testDetails.get(i).setNewTestId();
         testDetails.get(i).setCourseId(this.getCourseId());
         testDetails.get(i).persistTest(dbConnName, logger);
      }
   }

   private void persistCourse(String dbConnName, Logger logger, Map<String, String> contentDirIdMap) throws Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertCourse");
         ps.setString(1, getCourseId());
         ps.setString(2, getCourseName());
         ps.setString(3, getShortDesc());
         ps.setString(4, contentDirIdMap.get(getIntroVid()));
         ps.setString(5, chapterDirIdMap.get(getDefChapter()));
         ps.setString(6, chapterDirIdMap.get(getLmr()));
         ps.setString(7, getContentCost());
         ps.setString(8, getTestCost());
         ps.setString(9, getTotalCost());
         ps.setInt(10, getLongDescType());
         ps.setString(11, getLongDescString());
         ps.setString(12, getLongDescContent() != null ? contentDirIdMap.get(getLongDescContent()) : null);
         ps.setString(13, chapterDirIdMap.get(getUnivQuestPaper()));
         ps.setString(14, chapterDirIdMap.get(getUnivSolution()));
         ps.setString(15, getEndDate());

         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in db while inserting course only " + courseName);
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while inserting course in db", e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while inserting course in db", e);
         throw e;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   private void persistCourseOffering(String dbConnName, Logger logger) throws Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertCourseOffering");
         ps.setString(1, getOfferingId());
         ps.setString(2, getCourseId());

         int rows = ps.executeUpdate();
         if (logger.isDebugEnabled())
         {
            logger.debug(rows + " rows inserted in db while inserting course offering");
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while inserting course offering in db", e);
         throw e;
      }
      catch (Exception e)
      {
         logger.error("Error while inserting course offering in db", e);
         throw e;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   private void persistTestPath(String dbConnName, Logger logger, int count) throws Exception
   {
      for (int i = count; i < testDetails.size(); i++)
      {
         testDetails.get(i).persistTestInCoursePath(dbConnName, logger, i + 1);
      }
   }

   private int persistCoursePath(String dbConnName, Logger logger) throws Exception
   {
      Connection conn = null;
      PreparedStatement ps = null;
      int count = 0;
      for (int i = 0; i < chapterPath.length; i++)
      {
         conn = DbConnectionManager.getConnectionByName(dbConnName);
         try
         {
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "PersistCoursePath");
            ps.setString(1, getCourseId());
            ps.setString(2, chapterDirIdMap.get(chapterPath[i]));
            ps.setInt(3, (i + 1));

            int rows = ps.executeUpdate();
            if (logger.isDebugEnabled())
            {
               logger.debug(rows + " rows inserted in db while inserting course path");
            }
            count++;
         }
         catch (SQLException e)
         {
            logger.error("Error while inserting course path in db", e);
            //            throw e;
         }
         catch (Exception e)
         {
            logger.error("Error while inserting course path in db", e);
            //            throw e;
         }
         finally
         {
            DbConnectionManager.releaseConnection(dbConnName, conn);
         }
      }
      return count;
   }
   public void setCourseFile(File singleCourseFile)
   {
      this.courseFile = singleCourseFile;
   }

   public File getCourseFile()
   {
      return this.courseFile;
   }

   public void printDetails(Logger logger)
   {
      if (logger.isTraceEnabled())
      {
         logger.trace("coursename " + getCourseName());
         logger.trace("courseid " + getCourseId());
         logger.trace("short desc " + getShortDesc());
         logger.trace("long desc type " + getLongDescType());
         logger.trace("long desc string " + getLongDescString());
         logger.trace("long desc content " + getLongDescContent());
         logger.trace("intro vid " + getIntroVid());
         logger.trace("def chapter " + getDefChapter());
         logger.trace("lmr " + getLmr());
         logger.trace("univ quest paper " + getUnivQuestPaper());
         logger.trace("univ solution " + getUnivSolution());
         logger.trace("end date " + getEndDate());
      }
   }

   public void writeToFile(String rootDir, String courseDetailsFileName, ArrayList<TestDetails> tests) throws IOException
   {
      File file = new File(rootDir + File.separator + getUnivname());
      if (!file.exists())
      {
         file.mkdir();
      }

      file = new File(file.getAbsolutePath() + File.separator + getDegree());
      if (!file.exists())
      {
         file.mkdir();
      }

      file = new File(file.getAbsolutePath() + File.separator + getStream());
      if (!file.exists())
      {
         file.mkdir();
      }

      file = new File(file.getAbsolutePath() + File.separator + getYear());
      if (!file.exists())
      {
         file.mkdir();
      }

      file = new File(file.getAbsolutePath() + File.separator + getSemester());
      if (!file.exists())
      {
         file.mkdir();
      }

      file = new File(file.getAbsolutePath() + File.separator + getCourseId());
      if (!file.exists())
      {
         file.mkdir();
      }

      File courseFile = new File(file.getAbsolutePath() + File.separator + courseDetailsFileName);
      if (!courseFile.exists())
      {
         courseFile.createNewFile();
      }
      this.courseDirName = file.getAbsolutePath();
      FileOutputStream fos = null;
      try
      {
         fos = new FileOutputStream(courseFile);
         StringBuffer sb = new StringBuffer();
         sb.append("<coursedetails>\n");
         sb.append("<name>");
         sb.append(getCourseName());
         sb.append("</name>\n");
         sb.append("<id>");
         sb.append(getCourseId());
         sb.append("</id>\n");
         sb.append("<longdesctype>");
         sb.append(getLongDescType());
         sb.append("</longdesctype>\n");
         if (getLongDescType() == QuestionContent.STRING_TYPE)
         {
            sb.append("<longdescstring>");
            sb.append(getLongDescString());
            sb.append("</longdescstring>\n");
         }
         else
         {
            sb.append("<longdesccontent>");
            sb.append(getLongDescContent());
            sb.append("</longdesccontent>\n");
         }
         sb.append("<introvid>");
         sb.append(getIntroVid());
         sb.append("</introvid>\n");
         sb.append("<defchapter>");
         sb.append(getDefChapter());
         sb.append("</defchapter>\n");
         sb.append("<lmr>");
         sb.append(getLmr());
         sb.append("</lmr>\n");
         sb.append("<univquestpaper>");
         sb.append(getUnivQuestPaper());
         sb.append("</univquestpaper>\n");
         sb.append("<univsolution>");
         sb.append(getUnivSolution());
         sb.append("</univsolution>\n");
         sb.append("<enddate>");
         sb.append(getEndDate());
         sb.append("</enddate>\n");
         sb.append("<coursepath>");
         sb.append(getCommaSeparatedChapterPath());
         sb.append("</coursepath>\n");
         for (int i = 0; i < tests.size(); i++)
         {
            sb.append("<test>\n");
            sb.append("<time>");
            sb.append(tests.get(i).getTestTime());
            sb.append("</time>\n");
            sb.append("<id>");
            sb.append(tests.get(i).getTestId());
            sb.append("</id>\n");
            sb.append("<noofquestions>");
            sb.append(tests.get(i).getNoOfQuestions());
            sb.append("</noofquestions>\n");
            sb.append("<name>");
            sb.append(tests.get(i).getTestName());
            sb.append("</name>\n</test>\n");
         }
         sb.append("</coursedetails>");
         fos.write(sb.toString().getBytes());
      }
      finally
      {
         if (fos != null)
         {
            try
            {
               fos.close();
            }
            catch (IOException e)
            {

            }
         }
      }
   }

   public void fetchAllMemberChapterDetails(String dbConnName, Logger logger, String chapterDetailFileName, String questionBankFileName) throws SQLException, Exception
   {
      ArrayList<String> chapters = new ArrayList<String>();
      chapters.add(getDefChapter());
      chapters.add(getLmr());
      chapters.add(getUnivQuestPaper());
      chapters.add(getUnivSolution());
      for (int i = 0; i < getChapterPath().length; i++)
      {
         chapters.add(getChapterPath()[i]);
      }
      Connection conn = null;
      PreparedStatement ps = null;
      ResultSet rs = null;
      ChapterDetails chapterData = null;
      ArrayList<String> contentDetails = null;
      ArrayList<String> contentShowFlag = null;
      ArrayList<TestQuestion> testQuestions = null;
      ArrayList<TestDetails> tests = null;
      boolean isChapter = false;
      for (int i = 0; i < chapters.size(); i++)
      {
         isChapter = false;
         try
         {
            conn = DbConnectionManager.getConnectionByName(dbConnName);
            ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchChapterLearningPathDetails");
            ps.setString(1, chapters.get(i));
            rs = ps.executeQuery();
            while (rs.next())
            {
               isChapter = true;
               chapterData = new ChapterDetails();
               contentDetails = new ArrayList<String>();
               contentShowFlag = new ArrayList<String>();
               chapterData.setChapterId(rs.getString("chapterid"));
               chapterData.setName(rs.getString("chaptername"));
               contentDetails.add(rs.getString("contentid"));
               contentShowFlag.add(rs.getString("contentshowflag"));
            }
            if (!isChapter)
            {
               continue;
            }
            chapterData.setLearningPath(new String[contentDetails.size()]);
            chapterData.setShowFlags(new String[contentDetails.size()]);
            for (int j = 0; j < contentDetails.size(); j++)
            {
               chapterData.getLearningPath()[j] = contentDetails.get(j);
               chapterData.getShowFlag()[j] = contentShowFlag.get(j);
            }
            testQuestions = chapterData.populateQuestionBankFromDb(dbConnName, logger, getCourseId());
            tests = chapterData.populateTestsFromDb(dbConnName, logger, getCourseId());
            chapterData.writeToFile(courseDirName, chapterDetailFileName, tests);
            if (testQuestions.size() > 0)
            {
               writeQuestions(getCourseDirName() + File.separator + chapterData.getChapterId(), questionBankFileName, testQuestions);
            }
         }
         finally
         {
            if (rs != null)
            {
               try
               {
                  rs.close();
               }
               catch (Exception e)
               {
               }
            }
            DbConnectionManager.releaseConnection(dbConnName, conn);
         }
      }
   }

   public void writeQuestions(String courseDirName, String questionBankFileName, ArrayList<TestQuestion> testQuestions) throws IOException
   {
      File file = new File(courseDirName + File.separator + questionBankFileName);
      if (!file.exists())
      {
         file.createNewFile();
      }
      FileOutputStream fos = new FileOutputStream(file);
      fos.write("<questionbank>\n".getBytes());
      for (int i = 0; i < testQuestions.size(); i++)
      {
         testQuestions.get(i).writeToFile(fos);
      }
      fos.write("</questionbank>\n".getBytes());
   }

   public ArrayList<TestQuestion> populateQuestionBankFromDb(String dbConnName, Logger logger) throws SQLException, Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      ArrayList<TestQuestion> testQuestion = new ArrayList<TestQuestion>();
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchCourseQuestionBank");
         ps.setString(1, getCourseId());
         rs = ps.executeQuery();
         QuestionContent content = null;
         while (rs.next())
         {
            int questionType = rs.getInt("questiontype");
            if (questionType == TestQuestionData.MCQ_QUESTION_TYPE)
            {
               MCQData data = new MCQData();
               data.setQuestionid(rs.getString("questionid"));
               content = new QuestionContent();
               content.setType(rs.getInt("questioncontenttype"));
               if (content.getType() == QuestionContent.STRING_TYPE)
               {
                  content.setContentString(rs.getString("questionstring"));
               }
               else
               {
                  content.setContentLoc(rs.getString("questioncontent"));
               }
               data.setQuestion(content);

               for (int i = 1; i < 7; i++)
               {
                  content = new QuestionContent();
                  content.setType(rs.getInt("ans" + i + "type"));
                  if (content.getType() == QuestionContent.STRING_TYPE)
                  {
                     content.setContentString(rs.getString("ans" + i + "string"));
                     data.addAnswers(content);
                  }
                  else if (content.getType() == QuestionContent.CONTENT_TYPE)
                  {
                     content.setContentLoc(rs.getString("ans" + i + "content"));
                     data.addAnswers(content);
                  }
               }
               data.setDifficultyLevel(rs.getInt("difficultylevel"));
               data.setExplanation(new QuestionContent());
               data.getExplanation().setType(rs.getInt("explanationtype"));
               data.getExplanation().setContentString(rs.getString("explanation"));
               data.getExplanation().setContentLoc(rs.getString("explanationcontent"));
               data.setCorrectAnswer(rs.getString("correctans"));
               testQuestion.add(data);
            }
            else if (questionType == TestQuestionData.MTF_QUESTION_TYPE)
            {
               MTFData data = new MTFData();
               data.setQuestionid(rs.getString("questionid"));
               content = new QuestionContent();
               content.setType(rs.getInt("questioncontenttype"));
               String questionString = rs.getString("questionstring");
               String[] questions = StringSplitter.splitString(questionString, ";");
               if (content.getType() == QuestionContent.STRING_TYPE)
               {
                  content.setContentString(questions[0]);
               }
               else
               {
                  content.setContentLoc(questions[0]);
               }
               data.setQuestion(content);
               int type = Integer.parseInt(questions[1]);
               for (int i = 2; i < questions.length; i++)
               {
                  content = new QuestionContent();
                  content.setType(type);
                  if (type == QuestionContent.STRING_TYPE)
                  {
                     content.setContentString(questions[i]);
                  }
                  else
                  {
                     content.setContentLoc(questions[i]);
                  }
                  data.addQuestions(content);
               }

               for (int i = 1; i < 7; i++)
               {
                  content = new QuestionContent();
                  content.setType(rs.getInt("ans" + i + "type"));
                  if (content.getType() == QuestionContent.STRING_TYPE)
                  {
                     content.setContentString(rs.getString("ans" + i + "string"));
                     data.addAnswers(content);
                  }
                  else if (content.getType() == QuestionContent.CONTENT_TYPE)
                  {
                     content.setContentLoc(rs.getString("ans" + i + "content"));
                     data.addAnswers(content);
                  }
               }
               data.setDifficultyLevel(rs.getInt("difficultylevel"));
               data.setExplanation(new QuestionContent());
               data.getExplanation().setType(rs.getInt("explanationtype"));
               data.getExplanation().setContentString(rs.getString("explanation"));
               data.getExplanation().setContentLoc(rs.getString("explanationcontent"));
               data.setCorrectAnswer(rs.getString("correctans"));
               testQuestion.add(data);
            }
            else
            {
               FIBData data = new FIBData();
               data.setQuestionid(rs.getString("questionid"));
               content = new QuestionContent();
               content.setType(rs.getInt("questioncontenttype"));
               if (content.getType() == QuestionContent.STRING_TYPE)
               {
                  content.setContentString(rs.getString("questionstring"));
               }
               else
               {
                  content.setContentLoc(rs.getString("questioncontent"));
               }
               data.setQuestion(content);

               for (int i = 1; i < 7; i++)
               {
                  content = new QuestionContent();
                  content.setType(rs.getInt("ans" + i + "type"));
                  if (content.getType() == QuestionContent.STRING_TYPE)
                  {
                     content.setContentString(rs.getString("ans" + i + "string"));
                     data.addAnswers(content);
                  }
                  else if (content.getType() == QuestionContent.CONTENT_TYPE)
                  {
                     content.setContentLoc(rs.getString("ans" + i + "content"));
                     data.addAnswers(content);
                  }
               }
               data.setDifficultyLevel(rs.getInt("difficultylevel"));
               data.setExplanation(new QuestionContent());
               data.getExplanation().setType(rs.getInt("explanationtype"));
               data.getExplanation().setContentString(rs.getString("explanation"));
               data.getExplanation().setContentLoc(rs.getString("explanationcontent"));
               testQuestion.add(data);
            }
         }
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
      return testQuestion;
   }

   public void populateChaptersFromDb(String dbConnName, Logger logger) throws SQLException, Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchCoursePath");
         ps.setString(1, getCourseId());
         rs = ps.executeQuery();
         ArrayList<String> chapterList = new ArrayList<String>();
         String chapterId = null;
         while (rs.next())
         {
            chapterId = rs.getString("chapterid");
            if (chapterId == null)
            {
               chapterId = rs.getString("testid");
            }
            chapterList.add(chapterId);
         }
         this.chapterPath = new String[chapterList.size()];
         for (int i = 0; i < chapterList.size(); i++)
         {
            chapterPath[i] = chapterList.get(i);
         }
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   public String getCourseDirName()
   {
      return courseDirName;
   }

   public void setCourseDirName(String courseDirName)
   {
      this.courseDirName = courseDirName;
   }

   public ArrayList<TestDetails> populateTestsFromDb(String dbConnName, Logger logger) throws SQLException, Exception
   {
      ArrayList<TestDetails> datas = new ArrayList<TestDetails>();
      TestDetails data = null;
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchCourseTests");
         ps.setString(1, getCourseId());
         rs = ps.executeQuery();
         while (rs.next())
         {
            data = new TestDetails();
            data.setTestId(rs.getString("testid"));
            data.setTestTime(rs.getString("testtime"));
            data.setNoOfQuestions(rs.getInt("numberofquestion"));
            data.setTestName(rs.getString("testname"));
            datas.add(data);
         }
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
      return datas;
   }
}
