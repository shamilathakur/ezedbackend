package com.intelli.ezed.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

import org.sabre.db.DbConnectionManager;
import org.slf4j.Logger;

import com.intelli.ezed.utils.EzedHelper;

public class TestCorrectAnsTracker
{
   private AtomicInteger atomicInt = new AtomicInteger(0);
   private static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

   public void increaseCorrectAnsCount(int count, String actionId, UserProfile userProfile, String dbConnName, Logger logger, String testId, String courseId) throws Exception
   {
      synchronized (this)
      {
         int val = atomicInt.incrementAndGet();
         if (count == val)
         {
            atomicInt.set(0);
            ActionData actionData = MasterActionRecords.fetchRecord(actionId);

            String testName = EzedHelper.fetchTestNameForTestId(dbConnName, testId, logger);
            String courseName = EzedHelper.fetchCourseNameForCourseId(dbConnName, courseId, logger);

            StringBuffer sb = new StringBuffer();
            sb.append(userProfile.getName());
            sb.append("answered ");
            sb.append(count);
            sb.append(" questions correctly in a row in ");
            sb.append(testName);
            sb.append(" test in course ");
            sb.append(courseName);
            String fbPostScoreStream = sb.toString();

            Connection conn = null;
            PreparedStatement ps = null;
            try
            {
               conn = DbConnectionManager.getConnectionByName(dbConnName);
               ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdateCoursePoints");
               ps.setInt(1, actionData.getPoints());
               ps.setString(2, userProfile.getUserid());
               ps.setString(3, courseId);
               int rows = ps.executeUpdate();
               if (logger.isDebugEnabled())
               {
                  logger.debug(rows + " rows updated in db while incrementing points for userid " + userProfile.getUserid() + " and course " + courseId);
               }
            }
            catch (SQLException e)
            {
               logger.error("Error while incrementing points for userid " + userProfile.getUserid() + " and course " + courseId, e);
            }
            catch (Exception e)
            {
               logger.error("Error while incrementing points for userid " + userProfile.getUserid() + " and course " + courseId, e);
            }
            finally
            {
               DbConnectionManager.releaseConnection(dbConnName, conn);
            }


            try
            {
               conn = DbConnectionManager.getConnectionByName(dbConnName);
               ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdateUserPoints");
               ps.setInt(1, actionData.getPoints());
               ps.setString(2, userProfile.getUserid());
               int rows = ps.executeUpdate();
               if (logger.isDebugEnabled())
               {
                  logger.debug(rows + " rows updated in db while incrementing points for userid " + userProfile.getUserid());
               }
            }
            catch (SQLException e)
            {
               logger.error("Error while incrementing points for userid " + userProfile.getUserid(), e);
            }
            catch (Exception e)
            {
               logger.error("Error while incrementing points for userid " + userProfile.getUserid(), e);
            }
            finally
            {
               DbConnectionManager.releaseConnection(dbConnName, conn);
            }

            try
            {
               conn = DbConnectionManager.getConnectionByName(dbConnName);
               ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "InsertUserStreamAll");
               ps.setString(1, fbPostScoreStream);
               ps.setString(2, userProfile.getUserid());
               ps.setString(3, sdf.format(new Date()));
               ps.setString(4, courseId);
               ps.setString(5, null);
               ps.setString(6, testId);
               ps.setString(7, actionId);
               int rows = ps.executeUpdate();
               if (logger.isDebugEnabled())
               {
                  logger.debug(rows + " rows inserted in db while inserting stream for user " + userProfile.getUserid());
               }
            }
            catch (Exception e)
            {
               logger.error("Error inserting stream entry for " + userProfile.getUserid(), e);
               logger.error("INSERT FAILED " + fbPostScoreStream);
            }
            finally
            {
               DbConnectionManager.releaseConnection(dbConnName, conn);
            }
         }
      }
   }
}
