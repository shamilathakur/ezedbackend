package com.intelli.ezed.analytics.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.sabre.db.DbConnectionManager;
import org.slf4j.Logger;

import com.intelli.ezed.data.ResponseObject;

public class GroupTestResultData extends ResponseObject
{
   private String groupId;
   private ArrayList<String> testId = null;
   private ArrayList<String> groupUsers = new ArrayList<String>();
   private ArrayList<SingleGroupTestResultData> tests = new ArrayList<SingleGroupTestResultData>();
   private GenericChartData title = new GenericChartData();
   private GenericChartData xAxis = new GenericChartData();
   private GenericChartData yAxis = new GenericChartData();
   private GenericChartData type = new GenericChartData();
   private GenericChartData plotOptions = new GenericChartData();
   private GenericChartData toolTip = new GenericChartData();
   private int maxQuestionCount = 0;

   public void prepareChart() throws Exception
   {
      type.getText().add("type");
      type.getValues().add("column");
      type.prepareResponse();

      title.getText().add("text");
      title.getValues().add("Group Test Report");
      title.prepareResponse();

      toolTip.getText().add("headerFormat");
      toolTip.getValues().add("<span style=\"font-size:10px\">{point.key} correct answers</span><table>");
      toolTip.getText().add("pointFormat");
      toolTip.getValues().add("<tr><td style=\"color:{series.color};padding:0\">{series.name}: </td><td style=\"padding:0\"><b>{point.y:f} student/s</b></td></tr>");
      toolTip.getText().add("footerFormat");
      toolTip.getValues().add("</table>");
      toolTip.getText().add("useHTML");
      toolTip.getValues().add("true");
      toolTip.getText().add("shared");
      toolTip.getValues().add("true");
      toolTip.prepareResponse();

      plotOptions.getText().add("column");
      GenericChartData plotColumnData = new GenericChartData();
      plotColumnData.getText().add("pointPadding");
      plotColumnData.getValues().add(0.2);
      plotColumnData.getText().add("borderWidth");
      plotColumnData.getValues().add(0);
      plotColumnData.prepareResponse();
      plotOptions.getValues().add(plotColumnData);
      plotOptions.prepareResponse();

      //      xAxis.getText().add("categories");
      //      String[] cats = {"Current Period", "Previous Period"};
      //      xAxis.getValues().add(cats);
      xAxis.getText().add("title");
      GenericChartData xAxisTitleData = new GenericChartData();
      xAxisTitleData.getText().add("text");
      xAxisTitleData.getValues().add("No. of Correct Answers");
      xAxisTitleData.prepareResponse();
      xAxis.getValues().add(xAxisTitleData);
      //      xAxis.prepareResponse();

      yAxis.getText().add("min");
      yAxis.getValues().add(0);
      yAxis.getText().add("title");
      GenericChartData yAxisText = new GenericChartData();
      yAxisText.getText().add("text");
      yAxisText.getValues().add("No. of users");
      //      yAxisText.getText().add("align");
      //      yAxisText.getValues().add("high");
      yAxis.getValues().add(yAxisText);
      //      yAxis.getText().add("labels");
      //      GenericChartData yAxisLabels = new GenericChartData();
      //      yAxisLabels.getText().add("overflow");
      //      yAxisLabels.getValues().add("justify");
      //      yAxis.getValues().add(yAxisLabels);

      //      yAxisLabels.prepareResponse();
      yAxisText.prepareResponse();
      yAxis.prepareResponse();
   }

   public void handleReportFetch(String dbConnName, Logger logger) throws SQLException, Exception
   {
      this.fetchGroupUsers(dbConnName, logger);
      if (getTestId() == null)
      {
         this.fetchGroupTests(dbConnName, logger);
      }
      SingleGroupTestResultData singleTestData = null;
      int maxQuestions = 0;
      for (int i = 0; i < testId.size(); i++)
      {
         singleTestData = new SingleGroupTestResultData();
         singleTestData.setTestId(getTestId().get(i));
         tests.add(singleTestData);
         singleTestData.fetchTestName(dbConnName, logger);
         maxQuestions = singleTestData.fetchUserTestResult(dbConnName, logger, groupUsers);
         if (maxQuestionCount < maxQuestions)
         {
            maxQuestionCount = maxQuestions;
         }
      }
   }

   private void fetchGroupTests(String dbConnName, Logger logger) throws SQLException, Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchGroupLevelTest");
         ps.setString(1, getGroupId());
         rs = ps.executeQuery();
         testId = new ArrayList<String>();
         while (rs.next())
         {
            testId.add(rs.getString("testid"));
         }
         if (testId.size() == 0)
         {
            throw new Exception("No tests found for group");
         }
      }
      catch (Exception e)
      {
         throw e;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   private void fetchGroupUsers(String dbConnName, Logger logger) throws SQLException, Exception
   {
      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchGroupUserList");
         ps.setString(1, getGroupId());
         rs = ps.executeQuery();
         while (rs.next())
         {
            groupUsers.add(rs.getString("userid"));
         }
      }
      catch (Exception e)
      {
         throw e;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (Exception e)
            {
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }

   @Override
   public void prepareResponse() throws Exception
   {
      xAxis.getText().add("categories");
      String[] cats = new String[maxQuestionCount + 1];
      for (int i = 0; i < maxQuestionCount; i++)
      {
         cats[i] = Integer.toString(i);
      }
      cats[cats.length - 1] = "Not Attempted";

      xAxis.getValues().add(cats);
      xAxis.prepareResponse();
      this.put("chart", type);
      this.put("title", title);
      this.put("xAxis", xAxis);
      this.put("yAxis", yAxis);
      this.put("tooltip", toolTip);
      this.put("plotOptions", plotOptions);

      //      GenericChartData singleSeriesData = new GenericChartData();
      //      singleSeriesData.getText().add("name");
      //      singleSeriesData.getValues().add(getTestId());
      //      singleSeriesData.getText().add("data");
      //      singleSeriesData.getValues().add(testResult);
      //      singleSeriesData.prepareResponse();

      JSONArray jsonArray = new JSONArray();
      for (int i = 0; i < tests.size(); i++)
      {
         tests.get(i).setMaxQuestions(maxQuestionCount);
         tests.get(i).prepareResponse();
         jsonArray.put(tests.get(i));
      }
      this.put("series", jsonArray);
   }

   public String getGroupId()
   {
      return groupId;
   }

   public void setGroupId(String groupId)
   {
      this.groupId = groupId;
   }

   public ArrayList<String> getTestId()
   {
      return testId;
   }

   public void setTestId(ArrayList<String> testId)
   {
      this.testId = testId;
   }
}
