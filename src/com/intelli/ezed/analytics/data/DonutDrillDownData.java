package com.intelli.ezed.analytics.data;

import com.intelli.ezed.data.ResponseObject;

public class DonutDrillDownData extends ResponseObject
{
   private String name;
   private String[] categories;
   private float[] data;
   private String[] colors;
   
   public String getName()
   {
      return name;
   }
   public void setName(String name)
   {
      this.name = name;
   }
   public String[] getCategories()
   {
      return categories;
   }
   public void setCategories(String[] categories)
   {
      this.categories = categories;
   }
   public float[] getData()
   {
      return data;
   }
   public void setData(float[] data)
   {
      this.data = data;
   }
   public String[] getColors()
   {
      return colors;
   }
   public void setColors(String[] colors)
   {
      this.colors = colors;
   }
   
   @Override
   public void prepareResponse() throws Exception
   {
      this.put("name", getName());
      this.put("categories" , getCategories());
      this.put("data", getData());
      this.put("color", getColors());
   }
}
