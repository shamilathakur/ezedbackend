package com.intelli.ezed.data;

public class EnrolledCourseChapter {
	private String chapterId;
	private String chapterName;
	private int sequenceNo;
	private String testId;
	private String testName;
	private String questionPaperId;
	private String solvedPaperId;
	private String lmr;
	private String lmrName;
	private String questionPaperName;
	private String solvedPaperName;
	
	/**
	 * @return the chapterId
	 */
	public String getChapterId() {
		return chapterId;
	}

	/**
	 * @param chapterId the chapterId to set
	 */
	public void setChapterId(String chapterId) {
		this.chapterId = chapterId;
	}

	/**
	 * @return the chapterName
	 */
	public String getChapterName() {
		return chapterName;
	}

	/**
	 * @param chapterName the chapterName to set
	 */
	public void setChapterName(String chapterName) {
		this.chapterName = chapterName;
	}

	/**
	 * @return the sequenceNo
	 */
	public int getSequenceNo() {
		return sequenceNo;
	}

	/**
	 * @param sequenceNo the sequenceNo to set
	 */
	public void setSequenceNo(int sequenceNo) {
		this.sequenceNo = sequenceNo;
	}

	/**
	 * @return the testId
	 */
	public String getTestId() {
		return testId;
	}

	/**
	 * @param testId the testId to set
	 */
	public void setTestId(String testId) {
		this.testId = testId;
	}

	/**
	 * @return the testName
	 */
	public String getTestName() {
		return testName;
	}

	/**
	 * @param testName the testName to set
	 */
	public void setTestName(String testName) {
		this.testName = testName;
	}

	/**
	 * @return the questionPaperId
	 */
	public String getQuestionPaperId() {
		return questionPaperId;
	}

	/**
	 * @param questionPaperId the questionPaperId to set
	 */
	public void setQuestionPaperId(String questionPaperId) {
		this.questionPaperId = questionPaperId;
	}

	/**
	 * @return the answerPaperId
	 */
	public String getSolvedPaperId() {
		return solvedPaperId;
	}

	/**
	 * @param answerPaperId the answerPaperId to set
	 */
	public void setSolvedPaperId(String solvedPaperId) {
		this.solvedPaperId = solvedPaperId;
	}

	/**
	 * @return the lmr
	 */
	public String getLmr() {
		return lmr;
	}

	/**
	 * @param lmr the lmr to set
	 */
	public void setLmr(String lmr) {
		this.lmr = lmr;
	}

	/**
	 * @return the lmrName
	 */
	public String getLmrName() {
		return lmrName;
	}

	/**
	 * @param lmrName the lmrName to set
	 */
	public void setLmrName(String lmrName) {
		this.lmrName = lmrName;
	}

	/**
	 * @return the questionPaperName
	 */
	public String getQuestionPaperName() {
		return questionPaperName;
	}

	/**
	 * @param questionPaperName the questionPaperName to set
	 */
	public void setQuestionPaperName(String questionPaperName) {
		this.questionPaperName = questionPaperName;
	}

	/**
	 * @return the solvedPaperName
	 */
	public String getSolvedPaperName() {
		return solvedPaperName;
	}

	/**
	 * @param solvedPaperName the solvedPaperName to set
	 */
	public void setSolvedPaperName(String solvedPaperName) {
		this.solvedPaperName = solvedPaperName;
	}

}
