package com.intelli.ezed.data;

public class PaymentData
{
   public static final String CART_ADDED = "0";
   public static final String CART_PAID = "1";
   public static final String CART_PAYMENT_FAILED_TIMEOUT = "2";
   public static final String CART_ADDED_TO_USER = "3";

   private String paymentId;
   private int paymentStatus;
   private String merchantRef;
   private String appId;

   public String getAppId()
   {
      return appId;
   }
   public void setAppId(String appId)
   {
      this.appId = appId;
   }
   public String getPaymentId()
   {
      return paymentId;
   }
   public void setPaymentId(String paymentId)
   {
      this.paymentId = paymentId;
   }
   public int getPaymentStatus()
   {
      return paymentStatus;
   }
   public void setPaymentStatus(int paymentStatus)
   {
      this.paymentStatus = paymentStatus;
   }
   public String getMerchantRef()
   {
      return merchantRef;
   }
   public void setMerchantRef(String merchantRef)
   {
      this.merchantRef = merchantRef;
   }
}
