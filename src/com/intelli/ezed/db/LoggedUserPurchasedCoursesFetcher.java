package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.FrontendFetchData;
import com.intelli.ezed.data.LoggedUserPurchasedCourseData;
import com.intelli.ezed.data.QuizRequestResponse;
import com.intelli.ezed.data.Response;


public class LoggedUserPurchasedCoursesFetcher extends SingletonRule{

	private String dbConnName;
	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
	
	 public LoggedUserPurchasedCoursesFetcher(String ruleName, String dbConnName)
	   {
	      super(ruleName);
	      this.dbConnName = dbConnName;
	   }

	 @Override
	   public RuleDecisionKey executeRule(FlowContext flowContext)
	   {
	      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
	      //LoggedUserPurchasedCourseData courseDataResponse = (LoggedUserPurchasedCourseData) response.getResponseObject();
	      
	      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
	      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
	      String email = messageData.getParameter("email");	
	      PreparedStatement ps = null;
	      ResultSet rs = null;
	      String[] loggedUserCourseArr = null;
	      FrontendFetchData loggedUserCourseData = new FrontendFetchData();
	      response.setResponseObject(loggedUserCourseData);
	      response.setStatusCodeSend(false);
	      loggedUserCourseData.setAttribName("aaData");
	      ArrayList<String[]> courses = new ArrayList<String[]>();
	      try
	      {
	         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchLoggedUserPurchasedCourses");
	         ps.setString(1, email);
	         rs = ps.executeQuery();
	         int i = 1;
	         while (rs.next())
	         {
	        	 loggedUserCourseArr = new String[6];
	        	 loggedUserCourseArr[0] = rs.getString("courseid");
	        	 loggedUserCourseArr[1] = rs.getString("coursename");
	        	 loggedUserCourseArr[2] = rs.getString("courseimage");
	        	 loggedUserCourseArr[3] = Integer.toString(rs.getInt("videocount"));
	        	 loggedUserCourseArr[4] = Integer.toString(rs.getInt("chaptercount"));
	        	 loggedUserCourseArr[5] = Integer.toString(rs.getInt("questioncount"));
	        	 i++;
	        	 courses.add(loggedUserCourseArr);
	         }
	         loggedUserCourseData.setAttribList(courses);
	         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
	      }
	      catch (SQLException e)
	      {
	         logger.error("DB error while fetching list of courses for logged user", e);
	         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "DB error while fetching list of courses for logged user");
	         return RuleDecisionKey.ON_FAILURE;
	      }
	      catch (Exception e)
	      {
	         logger.error("Error while fetching list of courses for logged user", e);
	         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching list of courses for logged user");
	         return RuleDecisionKey.ON_FAILURE;
	      }
	      finally
	      {
	         if (rs != null)
	         {
	            try
	            {
	               rs.close();
	            }
	            catch (SQLException e)
	            {
	               logger.error("Error while closing result set while fetching list of courses for logged user", e);
	            }
	         }
	         DbConnectionManager.releaseConnection(dbConnName, conn);
	      }
	      return RuleDecisionKey.ON_SUCCESS;
	   }

	
}
