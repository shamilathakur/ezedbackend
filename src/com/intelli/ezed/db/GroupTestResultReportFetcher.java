package com.intelli.ezed.db;

import java.sql.SQLException;
import java.util.ArrayList;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.a.helper.utils.StringSplitter;
import com.intelli.ezed.analytics.data.GroupTestResultData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class GroupTestResultReportFetcher extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public GroupTestResultReportFetcher(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      //      String userId = (String) flowContext.getFromContext(EzedKeyConstants.USER_ID);
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());

      String groupId = messageData.getParameter("groupid");
      if (groupId == null || groupId.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Group id in request is null or blank");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid group id");
         return RuleDecisionKey.ON_FAILURE;
      }

      GroupTestResultData data = new GroupTestResultData();
      data.setGroupId(groupId);

      String testId = messageData.getParameter("testid");
      if (testId == null || testId.trim().compareTo("") == 0 || testId.trim().compareToIgnoreCase("null") == 0)
      {
         testId = null;
      }

      if (testId != null)
      {
         String[] testIdList = StringSplitter.splitString(testId, ",");
         ArrayList<String> testIdArray = new ArrayList<String>(testIdList.length);
         for (int i = 0; i < testIdList.length; i++)
         {
            testIdArray.add(testIdList[i]);
         }
         data.setTestId(testIdArray);
      }


      try
      {
         data.handleReportFetch(dbConnName, logger);
         data.prepareChart();
         response.setResponseObject(data);
         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
         response.setStatusCodeSend(false);
         return RuleDecisionKey.ON_SUCCESS;
      }
      catch (SQLException e)
      {
         logger.error("Error while creating group test report", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while creating group test report");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while creating group test report", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while creating group test report");
         return RuleDecisionKey.ON_FAILURE;
      }
   }
}
