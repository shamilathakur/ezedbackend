package com.intelli.ezed.checks;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.db.EzedRequestData;

public class LoginDataChecker extends SingletonRule
{

   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public LoginDataChecker(String ruleName)
   {
      super(ruleName);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {


	   MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      String userid = messageData.getParameter("email");
      String pwd = messageData.getParameter("password");
      String type = messageData.getParameter("type");
      String loginWith = messageData.getParameter("loginwith");

      if (userid == null || userid.equals("") || userid.equals(" "))
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Email address not provided.");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Please enter correct email id.");
         return RuleDecisionKey.ON_FAILURE;
      }


      if (type == null || type.equals("") || type.equals(" "))
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Type not provided.");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Please enter type.");
         return RuleDecisionKey.ON_FAILURE;
      }

      int loginType = Integer.parseInt(type);

      if (loginType == 0)
      {
         if (pwd == null || pwd.equals("") || pwd.equals(" "))
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Email password not provided.");
            }
            response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Please enter password id.");
            return RuleDecisionKey.ON_FAILURE;
         }
      }

      if (loginType == 1)
      {
         if (loginWith == null || loginWith.equals("") || loginWith.equals(" "))
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("loginWith site name not provided.");
            }
            response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Please enter name of site.");
            return RuleDecisionKey.ON_FAILURE;
         }
      }

      //Creating this new Data object so that we can keep incoming request data in this object and compare it with the fetched userprofile data
      EzedRequestData incomingRequestData = new EzedRequestData();
      incomingRequestData.setUserid(userid);
      incomingRequestData.setPassword(pwd);
      incomingRequestData.setLoginWith(loginWith);
      incomingRequestData.setRegLoginType(loginType);
      flowContext.putIntoContext(EzedRequestData.EZED_REQUEST_DATA, incomingRequestData);
      //Setting userid so that we can fetch user profile later
      flowContext.putIntoContext(EzedKeyConstants.USER_ID, userid);

      if (logger.isDebugEnabled())
      {
         logger.debug("Entered email id is : " + userid + " and password is : " + pwd);
      }
      if (loginType == 1)
      {
         response.setParameters(EzedTransactionCodes.SUCCESS, "Userid validated.");
         return RuleDecisionKey.ON_PARTIAL;
      }
      return RuleDecisionKey.ON_SUCCESS;
   }

}
