package com.intelli.ezed.dropdowns;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import bsh.org.objectweb.asm.Constants;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.dropdowns.data.DropDownListData;
import com.intelli.ezed.dropdowns.data.SingleDropDownData;
import com.intelli.ezed.utils.Globals;


public class SemesterListForUniversityM extends SingletonRule{
	
	private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
	private String dbConnName;

	public SemesterListForUniversityM(String ruleName, String dbConnName)
	   {
	      super(ruleName);
	      this.dbConnName = dbConnName;
	   }
	
	@Override
	   public RuleDecisionKey executeRule(FlowContext flowContext)
	   {
		  MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
	      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
	      
	      String univname = messageData.getParameter("univname");
	      if (univname == null || univname.trim().compareTo("") == 0)
	      {
	         if (logger.isDebugEnabled())
	         {
	            logger.debug("University name is invalid in request " + univname);
	         }
	         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "University name is invalid");
	         return RuleDecisionKey.ON_FAILURE;
	      }
	      
	      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
	      
	      PreparedStatement ps = null;
	      ResultSet rs = null;
	      DropDownListData dropDownListData = new DropDownListData();
	      dropDownListData.setName("SemesterIdMapList");
	      try
	      {
	         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "GetSemesterListForUniversity");
	         ps.setString(1,univname);
	         rs = ps.executeQuery();
	         SingleDropDownData data = null;
	         while (rs.next())
	         {
	            data = new SingleDropDownData();
	            data.setParam(rs.getString("semester"));
	            data.setValue(rs.getString("semester"));
	            dropDownListData.getDropDownDatas().add(data);
	         }
	         response.setStatusCodeSend(false);
	         response.setResponseObject(dropDownListData);
	         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
	         return RuleDecisionKey.ON_SUCCESS;
	      }
	      catch (SQLException e)
	      {
	         logger.error("Error while fetching semester for universities drop downs", e);
	         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while populating semester list drop down");
	         return RuleDecisionKey.ON_FAILURE;
	      }
	      catch (Exception e)
	      {
	         logger.error("Error while fetching semester drop downs", e);
	         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while populating semester list drop down");
	         return RuleDecisionKey.ON_FAILURE;
	      }
	      finally
	      {
	         if (rs != null)
	         {
	            try
	            {
	               rs.close();
	            }
	            catch (SQLException e)
	            {
	               logger.error("Error while closing result set while populating drop down for semester");
	            }
	         }
	         DbConnectionManager.releaseConnection(dbConnName, conn);
	      }
	   }

	
}
