package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.a.helper.utils.StringSplitter;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.utils.EzedHelper;

public class CollegeUpdater extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public CollegeUpdater(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      response.setStringOnlySend(true);

      String newCollegeValue = messageData.getParameter("value");
      if (newCollegeValue == null || newCollegeValue.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("New college value is invalid " + newCollegeValue);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "");
         return RuleDecisionKey.ON_FAILURE;
      }
      int colNo = 0;
      try
      {
         colNo = Integer.parseInt(messageData.getParameter("columnid"));
      }
      catch (Exception e)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Column no is invalid in request", e);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "");
         return RuleDecisionKey.ON_FAILURE;
      }

      String origRowData = messageData.getParameter("rowdata");
      if (origRowData == null || origRowData.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Orig row data is invalid " + origRowData);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "");
         return RuleDecisionKey.ON_FAILURE;
      }

      String[] origRowStrings = StringSplitter.splitString(origRowData, "~~");
      if (origRowStrings.length < 5)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Orig row data is invalid since its length is not proper " + origRowData);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "");
         return RuleDecisionKey.ON_FAILURE;
      }

      String collegeId = origRowStrings[1];
      if (collegeId == null || collegeId.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("College id in request is not proper " + collegeId);
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "");
         return RuleDecisionKey.ON_FAILURE;
      }

      if (colNo == 4)
      {
         if (EzedHelper.getTenDigMobNo(newCollegeValue.trim()) == null)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("Ph no to be updated for college is not valid " + newCollegeValue);
            }
            response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "");
            return RuleDecisionKey.ON_FAILURE;
         }
         else
         {
            if (newCollegeValue.startsWith(" ") && newCollegeValue.trim().length() == 12)
            {
               newCollegeValue = newCollegeValue.trim() + "+";
            }
         }
      }

      origRowStrings[colNo] = newCollegeValue;

      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "UpdateCollege");
         ps.setString(1, origRowStrings[1]);
         ps.setString(2, origRowStrings[3]);
         ps.setString(3, origRowStrings[4]);
         ps.setString(4, collegeId);
         int rows = ps.executeUpdate();
         if (rows > 0)
         {
            if (logger.isDebugEnabled())
            {
               logger.debug(rows + " rows updated in db while updating college name for id " + collegeId);
            }
            response.setParameters(EzedTransactionCodes.SUCCESS, newCollegeValue);
            return RuleDecisionKey.ON_SUCCESS;
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("College cannot be updated in request for college id " + collegeId);
            }
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "");
            return RuleDecisionKey.ON_FAILURE;
         }
      }
      catch (SQLException e)
      {
         logger.error("DB error while updating college name for id " + collegeId, e);
         if (e.getMessage().toUpperCase().contains("WEBSITE"))
         {
            response.setParameters(EzedTransactionCodes.WEBSITE_NAME_REPEATED, "");
         }
         else if (e.getMessage().toUpperCase().contains("PHONE"))
         {
            response.setParameters(EzedTransactionCodes.PH_NO_REPEATED, "");
         }
         else
         {
            response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "");
         }
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while updating college name for id " + collegeId, e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
}
