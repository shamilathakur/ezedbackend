package com.intelli.ezed.checks;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.db.EzedRequestData;

public class FetchDefChapterRequestDataChecker extends SingletonRule
{

   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public FetchDefChapterRequestDataChecker(String ruleName)
   {
      super(ruleName);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      String email = messageData.getParameter("email");
      String chapterId = messageData.getParameter("courseid");

      if (email == null || email.equals("") || email.equals(" "))
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Email address not provided.");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Please enter correct email id.");
         return RuleDecisionKey.ON_FAILURE;
      }

      if (chapterId == null || chapterId.equals("") || chapterId.equals(" "))
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("course id not provided.");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Please enter course id.");
         return RuleDecisionKey.ON_FAILURE;
      }

      EzedRequestData requestData = new EzedRequestData();
      requestData.setUserid(email);
      requestData.setChapterId(chapterId);
      flowContext.putIntoContext(EzedRequestData.EZED_REQUEST_DATA, requestData);

      if (logger.isDebugEnabled())
      {
         logger.debug("Entered email id is : " + email);
      }
      return RuleDecisionKey.ON_SUCCESS;
   }
}
