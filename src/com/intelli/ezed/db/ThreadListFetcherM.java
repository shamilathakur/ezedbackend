package com.intelli.ezed.db;

/*
 * Class used for fetching thread list in the user profile. Used for mobile implementation.
 */

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.json.JSONArray;
import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.analytics.data.GenericChartData;
import com.intelli.ezed.data.SingleGroupData;
import com.intelli.ezed.data.ThreadData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.FrontendFetchData;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.utils.Globals;

public class ThreadListFetcherM extends SingletonRule{

	private String dbConnName;
	
    private Logger log = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);
	
	public ThreadListFetcherM(String ruleName, String dbConnName)
	   {
	      super(ruleName);
	      this.dbConnName = dbConnName;
	   }

	   @Override
	   public RuleDecisionKey executeRule(FlowContext flowContext)
	   {
		  Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
	      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());
		  String sqlst = null;
		  ResultSet resultSet=null;
		  Connection connection=null;
		  connection = DbConnectionManager.getConnectionByName(dbConnName);
		  PreparedStatement preparedStatement=null;
		  ThreadData singleThreadData = null;
	      ArrayList<ThreadData> threads = new ArrayList<ThreadData>();
	      try{
		        String courseId = messageData.getParameter("courseId");
		        if (courseId == null || courseId.trim().compareTo("") == 0 || courseId.trim().compareToIgnoreCase("default") == 0)
		        {
		        	if (log.isDebugEnabled()) {
						log.debug("Course id received in request is invalid");
					}
					response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER,"Invalid course id");
					return RuleDecisionKey.ON_FAILURE;
		        }

		        String chapterId = messageData.getParameter("chapterId");
		        if (chapterId == null || chapterId.trim().compareTo("") == 0 || chapterId.trim().compareToIgnoreCase("default") == 0)
		        {
		        	chapterId = null;
		        }
		        
		        String contentId = messageData.getParameter("contentId");
		        if (contentId == null || contentId.trim().compareTo("") == 0 || contentId.trim().compareToIgnoreCase("default") == 0)
		        {
		        	contentId = null;
		        }
		        
		        if ((contentId!=null) && (chapterId!=null)) {
		        	sqlst = "select threadid,threadtitle,threaddescription,createtime,level,status,postcount from forumthread where status=\'1\' AND courseid=? AND chapterid=? AND contentid=? order by createtime DESC";
		        	preparedStatement = connection.prepareStatement(sqlst);
					preparedStatement.setString(1, courseId);
					preparedStatement.setString(2, chapterId);
					preparedStatement.setString(3, contentId);

		        }
		        else if ((chapterId!=null) && (contentId==null)) {
		        	sqlst = "select threadid,threadtitle,threaddescription,createtime,level,status,postcount from forumthread where status=\'1\' AND courseid=? AND chapterid=? AND contentid is null order by createtime DESC";
		        	preparedStatement = connection.prepareStatement(sqlst);
					preparedStatement.setString(1, courseId);
					preparedStatement.setString(2, chapterId);

		        }
		        else if ((contentId!=null) && (chapterId==null)) {
		        	sqlst = "select threadid,threadtitle,threaddescription,createtime,level,status,postcount from forumthread where status=\'1\' AND courseid=? AND chapterid is null AND contentid=? order by createtime DESC";
		        	preparedStatement = connection.prepareStatement(sqlst);
					preparedStatement.setString(1, courseId);
					preparedStatement.setString(2, contentId);

		        }
		        else {
					sqlst = "select threadid,threadtitle,threaddescription,createtime,level,status,postcount from forumthread where courseid=? AND status=\'1\' order by createtime DESC";
					preparedStatement = connection.prepareStatement(sqlst);
					preparedStatement.setString(1, courseId);
				}
		        
		        log.info("courseid : " + courseId);
				log.info("chapterid : " + chapterId);
				log.info("contentid : " + contentId);

				log.info("sqlst for getting forumthread  : " + sqlst);
		        
		        ThreadData editBean=null;
				resultSet = preparedStatement.executeQuery();
				String status = null;

				SimpleDateFormat dbOutFormat=new SimpleDateFormat(Globals.DB_IN_OUT_DATE_FORMAT);
				SimpleDateFormat displyFormat=new SimpleDateFormat(Globals.DISPLAY_DATE_FORMAT);
				
				if (resultSet != null) {
					while (resultSet.next()) 
					{
						singleThreadData= new ThreadData();
						singleThreadData.setThreadId(resultSet.getString(1));
						singleThreadData.setThreadTitle(resultSet.getString(2));
						singleThreadData.setThreadDesc(resultSet.getString(3));
						String temp="";
						String createTime="";
						if(resultSet.getString(4)!=null)
						{
							temp=resultSet.getString(4);
							try{
								Date date=dbOutFormat.parse(temp);
								createTime=displyFormat.format(date);
							}catch(Exception e)
							{
								
							}
							
							if((createTime)==null)
							{
								createTime="";
							}
							
						}//end of resultSet.getString(4)!=null 	

						singleThreadData.setCreateTime(createTime);	

						singleThreadData.setLevel(resultSet.getString(5));

						status = resultSet.getString(6);

						if (status != null && status.equals("1")) {
							singleThreadData.setStatus("Block");

						} else {
							singleThreadData.setStatus("Unblock");
						}

						singleThreadData.setPostCount(resultSet.getString(7));

						editBean=getEditedThreadData(singleThreadData.getThreadId());
						
						if(editBean!=null)
						{
							if(editBean.getEditedThreadDesc()==null || editBean.getEditedThreadDesc().equals(""))
								{
								singleThreadData.setEditedThreadDesc("");
								}else{
									
									singleThreadData.setEditedThreadDesc(editBean.getEditedThreadDesc());
								}
							
							if(editBean.getModifiedTime()==null || editBean.getModifiedTime().equals(""))
							{
								singleThreadData.setModifiedTime("");
							}else{
								
								singleThreadData.setModifiedTime(editBean.getModifiedTime());
							}
							
						}else{
							
						singleThreadData.setEditedThreadDesc("");
						singleThreadData.setModifiedTime("");
						}
						
						threads.add(singleThreadData);
					}// end of while
					
				}// end of if(resultSet!=null)

		         GenericChartData data = new GenericChartData();
		         JSONArray array = new JSONArray();
		         for (int i = 0; i < threads.size(); i++)
		         {
		        	 threads.get(i).prepareResponse();
		            array.put(threads.get(i));
		         }
		         data.getText().add("threads");
		         data.getValues().add(array);
		         response.setResponseObject(data);
		         response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
		         return RuleDecisionKey.ON_SUCCESS;
				
		    }//end of try
		    catch (SQLException e)
		      {
		         log.error("DB error while fetching list of threads", e);
		         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "DB error while fetching threads");
		         return RuleDecisionKey.ON_FAILURE;
		      }
		      
		      catch(Exception e){
    	         log.error("Error while fetching list of threads", e);
    	         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching threads");
    	         return RuleDecisionKey.ON_FAILURE;
		    }
		    finally
		      {
		         if (resultSet != null)
		         {
		            try
		            {
		            	resultSet.close();
		            }
		            catch (SQLException e)
		            {
		               log.error("Error while closing result set while fetching thread list", e);
		            }
		         }
		         DbConnectionManager.releaseConnection(dbConnName, connection);
		      }
	      
	   }

		public ThreadData getEditedThreadData(String threadId) {
			log.info("In UserDAO getEditedThreadData()");
			String sqlst = null;
			ResultSet resultSet=null;
			Connection connection=null;
			connection = DbConnectionManager.getConnectionByName(dbConnName);
			PreparedStatement preparedStatement=null;
			ThreadData singleThreadData = null;
		    
			SimpleDateFormat dbOutFormat=new SimpleDateFormat(Globals.DB_IN_OUT_DATE_FORMAT);
			SimpleDateFormat displyFormat=new SimpleDateFormat(Globals.DISPLAY_DATE_FORMAT);
					
			connection = DbConnectionManager.getConnectionByName(dbConnName);;

			sqlst = "select editeddesc,createtime from forumthreadhistory where threadid=? order by historyid desc limit 1";
			
			try {
				
				preparedStatement = connection.prepareStatement(sqlst);
				preparedStatement.setString(1, threadId);
				resultSet = preparedStatement.executeQuery();
				String dbDateTime=null;
				String temp="";
				
				while (resultSet.next()) {
					singleThreadData=new ThreadData();
					singleThreadData.setEditedThreadDesc(resultSet.getString(1));
					
					try{
						Date date=dbOutFormat.parse(temp);
						dbDateTime=displyFormat.format(date);
					}catch(Exception e)
					{
						log.error("Error while parsing date !",e);
					}
					
					if(dbDateTime == null || dbDateTime.equals(""))	
					{
						singleThreadData.setModifiedTime("");
					}else{
						singleThreadData.setModifiedTime(dbDateTime);
					}
						
					return singleThreadData;
				}// end of while

			} catch (Exception e) {
				log.error("Error in getEditedThreadData() method of ThreadListFetcherM !", e);
			} finally {
				
				if (resultSet != null)
		         {
		            try
		            {
		            	resultSet.close();
		            }
		            catch (SQLException e)
		            {
		               log.error("Error while closing result set while fetching edited thread list", e);
		            }
		         }
		         DbConnectionManager.releaseConnection(dbConnName, connection);
				
			}// end of finally

			return singleThreadData;

		}// end of getEditedThreadData

	   
}
