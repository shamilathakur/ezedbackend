package com.intelli.ezed.analytics.data;

import com.intelli.ezed.data.ResponseObject;

public class DonutOuterData extends ResponseObject
{
   private float y;
   private String[] colors;
   private DonutDrillDownData drilldown;

   public float getY()
   {
      return y;
   }
   public void setY(float y)
   {
      this.y = y;
   }
   public String[] getColors()
   {
      return colors;
   }
   public void setColors(String[] colors)
   {
      this.colors = colors;
   }
   public DonutDrillDownData getDrilldown()
   {
      return drilldown;
   }
   public void setDrilldown(DonutDrillDownData drilldown)
   {
      this.drilldown = drilldown;
   }

   @Override
   public void prepareResponse() throws Exception
   {
      this.put("y", getY());
      this.put("color", getColors());
      DonutDrillDownData donutDrillDownData = getDrilldown();
      donutDrillDownData.prepareResponse();
      this.put("drilldown", donutDrillDownData);

   }
}
