package com.intelli.ezed.db;

import java.sql.SQLException;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intelli.ezed.data.AdminProfile;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class AdminProfilePersister extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public AdminProfilePersister(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      AdminProfile adminProfile = (AdminProfile) flowContext.getFromContext(EzedKeyConstants.ADMIN_PROFILE);
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);

      try
      {
         int status = adminProfile.commitAdminProfile(dbConnName, logger);
         if (status != EzedTransactionCodes.SUCCESS)
         {
            response.setParameters(status, "Error while persisting admin profile");
            if (logger.isDebugEnabled())
            {
               logger.debug("Error while persisting admin profile " + status);
            }
            return RuleDecisionKey.ON_FAILURE;
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while persisting admin profile in DB", e);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Failed to persist admin profile");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while persist admin profile from DB", e);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Failed to persist admin profile");
         return RuleDecisionKey.ON_FAILURE;
      }
      return RuleDecisionKey.ON_SUCCESS;
   }

}
