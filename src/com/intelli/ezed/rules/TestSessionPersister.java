package com.intelli.ezed.rules;

import java.util.HashSet;
import java.util.Set;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.session.SessionException;
import com.a.helper.session.SessionManager;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;
import com.intelli.ezed.data.TestDetails;
import com.intelli.ezed.data.UserProfile;

public class TestSessionPersister extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public TestSessionPersister(String ruleName)
   {
      super(ruleName);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      SessionManager sessionManager = (SessionManager) flowContext.getFromContext(EzedKeyConstants.TEST_SESSION_MANAGER);
      String sessionId = (String) flowContext.getFromContext(EzedKeyConstants.TEST_SESSION_ID);
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      Set<String> contextKeySet = new HashSet<String>();
      contextKeySet.add(EzedKeyConstants.REQUEST_IN_TIME);
      contextKeySet.add(EzedKeyConstants.USER_DETAILS);
      contextKeySet.add(EzedKeyConstants.TEST_SESSION_ID);
      contextKeySet.add(EzedKeyConstants.CORRECT_ANSWER_TRACKER);


      //adding test object to session data
      contextKeySet.add(TestDetails.TEST_DETAILS);

      UserProfile userProfile = (UserProfile) flowContext.getFromContext(EzedKeyConstants.USER_DETAILS);
      String emailId = userProfile.getUserid();
      if (emailId == null || emailId.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Email id is null or blank. Not persisting session");
         }
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Could not persist session");
         return RuleDecisionKey.ON_FAILURE;
      }
      try
      {
         sessionManager.addDataToSession(emailId, flowContext, contextKeySet, logger);
         if (logger.isDebugEnabled())
         {
            logger.debug("Session persisted successfully for id " + emailId + " and session id " + sessionId);
         }
      }
      catch (SessionException e)
      {
         logger.error("Error while persisting session", e);
         response.setParameters(EzedTransactionCodes.SESSION_ERROR_OCCURRED, "Error while persisting session");
         return RuleDecisionKey.ON_FAILURE;
      }
      return RuleDecisionKey.ON_SUCCESS;
   }
}
