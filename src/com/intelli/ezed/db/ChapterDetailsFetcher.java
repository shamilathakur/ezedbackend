package com.intelli.ezed.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.sabre.db.DbConnectionManager;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.ChapterData;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;
import com.intelli.ezed.data.EzedTransactionCodes;
import com.intelli.ezed.data.Response;

public class ChapterDetailsFetcher extends SingletonRule
{
   private String dbConnName;
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.SERVICE_LOG);

   public ChapterDetailsFetcher(String ruleName, String dbConnName)
   {
      super(ruleName);
      this.dbConnName = dbConnName;
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      Response response = (Response) flowContext.getFromContext(EzedKeyConstants.EZED_RESPONSE);
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());

      String chapterId = messageData.getParameter("chapterid");
      if (chapterId == null || chapterId.trim().compareTo("") == 0)
      {
         if (logger.isDebugEnabled())
         {
            logger.debug("Chapter id in request in invalid");
         }
         response.setParameters(EzedTransactionCodes.INVALID_MESSAGE_PARAMETER, "Invalid chapter id");
         return RuleDecisionKey.ON_FAILURE;
      }

      Connection conn = DbConnectionManager.getConnectionByName(dbConnName);
      PreparedStatement ps = null;
      ResultSet rs = null;
      try
      {
         ps = DbConnectionManager.getPreparedStatement(dbConnName, conn, "FetchChapterDetail");
         ps.setString(1, chapterId);
         rs = ps.executeQuery();
         if (rs.next())
         {
            ChapterData chapterData = new ChapterData();
            chapterData.setChapterId(rs.getString("chapterid"));
            chapterData.setChapterName(rs.getString("chaptername"));
            chapterData.setDesc(rs.getString("description"));
            response.setResponseObject(chapterData);
            response.setParameters(EzedTransactionCodes.SUCCESS, "SUCCESS");
            return RuleDecisionKey.ON_SUCCESS;
         }
         else
         {
            if (logger.isDebugEnabled())
            {
               logger.debug("No record found in DB for chapterid " + chapterId);
            }
            response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Course not found");
            return RuleDecisionKey.ON_FAILURE;
         }
      }
      catch (SQLException e)
      {
         logger.error("Error while fetching chapter details from db for chapter id " + chapterId);
         response.setParameters(EzedTransactionCodes.DB_EXCEPTION_OCCURRED, "Error while fetching chapter details");
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while fetching chapter details from db for chapter id " + chapterId);
         response.setParameters(EzedTransactionCodes.INTERNAL_ERROR, "Error while fetching chapter details");
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         if (rs != null)
         {
            try
            {
               rs.close();
            }
            catch (SQLException e)
            {
               logger.error("Error while closing result set while fetching chapter detail", e);
            }
         }
         DbConnectionManager.releaseConnection(dbConnName, conn);
      }
   }
}
