package com.intelli.ezed.bulkupload.rules;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.apache.commons.digester3.Digester;
import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import com.intelli.ezed.bulkupload.data.QuestionConditionRule;
import com.intelli.ezed.bulkupload.data.QuizDetails;
import com.intelli.ezed.data.EzedKeyConstants;
import com.intelli.ezed.data.EzedLoggerName;

public class QuizDetailsHandler extends SingletonRule
{
   private Logger logger = LoggerFactory.getLogger(EzedLoggerName.BULK_UPLOAD_LOG);
   private String quizFileName;
   private Digester digester = new Digester();
   private static Object lockObject = new Object();
   private String dbConnName;


   public QuizDetailsHandler(String ruleName, String dbConnName, String quizFileName, String quizDetailsClassFQCN)
   {
      super(ruleName);
      initDigester(quizDetailsClassFQCN);
      this.dbConnName = dbConnName;
      this.quizFileName = quizFileName;
   }

   private void initDigester(String quizDetailsClassFQCN)
   {
      digester.addObjectCreate("quizquestions", quizDetailsClassFQCN);
      digester.addRule("quizquestions/question", new QuestionConditionRule());

      digester.addObjectCreate("quizquestions/question/questionstatement", "com.intelli.ezed.data.QuestionContent");
      digester.addCallMethod("quizquestions/question/questionstatement/type", "setType", 0, new Class<?>[]{Integer.class});
      digester.addCallMethod("quizquestions/question/questionstatement/string", "setContentString", 0);
      digester.addCallMethod("quizquestions/question/questionstatement/content", "setContentLoc", 0);
      digester.addSetNext("quizquestions/question/questionstatement/content", "setQuestion", "com.intelli.ezed.data.QuestionContent");

      digester.addObjectCreate("quizquestions/question/answer1statement", "com.intelli.ezed.data.QuestionContent");
      digester.addCallMethod("quizquestions/question/answer1statement/type", "setType", 0, new Class<?>[]{Integer.class});
      digester.addCallMethod("quizquestions/question/answer1statement/string", "setContentString", 0);
      digester.addCallMethod("quizquestions/question/answer1statement/content", "setContentLoc", 0);
      digester.addSetNext("quizquestions/question/answer1statement/content", "addAnswers", "com.intelli.ezed.data.QuestionContent");

      digester.addObjectCreate("quizquestions/question/question1statement", "com.intelli.ezed.data.QuestionContent");
      digester.addCallMethod("quizquestions/question/question1statement/type", "setType", 0, new Class<?>[]{Integer.class});
      digester.addCallMethod("quizquestions/question/question1statement/string", "setContentString", 0);
      digester.addCallMethod("quizquestions/question/question1statement/content", "setContentLoc", 0);
      digester.addSetNext("quizquestions/question/question1statement/content", "addQuestions", "com.intelli.ezed.data.QuestionContent");

      digester.addObjectCreate("quizquestions/question/answer2statement", "com.intelli.ezed.data.QuestionContent");
      digester.addCallMethod("quizquestions/question/answer2statement/type", "setType", 0, new Class<?>[]{Integer.class});
      digester.addCallMethod("quizquestions/question/answer2statement/string", "setContentString", 0);
      digester.addCallMethod("quizquestions/question/answer2statement/content", "setContentLoc", 0);
      digester.addSetNext("quizquestions/question/answer2statement/content", "addAnswers", "com.intelli.ezed.data.QuestionContent");

      digester.addObjectCreate("quizquestions/question/question2statement", "com.intelli.ezed.data.QuestionContent");
      digester.addCallMethod("quizquestions/question/question2statement/type", "setType", 0, new Class<?>[]{Integer.class});
      digester.addCallMethod("quizquestions/question/question2statement/string", "setContentString", 0);
      digester.addCallMethod("quizquestions/question/question2statement/content", "setContentLoc", 0);
      digester.addSetNext("quizquestions/question/question2statement/content", "addQuestions", "com.intelli.ezed.data.QuestionContent");

      digester.addObjectCreate("quizquestions/question/answer3statement", "com.intelli.ezed.data.QuestionContent");
      digester.addCallMethod("quizquestions/question/answer3statement/type", "setType", 0, new Class<?>[]{Integer.class});
      digester.addCallMethod("quizquestions/question/answer3statement/string", "setContentString", 0);
      digester.addCallMethod("quizquestions/question/answer3statement/content", "setContentLoc", 0);
      digester.addSetNext("quizquestions/question/answer3statement/content", "addAnswers", "com.intelli.ezed.data.QuestionContent");

      digester.addObjectCreate("quizquestions/question/question3statement", "com.intelli.ezed.data.QuestionContent");
      digester.addCallMethod("quizquestions/question/question3statement/type", "setType", 0, new Class<?>[]{Integer.class});
      digester.addCallMethod("quizquestions/question/question3statement/string", "setContentString", 0);
      digester.addCallMethod("quizquestions/question/question3statement/content", "setContentLoc", 0);
      digester.addSetNext("quizquestions/question/question3statement/content", "addQuestions", "com.intelli.ezed.data.QuestionContent");

      digester.addObjectCreate("quizquestions/question/answer4statement", "com.intelli.ezed.data.QuestionContent");
      digester.addCallMethod("quizquestions/question/answer4statement/type", "setType", 0, new Class<?>[]{Integer.class});
      digester.addCallMethod("quizquestions/question/answer4statement/string", "setContentString", 0);
      digester.addCallMethod("quizquestions/question/answer4statement/content", "setContentLoc", 0);
      digester.addSetNext("quizquestions/question/answer4statement/content", "addAnswers", "com.intelli.ezed.data.QuestionContent");

      digester.addObjectCreate("quizquestions/question/question4statement", "com.intelli.ezed.data.QuestionContent");
      digester.addCallMethod("quizquestions/question/question4statement/type", "setType", 0, new Class<?>[]{Integer.class});
      digester.addCallMethod("quizquestions/question/question4statement/string", "setContentString", 0);
      digester.addCallMethod("quizquestions/question/question4statement/content", "setContentLoc", 0);
      digester.addSetNext("quizquestions/question/question4statement/content", "addQuestions", "com.intelli.ezed.data.QuestionContent");

      digester.addObjectCreate("quizquestions/question/answer5statement", "com.intelli.ezed.data.QuestionContent");
      digester.addCallMethod("quizquestions/question/answer5statement/type", "setType", 0, new Class<?>[]{Integer.class});
      digester.addCallMethod("quizquestions/question/answer5statement/string", "setContentString", 0);
      digester.addCallMethod("quizquestions/question/answer5statement/content", "setContentLoc", 0);
      digester.addSetNext("quizquestions/question/answer5statement/content", "addAnswers", "com.intelli.ezed.data.QuestionContent");

      digester.addObjectCreate("quizquestions/question/question5statement", "com.intelli.ezed.data.QuestionContent");
      digester.addCallMethod("quizquestions/question/question5statement/type", "setType", 0, new Class<?>[]{Integer.class});
      digester.addCallMethod("quizquestions/question/question5statement/string", "setContentString", 0);
      digester.addCallMethod("quizquestions/question/question5statement/content", "setContentLoc", 0);
      digester.addSetNext("quizquestions/question/question5statement/content", "addQuestions", "com.intelli.ezed.data.QuestionContent");

      digester.addObjectCreate("quizquestions/question/answer6statement", "com.intelli.ezed.data.QuestionContent");
      digester.addCallMethod("quizquestions/question/answer6statement/type", "setType", 0, new Class<?>[]{Integer.class});
      digester.addCallMethod("quizquestions/question/answer6statement/string", "setContentString", 0);
      digester.addCallMethod("quizquestions/question/answer6statement/content", "setContentLoc", 0);
      digester.addSetNext("quizquestions/question/answer6statement/content", "addAnswers", "com.intelli.ezed.data.QuestionContent");

      digester.addObjectCreate("quizquestions/question/question6statement", "com.intelli.ezed.data.QuestionContent");
      digester.addCallMethod("quizquestions/question/question6statement/type", "setType", 0, new Class<?>[]{Integer.class});
      digester.addCallMethod("quizquestions/question/question6statement/string", "setContentString", 0);
      digester.addCallMethod("quizquestions/question/question6statement/content", "setContentLoc", 0);
      digester.addSetNext("quizquestions/question/question6statement/content", "addQuestions", "com.intelli.ezed.data.QuestionContent");

      digester.addObjectCreate("quizquestions/question/explanation", "com.intelli.ezed.data.QuestionContent");
      digester.addCallMethod("quizquestions/question/explanation/type", "setType", 0, new Class<?>[]{Integer.class});
      digester.addCallMethod("quizquestions/question/explanation/string", "setContentString", 0);
      digester.addCallMethod("quizquestions/question/explanation/content", "setContentLoc", 0);
      digester.addSetNext("quizquestions/question/explanation/content", "setExplanation", "com.intelli.ezed.data.QuestionContent");

      digester.addCallMethod("quizquestions/question/correctanswer", "setCorrectAnswer", 0);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      String contentDirFileName = (String) flowContext.getFromContext(EzedKeyConstants.BULK_UPLOAD_CONTENTFILE);
      File contentDirFile = new File(contentDirFileName);
      String quizFile = contentDirFile.getAbsolutePath() + File.separator + quizFileName;
      String contentId = (String) flowContext.getFromContext(EzedKeyConstants.BULK_UPLOAD_CONTENTID);
      QuizDetails quizDetails = null;
      BulkUploadInitiator initiator = (BulkUploadInitiator) flowContext.getFromContext(EzedKeyConstants.BULK_UPLOAD_STARTER);
      Map<String, String> contentDirIdMap = (Map<String, String>) flowContext.getFromContext(EzedKeyConstants.BULK_UPLOAD_CONTENTMAP);

      try
      {
         synchronized (lockObject)
         {
            quizDetails = (QuizDetails) digester.parse(new File(quizFile));
         }
         quizDetails.setQuizId(contentId);
         quizDetails.replaceContentIds(contentDirIdMap);
         quizDetails.persistQuiz(dbConnName, logger);
      }
      catch (IOException e)
      {
         logger.error("Error while reading quiz file", e);
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (SAXException e)
      {
         logger.error("Error while reading quiz file", e);
         return RuleDecisionKey.ON_FAILURE;
      }
      catch (Exception e)
      {
         logger.error("Error while processing quiz after reading properly from file", e);
         return RuleDecisionKey.ON_FAILURE;
      }
      finally
      {
         initiator.decrementQuizCount();
      }
      return RuleDecisionKey.ON_SUCCESS;
   }
}
