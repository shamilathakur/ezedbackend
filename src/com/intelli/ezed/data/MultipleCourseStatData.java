package com.intelli.ezed.data;

import java.util.ArrayList;

import org.json.JSONArray;

public class MultipleCourseStatData extends ResponseObject
{
   private ArrayList<CourseStatData> dataList = new ArrayList<CourseStatData>();

   @Override
   public void prepareResponse() throws Exception
   {
      JSONArray jsonArray = new JSONArray();
      for (int i = 0; i < dataList.size(); i++)
      {
         dataList.get(i).prepareResponse();
         jsonArray.put(dataList.get(i));
      }
      this.put("coursestats", jsonArray);
   }

   public ArrayList<CourseStatData> getDataList()
   {
      return dataList;
   }

   public void setDataList(ArrayList<CourseStatData> dataList)
   {
      this.dataList = dataList;
   }

}
