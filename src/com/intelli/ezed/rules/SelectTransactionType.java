package com.intelli.ezed.rules;

import org.sabre.rulengine.RuleDecisionKey;
import org.sabre.rulengine.rule.SingletonRule;
import org.sabre.workflow.context.FlowContext;

import com.a.helper.data.MessageData;
import com.intelli.ezed.data.EzedKeyConstants;

public class SelectTransactionType extends SingletonRule
{
   public SelectTransactionType(String ruleName)
   {
      super(ruleName);
   }

   @Override
   public RuleDecisionKey executeRule(FlowContext flowContext)
   {
      MessageData messageData = (MessageData) flowContext.getFromContext(MessageData.class.getName());

      String keyword = messageData.getParameter("svc");
      flowContext.putIntoContext(EzedKeyConstants.KEYWORD, keyword);

      return RuleDecisionKey.ON_SUCCESS;
   }

}
